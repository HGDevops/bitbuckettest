<apex:component controller="CARPOL_UNI_ApplPDFCompController" >
<apex:attribute type="boolean" name="showSearchForm" description="header search form" default="false" />
<apex:attribute name="showapplicationPDF" type="boolean" description="do you want to include the Application PDF in the custom sidebar section" default="false"/>
<apex:attribute name="lineitemIDfromcomp" type="string" description="do you want to include the Application PDF in the custom sidebar section" default="true" assignTo="{!lineitemID}"/>
<apex:attribute name="applicationIDfromcomp" type="string" description="pass application ID" default="true" assignTo="{!applicationid}"/>
<apex:attribute name="CBIcheckfromcomp" type="string" description="pass CBI Check value" default="true" assignTo="{!CBIcheckvalue}"/>
<apex:attribute name="showUserMenuOption" type="boolean" description="displays the menu option allow the user to view and make changes in the user invite page. this should be available to account administrators only" default="true" />
<apex:attribute name="showHeader" type="boolean" description="remove header menu optional" default="true" />
<apex:attribute name="showAccountApps" type="boolean" description="displays the menu option allow the user to view and make changes in the Account Application Page. this should be available to account administrators only" default="true"/>
<apex:attribute name="showLogo" type="boolean" description="removes logo from header" default="true" />

    <script type="text/javascript"> 
      window.$Label = window.$Label || {};
      $Label.EFLCBINoValidationMsg = '{!JSENCODE($Label.EFLCBINoValidationMsg)}';
      $Label.EFLCBIYesValidationMsg = '{!JSENCODE($Label.EFLCBIYesValidationMsg)}';
</script>
<div class="row" >
    <div class="col-xs-12">
        <div class="header-top">
                <!-- site branding -->
            <span id="branding" display="none">
                    <apex:outputPanel rendered="{!showLogo}"> 
                    <a href="https://www.aphis.usda.gov/aphis/home/" target="_blank" class="logo big-logo">
                      <apex:image width="65" height="44" title="USDA Logo" alt="USDA Logo" url="{!URLFOR($Resource.newMobileLogo)}" styleClass="logo header-logo"  rendered="{!showLogo}"/>
                    </a>                            
                        <div class="logo big-logo"><h4 class="header-title-top"><b>United States Department of Agriculture</b></h4></div>
                        <div class="logo big-logo"><h5 class="header-title-bottom">Animal and Plant Health Inspection Service eFile</h5></div>
                    <a href="" class="logo small-logo">
                        <apex:image width="72" height="51" title="USDA Logo" alt="USDA Logo" url="{!URLFOR($Resource.newMobileLogo)}" />
                    </a>
                    </apex:outputPanel>
                </span>
                <!--<span id="branding" display="none">
                    <apex:outputPanel rendered="{!showLogo}"> 
                    <a href="https://www.aphis.usda.gov/aphis/home/" target="_blank" class="logo big-logo">
                      <apex:image url="{!URLFOR($Resource.newLogo)}" styleClass="logo" width="450" rendered="{!showLogo}"/>
                    </a>
                    <a href="" class="logo small-logo">
                        <apex:image width="72" height="51" url="{!URLFOR($Resource.newMobileLogo)}" />
                    </a>
                    </apex:outputPanel>
                </span>-->
        <apex:outputPanel rendered="{!showHeader}">
                <!-- mobile navigation toggle -->
                <div class="mobile-nav navigation-toggle">
                    <nav class="navbar-default">
                        <button aria-expanded="false" data-target=".nav-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button" >
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </nav>
                <!-- top navigation -->
                <nav class="navbar-collapse" id="top-nav">
                    <c:listgroup styleClass="nav">
                        <c:listgroupitem role="menu">
                            <a target="_blank" href="apex/CARPOL_UNI_eAuthExternalProfileRequest"><span>Profile Request</span></a>
                        </c:listgroupitem>
                        <!--<c:listgroupitem rendered="{!TEXT($User.UserType) == 'CSPLitePortal'}">
                            <a target="_blank" class="brokerRequest" href="apex/brokerPrepareForm"><span>Request Broker/Preparer</span></a>
                        </c:listgroupitem>-->
                        <c:listgroupitem role="menu">
                            <a target="_blank" href="https://www.aphis.usda.gov/help/efile/contactus"><span>Contact Us</span></a>
                        </c:listgroupitem>
                        <c:listgroupitem role="menu">
                            <a target="_blank" href="https://www.aphis.usda.gov/help/efile/glossary"><span>Glossary</span></a>
                        </c:listgroupitem>
                        <c:listgroupitem role="menu" >
                            <a href="https://www.aphis.usda.gov/help/efile/support" target="_blank"><span>Help</span></a>
                        </c:listgroupitem>
                    </c:listgroup>
                </nav>
                </div>
        </apex:outputPanel>
        </div>
        <apex:outputPanel rendered="{!showHeader}">
        <div class="header-middle">
            <!-- search form -->
            <div id="search-form">
                <c:header_search rendered="{!showSearchForm}"/>
            </div><!-- end #search-form-->
        </div>
        <!-- main navigayion -->
        <div id="main-nav-wrap">
            <nav class="navbar navbar-default">
                <div class="navbar-collapse nav-collapse collapse main-nav" aria-expanded="true" style="">
                  <nav id="main-nav">
                            <c:navigation showUserMenuOption="{!showUserMenuOption}" showAccountApps="{!showAccountApps}"/>
                    </nav> 
                    <!-- user navigation -->
                    <div class="right-nav">
                        <c:ApplicationPDF lineitem="{!lineitemIDfromcomp}" application="{!applicationIDfromcomp}" rendered="{!showapplicationPDF}" CBIcheck="{!CBIcheckfromcomp}"/>
                        <!-- start app -->
                        <apex:outputPanel rendered="{!TEXT($User.UserType) == 'CSPLitePortal' || TEXT($User.UserType) == 'Standard' || (TEXT($User.UserType) == 'PowerCustomerSuccess' && $Profile.Name == 'eFile Applicant Plus') || (TEXT($User.UserType) == 'PowerPartner' && $Profile.Name == 'Org Admin')}">      
                         <div class="start-app" style="float:left;">                             
                            <nav class="navbar navbar-default">
                                <div class="navbar-collapse nav-collapse collapse" aria-expanded="true">
                                    <nav>
                                         <ul class="nav navbar-nav">
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle background-blue btn" role="button"><i class="start-icon glyphicon glyphicon-plus-sign"></i><span>Start New ...</span><b class="caret"></b></a>
                                                 <ul class="dropdown-menu">
                                                    <!--<li>
                                                        <a href="/apex/CARPOL_HOMEPAGE"><span>New Application</span></a>
                                                    </li>-->
                                                     <li role="menu">
                                                        <a href="/apex/EFLPreScreeningQuestionnaire"><span>New Application</span></a>
                                                     </li>                                                      
                                                     <!--W-033797 M.Coindrea 2-27-19
                                                    <li>
                                                        <a href="/apex/CARPOL_BRS_Upload_Application_XML_FILE"><span>New Biotechnologist Application via XML</span></a>
                                                    </li>-->
                                                    <apex:outputPanel rendered="{!Not(ISPICKVAL($User.UserType,'Standard'))}" >
                                                    <li role="menu">
                                                     <a href="/apex/EFLXMLApplication"><span>New Application via XML</span></a>
                                                    </li>
                                                    </apex:outputPanel> 
                                                    <!--<li>
                                                        <a href="/apex/EFLCreateApplFromTemplate"><span>New Application from Template</span></a>
                                                    </li>-->                                                                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </nav>
                        </div>
                        </apex:outputPanel>
                        <nav class="navbar navbar-default" id="userNav-wrapp">
                            <div class="navbar-collapse nav-collapse collapse" aria-expanded="true" >
                                <nav id="userNav" ><ul class="nav navbar-nav"><li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle" role="button">
                                        <span class="nav-name">{!$User.FirstName} {!$User.LastName}</span><b class="caret"></b></a>
                                           <ul aria-labelledby="drop1" role="menu" class="dropdown-menu"><li role="presentation">
                                                <a href="{!logoutURL}" role="menuitem" class="logout">
                                                   <span>Logout</span> </a>
                                              </li></ul>    
                                   </li></ul></nav>
                            </div>
                        </nav>
                    </div>
                </div>
            </nav>
            
        </div> <!--main-nav-wrap-->
        </apex:outputPanel>
    </div>
</div>
                    <!-- end main navigation -->
</apex:component>