trigger CARPOL_Application_Condition_ChangeHistory on Application_Condition__c (after insert, after update, after delete) {
        //GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Application_Condition__c');
   
    if(trigger.isafter) {
     if (Trigger.isUpdate )
     {GenericHistoryClass.LogUpdates(trigger.new,trigger.oldmap);}
   
    else if (Trigger.isInsert)
    {GenericHistoryClass.LogNewEntries(trigger.new);}
    
    else if (Trigger.isDelete)
    {GenericHistoryClass.LogDeletedEntries(trigger.old);}
        
    }
}