trigger EFLReportSummaryTrigger on Report_Summary__c ( before insert, before update, before delete, 
                                                       after insert, after update, after delete ) {
    
    
    EFLTriggerFactory.createAndExecuteHandler(EFLReportSummaryTriggerHandler.class); 
  
}