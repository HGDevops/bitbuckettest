trigger EFLESignLive on Attachment (after insert) {
   /**** Teh Liu: Removed comment as I updated helper class to run for esl_package_document
   Comment out due to inactive trigger - josh thigpen
   **********/ 
    EFLESLPackageTriggerHelper eslpTriggerHelper = new EFLESLPackageTriggerHelper();
    
    if(trigger.isAfter && trigger.isInsert && System.Label.EFLeSignLive == 'Yes'){
        if (EFLESLPackageTriggerHelper.runOnce()) {
           
            eslpTriggerHelper.attachPermit(Trigger.new);   
        }
    }

}