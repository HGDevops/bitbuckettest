trigger EFLAnnualReportTrigger on EFLAnnual_Report__c (before insert, before update, before delete
                                                      , after insert, after update, after delete
                                                      , after undelete) {

	new EFLAnnualReportTriggerHandler().run();
}