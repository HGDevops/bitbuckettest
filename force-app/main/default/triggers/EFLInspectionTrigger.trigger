trigger EFLInspectionTrigger on Inspection__c (before insert, before update, before delete, 
                                               after insert, after update, after delete) {
 
    EFLTriggerFactory.createAndExecuteHandler(EFLInspectionTriggerHandler.class); 
    /*if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)){
        EFLInspectionTriggerHandler.addQuestionOnInspection(trigger.newMap, trigger.oldMap);
    }*/
}