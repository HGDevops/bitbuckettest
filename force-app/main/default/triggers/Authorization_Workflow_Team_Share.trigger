trigger Authorization_Workflow_Team_Share on Team__c (after insert, after update) {

    if(trigger.isInsert){        
    
    List<Authorizations__Share> teamShares = new List<Authorizations__Share>();    
    Authorizations__Share mbrShr;
        
    for(Team__c member : trigger.new){        
        mbrShr = new Authorizations__Share();        
        mbrShr.ParentId = member.Authorization__c;        
        mbrShr.UserOrGroupId = member.Member__c;        
        mbrShr.AccessLevel = 'edit';        
        mbrShr.RowCause = Schema.Authorizations__Share.RowCause.Authorization_Workflow_Team__c;        
        teamShares.add(mbrShr);    
    }    
    Database.SaveResult[] teamShareInsertResult = Database.insert(teamShares,false);
    
    } else if (Trigger.isUpdate) {
        
        Map<String,String> membersToDelete = new Map<String,String>();
        List<String> relatedRecordForDeletion = new List<String>();
        List<Authorizations__Share> teamDelete = new List<Authorizations__Share>();
        Map<String,Authorizations__Share> TeamDeleteMap = new Map<String,Authorizations__Share>();
        List<Authorizations__Share> finalDeleteList = new List<Authorizations__Share>();
        List<Authorizations__Share> teamShares = new List<Authorizations__Share>();
        
        for(Team__c updatedMember : trigger.new){            
            Team__c oldmember = Trigger.oldMap.get(updatedMember.id);
            if(oldmember.Member__c != updatedMember.Member__c){
               membersToDelete.put(oldmember.Member__c +';'+ oldmember.Authorization__c,oldmember.Member__c);
               relatedRecordForDeletion.add(updatedMember.Authorization__c);
            }            
        }
        
        if(!membersToDelete.isEmpty()){
            teamDelete = [select id,ParentID,UserOrGroupId from Authorizations__Share 
                          where ParentID in:relatedRecordForDeletion AND RowCause =: Schema.Authorizations__Share.RowCause.Authorization_Workflow_Team__c ];
            
             for(Authorizations__Share currentShare : teamDelete){
                TeamDeleteMap.put(currentShare.UserOrGroupId + ';' + currentShare.ParentId , currentShare);
             }      
        
             for(String currentDeleteRecord : membersToDelete.keySet()){
                 if(TeamDeleteMap.get(currentDeleteRecord) != null){
           		      finalDeleteList.add(TeamDeleteMap.get(currentDeleteRecord));
                 }        
             }
            delete(finalDeleteList);
        }      
            
        Authorizations__Share mbrShr;
        
        for(Team__c member : trigger.new){        
        mbrShr = new Authorizations__Share();        
        mbrShr.ParentId = member.Authorization__c;        
        mbrShr.UserOrGroupId = member.Member__c;        
        mbrShr.AccessLevel = 'edit';        
        mbrShr.RowCause = Schema.Authorizations__Share.RowCause.Authorization_Workflow_Team__c;        
        teamShares.add(mbrShr);    
        }        
        Database.SaveResult[] teamShareInsertResult = Database.insert(teamShares,false);
    }
}