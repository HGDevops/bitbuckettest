trigger EFLAuthorizationTrigger on Authorizations__c ( before insert, before update, before delete, 
                                                       after insert, after update, after delete ) {
   
    EFLTriggerFactory.createAndExecuteHandler(EFLAuthorizationTriggerHandler.class);
    
}