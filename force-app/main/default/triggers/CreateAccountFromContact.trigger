trigger CreateAccountFromContact on Contact (before insert,before update) {
    
    //SG - 8/8/2019
    //change all logic to be in trigger handler instead of trigger
    if (trigger.isinsert){
        if(trigger.isbefore){
            EFLContactTriggerHandler.doBeforeInsert(trigger.new);
        }
    }
    
    // VV added for AC Annual reports: There should be at least 1 FA and no more than 2 FAs. 
    // Validation is just for the internal users only
    if (trigger.isupdate){
        if(trigger.isbefore){
            EFLContactTriggerHandler.doBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
}