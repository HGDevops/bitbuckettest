trigger EFLESLPackageTrigger on ESL__Package__c (After Update) {
    EFLESLPackageTriggerHelper eslpTriggerHelper = new EFLESLPackageTriggerHelper();
    
    if(trigger.isAfter && System.Label.EFLeSignLive == 'Yes'){
        if(trigger.isUpdate){
            eslpTriggerHelper.isAfterUpdate(Trigger.new, Trigger.OldMap);
        }
    }
}