trigger ArticleSupplierDeveloperTrigger on Article_Supplier_Developer__c (before insert, before update, after update ,after insert, after delete) {
/*
 * Author: Ravee Racharla Date:11/14/2018
 * Purpose: Trigger for Article Supplier/Developer object
*/
    if (Trigger.isBefore &&  (Trigger.isinsert || Trigger.isUpdate) ) {
        EFLASDTriggerHandler.ValidateRequiredFields(Trigger.new);          
    }       
    
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert || Trigger.isUpdate)
        {
            EFLASDTriggerHandler.ACArticlSupplierProcessReadiness(Trigger.New);
        }
        if(Trigger.isDelete)
        {
            EFLASDTriggerHandler.ACArticlSupplierProcessReadiness(Trigger.Old);
        }
        GenericHistoryClass.LogChangeHistory();
    }        
}