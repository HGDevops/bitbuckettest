/**********************************************************************************************
Description: Apex trigger to update MAX(LastTDY) on Responder from All Related Resource Requests
Author: Harish Gudipudi
***********************************************************************************************/

trigger EQS_UpdateLastTDY on EQS_Resource_Request__c(after update, after insert){       
    set<Id> conIds = new Set<Id>(); 
    List<Contact> con = new List<Contact>();
    List<AggregateResult> requests = new List<AggregateResult>();
    
    For(EQS_Resource_Request__c er:trigger.new){    
        conIds.add(er.EQS_Responder__c); 
    }
   
    con = [SELECT Id, EQS_Last_TDY__c FROM Contact WHERE Id IN: conIds];    
    requests = [SELECT EQS_Responder__c, MAX(EQS_Last_TDY__c) LastTDY FROM EQS_Resource_Request__c WHERE EQS_Responder__c IN: conIds GROUP BY EQS_Responder__c ];
           
    for(Contact c: con){
        Date Lastdate;        
        for (AggregateResult b : requests){         
          if (b.get('EQS_Responder__c')==c.Id){ 
                if(b.get('LastTDY') != NULL){
                    Lastdate = (Date)b.get('LastTDY');
                    system.debug('@@@@'+Lastdate );
                }                 
            }
        } 
        c.EQS_Last_TDY__c = Lastdate;
    }
    update (con);  
}