trigger EFLInspectionQuestionnaireQns on EFL_Inspection_Questionnaire_Questions__c (after delete, after insert, after update, before delete, before insert, before update) {
    
     EFLTriggerFactory.createAndExecuteHandler(EFLInspQuestionnaireQnsTriggerHandler.class);
}