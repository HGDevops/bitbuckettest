trigger EFLLocationRelatedMaterialTrigger on Material__c (before insert, before update,after insert, after update, after delete) {
    list<location__c> locationList = new list<location__c>();
    set<id> locationIdSet = new set<id>();
    if (Trigger.IsBefore && Trigger.IsInsert){
        EFLLocationRelatedMaterialTriggerHandler.validateFieldFormatAndRequired(Trigger.New);
    }
    if (Trigger.IsBefore && Trigger.IsUpdate){
        EFLLocationRelatedMaterialTriggerHandler.validateFieldFormatAndRequired(Trigger.New);
    }
    if(Trigger.isafter){
        if( Trigger.Isinsert){
        for(Material__c mat : Trigger.New){
           locationIdSet.add(mat.location__c); 
        } 
        }
        if( Trigger.Isdelete){
        for(Material__c mat : trigger.old){
           locationIdSet.add(mat.location__c); 
        } 
        }
       locationList = [select id,Name,Line_item__c from Location__c where id IN:locationIdSet];
        if(locationList.size()>0){
        CARPOL_BRS_LocationsTriggerHandler.processLocationReadiness(locationList);
        }
        
         GenericHistoryClass.LogChangeHistory();
}
}