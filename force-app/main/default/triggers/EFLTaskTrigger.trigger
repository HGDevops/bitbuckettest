trigger EFLTaskTrigger on Task ( before insert, before update, before delete, 
                                                       after insert, after update, after delete ) {
   
    EFLTriggerFactory.createAndExecuteHandler(EFLTaskTriggerHandler.class);
    
    
    /*
     List<Task> taskToInsert = new list<Task>();
     if (trigger.isUpdate && trigger.isAfter){
    	 system.debug(' INSIDE After-Update Event ');

	    //Get all my Updated complete Task Id's.
	    for( Task t : Trigger.new ) {
            system.debug(' INSIDE task level$$'+t);
             system.debug(' INSIDE t.stage__c$$'+t.stage__c);
            
        Authorizations__c authorizationRec = [SELECT id,prefix__c
                                                   FROM Authorizations__c
                                                  WHERE id =: t.whatId];
            
	    list<Program_Activity__mdt> progActivityMetaData = [select Activity_Sequence__c,Activity_Type__c,
                                                                   Assigned_To__c,Due_Date__c,
                                                                   Priority__c,Program_Stage__c,Subject__c,
                                                                   Program_Stage__r.Stage__c
                                                              from Program_Activity__mdt
                                                             where Program_Stage__r.Stage__c =: t.stage__c
                                                               AND Program_Stage__r.Program_Prefix__c =: authorizationRec.Prefix__c
                                                               AND Activity_Sequence__c =: t.Activity_Sequence__c+1]; 
          system.debug(' INSIDE progActivityMetaData$$'+progActivityMetaData);            
            if( t.Status == 'Completed' && progActivityMetaData.size()>0){
                 system.debug(' INSIDE t.Status$$'+t.Status); 
                for(Program_Activity__mdt pa : progActivityMetaData){ 
                      system.debug(' INSIDE pa.Program_Stage__r.Stage__c$$'+pa.Program_Stage__r.Stage__c); 
                    Task objTask = new Task();
                    objTask.Subject           = pa.Subject__c;
                    objTask.Priority          = pa.Priority__c;
                   // objTask.OwnerId           = pa.User__c;
                    objTask.whatId   = t.whatId;
                    objTask.Status   = 'Not Started';
                    objTask.stage__c = pa.Program_Stage__r.Stage__c;
                    objTask.Activity_Sequence__c = pa.Activity_Sequence__c;
                    objTask.RecordTypeId = EFLGenericUtility.getRecordTypeId('TASK Standard');
                    objTask.Type = pa.Activity_Type__c;
                   // objTask.OwnerId  = taskOwnerID;                
                 system.debug('pa.Assigned_To__c@@@'+pa.Assigned_To__c);
                    id taskOwnerID = [select Member__c 
                                      from Team__c
                                      where member_role__c =: pa.Assigned_To__c
                                      and authorization__c =: t.whatId].Member__c;
                    if(taskOwnerID!=null){
                        objTask.OwnerId  = taskOwnerID;
                    }
                    taskToInsert.add(objTask);
                       system.debug(' INSIDE taskToInsert$$'+taskToInsert); 
                }
            }
        }
         
         
     }   
    
     if(!taskToInsert.isEmpty())
        insert taskToInsert ;
    
    //Validation to prevent the user from deleting activity history
    if (trigger.isDelete && trigger.isBefore){ 
        for( Task t : Trigger.old ) {
            string taskDeletionMsg = EFLGenericUtility.getMessage('ActivityHistoryDelete');   
            t.addError(taskDeletionMsg); 
        } 
    }

*/
    
}