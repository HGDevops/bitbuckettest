trigger CARPOL_UNI_MasterAuthJunctTrigger on Authorization_Junction__c (before insert, before update, after insert, after update, before delete, after delete) {
    
 
if (Trigger.isBefore) //====================================================BEFORE=================================================
          {
          
              if (Trigger.isInsert) //====================================================BEFORE=================================================
                  {
                      //Refactored by Terry Hale 2/5/19 to remove SOQL from for loop
                      List<Regulation__c> Reg = new List<Regulation__c>();
                      List<String> regulationIds = new List<String>();
                      Map<String, Regulation__c> regMap = new Map<String, Regulation__c>();
                      for(Authorization_Junction__c AJ:trigger.new){
                          if(!String.isBlank(AJ.Regulation__c)){
                              regulationIds.add(AJ.Regulation__c);
                          }
                      } 
                          
                      Reg = [SELECT ID, Name, Regulation_Description__c, Sub_Type__c, Type__c, Title__c, Custom_Name__c 
                                                         FROM Regulation__c WHERE ID in :regulationIds];
                                            
                      for(Regulation__c newReg:Reg){
                          regMap.put(newReg.id, newReg);
                      }       
                      
                      for(Authorization_Junction__c AJ:trigger.new){
                          system.debug('---->'+AJ);
                          //Commented out by Terry Hale 2/5/19 W-026313 -- SOQL inside for loop
                          //if(AJ.Regulation__c!= null || AJ.Regulation__c!=''){
                              //List<Regulation__c> Reg = [SELECT ID, Name, Regulation_Description__c, Sub_Type__c, Type__c, Title__c, Custom_Name__c 
                              //FROM Regulation__c WHERE ID=:AJ.Regulation__c LIMIT 1];
                           //}   
                          if(!regMap.isEmpty() && AJ.Regulation__c != NULL){
                                 // AJ.Regulation_Description__c = Reg[0].Regulation_Description__c;
                                 AJ.Regulation_Description__c = regMap.get(AJ.Regulation__c).Regulation_Description__c;
                              }
                          
                      }
                  }
         // GenericHistoryClass.LogChangeHistory();

          }
 if (Trigger.IsAfter)
    {
        GenericHistoryClass.LogChangeHistory();
    } 
  }