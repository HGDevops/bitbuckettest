trigger EFL_RegulationTrigger on Regulation__c (after insert) {
    
    if(Trigger.isAfter){
        
        if(Trigger.isInsert){
            
            CARPOL_Application_ManagedSharing.afterRegulationInsert(Trigger.New);
        }
    }

}