trigger CARPOL_BRS_LocationsTrigger on Location__c (before update,before insert,after update , after insert,after delete,before delete) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_LocationsTrigger                     */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities                      */ 
/*           Notifications,Status Update,                 */
/*           Validation Checks,Error handling             */    
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */

/*  ====================================================  */  
CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_LocationsTrigger');
 if(!dt.disable__c) {
    list<Location__c> lstloc  = trigger.new;
    list<string> lstlocids = new list<string>();
    list<string> lstorglocids = new list<string>();
    list<string> lstsite = new list<string>();
      List<String> sortedvalue= new list<string>();
    list<AC__c> lineitemlst = new list<AC__c>();
    list<ID> BRSlineitemlst = new list<ID>();
    list<ID> locids = new list<ID>();
    list<AC__c> listlineitemToUpdate = new list<AC__c>();
    list<Location__c> listlocToUpdate = new list<Location__c>();
    list<string> lstappids = new list<string>();
    string strprofileid = UserInfo.getProfileId();
    string brsappstatus = '';
    string brsappid = '';
    //string BRSProfile = [select id,Name from Profile where id=:strprofileid].Name;
    string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
    string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
    string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
    string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();  
    boolean olocrectype;
    boolean rslocrectype;
    boolean dlocrectype;
    boolean oanddlocrectype;
    
        //----------------------------------------------------BEFORE-------------------------------------------------------------------------// 
          if(trigger.isbefore){
          
           //--------------------------------UPDATE------------------------------------------------------------------//
           if(trigger.isupdate){
               EFLLocationTriggerHandler.validate(Trigger.New, Trigger.Old);                
         } 
         
           //--------------------------------INSERT------------------------------------------------------------------// 
           if(trigger.isInsert){
               EFLLocationTriggerHandler.validate(Trigger.New, null);
               
            list<string> lstall = new list<string>();
            list<string> newLineitemlist = new list<string>(); // newly Added Niharika-to get all new line items 
             
             for(Location__c l:trigger.new)
              {
                   if(l.Line_Item__c!=null ){//&& l.recordtypeid == olocrectypeid ){
                      lstall.add(l.Line_Item__c);
                  }
                  if(l.Line_Item__c!=null && l.recordtypeid == locrectypeid )
                  {
                      lstsite.add(l.Line_Item__c);
                  } 
                  System.debug('***Line Item:'+ l.Line_Item__c);
                  newLineitemlist.add(l.Line_Item__c);// newly Added Niharika
              }
                
                for(AC__c ac:[select id,Name,RecordType.Name,Status__c,Purpose_of_the_Importation__c,
                            (select id,RecordType.Name from locations__r) 
                              from AC__c 
                              where id in:lstall]){
                     if(ac.Status__c=='Submitted')// && (BRSProfile=='APHIS Applicant' || BRSProfile=='system administrator'))
                     {
                         lstloc[0].addError('Application is submitted you cannot insert new Location');
                     } 
                                  
                 } 
                 
             // Newly Added - Niharika- April 13
          /*  List<location__c> locationList = new list<location__c>();      
              string movementtype ='Import';
             String RecordTypeCourtesy ='Biotechnology Regulatory Services - Courtesy Permit';      
             String RecordTypeNotification ='Biotechnology Regulatory Services - Notification'; 
             
             list<AC__c> newLineitemlistTemp = [SELECT ID,RecordType.Name,Purpose_of_the_Importation__c From AC__c WHERE ID in:newLineitemlist AND movement_type__c=:movementtype AND (RecordType.Name=:RecordTypeCourtesy OR RecordType.Name=:RecordTypeNotification)];        
             System.debug('***Line Item new'+newLineItemlistTemp);
              list<string> newLineitemlist1=new list<string>();      
            
             for(ac__c ac: newLineitemlistTemp){        
                 newLineitemlist1.add(ac.id);       
             }  
             System.debug('***newLineitemlist1:'+newLineitemlist1);
             locationList = [select id,RecordType.Name,Line_Item__c from location__c WHERE Line_Item__c in: newLineitemlist1];
              System.debug('***Check HereLocationsList:' + locationlist);
             Map<String,List<location__c>> mapLineItemWiseLocations = new Map<String,List<location__c>>();  
             
                    For(location__c cont : locationList)        
                    {       
                        if(mapLineItemWiseLocations.containsKey(cont.Line_Item__c))     
                        {       
                            List<location__c> lstCont = mapLineItemWiseLocations.get(cont.Line_Item__c);        
                            lstCont.add(cont);      
                        }       
                        else        
                        {       
                            List<location__c> lstCont = new List<location__c>();        
                            lstCont.add(cont);      
                            mapLineItemWiseLocations.put(cont.Line_Item__c,lstCont);        
                        }       
                    } */

               }
               
       }       
       
         //----------------------------------------------------AFTER-------------------------------------------------------------------------//  
     if(trigger.isafter){
 
              if(checkRecursive.runOnce()){
             // After Insert 
                if(trigger.isinsert){
                    
                    //Anh Changes starts here
                    //femi-commented: CARPOL_UNI_LineItemRelatedRecords.fetchLineItemFromLoc(trigger.new);
                    //Anh Changes ends here
                    
                            System.debug('***Trigger.new insert'+Trigger.new.size());
                           for(Location__c l:trigger.new)                          
                           {
                              BRSlineitemlst.add(l.Line_Item__c); 
                              System.debug('***BRSLineItem:'+BRSLineitemlst +' '+l.Line_Item__c);
                              lstlocids.add(l.id);
                           }
                           
                            System.debug('***Location Ids after insert:' + lstlocids);
                            /*
                             list<AC__c> lineitemlistafter = [SELECT ID,RecordType.Name,movement_type__c,Location_Status__c ,(select id,recordtypeid from Locations__r) From AC__c WHERE ID in: BRSlineitemlst];       
                             System.debug('***BRS Line items after insert:'+ lineitemlistafter);
                                for (AC__c a : lineitemlistafter) {
                                    olocrectype = false;               //origin location
                                    oanddlocrectype = false;           // origin and destination
                                    dlocrectype = false;               //destination
                                    rslocrectype = false;              // release
                                if(a.Locations__r.size() > 0){
                                    for(Location__c lc:a.Locations__r){
                                         if(lc.recordtypeid == olocrectypeid)
                                            olocrectype = true;
                                         if(lc.recordtypeid == oanddlocrectypeId)
                                            oanddlocrectype = true;
                                         if(lc.recordtypeid == dlocrectypeId)
                                            dlocrectype = true;
                                        if(lc.recordtypeid == locrectypeid)
                                            rslocrectype = true;
                                        
                                    }
                                    
                                    if((olocrectype == true || oanddlocrectype == true) && (oanddlocrectype == true || dlocrectype == true) && a.movement_type__c == 'Interstate Movement') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    }else if((olocrectype == true || oanddlocrectype == true) && (oanddlocrectype == true || dlocrectype == true) && rslocrectype == true && a.movement_type__c == 'Interstate Movement and Release') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    } else if((olocrectype == true || oanddlocrectype == true) && (oanddlocrectype == true || dlocrectype == true) && a.movement_type__c == 'Import') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    } else if(rslocrectype == true && a.movement_type__c == 'Release') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    }  
  
                                }
                                  
                              } */
                              CARPOL_BRS_LocationsTriggerHandler.processLocationReadiness(trigger.new);
                       
                            //-------------CBI Fields---------------//                    
                        /*     for(Location__c loc1:[select id,Name,Country_CBI__c,Country__c,Country_Text__c,State__c,State_CBI__c,Level_2_Region__c,County__c,County_CBI__c,Primary_Country_CBI__c,Primary_Country__c,Primary_Country_Text__c, Primary_State__c, 
                                                        Primary_State_CBI__c,Primary_State__r.name, Contact_state__c,Primary_County__c, Contact_County__c, Primary_County_CBI__c, Secondary_Contact_County__c, Secondary_County__c, Secondary_Contact_State__c, Secondary_State__c,Secondary_Country__c, 
                                                        Secondary_Country_CBI__c, Material_Type_CBI__c,Material_Type_Text__c,Material_Type__c,Critical_Habitat_Involved_CBI__c,Critical_Habitat_Involved__c,If_Yes_Please_Explain__c,Secondary_Country_Text__c,Secondary_County_CBI__c, Secondary_State_CBI__c,Primary_County__r.name,Primary_Country__r.name,Secondary_State__r.name,Secondary_Country__r.name,Secondary_County__r.name,Unit_of_Measure_Text__c,Unit_of_Measure__c,Unit_of_Measure_CBI__c from Location__c where id in: lstlocids])
                               {

                                 if(loc1.Unit_of_Measure_CBI__c == true){
                                    loc1.Unit_of_Measure_Text__c = '['+ loc1.Unit_of_Measure__c +']';
                                 }
                                 else{
                                     loc1.Unit_of_Measure_Text__c =loc1.Unit_of_Measure__c;
                                }
                                if(loc1.Material_Type_CBI__c == true){
                                    loc1.Material_Type_Text__c = '['+ loc1.Material_Type__c +']';
                                 }
                                else{
                                     loc1.Material_Type_Text__c =loc1.Material_Type__c;
                                }
 
                                if(loc1.Primary_Country_CBI__c == true){
                                loc1.Primary_Country_Text__c = '['+loc1.Primary_Country__r.name+']';
                               }else
                                {
                                     loc1.Primary_Country_Text__c = loc1.Primary_Country__r.name;
                                }
                                
                                if(loc1.Primary_State_CBI__c == true){
                                loc1.Primary_State_Text__c = '['+loc1.Primary_State__r.name+']';
                               }else
                                {
                                     loc1.Primary_State_Text__c = loc1.Primary_State__r.name;
                                }
                                if(loc1.Secondary_Country_CBI__c == true){
                                loc1.Secondary_Country_Text__c = '['+loc1.Secondary_Country__r.name+']';
                               }else
                                {
                                     loc1.Secondary_Country_Text__c = loc1.Secondary_Country__r.name;
                                }
                                if(loc1.Secondary_County_CBI__c == true){
                                loc1.Secondary_Contact_County__c = '['+loc1.Secondary_County__r.name+']';
                               }else
                                {
                                     loc1.Secondary_Contact_County__c = loc1.Secondary_County__r.name;
                                }
                                if(loc1.Secondary_State_CBI__c == true){
                                loc1.Secondary_Contact_State__c = '['+loc1.Secondary_State__r.name+']';
                               }else
                                {
                                     loc1.Secondary_Contact_State__c = loc1.Secondary_State__r.name;
                                }
                               
                                listlocToUpdate.add(loc1);
                               
                               } 
                              
                             if(listlocToUpdate.size()>0){
                                update listlocToUpdate;}  */
                 }  
             // After Update 
                if(trigger.isupdate){
                    
                    //Anh Changes starts here
                    
                    //femi-commented: CARPOL_UNI_LineItemRelatedRecords.fetchLineItemFromLoc(trigger.new);
                    
                    //Anh Changes ends here
                      //-------------CBI Fields---------------// 
                      System.debug('***Inside After Update Trigger' + trigger.old);
                    /*femi-commented: 
                    for(Location__c l:trigger.old){
                            System.debug('*** update Trigger old:'+ Trigger.old);
                            BRSlineitemlst.add(l.Line_Item__c); 
                              lstlocids.add(l.id);
                       }
femi-commented: */
                    CARPOL_BRS_LocationsTriggerHandler.processLocationReadiness(trigger.new);
                         /*  system.debug('lstappids###'+lstappids);
                            for(Location__c loc2:[select id,Name,Country_CBI__c,Status__c,Line_Item__c,Country__c,Country_Text__c,State__c,State_CBI__c,Level_2_Region__c,County__c,County_CBI__c,Primary_Country_CBI__c,Primary_Country__c,Primary_Country_Text__c, Primary_State__c, 
                                                        Primary_State_CBI__c,Primary_State__r.name,Applicant_Instructions__c, Contact_state__c,Primary_County__c, Contact_County__c, Primary_County_CBI__c, Secondary_Contact_County__c, Secondary_County__c, Secondary_Contact_State__c, Secondary_State__c,Secondary_Country__c, 
                                                        Secondary_Country_CBI__c, Material_Type_CBI__c,Material_Type_Text__c,Material_Type__c,Secondary_Country_Text__c,Secondary_County_CBI__c, Secondary_State_CBI__c,Primary_County__r.name,Primary_Country__r.name,Secondary_State__r.name,Secondary_Country__r.name,Secondary_County__r.name,Unit_of_Measure_CBI__c,Unit_of_Measure_Text__c,Unit_of_Measure__c from Location__c where id in:lstlocids])
                              {
                                  if(loc2.Status__c == 'Review Complete' && loc2.Applicant_Instructions__c != null){
                                    loc2.Applicant_Instructions__c='';
                               }

                                if(loc2.Unit_of_Measure_CBI__c == true){
                                    loc2.Unit_of_Measure_Text__c = '['+ loc2.Unit_of_Measure__c +']';
                                 }
                                else{
                                     loc2.Unit_of_Measure_Text__c =loc2.Unit_of_Measure__c;
                                }
                                if(loc2.Material_Type_CBI__c == true){
                                    loc2.Material_Type_Text__c = '['+ loc2.Material_Type__c +']';
                                
                               }
                               else{
                                     loc2.Material_Type_Text__c =loc2.Material_Type__c;
                                }
                 
                               if(loc2.Primary_Country_CBI__c == true){
                                loc2.Primary_Country_Text__c = '['+loc2.Primary_Country__r.name+']';
                               }else
                                {
                                     loc2.Primary_Country_Text__c = loc2.Primary_Country__r.name;
                                }
                                
                                if(loc2.Primary_State_CBI__c == true){
                                loc2.Primary_State_Text__c = '['+loc2.Primary_State__r.name+']';
                               }else
                                {
                                     loc2.Primary_State_Text__c = loc2.Primary_State__r.name;
                                }
                                 if(loc2.Secondary_Country_CBI__c == true){
                                loc2.Secondary_Country_Text__c = '['+loc2.Secondary_Country__r.name+']';
                               }else
                                {
                                     loc2.Secondary_Country_Text__c = loc2.Secondary_Country__r.name;
                                }
                                 if(loc2.Secondary_County_CBI__c == true){
                                loc2.Secondary_Contact_County__c = '['+loc2.Secondary_County__r.name+']';
                               }else
                                {
                                     loc2.Secondary_Contact_County__c = loc2.Secondary_County__r.name;
                                }
                                 if(loc2.Secondary_State_CBI__c == true){
                                loc2.Secondary_Contact_State__c = '['+loc2.Secondary_State__r.name+']';
                               }else
                                {
                                     loc2.Secondary_Contact_State__c = loc2.Secondary_State__r.name;
                                }
                               
                                 
                               listlocToUpdate.add(loc2);
                                if(listlocToUpdate.size()>0)
                               {
                                update listlocToUpdate;}
                               } */
                
                            //   } 
                }
      // Commented - Kishore Start  
      // Note: This logic is redundancy, will be removed once tested in QA.         
      /*          if(trigger.isdelete){
                    
                      System.debug('***delete Trigger.old' +trigger.old);
                    //Anh Changes starts here
                    
                    CARPOL_UNI_LineItemRelatedRecords.fetchLineItemFromLoc(trigger.old);
                    
                    //Anh Changes ends here
                      for(Location__c l:trigger.old){
                            if(l.Line_Item__c!=null){
                                lstappids.add(l.Line_Item__c);
                            }
                       }

                    List<AC__c> LineItemList = [select id, (select id from Locations__r) from AC__c where id in : lstappids];
                    
                    for (AC__c a : LineItemList) {
                        if(a.Locations__r.size() == 0){
                            a.Location_Status__c = 'Yet to Add';
                            listlineitemToUpdate.add(a); 
                            }
                          
                        }
                   
                  } 
             
                if(listlineitemToUpdate.size()>0){
                    //update listlineitemToUpdate;
                   } */
      // Commented - Kishore End
               
              } 
         GenericHistoryClass.LogChangeHistory();
     }     
     if(trigger.isafter && trigger.isdelete){
          CARPOL_BRS_LocationsTriggerHandler.processLocationReadiness(trigger.old);
     }     

    }  // End of Custom setting If statement        
}