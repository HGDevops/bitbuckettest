trigger EFLRelatedRecordTrigger on EFL_Related_Record__c (before insert, before update,after insert, after update, after delete) {

    //CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('EFL_Related_RecordTrigger');
    //if(!dt.disable__c){ 
        if (Trigger.isInsert) {
            if (Trigger.isBefore) {
                EFLRelatedRecordTriggerHandler.validateUniqueEFLRelatedRecord(Trigger.new);
            }
        }
        if(Trigger.isDelete){
            if(Trigger.isAfter){
                EFLRelatedRecordTriggerHandler.validateUniqueEFLRelatedRecord(Trigger.Old);
            }
        }
  //  } 
}