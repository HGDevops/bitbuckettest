trigger EFLIncidentTrigger on Incident__c (before insert, before update, before delete, 
                                           after insert, after update, after delete) {

    
     EFLTriggerFactory.createAndExecuteHandler(EFLIncidentTriggerHandler.class);
}