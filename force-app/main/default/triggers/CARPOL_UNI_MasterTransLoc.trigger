trigger CARPOL_UNI_MasterTransLoc on Transit_Locale__c (before insert, after insert, 
              before update, after update, 
              before delete, after delete ) 
{

  if (Trigger.IsAfter) {
    if (Trigger.isInsert) {
      CARPOL_UNI_MasterTransLoc_helper.insertRelatedDM(trigger.newmap.keyset());
      }
    }

  if (Trigger.IsBefore) {
    if(trigger.isUpdate){ 
         CARPOL_UNI_MasterTransLoc_helper.prevAddlstopsEdit(trigger.newmap.keyset());
        } 
    if (Trigger.isDelete) {
        system.debug('--is before delete--'+trigger.oldmap.keyset());
        CARPOL_UNI_MasterTransLoc_helper.deleteRLD(trigger.oldmap.keyset());
     }
    }

}