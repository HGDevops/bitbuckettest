trigger Construct_Application_Junction_Trigger on Construct_Application_Junction__c (
    after insert, after update, before insert, before update, before delete,after delete) {
        
        ConApp_Jun_TriggerHelper helper = new ConApp_Jun_TriggerHelper();
        if(Trigger.isInsert && Trigger.isAfter){
            //System.debug('*************Before helper class*****************');
            helper.updateSOP_And_Construct_Status_On_LineItem(Trigger.new);
            //System.debug('*************After helper class *****************');
        }
        if(Trigger.isDelete && Trigger.isBefore){
            //system.debug('@@@ inside Before DELETE TRIGGER **** ');   
            helper.updateSOP_And_Construct_Status_On_LineItem(Trigger.old);
        }
        // start W-035176 Validation logic to check PSC have related RA's when creating new aplications                                                                                        
        if(Trigger.isInsert && Trigger.isBefore){
            //  system.debug('@@@ inside Before INSERT TRIGGER **** ');   
            helper.validatePSCHasRA(Trigger.new);
        }
        //--------------********************* AFTER ***********************-------------------
        
        // Added by Venkata Bhavanam on 07-30-17 to clear  applicant instructions field values when status is review complete
        
        if(trigger.isafter){
            if(checkRecursive.runOnce()){ 
                
                //*********** Variable Declaration ***************//
                
                list<string> lstconstructids = new list<string>();
                list<ID> lineitemlistid = new list<ID>();
                list<Construct_Application_Junction__c> listconstructToUpdate = new list<Construct_Application_Junction__c>();
                
                if(trigger.isupdate){
                    for(Construct_Application_Junction__c c:trigger.old){
                        
                        lineitemlistid.add(c.Line_Item__c); 
                        lstconstructids.add(c.id);
                    }
                    for(Construct_Application_Junction__c construct1 :
                        [select id,Status__c, Applicant_Instructions__c FROM Construct_Application_Junction__c where id in:lstconstructids]){
                            if(construct1.Status__c == 'Review Complete' && construct1.Applicant_Instructions__c != null)
                            {
                                construct1.Applicant_Instructions__c='';
                            }
                            listconstructToUpdate.add(construct1);
                        }
                    if(listconstructToUpdate.size()>0)
                    {
                        update listconstructToUpdate;
                    }
                }                             
            }
            GenericHistoryClass.LogChangeHistory();           
        }      
    }