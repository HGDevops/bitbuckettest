trigger CARPOL_BRS_ConstructTrigger on Construct__c (before update,before insert,before delete,after update ,after insert, after delete) {
    /*  ====================================================  */
    /*  Name: CARPOL_BRS_ConstructTrigger                     */                
    /*  Copyright notice:                                     */                       
    /*  ====================================================  */ 
    /*  ====================================================  */
    /*  Purpose: BRS Specific Activities                      */  
    /*           Status Update, Locking Functionality         */  
    /*           Change History                               */
    /*  ====================================================  */ 
    /*  ====================================================  */
    /*  History:                                              */    
    /*  ----------------------------------------------------  */
    /*  VERSION AUTHOR  DATE    DETAIL  RELEASE/CSR           */
    /*  ====================================================  */
    
    CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_ConstructTrigger');
    if(!dt.disable__c){ 
        //-----------------------------Update------------------------------------//
        //TODO: Trigger is not bulkified 
        //TODO: Adopt Trigger pattern
        //
        if (Trigger.isBefore) {
            if (Trigger.isinsert){
                EFLConstructTriggerHandler.ValidateRequiredFields(Trigger.new);
                EFLConstructTriggerHandler.ValidateConstructs(Trigger.new);
                EFLConstructTriggerHandler.setConstructSharing(Trigger.new); //only needs to be called on insert since this is set for XML applications
            } 
            if (Trigger.isupdate) {
                EFLConstructTriggerHandler.ValidateRequiredFields(Trigger.new);
                EFLConstructTriggerHandler.ValidateConstructs(Trigger.old, Trigger.new);
                         
            }
            if (Trigger.isdelete) {
                Set<ID> constructIds = (new Map<Id,Construct__c>((List<Construct__c >)Trigger.Old)).keySet(); 
                if(constructIds != null){
                    List<Construct_Application_Junction__c> pscList = [Select id from Construct_Application_Junction__c 
                                                                       where construct__c in :constructIds];
                	if(pscList != null)
                    	delete pscList;
                }
            }
        } 
        //----------------------------------------------------AFTER-------------------------------------------------------------------------//  
        if(trigger.isafter){
            
            //--------------------------------AFTER INSERT------------------------------------------------------------------// 
            if (Trigger.isinsert) {
                   EFLConstructTriggerHandler.processConstructReadiness(trigger.new);
            }             
            
            //--------------------------------AFTER UPDATE------------------------------------------------------------------//   
            if (Trigger.isupdate) {
               EFLConstructTriggerHandler.processConstructReadiness(trigger.new);     
            }
            //--------------------------------AFTER DELETE------------------------------------------------------------------//
            //Updated to check Pre Rev Construct - Requirement : W-010921 - By : Jagadish Konduru
            if(trigger.isdelete){
                EFLConstructTriggerHandler.resetConstructReadiness(Trigger.Old);
               
            }
            
            //--------------------------------UPDATE Line Item------------------------------------------------------------------//   
          
            GenericHistoryClass.LogChangeHistory();
        }
        
    }    
}