trigger CARPOL_AccountTrigger on Account (Before Update, After Update, Before Insert) {
    
    if(Trigger.isinsert) {
        if(Trigger.isbefore){
           // CARPOL_AccountTriggerHandler.setFAAccessCode(Trigger.new);
        }
    }
    
    if(Trigger.isUpdate) {
        if(Trigger.isAfter){
            CARPOL_AccountTriggerHandler.afterUpdateHandler(Trigger.new);
        }
        else {
            //CARPOL_AccountTriggerHandler.setFAAccessCode(Trigger.new);
        }
    }
    
}