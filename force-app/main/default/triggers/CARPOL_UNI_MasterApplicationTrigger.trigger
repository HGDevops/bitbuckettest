trigger CARPOL_UNI_MasterApplicationTrigger on Application__c (before insert, after insert, before update, after update, before delete, after delete) 
{
    CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_UNI_MasterApplicationTrigger');
    if(!dt.disable__c) {
        if (Trigger.isBefore) //====================================================BEFORE=================================================
        {
            if (Trigger.isInsert) //----------------------------------------before insert------------------------------------
            {
                for(Application__c app: trigger.new)
                {  
                    if (app.Source__c == 'XML'){
                        //this is not bulkified but should be acceptable for now as only a single app at a time would be inserted via XML
                        app = EFLApplicationEngineUtility.prepareGenericApplication(app, app.Applicant_Account__c);
                    }
                    
                }
            }
            //KA:W-026020: Delete the related records associated with application before application get deleted
            if (Trigger.isDelete){
                set<ID> appids = new set<ID>();
                for(Application__c appl: trigger.old)
                {                
                    appids.add(appl.Id);
                }
                if (!appids.isempty()){  
                    
                    CARPOL_UNI_ApplicationHelperClass.deleteAllAssociateRecord(appids);
                }
                
            } 
        }
        
        if (Trigger.IsAfter) //====================================================AFTER=================================================
        {
            if (Trigger.isInsert){
                
                // Nitish Jakkani: The following piece creates sharing records for the application when the applicant is a Partner user 
                List<Application__c>Applications = trigger.new;
                String AccName,AccProfileName;
                ID ApplicantContactID;
                
                for(Application__c app:trigger.new)
                {
                    if(app.Applicant_Name__c!=null )
                    {
                        ApplicantContactID = app.Applicant_Name__c;
                        AccName = app.Organization__c;
                        AccProfileName = app.Profile_Name__c;
                    }
                    
                }
                if((AccName!=null) && (ApplicantContactID!=null) && (!Test.isRunningTest())  && AccProfileName == 'efile Applicant' )
                {
                    User ApplicantUser = new User();
                    ApplicantUser = [SELECT Phone, Id, ContactID, UserType FROM User WHERE ContactID = :ApplicantContactID LIMIT 1];
                    
                    if(ApplicantUser != Null && ApplicantUser.UserType=='PowerPartner')
                    {
                        CARPOL_Application_ManagedSharing.afterApplicationInsert(ApplicantUser.ID, Applications);
                    }
                }
                
            }
            if (Trigger.isUpdate) 
            {
                if(checkRecursive.runOnce())
                {   
                    //W-037859: status changed from WOC (most likely to "submitted") 
                    Set<Id> authIdsForWocBRS = new Set<Id>();
                    Set<Id> authIdsForWoc = new Set<Id>();
                    Set<Id> appIdsForWoc = new Set<Id>();
                    for (Application__c a : Trigger.New){
                        if (EFLGenericUtility.isFieldValueChanged(Trigger.OldMap, a, 'Application_Status__c') && Trigger.OldMap.get(a.Id).Application_Status__c == 'Waiting on Customer'){
                            appIdsForWoc.add(a.Id);
                        }                    
                    }
                    
                    List<AC__c> LineItemsList = [SELECT ID, Name, Status__c, Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.name, 
                                                 Program_Line_Item_Pathway__r.Program__c, Program_Line_Item_Pathway__r.Program__r.Name,Authorization__c 
                                                 FROM AC__c WHERE (Application_Number__c in :appIdsForWoc
                                                                   AND Program_Line_Item_Pathway__r.Program__r.Name in ('BRS','AC'))];
                    if (LineItemsList.size()>0){ 
                        for (Ac__c li : LineItemsList){
                            li.Status__c = 'Submitted';  
                            if (li.Program_Line_Item_Pathway__r.Program__r.Name == 'BRS'){
                                authIdsForWocBRS.add(li.Authorization__c);
                            } 
                            authIdsForWoc.add(li.Authorization__c);
                        }
                        update LineItemsList; 
                        
                        if (!authIdsForWocBRS.isEmpty()){
                            List<Workflow_Task__c> wtList = new List<Workflow_Task__c>();
                            wtList = [SELECT id,Status__c,Status_Categories__c FROM Workflow_Task__c WHERE Status__c='Pending' AND Status_Categories__c='Customer Feedback' 
                                      AND Authorization__c in :authIdsForWocBRS limit 1];
                            for (Workflow_Task__c wt : wtList){
                                wt.Status__c='In Progress';
                                wt.Status_Categories__c='';    
                            }
                            update wtList;
                            
                        }
                        if (!authIdsForWoc.isEmpty()){
                            List<Authorizations__c> authList = [SELECT status__c FROM Authorizations__c WHERE id in :authIdsForWoc limit 1];
                            for (Authorizations__c auth : authList) {
                                auth.Status__c = 'Submitted';                                
                            } 
                            update authList;                            
                        }
                        
                    }
                    
                    
                    set<ID> appIdsToWithdraw = new set<ID>();
                    
                    for(Application__c appl: trigger.new)
                    {  
                        if (appl.Application_Status__c == 'Withdrawn'){
                            appIdsToWithdraw.add(appl.id);  
                        }
                        
                    }
                    if (!appIdsToWithdraw.isempty()){                         
                        EFLApplicationEngineUtility.WithdrawStatusUpdate(appIdsToWithdraw );
                    } 
                    
                    Map < Authorizations__c, List < AC__c >> AuthLineListMap = new Map < Authorizations__c, List < AC__c >>();
                    
                    // 1-28--17 VV added to prevent auth generation when saving flag 
                    //W-037852 (FA): "EFLSaveastemplate__c " defaults to false, unless explicitly set, so this block always runs -
                    //and generates new auths. Adding conditions to only execute if the application is currently being submitted
                    if(trigger.new[0].EFLSaveastemplate__c==false 
                       && EFLGenericUtility.isFieldValueChanged(Trigger.OldMap, trigger.new[0], 'Application_Status__c')
                       && trigger.new[0].Application_Status__c == 'Submitted'){                     
                           AuthLineListMap = CARPOL_UNI_SubmitApplication.onAfterUpdate(trigger.new);                              
                       }
                    
                    Map < ID, List < AC__c >> AuthIdLineListmap = new Map < ID, List < AC__c >> ();
                    Map < Authorizations__c, List < AC__c >> passingMap = new Map < Authorizations__c, List < AC__c >> ();
                    Map < ID, Authorizations__c > packageParentAuthmap = new Map < ID, Authorizations__c > ();
                    List < Authorizations__c > Ingredientauthlist = new List < Authorizations__c > ();
                    
                    
                    
                    if (AuthLineListMap != null) {
                        Map < AC__c, Id > LineAuthIdMap = new Map < AC__c, Id > ();
                        List < AC__c > lineitemlist = new List < AC__c > ();
                        List < AC__c > updlineitemlist = new List < AC__c > (); 
                        Map < ID, AC__c > lineIdlinemap = new Map < ID, AC__c > ();
                        Map < ID, Authorizations__c > authidauth = new Map < ID, Authorizations__c > ();
                        
                        for (Authorizations__c auth: AuthLineListMap.keyset()) {
                            AuthIdLineListmap.put(auth.id, AuthLineListMap.get(auth));
                            authidauth.put(auth.id, auth);
                        }                    
                        
                        
                        for (List < AC__c > line: AuthLineListMap.values()) {
                            lineitemlist.addall(line);
                        }
                        
                        for (AC__c line: lineitemlist) {
                            LineAuthIdMap.put(line, line.Authorization__c);
                            lineIdlinemap.put(line.id, line);
                            
                            if (line.Program_Line_Item_Pathway__r.name == 'New Facility') {
                                if (line.Parent_Facility_for_New_Facility__c != null) {
                                    ID parentline = line.Parent_Facility_for_New_Facility__c;
                                    AC__c parent = lineIdlinemap.get(line.Parent_Facility_for_New_Facility__c);
                                    Authorizations__c auth = authidauth.get(line.Authorization__c);
                                    Authorizations__c parentauth = authidauth.get(parent.Authorization__c);
                                    auth.Parent_Authorization__c = parentauth.id;
                                    passingMap.put(auth, AuthIdLineListmap.get(auth.id));
                                    
                                }
                                
                            }
                            
                        }
                        
                        
                        string packageAuth = '';
                        for (Authorizations__c auth: AuthLineListMap.keyset()) {
                            if (((auth.Auth_Type_Hidden_Flag__c == 'Product') || (auth.Auth_Type_Hidden_Flag__c == 'Ingredient')) && (auth.Regulated_Article_PackageId__c != null || auth.Regulated_Article_PackageId__c != '')) {
                                if (auth.Auth_Type_Hidden_Flag__c == 'Product') {
                                    packageParentAuthmap.put(auth.Regulated_Article_PackageId__c, auth);
                                    
                                }
                                if (auth.Auth_Type_Hidden_Flag__c == 'Ingredient') {
                                    Ingredientauthlist.add(auth);
                                }
                            }
                            
                        }
                        
                        
                        
                        if (Ingredientauthlist != null) {
                            for (Authorizations__c auth: Ingredientauthlist) {
                                if(packageParentAuthmap.get(auth.Regulated_Article_PackageId__c) != null){
                                    auth.Parent_Authorization__c = packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).id;
                                    Integer i = 1;
                                    if (packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c == null) {
                                        packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c = string.valueOf(i);
                                    } else {
                                        Integer j;
                                        j = integer.valueOf(packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c);
                                        j++;
                                        packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c = string.valueOf(j);
                                    }
                                    passingMap.put(auth, AuthIdLineListmap.get(auth.id));
                                    
                                }
                            }
                        }
                        
                        
                        for (Authorizations__c auth: AuthLineListMap.keyset()) {
                            if (!passingMap.containskey(auth)) {
                                passingMap.put(auth, AuthIdLineListmap.get(auth.id));
                            }
                        }
                        
                    }
                    
                    
                    CARPOL_UNI_Auth_Junction_WF createAuthjunction = new CARPOL_UNI_Auth_Junction_WF();
                    createAuthjunction.createAuthorizationJunctionRecord(passingMap); //***************************** creates auth junctions, wf tasks and update authorizations
                    
                    EFLTriggerFactory.createAndExecuteHandler(EFLApplicationTriggerHandler.class); 
                }
                
            }
            
            GenericHistoryClass.LogChangeHistory();
        }
    }    
}