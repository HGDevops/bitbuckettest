trigger CARPOL_BRS_PhenotypeTrigger on Phenotype__c (before update,before insert,
                                                     after insert, after update, after delete) {
                               
        if (Trigger.isBefore) {
            if (Trigger.isinsert){
                EFLPhenotypeTriggerHandler.ValidateRequiredFields(Trigger.New); 
            } 
            if (Trigger.isupdate) {
                    EFLPhenotypeTriggerHandler.ValidateRequiredFields(Trigger.New); 
            }
          }
                                                         
         if (Trigger.isAfter) {
                if (Trigger.isInsert){
                    EFLPhenotypeTriggerHandler.processConstructReadiness(Trigger.new); 
                }
                if (Trigger.isUpdate){
                    EFLPhenotypeTriggerHandler.processConstructReadiness(Trigger.new); 
                }        
                if (Trigger.isDelete){
                    EFLPhenotypeTriggerHandler.resetConstructReadiness(Trigger.Old); 
                }        
            }                                                          
           GenericHistoryClass.LogChangeHistory();

}