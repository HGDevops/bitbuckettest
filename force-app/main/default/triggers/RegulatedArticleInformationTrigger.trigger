trigger RegulatedArticleInformationTrigger on RegulatedArticle_Information__c(before insert, before update, after insert, after update, after delete, before delete) {
/*
 * Author: keerthi Jakkaraju Date:03/08/2019
 * Purpose: Trigger for RegulatedArticle Information object.
*/
    if (Trigger.IsBefore && Trigger.IsInsert){
       // EFLregarticleinformationTriggerHandler.validateFieldFormatAndRequired(Trigger.New);
    }
    if (Trigger.IsBefore && Trigger.IsUpdate){
       // EFLregarticleinformationTriggerHandler.validateFieldFormatAndRequired(Trigger.New);
    }
    
    if(Trigger.isAfter)
    {
    	if(Trigger.isInsert || Trigger.isUpdate)
    	{
            EFLregarticleinformationTriggerHandler.ACRegulatedArticleInformationProcessReadiness(Trigger.New);
        }
        if(Trigger.isDelete)
    	{
            EFLregarticleinformationTriggerHandler.ACRegulatedArticleInformationProcessReadiness(Trigger.Old);
           
        }
    }
    
    //SpringCM
    if(Trigger.isBefore && Trigger.isDelete)
    {
        List<RegulatedArticle_Information__c> dogsForSpringCMFolderDeletion = new List<RegulatedArticle_Information__c>();
        for(RegulatedArticle_Information__c rai: Trigger.Old)
        {
            dogsForSpringCMFolderDeletion.add(rai);
        }
        SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), dogsForSpringCMFolderDeletion.get(0).getSObjectType().getDescribe().getName(), dogsForSpringCMFolderDeletion, 'Live Dogs - Dog Folder Deletion');
    }
    
  }