trigger stateReviewPackageRollup on Reviewer__c (after delete, after insert, after update) {
    system.debug('starting trigger stateReviewPackageRollup**********************');    
    List<Reviewer__c> listSRPermitNoCBI = new List<Reviewer__c>();
    List<Reviewer__c> listSRPermitCBIDeleted = new List<Reviewer__c>();
    List<Reviewer__c> listSRNotificationNoCBI = new List<Reviewer__c>();
    List<Reviewer__c> listSRNotificationCBIDeleted = new List<Reviewer__c>();
    final String workflowSRPermitNoCBI = 'State Review Permit No CBI';
    final String workflowSRPermitCBIDeleted = 'State Review Permit CBI Deleted';
    final String workflowSRNotificationNoCBI = 'State Review Notification No CBI';
    final String workflowSRNotificationCBIDeleted = 'State Review Notification CBI Deleted';
    
    Set<id> reviewIds = new Set<id>();
    List<Authorizations__c> officialReviewToUpdate = new List<Authorizations__c>();
    
    if (Trigger.isUpdate || Trigger.isDelete) {
        for (Reviewer__c item : Trigger.old)
            reviewIds.add(item.Authorization__c);
    }
    if (Trigger.isInsert){
        for (Reviewer__c item : Trigger.new)
            reviewIds.add(item.Authorization__c);
        if(Trigger.isAfter){
            System.debug('Entering after Insert portion*******************');
                for(Reviewer__c sr:[SELECT id, Authorization__r.Application_CBI__c, Authorization__r.Authorization_Type__c FROM Reviewer__c WHERE Id IN :Trigger.New]){
                    
                    system.debug('sr.Authorization__r.Application_CBI__c:'+sr.Authorization__r.Application_CBI__c);
                    system.debug('sr.Authorization__r.Authorization_Type__c: '+sr.Authorization__r.Authorization_Type__c);
                    if(sr.Authorization__r.Authorization_Type__c == 'Notification'){
                        if(sr.Authorization__r.Application_CBI__c == 'Yes'){ 
                            listSRNotificationCBIDeleted.add(sr);
                        }
                        else
                        {
                            listSRNotificationNoCBI.add(sr); 
                        }
                    }
                    else
                            if(sr.Authorization__r.Application_CBI__c == 'Yes'){ 
                                listSRPermitCBIDeleted.add(sr);
                            }
                            else
                            {
                                listSRPermitNoCBI.add(sr); 
                            }
                }
        }
    }
    
    Map <id,Authorizations__c> reviewMap = new Map <id,Authorizations__c>([select id from Authorizations__c where id IN :reviewIds]);
    for (Authorizations__c auth: [select Id, Name, (select id,status__c from Official_Review_Records__r) from Authorizations__c where Id IN :reviewIds]) {
        reviewMap.get(auth.Id).Total_State_Review_Packages__c = auth.Official_Review_Records__r.size();
        // add the total in the map to a list so we can update it
        officialReviewToUpdate.add(reviewMap.get(auth.Id));
    }
    
    for (Authorizations__c auth: [select Id, Name, (select id,status__c from Official_Review_Records__r WHERE status__c = 'Open') from Authorizations__c where Id IN :reviewIds]) {
        reviewMap.get(auth.Id).Remaining_State_Packages_Needing_Review__c = auth.Official_Review_Records__r.size();
        // add the total in the map to a list so we can update it
        if(!officialReviewToUpdate.contains(reviewMap.get(auth.Id))){
            officialReviewToUpdate.add(reviewMap.get(auth.Id));
        }
    }
    
    update officialReviewToUpdate;
    //Run State review workflows
    if(listSRPermitNoCBI.Size() > 0){
        system.debug('Run***********workflowSRPermitNoCBI');
        SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), listSRPermitNoCBI.get(0).getSObjectType().getDescribe().getName(), listSRPermitNoCBI, workflowSRPermitNoCBI);
    }
    if(listSRPermitCBIDeleted.Size() > 0){  
        system.debug('Run***********workflowSRPermitCBIDeleted');
        SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), listSRPermitCBIDeleted.get(0).getSObjectType().getDescribe().getName(), listSRPermitCBIDeleted, workflowSRPermitCBIDeleted);
    }
    if(listSRNotificationNoCBI.Size() > 0){ 
        system.debug('Run***********workflowSRNotificationNoCBI');
        SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), listSRNotificationNoCBI.get(0).getSObjectType().getDescribe().getName(), listSRNotificationNoCBI, workflowSRNotificationNoCBI);
    }
    if(listSRNotificationCBIDeleted.Size() > 0){  
            system.debug('Run***********workflowSRNotificationCBIDeleted');
        SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), listSRNotificationCBIDeleted.get(0).getSObjectType().getDescribe().getName(), listSRNotificationCBIDeleted, workflowSRNotificationCBIDeleted);
    }
    
    //Call the trigger handler to send out email if necessary
   //KK - Once 'MASS_EMAIL" permission issue is resolved, we will uncomment this part. 
  /*   if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){ 
        OfficialReviewRecordTriggerHandler.sendCompletedReviewsEmail(Trigger.New);
    }*/
}