trigger CARPOL_BRS_GenotypeTrigger on Genotype__c (before update,before insert,after insert, after update, after delete) {

CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_GenotypeTrigger');
 if(!dt.disable__c){
        //GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Genotype__c');
         
          //W-027020 - Validations
          if (Trigger.isBefore) {
                if (Trigger.isinsert){
                     EFLGenotypeTriggerHandler.ValidateRequiredFields(Trigger.New); 
                }  
                if (Trigger.isupdate) {
                     EFLGenotypeTriggerHandler.ValidateRequiredFields(Trigger.New); 
                }
            }
            
            //W-027020 - Sort Order
            if (Trigger.isBefore) {
                if (Trigger.isinsert){
                         EFLGenotypeTriggerHandler.SortOrderEvaluationOnInsert(Trigger.New); 
                }
                
                /*//Re-Ordering on Update
                if (Trigger.isUpdate){
                         EFLGenotypeTriggerHandler.SortOrderEvaluationOnUpdate(Trigger.New, Trigger.old); 
                }*/
            }
            
            //W-027020 - Delete
            if (Trigger.isAfter) {
                
               if (Trigger.isInsert){
                    //Process Genotype Readiness Check for Submission
                    EFLGenotypeTriggerHandler.processGenotypeReadiness(Trigger.new); 
                    //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
                    EFLGenotypeTriggerHandler.updateConstructCBIflag(Trigger.new);
                    //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
                }
                //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
                if (Trigger.isUpdate){
                    
                    EFLGenotypeTriggerHandler.updateConstructCBIflag(Trigger.new);
                }
                //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
                if (Trigger.isDelete){
                    //Reset Genotype Submission flag
                    EFLGenotypeTriggerHandler.resetConstructReadiness(Trigger.Old); 
                    EFLGenotypeTriggerHandler.DeleteParentAndChildOrder(Trigger.old); 
                }                        
            } 
  
        GenericHistoryClass.LogChangeHistory();
    }
}