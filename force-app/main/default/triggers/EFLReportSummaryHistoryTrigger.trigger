trigger EFLReportSummaryHistoryTrigger on Report_Summary_History__c (before insert, before update, before delete, 
                                                                     after insert, after update, after delete ) {
    
    
    EFLTriggerFactory.createAndExecuteHandler(EFLReportSummaryHistoryTriggerHandler.class); 
  
}