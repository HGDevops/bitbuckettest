trigger EQS_CheckIncidentStatus on Case (before update) 
{
   if(Trigger.isBefore)
    {
       
       EQS_CheckIncidentStatusHandler.beforeCloseRFRCheck(Trigger.New);
    }
}