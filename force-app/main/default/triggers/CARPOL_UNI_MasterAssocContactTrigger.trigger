trigger CARPOL_UNI_MasterAssocContactTrigger on Applicant_Contact__c (before Insert, before Update, after Insert, after Update) {
     if (Trigger.isBefore) {
     
     List<Id> UserIDs;
     List<User> UserList; 
     List<ID> ContactIDs;
     List<Contact> ContactList;
     List<ID>AccountIDs;
     Map<ID,ID>UserContactIDMap = new Map<ID,ID>();
     Map<ID,Applicant_Contact__c> AssocConUserIDMap = new Map<ID,Applicant_Contact__c>();
     
     for(Applicant_Contact__c ac: trigger.new){
            if(ac.Account__c==null)
            {
                system.debug('ac is>>'+ac.OwnerID);
                
                   AssocConUserIDMap.put(ac.OwnerID,ac);
                   //UserIDs.add(ac.OwnerID);
                
            }else{
            continue;
            }
        }
        
        if(AssocConUserIDMap.size()!=0)
        {
               UserList =[SELECT ID,Name, ContactID FROM User WHERE ID IN : AssocConUserIDMap.keyset()];// new List<User>();
               if(UserList.size()!=0)
                   {
                        for(User u:UserList)
                          {
                            //ContactIDs.add(u.ContactID);
                            UserContactIDMap.put(u.ContactID,u.ID);
                          }   
                    }
               Map<ID,ID> UserAccountIDMap = new Map<ID,ID>();
               if(UserContactIDMap.size()!=0)
                   {
                       ContactList = [SELECT ID, AccountId FROM Contact WHERE ID IN : UserContactIDMap.keyset()];
                       if(ContactList.size()!=0)
                           {
                              for(Contact con:ContactList)
                                {
                                    if(UserContactIDMap.containskey(con.ID))
                                        {
                                            UserAccountIDMap.put(UserContactIDMap.get(con.ID),con.AccountID);
                                        }
                                }
                           }
                   }
                
                
                if(UserAccountIDMap.size()!=0)
                    {
                       for(Applicant_Contact__c ac: trigger.new)
                           {
                               if(UserAccountIDMap.containskey(ac.OwnerID)){
                                   ac.Account__c = UserAccountIDMap.get(ac.OwnerID);
                               }
                           }
                    }
          }
          
       String USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;    
        
        for (Applicant_Contact__c  ac : trigger.new){
            if (ac.Mailing_Country_LR__c== usCountryId &&  ac.Mailing_Zip_Postal_Code__c!= null ){
                Pattern zipcodePattern = pattern.compile('[0-9]{5}(?:-[0-9]{4})?$');
                Matcher zipcodeMatcher = zipcodePattern.matcher(ac.Mailing_Zip_Postal_Code__c);
                if (!zipcodeMatcher.matches()){
                    ac.Mailing_Zip_Postal_Code__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorFailZip'));
                }                
            }
        }
    
       
          
          
        /* for(Applicant_Contact__c ac: trigger.new){
               List<User> UserList = new List<User>();
               
            if(ac.Account__c==null){
                if(ac.OwnerID!=null || ac.OwnerID!=''){
                    User u = [SELECT ID,Name, ContactID FROM User WHERE ID=:ac.OwnerID];
                    
                    if(u.ContactID!=null){
                        Contact con = [SELECT ID,Name, AccountId FROM Contact WHERE ID=:u.ContactID];
                        
                        ac.Account__c = con.AccountID;
                    }
                }
            }else{
            continue;
            }
        }*/
        
        
     }
     
     
     
     if (Trigger.isAfter) {
        system.debug('-------------Trigger After');
        map<string,string> countryMap = new map<String,String>();
        for(Country__c con: [Select Name,Id from Country__C where name ='United States of America']){
            countryMap.put(con.ID,con.ID);
        }
        
        for(Applicant_Contact__c ac: trigger.new){
        
           if(ac.Mailing_Country_LR__c == countryMap.get(ac.Mailing_Country_LR__c) && (!ac.Address_Verified__c) && ac.Gen_Text__c==null){
            system.debug('-------------Trigger Started');
            CARPOL_UNI_USPS_integration_class.validate(ac.Mailing_Street__c,ac.Mailing_City__c,ac.Mailing_Zip_Postal_Code__c,ac.Mailing_State_LR__c,ac.id);
        
            }
        }
      }
}