trigger CARPOL_UNI_MasterTempQJunctionTrigger on EFL_INS_Template_Question_Junction__c (before Insert, before update) {
    if (Trigger.isBefore) //====================================================BEFORE=================================================
    {
        if(Trigger.isInsert){
            List<ID> sourceQuestionIDs = new List<ID>();
            for(EFL_INS_Template_Question_Junction__c Junc:trigger.new){
                if(string.isNotBlank(Junc.Inspection_Template_Questions__c)){
            		sourceQuestionIDs.add(Junc.Inspection_Template_Questions__c);
                }
            }
            
            map<Id, EFL_Inspection_Questions__c> qMap = new map<Id, EFL_Inspection_Questions__c>(
            [SELECT ID,Name,Question__c FROM EFL_Inspection_Questions__c WHERE ID IN :sourceQuestionIDs]);
            //Map<>
            for(EFL_INS_Template_Question_Junction__c Junc:trigger.new){
                if(qMap.containsKey(Junc.Inspection_Template_Questions__c)){
                    Junc.Question__c = qMap.get(Junc.Inspection_Template_Questions__c).Question__c;
                }
            }
            
            
        
            /*for(sourcelist){
                if().containskey()
            }*/
        }
        if(Trigger.isUpdate){
            
        }
   }
}