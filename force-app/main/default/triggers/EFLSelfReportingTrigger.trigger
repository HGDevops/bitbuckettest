//Developer Name: Peter Tran
//Creation Date: 03/18/2019
//File Name: EFLSelfReportingTrigger.apxt
//sObject Affected: Self_Reporting__c
//Client: United States Department of Agriculture
//Client Project Name: APHIS CARPOL
//Contractor: Accenture Federal Services

    /*****************************
     *  Developer Documentation  *
     *****************************
     * _________________________
     * Trigger Description
     * 
     * Changelog
     * V1.0: Creation of EFLSelfReportingTrigger.apxt
     * - File created in DevShare on 03/18/2019
     *  
     */

//Begin trigger declaration
trigger EFLSelfReportingTrigger on Self_Reporting__c (before insert,before update, after insert, after update, after delete) {
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            
            //This is common on beforeInsert Method where validations and other required logics can be performed by recordType
            EFLSelfReportingTriggerHandler.onBeforeInsert(Trigger.new);
            
            //Perform data validation checks for creating Self_Reporting__c records that have the same location and start date in existing Self_Reporting__c records
            EFLSelfReportingTriggerHandler.checkForExistingRecordLocationAndStartDate(Trigger.new);
            
            //Perform validation checks for creating Self_Reporting__c records with unique planting Id
            EFLSelfReportingTriggerHandler.checkUniquePlantingId(Trigger.new);
            
        } else if (Trigger.isAfter) {
            // Process after insert
            EFLSelfReportingTriggerHandler.UpdateNoPlantingFlag(Trigger.new, null);
        }        
    }
    else if (Trigger.isDelete) {
        // Process after delete
        if(trigger.isafter){
        }
    }
    if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            // Process before update
            
            //This is common on beforeInsert Method where validations and other required logics can be performed by recordType
            EFLSelfReportingTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.old);

            //Perform validation checks for creating Self_Reporting__c records with unique planting Id
            //EFLSelfReportingTriggerHandler.checkUniquePlantingId(Trigger.new);
         }  
        else if (Trigger.isAfter) {
            // Process after update
            EFLSelfReportingTriggerHandler.UpdateNoPlantingFlag(Trigger.new, Trigger.OldMap);
        }  
    }   
}//End trigger declaration