trigger CARPOL_BRS_GenotypeTypeTrigger on GenotypeType__c (before insert,before update,
                                                            after insert, after update, after delete) {
 CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_GenotypeTrigger');
 if(!dt.disable__c){
      
          if (Trigger.isBefore) {
                if (Trigger.isinsert){
                     EFLGenotypeTypeTriggerHandler.ValidateRequiredFields(Trigger.New); 
                }  
                if (Trigger.isupdate) {
                     EFLGenotypeTypeTriggerHandler.ValidateRequiredFields(Trigger.New); 
                }
            }
    
         if (Trigger.isAfter) {
                if (Trigger.isInsert){
                    EFLGenotypeTypeTriggerHandler.processConstructReadiness(Trigger.new); 
                }
                if (Trigger.isUpdate){
                    EFLGenotypeTypeTriggerHandler.processConstructReadiness(Trigger.new); 
                }        
                if (Trigger.isDelete){
                    EFLGenotypeTypeTriggerHandler.resetConstructReadiness(Trigger.Old); 
                }        
                
            }     
  
       // GenericHistoryClass.LogChangeHistory();
 }
}