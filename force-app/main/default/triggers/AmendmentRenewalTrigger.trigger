trigger AmendmentRenewalTrigger on Amendment_Renewal__c (before insert, after insert, before update, after update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            
        }
        if(trigger.isUpdate){
            
        }  
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
            List<Amendment_Renewal__c> amendRenewList = new List<Amendment_Renewal__c>();
            for(Amendment_Renewal__c ar:trigger.New){
                if(Schema.getGlobalDescribe().get(ar.Id.getSObjectType().getDescribe().getName()).getDescribe().getRecordTypeInfosById().get(ar.RecordTypeId).getName() == 'Amendment'){
                    amendRenewList.Add(ar); 
                }
            }
            final String Workflow = 'Clone Line Item Folder';
            if(amendRenewList.Size() > 0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), amendRenewList.get(0).getSObjectType().getDescribe().getName(), amendRenewList, workflow);
            }
        }
        if(trigger.isUpdate){
            
        }
    }
}