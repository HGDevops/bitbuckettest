trigger EFLUploadedFileTrigger on Uploaded_File__c (before insert, after insert, before update, after update) {

    if(trigger.isBefore){
        if(trigger.isInsert){
            EFLUploadedFileTriggerHandler.doBeforeInsert(trigger.new);
        }
    }else if (trigger.isAfter){
        if(trigger.isUpdate){
            EFLUploadedFileTriggerHandler.doAfterUpdate(trigger.new);
        }
    }
}