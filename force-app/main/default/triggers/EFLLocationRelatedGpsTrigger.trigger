trigger EFLLocationRelatedGpsTrigger on GPS_Coordinate__c (before insert, before update,after insert, after update, after delete) {
    list<location__c> locationList = new list<location__c>();
    set<id> locationIdSet = new set<id>();
    if (Trigger.IsBefore && Trigger.IsInsert){
        System.debug('Before Insert Trigger');
        EFLLocationRelatedGpsTriggerHandler.validateGpsRecordSize(Trigger.New);
    }
    if (Trigger.IsBefore && Trigger.IsUpdate){
        System.debug('Before Update Trigger');
    }
    if(Trigger.isafter){
        if( Trigger.Isinsert){
            for(GPS_Coordinate__c gps : Trigger.New){
               locationIdSet.add(gps.location__c); 
            } 
        }
        if( Trigger.Isdelete){
            for(GPS_Coordinate__c gps : trigger.old){
               locationIdSet.add(gps.location__c); 
            } 
            EFLLocationRelatedGpsTriggerHandler.validateGpsRecordSize(trigger.old);
        }
       locationList = [select id,Name,Line_item__c from Location__c where id IN:locationIdSet];
       if(locationList.size()>0){
            CARPOL_BRS_LocationsTriggerHandler.processLocationReadiness(locationList);
       }
       
        GenericHistoryClass.LogChangeHistory();
       
    }
}