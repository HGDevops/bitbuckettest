trigger EFLRestrictAnnualReportCreation_Trigger on EFLAnnual_Report__c (before insert, after insert) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
           for(EFLAnnual_Report__c newAnnualReport : Trigger.new){
               try{
                   if(newAnnualReport.EFLRegistration__r.EFLAnnual_Report_Created__c = true){
                    newAnnualReport.addError('A Research Registration may only have one Annual Report per fiscal year');
                    }
               }catch(Exception e){
                   String newEx = e.getMessage();
                   newAnnualReport.addError('A Research Registration may only have one Annual Report per fiscal year');
               }
           }  
        }
        else if(Trigger.isAfter){
            EFLRegistration__c reg = [SELECT Id, EFLAnnual_Report_Created__c FROM EFLRegistration__c WHERE Id=:Trigger.new[0].EFLRegistration__c];
            reg.EFLAnnual_Report_Created__c = true;
            update reg;
        }
    }
}