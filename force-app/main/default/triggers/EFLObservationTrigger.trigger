//Developer Name: Peter Tran
//Creation Date: 03/18/2019
//File Name: EFLObservationTrigger.apxt
//sObject Affected: Observation__c
//Client: United States Department of Agriculture
//Client Project Name: APHIS CARPOL
//Contractor: Accenture Federal Services

    /*****************************
     *  Developer Documentation  *
     *****************************
     * _________________________
     * Trigger Description
     * 
     * Changelog
     * V1.0: Creation of EFLObservationTrigger.apxt
     * - File created in DevShare on 03/18/2019
     * 
     */

//Begin trigger declaration
trigger EFLObservationTrigger on Observation__c (before insert, after insert, after delete) {
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            // Process before insert
            
            //Data validation for custom field Observation_Date__c: "Observation start date has to be Unique."
            EFLObservationTriggerHandler.checkObservationStartDate(Trigger.new);
        } else if (Trigger.isAfter) {
            // Process after insert     
        }        
    }
    else if (Trigger.isDelete) {
        // Process after delete
    }
}