/* ***************************************************************************************
-- Begin Default  ---
** Class Name     : EQS_RFRTrigger 
** Description   : Apex Trigger 
** Author(s)     : Harish Gudipudi (HG)
**        Revision History:-
** Version  Date        Author  Description of Action
** 1.0      08/24/2018 HG      Initiated Script  
 
**************************************************************************************** **/

trigger EQS_RFRTrigger on EQS_Resource_Request__c (after delete, after insert, after update, after undelete) {
    Set<Id> rfrIds = new Set<Id>();
    Set<Id> conIds = new Set<Id>(); 
    if (Trigger.isDelete) {
            for(EQS_Resource_Request__c std : Trigger.old) {
                rfrIds.add(std.EQS_Position__c);
                conIds.add(std.EQS_Responder__c);
            }          
    } else if(Trigger.isInsert || Trigger.isUnDelete){
        for(EQS_Resource_Request__c std : Trigger.new) {
            rfrIds.add(std.EQS_Position__c);
            conIds.add(std.EQS_Responder__c);
        }
    }
    else if(Trigger.isUpdate){
        EQS_Resource_Request__c oldStu;
        for(EQS_Resource_Request__c stu : Trigger.new) {
           // oldStu = Trigger.oldMap.get(stu.Id);
           // if(stu.EQS_Status__c != oldStu.EQS_Status__c || stu.EQS_Type__c != oldStu.EQS_Type__c ){
                rfrIds.add(stu.EQS_Position__c);
           // }
                conIds.add(stu.EQS_Responder__c);
        }
    }
    if(rfrIds!=null && rfrIds.size()>0) {
         try{
            EQS_RFRTriggerHandler.updateRollUponPosCert(rfrIds);
         }
         catch(Exception e){
            system.debug('@@@@'+e.getMessage());
         }
    }
    
     if(conIds!=null && conIds.size()>0) {
         try{
            EQS_RFRTriggerHandler.updateLastTDYonContact(conIds);
         }
         catch(Exception e){
            system.debug('@@@@'+e.getMessage());
         }
    }
     
  }