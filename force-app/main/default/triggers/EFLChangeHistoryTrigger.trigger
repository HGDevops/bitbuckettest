trigger EFLChangeHistoryTrigger on Change_History__c (after Insert) {

    CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('EFLChangeHistoryTrigger');
    if(!dt.disable__c){
    
         //ChangeHistory record after event   
         if(trigger.isAfter)
         {
             //ChangeHistory record after insert event 
             if(trigger.isInsert)
             {
               EFLChangeHistoryTriggerHandler.onAfterInsert(trigger.newMap);  
             }
         }
    
    }
    
}