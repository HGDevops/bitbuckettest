/*
* ToDo: Need to consolidate the Handlers and remove unused Code
* Handlers Used: 
* EFLLineItemUtility               
* CARPOL_UNI_CopyContacts               
* CARPOL_DuplicatePreventer               
* EFLLineItemHelper               
* CARPOL_Application_ManagedSharing
* CARPOL_UNI_RelatedLineDecision
* CARPOL_UNI_AuthGroupingString
* ViolatorLookupService
* CARPOL_LineItemApplicationTriggerHelper
* EFLAccountSharingUtility
* SpringCMTriggerHandler
* GenericHistoryClass
* 
*/ 

trigger CARPOL_UNI_MasterLineItemTrigger on AC__c (before insert, after insert, before update, after update, before delete, after delete) 
{
    //FA: Moved code into trigger bypass block. Needed for scenarios like record transfer
   // boolean  isdisabled = EFLContactUtility.islidisabled;
    CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_UNI_MasterLineItemTrigger');
    if (!dt.Disable__c){
        //static Integer count=0;
        
        //Date Validations Ravee Racharla 10/22/2018 Start of the code
        if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate) ) {
            
            //AC LiveDog LineItem RecordtypeId
            Id acLiveDogRecordtypeId = null;
            try
            {
                acLiveDogRecordtypeId = EFLGenericUtility.getRecordTypeId('AC Line Item Live Dogs');
            }
            catch(Exception ex)
            {
                acLiveDogRecordtypeId = null;
            }
            
            for(AC__c a : trigger.new){
                if(acLiveDogRecordtypeId!=null && a.recordtypeId == acLiveDogRecordtypeId)
                { 
                    //AC Animal Transportation Completedness Check
                    EFLLineItemUtility.ACAnimalTransportationProcessReadiness(a);
                    //AC Live Dog Lineitem Readiness
                    EFLLineItemUtility.ACLiveDogLineItemProcessReadiness(a);
                }
                string strBrs = a.Record_Type_Name__c; 
                system.debug(strBRS + '   ' + a.Movement_Type__c ); 
                
                if (strbrs.contains('Biotechnology Regulatory Services') && a.skip_validation__c!=TRUE) {
                    
                    //All Notifications are only one year 
                    //Movement_Type__c Import,   Interstate Movement, Interstate Movement and Release, Release
                    if (a.Type_of_Permit__c =='Notification' || a.Movement_Type__c == 'Import' || a.Movement_Type__c == 'Interstate Movement')  {
                        //Date endDate = a.Proposed_Start_Date__c.addYears(1)
                        if ( a.Proposed_End_Date__c > a.Proposed_Start_Date__c.addYears(1)){
                            a.Proposed_End_Date__c.addError('Proposed Expiration Date must be within one year from the Proposed Effective date.'); 
                        }
                        
                    }
                    if (a.Type_of_Permit__c !='Notification' && ( a.Movement_Type__c == 'Interstate Movement and Release' || a.Movement_Type__c == 'Release') ) {
                        //Date endDate = a.Proposed_Start_Date__c.addYears(3)
                        if ( a.Proposed_End_Date__c > a.Proposed_Start_Date__c.addYears(3)){
                            a.Proposed_End_Date__c.addError('Proposed Expiration Date must be within three years from the Proposed Effective date.'); 
                        }
                    }
                }
            }
            
        }
        //====================================================BEFORE INSERT=================================================    
        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                //W-034895 - Rajesh Potle
                //SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), issuedAuthListSCM.get(0).getSObjectType().getDescribe().getName(), issuedAuthListSCM, workflowCreateAuthFolders);
                Id incidentRecordTypeId = null;
                try
                {
                    incidentRecordTypeId = EFLGenericUtility.getRecordTypeId('Incident');
                }
                catch(Exception ex)
                {
                    incidentRecordTypeId = null;
                }
                map<Id, AC__c> mapLinesNewins = new map<Id, AC__c>();
                List<Regulated_Article__c> Ralst = new List<Regulated_Article__c>();
                
                for(AC__c o : trigger.new)
                {
                    if(string.isNotBlank(o.AccountID__c)){
                        o.Account__c = o.AccountID__c;
                        if(o.source__c == 'XML'){
                            o.Sharing_account__c = EFLApplicationEngineUtility.getSharingAccount(o.AccountID__c);
                        } 
                    }
                    mapLinesNewins.put(o.Id, o);
                    
                    //Added by Conor for W-026057
                    integer sopCount = o.SOP_Count__c <> null ? integer.valueOf(o.SOP_Count__c):0;
                    integer sopCBICount = o.SOP_CBI_Count__c <> null ? integer.valueOf(o.SOP_CBI_Count__c):0;
                    integer sopCBIDelCount = o.SOP_CBI_Deleted_Count__c <> null ? integer.valueOf(o.SOP_CBI_Deleted_Count__c):0;
                    if(sopCount <= 0 || sopCBICount != sopCBIDelCount){
                        o.SOP_Status__c = 'Yet to Add';
                    }else{
                        o.SOP_Status__c = 'Ready to Submit';
                    }
                    
                    //Added by Conor for W-026060
                    integer attachmentCBICount = o.Attachment_CBI_Count__c <> null ? integer.valueOf(o.Attachment_CBI_Count__c):0;
                    integer attachmentCBIDelCount = o.Attachment_CBI_Deleted_Count__c <> null ? integer.valueOf(o.Attachment_CBI_Deleted_Count__c):0;
                    if(attachmentCBICount != attachmentCBIDelCount){
                        o.Attachment_Status__c = 'Yet to Add';
                    }else{
                        o.Attachment_Status__c = 'Ready to Submit';
                    }
                    
                    CARPOL_UNI_CopyContacts.copying(trigger.new);
                    
                    CARPOL_DuplicatePreventer dupchk = new CARPOL_DuplicatePreventer(mapLinesNewins);
                    dupchk.duplicatePreventer();
                    
                    //VV added for multt shipper button logic - If the line item movementtype is not in pathway Allow_Various_Shippers_for__c, and Various Shipper was selected as
                    // Exporter then throw error and prevent saving 
                    //W-034895 - Rajesh Potla - Bypass for Incident rectype
                    if(o.RecordTypeId==incidentRecordTypeId){
                        program_line_item_pathway__c pthwy = [select id, Allow_Various_Shippers_for__c from program_line_item_pathway__c where id=:o.program_line_item_pathway__c ];   
                        if (pthwy.Allow_Various_Shippers_for__c!=null && !pthwy.Allow_Various_Shippers_for__c.contains(o.movement_type__c) && o.Exporter_First_Name__c.equalsIgnoreCase('Various'))                        
                            o.addError('Various Shippers is not allowed for this Movement Type');
                    }
                    
                }
                
            } 
            
            // TehL: Don't understand need of this IF clause and the code, what are we doing afer populating mapLinesNew
            if (Trigger.isUpdate) //-----------------------------------------before update ----------------------------
            {            
                map<Id, AC__c> mapLinesNew;
                mapLinesNew = new map<Id, AC__c>();
                for(AC__c o : trigger.new)
                {
                    mapLinesNew.put(o.Id, o);
                    //This piece was added to replace workflow Update on Submit - which sets Line Item to Locked
                    //5/2 Ravee Racharla. Locking eveing the withdrawn line items W-026600
                    if (o.Status__c=='Submitted' || o.Status__c == 'Withdrawn'){
                        o.Locked__c = 'Yes';
                    } 
                    else
                    {
                        o.Locked__c = 'No';
                    }
                    //Added by Conor for W-026057
                    //Ali, 11/20/2018: Added null check in following three statements to avoid null exception
                    integer sopCount = o.SOP_Count__c <> null ? integer.valueOf(o.SOP_Count__c):0;
                    integer sopCBICount = o.SOP_CBI_Count__c <> null ? integer.valueOf(o.SOP_CBI_Count__c):0;
                    integer sopCBIDelCount = o.SOP_CBI_Deleted_Count__c <> null ? integer.valueOf(o.SOP_CBI_Deleted_Count__c):0;
                    if(sopCount <= 0 || sopCBICount != sopCBIDelCount){
                        o.SOP_Status__c = 'Yet to Add';
                    }else{
                        o.SOP_Status__c = 'Ready to Submit';
                    }
                    
                    //Added by Conor for W-026060
                    integer attachmentCBICount = o.Attachment_CBI_Count__c <> null ? integer.valueOf(o.Attachment_CBI_Count__c):0;
                    integer attachmentCBIDelCount = o.Attachment_CBI_Deleted_Count__c <> null ? integer.valueOf(o.Attachment_CBI_Deleted_Count__c):0;
                    if(attachmentCBICount != attachmentCBIDelCount){
                        o.Attachment_Status__c = 'Yet to Add';
                    }else{
                        o.Attachment_Status__c = 'Ready to Submit';
                    }
                }            
                
            }
            //KA::W-026020::Delete the related object records associated with application
            if (Trigger.isDelete){
                set<ID> liids = new set<ID>();
                for(AC__c li: trigger.old)
                {
                    
                    liids.add(li.Id);
                }
                if (!liids.isempty()){  
                    List<Construct__c> constructList = new List<Construct__c>();
                    constructList = [SELECT Id,Line_Item__c FROM Construct__c WHERE Line_Item__c IN:liids];            
                    if(!constructList.isEmpty()){
                        delete constructList;
                    }
                }
                
            } 
            
        }// END of Trigger.isBefore
        
        
        
        if (Trigger.IsAfter) //====================================================AFTER=================================================
        {
            
            if (Trigger.isInsert) //-----------------------------------------after insert ----------------------------
            {
                if (EFLLineItemHelper.runOnce()) {
                    EFLLineItemHelper.copyToApplication(Trigger.new, Trigger.OldMap);
                }             
                // Nitish Jakkani: The following piece creates sharing records for the line when the applicant is a Partner user 
                List<AC__c>Lines = trigger.new;
                Set<ID> ApplicationIDs = new Set<ID>();
                for(AC__c Line:trigger.new){                
                    ApplicationIDs.add(Line.Application_Number__c);
                }
                
                List<Application__c>Applications = [SELECT ID,Name,Organization__c,Applicant_Name__c,Profile_Name__c FROM Application__c WHERE ID IN: ApplicationIDs];           
                String AccName,AccProfileName;
                
                ID ApplicantContactID;
                
                for(Application__c app:Applications)
                {
                    if(app.Applicant_Name__c!=null )
                    {                    
                        ApplicantContactID = app.Applicant_Name__c;
                        AccName = app.Organization__c;
                        AccProfileName = app.Profile_Name__c;
                    }
                }
                if((AccName!=null) && (ApplicantContactID!=null) && (!Test.isRunningTest()) && AccProfileName == 'efile Applicant')
                {
                    
                    User ApplicantUser = [SELECT Phone, Id, ContactID, UserType FROM User WHERE ContactID = :ApplicantContactID LIMIT 1];
                    if(ApplicantUser.UserType=='PowerPartner')
                    {
                        CARPOL_Application_ManagedSharing.afterLineItemInsert(ApplicantUser.Id, Lines);
                    }
                }
                
                //the next two classes update fields on the line items, we then take changes and update in storage
                List<AC__c> lineitemlist = CARPOL_UNI_RelatedLineDecision.insertRLD(trigger.new); //Inserting Related Line decisions
                lineitemlist = CARPOL_UNI_AuthGroupingString.groupString(lineitemlist); //Create Auth group string
                //set now so this update will not run additional classes
                checkRecursive.runOnce();
                update lineitemlist;                      
                if(!system.isFuture())
                {
                    for(AC__c ac : Trigger.new)
                    {    
                        if(ac.Cloned_Line_Item__c != true)
                            ViolatorLookupService.lookupViolator(ac.ID);                                     
                    }
                }
                
                //Added by Conor for W-037542
                List<AC__c> scmList = new List<AC__c>();
                
                for(AC__c ac: trigger.new){
                    scmList.add(ac);
                }
                
                if(scmList.size() > 0){
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), scmList.get(0).getSObjectType().getDescribe().getName(), scmList, 'Create Line Item Folder');   
                }
            }
            
            
            
            if (Trigger.isUpdate) //-----------------------------------------after update ----------------------------
            {
                final String workflow1 = 'LockFolders';
                final String workflow2 = 'Permit Attachment Names';
                final String workflow3 = 'UnlockFolders';
                List<AC__c> lineItemsSubmittedSpringCM = new List<AC__c>();
                List<AC__c> lineItemsWaitingSpringCM = new List<AC__c>();
                // W-026600. We need to run lockfolders workflow for withdrawn line items too
                List<AC__c> lineItemsWithdrawnSpringCM = new List<AC__c>();
                
                //Anh Phan changes stars here
                CARPOL_LineItemApplicationTriggerHelper.updateLineitemChildRecords(trigger.newMap);
                //Anh Phan changes ends here
                
                if (EFLLineItemHelper.runOnce()) {
                    EFLLineItemHelper.copyToApplication(Trigger.new, Trigger.OldMap);
                }             
                integer LineitemCount = 0;
                Authorizations__c auth = new Authorizations__c();
                AC__c lineitemStatusUpd = new AC__c();
                
                map<Id, AC__c> mapLinesNew = new map<Id, AC__c>();
                Integer x=0;
                list<AC__c> changedSharingAccounts = new list<AC__c>();
                
                for(AC__c o : trigger.new)
                {
                    if(Trigger.oldMap.get(o.Id).Status__c <> o.Status__c){
                        if(o.Status__c == 'Submitted')
                        {
                            lineItemsSubmittedSpringCM.Add(o); //For SpringCM workflow trigger
                            x++;
                        }
                        if(o.Status__c == 'Waiting on Customer')
                        {
                            lineItemsWaitingSpringCM.Add(o); //For SpringCM workflow trigger
                        }
                        //W-026600
                        if(o.Status__c == 'Withdrawn')
                        {
                            lineItemsWithdrawnSpringCM.Add(o); //For SpringCM workflow trigger
                        }
                    }
                    mapLinesNew.put(o.id,o);
                    
                    if(o.Sharing_Account__c != trigger.oldMap.get(o.Id).Sharing_Account__c){
                        changedSharingAccounts.add(o);
                    }
                }
                
                if(!changedSharingAccounts.isEmpty()){
                    EFLAccountSharingUtility.shareChildrenFromParent(changedSharingAccounts, 'BRS');
                }
                
                if(checkRecursive.runOnce() && (x<1))
                {
                    CARPOL_UNI_Update_RLD.updateRLD(trigger.old);
                    //the next two classes update fields on the line items, we then take changes and update in storage
                    List<AC__c> lineitemlist = CARPOL_UNI_RelatedLineDecision.insertRLD(trigger.new);
                    boolean AuthgrpStrExists = false;  
                    for (AC__c li: lineitemlist){
                        if(li.Authorization_Group_String__c!=null) 
                            AuthgrpStrExists = true;
                    }
                    if (!AuthgrpStrExists) // If authgrpstring exists, don't run it again
                        lineitemlist = CARPOL_UNI_AuthGroupingString.groupString(lineitemlist);
                    if (trigger.old[0].status__c != 'Submitted' && trigger.new[0].status__c != 'Withdrawn' ) // VV added to prevent the status going back to Ready to Submit upon Withdrawn
                    { 
                        update lineitemlist;
                    }
                    
                    if(!system.isFuture())
                    {
                        for(AC__c ac : Trigger.new)
                        {
                            ViolatorLookupService.lookupViolator(ac.ID);                                             
                        }
                    }
                }
                
                Id AuthId = null;
                for(AC__c lnupdatest : trigger.new){
                    AuthId= lnupdatest.Authorization__c;
                }
                if (AuthId != null){
                    // Get the line items of the line item's auth.
                    auth = [Select Name,ID, (Select Id,Name,Status__c From Animal_Care_AC_Authorization__r) from Authorizations__c Where ID =:AuthId];
                    
                    //check if there is atleast one Line item that is not voided/canceled/No Jurisdiction. 
                    //12/18/17 VV Updated to add Withdrawn per US-17053
                    boolean AuthwithOneValidLn = false;
                    for(Ac__c lineItem : auth.Animal_Care_AC_Authorization__r)
                    {
                        if(!(lineItem.Status__c == 'Voided' || lineItem.Status__c == 'Not Permitted' ||lineItem.Status__c == 'Cancelled' ||lineItem.Status__c == 'No jurisdiction'|| lineItem.Status__c =='Withdrawn' ))
                        {
                            AuthwithOneValidLn = true;
                            break;
                        } 
                        
                    }
                    // VV update for 17738 to update Auth Date of arrival with changes to line item proposed date of arrival
                    if((Trigger.new[0].Proposed_date_of_arrival__c != Trigger.Old[0].Proposed_date_of_arrival__c)) {
                        auth.Proposed_Date_of_Arrival__c = Trigger.new[0].Proposed_date_of_arrival__c;
                        update auth;
                    }
                    
                }
                // VV added for status updates per 16903
                //Count number of line items under auth
                for(Ac__c lineItemcurr : auth.Animal_Care_AC_Authorization__r)
                {
                    LineitemCount ++; 
                    if(lineitemcurr.id == Trigger.New[0].id) 
                    {
                        lineitemStatusUpd = lineitemcurr; //sobject for current LI
                    }         
                }                         
                //if the status is on of the listed, set Auth=null on LI
                //12/18/17 VV Updated to add Withdrawn per US-17053
                if(lineitemStatusUpd.Status__c == 'Disapproved' || lineitemStatusUpd.Status__c == 'Voided' || lineitemStatusUpd.Status__c == 'Permit Not Required' || lineitemStatusUpd.Status__c == 'Cancelled' || lineitemStatusUpd.Status__c == 'No jurisdiction'){        
                    LineItemCount-- ; // remove the current one from count
                    if(LineItemCount ==0){  // if the current one is the only one (left), set Auth status same as LI
                        auth.status__c = lineitemStatusUpd.Status__c;
                        update auth;  
                    }                                                              
                }
                //end 16903 changes
                if(lineItemsSubmittedSpringCM.size() > 0){
                    //Run SpringCM workflow
                    //Ali - 02/19/2019
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), lineItemsSubmittedSpringCM.get(0).getSObjectType().getDescribe().getName(), lineItemsSubmittedSpringCM, workflow1);
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), lineItemsSubmittedSpringCM.get(0).getSObjectType().getDescribe().getName(), lineItemsSubmittedSpringCM, workflow2);
                }
                if(lineItemsWaitingSpringCM.size() > 0){
                    //Run SpringCM workflow
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), lineItemsWaitingSpringCM.get(0).getSObjectType().getDescribe().getName(), lineItemsWaitingSpringCM, workflow3);
                }
                if(lineItemsWithdrawnSpringCM.size() > 0){
                    //Run SpringCM workflow
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), lineItemsWithdrawnSpringCM.get(0).getSObjectType().getDescribe().getName(), lineItemsWithdrawnSpringCM, workflow1);
                }  
            }
            
            GenericHistoryClass.LogChangeHistory();
            
        } // END of Trigger.isUpdate
    }
    
    
} // END of Trigger