trigger FindContacts on Preparer_Request__c (before insert, before update) {
    set<string> emails = new set<String>();
    for (Preparer_Request__c preparerContact : Trigger.new) {
        if (preparerContact.Contact_Email__c != null) {
            emails.add(preparerContact.Contact_Email__c);
        }
    }
    
    map<string,Id> contactMap = new map<string,Id>();
    if(!emails.isEmpty()){
        for(Contact c : [SELECT Id, Email FROM Contact WHERE Email IN :emails]){
            contactMap.put(c.Email, c.Id);
        }
    }
    for (Preparer_Request__c preparerContact : Trigger.new) {
        if (contactMap.containsKey(preparerContact.Contact_Email__c)) {
            preparerContact.Preparer_Broker_Name__c = contactMap.get(preparerContact.Contact_Email__c);
        } else {
            preparerContact.Preparer_Broker_Name__c = null;
        }                             
    }
}