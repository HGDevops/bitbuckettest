trigger CreateExternalUserOnInviteAccept on User_Invite__c (after update){
    Set<Id> usersIdsToDeactivate = new Set<Id>();
    List<User> usersToDeactivate = new List<User>();
    system.debug('--trigger.new--'+trigger.new);
    Map<Id, String> mapForFedPopulation = new Map<Id, String>();
        for (User_Invite__c ui: Trigger.new){
            if(ui.From_Accept__c){
                List<CARPOL_UserInviteController.InvitedContact> newInviteeList = (List<CARPOL_UserInviteController.InvitedContact>) Json.deserialize(ui.Invitee_List__c, List<CARPOL_UserInviteController.InvitedContact>.class);
                List<CARPOL_UserInviteController.InvitedContact> oldInviteeList = (List<CARPOL_UserInviteController.InvitedContact>) Json.deserialize(Trigger.oldMap.get(ui.id).Invitee_List__c, List<CARPOL_UserInviteController.InvitedContact>.class);
                system.debug('---newInviteeList ---'+newInviteeList );
                system.debug('---oldInviteeList ---'+oldInviteeList );
                if(newInviteeList != null && oldInviteeList != null){
                    for (CARPOL_UserInviteController.InvitedContact nI: newInviteeList){
                        for (CARPOL_UserInviteController.InvitedContact oI: oldInviteeList){
                           if(nI.status != null){
                               if (nI.status != oI.status){
                                   if (nI.status!=null){//
                                        //if(nI.userName=='aphisinvite@mailinator.com'){
                                            usersIdsToDeactivate.add(nI.oldUserId);
                                            mapForFedPopulation.put(nI.FedId, JSON.serialize(nI));
                                            //}
                                        }                       
                                }    
                            }
                        }   
                    }
                }
            }
        }
    if(usersIdsToDeactivate.size()>0 && !mapForFedPopulation.isEmpty()){    
        EFLUserInvitetriggerclass.updateOldUser(usersIdsToDeactivate,mapForFedPopulation);
    }
   
    }            
//}