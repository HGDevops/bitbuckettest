trigger EFLLocationRelatedContactTrigger on Related_Contact__c (before insert, before update, after insert, after update, after delete) {
    
    if (Trigger.IsBefore && Trigger.IsInsert){
        EFLLocationRelatedContactTriggerHandler.validateFieldFormatAndRequired(Trigger.New);
    }
    if (Trigger.IsBefore && Trigger.IsUpdate){
        EFLLocationRelatedContactTriggerHandler.validateFieldFormatAndRequired(Trigger.New);
    }
    if (Trigger.IsAfter)
    {
        GenericHistoryClass.LogChangeHistory();
    } 
    
}