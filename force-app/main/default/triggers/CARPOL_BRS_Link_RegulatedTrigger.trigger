trigger CARPOL_BRS_Link_RegulatedTrigger on Link_Regulated_Articles__c (before update,before insert,after insert,after update,before delete, after delete) {
    /*  ====================================================  */
    /*  Name: CARPOL_BRS_Link_RegulatedTrigger                */                
    /*  Copyright notice:                                     */                       
    /*  ====================================================  */ 
    /*  ====================================================  */
    /*  Purpose: BRS Specific Activities,                     */ 
    /*           Locking factor,Added Validations             */    
    /*  ====================================================  */ 
    /*  ====================================================  */
    /*  History:                                              */    
    /*  ----------------------------------------------------  */
    /*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
    /*  1.0 - D12/Kishore 04/08/2015  INITIAL DEVELOPMENT     */ 
    /*  ====================================================  */
    
    CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_Link_RegulatedTrigger');
    if(!dt.disable__c) {
        
        // check record type of application
        Map<ID, Schema.RecordTypeInfo> apprtMap = Schema.SObjectType.AC__c.getRecordTypeInfosById();
        list<string> lstappids = new list<string>();
        list<ac__c> listlineitemToUpdate = new list<ac__c>();
        string apprt='';
        string RAStatus ='';
        list<Link_Regulated_Articles__c> lstlnkart = trigger.new;
        list<Link_Regulated_Articles__c> lraupdate = new list<Link_Regulated_Articles__c>();
        
        /*if(trigger.isbefore){
            if(trigger.isinsert || trigger.isupdate){
                for(Link_Regulated_Articles__c l:trigger.new){
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                        }
                      }
                for(AC__c a:[select id,RecordTypeid,Status__c from AC__c where id in:lstappids]){
                    apprt = apprtMap.get(a.RecordTypeId).getName();
                    if(apprt=='Biotechnology Regulatory Services - Notification' || apprt=='Biotechnology Regulatory Services - Standard Permit' || apprt=='Biotechnology Regulatory Services - Courtesy Permit')
                        {
                        if(a.Status__c=='Submitted' || a.Status__c=='In Review' || a.Status__c=='In Review Biotechnologist' || a.Status__c=='In Review Compliance')
                            {
                            lstlnkart[0].addError('Application is submitted you cannot Add/Edit the Link Regulated Article');
                            }
                         }
                       }
                    }
                  }*/
        
        if(trigger.isbefore){
            
        }
        
        // Added login for BRS line item status change
        if(trigger.isafter){
            
            if(trigger.isinsert){
                
                for(Link_Regulated_Articles__c l:trigger.new){
                    System.debug('Regulated Article - In insert is Line item null ?>> '+ l.Line_Item__c);
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                    }
                }
                
                for(AC__c a:[select id,RecordTypeid,Status__c,Regulated_Article_status__c from AC__c where id in:lstappids])
                {	
                    System.debug('Regulated Article - Line Item Details Insert?>> '+ a);
                    apprt = apprtMap.get(a.RecordTypeId).getName();
                    if(apprt=='Biotechnology Regulatory Services - Notification' || apprt=='Biotechnology Regulatory Services - Standard Permit' || apprt=='Biotechnology Regulatory Services - Courtesy Permit')
                    {
                        if(a.Status__c=='Saved' || a.Status__c== 'Ready to Submit' || a.Status__c=='Draft')
                        {
                            a.Regulated_Article_status__c = 'Ready to Submit';
                            listlineitemToUpdate.add(a);
                        }
                    }
                }
                
                
            }
            if(trigger.isupdate){
                for(Link_Regulated_Articles__c l:trigger.new)
                {
                    System.debug('Regulated Article - In update is Line item null ?>> '+ l.Line_Item__c);
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                    }
                }
                system.debug('!!!!!!lstappids'+lstappids);
                AC__c ac = [SELECT ID,RecordType.Name,Construct_Status__c,Status__c,(select id,status__C,Corrections_Required__c from Link_Regulated_Articles__r) From AC__c WHERE ID in: lstappids];
                System.debug('Regulated Article - Line Item Details Update ?>> '+ ac);
                if(ac.Link_Regulated_Articles__r.size() > 0){
                    for(Link_Regulated_Articles__c lra :ac.Link_Regulated_Articles__r ){
                        system.debug('!!!!!!lra.Status__c'+lra.status__C);
                        if(lra.status__C == 'Review Complete' && lra.Corrections_Required__c != null){
                            lra.Corrections_Required__c='';
                            lraupdate.add(lra);
                            
                        }
                        
                    }
                }
            }
            //KM:12/17/2018:W-026950::Starts:Deleting a Regulated Article record would invalidate and clear any Construct records associated with the Regulated Article::
            if(trigger.isdelete){
                List<Construct__c> cList= new List<Construct__c>();
                List<String> sList = new List<String>();
                sList.add('Submitted');
                sList.add('Review Complete');
                sList.add('Waiting on Customer');
                sList.add('In Review');
                Set<Id> regulatedIds;
                Set<Id> ConstructApplicationJunctionSet;
                Map<Id,construct__c> cMap = new Map<Id,construct__c >();
                Map<Id,construct__c> cMap2 = new Map<Id,construct__c >();
                Map<Id,Link_Regulated_Articles__c> rMap = new Map<Id,Link_Regulated_Articles__c>();
                List<Id>  newConstructList = new List<Id>();
                List<Construct_Application_Junction__c> cajList = new List<Construct_Application_Junction__c>();
                for(Link_Regulated_Articles__c l:trigger.old){
                    if(l.Line_Item__c!=null){
                        
                        lstappids.add(l.Line_Item__c);
                    }
                    
                    if(l.Regulated_Article__c!=null){
                        rMap.put(l.Regulated_Article__c, l);
                    }
                    
                }
                
                if(!rMap.isEmpty()){ // empty check
                    
                    for(Regulated_Article__c r:[SELECT Id,(SELECT Id,Link_Regulated_Article__c FROM Constructs__r Where Link_Regulated_Article__c IN : rMap.keyset() AND Line_Item__c IN :lstappids) FROM Regulated_Article__c WHERE Id IN:rMap.keyset()]){ // bulkifing 
                        for(construct__c c : r.Constructs__r){
                            system.debug('c data::' +c);
                            cList.add(c);
                            cMap.put(c.id,c);
                        }
                    }
                    for(Regulated_Article__c ra : [SELECT Id  ,(SELECT Id FROM Constructs__r WHERE Status__c In:sList)  FROM Regulated_Article__c WHERE id IN: rMap.keyset()]){
                        
                        for(construct__c c: ra.Constructs__r){
                            cMap2.put(c.Id,c);
                        }
                    }
                    set<Id> cSet = cMap2.keyset();  
                    List<Construct_Application_Junction__c> caList = [select id,Construct__c from Construct_Application_Junction__c where Line_Item__c In: lstappids];   // get all Construct Application Junctions
                    
                    if(!caList.isEmpty()){
                        for(Construct_Application_Junction__c c: caList){
                            
                            newConstructList.add(c.Construct__c); // get all constucts
                        }
                    }
                    List<Id> prepareIdToDelete = new List<Id>();
                    if(!newConstructList.isEmpty()){ 
                        for(Id cid: newConstructList){
                            if(cSet.contains(cid)){
                                prepareIdToDelete.add(cid);
                            }    
                        }
                    } 
                    if(!prepareIdToDelete.isEmpty()){ 
                        DELETE [select id,Construct__c from Construct_Application_Junction__c where Construct__c In: prepareIdToDelete ] ; 
                    } 
                }  
                if(!cList.isEmpty()){
                    database.delete(cList);
                }
                //KM:12/17/2018:W-026950::::::::::::::::::: ENDS::::::::::::::::::::::::::::::
                
                
                List<AC__c> LineItemList = [select id, (select id from Link_Regulated_Articles__r) from AC__c where id in : lstappids];
                for (AC__c a : LineItemList) {
                    if(a.Link_Regulated_Articles__r.size() == 0){
                        a.Regulated_Article_status__c = 'Yet to Add';
                        listlineitemToUpdate.add(a); 
                    }              
                }
                /*   set<id> lid = trigger.oldMap.keyset();
                    List<Regulated_Article__c> raList = [select id from Regulated_Article__c where id in : lid]; 
                    List<Construct__c> ConstructList = [select id,(select id from Constructs__r); */
                
                } 
            
            
            if(listlineitemToUpdate.size()>0)
                update listlineitemToUpdate; 
            
            GenericHistoryClass.LogChangeHistory();
        } 
        if (!lraupdate.isEmpty()){
            update lraupdate;
        }
        
    }    
}