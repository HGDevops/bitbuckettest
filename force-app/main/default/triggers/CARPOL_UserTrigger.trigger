trigger CARPOL_UserTrigger on User (after insert) {
    
    if(Trigger.isInsert){
        
        if(Trigger.isAfter){
            CARPOL_UserTriggerHandler.afterInsertHandler(Trigger.New);
        }
    }

}