trigger CARPOL_UNI_Master_Inspection_Question_trigger on EFL_Inspection_Questions__c (before insert, after insert, before update, after update, before delete, after delete) {
 if (Trigger.isBefore) //====================================================BEFORE=================================================
    {



    }
    if (Trigger.IsAfter) //====================================================AFTER=================================================
    {

        if (Trigger.isInsert) 
        {
            List<EFL_Inspection_Responses__c> optionlist = new List<EFL_Inspection_Responses__c>();
            for(EFL_Inspection_Questions__c Question:trigger.new){
                
                if(Question.Use_Yes_No_options__c){
                    
                    EFL_Inspection_Responses__c Option1 = new EFL_Inspection_Responses__c();
                    option1.Response__c = 'Yes';
                    option1.EFL_Inspection_Questions__c = question.id;
                    
                    optionlist.add(option1);
                    
                }
                
                if(Question.Use_Yes_No_options__c){
                    
                    EFL_Inspection_Responses__c Option1 = new EFL_Inspection_Responses__c();
                    option1.Response__c = 'No';
                    option1.EFL_Inspection_Questions__c = question.id;
                    
                    optionlist.add(option1);
                    
                }
                
            }
            
            if(optionlist!=null || !optionlist.isEmpty()){
                insert optionlist;
            }
        }
        
    }
}