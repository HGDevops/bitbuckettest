<apex:page standardController="Authorizations__c"  extensions="EFLManageLabelsExtensionPDF" showHeader="false" sidebar="false" standardStylesheets="false" applyBodyTag="false" applyHtmlTag="false" renderas="PDF">
  <html>
    <head>
        <style media="print">
            .labels{ page: labels}
            .instructions { page: instructions }        
        
            @page{
                size: letter;
                margin: 1in 1in .9in 1in;
            }
            
            @page labels{
                size: letter landscape;
                margin-top: 0.4in;
                margin-bottom: 0.36in;
                margin-left: 0.3in;
                margin-right 0.25in;
            }
            
            .page-break {
                display:block;
                page-break-after:always;
            }
            body {
                font-family: Arial Unicode MS;
            }
            div.label {
                border-style:solid;
                border-width:1px;
                border-color:black;
                //overflow:hide;
                width:4.75in;
                height:3.75in;
                text-align:center;
                font-family:sans-serif;
                font-size:9pt;
                float:left;
                //display:block;
                margin-bottom:.10in;
            }

            div.labelright {
                //display:block;
                border-style:solid;
                border-width:1px;
                border-color:black;
                //dmsoverflow:hide;
                width:4.75in;
                height:3.75in;
                text-align:center;
                font-family:sans-serif;
                font-size:9pt;
                margin-left:4.85in;
                margin-bottom:.10in;   
            }

        .colorTextTop {
            margin-top:.07in;
            margin-bottom:.2in;
            font-face:Arial;
            font-size:7pt;
            text-align:center;

        }
        .colorTextBottom {
            margin-bottom:.07in;
            font-face:Arial;
            font-size:7pt;        
            text-align:center;       
        }
        .contents1 {
            margin-top:.25 in;
        }

        .contents2 {
            font-size:10pt;
            font-face:Arial;
            font-weight:bolder;
            text-decoration:underline;
        }
        .contents3 {
            font-size:10pt;
            font-weight:bolder;
        }
        .contentInstruction {
            font-size:9pt;
            font-style:italic;
        }

        .deliverTo1 {
            font-size   : 18pt;
            font-weight : bold;
            margin-top  : .2in;
            text-align  : left;
            margin-left : 2.20in;
        }
        .deliverTo2 {
            font-size   : 12pt;
            font-weight : bold;
        }
        .deliverTo3 {
            font-size       : 8pt;
            margin-bottom   : .07 in;
        }
        .deliverTo4 {
            margin-top  : .2in;        
            font-size       : 18pt;
            margin-bottom   : .07 in;
            font-weight : bold;            
        }        

        .formNo {
            font-family : sans-serif;
            font-size   : 7pt;
            text-align  : left;
            clear       : left;
            margin-left : .20in;
            margin-top  : .03in;
            width       : 1.5in; 
            height      : .3in;     
            display     : block;
        }
        .permitNo1 {
            font-family : sans-serif;
            font-size   : 10pt;
            font-weight : bold;
            text-align  : left;
            margin-left : 0.42in;                      
        }
        .permitNo2 {
            font-family : "Times New Roman";
            font-size   : 9pt;
        }

        .labelNumber, .expirationDate {
            font            : 9pt "Times New Roman";
            font-weight     : bold;
        }

        .labelNumber {
            text-align  : left;
            margin-left : 0.20in;
            width:1.5in;
        }

        .expirationDate {
            text-align      : right;
            margin-right    : 0.20in;
        }

        .barCode {
            margin      : 0.08in 0in 0in 0.07in;
            text-align:left;
        }

        .Address {
            font        : 10pt "Times New Roman";
            text-align  : left;
            display     : block;
            height      : .83in;  
            width       : 2.5in;
            overflow    : hide;
        }          
        </style>
    </head>
    <body>
       <div>
            <apex:outputtext value="{!labelGuidanceContent}" escape="false"/> 
       </div>
       <div class="labels">
           <apex:variable var="rowNum" value="{!1}"/>
           <apex:repeat value="{!labelfinallist}" var="l" >  
                <!-- Red and White Labels -->
                <apex:outputPanel layout="none">            
                    <div class="{!IF(MOD(rowNum,2) = 0,'labelright', 'label')}" style="background-image: url({!URLFOR($Resource.EFLBRS_BW_Label_Background)})">
                        <div class="colorTextTop">Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;<b>OMB NO. 0579-0085</b></div> 
                        <div class="contents1">This Package Contains</div>
                        <div class="contents2">GENETICALLY ENGINEERED ORGANISMS</div>
                        <div class="contentInstruction">
                            DO NOT OPEN EXCEPT IN THE PRESENCE OF AN APHIS<br/>
                            INSPECTOR OR DESIGNATED REPRESENTATIVE OF USDA
                        </div>
                        <div class="deliverTo4">DELIVER TO</div>
                        <div class="deliverTo2">U.S. DEPARTMENT OF AGRICULTURE</div>
                        <div class="deliverTo3">
                            ANIMAL AND PLANT HEALTH INSPECTION SERVICE<br/>
                            PLANT PROTECTION AND QUARANTINE
                        </div>
                        <table width="100%">
                        <tr>
                        <td width="46%">
                        <!--div class="labelNumber" style="margin-top:.15in;">Label #<apex:outputLabel value="{!Right(Right(l.Name, LEN(l.Name) - FIND('-', l.Name)),LEN(Right(l.Name, LEN(l.Name) - FIND('-', l.Name)))-7)}"/></div-->
                        <div class="labelNumber" style="margin-top:.15in;">Label #<apex:outputLabel value="{!l.AutoNumber__c}"/></div>
                            </td>
                        <td width="64%">
                        <div class="expirationDate" style="margin-top:.15in;">Exp&nbsp;<apex:outputText value="{0,date,MM/dd/yyyy}"><apex:param value="{!l.Notification_Expiration_Date__c}" /></apex:outputText></div>
                        </td>
                        </tr>
                        <tr>
                        <td valign="top">
                        <div class="barCode" style="margin-left:0.20in;"><c:EFLBarcode content="{!l.Name}" codeType="CODE128"></c:EFLBarcode></div>
                        <div class="barCode" style="margin-left:0.20in;">{!Authorizations__c.Name}</div>
                        </td>
                        <td>
                        <div class="Address">
                        <apex:outputLabel rendered="{!if(Authorizations__c.BRS_Hand_Carry_For_Importation_Only__c=='Yes',true,false)}">BRS Hand Carry Permit<br/></apex:outputLabel>
                        <apex:outputLabel value="{!l.Plant_Inspection_Station_lkup__r.Address_1__c}"/>
                        <br/>
                        <apex:outputLabel value="{!l.Plant_Inspection_Station_lkup__r.Address_2__c}"/>
                        <br/>
                        <apex:outputLabel value="{!l.Plant_Inspection_Station_lkup__r.City__c}"/>,&nbsp;<apex:outputLabel value="{!l.Plant_Inspection_Station_lkup__r.State_Code__c}"/>&nbsp;<apex:outputLabel value="{!l.Plant_Inspection_Station_lkup__r.Zip__c}"/>
                        </div>
                        </td>
                        </tr>
                        </table>
                        <table>
                        <tr>
                        <td valign="top">
                        <div class="formNo">APHIS FORM 2051 (JUNE 2004)</div>
                        </td>
                        <td valign="top">                        
                        <div class="permitNo1">PERMIT NO.  <span style="font-size:9pt;font-weight:normal;"><apex:outputLabel value="{!Authorizations__c.Permit_Number__c}"/></span></div>
                        </td>
                        </tr>
                        </table>
                        <div class="colorTextBottom">Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White&nbsp;&nbsp;&nbsp;Blue &amp; White</div>
                    </div>
                </apex:outputPanel>          
                <apex:variable var="rowNum" value="{!rowNum + 1}"/>                             
           </apex:repeat><apex:outputPanel layout="none" rendered="{!IF(MOD(rowNum-1,4) = 0 || MOD(rowNum-1,3) = 0,true, false)}"><div style="float:left;font-style:italic;">Page Intentionally Left Blank</div></apex:outputPanel>
       </div>
    </body>
</html> 
</apex:page>