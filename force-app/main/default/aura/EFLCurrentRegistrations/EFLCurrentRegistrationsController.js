({
    startAnnualreport : function(component, event, helper) {
        //console.log('event.getSource().get("v.name")',event.getSource().get("v.name"));
        helper.goToReportPage(event.getSource().get("v.name"));
    }, 
    startNewAnnualreport : function(component, event, helper){ 
        //console.log('event.getSource().get("v.name")',event.getSource().get("v.name"));
        helper.makeNewAnnualReport(component, event.getSource().get("v.name"));
    },
    viewAnnualreport : function(component, event, helper){  
        var regtnId = event.getSource().get("v.name");
        //console.log('event.getSource().get("v.name")',event.getSource().get("v.name"));
        helper.goToReportPage(regtnId);
	},    
    doInit : function(component, event, helper) {
        // check if use is on IE 
        var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/;
        if (detectIEregexp.test(navigator.userAgent)){ 
            helper.displayModalMessage(component, $A.get("$Label.c.EFLACBrowserNotSupported"),$A.get("$Label.c.EFLACBestExpBrowsers"));          
        }        
        var action = component.get("c.getPageData");
        action.setCallback(this,function(response){
            var state = response.getState();   
            if(state === "SUCCESS"){
                component.set("v.pageData", response.getReturnValue());
                //console.log(response.getReturnValue());
            }else{
                console.log('Exception occured '+state.Error);
            }
            component.set("v.getRegistrations",response.getReturnValue());
        });  
        
        var action2 = component.get("c.getConType");
        action2.setCallback(this,function(response){
            var type = response.getReturnValue();
            component.set("v.contactType",type);
        });
        $A.enqueueAction(action); 
        $A.enqueueAction(action2);
        if(window.location.href.includes("eflannual-report")) {
            component.set("v.showHomePage", false);
        }
    }, 
    
    homePage : function(component, event, helper) {
        component.set("v.showHomePage",event.getParam("showHomePage"));
        var action = component.get("c.doInit");
        $A.enqueueAction(action);
    }
})