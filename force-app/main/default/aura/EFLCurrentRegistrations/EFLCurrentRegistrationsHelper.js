({
    goToReportPage : function(repId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            recordId: repId
        });
        navEvt.fire();
    },
    makeNewAnnualReport : function(component, regId){
        // make new report here.
        var action = component.get('c.saveNewAnnualReport');
        action.setParams({
            registrationId : regId
        });
        action.setCallback(this,function(result){
            console.log('going to report page: ' , result.getReturnValue());
            this.goToReportPage(result.getReturnValue());
            
        });
        $A.enqueueAction(action);
    },
    	displayModalMessage : function(component, header, messageBody){
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : false,
            showConfirm : true,
            confirmText : 'OK',
            cssFile : 'EFLAR_ConfirmationDialog'/*,
            cancelCallback : component.get('c.cancelCallback') // optional
            */
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            true,
            'confirmModal', // css file
            null
        );
    }
})