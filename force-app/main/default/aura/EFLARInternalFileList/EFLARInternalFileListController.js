({
    doInit : function(component, event, helper) {
        // get the parent Ids to display.
        var action = component.get('c.getPageData');
        action.setParams({
            arId : component.get('v.recordId')
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            //var usagekey ='EFL AR Internal File List';
            var usagekey = res.usageKey;
            var fileListParams = {
                usageKey : usagekey,
                parentIds : res.parentIds,
                recordId : component.get('v.recordId')
            }
            $A.createComponent('c:EFLFileList', fileListParams,
                               function(content, status, error) {
                                   var div = component.find('fileListDiv').get('v.body');
                                   div.push(content);
                                   component.find('fileListDiv').set('v.body', div);
                               });
        });
        $A.enqueueAction(action);
    }
})