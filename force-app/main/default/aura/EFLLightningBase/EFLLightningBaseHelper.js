({
    showModalComponent : function(component, componentToInclude, params, headerVal, showClose, className, closeCallback) {
        $A.createComponent(componentToInclude, params,
                           function(content, status, error) {
                               if (status === "SUCCESS") {
                                   try{
                                       component.getSuper().find('overlayLib').showCustomModal({
                                           header: headerVal,
                                           body: content,
                                           showCloseButton: showClose,
                                           cssClass: className,
                                           closeCallback: closeCallback
                                       });
                                   }catch(err){
                                       console.log(err);
                                   }
                               } else {
                                   console.log(error);
                                   throw new Error(error);
                               }
                           });
    },
    closeModal : function(component){
        component.getSuper().find("overlayLib").notifyClose();
    }
})