({
	getParentIdsForFiles : function(component) {
		// give the annual reporting Id, go find all related object's Id's and populate them into a list.
		// This list will be passed into the FileList component
		var action = component.get('c.getParentIds');
        action.setParams({
            arId : component.get('v.annualReportId')
        });
        action.setCallback(this, function(response){
            var res = response.getReturnValue();
            component.set('v.parentIds',res);
            var isLocked = false;
            var usageKey = 'EFL AR Summary Page';
            //console.log('the annual report record: ' , component.get('v.annualReport.EFL_Locked__c'));
            if(component.get('v.annualReport.EFL_Locked__c') != null){
                isLocked = component.get('v.annualReport').EFL_Locked__c;
            }
            if(isLocked == true){
                usageKey = 'EFL AR Summary Page Locked';
            }
            
            var fileListParams = {
                parentIds : res,
                usageKey : usageKey
            };
            //console.log('creating file list component instance');
            $A.createComponent('c:EFLFileList', fileListParams,
                               function(content, status, error) {
                                   var div = component.find('fileListDiv').get('v.body');
                                   div.push(content);
                                   component.find('fileListDiv').set('v.body', div);
                               });
        });
        $A.enqueueAction(action);
	}
})