({
    doInit : function(component, event, helper) {
        var loading = component.find("loading");
        $A.util.removeClass(loading,"slds-hide");
        
        var baseURLHost = window.location.host;
        var indexOfHyphen = baseURLHost.indexOf("-aphis");
        var currentEnvironment = baseURLHost.substring(0,indexOfHyphen);
        component.set("v.currentEnvironment",currentEnvironment);
        
        var annualReportId = component.get('v.recordId');
        //console.log('event param: ' ,event.getParam("showSummary"));
        if(event.getParam("showSummary")){
        	component.set('v.showSummary',event.getParam("showSummary"));
        }
        
        component.set("v.annualReportId",annualReportId);
        var action = component.get("c.fetchFacilities");
        action.setParams({
            annualReportId : annualReportId
        });
        action.setCallback(this,function(response){
            if(response.getReturnValue().EFLSites__c != undefined){
                var siteArray;
                if(response.getReturnValue().EFLSites__c.includes('\n')){
                    siteArray = response.getReturnValue().EFLSites__c.split('\n');
                    component.set("v.allSites",siteArray);
                }else{
                    component.set("v.allSites",response.getReturnValue().EFLSites__c);
                }
            }
            component.set("v.signerCertified",response.getReturnValue().EFLSigner_Certified__c);
            component.set("v.signerAgreed",response.getReturnValue().EFLSigner_Agreed__c);
            component.set("v.signerSelEmail",response.getReturnValue().EFLSigner_Selected_Email__c);
            component.set("v.registrationId",response.getReturnValue().EFLRegistration__c);
            component.set("v.showSummary",component.get('v.showSummary'));
            component.set("v.showSummaryStep1",true);
            component.set("v.showSummaryStep2",false);
            component.set("v.arName", response.getReturnValue().Name);
            component.set("v.showButtons",true);
            component.set("v.usingFederalFunds",response.getReturnValue().EFL_Using_Federal_Funds__c);
            component.set("v.changeInOwnership",response.getReturnValue().EFL_Change_In_Ownership__c);
            component.set("v.annualReport",response.getReturnValue());
            if(response.getReturnValue().EFL_Locked__c){
                component.set("v.showButtons",false);
            }
            var animalUsage = component.get("c.getAnimalUsage");
        	helper.getParentIdsForFiles(component);
            $A.enqueueAction(animalUsage);
        });
        // $A.enqueueAction(action);
        
        var action2 = component.get("c.getConType");
        action2.setCallback(this,function(response){
            var type = response.getReturnValue();
            component.set("v.contactType",type);
            var loading = component.find("loading");
            $A.util.addClass(loading,"slds-hide");
        });
        $A.enqueueAction(action);
        
        $A.enqueueAction(action2);
        
    },
    updateSingerAgreedClient  : function(component, event, helper) {
        var annualReportId = component.get("v.annualReportId");
        var signerAgreed = component.get("v.signerAgreed");
        var action = component.get("c.updateSingerAgreed"); 
        action.setParams({
            annualReportId : annualReportId,
            signerAgreed : signerAgreed
        }); 
        $A.enqueueAction(action);
    },
    getAnimalUsage : function(component, event, helper) {	
        var annualReportId = component.get("v.annualReportId");
        var action = component.get("c.fetchAnimalUsage");
        action.setParams({
            annualReportId : annualReportId
        });
        action.setCallback(this,function(response){
            var tableData = response.getReturnValue();
            for(var i=0;i<response.getReturnValue().length;i++){
                if(response.getReturnValue()[i].EFLColumnBHeldNotUsed__c == undefined)
                    tableData[i].EFLColumnBHeldNotUsed__c = 0;
                if(response.getReturnValue()[i].EFLColumnCUsedPainMinimized__c == undefined)
                    tableData[i].EFLColumnCUsedPainMinimized__c = 0;
                if(response.getReturnValue()[i].EFLColumnDUsedPainMinimized__c == undefined)
                    tableData[i].EFLColumnDUsedPainMinimized__c = 0;
                if(response.getReturnValue()[i].ELFColumnEPainNotMinimized__c == undefined)
                    tableData[i].ELFColumnEPainNotMinimized__c = 0;
            } 
            component.set("v.selectedAnimals",tableData);
            component.set("v.animalTableColumns",[
                { label: 'COLUMN A', 
                 fieldName: 'EFLAnimal__c',
                 type: 'text'
                },
                { label: 'COLUMN B', fieldName: 'EFLColumnBHeldNotUsed__c', type: 'number', cellAttributes: { alignment: 'left' }},
                { label: 'COLUMN C', fieldName: 'EFLColumnCUsedPainMinimized__c', type: 'number', cellAttributes: { alignment: 'left' }},
                { label: 'COLUMN D', fieldName: 'EFLColumnDUsedPainMinimized__c', type: 'number', cellAttributes: { alignment: 'left' }},
                { label: 'COLUMN E', fieldName: 'ELFColumnEPainNotMinimized__c', type: 'number', cellAttributes: { alignment: 'left' }},
                { label: 'TOTAL NUMBER OF ANIMALS (COLS C+D+E)', fieldName: 'EFLTotalAnimals__c', type: 'number', cellAttributes: { alignment: 'left' }}
            ]);
            
        });
        $A.enqueueAction(action);
    },
    
    goToAuthSignPage : function(component, event, helper) {
        var annualReportId = component.get("v.annualReportId");
        var action = component.get("c.updateARStatus");
        action.setParams({
            annualReportId : annualReportId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state != "SUCCESS"){
                alert('Exception occured '+state.Error);
            }           
        });
        $A.enqueueAction(action);
        component.set("v.showSummaryStep1",false);
        component.set("v.showSummaryStep2",true);
        
    },
    
    goToReviewPage : function(component, event, helper) {
        component.set("v.showSummaryStep1",true);
        component.set("v.showSummaryStep2",false);
    },
    
    backToHome : function(component, event, helper) {
        var eventBackPath = $A.get("e.c:EFLStageChangeInfoToPath");
        eventBackPath.setParams({
            completedStageName : "Stage 0"
        });
        eventBackPath.fire();
        component.set("v.showSummary",false);        
    },
    
    backToHQ : function(component, event, helper){
        var eventBackPath = $A.get("e.c:EFLStageChangeInfoToPath");
        eventBackPath.setParams({
            completedStageName : "Stage 0"
        });
        eventBackPath.fire();
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            recordId: component.get("v.annualReportId")
        });
        navEvt.fire();
        component.set("v.showSummary",false);
    },
    signAndSubmit : function(component, event, helper){
        var annRepId = component.get("v.annualReportId");  
        var arName = component.get("v.arName");
        var action = component.get("c.updateARContact");
        
        action.setParams({
            annualReportId : annRepId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state != "SUCCESS"){
                alert('Exception occured '+state.Error);
            }else{
                component.set("v.showConfirmation",true);
            }
        });
        $A.enqueueAction(action);
    },
    closeConfirm : function(component, event, helper){
        component.set("v.showConfirmation",false);        
        //document.location.reload(true);
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            url : "/",
            isredirect : true
        });
        navEvt.fire();
    }
})