({
    doInit : function(component, event, helper) {
        var device = $A.get("$Browser.formFactor");
        if (device=='PHONE' && window.matchMedia("(orientation: portrait)").matches){
            component.set("v.isMobile",true);
            //alert('This app is best viewed in landscape mode');
        }        
        helper.getPageData(component);
    },
    doExitPage: function(component, event, helper){
        try{
            window.open($A.get('$Label.c.EFLACLRA_Exit_Button_URL'),'_top')
        }catch(err){
            console.log(err);
        }
    }
})