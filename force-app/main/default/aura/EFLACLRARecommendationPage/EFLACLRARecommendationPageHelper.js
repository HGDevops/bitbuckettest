({
    getPageData : function(component) {
        // fetch data for the page.
        var action = component.get('c.getResultsPageData');
        action.setCallback(this, function(response){
            var state= response.getState();  
            if(state==='SUCCESS'){
                component.set("v.pageData",response.getReturnValue());
                this.setupResultsList(component);
            }else{
                console.log(response.getState());
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    setupResultsList : function(component) {
        // the component should have v.pageData and v.qualifiedPathways set before calling this function.
        try{
            var allPaths = component.get('v.pageData').pathwayMDT;
            var qualifiedPathways = component.get('v.qualifiedPathways');
            var allOutputTracker = [];
            var outputLicenses = [];
            var outputRegistrations = [];
            
            // first, loop through the qualified pathways only.
            for(var i = 0; i < qualifiedPathways.length; i++){
                var thisQualifiedPathway = qualifiedPathways[i];
                // get the matching pathway MDT and set the output object properties.
                for(var j = 0; j < allPaths.length; j++){
                	var thisPathwayMDT = allPaths[j];
                    if(thisPathwayMDT.Pathway_Name__c == thisQualifiedPathway.pathway
                      && thisPathwayMDT.Status__c == thisQualifiedPathway.status){
                        var thisOutput = {
                            pathResult : thisPathwayMDT,
                            qualified : true,
                            icon : thisPathwayMDT.Qualified_Icon_Name__c,
                            iconText : thisPathwayMDT.Icon_Text__c,
                            iconClass : thisPathwayMDT.Icon_Class__c
                        };
                        allOutputTracker.push(thisPathwayMDT.Pathway_Name__c);
                        thisPathwayMDT.Pathway_Type__c == 'Registration' ? outputRegistrations.push(thisOutput) : outputLicenses.push(thisOutput);
                        break;
                    }
                }
            }
            
            for(var i = 0; i < allPaths.length; i++){
                var thisPath = allPaths[i];
                if(!allOutputTracker.includes(thisPath.Pathway_Name__c)){
                    var res = {pathResult : thisPath};
                    var icon, iconText;
                    res['qualified'] = false;
                    res['icon'] = 'utility:ban';
                    res['iconText'] = $A.get('$Label.c.EFLACLRA_Likely_Not_Needed');
                    res['iconClass'] = 'ban';
                    allOutputTracker.push(thisPath.Pathway_Name__c);
                    // add to correct list.
                    thisPath.Pathway_Type__c == 'Registration' ? outputRegistrations.push(res) : outputLicenses.push(res);
                }
            }
            
            // sort each list.
            outputLicenses.sort(this.sortArray);
            outputRegistrations.sort(this.sortArray);
            var output = [];
            output.push({heading : $A.get('$Label.c.EFLACLRA_LICENSE_RESULTS'), paths : outputLicenses});
            output.push({heading : $A.get('$Label.c.EFLACLRA_REGISTRATION_RESULTS'), paths : outputRegistrations});
            component.set('v.output',output);
        }catch(err){
            console.log(err);
        }
    },
    sortArray : function(a,b){
        if (a.pathResult.Display_Order__c < b.pathResult.Display_Order__c)
            return -1;
        if (a.pathResult.Display_Order__c > b.pathResult.Display_Order__c)
            return 1;
        return 0;
    }
})