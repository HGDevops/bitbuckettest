({
    closeModal : function(component){
        var callBack = component.get('v.cancelCallback');
        if(callBack){
            callBack;
        }
        component.find("overlayLibrary").notifyClose();
    },
    doConfirm : function(component){
        var callBack = component.get('v.confirmCallback');
        //alert(callBack);
        if(callBack){
            callBack();
            component.find("overlayLibrary").notifyClose();
        }else{
        	component.find("overlayLibrary").notifyClose();
        }
    }
})