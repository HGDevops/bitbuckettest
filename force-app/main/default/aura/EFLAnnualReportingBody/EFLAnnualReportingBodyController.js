({
    doInit : function(component, event, helper) {
        try{
            //console.log('doInit');
            var action = component.get('c.getReportForView');
            action.setParams({
                arId : component.get('v.recordId')
            });
            //console.log('component.get(v.recordId)', component.get('v.recordId'));
            action.setCallback(this,function(response){
                //console.log('v.annualReport');
                //console.log('v.annualReport',response.getReturnValue());
                component.set('v.annualReport',response.getReturnValue());
                if(response.getReturnValue().EFL_Locked__c == true){
                    // fire the nav event.
                    var evt = $A.get("e.c:EFLStageChangeInfoToPath");
                    evt.setParams({
                        completedStageName : "Stage 4"
                    });
                    evt.fire();
                    //console.log('fired event');
                }
                component.set('v.pageLoaded',true);
            });
            //console.log('starting action');
            $A.enqueueAction(action);
        }catch(e){
            //console.log(e);
        }
    }
})