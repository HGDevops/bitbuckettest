({
    initKeywords : function(component) {
        try{
            var theText = component.get('v.questionText');
            var keywords = component.get('v.keywords');
            var newQuestion;
            var num = this.getOccurrences(theText,'</u>',false);
            try{
                var keywordMap = [];
                for(var k = 0 ; k < keywords.length; k++){
                    var kw = keywords[k].Keyword__c;
                    console.log('kw',kw);
                    var indices = this.getIndices(theText, kw);
                    console.log('indices',indices);
                    for(var o = 0; o < indices.length; o++){
                        keywordMap.push({index : indices[o], keyword : kw, Id : keywords[k].Id});
                    }
                }
                console.log('keywordMap',keywordMap);
                // now sort the keyword occurrences by index.
                if(keywordMap.length > 0){
                    // sort it by index.
                    //var sortedMap = keywordMap.sort((a,b) => a.index - b.index).map((keywordMap, index, array) => keywordMap.keyword);
                    var sortedMap = keywordMap.reduce(function (r,v) {
                        if(v.keyword && v.keyword != null){
                            r.push({index : v.index, keyword : v.keyword, Id : v.Id});
                            return r;
                        }
                    }, []);
                    
                    // sortedMap is now a list of keywords that appear in the text, in the order they appear with the index as the starting point.
                    var sIndex = 0;
                    var output = [];
                    var outputString = '';
                    // traverse the text and put the pieces into a list of objects.
                    // set showLink=true if it's a keyword that needs to be clickable on the page.
                    for(var s = 0; s<sortedMap.length; s++){
                        var lengthToStartingIndex = sortedMap[s].index; // the length of text to grab.
                        var lengthToEndingIndex = lengthToStartingIndex + sortedMap[s].keyword.length;
                        var startString = theText.substring(sIndex,lengthToStartingIndex);
                        
                        // set the first string - non-clickable (could have 0 characters but that's ok and very unlikely)
                        output.push({value : startString, showLink : false});
                        outputString += startString;
                        
                        sIndex = lengthToStartingIndex;
                        var keywordString = theText.substring(sIndex,lengthToEndingIndex);
                        
                        // set the keyword string - set showLink to true and pass in the keyword's Id (used for modal)
                        output.push({value : keywordString, showLink : true, Id : sortedMap[s].Id});
                        outputString += '<a href="#" id="keywordClick">' + keywordString + '</a>';
                        
                        sIndex = sIndex + sortedMap[s].keyword.length;
                    }
                    
                    // if there are still characters after the final keyword, add them as the last array object.
                    if(sIndex < theText.length){
                        var finalString = theText.substring(sIndex);
                        output.push({value : finalString, showLink : false});
                        outputString += finalString;
                    }
                    
                    component.set('v.outputString', outputString);
                    console.log('v.output', output);
                    // outputList is used in the page to iterate through the text.
                    component.set('v.outputList',output);
                }else{
                    console.log('keyword NOT found');
                    var output = [];
                    output.push({value : theText, showLink : false});
                    component.set('v.outputString',theText);
                    component.set('v.outputList',output);
                }
            }catch(err){
                console.log(err);
            }
           
           /* for(var k = 0 ; k < keywords.length; k++){
                console.log('keyword: ' , keywords[k].Keyword__c);
                var kw = keywords[k];
                if(theText.includes(kw.Keyword__c)){
                    var tokens = theText.split(kw.Keyword__c);
                    var newString = '<span style="color:red !important;" id="clickText" data-id="' + kw.Id + '">' + kw.Keyword__c + '</span>'
                    newQuestion = tokens.join(newString);
                    console.log('newQuestion',newQuestion);
                    console.log('theText',theText);
                }
            }
            //component.set('v.questionText',newQuestion);
            component.set('v.clickhandler',$A.getCallback(function(evt){
                console.log('evt',evt);
            }));
            */
        }catch(err){
            console.log(err);
        }
    },
    getOccurrences : function(string, subString, allowOverlapping) {
        
        string += "";
        subString += "";
        if (subString.length <= 0) return (string.length + 1);
        
        var n = 0;
        var pos = 0;
        var step = allowOverlapping ? 1 : subString.length;
        
        while (true) {
            pos = string.indexOf(subString, pos);
            if (pos >= 0) {
                ++n;
                pos += step;
            } else break;
        }
        return n;
    },
    getIndices : function (string, subString) {
        var result = [];
        for (var i = 0; i < string.length; ++i) {
            if (string.substring(i, i + subString.length).toLowerCase() == subString) {
                //if (string.substring(i, i + subString.length) == subString) {
                result.push(i);
            }
        }
        return result;
    },
    getSubstringIndex : function(str, substring, n) {
        var times = 0;
        var index = null;
        while (times < n && index !== -1) {
            index = str.indexOf(substring, index+1);
            times++;
        }
        return index;
    },
    addListeners : function (component){
        try{
            var cmp = document.getElementById("keywordClick");
            cmp.addEventListener("click", function(item){
                    console.log('item clicked');
                    console.log(item.get('v.name'));
                });
            cmp.forEach(function(item){
                console.log('cmp found: ' , item.getLocalId());
                item.getElement().addEventListener("click", function(item){
                    console.log('item clicked');
                    console.log(item.get('v.name'));
                });
            }); 
        }catch(err){
            console.log(err);
        }
    }
})