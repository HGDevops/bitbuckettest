//EFLAnnualReportPathController

({
    doInit : function(component, event, helper) {
        component.set("v.showPath", true);
    },
	initiateComponent : function(component, event, helper) {
		var eventValue = event.getParam("showPath");
        component.set("v.showPath",eventValue);
	},
    homePage : function(component, event, helper) {
        component.set("v.showPath",event.getParam("showBody"));
    },
    stageChange : function(component, event, helper) {
        var oldStageSLDS = 'slds-path__item slds-is-complete customPath';
        var newStageSLDS = 'slds-path__item slds-is-current slds-is-active customPath';
        var pendingStageSLDS = 'slds-path__item slds-is-incomplete customPath';
        var stageComplete = event.getParam("completedStageName");
        if(stageComplete == 'Stage 0'){
            component.set("v.stage1Complete",false);
            component.set("v.stage1Class",newStageSLDS);
			
            component.set("v.stage2Complete",false);
            component.set("v.stage2Class",pendingStageSLDS);
            component.set("v.stage3Complete",false);
            component.set("v.stage3Class",pendingStageSLDS);
            component.set("v.stage4Complete",false);
            component.set("v.stage4Class",pendingStageSLDS);
            component.set("v.stage5Class",pendingStageSLDS);
        }
        if(stageComplete == 'Stage 1'){
            component.set("v.stage1Complete",true);
            component.set("v.stage1Class",oldStageSLDS);
			
			component.set("v.stage2Complete",false);
			component.set("v.stage2Class",newStageSLDS);
			
            component.set("v.stage3Complete",false);
            component.set("v.stage3Class",pendingStageSLDS);
            component.set("v.stage4Complete",false);
            component.set("v.stage4Class",pendingStageSLDS);
            component.set("v.stage5Class",pendingStageSLDS);
        }
        if(stageComplete == 'Stage 2'){
            component.set("v.stage1Complete",true);
            component.set("v.stage1Class",oldStageSLDS);
            component.set("v.stage2Complete",true);
            component.set("v.stage2Class",oldStageSLDS);
			
			component.set("v.stage3Complete",false);
            component.set("v.stage3Class",newStageSLDS);
			
            component.set("v.stage4Complete",false);
            component.set("v.stage4Class",pendingStageSLDS);
            component.set("v.stage5Class",pendingStageSLDS);
        }
        if(stageComplete == 'Stage 3'){
            component.set("v.stage1Complete",true);
            component.set("v.stage1Class",oldStageSLDS);
            component.set("v.stage2Complete",true);
            component.set("v.stage2Class",oldStageSLDS);
            component.set("v.stage3Complete",true);
            component.set("v.stage3Class",oldStageSLDS);
			
			component.set("v.stage4Complete",false);
            component.set("v.stage4Class",newStageSLDS);
			
            component.set("v.stage5Class",pendingStageSLDS);
        }
        if(stageComplete == 'Stage 4'){
            component.set("v.stage1Complete",true);
            component.set("v.stage1Class",oldStageSLDS);
            component.set("v.stage2Complete",true);
            component.set("v.stage2Class",oldStageSLDS);
            component.set("v.stage3Complete",true);
            component.set("v.stage3Class",oldStageSLDS);
            component.set("v.stage4Complete",true);
            component.set("v.stage4Class",oldStageSLDS);
            component.set("v.stage5Class",newStageSLDS);
        }
    }
})