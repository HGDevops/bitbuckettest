({
    getData : function(component, event, helper) {
        // method to take inputs on the first time login page and verify that the access code and
        // Regn numbers are valid and belong to the same account
        let button = event.getSource();
        button.set('v.disabled',true);
        var rNum = component.get("v.regNum");        
        var faAccessCode = component.get("v.faAccessCode"); 
        var txtContactUrFA = $A.get("$Label.c.EFLACAR_Contact_Your_FA");
        var txtFACodeUsed = $A.get("$Label.c.EFLACAR_FAACCode_Used");
        var txtIncorrPairing = $A.get("$Label.c.EFLACAR_Incorr_Pairing");
        var txtWrongPairError = $A.get("$Label.c.EFLACAR_Wrong_Pair_Error");
        var txtFAAddContacts = $A.get("$Label.c.EFLACAR_FA_Add_contacts");
        var txtFAAddContactsHdr = $A.get("$Label.c.EFLACAR_Add_contacts");
        var txtUserEntryFormMissingValues = $A.get("$Label.c.EFLARUserEntryFormMissingValues");
        var txtUserEntryFormMissingValuesHDR = $A.get("$Label.c.EFLARUserEntryFormMissingValuesHDR");
        if(rNum == null || faAccessCode == null){
            helper.displayModalMessage(component,txtUserEntryFormMissingValuesHDR,txtUserEntryFormMissingValues); // show Error for missing entries
        }
        if(rNum != null && faAccessCode != null ) {
            rNum = rNum.trim();
            faAccessCode = faAccessCode.trim();
            var action = component.get("c.findInfo");
            var regId = '';
            action.setParams({
                regNum: rNum,
                faAccessCode: faAccessCode,
            });
            action.setCallback(this, function(response){
                var state = response.getState();  
                if(state === "SUCCESS"){
                    //Error: Access code was used before and JIT contact email is on one of ACIS contacts
                    //show EFLCurrentRegistrations cmp w/data
                    if(response.getReturnValue().EFLAccount__r.EFL_FA_Code_Used__c == true){
                        helper.displayModalMessage(component,txtContactUrFA,txtFACodeUsed);
                    }else{                        
                        component.set("v.showForm",false); // insert acis contact/update email and set Account's FA Access Code Used? = true
                        helper.displayModalMessage(component,txtFAAddContactsHdr,txtFAAddContacts); // show prompt for this FA contact first time they were setup 
                        
                        // Call methods to load the regns and contacts table
                        var action2 = component.get("c.checkContactAccess"); 
                        action2.setCallback(this,function(response){
                            if(response.getReturnValue() == null){
                                component.set("v.showForm",true);    
                                // component.set("v.loggedinContact",response.getReturnValue());
                            }else{
                                var res = response.getReturnValue();
                                helper.showRegnList(component);
                                component.set('v.acctId',res.AccountId);
                                component.set('v.isAdmin',false);
                                if(res.EFL_ACIS_Contact__c && res.EFL_ACIS_Contact__r.EFL_Facility_Admin__c == true){
                                    component.set('v.isAdmin',true);
                                    helper.showContactList(component);
                                }
                                component.set("v.showForm",false); 
                            }
                            $A.util.addClass(loading,"slds-hide"); 
                        });
                        $A.enqueueAction(action2); 
                        
                        // 
                    }
                }else{
                    // Error: entered access code and/or regn# are not valid, throw error                    
                    helper.displayModalMessage(component,txtIncorrPairing,txtWrongPairError);                    
                }                
            });
            $A.enqueueAction(action);
        } 
        
    },
    //First check if the JIT email address is in any of the ACIS contacts
    //If yes the apex method checkContactAccess returns true or false
    doInit : function(component, event, helper) {
        var txtEFLACFTULGreeting1 = $A.get("$Label.c.EFLACFTULGreeting1");
        var txtEFLACFTULGreeting2 = $A.get("$Label.c.EFLACFTULGreeting2");
        var txtEFLACFTULFooter = $A.get("$Label.c.EFLACFTULFooter");
        component.set("v.txtEFLACFTULGreeting1", txtEFLACFTULGreeting1); 
        component.set("v.txtEFLACFTULGreeting2", txtEFLACFTULGreeting2);
        component.set("v.txtEFLACFTULFooter",txtEFLACFTULFooter);
        var loading = component.find("loading");
        $A.util.removeClass(loading,"slds-hide");
        
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userInfo = response.getReturnValue();
                component.set("v.userInfo", userInfo);
            }
        });
        $A.enqueueAction(action);
        
        
        var action = component.get("c.checkContactAccess"); 
        action.setCallback(this,function(response){
            if(response.getReturnValue() == null){
                component.set("v.showForm",true);    
                // component.set("v.loggedinContact",response.getReturnValue());
            }else{
                var res = response.getReturnValue();
                helper.showRegnList(component);
                component.set('v.acctId',res.AccountId);
                component.set('v.isAdmin',false);
                if(res.EFL_ACIS_Contact__c && res.EFL_ACIS_Contact__r.EFL_Facility_Admin__c == true){
                    component.set('v.isAdmin',true);
                    helper.showContactList(component);
                }
                component.set("v.showForm",false); 
            }
            $A.util.addClass(loading,"slds-hide");                
        });
        $A.enqueueAction(action);
    }
    
})