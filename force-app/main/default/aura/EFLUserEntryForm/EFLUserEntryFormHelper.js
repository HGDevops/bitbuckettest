({
	displayModalMessage : function(component, header, messageBody){
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : false,
            showConfirm : true,
            confirmText : 'OK',
            cssFile : 'EFLAR_ConfirmationDialog'/*,
            cancelCallback : component.get('c.cancelCallback') // optional
            */
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            true,
            'confirmModal', // css file
            null
        );
    },
    showContactList : function(component){
        // only show if user is a facility admin.
        
        var params = {
            parentId : component.get('v.acctId'),
            usageKey : 'EFL AR Community Contact Table'
        };
        $A.createComponent('c:EFLRelatedList', params,
                           function(content, status, error) {
                               var div = component.find('RelatedListDiv').get('v.body');
                               div.push(content);
                               component.find('RelatedListDiv').set('v.body', div);
                           });
    },
    showRegnList : function(component){
        // only show if user is a facility admin.
        $A.createComponent('c:EFLCurrentRegistrations',{},
                           function(content, status, error) {
                               var div = component.find('ListRegn').get('v.body');
                               div.push(content);
                               component.find('ListRegn').set('v.body', div);
                           });
    }
})