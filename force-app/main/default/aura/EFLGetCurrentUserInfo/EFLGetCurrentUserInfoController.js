({
   doInit : function(component, event, helper){
        var action = component.get("c.getConInfo");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.contactInfo",response.getReturnValue());
            }else{
                alert('Exception occured '+state.Error);
            }
        })
        	  $A.enqueueAction(action);
    } 
})