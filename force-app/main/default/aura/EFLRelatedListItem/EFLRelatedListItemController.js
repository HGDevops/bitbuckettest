({
	doEdit : function(component, event, helper) {
		helper.showEditForm(component);
	},
    onCheck : function(component, event, helper) {
        // fire checked event.
        var compEvent = component.getEvent("selectEvent");
        compEvent.setParams({
            record : component.get('v.record'),
            selected : component.get('v.record.isSelected')
        });
        compEvent.fire();
    },
    doDelete : function(component, event, helper){
        // fire event to tell parent to delete a record.
        var compEvent = component.getEvent("deleteEvent");
        compEvent.setParams({
            recordId : component.get('v.record.id'),
        });
        compEvent.fire();
    }
})