({
	showEditForm : function(component) {
		var editUI = component.get('v.editComponent');
        if(!editUI){
            console.log('the edit screen has not been configured');
            return;
        }else{
            editUI = 'c:' + editUI;
        }
        
        var self = this;
        var params = {
            recordId : component.get('v.record.id'),
            sObjectType : component.get('v.record').sObjectType,
            cssFile : 'EFLAR_ConfirmationDialog',
            cancelText : component.get('v.config.Cancel_Button_Text__c'),
            confirmText : component.get('v.config.Save_Button_Text__c'),
            usageKey : component.get('v.usageKey')
        };
        self.showModalComponent(
            component,
            editUI,
            params,
            component.get('v.config.Modal_Header__c'), // change this so it's passed in from config
            true,
            'confirmModal', // css file
            null // close callback
        );
	}
})