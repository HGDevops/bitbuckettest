({
    getData : function(cmp) {       
        var action = cmp.get("c.getAccountSites");
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                cmp.set("v.siteList", response.getReturnValue()); 
                console.log('apex results '+response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
             
            }
        }));
        $A.enqueueAction(action);
    }
})