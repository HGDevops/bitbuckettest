({
   fetchSiteData : function (cmp, event, helper) {
       var regtnId = event.getParam("regId");
       var action = cmp.get("c.getAccountSites");
       action.setParams({
           registrationId : regtnId
       });
       action.setCallback(this,function(response){
           cmp.set("v.siteList",response.getReturnValue());
           cmp.set("v.showFacilities",true);
       });
       $A.enqueueAction(action);
    },
    showGuidance : function(cmp,event, helper){
        alert('Contact ACIS at 800-555-1212');
    },
    homePage : function(component, event, helper) {
        component.set("v.showFacilities",event.getParam("showBody"));
    }
})