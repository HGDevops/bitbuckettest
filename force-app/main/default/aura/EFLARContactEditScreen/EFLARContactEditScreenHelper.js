({
    getPageData : function(component){
        // fetch the values for the role field.
        var action = component.get('c.getPageData');
        action.setParams({
            contactId : component.get('v.recordId')
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            console.log('res:',res);
            component.set('v.roleOptions',res.roleOptions);
            component.set('v.adminDetails', res.adminDetails);
            component.set('v.record.AccountId', res.accountId);
            if(res.contactRecord.EFL_Role__c && res.contactRecord.EFL_Role__c != null){
                component.set('v.selectedRoles',res.contactRecord.EFL_Role__c.split(';'));
            }
            component.set('v.record',res.contactRecord);
            var showCheckbox = true;
            //console.log('res.contactRecord.Id',res.contactRecord.Id);
            //console.log('res.contactRecord.EFL_Send_Welcome_Email__c',res.contactRecord.EFL_Send_Welcome_Email__c);
            if(res.contactRecord.Id && res.contactRecord.EFL_Send_Welcome_Email__c && res.contactRecord.EFL_Send_Welcome_Email__c == true){
                showCheckbox = false;
            }
            //console.log('showCheckbox',showCheckbox);
            component.set('v.showEmailCheckbox',showCheckbox);
        });
        $A.enqueueAction(action);
    },
    closeModal : function(component){
        console.log('checking cancelCallback');
        var callBack = component.get('v.cancelCallback');
        console.log('closing modal');
        if(callBack){
            callBack;
        }
        component.find("overlayLibrary").notifyClose();
        console.log('notifyClose');
    },
    validateForm : function(component){
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var allValid = component.find('requiredField').reduce(function (validSoFar, inputCmp) {
            if(inputCmp.get('v.type') == 'email' && inputCmp.get('v.value')){
                if(inputCmp.get('v.value').match(regExpEmailformat)){
                    inputCmp.setCustomValidity('');
                    $A.util.removeClass(inputCmp, 'slds-has-error');  
                }else{
                    inputCmp.setCustomValidity('You have entered an invalid format.');
                    $A.util.addClass(inputCmp, 'slds-has-error');
                }
            }
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if (allValid) {
            return true;
        } else {
            return false;
        }
        return true;
    },
    saveContact : function(component){
        //console.log('helper.saveContact');
        var contact = component.get('v.record');
        contact.EFL_Role__c = component.get('v.selectedRoles').join(';');
        var action = component.get('c.saveContact');
        action.setParams({
            contactRecord : contact
        });
        action.setCallback(this,function(result){
            //console.log('res.getState()',result.getState());
            if(result.getState() === 'SUCCESS'){
                try{
                    var appEvent = $A.get("e.c:EFLRefreshRelatedList");
                    appEvent.setParams({
                        usageKey : component.get('v.usageKey')
                    });
                    appEvent.fire();
                    this.closeModal(component);
                }catch(err){
                    //console.log(err);
                }
                var res = result.getReturnValue();
            }else{
                //console.log('stuff happened bad');
                console.log(res.getErrors());
            }
        });
        $A.enqueueAction(action);
    }
})