({
    doInit : function(component, event, helper) {
        // convert record to contact.
        helper.getPageData(component);
    },
    handleChange : function(component, event, helper){
        var selectedOptionsList = event.getParam("value");
        component.set("v.selectedRoles", selectedOptionsList);
    },
    closeModal : function(component, event, helper){
        console.log('closing modal');
        helper.closeModal(component);
    },
    doSave : function(component, event, helper){
        if(helper.validateForm(component)){
            helper.saveContact(component);
        }
    },
    check : function(component, event, helper){
        var isChecked = component.find('adminCheck').get('v.checked');
        component.set('v.record.EFL_Facility_Admin__c', isChecked);
    }
})