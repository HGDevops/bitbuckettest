({
	doInit : function(component, event, helper) {
		// get the types of documents available for Annual Reports.
		var docTypes = $A.get('$Label.c.EFLAR_All_Doc_Types');
        var docArray = docTypes.split(';');
        for (var i = 0; i < docArray.length; i++){
            docArray[i] = docArray[i].trim();
        }
        component.set('v.docTypes',docArray);
	},
    handleUploadFinished : function(component, event, helper){
        var uploadedFiles = event.getParam("files");
        for(var f in uploadedFiles){
            console.log(uploadedFiles[f]);
        }
        component.set('v.isLoading',true);
        var fieldMap = [];
        fieldMap.push({fieldName: 'File_Category__c', fieldValue : component.get('v.fileType')});
        fieldMap.push({fieldName: 'Description__c', fieldValue : component.get('v.description')});
        
        var action = component.get('c.setupNewFileWithParams');
        action.setParams({
            cds : JSON.stringify(uploadedFiles),
            parentId : component.get('v.parentId'),
            fileDetails : component.get('v.usageKey'),
            fieldMappingString : JSON.stringify(fieldMap)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var res = response.getReturnValue();
            if (state === "SUCCESS") {
                var appEvent = $A.get("e.c:EFLFileDeletedEvent");
            	appEvent.fire();
                component.find("overlayLibrary").notifyClose();
            }else{
                var errors = response.getError();
                var message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
            }
        });
        $A.enqueueAction(action);
    },
})