({
	initiateComponent : function(component, event, helper) {
        component.set('v.columns',[
            {type:'button',typeAttributes:{
                iconName:'utility:delete',
                name:'deleteRecord',
                variant:'base'
            	}
            },
            {label:'Column A',fieldName:'ColA',editable:'true',type:'text',iconName:'utility:info'},
            {label:'Column B',fieldName:'ColB',editable:'true',type:'number',iconName:'utility:info'},
            {label:'Column C',fieldName:'ColC',editable:'true',type:'number',iconName:'utility:info'},
            {label:'Column D',fieldName:'ColD',editable:'true',type:'number',iconName:'utility:info'},
            {label:'Column E',fieldName:'ColE',editable:'true',type:'number',iconName:'utility:info'},
            {label:'TOTAL NUMBER OF ANIMALS',fieldName:'ColF',iconName:'utility:info'}
             //{label:'Column E Explanations',fieldName:'ColG'}
                
            ]);  
        var currentAnnRprtId = event.getParam("annualReportId");
		var action = component.get('c.fetchTableDate');
        action.setParams({
            annualReportId : currentAnnRprtId
        });
        action.setCallback(this,function(response){
            var returnedData = response.getReturnValue();
            var finalDataToDisplay = [];
            for(var i=0;i<returnedData.length;i++){
                finalDataToDisplay.push({
                    ColA : returnedData[i].finalAnimalList.EFLAnimal__c,
                    ColB : returnedData[i].finalAnimalList.EFLColumnBHeldNotUsed__c,
                    ColC : returnedData[i].finalAnimalList.EFLColumnCUsedPainMinimized__c,
                    ColD : returnedData[i].finalAnimalList.EFLColumnDUsedPainMinimized__c,
                    ColE : returnedData[i].finalAnimalList.ELFColumnEPainNotMinimized__c,
                    ColF : returnedData[i].finalAnimalList.totalColumnValues,
                    ColG : returnedData[i].finalAnimalList.Id
                });
            }
            component.set('v.tableData',finalDataToDisplay);
            component.set('v.showAnimalTable',event.getParam("showAnimalUsage"));
            //alert(response.getReturnValue());
        });
        $A.enqueueAction(action);
        //helper.getTableData(component, event, helper);
	},
    
    homePage : function(component, event, helper) {
        component.set("v.showAnimalTable",event.getParam("showBody"));
    },
     //cellValueChanged : function(component, event, helper) {
     //var changedData = event.getParam('draftValues');
    //},
    
 	cellValueChanged : function(component, event, helper) {          
    var action = component.get('c.saveRegAnmls');
	var drafts = JSON.stringify(event.getParam('draftValues')[0]);
        var changedValue =drafts.substr(0,drafts.length);
    //alert(changedValue);
	action.setParams({
         anmlList : drafts
        });
    action.setCallback(this, function(response){
        });
        $A.enqueueAction(action);
        //component.find("animalsDataTable").set("v.draftValues", null);        
    },
    deleteAnimal : function(component, event, helper) {
        var row = event.getParam('row');
        var rows = component.get('v.tableData');
        var rowIndex = rows.indexOf(row);
        var action = component.get('c.deleteAnimalReg');
        action.setParams({
            animalToDelete : JSON.stringify(row)
        });
        action.setCallback(this,function(response){
            rows.splice(rowIndex,1);
            component.set('v.tableData',rows);
        });
        $A.enqueueAction(action);
    },
    upsert : function(component, event, helper){
            var rows = component.get('v.tableData');
        for(var i=0;i<rows.length;i++){
             // alert(JSON.stringify(rows[i]));
             }                  	        	
			var action = component.get('c.fetchTableDate');
       		action.setParams({
            	saveList : rows
       		 });
        		action.setCallback(this,function(response){
			});
       		 //$A.enqueueAction(action);
    },
    
        /*
     * This function get called when user clicks on Save button
     * user can get all modified records
     * and pass them back to server side controller
     * */
    handleSaveTable : function(component, event, helper) {
        var editedRecords =  component.find('animalsDataTable').get('v.draftValues');
        var totalRecordEdited = editedRecords.length;
        var action = component.get('c.saveRegAnmls');
        action.setParams({
            anmlList : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": totalRecordEdited+" Account Records Updated"
                    });
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                }
            }
        });
        $A.enqueueAction(action);
    }
 
})