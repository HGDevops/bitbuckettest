({
    //
    doInit : function(component, event, helper) {            
        var action=component.get("c.fetchWQ");
        var device = $A.get("$Browser.formFactor");
        if (device=='PHONE'){// && window.matchMedia("(orientation: portrait)").matches){
            component.set("v.isMobile",true);
        }
        action.setParams({
            questionSelections : component.get("v.quesInstance")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === 'SUCCESS'){ 
                component.set("v.richTextData",response.getReturnValue().richTextData);
                //console.log(response.getReturnValue().richTextData);
                component.set("v.startWithNonHover",true);                
                component.set("v.questionId",response.getReturnValue().questionId);
            }
        });
        $A.enqueueAction(action);
        
    },
    openPopup : function(component, event, helper) {        
        var action=component.get("c.fetchPopupData");
        var keyword = event.target.id;
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var explText ='';            
            if (state === 'SUCCESS'){  
                var responseList = response.getReturnValue();
                for(var i=0;i<responseList.length;i++) {
                    if(responseList[i].Name.toLowerCase() == keyword.toLowerCase().trim() ) {
                        component.set("v.keywordId",responseList[i].Id);
                        keyword = keyword.charAt(0).toUpperCase()+keyword.slice(1);
                        component.set("v.popupHeader",keyword);
                        explText =responseList[i].Explanation_Text__c;
                        //provide extra line break to reflect what is in the Explanation Text field.
                        if(!explText.includes('<p><br></p><p><br></p>')){
                            explText = explText.replace ('<p><br></p>','<p><br></p><p><br></p>');
                        }
                        component.set("v.popupBody",explText); 
                        //console.log("check explanation: " +explText);                        
                        component.set("v.showPopup",true); 
                        
                        try{
                            // fire app event.
                            var appEvent = $A.get("e.c:EFLACLRAKeywordClickedEvent");
                            appEvent.setParams({
                                keywordId : responseList[i].Id
                            });
                            appEvent.fire();
                        }catch(err){
                            //console.log(err);
                        }
                        break;
                        
                    } 
                }               
            }            
        });
        $A.enqueueAction(action); 
    },
    closePopup : function(component, event, helper) {
        component.set("v.showPopup",false);
    }
})