({
    initiateComponent : function(component, event, helper){
        component.set("v.annualReportId",component.get('v.recordId'));
        component.set("v.showExceptions",event.getParam("showExceptions"));
        var action = component.get("c.getPicklistValues");
        action.setCallback(this,function(response){
            if(response.getState() === 'SUCCESS'){
                component.set("v.pickListValues",response.getReturnValue());
                
                var action1 = component.get("c.getARDetails");
                action1.setParams({
                    arId : component.get("v.annualReportId")
                });
                action1.setCallback(this,function(response){
                    if(response.getState() === 'SUCCESS'){
                        component.set("v.arDetail",response.getReturnValue());
                        //alert(response.getReturnValue().EFLHasExceptions__c);
                        component.set("v.selectedPickListvalue",response.getReturnValue().EFLHasExceptions__c);
                    }
                });
                $A.enqueueAction(action1);
            }
        });
        $A.enqueueAction(action);
    },
    
    saveSelection : function(component,event, helper) {
        var action = component.get("c.updateReport");
        action.setParams({
            annualReportId : component.get("v.annualReportId"),
            selectedValue : component.get("v.selectedPickListvalue")
        });
        action.setCallback(this,function(response){
        });
        $A.enqueueAction(action);
    },
    
    saveInputs : function(component, event, helper){
        if(component.get("v.selectedPickListvalue")=="" || 
           component.get("v.selectedPickListvalue")== undefined){  
            var txtMissingFieldsHdr = $A.get("$Label.c.EFLARUserEntryFormMissingValuesHDR");
            var txtMissingFieldsMsg = $A.get("$Label.c.EFLARUserEntryFormMissingValues");            
            helper.displayModalMessage(component,txtMissingFieldsHdr,txtMissingFieldsMsg); 
            //alert('Please indicate if you have any Exceptions to report in the field below.');
            
            
        }
        else{
            var action = component.get("c.updateReport");
            action.setParams({
                annualReportId : component.get("v.annualReportId"),
                selectedValue : component.get("v.selectedPickListvalue")
            });
            action.setCallback(this,function(response){
            });
            $A.enqueueAction(action);            
        }
    },
    
    doSomething : function(component,event, helper) {
        window.open('https://www.aphis.usda.gov/animal_welfare/downloads/Animal-Care-Inspection-Guide.pdf', '_blank');
        
    },
    /* upsertAndnext : function(component, event, helper) {
        var triggerButton = event.getSource().getLocalId();
        //alert(JSON.stringify(component.get("v.animalList")));
        var action = component.get("c.upsertAnimalInfo");
        action.setParams({
            animalsToUpsert : component.get("v.animalList"),
            parentId : component.get("v.currentAnnRprtId")
        });
        action.setCallback(this,function(response){
            //alert(response.getState());
            if(response.getState() == 'SUCCESS'){
                component.set("v.showError",response.getReturnValue());
                if(triggerButton == 'backBtn' && response.getReturnValue() == false){
                    var event = $A.get("e.c:EFLRegulatedAnimalsTableEvent");
                    event.setParams({
                        annrepId : component.get("v.currentAnnRprtId"),
                        showParentTable : true
                    });
                    event.fire();
                    component.set("v.showExceptions",false);
                }
                }
        });
        $A.enqueueAction(action);
    }, */
    BacktoUsage : function(component, event, helper) {
        if(component.get("v.selectedPickListvalue")=="" || 
           component.get("v.selectedPickListvalue")== undefined){
            var txtMissingFieldsHdr = $A.get("$Label.c.EFLARUserEntryFormMissingValuesHDR");
            var txtMissingFieldsMsg = $A.get("$Label.c.EFLARUserEntryFormMissingValues");             
            helper.displayModalMessage(component,txtMissingFieldsHdr,txtMissingFieldsMsg); 
            //alert('Please indicate if you have any Exceptions to report in the field below.');
        }
        else{
            var eventBackPath = $A.get("e.c:EFLStageChangeInfoToPath");
            eventBackPath.setParams({
                completedStageName : "Stage 2"
            });
            eventBackPath.fire();
            var event2 = $A.get("e.c:EFLShowAnimalUsageTable");
            event2.setParams({
                showAnimalUsage : true,
                annualReportId : component.get("v.annualReportId")
            });
            event2.fire();
            component.set("v.showExceptions",false);  
        }
        
        /* var event = $A.get("e.c:EFLRegulatedAnimalsTableEvent");
       event.setParams({
                        annrepId : component.get("v.annRepId"),
                        showParentTable : true
                    });
                    event.fire();
                    component.set("v.showExceptions",false);*/
        
    },
    
    proceedToReview : function(component, event, helper) {
        if(component.get("v.selectedPickListvalue")=="" || 
           component.get("v.selectedPickListvalue")== undefined){
            //alert('Please indicate if you have any Exceptions to report in the field below.');
            var txtMissingFieldsHdr = $A.get("$Label.c.EFLARUserEntryFormMissingValuesHDR");
            var txtMissingFieldsMsg = $A.get("$Label.c.EFLARUserEntryFormMissingValues"); 
            helper.displayModalMessage(component,txtMissingFieldsHdr,txtMissingFieldsMsg); 
        }
        else{
            var eventVar = $A.get("e.c:EFLStageChangeInfoToPath");
            eventVar.setParams({
                completedStageName : "Stage 4"
            });
            eventVar.fire();
            var eventVarx = $A.get("e.c:EFLNavigateToSummary");
            eventVarx.setParams({
                showSummary : true,
                annualReportId : component.get("v.annualReportId")
            });
            eventVarx.fire();
            component.set("v.showExceptions",false);
        }
        
        
    },
    goToHome : function(component, event, helper) {
        var eventBackPath = $A.get("e.c:EFLStageChangeInfoToPath");
        eventBackPath.setParams({
            completedStageName : "Stage 0"
        });
        eventBackPath.fire();
        component.set("v.showExceptions",false);
    }
})