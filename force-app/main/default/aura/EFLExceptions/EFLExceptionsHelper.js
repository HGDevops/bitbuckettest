({
displayModalMessage : function(component, header, messageBody){
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : false,
            showConfirm : true,
            confirmText : 'OK',
            cssFile : 'EFLAR_ConfirmationDialog'/*,
            cancelCallback : component.get('c.cancelCallback') // optional
            */
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            true,
            'confirmModal', // css file
            null
        );
    }
})