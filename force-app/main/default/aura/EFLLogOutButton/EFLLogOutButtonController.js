({
    userMenuAction : function(component, event, helper) {
        var optionSelected = event.detail.menuItem.get("v.value");
        if (optionSelected == "logout"){
            window.location.replace("https://acrlbuild1-aphis-efile.cs32.force.com/ACISCommunity/secur/logout.jsp?retUrl=https://acrlbuild1-aphis-efile.cs32.force.com/ACIS/login");
        }
    }, 
    doInit :function(component, event, helper){
        var action = component.get("c.getCurrentUser");
        action.setCallback(this,function(response){
            var fullName = response.getReturnValue();
            var nameArray = fullName.split(" ");
            var firstName = nameArray[0];
            component.set("v.userFirstName",firstName);
            var initials = nameArray[0].substring(0,1).toUpperCase()+nameArray[1].substring(0,1);
            component.set("v.userInitials",initials);
        });
        $A.enqueueAction(action);
    },
    basicInfoPop : function(component, event, helper) {
        if(component.get("v.LogOutPopUp") == false)
            component.set("v.LogOutPopUp",true);
        else
            component.set("v.LogOutPopUp",false);
    },
    Cancel: function(component, event, helper) {
        // for Hide/Close Model,set the "LogOutPopUp" attribute to "Fasle"  
        component.set("v.LogOutPopUp", false);
    },
    LogOut: function(component, event, helper) {
                alert('test');

        var baseHostURL = window.location.host;
        var indexOfHyphen = baseHostURL.indexOf("-aphis");
        var currentEnvironment =  baseHostURL.subString(0,indexOfHyphen);
        // for Hide/Close Model,set the "LogOutPopUp" attribute to "Fasle"  
        component.set("v.LogOutPopUp",false); 
        var currentEnvironment = baseURLHost.substring(0,indexOfHyphen);        
        if(currentEnvironment !=null){
        	window.location.replace("https://"+currentEnvironment+"-aphis-efile.cs32.force.com/ACISCommunity/secur/logout.jsp?retUrl=https://"+currentEnvironment+"-aphis-efile.cs32.force.com/ACIS/login");
        }else{
        	window.location.replace("https://www.eauth.usda.gov/Logout/logoff.asp");
        }
    }
})