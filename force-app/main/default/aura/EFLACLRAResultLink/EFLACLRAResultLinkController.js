({
    trackClick : function(component, event, helper) {
        // callback to apex that the link was clicked.
        var actionType;
        var linkText = component.get('v.linkText');
        var path = component.get('v.pathway');
        var link = component.get('v.linkHREF');
        var actionType = component.get('v.actionType');
        var cNumber = component.get('v.customerNumber');
        var summaryId = component.get('v.summaryRecordId');
        
        try{
            // do callback.
            var action = component.get('c.logResultActivity');
            action.setParams({
                customerNumber : cNumber,
                summaryRecordId : summaryId,
                actionType : actionType,
                pathway : path
            });
            action.setCallback(this,function(res){
                console.log(res.getState());
                console.log(res.getError());
            });
            $A.enqueueAction(action);
            component.set('v.wasClicked',true);
            
            // nav to pathInfo.Pathway_Link__c
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                url : link
            });
            urlEvent.fire();
        }catch(err){
            console.log(err);
        }
    }
})