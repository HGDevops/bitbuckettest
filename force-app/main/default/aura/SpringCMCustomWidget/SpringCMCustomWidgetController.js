({
    doInit: function(cmp, event, helper){
        var recordId = cmp.get("v.recordId");
        var objectType = cmp.get("v.sObjectName");
        var output = {
            recordId: recordId,
            sObjectName: objectType
        };
        cmp.set("v.getParameters", JSON.stringify(output));
    }
})