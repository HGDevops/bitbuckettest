({
    doInit :function(component, event, helper){
        // check if custom values are present, if so parse them into a list.
        var file = component.get('v.file');
        
        if(file.customValues != null){
            try{
                var customVals = [];
                var headers = component.get('v.headerKeys');
                for(var i = 0; i < headers.length; i++){
                    var header = headers[i];
                    
                    if(file.customValues[header]){
                        customVals.push(file.customValues[header]);
                    }else{
                        customVals.push('');
                    }
                }
                
                if(customVals.length > 0){
                    component.set('v.customValues',customVals);
                }
            }catch(err){
                console.log(err);
            }
        }
    },previewFile :function(component, event, helper){
        // navigate to download page.
        ///sfc/servlet.shepherd/document/download/{ContentDocumentId}?operationContext=S1
        window.open(component.get('v.file.downloadLink'), '_blank');
        /*var cdId = component.get('v.file.previewId');
        $A.get('e.lightning:openFiles').fire({
            recordIds: [cdId]
        });*/
    },
    showDeletePopup : function(component, event, helper){
        //console.log('showDeletePopup');
        helper.showDeleteConfirmation(component);
    },
    doDeleteFile : function(component, event, helper){
        //console.log('doDeleteFile called');
        helper.doDelete(component);
    },
    cancelCallback : function(component, event, helper){
        helper.cancelCallback(component);
    },
})