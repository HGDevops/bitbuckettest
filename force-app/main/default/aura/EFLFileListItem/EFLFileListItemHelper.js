({
    // NOTE: this component extends EFLLightningBase and makes use of the helper methods defined therein.
    
    showDeleteConfirmation : function(component){
        var self = this;
        var isCommunity = component.get('v.pageData.isCommunity');
        var params = {
            confirmationMessage : 'Are you sure you want to delete this record?',
            cancelText : 'Cancel',
            confirmText : 'Delete',
            confirmCallback : function(){self.doDelete(component);},
            cssFile : 'EFLAR_ConfirmationDialog',
            isCommunity : isCommunity
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            'Delete Record(s)',
            true,
            'confirmModal', // css file
            null // close callback
        );
    },
	doDelete : function(component){
        //console.log('doDelete called');
        //this.closeModal(component);
        var fId = component.get('v.file.id');
        var action = component.get('c.deleteFile');
        var doDelete = component.get('v.permaDelete');
        action.setParams({
            fileId : fId,
            doDelete : doDelete
        });
        var self = this;
        action.setCallback(this,function(response){
            // tell the parent that a file was deleted.
            var appEvent = $A.get("e.c:EFLFileDeletedEvent");
            appEvent.fire();
        });
        $A.enqueueAction(action);
    },
    cancelCallback : function(component){
        this.closeModal(component);
    }
})