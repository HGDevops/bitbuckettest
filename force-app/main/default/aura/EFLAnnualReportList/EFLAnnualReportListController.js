({
    toggleExpanded : function(component, event, helper) {
        var isExpanded = component.get('v.isExpanded');
        isExpanded = !isExpanded;
        component.set('v.isExpanded',isExpanded);
    },
    goToReportPage : function(component, event, helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            recordId: event.getSource().get("v.name")
        });
        navEvt.fire();
    }
})