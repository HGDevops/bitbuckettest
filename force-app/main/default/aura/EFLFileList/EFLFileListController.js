({
    doInit : function(component, event, helper) {
        helper.refresh(component, event, helper);
    },
            
    refreshFileList : function(component, event, helper){
        helper.refresh(component, event, helper);
    },
    
    handleUploadFinished : function(component, event, helper){
        var uploadedFiles = event.getParam("files");
        component.set('v.isLoading',true);
        var action = component.get('c.setupNewFile');
        
        action.setParams({
            cds : JSON.stringify(uploadedFiles),
            parentId : component.get('v.parentIds')[0],
            fileDetails : component.get('v.usageKey')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var res = response.getReturnValue();
            if (state === "SUCCESS") {
                // tell any open file viewer to refresh.
                var event = $A.get("e.c:EFLFileUploadedEvent");
                event.fire();
            }else{
                var errors = response.getError();
                var message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
            }
        });
        //console.log('calling setupNewFile: ' , component.get('v.usageKey'));
        $A.enqueueAction(action);
    },
    showCustomAddNew : function(component,event,helper){
        // a custom add new component has been identified in config.  Show that component in a modal.
        helper.showAddNew(component);
    }
})