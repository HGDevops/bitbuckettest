({
    refresh : function(component, event, helper){
        component.set('v.isLoading',true);
        //('usage key: ' , component.get('v.usageKey'));
        var pId = component.get('v.recordId');
        var parentIds = component.get('v.parentIds');
        if(!parentIds || parentIds == null || parentIds.length == 0){
            if(pId && pId != null){
                parentIds.push(pId);
            }
        }
        if(pId == null){
            // if recordId is null we MUST hide the upload button since no parent is known.
            component.set('v.allowUpload',false);
        }
        if(parentIds && parentIds.length > 0){
            helper.retrieveFiles(component);
        }
    },
    
    retrieveFiles : function(component) {
        try{
            var pIds = component.get('v.parentIds');
            //console.log('pIds', pIds);
            var action = component.get('c.getPageData');
            var filterLogic = component.get('v.usageKey');
            //console.log('filterLogic', filterLogic);
            action.setParams({
                parentIds : pIds,
                filterLogic : filterLogic
            });
            action.setCallback(this, function(response){
                var res = response.getReturnValue();
                component.set('v.pageData',res);
                component.set('v.fixedTableWidth',component.get('v.pageData.config.Fixed_Table_Width__c'));
                component.set('v.isLoading',false);
            });
            $A.enqueueAction(action);
        }catch(err){
            console.log(err);
        }
    },
    showAddNew : function(component){
        // note: this component extends EFLLightningBase so we can reuse modal features.
        var self = this;
        var params = {
            parentId : component.get('v.recordId'),
            usageKey : component.get('v.usageKey')
        };
        
        var header = $A.get('$Label.c.EFLAR_Add_File_Header');
        if(component.get('v.pageData.config.Allow_Multiple_Files__c') == true){
            header = $A.get('$Label.c.EFLAR_Add_Files_Header');
        }
        
        var newComponent = 'c:' + component.get('v.pageData.config.New_File_Modal_Component__c');
        
        self.showModalComponent(
            component,
            newComponent,
            params,
            header,
            true,
            'confirmModal',
            function(){self.retrieveFiles(component);}
        );
    }
})