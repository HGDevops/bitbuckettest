({
	initiateComp : function(component, event, helper) {
		component.set("v.showSummary",event.getParam("showSummary"));
        component.set("v.annualReportId",event.getParam("annualReportId"));
	},
    backToHome : function(component, event, helper) {
		component.set("v.showSummary",false);
	}
})