//EFLRegnHQDetailsController
({    
    fetchHQData : function(cmp, event, helper){
        ////console.log('fetchHQData');
        var spinner = cmp.find("loading");
        $A.util.removeClass(spinner,"slds-hide");
        
        var regtnId=event.getParam("regId");
        //cmp.set("v.AnnualRepId",event.getParam("annRepId"));
        var action=cmp.get("c.getAccountHQ");
        var annualReportId = cmp.get('v.recordId');
        //console.log('annualReportId',annualReportId);
        action.setParams({
            annualReportId:cmp.get('v.recordId')
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            //console.log('v.results',response.getReturnValue());
            if(state==='SUCCESS'){
                cmp.set("v.results",response.getReturnValue());   
                cmp.set("v.showHQ",true); 
                var loading = cmp.find("loading");
                $A.util.addClass(loading,"slds-hide");
                if($A.util.isUndefinedOrNull(cmp.get('v.recordId'))){               
                    cmp.set("v.showHQ", true);                    
                }
            } else {
                alert(state.ERROR);
            }
            var spinner = cmp.find("loading");
            $A.util.addClass(spinner,"slds-hide");
        });   
        if(cmp.get('v.recordId') != undefined) {
            $A.enqueueAction(action);
        }
    },
    homePage : function(component, event, helper) {
        component.set("v.showHQ",event.getParam("showBody"));
    },
    goToHome : function(component, event, helper) {
        var eventVar = $A.get("e.c:EFLHomePage");
        eventVar.setParams({
            showHomePage : true,
            showForm : false
        });
        eventVar.fire();
    },
    basicInfoPop : function(component, event, helper) {
        if(component.get("v.showBasicInfoPop") == false)
            component.set("v.showBasicInfoPop",true);
        else
            component.set("v.showBasicInfoPop",false);
    },
    showRegAnimals : function(component, event, helper) {
        var eventVar = $A.get("e.c:EFLStageChangeInfoToPath");
        eventVar.setParams({
            completedStageName : "Stage 1"
        });
        eventVar.fire();
        var eventVar2 = $A.get("e.c:EFLShowRegulatedAnimalsSelection");
        eventVar2.setParams({
            showAnimalSelectionPage : true,
            annualReportId : component.get("v.AnnualRepId")
        });
        eventVar2.fire();
        component.set("v.showHQ",false);
    }
})