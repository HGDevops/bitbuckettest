({
    getNextQuestion : function(component, selection){
        // check the existing MDT override conditions to see if the selection has a value
        var selectedName = selection.Name;
        var applicableOverride;
        try{
            for(var c = 0; c < component.get('v.conditionalOverrides').length; c++){
                var override = component.get('v.conditionalOverrides')[c];
                if(override.Selected_Answer__c == selectedName){
                    // this answer involves an override.
                    applicableOverride = override;
                    break;
                }
            }
            
            if(applicableOverride && applicableOverride != null){
                var idToReturn;
                // check for any prerequisites based on the 'qualified for' conditions.
                var qf = applicableOverride.Qualified_For__c;
                // check the precondition, if one exists.
                if(!qf || qf == null || qf == ''){
                    idToReturn = applicableOverride.Next_Wizard_Question__c; // return it without checking conditions.
                }else{
                    // loop through all the applicable pathways & see if they're currently qualified.
                    if(this.getQualifiedPathways(component).includes(qf)){
                        
                        // we now have the correct override value to use.
                        // loop through all the questions and return the Id of the question that matches the name
                        for(var q in component.get("v.allQuestionSelections")){
                            var question  = component.get("v.allQuestionSelections")[q];
                            if(question.Question_Number__c == applicableOverride.Next_Wizard_Question__c){
                                idToReturn = question.Id; 
                            }
                        }
                    }
                }
                if(applicableOverride.Overrides_Pathway__c && applicableOverride.Overrides_Pathway__c != null){
                    try{
                        this.overrideQualifiedPathway(component, applicableOverride.Overrides_Pathway__c, 'Qualified', 'Voided');
                    }catch(err){
                        console.log(err);
                    }
                }
                return idToReturn;
            }
            // return the standard next question if no override.
            return selection.Wizard_Next_Question__c;
        }catch(err){
            console.log(err);
            return selection.Wizard_Next_Question__c;
        }
    },
    undoOverrides : function(component, question){
        // get the child selections that may have caused an override:
        var overrideSelections = [];
        for(var s in question.Wizard_Questionnaire__r){
            overrideSelections.push(question.Wizard_Questionnaire__r[s].Name);
        }
        // we now have list of the possible selections.
        // see if any overrides exist for the question that was backed out of.
        var questionId = question.Question_Number__c;
        var applicableOverride;
        
        try{
            for(var c in component.get('v.conditionalOverrides')){ // for each MDT
                var override = component.get('v.conditionalOverrides')[c];
                if(overrideSelections.includes(override.Selected_Answer__c)){ // if the MDT's selected answer matches an answer from the question we just left
                    // this answer involves an override.
                    applicableOverride = override; // set the MDT in scope
                    break;
                }
            }
            
            var allTracking = component.get("v.allTrackingInstances");
            if(applicableOverride && applicableOverride != null){ // if we have an override to process
                if(applicableOverride.Overrides_Pathway__c && applicableOverride.Overrides_Pathway__c != null){ // if there was a pathway overridden
                    try{
                        // set the previously voided pathway to Qualified
                        allTracking = this.overrideQualifiedPathway(component, applicableOverride.Overrides_Pathway__c, 'Voided', 'Qualified'); 
                    }catch(err){
                        console.log(err);
                    }
                }
            }
            return allTracking;
        }catch(err){
            console.log(err);
        }
    },
    getQualifiedPathways : function(component){
        var allTrackingInstances = component.get("v.allTrackingInstances");
        var QlfdPthys ='';
        var qualifiedFor = [];
        for (var k=0;k<allTrackingInstances.length;k++){
            if((allTrackingInstances[k].Pathway_Status__c =='Qualified' || allTrackingInstances[k].Pathway_Status__c =='Contact AC')
               && allTrackingInstances[k].Wizard_Question__c==null
               && allTrackingInstances[k].Deleted__c == false){
                qualifiedFor.push(allTrackingInstances[k].Pathway__c);
            }
        }
        return qualifiedFor;
    },
    overrideQualifiedPathway : function(component, pathway, oldStatus, status){
        try{
            var allTrackingInstances = component.get("v.allTrackingInstances");
            var toUpdate = [];
            for(var i = 0; i < allTrackingInstances.length; i++){ // for each tracking instance in memory
                var trackingInstance = allTrackingInstances[i];
                if(allTrackingInstances[i].Pathway__c == pathway && allTrackingInstances[i].Pathway_Status__c == oldStatus){ // if it was the end of a pathway
                    // update the status of the instance to the new status passed.
                    allTrackingInstances[i].Pathway_Status__c  = status;
                }
                //toUpdate.push(trackingInstance);
            }
            return allTrackingInstances;
            //component.set('v.allTrackingInstances', toUpdate);
        }catch(err){
            console.log(err);
        }
    },
    initKeywords : function(component){
        try{
            var theText = component.get('v.questionSelections.Wizard_Question_Rich_Text__c');
            var keywords = component.get('v.keywords');
            var newQuestion;
            for(var k = 0 ; k < keywords.length; k++){
                var kw = keywords[k];
                if(theText.includes(kw.Keyword__c)){
                    var tokens = theText.split(kw.Keyword__c);
                    var newString = '<span style="color:red !important;cursor:pointer;" class="clickText" data-id="' + kw.Id + '">' + kw.Keyword__c + '</span>'
                    newQuestion = tokens.join(newString);
                }
            }
            component.set('v.questionText',newQuestion);
            component.set('v.clickhandler',$A.getCallback(function(evt){
            }));
            
        }catch(err){
            console.log(err);
        }
    },
    setExitPageOnSummary : function(component, currentPathway){
        var action = component.get('c.setEndPointOnSummary');
        action.setParams({
            summaryRecordId : component.get('v.summaryRecordId'),
            lastLocation : currentPathway
        });
        action.setCallback(this, function(result){
        });
        $A.enqueueAction(action);
    },
    updateSummaryLastQuestion : function(component, questionId){
        var action = component.get('c.updateSummaryLastQuestion');
        action.setParams({
            summaryRecordId : component.get('v.summaryRecordId'),
            lastQuestionId : questionId
        });
        action.setCallback(this, function(result){
        	if(result.getState() === 'SUCCESS'){
            }
            if(result.getState() != 'SUCCESS'){
            }
        });
        $A.enqueueAction(action);
    },
    saveApplicationRequest : function(component, trackingInstanceKey){
        var action = component.get('c.upsertTrackingrec');
        var arr = [trackingInstanceKey];
        action.setParams({
            trcRec : arr
        });
        action.setCallback(this, function(result){
        	if(result.getState() === 'SUCCESS'){
            }
            if(result.getState() != 'SUCCESS'){
            }
        });
        $A.enqueueAction(action);
    }
})