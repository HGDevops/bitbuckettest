({
    doInit : function(component, event, helper) {
        var rightNow = new Date().getTime();
       // var rightNowString = rightNow.toString();
        //var lastTen = rightNowString.substr(rightNowString.length - 10);
        //rightNow = parseInt(lastTen);
        component.set('v.thisQuestionStartTime',rightNow);
        var device = $A.get("$Browser.formFactor");
        if (device=='PHONE'){// && window.matchMedia("(orientation: portrait)").matches){
            component.set("v.isMobile",true);
        }
        //Fetch All Question and selections data into the WQ instance  and pass it to CMP
        var action = component.get("c.fetchAllQuestionSelections");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==='SUCCESS'){
                component.set("v.allQuestionSelections",response.getReturnValue());
                //get first intro question w/selections and pass it to CMP
                for(var i=0;i<response.getReturnValue().length;i++){
                    try{
                        var q = response.getReturnValue()[i];
                        if(q.AC_LRA_Pathway__c=='Introductory Question' && q.Type__c=='START'){
                            component.set("v.questionSelections",q);
                            component.set("v.loadStatus", true);
                            component.set("v.isMultiSelect", true);
                            helper.updateSummaryLastQuestion(component, q.Id);
                        }
                    }catch(err){
                        console.log(err);
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
        
        //Get the record type for Application request (tracking) records so the tracking records can be inserted w/RT       
        var fetchTrackingRT = component.get("c.fetchTrackingRT");
        //fetchTrackingRT.setParams({});
        fetchTrackingRT.setCallback(this, function(response){
            var state= response.getState();  
            if(state==='SUCCESS'){
                component.set("v.trackingRT",response.getReturnValue());
                //console.log('ar rtid ' , response.getReturnValue());
            }
        });
        $A.enqueueAction(fetchTrackingRT);        
        //Get the Pathway names from CMD in the sequence they need to be processed to use in Next button handler
        var pathwayCMD = component.get("c.fetchPathwayCMD");
        pathwayCMD.setCallback(this, function(response){
            var state= response.getState();  
            if(state==='SUCCESS'){
                component.set("v.allPathways",response.getReturnValue());
            }
        });
        
        $A.enqueueAction(pathwayCMD);
        
        var getOverrides = component.get('c.getConditionalOverrides');
        getOverrides.setCallback(this, function(response){
            var state =  response.getState();
            if(state === 'SUCCESS'){
                component.set('v.conditionalOverrides', response.getReturnValue());
                //console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(getOverrides);
    },
    // handle changes to checkboxes or radio buttons with  validations - make Next button clickable only when a choice is made
    selectionChanged:function(component, event,helper){
        //console.log('questionSelections ' , component.get('v.questionSelections'));
        var availableSelections = component.get("v.questionSelections.Wizard_Questionnaire__r");
        var optionType = component.get("v.questionSelections").Type_of_Options_LRA__c;
        //console.log('optionType ' , optionType);
        var anOptionSelected = false;
        var selectedOptionId;
        var noneOptionSelected; //None of the above was selected
        var QstnId_W_O_NoneOption = null; //Intro Qstn Id Without a None of the above Option
        if(optionType == 'Multi Select'){
            for (var i=0;i<availableSelections.length;i++){            
                QstnId_W_O_NoneOption = availableSelections[i].Wizard_Questionnaire__c;
                var option=document.getElementById(availableSelections[i].Id);
                if(option.checked==true){                 
                    if(availableSelections[i].EFLACLRAValue__c == 'None of the above'){                    
                        noneOptionSelected=true; 
                    }
                    anOptionSelected = true;
                    selectedOptionId = availableSelections[i].id;
                }
                if(availableSelections[i].EFLACLRAValue__c == 'None of the above' || availableSelections[i].EFLACLRAValue__c == 'No'){
                    QstnId_W_O_NoneOption = null;
                }
            }
            
            if (noneOptionSelected){ //disable all other options 
                for (var j=0;j<availableSelections.length;j++){
                    if (availableSelections[j].EFLACLRAValue__c != 'None of the above'){                    
                        document.getElementById(availableSelections[j].Id).disabled = true;                
                    }
                }
            }else{// disable none if other option selected
                for(var k=0;k<availableSelections.length;k++){
                    if (availableSelections[k].EFLACLRAValue__c == 'None of the above'){                    
                        document.getElementById(availableSelections[k].Id).disabled = true;                
                    }
                }
            }
            // on an Intro question that has no 'None of the above', disable other option if one is selected
            if(QstnId_W_O_NoneOption !=null){
                for(var p=0;p<availableSelections.length;p++){
                    var option = document.getElementById(availableSelections[p].Id);
                    if(option.checked!=true){   
                        document.getElementById(availableSelections[p].Id).disabled = true;  
                    }            
                }
            } 
            //Enable Next button only when at least one option is selected
            if(anOptionSelected){
                component.set("v.disableNextBtn","false");
            }else{ //enable all options when selected option is unchecked
                for(var l=0;l<availableSelections.length;l++){
                    document.getElementById(availableSelections[l].Id).disabled = false;                
                }
                component.set("v.disableNextBtn","true");
            }
        }
        else if(optionType == 'Single Select'){
            for (var i=0;i<availableSelections.length;i++){            
                var option=document.getElementById(availableSelections[i].Id);
                if(option.checked==true){                 
                    component.set("v.disableNextBtn",false);
                }
            }
        }        
    },
    
    handleNextButton: function (component, event, helper){
        try{
            var rightNow = new Date().getTime();
            //var rightNowString = rightNow.toString();
            //var lastTen = rightNowString.substr(rightNowString.length - 10);
            //rightNow = parseInt(lastTen);
            //component.set("v.loadStatus", false);
            component.set("v.disableNextBtn",true);
            var trackingRT = component.get("v.trackingRT");
            var prevQstn = component.get("v.questionSelections");     
            var customerNumber = component.get("v.customerNumber");
            var newTrackingInstances  = [];
            var currentTrackingInstances = [];
            var availableSelections = component.get("v.questionSelections.Wizard_Questionnaire__r");                
            var selectedOptions=null; //holds selected selections.Name like IQ2_A1, IQ2_A2
            var nextQstn = null;
            var optionType = component.get("v.questionSelections").Type_of_Options_LRA__c;
            var pathwayQstnResult; //pathway name from the selection of final question of a pathway 
            var pathwayQstnStatus; // Qualified or Not Qualified from selection    
            var pathwayStartTime;
            var pathwayEndTime;
            var wizardSelection;
            var trackingInstPthwyCmpl ={'sobjectType':'Application_Request__c'};            
            //get the information from the selected Wizard selection record (IQ and PQ).
            for (var a=0; a<availableSelections.length; a++){
                var option = document.getElementById(availableSelections[a].Id);
                if (option.checked == true){ //selected option
                    wizardSelection = availableSelections[a]; // set this here to use below.
                    nextQstn = helper.getNextQuestion(component, availableSelections[a]);
                    //need below two for pathway questions to check when the pathway ends, captures the outcome of the pathway Q&A
                    pathwayQstnResult = availableSelections[a].Pathway_Questions_Result__c; 
                    pathwayQstnStatus = availableSelections[a].Pathway_Questions_Status__c;       
                }
            }
            //console.log('wizardSelection',wizardSelection.Id);
            var trackingStatus = "Selected";        
            //insert question records and then the pathway records into the tracking object
            var existingTrackingInstances = component.get("v.allTrackingInstances");
            var countInst = existingTrackingInstances.length + 1;         
            
            // Create tracking instances based on the information from the WQ, WS 
            for (var i=0; i<availableSelections.length; i++){
                var option = document.getElementById(availableSelections[i].Id);
                if (option.checked == true){
                    //question records
                    var trackingInstanceq = {'sobjectType':'Application_Request__c'};
                    trackingInstanceq.External_Migration_Id__c = customerNumber+'-'+countInst;
                    trackingInstanceq.RecordTypeId = trackingRT;
                    trackingInstanceq.EFL_AC_LRA_Summary__c = component.get("v.summaryRecordId");
                    trackingInstanceq.Time_Started__c = component.get('v.thisQuestionStartTime');
                    trackingInstanceq.Time_Entered__c = rightNow;
                    if(availableSelections[i].Wizard_Questionnaire__r.AC_LRA_Pathway__c=='Introductory Question' ){
                        //console.log('making pathway tracking instance');
                        trackingInstanceq.Type__c = 'Pathway'; // for IQ - pathways
                        trackingInstanceq.Intro_Question_Status__c = trackingStatus;
                        trackingInstanceq.External_Migration_Id__c = customerNumber+'-' + availableSelections[i].Intro_Question_Result__c;                    
                        if (availableSelections[i].Intro_Question_Result__c.includes('Needed')){ //skipping 'No License Needed' and 'No Registration Needed'
                            trackingInstanceq.Intro_Question_Status__c='No Need to Process';
                        }
                    }else{
                        trackingInstanceq.Type__c = "Question"; // for PQ 
                    }
                    trackingInstanceq.Customer_Number__c = customerNumber;  
                    trackingInstanceq.Pathway__c = availableSelections[i].Intro_Question_Result__c;
                    trackingInstanceq.Deleted__c=false;
                    trackingInstanceq.Next_Question__c=nextQstn; 
                    trackingInstanceq.Wizard_Selection__c= availableSelections[i].Id;
                    trackingInstanceq.Wizard_Question__c = availableSelections[i].Wizard_Questionnaire__c;
                    newTrackingInstances.push(trackingInstanceq);
                }
            }
            
            component.set("v.allTrackingInstances",existingTrackingInstances.concat(newTrackingInstances)); // append trackinginstances w/question records
            
            //Type cast json object to SF object instance(s)
            for(var x=0;x<component.get("v.allTrackingInstances").length;x++){
                component.get("v.allTrackingInstances")[x].sobjectType = 'Application_Request__c'; // setting object type to data
            }
            
            var trackingInstancesToProcess = component.get("v.allTrackingInstances");
            var pathwaysEnded = true; // checking if all pathways ended. If so the tracking records will be insrted
            var allPathways = component.get("v.allPathways");
            var currentPathway = component.get("v.currentPathway");
            var nextPathwayFound = false;
            var newInstances = [];
            var trackingInstPthwyCmplList = [];
            
            // This block runs at the end of a pathway only, and creates a tracking record.
            if(nextQstn == null || nextQstn == ''){ // && pathwayQstnResult != null
                // handle creating a pathway qualification instance here.
                if(pathwayQstnResult && pathwayQstnResult != null){
                    //console.log('adding qualification instance with : ' , wizardSelection);
                    // make the qualification record.
                    var trackingInstancefnlq ={'sobjectType':'Application_Request__c'};
                    trackingInstancefnlq.Customer_Number__c = customerNumber; 
                    trackingInstancefnlq.Pathway__c = pathwayQstnResult;
                    //console.log('pathwayQstnResult:: ' , pathwayQstnResult);
                    trackingInstancefnlq.Deleted__c = false;              
                    trackingInstancefnlq.Pathway_Status__c = pathwayQstnStatus;
                    trackingInstancefnlq.External_Migration_Id__c = customerNumber+'-PP'+countInst;
                    trackingInstancefnlq.EFL_AC_LRA_Summary__c = component.get("v.summaryRecordId");
                    trackingInstancefnlq.Time_Started__c = component.get('v.thisQuestionStartTime');
                    trackingInstancefnlq.Time_Entered__c = rightNow;
                    trackingInstancefnlq.Wizard_Selection__c = wizardSelection.Id; // added for reporting purposes
                    newInstances.push(trackingInstancefnlq);
                    //console.log('trackingInstancefnlq', trackingInstancefnlq);
                    // update the existing entry record to procesed.
                    for(var ab =0; ab< trackingInstancesToProcess.length; ab++){
                        if(trackingInstancesToProcess[ab].Type__c == 'Pathway' // denotes an existing pathway record (from IQ)
                           && trackingInstancesToProcess[ab].Deleted__c == false
                           && trackingInstancesToProcess[ab].Intro_Question_Status__c != 'Processed'
                           && trackingInstancesToProcess[ab].Pathway__c == currentPathway){
                            //create new instance for listing the pathway name, needed as Pathway started can be a combination
                            //but the quailified might be just one of them (like qualified for License A - of AB pthwy) 
                            trackingInstancesToProcess[ab].Intro_Question_Status__c = 'Processed';
                            //countInst++;
                            break; // end this loop for this pathway
                        }
                    }
                    //component.set("v.allTrackingInstances",existingTrackingInstances.concat(trackingInstPthwyCmplList)); // append trackinginstances w/question records
                }
                
                // if new instances were added above (qualification instances), push them into the list.
                if(newInstances.length > 0){
                    for(var n in newInstances){
                        trackingInstancesToProcess.push(newInstances[n]);
                    }
                    component.set("v.allTrackingInstances",trackingInstancesToProcess);
                    //console.log('after pathway instances added, ', trackingInstancesToProcess);
                }
                
                // Get the first question of the next pathway from the click of next button on the final question of 
                // last pathway
                trackingInstancesToProcess = component.get("v.allTrackingInstances");
                
                for(var index in allPathways){
                    //allPathways contains the pathway names in the order they need to be processed (from CMD)
                    var nextPathwayToProcess = allPathways[index];  
                    
                    for(var ab =0;ab< trackingInstancesToProcess.length; ab++){
                        //if the selection has pathway question status and pathway question Result
                        //that is the end of that pathway need to end loop for this pathway and go to the next
                        if(trackingInstancesToProcess[ab].Pathway__c == nextPathwayToProcess
                           && trackingInstancesToProcess[ab].Deleted__c == false
                           && trackingInstancesToProcess[ab].Intro_Question_Status__c == 'Selected'){   
                            component.set("v.currentPathway",nextPathwayToProcess);
                            // call back to set the summary record's "exit page" to the current pathway.
                    		helper.setExitPageOnSummary(component, nextPathwayToProcess);
                            
                            //loop thru allQuestionSelections to get first and subsequent questions for this pathway
                            var allQs = component.get("v.allQuestionSelections"); // all Q&S instances from WQ and WS
                            //console.log('allQs',allQs);
                            pathwaysEnded=false; //since a 'Selected' pathway is found
                            //sending questionSelections to cmp - the instance of first Q of next pthwy
                            for (var e = 0; e < allQs.length; e++){
                                //Start of a pthwy
                                if(allQs[e].Type__c == 'START' && allQs[e].AC_LRA_Pathway__c == nextPathwayToProcess){
                                    //pathwayStartTime = new Date().getTime();
                                    //component.set("v.pathwayStartTime",pathwayStartTime);
                                    //console.log('Start of ', nextPathwayToProcess);
                                    component.set("v.questionSelections", allQs[e]);
                                    component.set("v.isMultiSelect", false); 
                                    nextPathwayFound= true;
                                    helper.updateSummaryLastQuestion(component, allQs[e].Id);
                                    break;
                                }
                            }
                            if(nextPathwayFound){ // for(var ab =0;ab< trackingInstancesToProcess.length; ab++)
                                break;
                            }
                        }
                        if(nextPathwayFound){ // for(var ab =0;ab< trackingInstancesToProcess.length; ab++)
                            break;
                        }
                    }
                    if(nextPathwayFound){ //for(var index in allPathways)
                        break;
                    }
                }
                
                //}
                component.set("v.disableNextBtn",false);
                if(nextPathwayFound == false){
                    // insert an instance to Summary object with time taken to complete the application
                    for(var i=0;i< trackingInstancesToProcess.length; i++){
                        //if the selection has pathway question status and pathway question Result
                        //that is the end of that pathway need to end loop for this pathway and go to the next 
                        if(pathwayQstnResult != null){
                            //trackingInstancesToProcess[i].Pathway_Status__c = pathwayQstnStatus;
                            trackingInstancesToProcess[i].Intro_Question_Status__c = 'Processed';
                            break; // end this loop for this pathway
                        }  
                    }
                    
                    //console.log('final DML');
                    /******DML********/
                    // insert all tracking record instances    
                    var pthwysQlfd='';                 
                    var action = component.get("c.upsertTrackingrec");
                    var allTrackingInstances = component.get("v.allTrackingInstances");
                    //console.log('allTrackingInstances: ' , allTrackingInstances);
                    $A.log('allTrackingInstances: ' , allTrackingInstances);
                    action.setParams({
                        trcRec: allTrackingInstances
                    });
                    action.setCallback(this,function(response){
                        var state = response.getState();                        
                        if(state==='SUCCESS'){
                            var paths = [];
                            for (var k=0;k<allTrackingInstances.length;k++){
                                allTrackingInstances[k].EFL_AC_LRA_Summary__c = component.get("v.summaryRecordId");
                                if((allTrackingInstances[k].Pathway_Status__c =='Qualified' ||  allTrackingInstances[k].Pathway_Status__c =='Contact AC')
                                   && allTrackingInstances[k].Wizard_Question__c==null
                                   && allTrackingInstances[k].Deleted__c == false){
                                    paths.push({pathway : allTrackingInstances[k].Pathway__c, status : allTrackingInstances[k].Pathway_Status__c});
                                }
                            }
                            
                            var evt = $A.get("e.c:EFLACLRAPathwaysComplete");
                            evt.setParams({
                                pathwayList : paths
                            });
                            evt.fire();
                        }else{
                            console.log('response: ' , response.getError());
                        }
                    });
                    $A.enqueueAction(action);   
                                        
                }
                
            }else{   // of (nextQstn == null) if statement; for the subsequent question within  the same pathway
                // update the summary record
                helper.updateSummaryLastQuestion(component, nextQstn);
                
                var allQA = component.get("v.allQuestionSelections");
                for(var i=0;i<allQA.length;i++){
                    if(allQA[i].Id != null){
                        if(allQA[i].Id == nextQstn){
                            component.set("v.questionSelections",allQA[i]);
                            //helper.initKeywords(component);
                            //try{
                                //console.log('Question #: ',component.get("v.questionSelections").Question_Number__c);
                            //}catch(err){
                                // no worries.
                            //}
                            if(allQA[i].Type_of_Options_LRA__c=='Multi Select'){   
                                component.set("v.isMultiSelect", true);
                            }
                            else if(allQA[i].Type_of_Options_LRA__c=='Single Select'){
                                component.set("v.isMultiSelect", false);
                            }
                        }
                    } 
                }
            }
            var showQuestionComp = component.find("showQuestion");
            showQuestionComp.refreshQuestion();
        }catch(err){
            console.log(err);
        }
        component.set('v.thisQuestionStartTime', rightNow);
        component.set("v.disableNextBtn","true");
        //component.set("v.loadStatus", true);
        component.set("v.countInst", countInst);
        //console.log(component.get("v.allTrackingInstances"));
    },
    
    handleBackButton: function (component, event, helper){
        try{
            component.set("v.disableNextBtn","true");
            // possible future updates using helper methods for common logic.
            // helper.undoOverrides(component);
            // helper.addTrackingInstance(component);
            // helper.setCurrentQuestion(component);
            
            var questionBackedOutOf = component.get("v.questionSelections"); // the question on screen when back was clicked.
            var questionBackedInto; // the question that should now show up after the back is completed.
            
            //Get all tracking instance and loop till the end to get the last question, set Deleted =true for it. 
            var allTrackingInstances = component.get("v.allTrackingInstances"); //records that gets the updates, like deleted__c
            var allTrackingInstancesb = component.get("v.allTrackingInstances"); //original stays as is
            var wqId = '';
            var backedWqId='';
            var trcRecExtId ='';
            var duplicateOnQuestionTrackingInstance
            
            // first, get the previous question that now needs to be displayed.
            var currQstnIndex;
            for(var a=0; a < allTrackingInstances.length; a++){
                if(allTrackingInstances[a].Deleted__c==false && allTrackingInstances[a].Wizard_Question__c!=null){
                    wqId = allTrackingInstances[a].Wizard_Question__c;
                    currQstnIndex = a; // This will end up with the last index of the question answered.
                }
            }
            
            // now, soft-delete the activity that represented the new question that will now be displayed.
            if(currQstnIndex != null){
                allTrackingInstances[currQstnIndex].Deleted__c = true;
            }
            
            var allQuestionSelections =  component.get("v.allQuestionSelections");
            if(wqId != '' && wqId != null){
                for (var k=0;k< allQuestionSelections.length;k++){
                    if(allQuestionSelections[k].Id == wqId ){
                        questionBackedInto = allQuestionSelections[k];
                        break;
                    }
                }
            }
            
            // if there is a question to display, get its instance w/selections and pass to CMP
            if(questionBackedInto && questionBackedInto != null){
                helper.updateSummaryLastQuestion(component, questionBackedInto.Id);
                var pthwyTrcExtId ='';
                questionBackedInto = allQuestionSelections[k];
                component.set("v.currentPathway", questionBackedInto.AC_LRA_Pathway__c);
                component.set("v.questionSelections", questionBackedInto);
                
                // switch the checkboxes/radio buttons based on Type_of_Options_LRA__c
                if(questionBackedInto.Type_of_Options_LRA__c=='Multi Select'){   
                    component.set("v.isMultiSelect", true);
                }else{
                    component.set("v.isMultiSelect", false);
                }
                
                // check to see if we're jumping from a pathway into a previous one...
                if(questionBackedInto.Wizard_Question__c == null){
                    // this represents a question at the end of the pathway
                    // Find the tracking record that said qualified (if existing) and undo it.
                    //create new instance for listing the pathway name, needed as Pathway started can be a combination
                    //but the quailified might be just one of them (like qualified for License A - of AB pthwy)
                    var availableSelections = questionBackedInto.Wizard_Questionnaire__r;
                    //console.log('available Selections: ' , availableSelections);
                    
                    var causedOutcome = []; // if previous question caused Qualified or Not Qualified regardless of answer...
                    for(var o = 0; o < availableSelections.length; o++){
                        if(availableSelections[o].Intro_Question_Result__c != null){
                        	causedOutcome.push(availableSelections[o].Pathway_Questions_Result__c);
                        }
                    }
                    //console.log('causedOutcome',causedOutcome);
                    
                    for(var a=0;a<allTrackingInstances.length;a++){
                        if(allTrackingInstances[a].Pathway_Status__c != null
                           && (questionBackedInto.AC_LRA_Pathway__c.includes(allTrackingInstances[a].Pathway__c)
                              || causedOutcome.includes(allTrackingInstances[a].Pathway__c))
                           ){
                            
                            // override the status so it's ignored in the qualification logic.
                            allTrackingInstances[a].Deleted__c = true;
                            allTrackingInstances[a].Pathway_Status__c = null;
                        }
                        if(allTrackingInstances[a].Type__c == 'Pathway' 
                           && allTrackingInstances[a].Deleted__c == false
                           && allTrackingInstances[a].Intro_Question_Status__c == 'Processed' 
                           && allTrackingInstances[a].Pathway__c == questionBackedOutOf.AC_LRA_Pathway__c){
                            //console.log('resetting pathway to selected for ',allTrackingInstances[a].Pathway__c);
                            allTrackingInstances[a].Intro_Question_Status__c = 'Selected';
                        }
                    }
                    
                    // call back to set the summary record's "exit page" to the current pathway.
                    helper.setExitPageOnSummary(component, questionBackedInto.AC_LRA_Pathway__c);
                }
                
                //console.log('allTrackingInstances', allTrackingInstances);
                // check to see if we've landed on a pathway setup (IQ) question and reset any pathways needed.
                if(questionBackedInto.AC_LRA_Pathway__c == 'Introductory Question'){
                    for(var a=0;a<allTrackingInstances.length;a++){
                        if(allTrackingInstances[a].Wizard_Question__c == questionBackedInto.Id){
                            // override the status so it's ignored in the qualification logic.
                            allTrackingInstances[a].Deleted__c = true;
                        }
                    }
                }
                //console.log('allTrackingInstances after', allTrackingInstances);
                
                for(var a=0;a<allTrackingInstances.length;a++){
                    if( allTrackingInstances[a].Wizard_Question__c != null && allTrackingInstances[a].Wizard_Question__c == questionBackedOutOf.Id){
                        //get the external id of the record that has same pathway and type=pathway
                        //that record's pathway_status__c needs be set to '' and intro question status to be set to selected
                        for(var b=0;b<allTrackingInstancesb.length;b++){
                            if(allTrackingInstancesb[b].Type__c=='Pathway' && allTrackingInstancesb[b].Pathway__c == allTrackingInstances[a].Pathway__c){
                                pthwyTrcExtId = allTrackingInstancesb[b].External_Migration_Id__c;
                            }
                        }
                        trcRecExtId = allTrackingInstances[a].External_Migration_Id__c;
                        var action = component.get("c.upsertTrackingSinglerec");
                        action.setParams({
                            trcRecExtId: trcRecExtId,
                            pthwyTrcExtId:pthwyTrcExtId
                        });
                        action.setCallback(this,function(response){
                            if(response.getState() === "ERROR"){
                                //alert(JSON.stringify(response.getError()));
                            }
                        });
                        $A.enqueueAction(action);
                    }
                }
                component.set("v.allTrackingInstances",allTrackingInstances);
                var allInstances = helper.undoOverrides(component, questionBackedInto);
                component.set("v.allTrackingInstances",allInstances);
                //console.log("v.allTrackingInstances",component.get("v.allTrackingInstances"));
                component.set("v.allQuestionSelections",allQuestionSelections);     
                
            }else{ // if no question found, go to welcome page
                var action=component.get("c.updateIQ1");
                action.setParams({
                    customerNumber: component.get("v.customerNumber")
                }); 
                action.setCallback(this,function(response){
                    component.set("v.startQA",false);
                });
                $A.enqueueAction(action);              
            }
            var showQuestionComp = component.find("showQuestion");
            showQuestionComp.refreshQuestion();
        }catch(err){
            console.log(err);
        }
    },
    handleKeywordClicked : function(component, event, helper){
        var existingTrackingInstances = component.get("v.allTrackingInstances");
        //console.log('event keyword: ' , event.getParam('keywordId'));
        var trackingInstanceKey ={'sobjectType':'Application_Request__c'};
        trackingInstanceKey.Customer_Number__c = component.get("v.customerNumber"); 
        trackingInstanceKey.Deleted__c = false;
        trackingInstanceKey.Selected_Keyword__c = event.getParam('keywordId') ;  
        trackingInstanceKey.User_Action__c = 'Clicked Keyword';
        trackingInstanceKey.EFL_AC_LRA_Summary__c = component.get('v.summaryRecordId');
        //console.log('trackingInstanceKey:',trackingInstanceKey);
        var countInst = component.get("v.countInst");
        countInst++;
        trackingInstanceKey.External_Migration_Id__c = component.get("v.customerNumber")+'-'+countInst;
        helper.saveApplicationRequest(component, trackingInstanceKey);
        //component.set("v.allTrackingInstances",existingTrackingInstances.concat(trackingInstanceKey)); // append trackinginstances w/question records
    }, 
    getTimeStartbtnclicked : function(component, event, helper){
        //console.log('timeStartBtnClicked',event.getParam('timeStartBtnClicked')); 
        component.set("v.timeStartBtnClicked", event.getParam('timeStartBtnClicked'));
    }
})