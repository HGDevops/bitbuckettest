({
	getDates : function(component, event, helper) {
		var today = new Date();
        component.set("v.currentYear",today.getFullYear());
        var previousYear = String(Number(today.getFullYear()) - 1);
        component.set("v.prevYear",previousYear);
        
        //Calculating the data difference
        var toDate = new Date(today.getFullYear(),11,2);	//Dec 1st of current year
        var diffinDates = String(Math.floor((toDate - today)/(60*60*24*1000)));
        component.set("v.diffDays",diffinDates);  
	}
})