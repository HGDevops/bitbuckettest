({
    loadPage: function(component){
        var loading = component.find("loading");
        $A.util.removeClass(loading,"slds-hide");
        var annRepId = component.get('v.recordId');
        var action = component.get("c.getPageData");
        action.setParams({
            arId : annRepId
        });
        action.setCallback(this,function(response){
            var res = response.getReturnValue();
            //console.log('res',res);
            component.set('v.pageData',res);
            $A.util.addClass(loading,"slds-hide");   
        });
        $A.enqueueAction(action);
    },
    validateNewAnimal : function(allAnimals, selectedAnimal) {
        for(var i = 0; i < allAnimals.length; i++){
            var thisAnimal = allAnimals[i];
            if(thisAnimal.record.EFLAnimalName__c == selectedAnimal.value){
                return false;
            }
        }
        return true;
    },
    validateLines : function(component){
            component.set('v.failedValidation',false);
        var allComps = [];
        var comps = component.find('singleAnimal');
        if(comps.constructor === Array){
            allComps = comps;
        }else{
            allComps.push(comps);
        }
        var allValid = allComps.reduce(function (validSoFar, c) {
            var isValid = c.validate();
            return validSoFar && isValid;
        }, true);
        if (allValid) {
            return true;
        } else {
            component.set('v.failedValidation',true);
            return false;
        }
        return true;
    },
    showError : function(component, headerValue, message){
        var self = this;
        var isCommunity = component.get('v.pageData.isCommunity');
        
        var params = {
            confirmationMessage : message,
            showCancel : false,
            confirmText : $A.get('$Label.c.EFL_AR_OK'),
            cssFile : 'EFLAR_ConfirmationDialog',
            isInternal : !isCommunity
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            headerValue,
            true,
            'confirmModal', // css file
            null // close callback
        );
    },
    toggleEdit : function(component, showEdit){
        component.set('v.editMode',showEdit);
    },
    updateTotals : function(component){
        var otherAnimalSummary = component.get('v.pageData.otherAnimalSummary');
        var b = 0;
        var c = 0; 
        var d = 0;
        var e = 0;
        var others = component.get('v.pageData.otherAnimalList');
        for(var i = 0; i < others.length; i++){
            var thisAnimal = others[i];
            b += thisAnimal.record.EFLColumnBHeldNotUsed__c != null ? thisAnimal.record.EFLColumnBHeldNotUsed__c : 0;
            c += thisAnimal.record.EFLColumnCUsedPainMinimized__c != null ? thisAnimal.record.EFLColumnCUsedPainMinimized__c : 0;
            d += thisAnimal.record.EFLColumnDUsedPainMinimized__c != null ? thisAnimal.record.EFLColumnDUsedPainMinimized__c : 0;
            e += thisAnimal.record.ELFColumnEPainNotMinimized__c != null ? thisAnimal.record.ELFColumnEPainNotMinimized__c : 0;
        }
        otherAnimalSummary.record.EFLColumnBHeldNotUsed__c = b;
        otherAnimalSummary.record.EFLColumnCUsedPainMinimized__c = c;
        otherAnimalSummary.record.EFLColumnDUsedPainMinimized__c = d;
        otherAnimalSummary.record.ELFColumnEPainNotMinimized__c = e;
        otherAnimalSummary.sumCDE = c + d + e;
        component.set('v.pageData.otherAnimalSummary', otherAnimalSummary);
    },
    saveAllLines : function(component){
        var loading = component.find("loading");
        //console.log('showloading');
        $A.util.removeClass(loading,"slds-hide");
        var action = component.get("c.upsertAnimals");
        var allAnimals = [];
        
        for(var i = 0; i < component.get('v.pageData.animalList').length; i++){
            allAnimals.push(component.get('v.pageData.animalList')[i].record);
        }
        
        for(var i = 0; i < component.get('v.pageData.otherAnimalList').length; i++){
            allAnimals.push(component.get('v.pageData.otherAnimalList')[i].record);
        }
        
        if(component.get('v.pageData.otherAnimalSummary.record') != null){
            allAnimals.push(component.get('v.pageData.otherAnimalSummary.record'));
        }
        //console.log('all animals: ' , JSON.parse(JSON.stringify(allAnimals)));
        
        action.setParams({
            animalsToUpsert : allAnimals
        });
        var self = this;
        return new Promise(function(resolve, reject) {
            action.setCallback(this,function(response){
                //console.log('hideloading');
                $A.util.addClass(loading,"slds-hide");
                if(response.getState() == 'SUCCESS'){
                    var retVal=response.getReturnValue();
                    resolve(retVal);
                }else{
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            reject(Error("Error message: " + errors[0].message));
                        }
                    }
                    else {
                        reject(Error("Unknown error"));
                    }
                }
            });
            $A.enqueueAction(action);
        });
    }
})