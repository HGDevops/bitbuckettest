({
    doInit : function(component, event, helper) {
        helper.loadPage(component);
    },
    showInCommunity : function(component, event, helper){
        component.set('v.editMode',true);
        component.set('v.showAnimalTable',true);
        helper.loadPage(component);
    },
    showEdit : function(component, event, helper){
    	helper.toggleEdit(component, true);  
    },
    hideEdit : function(component, event, helper){
    	helper.toggleEdit(component, false);  
    },
    addAnimal : function(component, event, helper) {
        var allAnimals = component.get("v.pageData.otherAnimalList");
        var animal = component.get('v.selectedAnimal');
        if(helper.validateNewAnimal(allAnimals, animal) == false){
            var header = $A.get('$Label.c.EFL_AR_Duplicate_Animal');
            var body = $A.get('$Label.c.EFL_AR_Duplicate_Animal_Message')
            helper.showError(component,header ,body );
            return;
        }
        var action = component.get("c.addOtherAnimal");
        var arId = component.get('v.recordId');
        action.setParams({
            parentId : arId,
            animalId : animal.value,
            animalName : animal.label
        });
        action.setCallback(this,function(response){
            if(response.getState() === 'SUCCESS'){
                var newAnimal = response.getReturnValue();
                allAnimals.push(newAnimal);
                component.set("v.pageData.otherAnimalList",allAnimals);
                var childCmp = component.find("cComp")
                childCmp.clearData();
            }else{
                console.log('Error: ' , resonse.getErrors());
            }
        });
        $A.enqueueAction(action);
    },
    doSave : function(component, event, helper) {
        if(helper.validateLines(component)){
            helper.saveAllLines(component).then( // use promise to determine next behavior.
                $A.getCallback(function(result){
                    // only hide edit if it's an internal user on the internal UI
                    if(component.get('v.pageData.isInternalUser') == true){
                        helper.toggleEdit(component, false);
                    }
                }),
                $A.getCallback(function(error){
                    // Something went wrong
                    console.log('An error occurred : ' + error.message);
                })
            );
        }else{
            var header = $A.get('$Label.c.EFL_AR_Missing_Required_Fields');
            var body = $A.get('$Label.c.EFL_AR_Missing_Data_Usage_Table')
            helper.showError(component,header ,body );
        }
    },
    saveAndNext : function(component, event, helper) {
        if(helper.validateLines(component)){
            helper.saveAllLines(component).then( // use promise to determine next behavior.
                $A.getCallback(function(result){
                    var eventVar = $A.get("e.c:EFLStageChangeInfoToPath");
                    eventVar.setParams({
                        completedStageName : "Stage 3"
                    });
                    eventVar.fire();
                    var event = $A.get("e.c:EFLNavigateToExceptions");
                    event.setParams({
                        showExceptions : true
                    });
                    event.fire();
                    component.set("v.showAnimalTable",false);
                }),
                $A.getCallback(function(error){
                    // Something went wrong
                    console.log('An error occurred getting the account : ' + error.message);
                })
            );
        }else{
            var header = $A.get('$Label.c.EFL_AR_Missing_Required_Fields');
            var body = $A.get('$Label.c.EFL_AR_Missing_Data_Usage_Table')
            helper.showError(component,header ,body );
        }
    },
    saveAndBack : function(component, event, helper) {
        if(helper.validateLines(component)){
            helper.saveAllLines(component).then( // use promise to determine next behavior.
                $A.getCallback(function(result){
                    var eventBackPath = $A.get("e.c:EFLStageChangeInfoToPath");
                    eventBackPath.setParams({
                        completedStageName : "Stage 1"
                    });
                    eventBackPath.fire();
                    var event = $A.get("e.c:EFLShowRegulatedAnimalsSelection");
                    event.setParams({
                        annualReportId : component.get("v.currentAnnRprtId"),
                        showAnimalSelectionPage : true
                    });
                    event.fire();
                    component.set("v.showAnimalTable",false);
                }),
                $A.getCallback(function(error){
                    // Something went wrong
                    console.log('An error occurred getting the account : ' + error.message);
                })
            );
        }else{
            var header = $A.get('$Label.c.EFL_AR_Missing_Required_Fields');
            var body = $A.get('$Label.c.EFL_AR_Missing_Data_Usage_Table')
            helper.showError(component,header ,body );
        }
    },/*
    closeError : function(component, event, helper) {
        component.set("v.showError",false);
    },
    showExplanationPop : function(component, event, helper) {
        component.set("v.animalInContext",event.getParam("currentAnimalId"));
        component.set("v.currentAnimalName",event.getParam("currentAnimalName"));
        component.set("v.showExpPop",true);
    },
    closeExplanationPopup : function(component, event, helper) {
        component.set("v.showExpPop",false);
    },
    updateColumnTotals : function(component, event, helper) {
        var colBTotal=0;
        var colCTotal=0;
        var colDTotal=0;
        var colETotal=0;
        var colFTotal=0;
        for(var i=0; i<component.get("v.animalList").length; i++){
            if(component.get("v.animalList")[i].EFLOtherAnimal__c){
                var animalColB = component.get("v.animalList")[i].EFLColumnBHeldNotUsed__c;
                if(isNaN(animalColB)){animalColB = 0;}
                colBTotal = colBTotal+animalColB;
                //--
                var animalColC = component.get("v.animalList")[i].EFLColumnCUsedPainMinimized__c;
                if(isNaN(animalColC)){animalColC = 0;}
                colCTotal = colCTotal+animalColC;
                //--
                var animalColD = component.get("v.animalList")[i].EFLColumnDUsedPainMinimized__c;
                if(isNaN(animalColD)){animalColD = 0;}
                colDTotal = colDTotal+animalColD;
                //--
                var animalColE = component.get("v.animalList")[i].ELFColumnEPainNotMinimized__c;
                if(isNaN(animalColE)){animalColE = 0;}
                colETotal = colETotal+animalColE;
            }
        }
        colFTotal = colCTotal+colDTotal+colETotal;
        component.set("v.colBTotal",colBTotal);
        component.set("v.colCTotal",colCTotal);
        component.set("v.colDTotal",colDTotal);
        component.set("v.colETotal",colETotal);
        component.set("v.colFTotal",colFTotal);
        //component.set("v.animalListDupe",component.get("v.animalList"));
    },*/
    RedirecthomePage : function(component, event, helper) {
        var upsertThenRedirectToHome = component.get("c.upsertAnimalInfo");
        upsertThenRedirectToHome.setParams({
            animalsToUpsert : component.get("v.animalList"),
            parentId : component.get("v.currentAnnRprtId"),
            colBTotal :component.get("v.colBTotal"),
            colCTotal:component.get("v.colCTotal"),
            colDTotal:component.get("v.colDTotal"),
            colETotal :component.get("v.colETotal")        
        });
        upsertThenRedirectToHome.setCallback(this,function(response){
            if(response.getState() == 'SUCCESS'){
                var responseFromServer = response.getReturnValue();
                component.set("v.showError",responseFromServer);
                if(responseFromServer == false){
                    var eventBackPath = $A.get("e.c:EFLStageChangeInfoToPath");
                    eventBackPath.setParams({
                        completedStageName : "Stage 0"
                    });
                    eventBackPath.fire();
                    component.set("v.showAnimalTable",false);
                }
            }
        });
        $A.enqueueAction(upsertThenRedirectToHome);
    },
    handleAnimalNameSelected : function(component, event, helper){
        var selectedAnimal = event.getParam("recordByEvent");
        component.set("v.selectedAnimal" , selectedAnimal);
    },
    clearSelectedAnimal : function(component, event, helper){
        component.set("v.selectedAnimal" , null); 
    },
    removeAnimal : function(component, event, helper){
        //console.log('animal to delete: ' , event.getParam('regAnimalId'));
        var animalId = event.getParam('regAnimalId');
        var stdAnimals = component.get('v.pageData.animalList');
        //console.log('stdAnimals',stdAnimals);
        var otherAnimals = component.get('v.pageData.otherAnimalList');
        //console.log('otherAnimals',otherAnimals);
        var index;
        for(var i = 0; i < stdAnimals.length; i++){
            var thisAnimal = stdAnimals[i];
            //console.log(thisAnimal.record.Id + ' == animalId',thisAnimal.record.Id == animalId);
            if(thisAnimal.record.Id == animalId){
                index = i;
                break;
            }
        }
        if(index != null){
            stdAnimals.splice(index,1);
            component.set('v.pageData.animalList',stdAnimals);
            helper.updateTotals(component);
        }else{
            //console.log('OTHER ANIMALS');
            for(var i = 0; i < otherAnimals.length; i++){
                var thisAnimal = otherAnimals[i];
            	//console.log(thisAnimal.record.Id + ' == animalId',thisAnimal.record.Id == animalId);
                if(thisAnimal.record.Id == animalId){
                    index = i;
                    break;
                }
            }
            if(index != null){
                otherAnimals.splice(index,1);
                component.set('v.pageData.otherAnimalList',otherAnimals);
                helper.updateTotals(component);
            }
        }
    },
    updateTotals : function(component, event, helper){
        helper.updateTotals(component);
    }
})