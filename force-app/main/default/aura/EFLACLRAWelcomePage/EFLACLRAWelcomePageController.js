({
    doInit : function(component, event, helper) {
        // check if IE 
        var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/;
        if (detectIEregexp.test(navigator.userAgent)){ 
            component.set("v.isIE",true);
        }
        var device = $A.get("$Browser.formFactor");
        if (device=='PHONE' && window.matchMedia("(orientation: portrait)").matches){
            component.set("v.isMobile",true);
            //alert('This app is best viewed in landscape mode');
        }
        if ($A.get("$Label.c.EFLACLRA_Show_OMB")=="Yes"){
            component.set("v.showOMB", true);
        }
        

    },
    startQuestionnaire1 : function(component, event, helper) {
        try{
            helper.closeError(component);
            var rightNow = new Date().getTime();
            var timeClicked = rightNow / 1000; // time in milliseconds / 1000 = seconds
            //var timeString = rightNow.toString();
            //var customerNumber = timeString.substring(timeString.length-10,timeString.length-1);
            var summaryRecId ='';
            
            //component.set("v.customerNumber",customerNumber);            
            var action = component.get("c.startActionTracking");
            action.setParams({
                //customerNumber : customerNumber
            });
            action.setCallback(this,function(response){
                try{
                    //console.log('startQuestionnaire1');
                    //console.log(response.getState());
                    //console.log(response.getError());
                    var res = response.getReturnValue();
                    component.set('v.summaryRecordId',res.summaryRecordId);
                    component.set('v.customerNumber',res.customerNumber);
                    console.log('customerNumber',res.customerNumber);
                    component.set('v.startTime',timeClicked);
                    component.set("v.startQA",true);
                }catch(err){
                    console.log(err);
                }
            });          
            $A.enqueueAction(action);
        }catch(err){
            console.log(err);
        }
    },
    handlePathwaysCompleted : function(component,event,helper){
        //console.log('pathway completed event received');
        var pathways = event.getParam('pathwayList');
        var pathwayStrings = [];
        var numberQualifiedFor = 0;
        for(var i = 0; i < pathways.length; i++){
            if(pathways[i].status == 'Qualified'){
                numberQualifiedFor += 1;
            }
        }
        var outcomeType;
        if(numberQualifiedFor > 1){
            outcomeType = 'Combination';
        }else if(numberQualifiedFor == 1){
            outcomeType = 'Single';
        }
        
        pathways.sort(function(a,b){
            if (a.pathway < b.pathway)
                return -1;
            if (a.pathway > b.pathway)
                return 1;
            return 0;
        });
        
        //console.log('pathways', pathways);
        component.set('v.pathwayResults',pathways);
        component.set('v.showResults',true);
        component.set('v.startQA',false);
        
        // calculate the time from start to finish of applicaiton.
        var startTime = component.get('v.startTime');
        var endTime = new Date().getTime() / 1000; // time in milliseconds / 1000 = seconds
        var appTime = (endTime - startTime) / 60; // time in minutes
        
        // set callback to update summary record with Time to Complete
        var action = component.get('c.finalizeSummary');
        action.setParams({
            summaryRecordId : component.get('v.summaryRecordId'),
            timeInMins : appTime,
            pathways : JSON.stringify(pathways)
        });
        action.setCallback(this,function(result){
            //console.log(result.getState());
            if(result.getState() !== 'SUCCESS'){
                console.log(result.getError());
            }
        });
        $A.enqueueAction(action);
    },
    trackAndNavigate : function(component, event, helper){
        //console.log('trackAndNavigate');
        if(component.get('v.summaryRecordId') && component.get('v.summaryRecordId') != null && component.get('v.summaryRecordId') != ''){
            try{
                var action = component.get('c.logWelcomeActivity');
                action.setParams({
                    summaryRecordId : component.get('v.summaryRecordId'),
                    actionType : $A.get('$Label.c.EFL_AC_LRA_Contact_Us_Welcome_Clicked')
                });
                action.setCallback(this, function(res){
                });
                $A.enqueueAction(action);
            }catch(err){
                console.log(err);
            }
        }
        window.open($A.get('$Label.c.EFL_AC_LRA_Contact_Us_HREF'), '_blank');
    },
    closeError : function(component,event, helper ){
        helper.closeError(component);
    }
})