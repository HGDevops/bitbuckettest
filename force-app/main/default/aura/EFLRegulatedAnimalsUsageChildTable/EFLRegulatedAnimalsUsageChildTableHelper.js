({
    showDeleteConfirmation : function(component) {
        var self = this;
        var isCommunity = component.get('v.isCommunity');
        //console.log('isCommunity', isCommunity);
        try{
            var params = {
                confirmationMessage : 'Are you sure you want to delete this record?',
                cancelText : 'Cancel',
                confirmText : $A.get('$Label.c.EFLAR_Delete'),
                confirmCallback : function(){self.doDelete(component);},
                cssFile : 'EFLAR_ConfirmationDialog',
            	isInternal : !isCommunity
            };
            self.showModalComponent(
                component,
                'c:EFLConfirmationDialog',
                params,
                'Delete Record(s)',
                true,
                'confirmModal', // css file
                null // close callback
            );
        }catch(er){
            console.log(er);
        }
    },
    doDelete : function(component){
        var loading = component.find("loading");
        $A.util.removeClass(loading,"slds-hide");
        var action = component.get("c.deleteThisAnimal");
        action.setParams({
            animalToDelete : component.get("v.singleAnimal.record")
        });
        action.setCallback(this,function(response){
            // this response is the ID of the animal that was just deleted.
            // Fire an event and tell the list to remove this Id from the list.
            var event = $A.get("e.c:EFLAnimalRemoved");
            event.setParams({
                regAnimalId : response.getReturnValue()
            });
            event.fire();
            $A.util.addClass(loading,"slds-hide");
        });
        $A.enqueueAction(action);
    },
    sumCDE : function(component){
        // first calc the sum C+D+E
        try{
            var thisAnimal = component.get('v.singleAnimal');
            //console.log('thisAnimal',JSON.parse(JSON.stringify(thisAnimal)));
            var sum = 0;
            sum += thisAnimal.record.EFLColumnCUsedPainMinimized__c != null ? thisAnimal.record.EFLColumnCUsedPainMinimized__c : 0;
            sum += thisAnimal.record.EFLColumnDUsedPainMinimized__c != null ? thisAnimal.record.EFLColumnDUsedPainMinimized__c : 0;
            sum += thisAnimal.record.ELFColumnEPainNotMinimized__c != null ? thisAnimal.record.ELFColumnEPainNotMinimized__c : 0;
            component.set('v.singleAnimal.sumCDE',sum);
        }catch(err){
            console.log(err);
        }
    },
    showExplanation : function(component){
        var self = this;
        var params = {
            registeredAnimalId : component.get('v.singleAnimal.record.Id'),
            animalName : component.get('v.singleAnimal.name'),
            cssFile : 'EFLAR_ConfirmationDialog',
            isCommunity : component.get('v.isCommunity')
        };
        self.showModalComponent(
            component,
            'c:EFLExplanation',
            params,
            'Column E Explanation',
            true,
            'confirmModal', // css file
            null // close callback
        );
    },
})