({
    doInit : function(component, event, helper) {
        //console.log('singleAnimal', JSON.parse(JSON.stringify(component.get('v.singleAnimal'))));
        helper.sumCDE(component);
    },
    //Delete Animal
    deleteAnimal : function(component, event, helper) {
        //console.log('deleteAnimal');
        helper.showDeleteConfirmation(component);
    },
    reEvaluateCalculation : function(component,event,helper){
        helper.sumCDE(component);
        
        //Column-wise Summation
        if(component.get("v.singleAnimal.record.EFLOtherAnimal__c")){
            var event = $A.get("e.c:EFLUpdateColumnTotals");
            event.fire();
        }
    },
    showExpPopup : function (component, event, helper) {
        helper.showExplanation(component);
    },
    validateEntries : function(component, event, helper){
        var cols = ['input-colB','input-colC','input-colD','input-colE'];
        component.set('v.isValid',true);
        var ret = true;
        if(component.get('v.singleAnimal.EFLAnimal__c') !='OTHER ANIMALS'){
            for(var i = 0; i < cols.length; i++){
                var input = component.find(cols[i]);
                var value = input.get('v.value');
                //console.log('value', value);
                if((!value || value == null) && value != 0){
                    //console.log('has error: ');
                    component.set('v.isValid',false);
                    $A.util.addClass(input, 'eflFieldHasError');
                    ret = false;
                }else{
                    $A.util.removeClass(input, 'eflFieldHasError');
                }
            }
        }
        return ret;
    }
})