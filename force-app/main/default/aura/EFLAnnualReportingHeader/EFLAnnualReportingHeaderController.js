({
	gotoHome : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": "/"
        });
        navEvt.fire();
        /*var eventVar = $A.get("e.c:EFLHomePage");
        eventVar.setParams({
            showHomePage : true,
            showBody : false
        });
        eventVar.fire(); */
	},
    userMenuAction : function(component, event, helper) {
        var optionSelected = event.detail.menuItem.get("v.value");
        if(optionSelected == 'logout'){
            window.location.replace("https://acrlbuild1-aphis-efile.cs32.force.com/ACISCommunity/secur/logout.jsp?retUrl=https://acrlbuild1-aphis-efile.cs32.force.com/ACIS/login");
        }
    },
    doInit : function(component, event, helper) {
        var action = component.get("c.getCurrentUser");
        action.setCallback(this,function(response){
            var fullName = response.getReturnValue();
            var nameArray = fullName.split(" ");
            var firstName = nameArray[0];
            component.set("v.userFirstName",firstName);
            var initials = nameArray[0].substring(0,1).toUpperCase()+nameArray[1].substring(0,1);
            component.set("v.userInitials",initials);            
        });
        $A.enqueueAction(action);
    },
     basicInfoPop : function(component, event, helper) {
        var action = component.get("c.getARStatus");
        action.setCallback(this,function(response){
            var atleastOneDraftStatus = response.getReturnValue();
            if(component.get("v.LogOutPopUp") == false && atleastOneDraftStatus == true)
                component.set("v.LogOutPopUp",true);
            else{
                var logMeOutWithoutPopUp = component.get("c.LogOut");
            	$A.enqueueAction(logMeOutWithoutPopUp);
            }
        });
        $A.enqueueAction(action);
        
    },
     Cancel: function(component, event, helper) {
      // for Hide/Close Model,set the "LogOutPopUp" attribute to "Fasle"  
      component.set("v.LogOutPopUp", false);
   },
    LogOut: function(component, event, helper) {
      // for Hide/Close Model,set the "LogOutPopUp" attribute to "Fasle"  
        component.set("v.LogOutPopUp",false);
        var baseURLHost = window.location.host;
        var indexOfHyphen = baseURLHost.indexOf("-aphis");
        var currentEnvironment = baseURLHost.substring(0,indexOfHyphen);        
        window.location.replace("https://"+currentEnvironment+"-aphis-efile.cs32.force.com/ACISCommunity/secur/logout.jsp?retUrl=https://"+currentEnvironment+"-aphis-efile.cs32.force.com/ACIS/login");
        
   }
})