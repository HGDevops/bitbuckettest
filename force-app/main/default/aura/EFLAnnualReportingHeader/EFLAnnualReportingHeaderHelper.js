({
	displayModalMessage : function(component, header, messageBody){
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : false,
            showConfirm : true,
            confirmText : 'OK'/*,
            cssFile : 'EFLConfirmationBox',
            cancelCallback : component.get('c.cancelCallback') // optional
            */
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            false,
            null,
            null
        );
    }
})