({
    getPageData : function(component) {
        component.set('v.selectedRecords',0);
        var usageKey = component.get('v.usageKey');
        var parentId = component.get('v.parentId');
        var action = component.get('c.getPageData');
        action.setParams({
            parentId : parentId,
            usageKey : usageKey
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            //console.log('res',res);
            component.set('v.pageData',res);
            component.set('v.isLoading',false);
        });
        $A.enqueueAction(action);
    },
    showError : function(component, headerValue, message){
        var self = this;
        var params = {
            confirmationMessage : message,
            showCancel : false,
            confirmText : $A.get('$Label.c.EFL_AR_OK'),
            cssFile : 'EFLAR_ConfirmationDialog'
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            headerValue,
            true,
            'confirmModal', // css file
            null // close callback
        );
    },
    doDeleteConfirm : function(component, selected){
        // show a confirmation modal for deleting rows.
        var self = this;
        var records = selected;
        var params = {
            confirmationMessage : $A.get('$Label.c.EFLAR_Delete_Record_Message'),
            cancelText : 'Cancel',
            confirmText : 'Delete',
            confirmCallback : function(){self.deleteRows(component,records);},
            cssFile : 'EFLAR_ConfirmationDialog'
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            $A.get('$Label.c.EFLAR_Delete_Record_s'),
            true,
            'confirmModal', // css file
            null // close callback
        );
    },
    deleteRows : function(component, selected){
        var action = component.get('c.deleteRecords');
        action.setParams({
            recordIds : selected,
            objectName : component.get('v.pageData.config.SObject_Type__c'),
            serviceClass : component.get('v.pageData.config.Service_Class__c'),
            doDelete : component.get('v.pageData.config.Permanent_Delete_on_Delete__c')
        });
        action.setCallback(this,function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                // Process server success response
                component.set('v.selectedRecords',0);
                component.set('v.isLoading',true);
                this.getPageData(component);
            }else{
                component.set('v.isLoading',false);
                console.log('error: ' , response.getErrors());
            }
        });
        $A.enqueueAction(action);
    },
    getSelected : function(component){
        var records = component.get('v.pageData').records;
        if(records.length == 0){
            return [];
        }
        var selected = [];
        var possible = [];
        /*list<Id> recordIds, string ObjectName, string serviceClass, boolean doDelete*/
        for(var i = 0; i < records.length; i++){
            var r = records[i];
            if(r.isSelectable == true){
                possible.push(r.Id);
            }
            if(r.isSelected == true){
                selected.push(r.id);
            }
        }
        return {selected : selected,possible : possible};
    },
    showAddModal : function(component){
        try{
            var addComponent = 'c:' + component.get('v.pageData.config.Add_New_Screen_Component__c');
            var self = this;
            var params = {
                sObjectType : component.get('v.pageData.config.SObject_Type__c'),
                cssFile : component.get('v.pageData.config.Edit_Screen_CSS_File__c'),
                cancelText : component.get('v.pageData.config.Cancel_Button_Text__c'),
                confirmText : component.get('v.pageData.config.Save_Button_Text__c'),
                usageKey : component.get('v.usageKey')
            };
            var commonName = component.get('v.pageData.config.Object_Common_Name__c');
            var label = $A.get('$Label.c.EFLAR_Add_New_Record').replace('{1}', commonName);
            self.showModalComponent(
                component,
                addComponent,
                params,
                label,
                true,
                'confirmModal', // css file
                null // close callback
            );
        }catch(err){
            console.log(err);
        }
    }
})