({
    doInit : function(component, event, helper) {
        component.set('v.selectedRecords',0);
        component.set('v.isLoading',true);
        var parentId;
        if(component.get('v.recordId') != null){
            parentId = component.get('v.recordId');
        }else if(component.get('v.parentId') != null){
            parentId = component.get('v.parentId');
        }
        component.set('v.parentId',parentId);
        helper.getPageData(component);
    },
    doSelectAll : function(component, event, helper) {
        try{
            var checkbox = component.find('selectAllCheckbox');
            var isChecked = checkbox.get("v.value");
            var records = component.get('v.pageData').records;
            for(var i = 0; i < records.length; i++){
                var r = records[i];
                if(r.isSelectable){
                    r.isSelected = isChecked;
                }
            }
            component.set('v.pageData.records',records);
            var selectedSummary = helper.getSelected(component);
            component.set('v.selectedRecords', selectedSummary.selected.length);
        }catch(err){
            console.log(err);
        }
    },
    doDelete : function(component, event, helper) {
        var selectedSummary = helper.getSelected(component);
        
        if(selectedSummary.selected.length == 0){
            return;
        }
        
        // show a delete confirmation
        helper.doDeleteConfirm(component, selectedSummary.selected);
    },
    setSelectedRecords : function(component, event, helper){
        component.find('selectAllCheckbox').set("v.value",false);
        var selectedSummary = helper.getSelected(component);
        if(selectedSummary.selected.length == selectedSummary.possible.length){
            component.find('selectAllCheckbox').set("v.checked",true);
        }
        component.set('v.selectedRecords', selectedSummary.selected.length);
    },
    showAdd : function(component, event, helper){
        helper.showAddModal(component);
    },
    checkRefresh : function(component, event, helper){
        if(component.get('v.usageKey') == event.getParam('usageKey')){
            component.set('v.selectedRecords',0);
            component.set('v.isLoading',true);
            helper.getPageData(component);
        }
    },
    deleteChildRecord : function(component, event, helper){
        var selected = [];
        var rId = event.getParam('recordId');
        selected.push(rId);
        if(selected.length > 0){
            helper.doDeleteConfirm(component, selected);
        }
    }
})