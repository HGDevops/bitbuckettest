({
    /*doInit : function(component, event, helper) {
        var annRepId = event.getParam("annRepId");
        component.set("v.annRepId",event.getParam("annRepId"));
        component.set("v.annRepId","a4Xr000000003RnEAI");
    },  */      
    selectionChanged : function(component, event, helper) {
        var animals = ["Dogs","Cats","GuineaPigs","Non-HumanPrimates",
                       "Sheep","Pigs","Rabbits","OtherAnimals",
                       "Hamsters"];
        var newChangeElement = event.target.id;
        var newChangeValue = event.target.checked;
        if(newChangeValue == true){
            if(newChangeElement != 'add-checkbox-No'){
                document.getElementById('add-checkbox-No').disabled=true;
            }
            else{
                for(var i=0;i<animals.length;i++){
                    var dynId='add-checkbox-'+animals[i];
                    document.getElementById(dynId).disabled=true;
                }
            }
            var selectedAnimals =component.get("v.selectedAnimals");
            
            switch (newChangeElement) {
                case 'add-checkbox-Dogs':
                    selectedAnimals.push('Dogs');
                    break;
                case 'add-checkbox-Rabbits':
                    selectedAnimals.push('Rabbits');
                    break;
                case 'add-checkbox-Pigs':
                    selectedAnimals.push('Pigs');
                    break;
                case 'add-checkbox-Cats':
                    selectedAnimals.push('Cats');
                    break;
                case 'add-checkbox-Sheep':
                    selectedAnimals.push('Sheep');
                    break;
                case 'add-checkbox-OtherAnimals':
                    selectedAnimals.push('Other Animals');
                    break;
                case 'add-checkbox-GuineaPigs':
                    selectedAnimals.push('Guinea Pigs');
                    break;
                case 'add-checkbox-Non-HumanPrimates':
                    selectedAnimals.push('Non-Human Primates');
                    break;
                case 'add-checkbox-Hamsters':
                    selectedAnimals.push('Hamsters');
                    break;
            }
            component.set("v.selectedAnimals",selectedAnimals);
            //alert(selectedAnimals);
        }
        else if(newChangeValue == false){
            if(newChangeElement == 'add-checkbox-No'){
                for(var i=0;i<animals.length;i++){
                    var dynId='add-checkbox-'+animals[i];
                    document.getElementById(dynId).disabled=false;
                }
            }
            else{
                var counter = 0;
                for(var i=0;i<animals.length;i++){
                    var dynId='add-checkbox-'+animals[i];
                    if(document.getElementById(dynId).checked==false){
                        counter++;
                    }
                }
                if(counter==animals.length){
                    document.getElementById('add-checkbox-No').disabled=false;
                }
            }
            var selectedAnimals =component.get("v.selectedAnimals");
            switch (newChangeElement) {
                case 'add-checkbox-Dogs':
                    selectedAnimals.splice(selectedAnimals.indexOf('Dogs'),1);
                    break;
                case 'add-checkbox-Rabbits':
                    selectedAnimals.splice(selectedAnimals.indexOf('Rabbits'),1);
                    break;
                case 'add-checkbox-Pigs':
                    selectedAnimals.splice(selectedAnimals.indexOf('Pigs'),1);
                    break;
                case 'add-checkbox-Cats':
                    selectedAnimals.splice(selectedAnimals.indexOf('Cats'),1);
                    break;
                case 'add-checkbox-Sheep':
                    selectedAnimals.splice(selectedAnimals.indexOf('Sheep'),1);
                    break;
                case 'add-checkbox-OtherAnimals':
                    selectedAnimals.splice(selectedAnimals.indexOf('Other Animals'),1);
                    break;
                case 'add-checkbox-GuineaPigs':
                    selectedAnimals.splice(selectedAnimals.indexOf('Guinea Pigs'),1);
                    break;
                case 'add-checkbox-Non-HumanPrimates':
                    selectedAnimals.splice(selectedAnimals.indexOf('Non-Human Primates'),1);
                    break;
                case 'add-checkbox-Hamsters':
                    selectedAnimals.splice(selectedAnimals.indexOf('Hamsters'),1);
                    break;
            }
            component.set("v.selectedAnimals",selectedAnimals);
        }
    },
    initiateComponent : function(component, event, helper) {
        console.log('initiating select animals component');
        var spinner = component.find("loading");
        $A.util.removeClass(spinner,"slds-hide");
        var animals = ["Dogs","Cats","GuineaPigs","Non-HumanPrimates",
                       "Sheep","Pigs","Rabbits","OtherAnimals",
                       "Hamsters"];
        var eventValue = event.getParam("showAnimalSelectionPage");
        component.set("v.showAnimals",eventValue);
        var annRepId = component.get('v.recordId');
        component.set("v.annRepId",annRepId);
        var action = component.get("c.lookForExistingAnimals");
        action.setParams({
            annRepId:annRepId
        });
        action.setCallback(this,function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.selectedAnimals",response.getReturnValue().selectedAnimals);
                var disableOthers =false;
                var disableNo = false;
                if(response.getReturnValue().selectedAnimals.length == 0 && 
                   response.getReturnValue().totalAnimals > 0){
                    document.getElementById('add-checkbox-No').checked = true;
                    disableOthers = true;
                }
                for(var i=0;i<component.get("v.selectedAnimals").length;i++){
                    switch (component.get("v.selectedAnimals")[i]) {
                            
                        case 'Dogs':
                            document.getElementById('add-checkbox-Dogs').checked = true;
                            disableNo = true;
                            break;
                        case 'Rabbits':
                            document.getElementById('add-checkbox-Rabbits').checked = true;
                            disableNo = true;
                            break;
                        case 'Pigs':
                            document.getElementById('add-checkbox-Pigs').checked = true;
                            disableNo = true;
                            break;
                        case 'Cats':
                            document.getElementById('add-checkbox-Cats').checked = true;
                            disableNo = true;
                            break;
                        case 'Sheep':
                            document.getElementById('add-checkbox-Sheep').checked = true;
                            disableNo = true;
                            break;
                        case 'Other Animals':
                            document.getElementById('add-checkbox-OtherAnimals').checked = true;
                            disableNo = true;
                            break;
                        case 'Guinea Pigs':
                            document.getElementById('add-checkbox-GuineaPigs').checked = true;
                            disableNo = true;
                            break;
                        case 'Non-Human Primates':
                            document.getElementById('add-checkbox-Non-HumanPrimates').checked = true;
                            disableNo = true;
                            break;
                        case 'Hamsters':
                            document.getElementById('add-checkbox-Hamsters').checked = true;
                            disableNo = true;
                            break;
                            /*case '':
                            document.getElementById('add-checkbox-No').checked = true;
                            disableOthers = true;
                            break;*/
                    }
                }
                if(disableOthers){
                    for(var i=0;i<animals.length;i++){
                        var dynId='add-checkbox-'+animals[i];
                        document.getElementById(dynId).disabled=true;
                    }
                }
                else if(disableNo){
                    document.getElementById('add-checkbox-No').disabled=true;
                }
                var loading = component.find("loading");
                $A.util.addClass(loading,"slds-hide");
            }
        });
        $A.enqueueAction(action);
    },
    upsertAndNext :function(component, event, helper) {
        if(component.get("v.selectedAnimals").length == 0 && 
           document.getElementById('add-checkbox-No').checked == false){
            //console.log('calling displayModalMessage');
            helper.displayModalMessage(
                component,
                'Select Animal(s)',
                'You must select at least one animal or the option indicating that you have no regulated animals to report in order to continue.'
            ) ;
        }
        else{
            var triggerButton = event.getSource().getLocalId();
            var annRepId = component.get("v.annRepId");
            var action = component.get('c.insertStdAnimals');
            var selectedAnimalList = component.get("v.selectedAnimals");
            //console.log('selectedAnimalList',selectedAnimalList);
            //var annRepId = Component.get("v.annRepId");
            action.setParams({
                annRepId:annRepId,
                selectedAnimalList:component.get("v.selectedAnimals")
            });
            action.setCallback(this,function(response){
                //alert(component.get("v.selectedAnimals")+annRepId);
                if(response.getState() == 'SUCCESS'){
                    var res = response.getReturnValue();
                    //console.log('upsert & next response: ' ,res );
                    if(res[0] == 'show error'){
                        component.set("v.showError",true);
                    }else{
                        component.set("v.showError",false);
                        //alert('Records Updated Successfully');
                        if(triggerButton == 'nextBtn'){
                            //if(component.get("v.selectedAnimals")[0]==''){
                            if(document.getElementById('add-checkbox-No').checked){
                                //component.set("v.showPop",true);
            helper.displayContinueMessage(
                component,
                'No Regulated Animals to Report',
                'You have indicated that you have no regulated animals to report. Selecting "Continue" below will take you to the "Review and Submit" page.'
            ) ;                                
                                helper.changeDisplayStage(component, 'Stage 4');
                                //var eventVar = $A.get("e.c:EFLStageChangeInfoToPath");
                                //eventVar.setParams({
                                //    completedStageName : "Stage 4"
                                //});
                                //eventVar.fire();
                            }
                            else{
                                helper.changeDisplayStage(component, 'Stage 2');
                                //var eventVar = $A.get("e.c:EFLStageChangeInfoToPath");
                                //eventVar.setParams({
                                //    completedStageName : "Stage 2"
                                //});
                                //eventVar.fire();
                                var event2 = $A.get("e.c:EFLShowAnimalUsageTable");
                                event2.setParams({
                                    showAnimalUsage : true,
                                    annualReportId : component.get("v.annRepId")
                                });
                                event2.fire();
                                component.set("v.showAnimals",false);
                            }
                        }
                        
                        if(triggerButton == 'backBtn'){
                            helper.changeDisplayStage(component, 'Stage 0');
                            //var eventVar4 = $A.get("e.c:EFLStageChangeInfoToPath");
                            //eventVar4.setParams({
                            //    completedStageName : "Stage 0"
                            //});
                            //eventVar4.fire();
                            //console.log('response.getReturnValue()',response.getReturnValue());
                            //console.log('v.annRepId',component.get("v.annRepId"));
                            var event3 = $A.get("e.c:EFLAnnualReporting_CommuicateRegId");
                            event3.setParams({
                                regId : response.getReturnValue()[1],
                                annRepId : component.get("v.annRepId")
                            });
                            event3.fire();	
                            component.set("v.showAnimals",false);
                            //EFLHomePage
                            var evt = $A.get("e.c:EFLHomePage");
                            evt.setParams({
                                showBody : true
                            });
                            evt.fire();	
                        }
                    }
                }
                //}
            })
            $A.enqueueAction(action);
        }
    },
    
    proceedToReview : function(component, event, helper) {
        var eventVarx = $A.get("e.c:EFLNavigateToSummary");
        eventVarx.setParams({
            showSummary : true,
            annualReportId : component.get("v.annRepId")
        });
        eventVarx.fire();
        component.set("v.showAnimals",false);
        component.set("v.showPop",false);
    },
    
    cancel : function(component, event, helper) {
        component.set("v.showPop",false);
    },
    
    closeError : function(component, event, helper) {
        component.set("v.showError",false);
    },
    homePage : function(component, event, helper) {
        var eventVar4 = $A.get("e.c:EFLStageChangeInfoToPath");
        eventVar4.setParams({
            completedStageName : "Stage 0"
        });
        eventVar4.fire();
        component.set("v.showAnimals",false);
    }
})