({
	displayModalMessage : function(component, header, messageBody){
        try{
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : false,
            confirmText : 'OK',
            cssFile : 'EFLAR_ConfirmationDialog'/*,
            cancelCallback : component.get('c.cancelCallback') // optional
            */
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            true,
            'confirmModal', // css file
            null // close callback
        );
        }catch(err){
            
            console.log(err);
        }
    },
    changeDisplayStage : function(component, stageName){
        var eventVar4 = $A.get("e.c:EFLStageChangeInfoToPath");
        eventVar4.setParams({
            completedStageName : stageName
        });
        eventVar4.fire();
    },
	displayContinueMessage : function(component, header, messageBody){
        try{
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : true,
            confirmText : 'Continue',
            cancelText:'Cancel',
            cssFile : 'EFLAR_ConfirmationDialog',
            cancelCallback : component.get('c.cancel'), 
            confirmCallback : function(){self.proceedToReview(component);}            
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            true,
            'confirmModal', // css file
            null // close callback
        );
        }catch(err){
            
            console.log(err);
        }
    },
    changeDisplayStage : function(component, stageName){
        var eventVar4 = $A.get("e.c:EFLStageChangeInfoToPath");
        eventVar4.setParams({
            completedStageName : stageName
        });
        eventVar4.fire();
    },
        proceedToReview : function(component, event, helper) {
        var eventVarx = $A.get("e.c:EFLNavigateToSummary");
        eventVarx.setParams({
            showSummary : true,
            annualReportId : component.get("v.annRepId")
        });
        eventVarx.fire();
        component.set("v.showAnimals",false);
        component.set("v.showPop",false);
    }
    
})