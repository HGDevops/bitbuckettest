({
    fetchHQData : function(component, event, helper) {
        helper.getPicklistOptions(component, event, "EFL_Using_Federal_Funds__c", "v.usingFederalFundsOptions");
        helper.getPicklistOptions(component, event, "EFL_Change_In_Ownership__c", "v.changeInOwnershipOptions");

        var annualRepId = component.get("v.annRepId");
        //component.set("v.showSites",event.getParam("showSites")); 
        var action = component.get('c.getAnnRepList');
        action.setParams({
            annRepId : annualRepId
        });
        action.setCallback(this,function(response){
            component.set("v.results",response.getReturnValue());
            component.set("v.sites",response.getReturnValue()[0].EFLSites__c);
            component.set("v.usingFederalFunds",response.getReturnValue()[0].EFL_Using_Federal_Funds__c);
            component.set("v.changeInOwnership",response.getReturnValue()[0].EFL_Change_In_Ownership__c);
            //alert(response.getReturnValue()[0].EFLSites__c);
            component.set("v.showSites", true);
            component.set("v.annRepId",annualRepId);
        });
        $A.enqueueAction(action);
    },
    saveAnnualReport : function(component, event, helper) {
        return helper.saveAnnualReport(component, event);
    },
    goToHome : function(component, event, helper) {
        var firstSave = helper.saveAnnualReport(component, event);// call another method above
        if(firstSave) {
            var navEvt = $A.get("e.force:navigateToURL");
            navEvt.setParams({
                "url": "/"
            });
            navEvt.fire();
           /* var eventVar = $A.get("e.c:EFLHomePage");
            eventVar.setParams({
                showHomePage : true,
                showForm : false
            });
            eventVar.fire();*/
            //component.set("v.showSites",false);
        }
    },
    basicInfoPop : function(component, event, helper) {
        if(component.get("v.showBasicInfoPop") == false)
            component.set("v.showBasicInfoPop",true);
        else
            component.set("v.showBasicInfoPop",false);
    },
    showRegAnimals : function(component, event, helper) {
        var firstSave = helper.saveAnnualReport(component, event);// call another method above
        if(firstSave) {
            var eventVar = $A.get("e.c:EFLStageChangeInfoToPath");
            eventVar.setParams({
                completedStageName : "Stage 1"
            });
            eventVar.fire();
             
            var eventVar2 = $A.get("e.c:EFLShowRegulatedAnimalsSelection");
            eventVar2.setParams({
                showAnimalSelectionPage : true,
                annualReportId : component.get("v.annRepId")
            });
            eventVar2.fire();
            
            component.set("v.showSites",false);
        }
    }
})