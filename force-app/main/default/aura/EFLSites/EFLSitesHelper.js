({
    getPicklistOptions : function(component, event, sObjectFieldName, attributeName) {
        var annualRepId = component.get("v.annRepId");
        var action = component.get('c.getPicklistValues');
        action.setParams({
            sObjectName : "EFLAnnual_Report__c",
            fieldName : sObjectFieldName
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set(attributeName, response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    saveAnnualReport : function(component, event) {
        var federalFunds = component.get("v.usingFederalFunds");
        var ownerChange = component.get("v.changeInOwnership");
        if(federalFunds === undefined || ownerChange === undefined) {
            component.find("federalFundsUsed").reportValidity();
            component.find("ownershipChange").reportValidity();
            
            var header = $A.get("$Label.c.EFLARUserEntryFormMissingValuesHDR");
            var messageBody = $A.get("$Label.c.EFLACLRA_Complete_all_required_fields");
            this.displayModalMessage(component, header, messageBody);
            return false;
        }

        var sitesValue = component.get("v.sites") 
        if (sitesValue === undefined) {
            sitesValue = ""; 
        }

        var action = component.get("c.saveData");
        action.setParams({
            annRepId : component.get("v.annRepId"),
            sites : sitesValue,
            usingFederalFunds : component.get("v.usingFederalFunds"),
            changeInOwnership : component.get("v.changeInOwnership")
        });
        action.setCallback(this,function(response){
        });
        $A.enqueueAction(action);
        return true;
    },
    
	displayModalMessage : function(component, header, messageBody){
        var self = this;
        var params = {
            confirmationMessage : messageBody,
            showCancel : false,
            showConfirm : true,
            confirmText : 'OK',
            cssFile : 'EFLAR_ConfirmationDialog'/*,
            cancelCallback : component.get('c.cancelCallback') // optional
            */
        };
        self.showModalComponent(
            component,
            'c:EFLConfirmationDialog',
            params,
            header,
            true,
            'confirmModal', // css file
            null
        );
    }    
})