function checkCBIData(btnparam) {
    
    //alert("Testing for mapping");
    
    try{
        $("#errDiv").show();
        
        var errMsg = '';
        
        var $list = $(":input[type='text']");
        
        var cbi = false;
        
        var tokens = [
            
            ['[', ']']
            
        ];
        
        
        var isCBIAllowed = $('[id$=Does_This_Application_Contain_CBI__c]').val();
        //alert("CBI Value =:  " + isCBIAllowed);
        var index = 0; 
        $list.each(function() {
            
            var input = $(this);
            
            
            input.parent().find('.myCustomMsg').remove();  //Ravee
            input.parent().find('.errorMsg').remove();  //Arif
            
            
            if (isCBIAllowed === 'No') {
                
                if (input.val().indexOf('[') != -1 || input.val().indexOf(']') != -1) {
                    
                    input.parent().append("<div class='errorMsg small-text danger-msg myCustomMsg'><strong>Error: </strong> "+($Label.EFLCBINoValidationMsg)+"</div>");
                    
                    cbi = true;
                    
                }
                
            } else if (isCBIAllowed === 'Yes') {
                if (input.val().includes('[') && input.val().includes(']')) { 
                    if($('[id$=DayPhone]').val() == input.val() || $('[id$=AlternatePhone]').val() == input.val() || $('[id$=primZip]').val() == input.val() ||$('[id$=Fax]').val() == input.val()||$('[id$=primaryEmail]').val() == input.val()||$('[id$=PrimaryAlternateEmail]').val() == input.val()) {
                        input.parent().append("<div class='errorMsg small-text danger-msg myCustomMsg'><strong>Error: </strong> To claim this field as CBI, select 'Claim as CBI' checkbox. Please remove brackets to proceed.</div>");
                        cbi = true;
                    }  }   
                
                if (input.val().includes('[') || input.val().includes(']')) {
                    
                    var inputStr = input.val();
                    
                    var expression = inputStr.split('');
                    
                    var stack = [];
                    
                    for (var i = 0; i < expression.length; i++) {
                        
                        if (isParanthesis(expression[i])) {
                            
                            if (isOpenParenthesis(expression[i])) {
                                
                                stack.push(expression[i]);
                                
                            } else {
                                
                                if (stack.length === 0) {
                                    
                                    return printToScreen(false);
                                    
                                }
                                
                                var top = stack.pop();
                                
                                if (!matches(top, expression[i])) {
                                    
                                    return printToScreen(false);
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    printToScreen(stack.length === 0)
                    
                    
                    function printToScreen(bool) {
                        
                        if (!bool) {
                            
                            input.parent().append("<div class='errorMsg small-text danger-msg myCustomMsg'><strong>Error: </strong> "+($Label.EFLCBIYesValidationMsg)+"</div>");
                            
                            cbi = true;
                            
                        }
                        
                    }
                    
                }
                
            }
            
        });
        
        
        
        function isParanthesis(char) {
            
            var str = '[]';
            
            if (str.indexOf(char) > -1) {
                
                return true;
                
            } else {
                
                return false;
                
            }
            
        }
        
        
        
        function isOpenParenthesis(parenthesisChar) {
            
            for (var j = 0; j < tokens.length; j++) {
                
                if (tokens[j][0] === parenthesisChar) {
                    
                    return true;
                    
                }
                
            }
            
            return false;
            
        }
        
        
        
        function matches(topOfStack, closedParenthesis) {
            
            for (var k = 0; k < tokens.length; k++) {
                
                if (tokens[k][0] === topOfStack && tokens[k][1] === closedParenthesis) {
                    
                    return true;
                    
                }
                
            }
            
            return false;
            
        }
        
        clearNonCBIElementErrors();
        
        if (isCBIAllowed === 'No') {
            
            if (cbi) {
                
                $('[id$=customMsgAlert]').show();
                $('[id$=btnSave]').prop('disabled', false); //Ravee Racharla 10/25/2018 Construct Page 
                return false; // VV added on 1-26-18
                
            } else {
                
                $('[id$='+btnparam+']').click();
                
            }
            
        } else {
            
            if (cbi) {
                
                $('[id$=customMsgAlert]').show();
                $('[id$=btnSave]').prop('disabled', false); //Ravee Racharla 10/25/2018 Construct Page 
                return false;  // VV added on 1-26-18
                
            } else {
                
                $('[id$='+btnparam+']').click();
                
            }
            
        }
    }catch(error){
        alert(error);
    }
    
}

function clearNonCBIElementErrors()
{
    try{
        //remove any previously triggered errors on picklists from the page
        $("select").each(function() {
            
            $(this).parent().find('.errorMsg').remove();  //Arif
            
        });
        
    }
    catch(error)
    {
        alert(error);
    }    
    
}