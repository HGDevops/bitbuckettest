$(document).ready(function(){
    //This functionality removes the btn class so that we can fully customize the apex:commandButton directive. Since it adds the btn class when the document is ready.
    // we can reuse this in the bottom of the output panels when the body rerenders on proceed with application. 
       removeBtnClass();

});

function removeBtnClass(){
    $("input.btn").addClass("customButton customButtonGreen");
    $("input.btn.customButton.customButtonGreen").removeClass("btn");
    $("input.btn.customButton.customButtonBlue").removeClass("btn");

}

function check(){
    //first run this should be empty
    //var temp= document.getElementsByClassName('errorMsg');
    //if subsequent run hide it, we might not need it
    //for(j=0;j<temp.length;j++)
    //   temp.style.display='none';

    //get array of elements with requiredInput in their class name, how are these being marked with this class, I don't see any code that does this.
       var temp11= document.getElementsByClassName('requiredInput');          
          for(var i=0;i<temp11.length;i++){
              var valid = document.getElementById(temp11[i].id);

              //clear bordercolor
              //Get the value of style attribute based on element's Id
              var validStyle = valid.getAttribute('style'); 

              if(validStyle != null){
                  validStyle = null;
              }

                if(valid.title.indexOf('Lookup') == -1){
                    if(valid.value === undefined || valid.value == null || valid.value == ''){
                         valid.style.borderColor = "red";
                    } 
                }
          }
        }     
//These two functions work with the openLookupFunction on the page
function matchLookupElement(element, textbox){
    var result = false;
    for(var i=0;i<element.length;i++){
              var valid = document.getElementById(element[i].id);
              var validName = valid.getAttribute('name');   
              if(textbox == validName){
                  result = true;
                  break;
              }
    }
    return result; 
}
function customPickerReturn(formtag, lkid, textbox, recordID, recordName, flag){
    var section;
    var importermatch = matchLookupElement(document.getElementsByClassName('Importer_Last_Name__c'), textbox);
    var exportermatch = matchLookupElement(document.getElementsByClassName('Exporter_Last_Name__c'), textbox);
    var delivrecipmatch = matchLookupElement(document.getElementsByClassName('DeliveryRecipient_Last_Name__c'), textbox);
    var handCarryrecipmatch = matchLookupElement(document.getElementsByClassName('Hand_Carrier_Last_Name__c'), textbox);

    if (importermatch) section = 'Importer';
    if (exportermatch) section = 'Exporter';
    if (delivrecipmatch) section = 'Delivery Recipient';
    if (handCarryrecipmatch) section = 'Hand Carrier';

    popLastNameLookup(section, recordID);
}
function populateApplicant(sectNumber, sectName){
    popApplicant(sectName, sectNumber);
}

function confirmStartOver() {
    return confirm('Are you sure? Your work will be lost.');
}

function confirmDeleteAttachment() {
    return confirm('Are you sure you want to delete this document?');
}

function confirmDeleteImage() {
    return confirm('Are you sure you want to delete this image?');
}

function openDatePicker(objField){ 
        $(objField).datepicker('destroy').datepicker({
            inline: true,
            showOtherMonths: true,
            dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],        
            showOn: 'both'
        }).focus();
}

function ValidateDate(dtValue)
{
var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
    if(dtRegex.test(dtValue)==false){
        alert('Please enter a valid date format: mm/dd/yyyy');
    }
}

function colorPageBlock(pageblock, color) {
if (pageblock != null) pageblock.firstChild.style.cssText = "background-color: " + color + ";";

}

