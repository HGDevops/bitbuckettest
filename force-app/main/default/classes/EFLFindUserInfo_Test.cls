@isTest(seeAllData=false)
public class EFLFindUserInfo_Test{    
    @testSetup
    public static void setupInitialData() {             
        String accRecordTypeId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('AC R&L Account').getRecordTypeId();
        list<account> delaccts = [select id from account where RecordTypeId = :accRecordTypeId and EFLAC_Access_Code__c !=null];
        for (account a: delaccts){
            delete a;
        }
        Account testAcc = new Account();
        testAcc.RecordTypeId = accRecordTypeId;
        testAcc.Name = 'Test Account';
        Database.insert(testAcc);       
        
        
        Contact ACISCon = new Contact();
        ACISCon.LastName = 'Test Contact';
        ACISCon.FirstName = 'ACIS';
        ACISCon.EFL_Role__c= 'CEO';
        ACISCon.AccountID = testAcc.id;
        Database.insert(ACISCon); 
        
        Contact JITCon = new Contact();
        JITCon.LastName = 'Test Contact';
        JITCon.FirstName = 'Name';
        JITCon.EFL_Role__c= 'CEO';
        JITCon.AccountID = testAcc.id;
        JITCon.EFL_ACIS_Contact__c = ACISCon.id;
        Database.insert(JITCon);      
        
        
        User usershare = new User();
        usershare.ProfileID = [Select Id From Profile Where Name='Report Preparer'].id;
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.LanguageLocaleKey = 'en_US';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.FirstName = 'first';
        usershare.LastName = 'last';
        usershare.Username = 'testUsername1@test.com';   
        usershare.CommunityNickname = 'testUser123';
        usershare.Alias = 't1';
        usershare.Email = 'no@email.com';
        usershare.IsActive = true;
        usershare.ContactId = JITCon.id;
        Database.insert(usershare);   
        
        EFLRegistration__c Reg = new EFLRegistration__c();
        Reg.EFLAccount__c =testAcc.id;
        reg.EFLRegistrationNumber__c = '11-P-2019';
        Database.insert(Reg);
        
        EFLAnnual_Report__c annRep = new EFLAnnual_Report__c();
        annRep.EFLRegistration__c = Reg.id;
        annRep.EFLSites__c = 'Site1,Site2,Site3';
        annRep.EFLAccount__c =testAcc.id; 
        Database.insert(annRep);
        
        EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
        regAnimal.EFLAnnual_Report__c = annRep.id;
        regAnimal.EFLSelectedForReport__c = true;
        regAnimal.EFLAnimal__c = 'Other Animals';
        regAnimal.ELFColumnEPainNotMinimized__c = 100;
        Database.insert(regAnimal); 
        integer countAnmls = [select count() from EFLRegistered_Animal__c];
    }
    @isTest
    public static void TestMethod1(){   
        user usershare = [select id,contactId from user where Username = 'testUsername1@test.com'];
        contact JITCon = [select id from contact where LastName = 'Test Contact' and FirstName = 'Name' and EFL_Role__c= 'CEO'];
        EFLRegistration__c reg = [select id, EFLAccount__r.EFLAC_Access_Code__c, EFL_Account_FA_Access_Code__c,EFLRegistrationNumber__c from EFLRegistration__c where EFLRegistrationNumber__c='11-P-2019'];        
        system.runas (usershare){ 
            Test.startTest();
            System.assertequals(EFLFindUserInfo.findInfo(String.valueOf(reg.EFLRegistrationNumber__c),String.valueOf(reg.EFL_Account_FA_Access_Code__c)).Id,reg.Id);         
            EFLFindUserInfo.regUserUpdate(JITCon.Id,usershare.Id); 
            System.assertequals(JITCon.Id,usershare.contactId);
            System.assertequals(EFLFindUserInfo.checkContactAccess().Id,usershare.contactId);             
            System.assertequals(EFLFindUserInfo.fetchUser().Id,usershare.Id);
            test.StopTest();
        }
    }
    @isTest
    public static void TestMethod2(){   
        user usershare = [select id,contactId from user where Username = 'testUsername1@test.com'];        
        contact JITCon = [select id, lastname, firstname from contact where LastName = 'Test Contact' and  FirstName = 'Name' and EFL_Role__c= 'CEO'];
        JITCon.FirstName = 'first';
        JITCon.LastName = 'last';
        update JITCon;
        EFLRegistration__c reg = [select id, EFLAccount__r.EFLAC_Access_Code__c, EFL_Account_FA_Access_Code__c,EFLRegistrationNumber__c from EFLRegistration__c where EFLRegistrationNumber__c='11-P-2019'];        
        system.runas (usershare){ 
            Test.startTest();
            System.assertequals(EFLFindUserInfo.findInfo(String.valueOf(reg.EFLRegistrationNumber__c),String.valueOf(reg.EFL_Account_FA_Access_Code__c)).Id,reg.Id);         
            EFLFindUserInfo.regUserUpdate(JITCon.Id,usershare.Id); 
            System.assertequals(JITCon.Id,usershare.contactId);
            //System.assertequals(EFLFindUserInfo.checkContactAccess().Id,usershare.contactId);             
            System.assertequals(EFLFindUserInfo.fetchUser().Id,usershare.Id);
            test.StopTest();
        }
    }    
    @isTest
    public static void TestMethod3(){   
        String accRecordTypeId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('AC R&L Account').getRecordTypeId();
        Account testAcc2 = new Account();
        testAcc2.RecordTypeId = accRecordTypeId;
        testAcc2.Name = 'Test Account2';
        Database.insert(testAcc2);
        user usershare = [select id,contactId from user where Username = 'testUsername1@test.com'];        
        contact JITCon = [select id, lastname, firstname from contact where LastName = 'Test Contact' and  FirstName = 'Name' and EFL_Role__c= 'CEO'];
        JITCon.accountid = 	testAcc2.id;
        update jitcon;
        EFLRegistration__c reg = [select id, EFLAccount__r.EFLAC_Access_Code__c, EFL_Account_FA_Access_Code__c,EFLRegistrationNumber__c from EFLRegistration__c where EFLRegistrationNumber__c='11-P-2019'];        
        system.runas (usershare){ 
            Test.startTest();
            System.assertequals(EFLFindUserInfo.findInfo(String.valueOf(reg.EFLRegistrationNumber__c),String.valueOf(reg.EFL_Account_FA_Access_Code__c)).Id,reg.Id);         
            EFLFindUserInfo.regUserUpdate(JITCon.Id,usershare.Id); 
            System.assertequals(JITCon.Id,usershare.contactId);
            System.assertequals(EFLFindUserInfo.checkContactAccess().Id,usershare.contactId);             
            System.assertequals(EFLFindUserInfo.fetchUser().Id,usershare.Id);
            test.StopTest();
        }
    }     
}