public class FileIntegrationServiceProviders extends VisualEditor.DynamicPickList{
    public override VisualEditor.DataRow getDefaultValue(){        
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('-none-', '');
        return defaultValue;
    }
    
    public override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows  myValues = new VisualEditor.DynamicPickListRows();
        myValues.addRow(new VisualEditor.DataRow('-none-', ''));
        for(File_Integration_Service_Provider__mdt p : 
            [SELECT MasterLabel
             FROM File_Integration_Service_Provider__mdt]){
                 myValues.addRow(new VisualEditor.DataRow(p.MasterLabel, p.MasterLabel));
             }
        return myValues;
    }
}