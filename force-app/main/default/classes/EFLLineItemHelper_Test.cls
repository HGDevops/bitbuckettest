@isTest 
private class EFLLineItemHelper_Test {
    static testMethod void validatecopyToApplication() {
        CARPOL_AC_TestDataManager dataHandler = new CARPOL_AC_TestDataManager();
        dataHandler.insertcustomsettings();
        Application__c app = dataHandler.newapplication();
        AC__c lineItem = dataHandler.newLineItem('Adoption', app);
        
        system.assert(lineItem.Importer_Last_Name__c == [Select Importer_Last_Name__c from application__c where id =: app.Id].Importer_Last_Name__c);
        EFLLineItemHelper.resetRunOnceFlag();
        Applicant_Contact__c appcont = dataHandler.newappcontact();
        lineItem.Importer_Last_Name__c = appcont.Id;
        update lineItem;
        system.assert(lineItem.Importer_Last_Name__c == [Select Importer_Last_Name__c from application__c where id =: app.Id].Importer_Last_Name__c);
        //Authorizations__c auth = dataHandler.newAuth(app.id);
        
        
    }
}