@isTest
public class CARPOL_ACLD_TestDataManager {
    
    public id dogProgramPathwayId {get; set;}
    public id brsProgramPathwayId {get; set;}
    public id acThumbprintId {get; set;}
    
    public CARPOL_ACLD_TestDataManager(){
        dogProgramPathwayId = newDogsPathway().id;
        brsProgramPathwayId = newBRSPathway().id;
        acThumbprintId = newThumbprint(newProgramPrefix(newProgram('AC').id).id).id;
    }
    
    public account newAccount(string AccRecordTypeId){
		Account acct = new  Account();
		acct.Name='ACLD Account';
		acct.RecordTypeId=AccRecordTypeId;
		Insert acct;
		System.assert(acct!=null);
		return acct;
	}
    
    public Contact newContact(id AccountId){
		Contact Cont = new  Contact();
		Cont.FirstName='Global Contact 1';
		Cont.LastName='LastName 1';
		Cont.Email='test@email.com';
		Cont.MailingStreet='Mailing Street 1';
		Cont.MailingCity='Mailing City 2';
		Cont.MailingState='Ohio';
		Cont.MailingCountry='United States';
		Cont.MailingPostalCode='32092';
        Cont.AccountId = AccountId;
		Insert Cont;
		System.assert(Cont!=null);
		return Cont;
	}
    
    public user newPortalUser(id contactId){
        user portalUser = new User();
        portalUser.Username ='orgadmin12192018@test.com';
        portalUser.LastName = 'orgadmin12192018';
        portalUser.Email = 'orgadmin12192018@test.com';
        portalUser.alias = 'org19201';
        portalUser.TimeZoneSidKey = 'America/New_York';
        portalUser.LocaleSidKey = 'en_US';
        portalUser.EmailEncodingKey = 'ISO-8859-1';
        portalUser.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        portalUser.LanguageLocaleKey = 'en_US';
        portalUser.ContactId = contactId;
        insert portalUser;
        return portalUser;
    }
    
    public Domain__c newProgram(String prog){
     Domain__c objProg = new Domain__c();
     objProg.Name = prog;
     objProg.Active__c = true;
     insert objProg;
     System.assert(objProg != null);     
     return objProg;
 	}
    
    public Program_Prefix__c newProgramPrefix(id programId){
        Program_Prefix__c pp = new Program_Prefix__c(Name='1', Program__c=programId);
        insert pp;
        System.assert(pp!=null);
		return pp;
    }
    
    public Signature__c newThumbprint(id programPrefixId){
        id thumbprintRecordtypeId = EFLGenericUtility.getRecordTypeId('Signature AC');
        Signature__c thumbprint = new Signature__c(Name='AC - Live Dogs for Resale CONUS - Permit', Program_Prefix__c=programPrefixId, Status__c='Active', recordtypeId=thumbprintRecordtypeId);
        insert thumbprint;
        System.assert(thumbprint!=null);
		return thumbprint;
    }
    
    public Program_Line_Item_Pathway__c newDogsPathway(){
        
        Program_Line_Item_Pathway__c parentPathway = new Program_Line_Item_Pathway__c();
        parentPathway.Program__c = newProgram('VS').Id;
        parentPathway.Name = 'Live Animals, Embryos, Semen and Cloning Tissue';
        parentPathway.Status__c = 'Active';  
        parentPathway.Movement_Type__c = 'Import; Transit';
        parentPathway.Effective_Date_Equals__c = 'Date of Arrival';
        insert parentPathway;
    
        Program_Line_Item_Pathway__c pathway = new Program_Line_Item_Pathway__c();
        pathway.Program__c = newProgram('AC').Id;
        pathway.Name = 'Dogs';       
        pathway.Parent_Id__c = parentPathway.Id;
        pathway.Status__c = 'Active';  
        pathway.Movement_Type__c = 'Import; Transit';        
        parentPathway.Effective_Date_Equals__c = 'Date of Arrival';
        pathway.Is_Country_Required__c = true;
        pathway.Is_State_of_Destination_Required__c = true;
        pathway.Show_Conditions_Required_for__c = 'Import; Transit'; 
        pathway.Validate_for_Disease__c = 'Rabies';
        pathway.Show_Required_Documents_Button__c = true;
        pathway.Show_Photos_Button__c = true;
        insert pathway;
    	System.assert(pathway!=null);
		return pathway;
 	}  
    
    public Program_Line_Item_Pathway__c newBRSPathway(){
		Program_Line_Item_Pathway__c objParentPathway=new  Program_Line_Item_Pathway__c();
		objParentPathway.Program__c=newProgram('BRS').Id;
		objParentPathway.Name='Genetically Engineered Organism Parent';
		System.assert(objParentPathway!=null);
		Insert objParentPathway;
		Program_Line_Item_Pathway__c objPathway=new  Program_Line_Item_Pathway__c();
		objPathway.Program__c=newProgram('BRS').Id;
		objPathway.Name='Genetically Engineered Organism';
		objPathway.Parent_Id__c=objParentPathway.Id;
		objPathway.Is_Country_Required__c=true;
		objPathway.Is_State_of_Destination_Required__c=true;
		objPathway.Show_Intended_Use__c=true;
		objPathway.Validate_for_Disease__c='Rabies';
		objPathway.Show_Required_Documents_Button__c=true;
		objPathway.Show_Photos_Button__c=true;
		objPathway.Record_Type__c='Biotechnology Regulatory Services - Standard Permit';
		Insert objPathway;
		return objPathway;
	}
    
    public Regulations_Association_Matrix__c newRegulationAssociationMatrix(id thumbprintId, id programLineItemPathwayId){
        Regulations_Association_Matrix__c regulationAssociationMatrix = new Regulations_Association_Matrix__c();
        regulationAssociationMatrix.Thumbprint__c = thumbprintId;
        regulationAssociationMatrix.Program_Line_Item_Pathway__c = programLineItemPathwayId;
        insert regulationAssociationMatrix;
        return regulationAssociationMatrix;
    }
    
    public Regulation__c newRegulation(id programId){
        Regulation__c regulation = new Regulation__c();
        regulation.Program__c = programId;
        regulation.Status__c = 'Active';
        regulation.Title__c = 'New Regulation';
        regulation.Custom_Name__c = 'Resale/Adoption > 6 months';
        regulation.Short_Name__c = 'Resale/Adoption > 6 months';
        insert regulation;
        return regulation;
    }
    
    public Signature_Regulation_Junction__c newSignatureRegulationJunction(id decisionMatrixId, id regulationId){
        Signature_Regulation_Junction__c signatureRegulationJunction = new Signature_Regulation_Junction__c();
        signatureRegulationJunction.Decision_Matrix__c = decisionMatrixId;
        signatureRegulationJunction.Regulation__c = regulationId;
        insert signatureRegulationJunction;
        return signatureRegulationJunction;
    }
    
    public Level_1_Region__c newlevel1regionVA(){
		Level_1_Region__c level1Region =new  Level_1_Region__c();
		level1Region.Country__c=newcountryus().Id;
		level1Region.Name='Virginia';
		level1Region.Level_1_Name__c='State';
		level1Region.Level_1_Region_Code__c='VA';
		level1Region.Level_1_Region_Status__c='Active';
		Insert level1Region;
		System.assert(level1Region!=null);
		return level1Region;
	}
    
    public Level_1_Region__c newlevel1regionHI(){
		Level_1_Region__c level1Region =new  Level_1_Region__c();
		level1Region.Country__c=newcountryus().Id;
		level1Region.Name='Hawaii (HI)';
		level1Region.Level_1_Name__c='State';
		level1Region.Level_1_Region_Code__c='HI';
		level1Region.Level_1_Region_Status__c='Active';
		Insert level1Region;
		System.assert(level1Region!=null);
		return level1Region;
	}
    
    public Trade_Agreement__c newta(){
		Trade_Agreement__c ta = new  Trade_Agreement__c();
		ta.Name='test ta';
		ta.Trade_Agreement_Code__c='ta';
		ta.Trade_Agreement_Status__c='Active';
		Insert ta;
		System.assert(ta!=null);
		return ta;
	}
        
    public Group__c newGroup(){
        id countriesGroupACLDRecordType = EFLGenericUtility.getRecordTypeId('Countries Group AC LD');
        Group__c grp = new  Group__c();
		grp.Name='Origin';
		grp.RecordTypeId=countriesGroupACLDRecordType;
		grp.Group_Custom_Name__c='Origin';
		grp.Status__c='Active';
		Insert grp;
		System.assert(grp!=null);
		return grp;
	}
    
    public Country__c newcountry(string name, string code){
		Country__c country = new  Country__c();
		country.Name=name;
		country.Country_Code__c=code;
		country.Country_Status__c='Active';
		country.Trade_Agreement__c=newta().Id;
		Insert country;
		System.assert(country!=null);
		return country;
	}
        
    public Country_Junction__c newCountryJunction(Country__c cntry,Group__c grp){
		Country_Junction__c countryJunction = new  Country_Junction__c();
		countryJunction.Country__c=cntry.Id;
		countryJunction.Group__c=grp.Id;
		Insert countryJunction;
		System.assert(countryJunction!=null);
		return countryJunction;
	}
    
    public Country__c newcountryus(){
		Country__c country = new  Country__c();
		country.Name='United States of America';
		country.Country_Code__c='US';
		country.Country_Status__c='Active';
		country.Trade_Agreement__c=newta().Id;
		Insert country;
		System.assert(country!=null);
		return country;
	}
    
    public void loadPSQRelatedCustomSetting(){
        List<CARPOL_URLs__c> URLs = new List<CARPOL_URLs__c>();
        CARPOL_URLs__c URL = new CARPOL_URLs__c(Name='External',URL__c='https://efldevshr-aphis-efile.cs32.force.com');
        URLs.add(URL);
        URL = new CARPOL_URLs__c(Name='Landing_Page',URL__c='/apex/CARPOL_CommunityLandingPage');
        URLs.add(URL);
        insert URLs;        
    }
    
    public void createACLiveDogsPermitQuestionsAndSelections(){
        string startQuestionExternalId =  'startQuestionExternalId';
        string countryOfOrginQuestionExternalId =  'countryOfOrginQuestionExternalId';
    	string stateOfDestinationQuestionExternalId =  'stateOfDestinationQuestionExternalId';
        string permitted1QuestionExternalId = 'permitted1QuestionExternalId';
        string permitted2QuestionExternalId = 'permitted2QuestionExternalId';
        string permitted3QuestionExternalId = 'permitted3QuestionExternalId';
        
    	id wizardQuestionnaireBRSRecorType = EFLGenericUtility.getRecordTypeId('Wizard Questionnaire BRS');
        id wizardQuestionnaireACRecorType = EFLGenericUtility.getRecordTypeId('Wizard Questionnaire AC');
        
        List<Wizard_Questionnaire__c> questions = new List<Wizard_Questionnaire__c>();
        
        //PSQ_Start
        Wizard_Questionnaire__c startQuestion = new Wizard_Questionnaire__c(
                                                recordtypeid=wizardQuestionnaireBRSRecorType,
                                                Question__c='Select one of the options below to begin the first phase of the application process.',
                                                Type_of_Options__c = 'LIST',
                                                Type__c = 'PSQ START',
                                                Movement_Type__c = 'Import; Transit; Register; Interstate Movement; Release; Interstate Movement and Release; Export',
                                                Question_Classification__c = 'Movement Type',	
                                                Application_Mapping_Index__c = 'MovementType',
                                                Status__c = 'Active',
                                                Domain__c = 'BRS',
            									External_Migration_Id__c = startQuestionExternalId
        								 	);
        questions.add(startQuestion);
        
        //Country Of Orgin Question
        Wizard_Questionnaire__c cooQuestion = new Wizard_Questionnaire__c(
                                                recordtypeid=wizardQuestionnaireACRecorType,
                                                Question__c='Please select the Country of Origin',
                                                Type_of_Options__c = 'LIST',
                                                Type__c = 'INFORMATION',
                                                Movement_Type__c = 'Import; Transit',
                                                Question_Classification__c = 'Movement Type',	
                                                Application_Mapping_Index__c = 'CountryofOrigin',
                                                Status__c = 'Active',
                                                Domain__c = 'AC',
            									Program_Line_Item_Pathway__c = dogProgramPathwayId,
            									External_Migration_Id__c = countryOfOrginQuestionExternalId,
            									PSQActivity_Processor__c = 'PSQActivity00001'
        								 	);
        questions.add(cooQuestion);
        
        //State of Destination Question
        Wizard_Questionnaire__c sodQuestion = new Wizard_Questionnaire__c(
                                                recordtypeid=wizardQuestionnaireACRecorType,
                                                Question__c='Please select the US state/province of destination',
                                                Type_of_Options__c = 'LIST',
                                                Type__c = 'INFORMATION',
                                                Movement_Type__c = 'Import; Transit',
                                                Question_Classification__c = 'Movement Type',	
                                                Application_Mapping_Index__c = 'StateOfDestination',
                                                Status__c = 'Active',
                                                Program_Line_Item_Pathway__c = dogProgramPathwayId,
            									External_Migration_Id__c = stateOfDestinationQuestionExternalId,
            									PSQActivity_Processor__c = 'PSQActivity00002'
        								 	);
        questions.add(sodQuestion);
        
        id hiStateId = newlevel1regionHI().id;
        newlevel1regionVA();
        group__c grp = newGroup();
        newCountryJunction(newCountry('Australia', 'AU'), grp);
        newCountry('India', 'IN');    
        //Last Question - Permit 1
        Wizard_Questionnaire__c permitted1Question = new Wizard_Questionnaire__c(
                                                recordtypeid=wizardQuestionnaireACRecorType,
                                                Question__c='You need a permit for this request. Please click on show conditions to see if you meet the regulatory requirements for importation. To continue your application click on the proceed with application button.',
                                                Type_of_Options__c = 'TEXT',
                                                Type__c = 'PERMITTED',
                                                Movement_Type__c = 'Import; Transit',
                                                Type_of_Permit__c = 'Standard Permit',	
                                                Status__c = 'Active',
                                                Program_Line_Item_Pathway__c = dogProgramPathwayId,
            									External_Migration_Id__c = permitted1QuestionExternalId,
            									Signature__c = acThumbprintId,
            									Country_Group__c = grp.id,
            									State_Province_of_Destination__c = hiStateId
        								 	);
        questions.add(permitted1Question);
        
        //Last Question - Permit 2
        Wizard_Questionnaire__c permitted2Question = new Wizard_Questionnaire__c(
                                                recordtypeid=wizardQuestionnaireACRecorType,
                                                Question__c='You need a permit for this request. Please click on show conditions to see if you meet the regulatory requirements for importation. To continue your application click on the proceed with application button.',
                                                Type_of_Options__c = 'TEXT',
                                                Type__c = 'PERMITTED',
                                                Movement_Type__c = 'Import; Transit',
                                                Type_of_Permit__c = 'Standard Permit',	
                                                Status__c = 'Active',
                                                Program_Line_Item_Pathway__c = dogProgramPathwayId,
            									External_Migration_Id__c = permitted2QuestionExternalId,
            									Signature__c = acThumbprintId,
            									State_Province_of_Destination__c = hiStateId
        								 	);
        questions.add(permitted2Question);
        
        //Last Question - Permit 3
        Wizard_Questionnaire__c permitted3Question = new Wizard_Questionnaire__c(
                                                recordtypeid=wizardQuestionnaireACRecorType,
                                                Question__c='You need a permit for this request. Please click on show conditions to see if you meet the regulatory requirements for importation. To continue your application click on the proceed with application button.',
                                                Type_of_Options__c = 'TEXT',
                                                Type__c = 'PERMITTED',
                                                Movement_Type__c = 'Import; Transit',
                                                Type_of_Permit__c = 'Standard Permit',	
                                                Status__c = 'Active',
                                                Program_Line_Item_Pathway__c = dogProgramPathwayId,
            									External_Migration_Id__c = permitted3QuestionExternalId,
            									Signature__c = acThumbprintId
            								);
        questions.add(permitted3Question);
        
        List<Wizard_Selections__c> selections = new List<Wizard_Selections__c>();
        
        //Import Option
        Wizard_Selections__c importSelection = new Wizard_Selections__c(
            									Name = 'Import',
            									Value__c = 'Import',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = startQuestionExternalId),
            									Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = countryOfOrginQuestionExternalId),
            									Display_Order__c = 1
        									); 
        selections.add(importSelection);
        
        //Transit Option
        Wizard_Selections__c transitSelection = new Wizard_Selections__c(
            									Name = 'Transit',
            									Value__c = 'Transit',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = startQuestionExternalId),
            									Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = countryOfOrginQuestionExternalId),
            									Display_Order__c = 2
        									); 
        selections.add(transitSelection);
        
        //Country Of Orgin Option
        Wizard_Selections__c cooSelection = new Wizard_Selections__c(
            									Name = 'Forward to State Of Destination',
            									Value__c = 'Forward to State Of Destination',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = countryOfOrginQuestionExternalId),
            									Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = stateOfDestinationQuestionExternalId),
            									Display_Order__c = 1,
            									Activity_Processor_Result__c = '1'
        									); 
        selections.add(cooSelection);
        
        //State Of Destination Option
        Wizard_Selections__c sodSelection1 = new Wizard_Selections__c(
            									Name = 'Forward to Last Question 1',
            									Value__c = 'Forward to Last Question 1',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = stateOfDestinationQuestionExternalId),
            									Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = permitted1QuestionExternalId),
            									Display_Order__c = 1,
            									Activity_Processor_Result__c = '1'
        									); 
        selections.add(sodSelection1);
        Wizard_Selections__c sodSelection2 = new Wizard_Selections__c(
            									Name = 'Forward to Last Question 2',
            									Value__c = 'Forward to Last Question 2',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = stateOfDestinationQuestionExternalId),
            									Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = permitted2QuestionExternalId),
            									Display_Order__c = 2,
            									Activity_Processor_Result__c = '1'
        									); 
        selections.add(sodSelection2);
        Wizard_Selections__c sodSelection3 = new Wizard_Selections__c(
            									Name = 'Forward to Last Question 3',
            									Value__c = 'Forward to Last Question 3',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = stateOfDestinationQuestionExternalId),
            									Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = permitted3QuestionExternalId),
            									Display_Order__c = 3,
            									Activity_Processor_Result__c = '1'
        									); 
        selections.add(sodSelection3);
        
        /*//Last Question Option
        Wizard_Selections__c permittedQuestionSelection = new Wizard_Selections__c(
            									Name = 'Forward to Last Question',
            									Value__c = 'Forward to Last Question',
            									Wizard_Questionnaire__r = new Wizard_Questionnaire__c(External_Migration_Id__c = permittedQuestionExternalId),
            									//Wizard_Next_Question__r = new Wizard_Questionnaire__c(External_Migration_Id__c = lastQuestionExternalId),
            									Display_Order__c = 1,
            									Activity_Processor_Result__c = '1'
        									); 
        selections.add(permittedQuestionSelection);*/
        
        insert questions;
        insert selections;
        
    }
    
    public void createAnimalTransportationSectionAndFields(){
        Program_Line_Item_Section__c section = new Program_Line_Item_Section__c(
            Name='Animal Transportation Information', 
            Type_of_Permit__c='Standard Permit', 
            Section_Order__c=2, 
            Include_Applicant_Information__c=false, 
            EFLAddMore__c=false, 
            Program_Line_Item_Pathway__c=dogProgramPathwayId, 
            RecordTypeId=EFLGenericUtility.getRecordTypeId('Program Pathway Section LineItem'),
            Status__c = 'Active'
        );
        insert section;
        
        List<Program_Line_Item_Field__c> fields = new List<Program_Line_Item_Field__c>();
        
        Program_Line_Item_Field__c field = new Program_Line_Item_Field__c(
            Name='Transporter Name', Always_Required__c=false, 
            Field_Help_Text__c='Please enter the Name of the Ground, Sea, or Air Transporter.', 
            Field_Order__c=1, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Transporter_Name__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Transporter Type', Always_Required__c=true, 
            Field_Help_Text__c='Please select correct Transporter type of Ground, sea, or air.', 
            Field_Order__c=2, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Transporter_Type__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Port of Entry', Always_Required__c=true, 
            Field_Help_Text__c='Please choose the port of entry.', 
            Field_Order__c=4, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Port_of_Entry__c'            
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Air Transporter Flight Number', Always_Required__c=false, Dependent_on__c='4', 
            Field_Help_Text__c='If transported by air, please provide flight number(s).', 
            Field_Order__c=8, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Air_Transporter_Flight_Number__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Departure Date', Always_Required__c=true, Display_Name__c='Departure Date', 
            Field_Help_Text__c='Please select a departure date that is either today or a date in the future.', 
            Field_Order__c=9, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Departure_Time__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
        	Name='Proposed date of arrival', Always_Required__c=true, Display_Name__c='Proposed Date of Arrival', 
            Field_Help_Text__c='Please enter proposed date of arrival in the continental United States or Hawaii.', 
            Field_Order__c=10, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Proposed_date_of_arrival__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Departure Time', Always_Required__c=true, Display_Name__c='Departure Time', 
            Field_Help_Text__c='Please provide the departure time.', Field_Order__c=11, isActive__c='Yes', 
            Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='AC_Departure_Time__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Arrival Time', Always_Required__c=true, 
            Field_Help_Text__c='please provide arrival date and time.', 
            Field_Order__c=12, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='AC_Arrival_Time__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Port of Export', Always_Required__c=false, Display_Name__c='Port of Export', 
            Field_Help_Text__c='Please choose the port of embarkation', Field_Order__c=13, 
            isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Port_of_Embarkation__c'
        );
        fields.add(field);
        
        field = new Program_Line_Item_Field__c(
            Name='Port If Not Listed', Always_Required__c=false, 
            Field_Help_Text__c='Please type the port of Export if not listed in the list of ports.', 
            Field_Order__c=14, isActive__c='Yes', Program_Line_Item_Section__c=section.id, Read_Only__c=false, 
            Movement_Type__c = 'Import; Transit',
            Field_API_Name__c='Port_If_Not_Listed__c'
        );
        fields.add(field);
        
        insert fields;        
        
    }    

}