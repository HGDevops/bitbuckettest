global without sharing class EFLCreatePackageSign{
    
    WebService static String PackageAutoCreate(String sAssID, String sAssName, String attachId, String pkgName) {
        
        String sURl = '/apex/esl__Package?ParentId='+ sAssID +'&Name='+ sAssName +'%20Agreement';
        
        //get document ID
        if (attachId == null || attachId == '') {
            String sFileName='test';
            for(Attachment objAtt : [SELECT Body,BodyLength,ContentType, Description,Id, Name,OwnerId,Parent.Name, ParentId FROM Attachment WHERE ParentID=:sAssID]){
                sFileName = objAtt.Name;
                if(sFileName.startsWith('Draft_Permit') || sFileName.startsWith('Acknowledgement Letter_')){
                    sURl = sURl + '&Documents=' + objAtt.id;
                    break;
                }
            }
            
        } else {
            sURl = sURl + '&Documents=' + attachId;           
        }
        
        //get Convention ID
        List<ESL__Convention__c> lstConv = [select id, name from ESL__Convention__c where name=: pkgName];
        if(lstConv.size()>0) {
            sURl = sURl + '&ConventionId=' + lstConv[0].id;  
            
            sURl = sURl + '&Signer1=' + UserInfo.getUserId();
            
            //get Signer record id;
            List<ESL__Signer_Label__c> lstSigner = [select id, name from ESL__Signer_Label__c 
                                                    where name='Signer Label' and ESL__Convention__r.Name =: pkgName];
            if(lstSigner.size()>0)
                sURl = sURl + '&Signer1Label=' + lstSigner[0].id;
            
            //auto prepare the package
            sURl = sURl + '&AutoPrepare='+1;
            //auto send to the signer
            sURl = sURl+'&Send='+1 + '&isdtp=mn';            
        }
        return sURl;
    }

}