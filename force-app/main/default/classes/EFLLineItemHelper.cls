/**
* Class Name: EFLLineItemHelper.cls
*
* Class that contains helper methods for working with LineItem Trigger
* 
*/
public with sharing class EFLLineItemHelper { 
    private static boolean run = true;
    /**
* Method to copy data from Line Item to Application
* 
*/
    public static String copyToApplication(List<Sobject> newList, Map<id,Sobject> oldDataMap) {    
        
        List<Application__c> appUpdateList = new List<Application__c>();
        Map<Id, Application__c> appUpdateMap = new Map<Id, Application__c>();
        
        for (AC__c item: (List<AC__c>)newList) {   
        system.debug('linteItem === ' + item.Id);        
            if (oldDataMap != null) {            
                // This is an update
                AC__c oldac = (AC__c)Trigger.oldMap.get(item.Id);  
                if(item.Importer_Last_Name__c != oldac.Importer_Last_Name__c && item.Importer_Last_Name__c != null && item.Application_Number__c != null) {
                    Application__c app = new Application__c(Id = item.Application_Number__c, 
                                                            Importer_Last_Name__c = item.Importer_Last_Name__c);
system.debug('adding app === ' + app.Id);                                                                                                                    
                    appUpdateList.add(app);
                    appUpdateMap.put(item.Application_Number__c, app);
                    
                }                
            } else {
                // This is an insert                
                if(item.Importer_Last_Name__c != null && item.Application_Number__c != null) {
                    Application__c app = new Application__c(Id = item.Application_Number__c, 
                                                            Importer_Last_Name__c = item.Importer_Last_Name__c);
                    appUpdateList.add(app);  
                    appUpdateMap.put(item.Application_Number__c, app);
system.debug('adding app2 === ' + app.Id);                                     
                }
            }                      
        }
        //if (appUpdateList.size() > 0) {
        if (!appUpdateMap.isEmpty()) {
            // To do Error handling
            //Database.UpsertResult[] srList = Database.upsert(appupdateList, Application__c.Id);
            Database.UpsertResult[] srList = Database.upsert(appUpdateMap.values(), Application__c.Id);
            
        }       
        return null;
    }
    
    /**
* Method to check recursive
* 
*/    
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }

    /**
* Method to reset recursive flag to True
* 
*/     
    public static void resetRunOnceFlag() {
        run = TRUE;
    }    
} // END Of EFLLinteItemHelper