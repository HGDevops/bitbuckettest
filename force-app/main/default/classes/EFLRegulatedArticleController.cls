public with sharing class EFLRegulatedArticleController {
    
   public Id lineItemId {get;set;}
   public string delrecid{get;set;}
   public Map<string,string> sobjectkeys{get;set;}
   public Link_Regulated_Articles__c linkRARecord{get;set;}
   public id linkRAID{get;set;} 
   public Boolean regArtEnableDisable{get;set;}
   public string lraExistError{get; set;}
   public List <AC__c> relatedLineItem{get; set;}
   public list<Link_Regulated_Articles__c> linkRAList{get
    { 
        if(lineItemID!=NULL){ 
            linkRAList = EFLLinkRegulatedArticleRepository.selectLRAByLineItemID(lineItemID);}
        system.debug('linkRAList###'+linkRAList);
       return linkRAList; 
    }set;}
    
    public AC__c lineItemRecord{      
        get{
            if(lineItemRecord==null && LineItemId!=null){ 
                lineItemRecord = efllineitemrepository.selectbyID(lineItemID);
            }
            return lineItemRecord;
        }set;
    }
    public boolean lockLineItem{
        get{
         lockLineItem = EFLLockUnlockUtility.lockLineitem(lineItemRecord);
         return lockLineItem;
        }set; 
    }
	public boolean enableApplicantInstructions{
        get{
         enableApplicantInstructions = EFLLockUnlockUtility.enableApplicantInstructions(lineItemRecord);
         return enableApplicantInstructions;
        }set; 
    }
    public string instructions{ 
        get{
            if(instructions == NULL){
                //instructions =  EFLGenericutility.getInstructions(NULL,NULL,'Regulated Article');  
                instructions =  EFLGenericutility.getInstructions(NULL,NULL,'Regulated Article','LineItem');
            } 
           return instructions; 
        }
        set;
    } 
    
   //Rerender Attributes
    public boolean renderList {get;set;}
    public boolean renderDetail {get;set;}
    public boolean isEdit;
    Construct__c cons = new Construct__c();
    
    public EFLRegulatedArticleController(ApexPages.StandardController controller){ 
       lineItemId=ApexPages.currentPage().getParameters().get('LineItemId');
       relatedLineItem = new List <AC__c>();
       relatedLineItem = [SELECT id, Does_This_Application_Contain_CBI__c FROM AC__c where id =: lineItemId];
       renderDetail = false;
       renderList = true; 
       Map<string,Schema.SobjectType> describe=Schema.getGlobalDescribe();
       sobjectkeys=new  Map<string,string>();
        for(string s:describe.keyset())sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
        
        
    } 
    
    public void showDetail(){
        lraExistError = null;
        linkRARecord = new Link_Regulated_Articles__c();
        system.debug('linkRAID@@'+ linkRAID);
        if(linkRAID!=null){
            regArtEnableDisable = false;
           linkRARecord = EFLLinkRegulatedArticleRepository.selectByID(linkRAID);
        }else{
            regArtEnableDisable = true;
           linkRARecord = new Link_Regulated_Articles__c();
            
        }        
        renderDetail = true;
        renderList = false;
      
    }
    
    public void cancel(){
        linkRAID =null;
        renderDetail = false;
        renderList = true;
        
       
    } 
    
   public void updateLinkRA(){
 
       // W-026338 Try/Catch enterred by Terry 9-30-2018
       // Check if this is already existing and then throw error message
      
           list<Link_Regulated_Articles__c> lraList = new list<Link_Regulated_Articles__c>( [SELECT id,  Regulated_Article__c FROM Link_Regulated_Articles__c 
                         WHERE Line_Item__c = :LineItemId
                               and Regulated_Article__c = :linkRARecord.Regulated_Article__c ]);
           if (lralist!= null && lralist.size()> 0 && linkRARecord.id == Null){
               //throw error messgage that this is alreay existing
                lraExistError = 'You have already selected this Regulated Article. Please choose a different Regulated Article.'; 
                return;
           }  
      
       try{       
         if(linkRARecord.line_Item__c ==null){
              linkRARecord.line_Item__c = lineItemID;
           }
            upsert linkRARecord;
            renderDetail = false;
            renderList = true;
            linkRARecord = null;
           linkRAID = null;
           
           
         linkRAList = EFLLinkRegulatedArticleRepository.selectLRAByLineItemID(lineItemID); 
       }  catch( Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
    }      
    
   public void deleteRecord(){ 
       system.debug('deleteRecord');
       string sobjkey=delrecid.substring(0,3);
       string sobjname=sobjectkeys.get(sobjkey);
       string strqurey='select id from '+sobjname+' where id=:delrecid';
       //cons.deleteConstructRecord();
       list<sobject> lst=database.query(strqurey); 
       Delete lst;
    } 
    
    
}