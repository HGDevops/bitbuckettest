@isTest(seealldata=true)
private class CARPOL_UNI_SubmitApplication_Test { 
    
    static String AccountRecordTypeId;
    static String ImpapcontRecordTypeId;
    static String PortsFacRecordTypeId;
    static String ACAppRecordTypeId;
    static String ACauthRecordTypeId;
    static String ACRegRecordTypeId;
    static Account objacct;
    static Contact objcont;
    static Program_Line_Item_Pathway__c objAcPthway;
    static Applicant_Contact__c apcont;
    static Applicant_Contact__c apcont2;
    static Facility__c fac;
    static Facility__c fac2;
    static Signature__c thumb;
    static Regulation__c newreg1;
    static Regulation__c newreg2;
    static Signature_Regulation_Junction__c srj1;
    static Signature_Regulation_Junction__c srj2;
    static Application__c objapp;
    static List<Application__c> appList;
    static AC__c ac1;
    static Regulation__c objreg1;
    static Regulation__c objreg2;
    static Regulation__c objreg3;
    static Attachment attach;
    static Authorizations__c objauth;
    static Authorization_Junction__c objauthjun1;
    static Authorization_Junction__c objauthjun2;
    static Authorization_Junction__c objauthjun3;
    static list<Link_Regulated_Articles__c>  linkregarticlelist;
    static integer numLRA = 1;
        
    static void init(string programName,string pathwayName) { 
        ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        //objacct = testData.newAccount(AccountRecordTypeId); 
        objcont = testData.newcontact();
        //breed__c objbrd = testData.newbreed(); 
        objAcPthway = new Program_Line_Item_Pathway__c();
        objAcPthway.Program__c = testData.newProgram(programName).Id;
        objAcPthway.Name = pathwayName;
        objAcPthway.Status__c = 'Active';  
        System.assert(objAcPthway != null);     
        insert objAcPthway;
          
        apcont = testData.newappcontact(); 
        //apcont2 = testData.newappcontact();
        fac = testData.newfacility('Domestic Port');  
        //fac2 = testData.newfacility('Foreign Port');
        thumb = testData.newThumbprint();
        thumb.name = 'PPQ--CITES--Fast Track';
        update thumb;
        newreg1 = testData.newRegulation('Import Requirements','Test');
        //newreg2 = testData.newRegulation('Additional Information','Test');        
        srj1 = testData.newSRJ(thumb.id,newreg1.id);
        //srj2 = testData.newSRJ(thumb.id,newreg2.id);             
        objapp = testData.newapplication();
        objapp.Signature__c = thumb.id;
        update objapp;
        appList = new List<Application__c>();
        appList.add(objapp);
        ac1 = testData.newLineItem('Personal use',objapp);        
        ac1.Program_Line_Item_Pathway__c = objAcPthway.ID;
        ac1.Line_Item_Type_Hidden_Flag__c = 'Product';
        ac1.Facility_Building_Type__c = 'Building';
        objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        attach = testData.newattachment(ac1.Id);           
        objauth = testData.newAuth(objapp.Id); 
        ac1.Authorization__c = objauth.id;
        ac1.type_of_permit__c = 'Notification';
        update ac1;
        objapp.Application_Status__c = 'Submitted';
        update objapp;
        objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        //objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        //objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id);
        linkregarticlelist = testData.linkregarticles(ac1.ID,objAcPthway.ID,numLRA);
        
    }
     
    public static testMethod void test1(){        
        
        init('BRS','New Facility');
        Test.startTest();
        CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          ac1.type_of_permit__c = 'Standard Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass1 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass1 != null);
          ac1.type_of_permit__c = 'Courtesy Permit';
          update ac1;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Status__c = 'Submitted';
          update objauth;
          CARPOL_UNI_SubmitApplication extclass2 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass2 != null);
          
          Workflow_task__c wftask = new Workflow_task__c  (Authorization__c= objauth.Id, Program__c = 'AC');
          insert wftask ;
          objapp.Application_Status__c = 'Withdrawn';
          update objapp;

          Test.stopTest();     
      }
      public static testMethod void test2(){        
        
        init('AC','New Facility');
        Test.startTest();
        CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          ac1.type_of_permit__c = 'Standard Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass1 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass1 != null);
          ac1.type_of_permit__c = 'Courtesy Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass2 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass2 != null);
          
          Workflow_task__c wftask = new Workflow_task__c  (Authorization__c= objauth.Id, Program__c = 'AC');
          insert wftask ;
          objapp.Application_Status__c = 'Withdrawn';
          update objapp;

          Test.stopTest();     
    }
    public static testMethod void test3(){        
        
        init('VS','New Facility');
        Test.startTest();
        CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          ac1.type_of_permit__c = 'Standard Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass1 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass1 != null);
          ac1.type_of_permit__c = 'Courtesy Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass2 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass2 != null);
          
          Workflow_task__c wftask = new Workflow_task__c  (Authorization__c= objauth.Id, Program__c = 'AC');
          insert wftask ;
          objapp.Application_Status__c = 'Withdrawn';
          update objapp;

          Test.stopTest();     
    }
    public static testMethod void test4(){        
        
        init('PPQ','Federal Noxious Weeds');
        Test.startTest();
        CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          ac1.type_of_permit__c = 'Standard Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass1 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass1 != null);
          ac1.type_of_permit__c = 'Courtesy Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass2 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass2 != null);
          
          Workflow_task__c wftask = new Workflow_task__c  (Authorization__c= objauth.Id, Program__c = 'AC');
          insert wftask ;
          objapp.Application_Status__c = 'Withdrawn';
          update objapp;

          Test.stopTest();     
    } 
    public static testMethod void test5(){        
        
        init('Operations','New Facility');
        Test.startTest();
        CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          ac1.type_of_permit__c = 'Standard Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass1 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass1 != null);
          ac1.type_of_permit__c = 'Courtesy Permit';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass2 = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass2 != null);
          
          Workflow_task__c wftask = new Workflow_task__c  (Authorization__c= objauth.Id, Program__c = 'AC');
          insert wftask ;
          objapp.Application_Status__c = 'Withdrawn';
          update objapp;

          Test.stopTest();     
    } 
     
    public static testMethod void test6(){        
        
         init('BRS','New Facility');
        Test.startTest();
          ac1.type_of_permit__c = 'Standard Permit';
          ac1.Line_Item_Type_Hidden_Flag__c = ' ';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          Test.stopTest();   
    }    
    
     public static testMethod void test7(){        
         numLRA = 2;
         init('BRS','New Facility');
        Test.startTest();
          ac1.type_of_permit__c = 'Standard Permit';
          ac1.Line_Item_Type_Hidden_Flag__c = ' ';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 

          Test.stopTest();   
    } 
    
   public static testMethod void test8(){        
         numLRA = 0;
         init('BRS','New Facility');
        Test.startTest();
          ac1.type_of_permit__c = 'Standard Permit';
          ac1.Line_Item_Type_Hidden_Flag__c = ' ';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          Test.stopTest();   
    }     

   public static testMethod void test9(){        
         numLRA = 0;
         init('AC','New Facility');
        Test.startTest();
          ac1.type_of_permit__c = 'Standard Permit';
          ac1.Line_Item_Type_Hidden_Flag__c = ' ';
          update ac1;
          CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null); 
          Test.stopTest();   
    }     
    
}