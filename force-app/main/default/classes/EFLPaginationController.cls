public class EFLPaginationController {

public List<sObject> idList{get;set;}
Public List<String> SobjFieldList{get;set;}
Public Integer size{get;set;}     
Public Integer noOfRecords{get; set;} 
public List<SelectOption> paginationSizeOptions{get;set;} 
public string searchString{get;set;}    
public EFLBRSLineItemReviewController pageController {get; set;} 
    
 public EFLPaginationController(){
        size=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        /*paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));*/
    }    
  
  Public ApexPages.StandardSetController setRecords{
    get{
     if(setRecords == null){
        setRecords = new ApexPages.StandardSetController(getIdListRecrds());
        setRecords.setPageSize(size);
        noOfRecords = setRecords.getResultSize();
     }  
      return setRecords;
    }set;
   }
    
   Public List<sObject> getSObjectRecs(){
        List<sObject> sObjList = New List<sObject>();
        for(sObject SObj :(List<sObject>)setRecords.getRecords())
            sObjList.add(SObj); 
        return  sObjList ;   
   }
   
   Public List<String> FieldList{
       get{
       List<String> FieldList = New List<string>();
       FieldList = getSobjtFieldList();
       return FieldList;
       }set;
   }
   
    public List<sObject> getIdListRecrds() {
       List<sObject> IdListRecrds =idList;
       return IdListRecrds;
    }
    
    public List<string> getSobjtFieldList() {
       List<String> FieldList = SobjFieldList;
       return FieldList ;
    }
    
    //Changes the size of pagination
    public PageReference refreshPageSize() {
         setRecords.setPageSize(size);
         return null;
    }
}