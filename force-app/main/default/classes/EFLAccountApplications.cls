public with sharing class EFLAccountApplications {
    public list <Application__c> myAccntApplications { get; set; }
    //public String changeFor{get;set;}
    public EFLAccountApplications(){
        user curruser = [select id, username, contactid from user where id=:UserInfo.getUserId()]; 
        id myAccountid = [SELECT accountid from contact where id =:curruser.contactID].accountid;
        myAccntApplications=[SELECT ID, Name, CreatedDate, Application_Status__c, Application_Type__c, RecordType.Name,Applicant_Name__c,Applicant_Name__r.name 
            FROM Application__c 
            WHERE Applicant_Name__r.AccountId =:myAccountid ORDER BY CreatedDate DESC];
        // myAccntApplications=[SELECT ID, Name, CreatedDate, Application_Status__c, Application_Type__c, RecordType.Name,Applicant_Name__c,Applicant_Name__r.name FROM Application__c ORDER BY CreatedDate DESC];
    }
    public pagereference changeOwner(){
        String changeFor = ApexPages.currentPage().getParameters().get('changeFor');
        System.Debug('changeFor>>>>> '+changeFor); 
        //Pagereference pgr = new Pagereference('/apex/EFLApplChangeOwner?appId='+ApexPages.currentPage().getParameters().get('changeFor'));
        Pagereference pgr = new Pagereference('/apex/EFLApplChangeOwner?appId='+changeFor);
        return pgr;
    }
}