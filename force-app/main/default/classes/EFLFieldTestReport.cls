public with sharing class EFLFieldTestReport {
    
    /*Constants, Variable and Properties - Start*/ 
    
    Public static final string FIELD_TEST_REPORT = 'Field Test Report';
    
    public static final string REPORT_SUBMITTED = 'Submitted';
    
    public static final string FIELD_TEST_REPORT_URL_PARAM = 'field_test_report';
    
    public id authorizationId
    {
        get{
            authorizationID = null;
            if(ApexPages.currentPage().getParameters().get('authId')!=null)
            {
                authorizationID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('authId'));
            }   
            return authorizationID; 
        }
        set;
    }
    
    public id reportSummaryId
    {
        get{
            reportSummaryId = null;
            if(ApexPages.currentPage().getParameters().get('rsId')!=null)
            {
                reportSummaryId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('rsId'));
            }   
            return reportSummaryId; 
        }
        set;
    }
    
    public string reportType
    {
        get{
            reportType = '';
            if(ApexPages.currentPage().getParameters().get('reportType')!=null)
            {
                reportType = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('reportType'));
            }   
            return reportType; 
        }
        set;
    }
    
    public Authorizations__c authorizationRecord {
        get{
            return [Select id, Name,Recordtype.name, Multi_Year_Permit__c, BRS_Introduction_Type__c, BRS_Purpose_of_Permit__c,Expiration_Date__c,
                    Date_Issued__c, Effective_Date__c ,Permit_Number__c, Status__c,Authorization_Type__c,Application_Type__c, Application_CBI__c 
                    From authorizations__c Where Id=:authorizationID limit 1];
        }
        set;   
    }
    
    public string reportTypeName
    {
        get{
            reportTypeName = '';
            if(reportType==FIELD_TEST_REPORT_URL_PARAM)
            {
                reportTypeName = CARPOL_Constants.FIELD_TEST_REPORT;
            }
            return reportTypeName;
        }
        set;
    }
    
    public string reportSummaryName 
    { 
        get{
            reportSummaryName = '';
            if(reportSummary!=null)
            {
                reportSummaryName = reportSummary.Name;   
            }
            return reportSummaryName;
        }
        set;
    }
    
    public Report_Summary__c reportSummary
    {
        get{
            if(reportSummaryId!=null)
            {
                reportSummary =  new Report_Summary__c();
                reportSummary = EFLSelfReportingRepository.getReportSummaryById(reportSummaryId);           
            }   
            
            return reportSummary; 
        }
        set;
    }
    
    public id fieldTestReportRecordTypeId
    {
        get{
            fieldTestReportRecordTypeId = EFLGenericUtility.getRecordTypeId(FIELD_TEST_REPORT);
            return fieldTestReportRecordTypeId;    
        }
        set;
    }
    
    EFLSelfReportingHelper selfReportingHelper = new EFLSelfReportingHelper();  
    
    public boolean checkboxBoolean {get
    {
        if(checkboxBoolean==null)
        {
            checkboxBoolean = false;
        }
        return checkboxBoolean;
    } set;}
    
    public boolean isReportSummarySubmitted {
        get{
            isReportSummarySubmitted = false;
            if(reportSummary!=null)
            {
                if(reportSummary.Status__c==REPORT_SUBMITTED) {
                    isReportSummarySubmitted = true;
                }   
            }
            return isReportSummarySubmitted;
        } 
        set;}
    
    public string applicantName {
        get{
            return UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
           }
        set;
    }
    
    public Self_Reporting__c selfReporting {get;set;}
    
    public boolean isViewMode {
        get{
            isViewMode= false;
            if(ApexPages.currentPage().getParameters().get('isView')!=null)
            {
                string mode = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('isView'));
                if(mode == 'true')
                {
                    isViewMode = true; 
                }
            }   
            return isViewMode; 
        } 
        set;
    }
    
    public boolean showReportDetail {get; set;}
    
    public String country{get;set;}
    
    public String State{get;set;}
    
    public Boolean PrePlantingview{get;set;}
    
    public List<SelectOption> fieldTestReportTypes
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            if(fieldTestReportTypes==null)
            {
                Schema.DescribeFieldResult fieldResult = Self_Reporting__c.Field_Test_Report_Type__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                for( Schema.PicklistEntry f : ple)
                {
                    if(authorizationRecord.Multi_Year_Permit__c==false && f.getLabel()=='Annual (only applicable for multi-year permits)')
                    {
                        continue;
                    }   
                    options.add(new SelectOption(f.getLabel(), f.getValue()));                 
                }   
            }     
            return options;
        }
        set;
    }
    
    public List<SelectOption> plantingQuestions
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            if(plantingQuestions==null)
            {
                Schema.DescribeFieldResult fieldResult = Self_Reporting__c.Planting_Material_Still_Growing__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                for( Schema.PicklistEntry f : ple)
                {
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }
            return options;
        }
    }
    
    public List<SelectOption> terminatedReasons
    {
        get
        {   
            List<SelectOption> options = new List<SelectOption>();
            if(terminatedReasons==null)
            {     
                Schema.DescribeFieldResult fieldResult = Self_Reporting__c.How_was_it_terminated__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                for( Schema.PicklistEntry f : ple)
                {
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }
            return options;
        }
    }
    public List<SelectOption> materialDisposed
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            if(materialDisposed==null)
            {
                Schema.DescribeFieldResult fieldResult = Self_Reporting__c.How_was_material_disposed__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                for( Schema.PicklistEntry f : ple)
                {
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }
            return options;
        }
    }
    
    public String locationIdParam{get;set;}
    
    public Map<string, string> pageErrorMap {get; set;}
    
    public id selfReportId {get; set;}
    
    public id newReportSummaryId {get; set;}
    
    //Paginaltion
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
    
    //Observation
    public Boolean showObservationListPanel {get;set;}
    
    public Boolean showObservationAddEditPanel {get;set;}
    
    public id observationId {get; set;}
    
    public Observation__c observation {get;set;}
    
    public List<Observation__c> observations {get;set;}
    
    //Submission Instruction
    public boolean isSubmissionVisible {get;set;}
    public Boolean cbiValidation {get; set;}
    public string submissionInstruction {
        get{
            if(!cbiValidation){
                submissionInstruction = EFLGenericutility.getInstructions(NULL,NULL,'CBI','Self Reporting');
            }else {
                submissionInstruction = EFLGenericUtility.getMessage('ObservationsMustOnAnnualFieldTestReport');
            }
            
            return submissionInstruction;
        }
        set;
    }   
        
    /*Variable and Properties - End*/ 
    
    /*Constructor - Start*/
    
    public EFLFieldTestReport(ApexPages.StandardController controller) {
    	cbiValidation = true;
        //Initialize the pagination components bound to the page
        initPagination();
        
        //Initialize error map bound to the page
        initializeErrors();        
    }
    
    /*Constructor - End*/
    
    /*Functions and Methods - Start*/
    
    //Initialize the pagination components bound to the page
    public void initPagination()
    {
        //pagination
        size=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        
    }
    
    //Set Controller - Load Planted Location and field test report by AuthId    
    public ApexPages.StandardSetController setCon {
        get
        {
            if(setCon == null) {
                List<Location__c> plantedLocations = new List<Location__c>();
                plantedLocations = EFLSelfReportingRepository.plantedReleaseLocationsByAuth(authorizationId);
                setCon = new ApexPages.StandardSetController(EFLSelfReportingRepository.getFieldTestLocationsAndReportsByPlantedLocations(plantedLocations, fieldTestReportRecordTypeId, reportSummaryId));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }
        set; 
    }
    
    //FieldTestList bound to the page
    public List<Location__c> getFieldTestLocationsAndReports() {   
        return (List<Location__c>) setCon.getRecords();
    } 
    
    //Pagination size setter
    public PageReference refreshPageSize() {
        setCon.setPageSize(size);
        return null;
    }
    
    //Certify Checkbox toggle
    public void callToggleCheckBox() {
        isSubmissionVisible=false;
        integer cbiCount = 0;
        integer cbiDelCount = 0;
        Report_Summary__c temp = [SELECT Id, CBI_Count__c, CBI_Deleted_Count__c FROM Report_Summary__c WHERE Id=:reportSummaryId LIMIT 1];
            cbiCount = !String.isBlank(temp.CBI_Count__c) ? integer.valueOf(temp.CBI_Count__c) :0;
            cbiDelCount = !String.isBlank(temp.CBI_Deleted_Count__c) ? integer.valueOf(temp.CBI_Deleted_Count__c) :0;
     
        if (cbiCount != cbiDelCount){
            cbiValidation = false;
        } else {
            cbiValidation = True;
        }
        if(checkboxBoolean){
            checkboxBoolean = false;
        }else{
            checkboxBoolean = True;
            if(EFLSelfReportingRepository.observationExistOnAllAnnualFieldTestReportByReportSummary(reportSummaryId) == false  || !cbiValidation)
            {
                isSubmissionVisible=true;
            }
        }
    }
    
    //SpringCM Utility
    public String fireDocLauncher(){
        String URL = EFLSpringCMUtility.buildDocLauncherURL(reportSummaryId, reportSummary.Name, '', 'Self Report Attachment');
        return URL;
    }
    
    //Submit logic
    public Pagereference doSubmit() {
        if(reportSummary != null && reportSummary.id != null) {
            EFLSelfReportingHelper EFLSelfReportHelper = new EFLSelfReportingHelper();
            EFLSelfReportHelper.submitFeildTestReport(reportSummary);
        }
        return new Pagereference('/apex/EFLReportSummary?authId='+authorizationID);
    }    
    
    //Initialize and loading bound variable for add field test report
    public void addFieldTestReport()
    {
        isViewMode = false;
        showReportDetail = true; 
        selfReporting = new Self_Reporting__c();
        resetPicklistRadioFields();
        selfReporting.Release_Record_ID__c = locationIdParam;
        if(reportSummary!=null)
        {
            selfReporting.Report_Summary__c = reportSummary.id;
        }    
        selfReporting.Authorization__c = authorizationId;
        selfReporting.RecordTypeId = fieldTestReportRecordTypeId;
        initializeErrors();
        if(authorizationRecord.Multi_Year_Permit__c==false)
        {
            selfReporting.Field_Test_Report_Type__c = 'Final';
        }        
    }
    
    //reset report fields
    public void resetPicklistRadioFields()
    {
        selfReporting.Field_Test_Report_Type__c = '';
        selfReporting.Any_Planting_Material_Harvested__c = '';
        selfReporting.How_was_it_terminated__c = '';
        selfReporting.How_was_material_disposed__c = '';
        selfReporting.Planted_Material_Destroyed_Before_Harves__c = '';
        selfReporting.Planting_Material_Still_Growing__c = '';
        selfReporting.Unexpected_Effects_Picklist__c = '';
        selfReporting.Deleterious_Effects_Picklist__c = '';
        //selfReporting.Deleterious_Effects_CBI__c = '';
        //selfReporting.Unexpected_Effects_CBI__c= '';
        //selfReporting.Stored_Quantity_CBI__c = '';
        //selfReporting.Still_Growing_Quantity_CBI__c = '';
        selfReporting.Is_monitoring_volunteers_required_FT__c = '';
        selfReporting.Is_monitoring_flowering_FT_site__c = '';
        selfReporting.Did_Flowering_Occur__c = '';
        selfReporting.Is_flowering_authorized__c = '';
        selfReporting.Required_to_submit_flowering_report__c= '';
    }
    
    //Initialize and loading bound variable for edit field test report
    public void editFieldTestReport()
    {
        newReportSummaryId = null;
        isViewMode = false;
        showReportDetail = true; 
        selfReporting = new Self_Reporting__c();
        selfReporting = EFLSelfReportingRepository.selfRepById(selfReportId);
        initializeErrors();
        loadObservations();
    }
    
    //save UI bound field test report input to Self_Reporting__c (create if report summary doesn't exist)
    public pagereference saveSelfReport() {
        EFLEnforceAccessUtility.checkObjectUpdateAccess('Self_Reporting__c');
        EFLEnforceAccessUtility.checkObjectInsertAccess('Self_Reporting__c');
        if(selfReporting.Report_Summary__c == null)
        {
            newReportSummaryId = createReportSummary().Id;
            selfReporting.Report_Summary__c = newReportSummaryId;
            ApexPages.currentPage().getParameters().put('rsId', newReportSummaryId);
        }
        upsert selfReporting;
        if(selfReporting.Field_Test_Report_Type__c == 'Final' || (selfReporting.Is_monitoring_volunteers_required_FT__c=='No' && string.isblank(selfReporting.Is_flowering_authorized__c)))
        {
            return returnToList();
        }
        
        loadObservations();
        return null;  
    }
    
    //Returning user to field test report list
    public pagereference returnToList()
    {
        showReportDetail = false; 
        initializeErrors();     
        getFieldTestLocationsAndReports();  
        PageReference EFLFieldTestReport = new PageReference('/apex/EFLFieldTestReport');
        EFLFieldTestReport.setRedirect(true);
        EFLFieldTestReport.getParameters().put('authId', authorizationId);
        EFLFieldTestReport.getParameters().put('reportType', reportType);
        if(newReportSummaryId!=null)
        { 
            EFLFieldTestReport.getParameters().put('rsId', newReportSummaryId);
        }
        else
        {
            EFLFieldTestReport.getParameters().put('rsId', reportSummaryId);
        }
        return EFLFieldTestReport;
    }
    
    //Create report summary if report summary doesn't exist
    public Report_Summary__c createReportSummary(){
        EFLEnforceAccessUtility.checkObjectInsertAccess('Report_Summary__c');
        Report_Summary__c reportSummary = new Report_Summary__c(Authorization__c = authorizationId,Report_Type__c = reportTypeName, Status__c = 'UnSubmitted');
        insert reportSummary;
        return reportSummary;
    }
    
    //Initialize error map bound to the page
    public void initializeErrors()
    {
        pageErrorMap = new Map<string, string>();
        pageErrorMap.put('Field_Test_Report_Type__c', '');
        pageErrorMap.put('Any_Planting_Material_Harvested__c', '');
        pageErrorMap.put('Planted_Material_Destroyed_Before_Harves__c', '');
        pageErrorMap.put('Planting_Material_Still_Growing__c', '');
        pageErrorMap.put('Anticipated_Harvest_Destruct_Date__c', '');
        pageErrorMap.put('In_field_Termination_Date__c', '');
        pageErrorMap.put('Off_Field_Destruction_Date__c', '');
        pageErrorMap.put('Explanation__c', '');
        pageErrorMap.put('Unexpected_Effects__c', '');
        pageErrorMap.put('Deleterious_Effects__c', '');
        pageErrorMap.put('Deleterious_Effects_Data__c', '');
        pageErrorMap.put('Crop_Observations__c', '');
        pageErrorMap.put('How_was_it_terminated__c', '');
        pageErrorMap.put('In_Field_Description__c', '');
        pageErrorMap.put('Stored_Quantity__c', '');
        pageErrorMap.put('Units__c', '');
        pageErrorMap.put('Stored_Material_Type__c', '');
        pageErrorMap.put('Stored_Description__c', '');
        pageErrorMap.put('Off_Field_Description__c', '');
        pageErrorMap.put('Before_Harvest_Destruction_Date__c', '');
        pageErrorMap.put('Before_Harvest_Description__c', '');
        pageErrorMap.put('Still_Growing_Quantity__c', '');
        pageErrorMap.put('Still_Growing_Description__c', '');
        pageErrorMap.put('How_was_material_disposed__c', '');
        
        pageErrorMap.put('Is_monitoring_volunteers_required_FT__c', '');
        pageErrorMap.put('Monitoring_Period_Start__c', '');
        pageErrorMap.put('Monitoring_Period_End__c', '');
        pageErrorMap.put('Is_monitoring_flowering_FT_site__c', '');
        pageErrorMap.put('Did_Flowering_Occur__c', '');
        pageErrorMap.put('Is_flowering_authorized__c', '');
        pageErrorMap.put('Required_to_submit_flowering_report__c', '');
        
    }
    
    //check if there were any validation errors added to error map
    public boolean isValid (Map<string, string> errorMap)
    {
        boolean isValid = true;
        for(string error: errorMap.values())
        {
            if(string.isNotBlank(error))
            {
                isValid = false;
                break;
            }
        }
        return isValid;
    }
    
    //call the helper validation helper method to load error map to render it appropitately altogether (same method is used by trigger validation as well)
    public pagereference validateFieldTestReport()
    {
        initializeErrors();
        showReportDetail = true;         
        set<Id> authIds = new set<Id>();
        authIds.add(selfReporting.Authorization__c);
        set<Id> locationIds = new set<Id>();
        locationIds.add(selfReporting.Release_Record_ID__c);
        EFLSelfReportingHelper EFLSelfReportHelper = new EFLSelfReportingHelper(authIds);
        map<Id, set<Date>> locationIdPlantingDatesMap = new map<Id, set<Date>>();
        locationIdPlantingDatesMap = EFLSelfReportHelper.locationIdplantingDatesMap(authIds, locationIds);
        pageErrorMap = EFLSelfReportHelper.validateFieldTestReport(false, selfReporting, pageErrorMap, locationIdPlantingDatesMap.get(selfReporting.Release_Record_ID__c));
        if(isValid(pageErrorMap))
        {
            //upsert call
            return saveSelfReport();
        }
        return null;
    }
    
    //delete field test report using the EFLGenericUtility Methods and initialize UI vairaibles for rerendering
    public pagereference deleteFieldTestReport()
    {
        EFLGenericUtility.deleteRecord(selfReportId);
        setCon=null;
        getFieldTestLocationsAndReports();  
        initializeErrors();
        showReportDetail = false; 
        return null;
    }
    
    //Load related observations
    public void loadObservations()
    {
        if(string.isNotBlank(selfReporting.Is_monitoring_volunteers_required_FT__c) || string.isNotBlank(selfReporting.Is_flowering_authorized__c))
        {
            if(selfReporting.Is_monitoring_volunteers_required_FT__c=='Yes' || selfReporting.Is_flowering_authorized__c=='Yes' || selfReporting.Is_flowering_authorized__c=='No')
            {
                observations = new List<Observation__c>();
                observations = EFLSelfReportingRepository.getRelatedObservationsBySelfReportId(selfReporting.Id);
                showObservationListPanel=true;
                showObservationAddEditPanel = false;
            }
            else
            {
                showObservationListPanel=false;
                showObservationAddEditPanel = false;  
            }
        }
        else
        {
             showObservationListPanel=false;
             showObservationAddEditPanel = false;   
        }
    }
    
    //Add related observation
     public void addObservation()
    {
        observation = new observation__c();
        observation.Self_Reporting__c = selfReporting.id;
        showObservationListPanel=false;
        showObservationAddEditPanel = true;
    }
    
    //Edit Observation
    public void editObservation()
    {
        observation = new observation__c();
        observation.Self_Reporting__c = selfReporting.id;
        if(observationId!=null)
        {
            observation = EFLSelfReportingRepository.getObservationById(observationId);
        }
        showObservationListPanel=false;
        showObservationAddEditPanel = true;
    }
    
    //Delete related observation
    public void deleteObservation()
    {
        EFLGenericUtility.deleteRecord(observationId);
        loadObservations();
        showObservationListPanel=true;
        showObservationAddEditPanel = false; 
    }
    
    //Save related observation
    public void saveObservation()
    {
        try{
            EFLEnforceAccessUtility.checkObjectUpdateAccess('observation__c');
            EFLEnforceAccessUtility.checkObjectInsertAccess('observation__c');
            upsert observation;
            loadObservations();
            showObservationListPanel=true;
            showObservationAddEditPanel = false;  
        }catch(exception ex)
        {
            //This catch is just to throw validation error
        }  
    }
    
    //Cancel related observation view
    public void cancelObservation()
    {
        observation = new observation__c();
        showObservationListPanel=true;
        showObservationAddEditPanel = false;
    }
    
    
    /*Functions and Methods - End*/
    
}