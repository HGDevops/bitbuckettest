@isTest
private class EFLRegAnimalsTableControllerTest {
    
    @isTest
    static void fetchTableDateNoRecords(){
        List<EFLRegAnimalsTableController.dataTableWrapper> tableRows = EFLRegAnimalsTableController.fetchTableDate('111111111111111');
        System.assertEquals(0, tableRows.size());
    }
    
    @isTest
    static void fetchTableDateRecords(){
        EFLRegistration__c registration = new EFLRegistration__c();
        insert registration;        
        
        EFLRegistration__c registration2 = new EFLRegistration__c();
        insert registration2;
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLRegistration__c = registration.id;
        insert annualReport;
        
        EFLAnnual_Report__c annualReport2 = new EFLAnnual_Report__c();
        annualReport2.EFLRegistration__c = registration2.id;
        insert annualReport2;
            
        EFLAnimal__c animal = new EFLAnimal__c();
        animal.Name = 'Test Animal';
        insert animal;        
        
        EFLAnimal__c animal2 = new EFLAnimal__c();
        animal2.Name = 'Test Animal2';
        insert animal2;  
       
        //shouldn't be selected, EFLOtherAnimal = true
        EFLRegistered_Animal__c registeredAnimal = new EFLRegistered_Animal__c();
        registeredAnimal.EFLAnimal__c = animal.Id;
        registeredAnimal.EFLRegistration__c = registration.Id;
        registeredAnimal.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal.EFLOtherAnimal__c = true;
        registeredAnimal.EFLSelectedForReport__c = true;
        insert registeredAnimal;        
        
        //shouldn't be selected, EFLSelectedForReport = false
        EFLRegistered_Animal__c registeredAnimal2 = new EFLRegistered_Animal__c();
        registeredAnimal2.EFLAnimal__c = animal.Id;
        registeredAnimal2.EFLRegistration__c = registration.Id;
        registeredAnimal2.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal2.EFLOtherAnimal__c = false;
        registeredAnimal2.EFLSelectedForReport__c = false;
        insert registeredAnimal2;        
        
        //shouldn't be selected, EFLSelectedForReport = false
        EFLRegistered_Animal__c registeredAnimal3 = new EFLRegistered_Animal__c();
        registeredAnimal3.EFLAnimal__c = animal.Id;
        registeredAnimal3.EFLRegistration__c = registration.Id;
        registeredAnimal3.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal3.EFLOtherAnimal__c = false;
        registeredAnimal3.EFLSelectedForReport__c = false;
        insert registeredAnimal3;
        
        //should be selected
        EFLRegistered_Animal__c registeredAnimal4 = new EFLRegistered_Animal__c();
        registeredAnimal4.EFLAnimal__c = animal.Id;
        registeredAnimal4.EFLRegistration__c = registration.Id;
        registeredAnimal4.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal4.EFLOtherAnimal__c = false;
        registeredAnimal4.EFLSelectedForReport__c = true;
        insert registeredAnimal4;
        
        //should be selected
        EFLRegistered_Animal__c registeredAnimal5 = new EFLRegistered_Animal__c();
        registeredAnimal5.EFLAnimal__c = animal2.Id;
        registeredAnimal5.EFLRegistration__c = registration.Id;
        registeredAnimal5.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal5.EFLOtherAnimal__c = false;
        registeredAnimal5.EFLSelectedForReport__c = true;
        insert registeredAnimal5;
        
        //shouldn't be selected wrong annual report
        EFLRegistered_Animal__c registeredAnimal6 = new EFLRegistered_Animal__c();
        registeredAnimal6.EFLAnimal__c = animal.Id;
        registeredAnimal6.EFLRegistration__c = registration.Id;
        registeredAnimal6.EFLAnnual_Report__c = annualReport2.Id;
        registeredAnimal6.EFLOtherAnimal__c = false;
        registeredAnimal6.EFLSelectedForReport__c = true;
        insert registeredAnimal6;
        
        List<EFLRegAnimalsTableController.dataTableWrapper> dataTableWrappers = EFLRegAnimalsTableController.fetchTableDate(annualReport.Id);
        System.assertEquals(2, dataTableWrappers.size());
    }
    
    @isTest
    static void deleteAnimalReg(){
        EFLRegistration__c registration = new EFLRegistration__c();
        insert registration;        
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLRegistration__c = registration.id;
        insert annualReport;
        
        EFLAnimal__c animal = new EFLAnimal__c();
        animal.Name = 'Test Animal';
        insert animal;      
        
        //shouldn't be selected, EFLOtherAnimal = true
        EFLRegistered_Animal__c registeredAnimal = new EFLRegistered_Animal__c();
        registeredAnimal.EFLAnimal__c = animal.Id;
        registeredAnimal.EFLRegistration__c = registration.Id;
        registeredAnimal.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal.EFLOtherAnimal__c = true;
        registeredAnimal.EFLSelectedForReport__c = true;
        insert registeredAnimal;  
                
        List<EFLRegistered_Animal__c> registeredAnimals = [SELECT Id FROM EFLRegistered_Animal__c WHERE Id = :registeredAnimal.Id];
        System.assertEquals(1, registeredAnimals.size());
        
        EFLRegAnimalsTableController.deleteAnimalReg(registeredAnimal.Id + 'ZZ');
        
        registeredAnimals = [SELECT Id FROM EFLRegistered_Animal__c WHERE Id = :registeredAnimal.Id];
        System.assertEquals(0, registeredAnimals.size());
    }
    
    @isTest
    static void saveRegAnmls(){
        EFLRegistration__c registration = new EFLRegistration__c();
        insert registration;    
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLRegistration__c = registration.id;
        insert annualReport;
        
        EFLAnimal__c animal = new EFLAnimal__c();
        animal.Name = 'Test Animal';
        insert animal;   
       
        //shouldn't be selected, EFLOtherAnimal = true
        EFLRegistered_Animal__c registeredAnimal = new EFLRegistered_Animal__c();
        registeredAnimal.EFLAnimal__c = animal.Id;
        registeredAnimal.EFLRegistration__c = registration.Id;
        registeredAnimal.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal.EFLColumnBHeldNotUsed__c = 1;
        registeredAnimal.EFLColumnCUsedPainMinimized__c = 1;
        registeredAnimal.EFLColumnDUsedPainMinimized__c = 1;
        registeredAnimal.ELFColumnEPainNotMinimized__c = 1;
        insert registeredAnimal;        
        
        //shouldn't be selected, EFLSelectedForReport = false
        EFLRegistered_Animal__c registeredAnimal2 = new EFLRegistered_Animal__c();
        registeredAnimal2.EFLAnimal__c = animal.Id;
        registeredAnimal2.EFLRegistration__c = registration.Id;
        registeredAnimal2.EFLAnnual_Report__c = annualReport.Id;
        registeredAnimal2.EFLColumnBHeldNotUsed__c = 1;
        registeredAnimal2.EFLColumnCUsedPainMinimized__c = 1;
        registeredAnimal2.EFLColumnDUsedPainMinimized__c = 1;
        registeredAnimal2.ELFColumnEPainNotMinimized__c = 1;
        insert registeredAnimal2; 
        
        registeredAnimal.EFLColumnBHeldNotUsed__c = 2;
        registeredAnimal.EFLColumnCUsedPainMinimized__c = 2;
        registeredAnimal.EFLColumnDUsedPainMinimized__c = 2;
        registeredAnimal.ELFColumnEPainNotMinimized__c = 2;
        registeredAnimal2.EFLColumnBHeldNotUsed__c = 3;
        registeredAnimal2.EFLColumnCUsedPainMinimized__c = 3;
        registeredAnimal2.EFLColumnDUsedPainMinimized__c = 3;
        registeredAnimal2.ELFColumnEPainNotMinimized__c = 3;
        
        List<EFLRegistered_Animal__c> animals = new List<EFLRegistered_Animal__c>();
        animals.add(registeredAnimal);
        animals.add(registeredAnimal2);
        EFLRegAnimalsTableController.saveRegAnmls(animals);
        
        List<String> animalIds = new List<String>();
        animalIds.add(registeredAnimal.Id);
        animalIds.add(registeredAnimal2.Id);
        List<EFLRegistered_Animal__c> updatedAnimals = [Select ID,EFLAnimalName__r.name,EFLAnimal__c, EFLAnnual_Report__c,
                                                     EFLColumnBHeldNotUsed__c,EFLColumnCUsedPainMinimized__c,
                                                     EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,
                                                     EFLOtherAnimal__c 
                                                        FROM EFLRegistered_Animal__c 
                                                        WHERE Id IN :animalIds];
        System.assertEquals(2, updatedAnimals.size());
        for (EFLRegistered_Animal__c regAnimal : updatedAnimals){
            if(regAnimal.Id == registeredAnimal.Id){
                System.assertEquals(2, regAnimal.EFLColumnBHeldNotUsed__c);
                System.assertEquals(2, regAnimal.EFLColumnCUsedPainMinimized__c);
                System.assertEquals(2, regAnimal.EFLColumnDUsedPainMinimized__c);
                System.assertEquals(2, regAnimal.ELFColumnEPainNotMinimized__c);
            }
            else if(regAnimal.Id == registeredAnimal2.Id){
                System.assertEquals(3, regAnimal.EFLColumnBHeldNotUsed__c);
                System.assertEquals(3, regAnimal.EFLColumnCUsedPainMinimized__c);
                System.assertEquals(3, regAnimal.EFLColumnDUsedPainMinimized__c);
                System.assertEquals(3, regAnimal.ELFColumnEPainNotMinimized__c);
            }
        }
    }
}