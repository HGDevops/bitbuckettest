public inherited sharing class EFLConstructsSelector {

    public static List<Phenotype__c> selectPhenotypesByConstructs(id constructID){
        return [SELECT ID,Name,Applicant_Instructions__c,Phenotypic_Category__c,Phenotype_Category_Text__c,
                       Phenotypic_Description__c,Construct__c 
                 FROM Phenotype__c 
                WHERE Construct__c=:constructID
               ]; 
    }
    
    public static List<Genotype__c> selectGenotypesByConstructs(id constructID){
        return [SELECT ID,Name,recordtype.name,recordtype.Id,
                       Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                       Construct_Component_Name__c,Donor_list__c,Description__c,
                       Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c 
                  FROM Genotype__c 
                 WHERE Related_Construct_Record_Number__c=:constructID 
              ORDER BY recordtype.name,
                       Construct_Component_Sort_Order__c ASC,
                       CreatedDate ASC 
               ];
    }  
    
    public static List<Genotype__c> selectGenotypesbyRecordtype(id constructID,id recordTypeId){
        try{
        EFLEnforceAccessUtility.checkObjectReadAccess('Genotype__c');
        return [SELECT ID,Name,recordtype.name,recordtype.Id,
                       Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                       Construct_Component_Name__c,Donor_list__c,Description__c,
                       Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c 
                  FROM Genotype__c 
                 WHERE Related_Construct_Record_Number__c=:constructID 
                   AND recordtypeid =: recordTypeId
              ORDER BY recordtype.name,
                       Construct_Component_Sort_Order__c ASC,
                       CreatedDate ASC
               ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLBRSConstructSelector.selectGenotypesbyRecordtype()',e);                
           } 
         return null;
    }    
    
    public static List<construct__c> selectByLineItemIDsWithGenoandPhenoTypes(id lineItemId){
        return [SELECT ID,Name,
                (SELECT ID,Name,recordtype.name,recordtype.Id,
                 Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                 Construct_Component_Name__c,Donor_list__c,Description__c,
                 Related_Construct_Record_Number__c 
                 FROM Genotypes__r
             ORDER BY recordtype.name,
                      Construct_Component_Sort_Order__c ASC,
                      CreatedDate ASC),
                (SELECT ID,Name,Applicant_Instructions__c,Phenotypic_Category__c,Phenotype_Category_Text__c,
                 Phenotypic_Description__c,Construct__c 
                 FROM Phenotypes__r)
                FROM Construct__c 
                WHERE line_item__c =:lineItemId 
               ];
    }
    
    public static construct__c selectByIDWithGenoandPhenoTypes(id constructID){
        return [SELECT ID,Name,
                (SELECT ID,Name,recordtype.name,recordtype.Id,
                        Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                        Construct_Component_Name__c,Donor_list__c,Description__c,
                        Related_Construct_Record_Number__c 
                   FROM Genotypes__r             
               ORDER BY recordtype.name,
                        Construct_Component_Sort_Order__c ASC,
                        CreatedDate ASC),
                (SELECT ID,Name,Applicant_Instructions__c,
                        Phenotypic_Category__c,Phenotype_Category_Text__c,
                        Phenotypic_Description__c,Construct__c 
                   FROM Phenotypes__r)
              FROM Construct__c 
             WHERE id =:constructID 
             ];

}

}