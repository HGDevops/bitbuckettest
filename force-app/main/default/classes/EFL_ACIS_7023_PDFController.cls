public class EFL_ACIS_7023_PDFController {
    public ID regnId {get; set;}
    public ID annRepId {get;set;}
    public string site1{get;set;}
    public string site2{get;set;}
    public string site3{get;set;}
    public string site4{get;set;}                
    public EFLAnnual_Report__c annRep {get; set;}    
    public  List<EFLRegistered_Animal__c> regAnmllst{get; set;}
    public list<EFLRegistered_Site__c> regSiteslist{get;set;}
    public EFL_ACIS_7023_PDFController (ApexPages.StandardController controller){ //------start of constructor 
        annRep = (EFLAnnual_Report__c)controller.getRecord();
        annRepId = ApexPages.currentPage().getParameters().get('id');
        annRep = [Select Id, name, EFLAccount__c,EFLAccount__r.phone,EFLAccount__r.name,EFLAccount__r.billingAddress, EFLAccount__r.BillingStreet, EFLAccount__r.billingCity,EFLAccount__r.billingState, EFLAccount__r.billingPostalcode, EFLContact__c, EFLCustomer_Number__c, EFLRegistration__c,EFLStage__c,Status__c from EFLAnnual_Report__c where Id=: annRepId]; // 9-22-16 VV added Thumbprint__r.Program_Prefix__r.Process_Type__c
        regnId = annRep.EFLRegistration__c;        
        getRegAnmlRecords();
        getRegisteredSites();
        integer i=0;
        if(regSiteslist.size()>0){
         for(EFLRegistered_Site__c rs:regSiteslist){           
           if(i==0){
              site1=rs.EFLAccount_Site__r.Name; 
           }else if(i==1){           
               site2=rs.EFLAccount_Site__r.Name;
           }else if(i==2){           
               site3=rs.EFLAccount_Site__r.Name;
           }else if(i==3){           
               site4=rs.EFLAccount_Site__r.Name;
           }
           i++;
         }
        }
    }                                                                              //------end of constructor
    public EFL_ACIS_7023_PDFController ()  //VV added
    {
    }
 
    
    public List<EFLRegistered_Animal__c> getRegAnmlRecords(){                                       //------start of AC Records method       
        if(annRep.Id != null){
             regAnmllst= [select Name,EFLAccount__c,EFLAccount_Number__c,EFLAnnual_Report__c,EFLColumnCUsedPainMinimized__c,EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,EFLException_to_Regulation__c,EFLFiscal_Year__c,EFLJustfication_Status__c,EFLJustificationsCount__c, EFLLocked__c,EFLRegistration__c,EFLRegistration_Number__c,EFLSign_Date__c, EFLSigned_By__c, EFLTotalAnimals__c from EFLRegistered_Animal__c where EFLAnnual_Report__c=:annRepid];
        }
        return regAnmllst;
    }    
    public list<EFLRegistered_Site__c> getRegisteredSites(){
       if(annRep.Id != null){
           regSiteslist = [select EFLAccount_Site__r.Name, EFLAccount_Site__r.EFLAddress_1__c, EFLAccount_Site__r.EFLAddress_2__c,EFLAccount_Site__r.EFLCity__c,EFLAccount_Site__r.EFLState_Code__c,EFLAccount_Site__r.EFLZip__c, EFLRegistration__c from EFLRegistered_Site__c where EFLRegistration__c=:regnId];
       }
      return regSiteslist;
    }
}