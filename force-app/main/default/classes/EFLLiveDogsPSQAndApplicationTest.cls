@IsTest
public class EFLLiveDogsPSQAndApplicationTest {
    
    static CARPOL_ACLD_TestDataManager testData = new CARPOL_ACLD_TestDataManager();
    
    static testMethod void testAcLiveDogsPSQApplication() {
        
        testData.createACLiveDogsPermitQuestionsAndSelections();
        testData.createAnimalTransportationSectionAndFields();
        testdata.loadPSQRelatedCustomSetting(); 
        Regulations_Association_Matrix__c regulationAssociationMatrix = testdata.newRegulationAssociationMatrix(testData.acThumbprintId, testdata.dogProgramPathwayId);
        Regulation__c regulation = testData.newRegulation(testData.newProgram('AC').Id);
        Signature_Regulation_Junction__c signatureRegulationJunction = testData.newSignatureRegulationJunction(regulationAssociationMatrix.id, regulation.id);
        Account acc = testdata.newAccount(EFLGenericUtility.getRecordTypeId('APHIS Efile Standard Account'));
        Contact con = testdata.newContact(acc.id);
        user portalUser = testdata.newPortalUser(con.id);        
        
        test.startTest();
        
        system.runAs(portalUser)
        {
            
            PageReference EFLPSQPage = Page.EFLPreScreeningQuestionnaire;
            Test.setCurrentPage(EFLPSQPage);
            EFLPreScreeningQuestionnaireController psqWizard = new EFLPreScreeningQuestionnaireController();
            psqWizard.psqq.SelectedOption = psqWizard.psqq.psqOptions[0].Id;
            psqWizard.psqq.SelectedOptionText = psqWizard.psqq.psqOptions[0].Value__c;
            psqWizard.psqq.psquestion.Question_classification__c = 'Program Pathway';
            psqWizard.psqq.psqOptions[0].Program_Pathway__c = testdata.dogProgramPathwayId;
            psqWizard.getNextQuestion();
            psqWizard.getPrevQuestion();
            psqWizard.psqq.psquestion.Question_classification__c = 'Movement Type';
            psqWizard.psqq.psqOptions[0].Program_Pathway__c = null;
            psqWizard.programPathway = null;
            psqWizard.psqq.SelectedOption = psqWizard.psqq.psqOptions[0].Id;
            psqWizard.psqq.SelectedOptionText = psqWizard.psqq.psqOptions[0].Value__c;
            psqWizard.psqqSelectOptionOnChange();
            psqWizard.getQuestionSelectOptions();
            
            //coo - Australia - Orgin Country Group
            psqWizard.getNextQuestion();
            psqWizard.getQuestionSelectOptions();
            psqWizard.psqqSelectOptionOnChange();
            List<SelectOption> cooOptions = new List<SelectOption>();
            cooOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = cooOptions[1].getValue();
            psqWizard.psqq.SelectedOptionText = cooOptions[1].getLabel();
            
            //sod - Hawaii
            psqWizard.getNextQuestion();
            psqWizard.psqqSelectOptionOnChange();
            List<SelectOption> sodOptions = new List<SelectOption>();
            sodOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = sodOptions[1].getValue();
            psqWizard.psqq.SelectedOptionText = sodOptions[1].getLabel();
            //permitted coo==>orgin, sod ==>Hawaii
            psqWizard.getNextQuestion();
            
            //Back to first question
            psqWizard.getPrevQuestion();
            psqWizard.getPrevQuestion();
            psqWizard.getPrevQuestion();
            
            //Transit Movement
            psqWizard.psqq.SelectedOption = psqWizard.psqq.psqOptions[1].Id;
            psqWizard.psqq.SelectedOptionText = psqWizard.psqq.psqOptions[1].Value__c;
            psqWizard.psqqSelectOptionOnChange();
            psqWizard.getQuestionSelectOptions();
            psqWizard.getNextQuestion();
            
            //coo - Non Orgin Group Country
            psqWizard.getQuestionSelectOptions();
            cooOptions = new List<SelectOption>();
            cooOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = cooOptions[2].getValue();
            psqWizard.psqq.SelectedOptionText = cooOptions[2].getLabel();
            psqWizard.getNextQuestion();
            
            //sod - Hawaii
            psqWizard.psqqSelectOptionOnChange();
            sodOptions = new List<SelectOption>();
            sodOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = sodOptions[1].getValue();
            psqWizard.psqq.SelectedOptionText = sodOptions[1].getLabel();
            //Hawaii permitted coo==>No Orgin Group, sod ==>Hawaii
            psqWizard.getNextQuestion();
            
            //Back to sod for Non Hawaii selection
            psqWizard.getPrevQuestion();
            
            psqWizard.psqqSelectOptionOnChange();
            sodOptions = new List<SelectOption>();
            sodOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = sodOptions[2].getValue();
            psqWizard.psqq.SelectedOptionText = sodOptions[2].getLabel();
            //Non Hawaii permitted coo==>No Orgin Group, sod ==>Non Hawaii
            psqWizard.getNextQuestion();
            psqWizard.getQuestions();
            psqWizard.getPageController();
            //psqWizard.proceedWithApplication();
            //Redirection to Lineitem page
           // pagereference EFLLineItem = psqWizard.proceedWithApplication();
           // Test.setCurrentPage(EFLLineItem);
            
            id lineItemId = Apexpages.currentPage().getParameters().get('id');
            ApexPages.StandardController sc = new ApexPages.StandardController(new AC__c(id=lineItemId));
            
            //test line item review first to see all incomplete instructions
            PageReference pageRef = Page.EFLACLineItemReview;
            pageRef.getParameters().put('lineItemId', String.valueOf(lineItemId));
            Test.setCurrentPage(pageRef);  
           /* EFLACLineItemReviewController lineitemReview = new EFLACLineItemReviewController();
            List<string> allInstructions = lineitemReview.allInstructions;
            List<RegulatedArticle_Information__c> FilteredRegulatedArticleRecords = new List<RegulatedArticle_Information__c>();
            FilteredRegulatedArticleRecords = lineitemReview.getFilteredRegulatedArticleRecords();
            lineitemReview.searchString = 'Test';
            lineitemReview.searchRegulatedArticles();
            FilteredRegulatedArticleRecords = lineitemReview.getFilteredRegulatedArticleRecords();
            lineitemReview.redirectToApplicationReview();
            lineitemReview.refreshRegulatedArticlePageSize(); */
            
            //PSQ Show Conditions(Regulation__c)
            ApexPages.StandardController scRegulation = new ApexPages.StandardController(regulation);
            CARPOL_UNI_ExtWiz_DecisionMatrix showConditionExtension = new CARPOL_UNI_ExtWiz_DecisionMatrix(scRegulation);
            
            pageRef = Page.CARPOL_UNI_Display_Conditions;
            pageRef.getParameters().put('thumbprintId', String.valueOf(regulation.id));
            Test.setCurrentPage(pageRef);
            
            showConditionExtension = new CARPOL_UNI_ExtWiz_DecisionMatrix(scRegulation);
            showConditionExtension.getrlist();
            string instruction = showConditionExtension.instruction;
            
            pageRef = Page.EFLLineItem;
            pageRef.getParameters().put('id', String.valueOf(lineItemId));
            Test.setCurrentPage(pageRef);            
            
        /*    EFLLineItemController EFLineItemCtrl = new EFLLineItemController(sc);
            EFLineItemCtrl.save();
            EFLineItemCtrl.Cancel();
            String chevronOptions = EFLineItemCtrl.chevronOptions;
            string instructions = EFLineItemCtrl.instructions;
            string currentStep = EFLineItemCtrl.currentStep;
            Decimal openSection = EFLineItemCtrl.openSection;
            string Sectiontype = EFLineItemCtrl.Sectiontype;
            AC__c objLineItem = EFLineItemCtrl.objLineItem; 
            string sSelectedLandOpt = EFLineItemCtrl.sSelectedLandOpt;
            id SelectedPathwayId = EFLineItemCtrl.SelectedPathwayId;
            EFLineItemCtrl.lineitemrecord.Transporter_type__c = 'Air';
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Transporter_type__c = 'Sea';
            EFLineItemCtrl.lineitemrecord.Proposed_date_of_arrival__c = date.valueof(system.Datetime.now().addDays(-2));
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Proposed_date_of_arrival__c = date.valueof(system.Datetime.now().addDays(+1));
            EFLineItemCtrl.lineitemrecord.Departure_Time__c = date.valueof(system.Datetime.now().addDays(-1));
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Proposed_date_of_arrival__c = null;
            EFLineItemCtrl.lineitemrecord.Departure_Time__c = null;
            EFLineItemCtrl.lineitemrecord.My_Port_Is_Not_Listed__c = true;
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Port_If_Not_Listed__c = '';
            EFLineItemCtrl.save();        
            EFLLineItemViewController liv = new EFLLineItemViewController();
            EFLLineItemController pageController = new EFLLineItemController(sc); 
            pageController = liv.pageController; */
        }
        
        test.stopTest();
        
    }
    
    static testMethod void testAcLiveDogsBRSPathwayChangeOver() {
        
        CARPOL_AC_TestDataManager testACData = new CARPOL_AC_TestDataManager();
        testACData.insertcustomsettingsWithBRSTriggerDisabled();
        
        testData.createACLiveDogsPermitQuestionsAndSelections();
        testData.createAnimalTransportationSectionAndFields();
        testdata.loadPSQRelatedCustomSetting(); 
        Account acc = testdata.newAccount(EFLGenericUtility.getRecordTypeId('APHIS Efile Standard Account'));
        acc.Type = 'Applicant';
        update acc;
        Contact con = testdata.newContact(acc.id);
        user portalUser = testdata.newPortalUser(con.id);        
        
        test.startTest();
        
        system.runAs(portalUser)
        {
            
            PageReference EFLPSQPage = Page.EFLPreScreeningQuestionnaire;
            Test.setCurrentPage(EFLPSQPage);
            EFLPreScreeningQuestionnaireController psqWizard = new EFLPreScreeningQuestionnaireController();
            psqWizard.psqq.SelectedOption = psqWizard.psqq.psqOptions[0].Id;
            psqWizard.psqq.SelectedOptionText = psqWizard.psqq.psqOptions[0].Value__c;
            psqWizard.psqq.psquestion.Question_classification__c = 'Program Pathway';
            psqWizard.psqq.psqOptions[0].Program_Pathway__c = testData.newDogsPathway().id;
            psqWizard.getNextQuestion();
            psqWizard.getPrevQuestion();
            psqWizard.psqq.psquestion.Question_classification__c = 'Movement Type';
            psqWizard.psqq.psqOptions[0].Program_Pathway__c = null;
            psqWizard.programPathway = null;
            psqWizard.psqq.SelectedOption = psqWizard.psqq.psqOptions[0].Id;
            psqWizard.psqq.SelectedOptionText = psqWizard.psqq.psqOptions[0].Value__c;
            psqWizard.psqqSelectOptionOnChange();
            psqWizard.getQuestionSelectOptions();
            psqWizard.getNextQuestion();
            
            //coo - Non Orgin Group Country
            system.debug('coo######: ' + psqWizard.psqq.psqOptions);
            psqWizard.getQuestionSelectOptions();
            List<SelectOption> cooOptions = new List<SelectOption>();
            cooOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = cooOptions[2].getValue();
            psqWizard.psqq.SelectedOptionText = cooOptions[2].getLabel();
            psqWizard.getNextQuestion();
            
            //sod - Hawaii
            psqWizard.psqqSelectOptionOnChange();
            List<SelectOption> sodOptions = new List<SelectOption>();
            sodOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = sodOptions[1].getValue();
            psqWizard.psqq.SelectedOptionText = sodOptions[1].getLabel();
            //Hawaii permitted coo==>No Orgin Group, sod ==>Hawaii
            psqWizard.getNextQuestion();
            
            //Back to sod for Non Hawaii selection
            psqWizard.getPrevQuestion();
            
            psqWizard.psqqSelectOptionOnChange();
            sodOptions = new List<SelectOption>();
            sodOptions = psqWizard.getQuestionSelectOptions();
            psqWizard.psqq.SelectedOption = sodOptions[2].getValue();
            psqWizard.psqq.SelectedOptionText = sodOptions[2].getLabel();
            //Non Hawaii permitted coo==>No Orgin Group, sod ==>Non Hawaii
            psqWizard.getNextQuestion();
            psqWizard.getQuestions();
            psqWizard.getPageController();
            psqWizard.apw.application = new Application__c();
            psqWizard.apw.application.Applicant_Account__c = acc.Id;
            psqWizard.proceedWithApplication();
            //Redirection to Lineitem page
           // pagereference EFLLineItem = psqWizard.proceedWithApplication();
          //  Test.setCurrentPage(EFLLineItem);
            
            id lineItemId = Apexpages.currentPage().getParameters().get('id');
            ApexPages.StandardController sc = new ApexPages.StandardController(new AC__c(id=lineItemId));
            
            //test line item review first to see all incomplete instructions
            pagereference pageRef = Page.EFLLineItem;
            pageRef.getParameters().put('id', String.valueOf(lineItemId));
            Test.setCurrentPage(pageRef);//change pathway name  
            
          /*  EFLLineItemController EFLineItemCtrl = new EFLLineItemController(sc);
            AC__c lineitem = new AC__c(id=EFLineItemCtrl.lineItemRecord.id,Program_Line_Item_Pathway__c = testdata.brsProgramPathwayId);   
            update lineitem;
            EFLineItemCtrl.lineitemrecord = lineitem;
            EFLineItemCtrl.CBIVal = true;
            EFLineItemCtrl.save(); */
            
        }
         
        test.stopTest();
         
     }
    
    static testMethod void testAcLiveDogsPSQExceptionPaths() {
        testData.createACLiveDogsPermitQuestionsAndSelections();
        Wizard_Selections__c lastSelection = new Wizard_Selections__c();
        lastSelection = [SELECT Id, Value__c, Wizard_Next_Question__c, Wizard_Questionnaire__c, Activity_Processor_Result__c FROM Wizard_Selections__c where Activity_Processor_Result__c != null and Wizard_Next_Question__c != null order by createddate desc limit 1];
        EFLPSQUtility.getNextQuestionByActivityResult(lastSelection.Wizard_Questionnaire__c, '', null, lastSelection.Activity_Processor_Result__c);
        EFLPSQUtility.getNextQuestionByValue(lastSelection.Wizard_Questionnaire__c, null, null, lastSelection.Value__c);
        EFLPSQUtility.getNextQuestion(lastSelection.Wizard_Questionnaire__c);
        testdata.loadPSQRelatedCustomSetting(); 
        //Covering technical exception areas
        ApexPages.currentPage().getParameters().put('rerenderPSQ2','true');
        Account acc = testdata.newAccount(EFLGenericUtility.getRecordTypeId('APHIS Efile Standard Account'));
        Contact con = testdata.newContact(acc.id);
        user portalUser = testdata.newPortalUser(con.id);        
        
        test.startTest();
        
        system.runAs(portalUser)
        {
        EFLPreScreeningQuestionnaireController psqWizard = new EFLPreScreeningQuestionnaireController();
        psqWizard.setToggleStatus();
        psqWizard.psqq.psquestion.Question_classification__c ='';
        psqWizard.getPrevQuestion();    
        psqWizard.psw.QuestionList = new List<EFLPSQQuestion>();
        psqWizard.getMovementType(null);
        EFLARPCountryOfOrigin arpCountry = new EFLARPCountryOfOrigin();
        arpCountry.processResult(null);
        psqWizard.Cancel();
        psqWizard.psqq.SelectedOption = null;
        psqWizard.getNextQuestion();
        }
         test.stopTest();
    }
    
     static testMethod void testACLiveDogsLineItemReview() {    
         
        CARPOL_AC_TestDataManager testACData = new CARPOL_AC_TestDataManager();
        testACData.insertcustomsettings();
        
        
        test.startTest();
        try{
            Application__c objApp = testACData.newapplication();
        	ac__c lineItem = testACData.newLineItem('Personal Use',objApp); 
        	PageReference pageRef = Page.EFLACLineItemReview;
            pageRef.getParameters().put('lineItemId', String.valueOf(lineItem.Id));
            Test.setCurrentPage(pageRef);  
            EFLACLineItemReviewController lineitemReview = new EFLACLineItemReviewController();
            List<string> allInstructions = lineitemReview.allInstructions;
            List<RegulatedArticle_Information__c> FilteredRegulatedArticleRecords = new List<RegulatedArticle_Information__c>();
            FilteredRegulatedArticleRecords = lineitemReview.getFilteredRegulatedArticleRecords();
            lineitemReview.searchString = 'Test'; 
            lineitemReview.searchRegulatedArticles();
            FilteredRegulatedArticleRecords = lineitemReview.getFilteredRegulatedArticleRecords();
            lineitemReview.redirectToApplicationReview();
            lineitemReview.refreshRegulatedArticlePageSize();    
            EFLLineItemViewController EFLLIView = new EFLLineItemViewController();
            EFLLineItemController pageController = EFLLIView.pageController;
         }catch(exception ex){}
         test.stopTest();
         
     }
    
    static testMethod void testACLiveDogsLineItem() {    
         
        CARPOL_AC_TestDataManager testACData = new CARPOL_AC_TestDataManager();
        testACData.insertcustomsettings();
        
        
        test.startTest();
        try{
            Application__c objApp = testACData.newapplication();
        	ac__c lineItem = testACData.newLineItem('Personal Use',objApp); 
       	    pagereference pageRef = Page.EFLLineItem;
            pageRef.getParameters().put('id', String.valueOf(lineItem.Id));
            Test.setCurrentPage(pageRef); 
        	id lineItemId = Apexpages.currentPage().getParameters().get('id');
            ApexPages.StandardController sc = new ApexPages.StandardController(new AC__c(id=lineItemId));
            EFLLineItemController EFLineItemCtrl = new EFLLineItemController(sc);
            EFLineItemCtrl.save();
            EFLineItemCtrl.Cancel();
            String chevronOptions = EFLineItemCtrl.chevronOptions;
            string instructions = EFLineItemCtrl.instructions;
            string currentStep = EFLineItemCtrl.currentStep;
            Decimal openSection = EFLineItemCtrl.openSection;
            string Sectiontype = EFLineItemCtrl.Sectiontype;
            AC__c objLineItem = EFLineItemCtrl.objLineItem; 
            string sSelectedLandOpt = EFLineItemCtrl.sSelectedLandOpt;
            id SelectedPathwayId = EFLineItemCtrl.SelectedPathwayId;
            EFLineItemCtrl.lineitemrecord.Transporter_type__c = 'Air';
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Transporter_type__c = 'Sea';
            EFLineItemCtrl.lineitemrecord.Proposed_date_of_arrival__c = date.valueof(system.Datetime.now().addDays(-2));
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Proposed_date_of_arrival__c = date.valueof(system.Datetime.now().addDays(+1));
            EFLineItemCtrl.lineitemrecord.Departure_Time__c = date.valueof(system.Datetime.now().addDays(-1));
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Proposed_date_of_arrival__c = null;
            EFLineItemCtrl.lineitemrecord.Departure_Time__c = null;
            EFLineItemCtrl.lineitemrecord.My_Port_Is_Not_Listed__c = true;
            EFLineItemCtrl.save();
            EFLineItemCtrl.lineitemrecord.Port_If_Not_Listed__c = '';
            EFLineItemCtrl.save();        
            EFLLineItemViewController liv = new EFLLineItemViewController();
            EFLLineItemController pageController = new EFLLineItemController(sc); 
            pageController = liv.pageController;    
        
        	EFLineItemCtrl = new EFLLineItemController(sc);
            AC__c lineitem1 = new AC__c(id=EFLineItemCtrl.lineItemRecord.id,Program_Line_Item_Pathway__c = testdata.brsProgramPathwayId);   
            update lineitem1;
            EFLineItemCtrl.lineitemrecord = lineitem1;
            EFLineItemCtrl.CBIVal = true;
            EFLineItemCtrl.save();
          }catch(exception ex){}
          test.stopTest();
         
     }
    
}