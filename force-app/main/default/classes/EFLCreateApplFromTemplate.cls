/*
* ClassName: EFLCreateApplFromTemplate
* CreatedBy: Vijay Vellaturi
* LastModifiedBy: Vijay Vellaturi
* LastModifiedOn: 20 Jan 2017
* Description: List out the applications that were saved as templates, with a link 'Select'. Insert and copy all the application, line item(s) data 
* the selected application. Navigate to application detail page where applicant can go to application or line items to make modifications and submit
* Revision History
*/

public with sharing class EFLCreateApplFromTemplate {

   
public List<Application__c> templateApplications { get; set; }
public string selectedApplId {get;set;}
public Id contactId;
public  EFLCreateApplFromTemplate () {
   selectedApplId =  ApexPages.currentPage().getParameters().get('appid');
   system.debug('UserInfo.getUserId()' + UserInfo.getUserId());
   contactId = [select contactid from user where id =:UserInfo.getUserId()limit 1].contactid;
   system.debug('contactId' + contactId);
   //templateApplications = [SELECT ID, Name, CreatedDate, Application_Status__c, Application_Type__c, RecordType.Name,(select id, name,Scientific_Name__c,Scientific_Name__r.Name,RA_Scientific_Name__c,Status__c,Authorization__c,Authorization__r.name,movement_Type__c,port_of_entry__r.name,port_of_embarkation__r.name from Animal_Care_AC__r ) FROM Application__c where Application_status__c = 'Submitted' and EFLSaveastemplate__c = true ORDER BY CreatedDate DESC limit 1000];
}

    public PageReference deepClonefromAppl() {
      system.debug('---selectedApplId ---'+selectedApplId ); 
        string withdrawstr = 'Withdrawn Customer';
        application__c objapp = new  application__c();
        application__c objAppNew = new application__c();
       
        DescribeSObjectResult dApp = Application__c.SObjectType.getDescribe();
        List<String> appFields = new List<String>(dApp.fields.getMap().keySet());
        DescribeSObjectResult dLine = AC__c.SObjectType.getDescribe();
        List<String> lineFields = new List<String>(dLine.fields.getMap().keySet());
        system.debug('---appFields---'+appFields );
        system.debug('---lineFields---'+lineFields );
        if(selectedApplId != null && selectedApplId !=''){
            String soqlApp = 'select ' + String.join(appFields , ', ')+' from Application__c where id =: selectedApplId';
            objapp = Database.query(soqlApp);        
        objAppNew  = objapp ;
        objAppNew.Application_Status__c = 'Open';
        objAppNew.Applicant_Name__c = contactid;
        objAppNew.Authorized_User__c = contactid;
        objAppNew.Locked__c = 'No';
       // objAppNew.OwnerId = UserInfo.getUserId();
        objAppNew.Agreed_and_certified__c = false;
        objAppNew.EFLSaveastemplate__c = false;
        objAppNew.id = null;
        if (objAppNew != null){        
          insert objAppNew;
        }
        list<ac__c> lineitemold = new list<ac__c>();
        list<ac__c> lineitemnew = new list<ac__c>();
        string soqlLine = 'select '+ string.join(lineFields , ',')+ ' from ac__c where Application_Number__c=:selectedApplId and status__c != :withdrawstr ';
          lineitemold = database.query(soqlLine);
          if (!lineitemold.isempty()) {
              for (ac__c li:lineitemold ){
                ac__c objAC = new ac__c();
                objAC  = li;
                objAC.status__c = 'Saved';
                objAC.application_number__c = objAppNew.id;
                objAC.id = null;
                objAC.Authorization__c = null;
                objAC.Proposed_date_of_arrival__c = date.today().addDays(1);
                objAc.Proposed_Start_Date__c = date.today().addDays(1);
                objAC.Proposed_End_Date__c = date.today().addYears(1);
                lineitemnew.add(objAC);
              }
             if (!lineitemnew.isempty()){
                   insert lineitemnew;
                }
               
             } // if (!lineitemold.isempty())
              pagereference pg = new pagereference ('/apex/portal_application_detail?id='+objAppNew.id);
              pg.setRedirect(true);
              return pg;
            }
        return null;
        
    }
   
}