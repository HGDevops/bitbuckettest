/**
 * Trigger Handler for the Incident SObject. This class implements the EFLITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */ 
public with sharing class EFLIncidentTriggerHandler 
                    implements EFLITrigger 
{

	list<sObject> activityList = new list<sObject>();
    list<sObject> finalActivityList = new list<sObject>();
    list<Incident__c> incidentsToUpdate = new list<Incident__c>();    
     
    // Allows unit tests (or other code) to disable this trigger for the transaction
     public static Boolean TriggerDisabled = false;
 
    /*
    * Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
       if (EFLGenericUtility.isTriggerDisabled('EFLIncidentTrigger'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    /*
     * Constructor
     */ 
    public EFLIncidentTriggerHandler()
    {
    
     }
 
    /**
     * bulkBefore
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
    }
    
    /**
     * bulkAfter
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter()
    {
        
    }    
  
    /**
     * beforeInsert
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so)
    {    
            
    }

    /**
     * beforeUpdate
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so)
    {
        Incident__c oldIncidentRecord = (Incident__c)oldSo;
        Incident__c newIncidentRecord = (Incident__c)so;
        
        EFLIIncidentEngine engine = EFLIncidentEngineFactory.getEngine(newIncidentRecord);
        
        //validate task creation and Load all activities along with authorizations
        if(EFLIncidentUtility.needsTaskCreation(oldIncidentRecord,newIncidentRecord)){
            newIncidentRecord.Activity_Sequence__c = 0;
            engine.loadActivities(newIncidentRecord, activityList);
            finalActivityList.addAll(activityList);
            incidentsToUpdate.add(newIncidentRecord);
        }
        
    } 
    
    /**
     * beforeDelete
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {
        
    }
 
    /**
     * afterInsert
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */ 
    public void afterInsert(SObject so)
    {
    }

    /**
     * afterUpdate
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so)
    {
        
    } 

    /**
     * afterDelete
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */ 
    public void afterDelete(SObject so)
    {
    }
 
    /**
     * andFinally
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
      if(finalActivityList != null){
            system.debug('finalActivityList'+ finalActivityList);
            Database.SaveResult[] results = Database.insert(finalActivityList,true); 
        }  
    }    
    
}