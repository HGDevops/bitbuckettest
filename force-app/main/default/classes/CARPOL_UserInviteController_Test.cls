@Istest
public class CARPOL_UserInviteController_Test {
    @IsTest static void CARPOL_UserInviteController_Test_Testmethod1(){
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Contact objcont = testData.newcontact();
        User usershare = new User();
        usershare.Username ='aphistestemail@test.com';
        usershare.LastName = 'APHISTestLastName';
        usershare.Email = 'APHISTestEmail@test.com';
        usershare.alias = 'APHItest';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name='BRS ROP Reviewer'].Id;

        usershare.LanguageLocaleKey = 'en_US';
        //usershare.ContactId = objcont.id;
        usershare.IsActive = true;
        insert usershare;       
        
        CARPOL_UserInviteController CUI = new CARPOL_UserInviteController();
        CARPOL_UserInviteController.userDetails = usershare;
        CARPOL_UserInviteController.contactDetails = objcont;
        CARPOL_UserInviteController.contactInvite = new CARPOL_UserInviteController.InvitedContact();
        CUI.con = objcont;
        CUI.myContacts = '';
        CARPOL_UserInviteController.inviteSent = false;
        CARPOL_UserInviteController.previousInviteId = null;
        CARPOL_UserInviteController.inviteeList = new List<CARPOL_UserInviteController.InvitedContact>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        Test.startTest();
        CARPOL_UserInviteController.initializeData();
        CARPOL_UserInviteController.InvitedContact invitedCont = new CARPOL_UserInviteController.InvitedContact();
        invitedCont.email = 'aphistestemail@test.com';
        invitedCont.firstName = 'Test';
        invitedCont.lastName = 'User';
        invitedCont.status = 'Valid';
        invitedCont.userName = 'InviteuserName';
        invitedCont.oldUserId = 'InviteoldUserId';
        invitedCont.FedId = 'InviteFEdId123';
        invitedCont.newConId = 'InvitenewConId';
        invitedCont.dateInvitation = 'InvitedateInvitation';
        invitedCont.errorMsg = 'InviteerrorMsg';
        invitedCont.UserInviteId = 'InvitedinvitedCont';
        
        List<CARPOL_UserInviteController.InvitedContact> inviteList = new List<CARPOL_UserInviteController.InvitedContact>();
        inviteList.add(invitedCont);
        CARPOL_UserInviteController.inviteUsers(inviteList);
        CARPOL_UserInviteController.resendEmail(inviteList);
        CARPOL_UserInviteController.sendInviteEmails(inviteList, objcont);
        
        string results = CARPOL_UserInviteController.verifyInviteeEmail(invitedCont);
        CARPOL_UserInviteController.getPreviousInvitees();
        Test.stopTest();
        
    }
    
}