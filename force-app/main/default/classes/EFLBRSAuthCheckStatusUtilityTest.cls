@isTest(seeAllData=true)
public class EFLBRSAuthCheckStatusUtilityTest {
    static testmethod void method1(){
        list<Authorizations__c> authList = new list<Authorizations__c>();
        Domain__c pgm = new Domain__c();
        pgm.name = 'BRS';
        insert pgm;
        
        Program_Prefix__c pgmPrefix = new Program_Prefix__c();
        pgmPrefix.program__c = pgm.id;
        insert pgmPrefix;
        
        Signature__c sig = new Signature__c();
        sig.RecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        sig.Program_Prefix__c = pgmPrefix.id;
        insert sig;
        CARPOL_BRS_TestDataManager carBRSTest = new CARPOL_BRS_TestDataManager();
        
        carBRSTest.insertcustomsettings();
        
        String AccountRecordTypeId = carBRSTest.AccountRecordTypeId; 
        Contact objCont = new Contact();
        objCont.FirstName = 'Global Contact'+carBRSTest.randomString5();
        objcont.LastName = 'LastName'+carBRSTest.randomString5();
        objcont.Email = carBRSTest.randomString5()+'test@email.com';        
        objcont.AccountId = carBRSTest.newAccount(AccountRecordTypeId).id;
        objcont.MailingStreet = 'Mailing Street'+carBRSTest.randomString5();
        objcont.MailingCity = 'Mailing City'+carBRSTest.randomString5();
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '32092'; 
        insert objcont;
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
        
        Application__c objapp = new Application__c();
        objapp.Applicant_Name__c = objcont.Id;
        objapp.Recordtypeid = ACAppRecordTypeId;
        objapp.Application_Status__c = 'Open';
        insert objapp; 
        
        Authorizations__c auth = carBRSTest.newAuth(objapp.id);
        /*auth.RecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId(); 
auth.BRS_Introduction_Type__c='Interstate Movement';
auth.thumbprint__c = sig.id;
auth.effective_date__c=system.today();*/
        auth.Expiry_Date__c = system.today().adddays(250);
        auth.BRS_Proposed_Start_Date__c=system.today();
        auth.BRS_Proposed_End_Date__c = system.today().adddays(250);
        //auth.Application__c=objapp.id;
        auth.Stage__c = 'Application Review';
        update auth; 
        authList.add(auth);
        
        // added 5/2019 by JB: make inspections related to auth to increase coverage on utility class.
        Inspection__c inspection = carBRSTest.newInspection();
        inspection.Authorization__c = auth.Id;
        inspection.Status__c = 'New';
        update inspection;
        // end changes by JB 5/2019
        
        Program_Line_Item_Pathway__c plip =carBRSTest.newCaninePathway();
        AC__c ac = new AC__c();
        ac.RecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordTypeId();
        ac.authorization__c = auth.id;
        ac.Departure_Time__c = date.today()+11;
        ac.Arrival_Time__c = date.today()+15;
        ac.Proposed_date_of_arrival__c = date.today()+15;
        ac.Program_Line_Item_Pathway__c=plip.id;
        ac.Transporter_Type__c = 'Ground';
        ac.Date_of_Birth__c = date.today()-400;
        ac.Color__c = 'Brown';
        ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
        ac.Sex__c = 'Male';
        ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
        ac.Status__c = 'Saved';
        ac.application_number__c=objapp.id;
        insert ac;
        Link_Regulated_Articles__c linkRA = carBRSTest.newlinkRegArticle(ac.id,objapp.id,auth.id);
        linkRA.status__c = 'Denied';
        update linkRA;
        Regulated_Article__c regArt = carBRSTest.newRegulatedArticle(plip.id);
        Article_Supplier_Developer__c ASD = carBRSTest.newASD(AC.id);
        Construct__c cons = carBRSTest.newconstruct(AC.id,regArt);
        Country__c countryUS = carBRSTest.newcountryus();
        Level_1_Region__c l1Reg = carBRSTest.newlevel1region(countryUS.id);
        Level_2_Region__c l2Reg = carBRSTest.newlevel2region(l1Reg.id);
        id locRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        Location__c loc = carBRSTest.newlocation(countryUS.id,l1Reg.id,l2Reg.id,ac.id,locRecTypeId);
        loc.status__c = 'Waiting on Customer';
        update loc;
        Applicant_Attachments__c appAttach = carBRSTest.newAttach(AC.id);
        appAttach.status__c = 'Waiting on Customer';
        update appAttach;
        
                
                
        system.test.starttest();
       // EFLBRSAuthCheckStatusUtility.getDataForAuthStatusValidation(authList);
        EFLBRSAuthCheckStatusUtility.validateInspectionStatus(auth);
        EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(auth);
        delete linkRA;
        EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(auth);
        delete ASD;
        //cons.Status__c = 'Review Complete';
        //update cons;
        EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(auth);
        delete appAttach;
        EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(auth);
        delete cons;
        EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(auth);
        delete loc;
        EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(auth);
        EFLBRSAuthCheckStatusUtility.validateAuthLineItemRelatedStatus(auth);
        EFLBRSAuthCheckStatusUtility.getDataForAuthStatusValidation(authList);
        auth.status__c='Waiting on Customer';
        update auth;
        authList.add(auth);
        EFLBRSAuthCheckStatusUtility.updateLineItemsFromAuthorizations(authList);
        EFLBRSAuthCheckStatusUtility.validateInspectionStatus(auth);
        system.test.stoptest();
    }
    
}