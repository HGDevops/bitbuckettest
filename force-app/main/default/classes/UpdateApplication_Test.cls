@isTest
private class UpdateApplication_Test {
    
    private static testMethod void UpdateAppTest() 
    {
       // CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
       CARPOL_BRS_TestDataManager testData  = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c li = testData .newlineitem('Personal Use', objapp);
        Domain__c program = new Domain__c(Name='BRS');
        insert program;
        Program_Prefix__c programPrefix = new Program_Prefix__c(Program__c=program.Id);
        insert programPrefix;
        Signature__c thumbprint = new Signature__c(Program_Prefix__c = programPrefix.id);
        insert thumbprint;  
        
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        objauth.thumbprint__c = thumbprint.Id;
        objauth.BRS_Proposed_Start_Date__c = system.today()+10;
        objauth.BRS_Proposed_End_Date__c=System.today()+20;
        objauth.Effective_Date__c=System.today()+20;   
        
        objapp.Share_with_Roles__c = 'BRS';
        update objapp;
        AC__c lineItem1 = testData.newLineItem('Resale',objapp);
        AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
        
        Test.startTest();
        Transaction__c trans = new Transaction__c();
        trans.Application__c = objapp.id;
        trans.Transaction_Amount__c = 150;
        trans.Transaction_Date_Time__c = System.now();
        trans.Payment_Type__c = 'CC';
        trans.Line_Item__c = li.id;
        insert trans;
        PageReference pageRef = Page.UpdateApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
        ApexPages.currentPage().getParameters().put('Id',objapp.id);
        UpdateApplication updateClass = new UpdateApplication(sc);
        updateClass.appUpdate();
        
        trans.Payment_Type__c = 'APHIS Account';
        trans.APHIS_Account_Number__c = '1234567';
        update trans;
        updateClass.appUpdate();
        trans.Payment_Type__c = 'Pay.gov';
        update trans;
        updateClass.appUpdate();
        trans.Payment_Type__c = 'Check/Money Order';
        update trans;
        updateClass.appUpdate();
        delete trans;
        updateClass.appUpdate();
        updateClass.goBackApplication();
        
        //Transaction__c trans2 = new Transaction__c();
        
        Test.stopTest();
        
    }
    
}