@isTest(seealldata=true)
private class CARPOL_InspectionLetter_Test {

    private static testMethod void UnitTest() {
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        
        Communication_Manager__c testcomm = testData.newCommunicationmanager('Permit');
        testcomm.Name = 'BRS Notification';
        update testcomm;
        
        
        Facility__c fac = new Facility__c();
        String FacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Inspection Station').getRecordTypeId();
        fac.RecordTypeId = FacRecordTypeId; 
        fac.Name = 'Test Facility';
        insert fac;
         
        Facility__c facc = [SELECT Id,Name FROM Facility__c WHERE ID=:fac.id];
        Inspection__c Ins = new Inspection__c();
        String InsRecordTypeId = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Ins.RecordTypeId = InsRecordTypeId;
        Ins.Facility__c = facc.id;
        Ins.Program__c = 'AC';   
        Ins.Communication_Manager_Template__c = testcomm.id;      
        insert Ins;
        
        Attachment objattach = new Attachment();
        objattach.Name = 'test class';
        objattach.body = Blob.valueOf('UNIT.TEST');
        objattach.parentid = ins.id;
        insert objattach;
        
        /*Attachment objattach2 = new Attachment();
        objattach2.name = 'test class attach2';
        objattach2.body = Blob.valueOf('UNIT.TEST');
        objattach2.parentid = ins.id;
        insert objattach2;*/
        
         test.startTest();
        PageReference pageRef = Page.CARPOL_InspectionLetter;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(Ins);
        Apexpages.currentpage().getparameters().put('id',ins.id); 
        CARPOL_InspectionLetter_controller cont = new CARPOL_InspectionLetter_controller(sc);
       
        
       // cont.inspectionId = ins.id;
        cont.inspection = ins;
        cont.baseURL = '';
        cont.letterType = 'test';
        
        
        pageRef = cont.populateTemplate();
        pageRef = cont.attachLetter();
        pageRef = cont.saveDraft();
        pageRef = cont.attachSignLetter();
        test.stopTest();
        
    }

}