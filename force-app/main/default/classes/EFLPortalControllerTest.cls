/**
* Purpose : This is used for code coverage of class PortalController 
* Related Class: PortalController
*/
@IsTest
public class EFLPortalControllerTest{  
    
    static testMethod void testForwardToAuthPageAsGuest(){
        CARPOL_AC_TestDataManager testData=new  CARPOL_AC_TestDataManager();
        //User currentUser = EFLUserTestDataFactory.getUser('Guest User');
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'Guest License User' LIMIT 1];
        User currentUser = new User(  firstname = 'TestPreparer',
                                    lastName = 'TestLastName',
                                    email = 'testpreparer@test.org',
                                    Username = 'testpreparer@test.org',
                                    EmailEncodingKey = 'ISO-8859-1',
                                    Alias ='tprep',
                                    TimeZoneSidKey = 'America/Los_Angeles',
                                    LocaleSidKey = 'en_US',
                                    LanguageLocaleKey = 'en_US',
                                    ProfileId = prof.Id);
        insert currentUser;
        testData.insertURLs(); 
        Test.startTest();
        system.runAs(currentUser) {  
            PortalController pController = new PortalController();
            
            PageReference pageRef = pController.forwardToAuthPage();
            System.assertNotEquals(null,pageRef);
        }
        Test.StopTest();
    }
    
    static testMethod void testForwardToAuthPageAsNonGuest(){
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData1=new  CARPOL_AC_TestDataManager();
        Contact objcont = testData.newcontact();
        User currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');  
        testData1.insertURLs();
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            PageReference pageRef = pController.forwardToAuthPage();
            System.assertEquals(null,pageRef);
        }
        Test.stopTest();
    }
    
    static testMethod void testEditContact(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        Test.startTest();
        system.runAs(currentUser){
            PortalController pController = new PortalController();
            PageReference pageRef = pController.editContact();
            System.assert(pageRef != null);            
        }
        Test.stopTest();
    }
    
    static testMethod void testDeactivateUser(){     
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Account business1 = testData.newAccount(testdata.AccountRecordTypeId);       
        Account business1div1 = testData.newAccount(testdata.AccountRecordTypeId); 
        
        User orgAdminUser = EFLUserTestDataFactory.getUser('eFile Applicant Plus');
        User emp1User = EFLUserTestDataFactory.getUser('eFile Applicant');
        
        Contact orgAdminContact = new Contact(Id = orgAdminUser.ContactId);
        orgAdminContact.AccountId = business1.Id;
        update orgAdminContact;  
        
        //Add new contact to business account
        Account emp1persAcct = testData.newAccount(testdata.AccountRecordTypeId); 
        Contact emp1Cntct = [Select Id,AccountId from Contact where Id =:emp1User.ContactId ]; 
        emp1Cntct.AccountId = business1.Id; 
        update emp1Cntct;
        //insert relationship to personal account
        Insert new AccountContactRelation(AccountId=emp1persAcct.Id,ContactId=emp1Cntct.Id,Personal__c=true);
        
        //insert relationship to sub-account
        Insert new AccountContactRelation(AccountId=business1div1.Id,ContactId=emp1Cntct.Id);
        
        Application__c app1 = testData.newapplication();
        app1.OwnerId = emp1User.Id;
        app1.Sharing_Account__c = business1.Id;
        update app1;
        
        Application__c app2 = testData.newapplication();
        app2.OwnerId = emp1User.Id;
        app2.Sharing_Account__c = business1div1.Id;
        update app2;
        
        
        System.assertEquals(business1.Id,emp1Cntct.AccountId);
        System.assertNotEquals(orgAdminUser.Id,app1.OwnerId );
        System.assertNotEquals(orgAdminUser.Id,app2.OwnerId );
        
        Test.startTest();
        system.runAs(orgAdminUser){
            ApexPages.currentPage().getParameters().put('deactivatedContactId',emp1Cntct.Id );
            ApexPages.currentPage().getParameters().put('currentAccountId',business1div1.Id );
            
            PortalController pController = new PortalController();
            pController.deactivateUser();
            
            ApexPages.currentPage().getParameters().put('deactivatedContactId',emp1Cntct.Id );
            ApexPages.currentPage().getParameters().put('currentAccountId',business1.Id );
            pController = new PortalController();
            pController.deactivateUser();
        }
        Test.stopTest();
        System.assertNotEquals(business1.Id,[Select AccountId from Contact where Id =:emp1Cntct.Id].AccountId);
        System.assertEquals(orgAdminUser.Id,[Select OwnerId from Application__c where Id =:app1.Id].OwnerId );
       System.assertEquals(orgAdminUser.Id,[Select OwnerId from Application__c where Id =:app2.Id].OwnerId );
    }   
    
    static testMethod void testDeleteRecord(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            PageReference pageRef = pController.deleteRecord();
            System.assert(pageRef != null);
        }
        Test.stopTest();
    }
    
    static testMethod void testGetlineItemRecords(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c lineItem = testData.newLineItem('Import',objapp);
        list<Link_Regulated_Articles__c> BulkLinkRegArticles = testData.newBulkLinkRegArticles(lineItem.ID, 2);
        Test.startTest();
        system.runAs(currentUser) {
            list <EFLExternalUsersHandler.lineItemsWrapper> lineItemsWrapperList = PortalController.getlineItemRecords(objapp.Id);
            System.assert(lineItemsWrapperList != null);            
        }
        Test.stopTest();
    }
    
    static testMethod void testGetReviewerRecords(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            List <EFLPartnerUsersHandler.reviewersWrapper> reviewRecords = pController.getReviewerRecords();
            System.assert(reviewRecords != null);
        }
        Test.stopTest();
    }
    
    static testMethod void testGetmyAuthorizations(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            List <Authorizations__c> authorizations = pController.getmyAuthorizations();
            System.Assert(authorizations != null);            
        }
        Test.StopTest();
    }
    
    static testMethod void testGetmyStateRegulations(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            List <Regulation__c> regulations = pController.getmyStateRegulations();
            System.assert(regulations != null);
        }
        Test.stopTest();
    }
    
    static testMethod void testRequiredAuthorizations(){
        user currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            List <Authorizations__c> authorizations = pController.getRequiredAttentionAuthorizations();
            System.Assert(authorizations != null);            
        }
        Test.StopTest();
    }
    
    static testMethod void testProperties() {
        User currentUser = EFLUserTestDataFactory.getUser('APHIS Applicant');     
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        
        Contact objcont = testData.newcontact();
        Test.startTest();
        system.runAs(currentUser) {
            //get user details
            PortalController pController = new PortalController();
            User applicant = pController.userDetails;  
            applicant.contact.id = objcont.id;
            update applicant;
            system.assert(applicant != null);
            
            //get contact details
            string contactName = pController.contactName;
            system.assert(contactName != null);
            
            //get all my contacts
            Map<Id, Contact> mycontacts = new Map<Id, Contact>();
            mycontacts = pController.mycontacts;
            System.Debug('mycontacts.size() ' + mycontacts.size());
            system.assert(mycontacts != null);  
            
            //get all associated contacts of the user
            list<Applicant_Contact__c> myAssocContacts = new list<Applicant_Contact__c>();
            myAssocContacts = pController.myAssocContacts;
            system.assert(myAssocContacts != null);  
            
            string privateColumnHelpText = pController.privateColumnHelpText; 
            string communityURL = pController.communityURL;
            string privateStatus = pController.privateStatus;
            id applicationId = pController.applicationId;
            id lineitemId = pController.lineItemId;
            string privateLink = pController.privateLink;  
            System.assertEquals(false, pController.isAdmin);  
        }
        
        Contact objcont2 = testData.newcontact();
        Applicant_Contact__c objAppContact = testData.newappcontact();        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'APHIS Broker/Preparer' LIMIT 1];
        User user2 = new User(  firstname = 'TestPreparer',
                              lastName = 'TestLastName',
                              email = 'testpreparer@test.org',
                              Username = 'testpreparer@test.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = 'tprep',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = prof.Id,
                              ContactId = objcont2.Id);
        insert user2;
        
        System.runAs(user2) {
            PortalController pController = new PortalController();
            Map<string, string> keysMap = pController.sobjectkeys;
            EFLUserUtility.userDetails = null;
            EFLUserUtility.contactDetails = null;
            pController.privateLink = null;
            String link = pController.privateLink;
            System.assert(link != null);
        }
        Test.stopTest();
    }  
    
    static testMethod void testMyAssocContacts(){
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        
        Contact objcont = testData.newcontact();
        Applicant_Contact__c objAppContact = testData.newappcontact();
        User usershare = new User();
        usershare = EFLUserTestDataFactory.getUser('eFile Applicant');           
        
        Test.startTest();
        system.runAs(usershare)
        {
            PortalController TestObj = new PortalController();
            TestObj.getlstConWrapper();
            list<account> myaccounts = new list<account>();
            myaccounts = TestObj.myAccounts;   
            list<Applicant_Contact__c> myAssocContacts = new list<Applicant_Contact__c>();
            myAssocContacts = TestObj.myAssocContacts;
            system.assert(myAssocContacts != null); 
        }
        Test.StopTest();
    }
    
    static testMethod void testAdminAccess(){ 
        user currentUser = EFLUserTestDataFactory.getUser('eFile Applicant Plus');     
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Test.startTest();
        system.runAs(currentUser) {
            PortalController pController = new PortalController();
            pController.getAdminAccess();
        }
        Test.StopTest(); 
    } 
}