public class EFLLocationRelatedGpsTriggerHandler {
    
    public static void validateGpsRecordSize(List<GPS_Coordinate__c> gpsCoords){     
        System.debug('Inside the validate Size for EFLLocationRelatedGpsTriggerHandler');
    List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Set<Self_Reporting__c> srIds = new Set<Self_Reporting__c>();
    Map<Id,GPS_Coordinate__c> mapSelfReporting = new Map<Id,GPS_Coordinate__c>();
        Boolean gpsCoordsExists=false;
        Boolean relRecordsExists=false;
        
        try{
            //Get self reporting Ids from all records that are inserted by the execution context in Trigger.new
            for(GPS_Coordinate__c gpsRec: gpsCoords){
                // Added condition for W-034597 so that null self reporting doesn't get included as part of GPS soql below
                    if(!String.isBlank(gpsRec.Self_Reporting__c))//gpsRec.Self_Reporting__c != null && gpsRec.Self_Reporting__c != '')
                    {
                        //If Record is not being deleted, then we are setting gpsCoordsExists to true for new record insertion
                        if(!trigger.isDelete){
                            gpsCoordsExists = true;
                        }
                        mapSelfReporting.put(gpsRec.Self_Reporting__c, gpsRec);
                        System.debug('mapSelfReporting>>'+mapSelfReporting);
                    }
               }
     
            Integer gpsCount=0;
            if(!mapSelfReporting.isEmpty()){
                System.debug('gpsCount in  loop>>'+gpsCount);
             //Retrieve all exisiting GPS Coordinate records for the self reporting Ids on records being inserted
                for(GPS_Coordinate__c gps: [Select id,Self_Reporting__c from GPS_Coordinate__c where Self_Reporting__c in :mapSelfReporting.keyset()]){
                    gpsCount++;
                    System.debug('gpsCount in for loop>>'+gpsCount);
                    //Validate if there are more than 6 GPS Coordinate records
                    if(gpsCount>6){
                        gps.addError('You can not add more than 6 co-ordinates');
                    }// end if
                }// end for loop
            }
            //Retrieve all Related records for the self reporting Ids on records being inserted 
            for(EFL_Related_Record__c relRec : [Select id,Self_Reporting__r.Ready__c from EFL_Related_Record__c 
                                                Where Self_Reporting__c in :mapSelfReporting.keyset()]){
                                               //If records exist we set relRecordsExists to true
                                                    relRecordsExists=true;
                                                    //When both Boolean values are true, we set Self Report field Ready to true 
                                                    if(relRecordsExists && gpsCoordsExists){
                                                        relRec.Self_Reporting__r.Ready__c = true;
                                                        srIds.add(relRec.Self_Reporting__r);
                                                    }else if (relRecordsExists && gpsCount == 0){
                                                        relRec.Self_Reporting__r.Ready__c = false;
                                                        srIds.add(relRec.Self_Reporting__r);
                                                    }
    
          }
            //Updating the 
            if(!srIds.isEmpty()){
                System.debug('srIds>>'+srIds);
                srList.addall(srIds);
                System.debug('srList>>'+srList);
                update srList;
            }    
        }
        catch(Exception e){
            //Error log
           // EFLErrorLog.createErrorLog('EFLLocationRelatedGpsTriggerHandler.validateGpsRecordSize()',e);  
        }
    }
   
}