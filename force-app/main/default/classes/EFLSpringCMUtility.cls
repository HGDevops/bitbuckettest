/*-----------------------------------------------------------------------  
 @Purpose: This Utility handles few core functions of springCM 
 ----------------------------------------------------------------------*/
public with sharing class EFLSpringCMUtility {
    
/*
* Attributes
*/ 
    final static string aid = 'aid';
    final static string docURL = 'Doc Launcher URL';
    final static string sandboxName = 'Sandbox';
    
   /*
   *@purpose:  Returns Doclauncher URL 
   */
      public static String buildDocLauncherURL(ID objectID, string recordName, String CBIFlagDescription, string functionaltype){
         //Object API Name
         string sObjAPIName = objectID.getSObjectType().getDescribe().getName();
         
         //get springCM URL and floder structure 
         list<EFLSpringURL__mdt> springURLMD = new list<EFLSpringURL__mdt>();
              springURLMD = [SELECT Folder_Structure__c,
                                    SpringCM_URL__c 
                               FROM EFLSpringURL__mdt 
                              WHERE Type__c =: functionaltype
                              LIMIT 1];
          
         //get springCM URL and floder structure 
         map<string,string> springCMAccountMap = new map<string,string>();
          for(SpringCM_Components__mdt scm : [SELECT MasterLabel,Component_Value__c 
                                                FROM SpringCM_Components__mdt]){
                                                    
              springCMAccountMap.put(scm.MasterLabel,scm.Component_Value__c);
          }         

           //build Document Launcher URL 
           string docLauncherURL = springCMAccountMap.get(docURL)+                            // Doc Launcher URL
                                   springURLMD[0].SpringCM_URL__c +                           // Name of the Doc Launcher
                                   '?aid='+springCMAccountMap.get(aid) +                      // springCM Account ID
                                   '&eos[0].Id='+objectID +                                   // Record Object ID
                                   '&eos[0].System=Salesforce&eos[0].Type='+sObjAPIName+
                                   '&eos[0].Name=' + recordName + 
                                   '&eos[0].ScmPath=/' + springCMAccountMap.get(sandboxName) + // Sandbox Name
                                   '/' + springURLMD[0].Folder_Structure__c; // Folder Structure name
          
          if(functionaltype == 'Live Dogs'){
               RegulatedArticle_Information__c temp1 = [SELECT Id, Name, Line_Item__c FROM RegulatedArticle_Information__c WHERE Id =: objectId];
               AC__c temp2 = [SELECT Id, Name FROM AC__c WHERE Id =: temp1.Line_Item__c];
               docLauncherURL += '/' + temp2.Name;
          }
         
         //returns Document Launcher URL        
         return docLauncherURL;
    }
    
    //This method will copy salesforce attachment into SpringCM in specified folder
    /*@future(callout=true)
    public static void MoveAttachmentToSpringCM(String objectId, String objectType)
    {
        SpringCMService springcm = new SpringCMService();
        SpringCMFolder folder = springcm.findOrCreateEosFolder(objectId, objectType);
        Attachment attachments = [Select Id, Name, Body from 
                                        Attachment where ParentId = :objectId order by createdDate DESC limit 1];
                
        springcm.uploadDocument(folder, attachments.Body, attachments.Name.replace('/','-'));
        
    }*/
    
    
}