/**
* Trigger Handler for the Authorization SObject. This class implements the EFLITrigger
* interface to help ensure the trigger code is bulkified and all in one place.
*/
public with sharing class EFLAuthorizationTriggerHandler implements EFLITrigger {
    /*
* Attributes
*/
    list<authorizations__c> approvedAuthorizations = new list<authorizations__c>();
    list<authorizations__c> issuedAuthList = new list<authorizations__c>();
    list<authorizations__c> issuedAuthListSCM = new list<authorizations__c>();
    list<sObject> activityList = new list<sObject>();
    list<sObject> finalActivityList = new list<sObject>();
    list<EFLAuthorizationActivityWrapper> authActivityWrapperList = new list<EFLAuthorizationActivityWrapper>();
    list<authorizations__c> authorizationsToUpdate = new list<authorizations__c>();
    list<authorizations__c> waitingOnCustAuthList = new list<authorizations__c>();
    Map<String, List<AC__c>> lineItemDataSet = new Map<String, List<AC__c>>();
    public static List<EFLAuthStatusValidation__mdt> statusFromCMD;
    public static set<string> statusSet;
    public static Boolean runWorkflow1 = true;
    public static Boolean runWorkflow2 = true;
    public static Boolean runWorkflow3 = true;
    
    final String WorkflowNotificationCBIApplication = 'Notification CBI Application';
    final String WorkflowNotificationNoCBIApplication = 'Notification No CBI Application';
    final String WorkflowPermitNoCBIApplication = 'Permit No CBI Application';
    final String WorkflowPermitCBIApplication = 'Permit CBI Application';
    final String workflowLineItemStgLock = 'LineItemLockFolders'; // Lock Line Item SpringCM widget after the stage is reached State Review
    //Added by Conor 4/11 for W-034688
    final String workflowCreateAuthFolders = 'Authorization Folder Creation';
    //Workflow for cloning parent auth attachments and copying them to the ammen/renew auth
    final String workflowCloneAuthAttachments = 'Clone Authorization Attachments';
    final String workflowAmenRenewCBP = 'Amendment Renewal CBP';
    final String acProgramConstant = 'AC';
    List<Authorizations__c> CBIAuthorizationsPermit = new List<Authorizations__c>();
    List<Authorizations__c> noCBIAuthorizationsPermit = new List<Authorizations__c>();
    List<Authorizations__c> CBIAuthorizationsNotification = new List<Authorizations__c>();
    List<Authorizations__c> noCBIAuthorizationsNotification = new List<Authorizations__c>();
    List<Authorizations__c> renewalAuthList = new List<Authorizations__c>();
    List<Authorizations__c> ammendAuthList = new List<Authorizations__c>();
    List<Authorizations__c> issuedRenewalAuthList = new List<Authorizations__c>();
    List<Authorizations__c> issuedAmmendAuthList = new List<Authorizations__c>();
    
    
    final string APP_TYPE_AMENDMENT ='Amendment'; //RR 1/30/2019 W-026318
    final string APP_TYPE_RENEWAL ='Renewal';     //RR 1/30/2019 W-026318
    //final string RENEWAL_WITHOUTCHANGES_APP_TYPE ='Renewal Without Changes';     //RR 1/30/2019 W-026318    
    final string AUTH_STATUS_ISSUED ='Issued';     //RR 1/30/2019 W-026318 
    final string AUTH_TYPE_PERMIT ='Permit';     //RR 1/30/2019 W-026318
    
    final string AUTH_REC_TYPE_NOTIFICATION ='Biotechnology Regulatory Services-Acknowledgement';
    final string AUTH_REC_TYPE_PERMIT ='Biotechnology Regulatory Services - Standard Permit';
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
* Checks to see if the trigger has been disabled either by custom setting or by running code
*/  
    
    public Boolean IsDisabled() {
        if (EFLGenericUtility.isTriggerDisabled('EFLAuthorizationTrigger')) {
            return true;
        } else {
            return TriggerDisabled;
        }
    }
    
    /*
* Constructor
*/
    public EFLAuthorizationTriggerHandler() {
        statusSet = new set<string>();
        statusSet = validAuthstatus();
    }
    
    /**
* bulkBefore
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore() {
        
        if(avoidrecursion.isfirstRunBulkBefore()){
            
            list<Authorizations__c> AuthStatusValidateList = new list<Authorizations__c>();
            if (trigger.IsUpdate) {                
                
                for (Authorizations__c a: (List<Authorizations__c>)trigger.new) {
                    if (a.Program__c=='BRS'&& statusSet.contains(a.Status__c)) {
                        AuthStatusValidateList.add(a);
                    }
                }
                
                if (AuthStatusValidateList.size()>0) {
                    EFLBRSAuthorizationEngine.getAuthorizationData(AuthStatusValidateList);
                }
                moveSubmittedReports(Trigger.OldMap, Trigger.newMap);
            }
            
            if(trigger.IsInsert || trigger.IsUpdate) {
                Set<Id> applicationIds = EFLAmendmentAndRenewalValidationHelper.getAppIds((List<Authorizations__c>)trigger.new);
                List<Application__c> applicationsList = EFLAmendmentAndRenewalValidationHelper.queryApplications(applicationIds);
                Set<Id> relatedAppIds = EFLAmendmentAndRenewalValidationHelper.getRelatedAppIds(applicationsList);
                Map<Id, Id> applicationtoRelatedApplicationMap = EFLAmendmentAndRenewalValidationHelper.mapApplicationIdToRelatedApplicationId(applicationsList);
                List<Authorizations__c> relatedAuthorizationsList = EFLAmendmentAndRenewalValidationHelper.queryAuthorizations(relatedAppIds);
                Map<Id, Authorizations__c> relatedAuthMap = EFLAmendmentAndRenewalValidationHelper.mapAuthorizationsToRelatedApplications(relatedAuthorizationsList);
                for (Authorizations__c a: (List<Authorizations__c>)trigger.new) {
                    if ( a.Effective_Date__c != null && a.Expiration_Date__c != null) {
                        if ( a.Expiration_Date__c > a.Effective_Date__c.addYears(1)) {
                            a.Multi_Year__c = True;
                        } else {
                            a.Multi_Year__c = False;
                        }
                    }
                    //W-026320 Start
                    EFLAmendmentAndRenewalValidationHelper.linkAuthorizations(a, relatedAuthMap, applicationtoRelatedApplicationMap);
                    //W-026320 END
                    //Permit Date Validations
                    if(a.RecordTypeId == Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get(AUTH_REC_TYPE_PERMIT).getRecordTypeId()){
                        if (a.BRS_Introduction_Type__c=='Import' || a.BRS_Introduction_Type__c== 'Interstate Movement'){
                            if ( a.Expiration_Date__c > a.Effective_Date__c.addYears(1)){
                                a.Expiration_Date__c.addError('Expiration Date must be within one year from the Effective date.'); 
                            }
                        }
                        if (a.BRS_Introduction_Type__c=='Release' || a.BRS_Introduction_Type__c== 'Interstate Movement and Release'){
                            if ( a.Expiration_Date__c > a.Effective_Date__c.addYears(3)){
                                a.Expiration_Date__c.addError('Expiration Date must be within three years from the Effective date.'); 
                            }
                        }
                    }
                    if(a.RecordTypeId == Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get(AUTH_REC_TYPE_NOTIFICATION).getRecordTypeId()){
                        if ( a.Expiration_Date__c > a.Effective_Date__c.addYears(1)){
                            a.Expiration_Date__c.addError('Expiration Date must be within one year from the Effective date.'); 
                        }
                    }    
                }
            }
        }
    }
    
    /**
* bulkAfter
* This method is called prior to execution of an AFTER trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkAfter() {
        
        list<Authorizations__c> auths = (List<Authorizations__c>)trigger.new;
        map<Id, Authorizations__c> oldMap = (map<Id, Authorizations__c>)trigger.oldMap;
        string superceded = EFLGlobalConstants.getConstantValue('AUTH_STATUS_SUPERCEDED');
        set<Id> authIds = new set<Id>();
        set<id> AmendAuthIds = new set<id>();  
        set<id> RenewalAuthIds = new set<id>();
        
        list<Authorizations__c> changedSharingAccount = new list<Authorizations__c>();
        if(trigger.isUpdate){
            for(Authorizations__c auth: auths){
                if(auth.Status__c == superceded && oldMap.get(auth.Id).Status__c != auth.Status__c){
                    authIds.add(auth.Id);
                }
            }
            if(!authIds.isEmpty()){
                string voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
                string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
                EFLLabelServices.changeLabelStatus(authIds, active, voided);
            }
        } 
        //RR 1/30/2019 W-026318 Begin
        if(avoidrecursion.isfirstRunBulkAfter()){  
            if(trigger.isUpdate){   
                for(Authorizations__c auth: auths){
                    if (auth.Application_Type__c == APP_TYPE_RENEWAL 
                        && auth.Authorization_Type__c == AUTH_TYPE_PERMIT && auth.Status__c == AUTH_STATUS_ISSUED){
                            Renewalauthids.add(auth.id); 
                        } 
                    
                    //amends
                    if (auth.Application_Type__c == APP_TYPE_AMENDMENT 
                        && auth.Authorization_Type__c == AUTH_TYPE_PERMIT && auth.Status__c == AUTH_STATUS_ISSUED){
                            Amendauthids.add(auth.id); 
                        } 
                    //RR 1/30/2019 W-026318 End
                    
                    if(auth.Sharing_Account__c != oldMap.get(auth.Id).Sharing_Account__c){
                        changedSharingAccount.add(auth);
                    }
                }
                
                
                //RR 1/30/2019 W-026318 Begin
                if(!Amendauthids.isEmpty()){
                    EFLAuthorizationEngineUtility.TransferRecordsForAmends(Amendauthids, auths);
                }
                if(!RenewalauthIds.isEmpty()){
                    EFLAuthorizationEngineUtility.TransferRecordsForRenewals(RenewalauthIds);
                }        
                //RR 1/30/2019 W-026318 End
                
                if(!changedSharingAccount.isEmpty()){
                    EFLBRSAuthorizationEngine engine = (EFLBRSAuthorizationEngine)EFLAuthorizationEngineFactory.getEngineByProgram('BRS');
                    engine.shareAuthorizationToAccount(changedSharingAccount);
                }
            }
            
        } 
    }
    
    /**
* beforeInsert
* This method is called iteratively for each record to be inserted during a BEFORE
* trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
*/
    public void beforeInsert(SObject so) {        
        EFLIAuthorizationEngine engine = EFLAuthorizationEngineFactory.getEngine((authorizations__c)so);
        engine.createAuthorization((authorizations__c)so);
    }
    
    
    /**                                     
* beforeUpdate
* This method is called iteratively for each record to be updated during a BEFORE
* trigger.
*/
    public void beforeUpdate(SObject oldSo, SObject so) {
        
        authorizations__c oldAuthRecord = (authorizations__c)oldSo;
        authorizations__c newAuthRecord = (authorizations__c)so;
        
        EFLIAuthorizationEngine engine = EFLAuthorizationEngineFactory.getEngine(newAuthRecord);
        
        if (engine.validate(oldAuthRecord,newAuthRecord)) {
            engine.updateAuthorization(oldAuthRecord,newAuthRecord);
        }
        
        //validate task creation and Load all activities along with authorizations
        if (EFLActivityUtility.needsTaskCreation(oldAuthRecord,newAuthRecord)) {
            newAuthRecord.Activity_Sequence__c = 0;
            engine.loadActivities(newAuthRecord, activityList);
            finalActivityList.addAll(activityList);
            authorizationsToUpdate.add(newAuthRecord);
        }
        
        if (newAuthRecord.Program__c=='BRS' && statusSet.contains(newAuthRecord.Status__c)) {
            EFLBRSAuthorizationEngine.validateLineItemsWithAuthorizations(newAuthRecord);
        }
    }
    
    
    /**
* beforeDelete
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete(SObject so) {}
    
    /**
* afterInsert
* This method is called iteratively for each record inserted during an AFTER
* trigger. Always put field validation in the 'After' methods in case another trigger
* has modified any values. The record is 'read only' by this point.
*/
    public void afterInsert(SObject so) {
        
        authorizations__c newAuthRecord = (authorizations__c)so;
        if (newAuthRecord.Status__c == 'Submitted' && (newAuthRecord.RecordTypeName__c == AUTH_REC_TYPE_PERMIT || newAuthRecord.RecordTypeName__c == AUTH_REC_TYPE_NOTIFICATION) ) {
            if (newAuthRecord.Application_CBI__c == 'Yes') {
                if (newAuthRecord.Authorization_Type__c == 'Permit') {
                    CBIAuthorizationsPermit.add(newAuthRecord);
                } else if (newAuthRecord.Authorization_Type__c == 'Notification') {
                    CBIAuthorizationsNotification.add(newAuthRecord);
                }
            } else {
                if (newAuthRecord.Authorization_Type__c == 'Permit') {
                    noCBIAuthorizationsPermit.add(newAuthRecord);
                } else if (newAuthRecord.Authorization_Type__c == 'Notification') {
                    noCBIAuthorizationsNotification.add(newAuthRecord);
                }
            }
        }
        
        if (newAuthRecord.Application_Type__c == APP_TYPE_RENEWAL){
            renewalAuthList.add(newAuthRecord); 
        } 
        
        if (newAuthRecord.Application_Type__c == APP_TYPE_AMENDMENT){
            ammendAuthList.add(newAuthRecord); 
        } 
    }
    
    /**
* afterUpdate
* This method is called iteratively for each record updated during an AFTER
* trigger.
*/
    public void afterUpdate(SObject oldSo, SObject so) {
        
        authorizations__c authRecord = (authorizations__c)so;
        authorizations__c authRecordOld = (authorizations__c)oldSo;
        List<AC__c> LineLst =[Select Id,Name,Authorization__c,AuthStage__c from AC__c where Authorization__c =:authRecord.Id]; //Added by Nupur To update authStage field in Line item as 'State Review'
        List<Authorizations__c> authList = new List<Authorizations__c>();
        authList.add(authRecord);
        
        if (authRecord.Status__c == 'Approved') {
            approvedAuthorizations.add(authRecord);
        } else if (authRecord.Status__c == 'Issued') {
            issuedAuthList.add(authRecord);
            if(authRecord.Program__c != 'AC'){
                issuedAuthListSCM.add(authRecord);
            }
            if (authList.Size() > 0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), authList.get(0).getSObjectType().getDescribe().getName(), authList, 'External Users View Only - Auth');
            }
            
            if (authRecord.Application_Type__c == APP_TYPE_RENEWAL){
                issuedRenewalAuthList.add(authRecord); 
            }
            
            if (authRecord.Application_Type__c == APP_TYPE_AMENDMENT){
                issuedAmmendAuthList.add(authRecord); 
            }
        } else if(authRecord.Status__c =='Acknowledged'){
            issuedAuthListSCM.add(authRecord);
        }
        
        if (((authRecordOld.status__c!='Waiting on Customer' && authRecord.Status__c == 'Waiting on Customer') ||(authRecordOld.status__c!='Withdrawn'&& authRecord.Status__c == 'Withdrawn')) &&authRecord.Program__c=='BRS') {  
            waitingOnCustAuthList.add(authRecord);
        }
        
        if(authRecord.Status__c == 'Submitted' && authRecord.Status__c <> authRecordOld.Status__c && (authRecord.RecordTypeName__c == AUTH_REC_TYPE_PERMIT || authRecord.RecordTypeName__c == AUTH_REC_TYPE_NOTIFICATION) ){
            if(authRecord.Application_CBI__c == 'Yes'){
                if(authRecord.Authorization_Type__c == 'Permit'){
                    CBIAuthorizationsPermit.add(authRecord);
                }
                else if(authRecord.Authorization_Type__c == 'Notification'){
                    CBIAuthorizationsNotification.add(authRecord);  
                }
            }
            else
            {
                if(authRecord.Authorization_Type__c == 'Permit'){
                    noCBIAuthorizationsPermit.add(authRecord);
                }
                else if(authRecord.Authorization_Type__c == 'Notification'){
                    noCBIAuthorizationsNotification.add(authRecord);  
                }
                
            }
        }
        
        
        if ((authRecord.Application_Type__c == 'Amendment' || authRecord.Application_Type__c == 'Renewal')   && authRecord.Authorization_Type__c == 'Permit' && authRecord.Status__c == 'Issued'){
            EFLAuthorizationEngineUtility.supercedeParentAuthorization(authRecord);
        }
        //Lock Line Item SpringCM widget after the stage is reached State Review,Trigger SpringCM workflow
        If (authRecord.Stage__c=='State Review')
        {
            if(LineLst.Size()>0)
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), LineLst.get(0).getSObjectType().getDescribe().getName(), LineLst, workflowLineItemStgLock);
        }
    }
    
    /**
* afterDelete
* This method is called iteratively for each record deleted during an AFTER
* trigger.
*/ 
    public void afterDelete(SObject so){ }
    
    /**
* andFinally
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    public void andFinally(){
        try{
            if(trigger.isbefore){
                if(finalActivityList != null){
                    Database.SaveResult[] results = Database.insert(finalActivityList,true); 
                }
            }
            //update line item status
            if(waitingOnCustAuthList.size()>0){
                EFLBRSAuthCheckStatusUtility.updateLineItemsFromAuthorizations(waitingOnCustAuthList); 
            }
            
            //handle communication 
            if(issuedAuthList != null){
                for(authorizations__c authRecord : issuedAuthList){
                    EFLIAuthorizationEngine engine = EFLAuthorizationEngineFactory.getEngine(authRecord); 
                    engine.HandleCommunication(authRecord);
                }
            }
            if(noCBIAuthorizationsNotification.size()>0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), noCBIAuthorizationsNotification.get(0).getSObjectType().getDescribe().getName(), noCBIAuthorizationsNotification, WorkflowNotificationNoCBIApplication);
            }
            if(CBIAuthorizationsNotification.size()>0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), CBIAuthorizationsNotification.get(0).getSObjectType().getDescribe().getName(), CBIAuthorizationsNotification, WorkflowNotificationCBIApplication);
            }
            
            if(noCBIAuthorizationsPermit.size()>0){
                If(!test.isRunningTest()){
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), noCBIAuthorizationsPermit.get(0).getSObjectType().getDescribe().getName(), noCBIAuthorizationsPermit, WorkflowPermitNoCBIApplication);
                }
            }
            if(CBIAuthorizationsPermit.size()>0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), CBIAuthorizationsPermit.get(0).getSObjectType().getDescribe().getName(), CBIAuthorizationsPermit, WorkflowPermitCBIApplication);
            }
            
            if(renewalAuthList.size()>0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), renewalAuthList.get(0).getSObjectType().getDescribe().getName(), renewalAuthList, workflowCloneAuthAttachments);
            }
            
            if(ammendAuthList.size()>0){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), ammendAuthList.get(0).getSObjectType().getDescribe().getName(), ammendAuthList, workflowCloneAuthAttachments);
            }
            
            if(runWorkflow1){
                if(issuedRenewalAuthList.size()>0){
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), issuedRenewalAuthList.get(0).getSObjectType().getDescribe().getName(), issuedRenewalAuthList, workflowAmenRenewCBP);
                    runWorkflow1 = false;
                }   
            }
            
            if(runWorkflow2){
                if(issuedAmmendAuthList.size()>0){
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), issuedAmmendAuthList.get(0).getSObjectType().getDescribe().getName(), issuedAmmendAuthList, workflowAmenRenewCBP);
                    runWorkflow2 = false;
                }   
            }
            
            
            if(runWorkflow3){
                if(issuedAuthListSCM.Size() > 0){
                    SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), issuedAuthListSCM.get(0).getSObjectType().getDescribe().getName(), issuedAuthListSCM, workflowCreateAuthFolders);
                    runWorkflow3 = false;
                }
            }
            
            GenericHistoryClass.LogChangeHistory();
        }
        catch(Exception e){
            EFLErrorLog.createErrorLog('EFLAuthorizationTriggerHandler.andFinally()',e);                
        }
        
    }
    
    private static set<string> validAuthstatus(){
        
        set<string> statusSet = new set<string>();
        statusFromCMD = [SELECT MasterLabel, Statuses__c, Message__c FROM EFLAuthStatusValidation__mdt];
        
        for (EFLAuthStatusValidation__mdt m :  statusFromCMD) {
            statusSet.add(m.MasterLabel);
        }
        return statusSet;
    }
    
    /**
* moveSubmittedReports.
* This method is called before Update
**/
    private void moveSubmittedReports(Map<Id, SObject> oldMap, Map<Id, SObject> newMap){
        
        Set<Id> AmenAuthIdSet = new Set<Id>();
        List<Amendment_Renewal__c> amendmentsList = new List<Amendment_Renewal__c>();
        Map<id, Amendment_Renewal__c> ParentAuthIds = new Map<id, Amendment_Renewal__c>();
        for(Authorizations__c auth : (List<Authorizations__c>)newMap.values()){
            if (auth.Application_Type__c == APP_TYPE_AMENDMENT 
                && auth.Authorization_Type__c == AUTH_TYPE_PERMIT && auth.Status__c == AUTH_STATUS_ISSUED){
                    AmenAuthIdSet.add(auth.id); 
                } 
        }
        amendmentsList = [SELECT Parent_Authorization__c, Child_Authorization__c 
                          FROM Amendment_Renewal__c 
                          WHERE Child_Authorization__c in :AmenAuthIdSet];
        
        if (amendmentsList.size() > 0 ){
            for (Amendment_Renewal__c ar :amendmentsList ){
                ParentAuthIds.put(ar.Parent_Authorization__c, ar);   
            }
        }
        
        Map<Id, Authorizations__c> parentAuthorizations = new Map<Id, Authorizations__c>(
            [select Id, Reports_Submitted__c from Authorizations__c where Id in :parentAuthids.keySet()]);
        
        for(Authorizations__c parentAuth : parentAuthorizations.values()){
            Authorizations__c newAuth = (Authorizations__c)newMap.get(ParentAuthIds.get(parentAuth.Id).Child_Authorization__c);
            newAuth.Reports_Submitted__c = parentAuth.Reports_Submitted__c;
        }
    }
    
}