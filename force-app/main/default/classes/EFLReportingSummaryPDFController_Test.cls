@isTest(seeAllData=true)
public class EFLReportingSummaryPDFController_Test{
    @isTest
    static void EFLReportingSummaryPDFController_TestMethod1(){
        Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];         
        system.runAs(u){
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            CARPOL_BRS_TestDataManager testData2=new  CARPOL_BRS_TestDataManager();
            String AccountRecordTypeId = testData.AccountRecordTypeId; testData.insertcustomsettings();
            Account objacct = testData.newAccount(AccountRecordTypeId); 
            Contact objcont = testData.newcontact();
            
            
            Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
            plip.Column_1_API_Name__c = 'Scientific_Name__r.Name';
            plip.Column_2_API_Name__c = 'Country_Of_Origin__r.Name';
            plip.Column_3_API_Name__c = 'Component__r.Name';
            plip.Column_4_API_Name__c = 'Where_will_the_RA_be_grown__c';
            plip.Column_5_API_Name__c = 'Intended_Use__r.Name';
            plip.Column_6_API_Name__c = 'Total_number_of_shipments__c';
            update plip;
            Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
            objdm.Program_Line_Item_Pathway__c = plip.id;
            insert objdm;
            Application__c objapp = testData.newapplication();
            AC__c li = TestData.newlineitem('Personal Use', objapp);
            Level_1_Region__c FacState = testdata.newlevel1regionAL();
            Facility__c objFac = testData.newfacility('Domestic Port');
            Facility__c objFac1 = testData.newfacility('Domestic Port');
            Facility__c facility = new Facility__c();
            country__c FacCountry = testdata.newcountryus();
            facility.name = 'Weeds Test Facility';
            facility.Facility_ID__c = 1234567890 ;
            facility.Address_1__c = '123 Weeds Test St';
            facility.Address_2__c = 'Suite 123';
            facility.City__c = 'Fairfax';
            facility.State__c = FacState.id;         
            facility.Zip__c = '12345';        
            facility.Country__c = FacCountry.id;          
            facility.Approved__c = 'Yes';
            insert facility;
            Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
            Authorizations__c objauth = testData.newAuth(objapp.id);
            li.Authorization__c = objauth.id;
            li.approved_facility__c = facility.id;
            li.Does_This_Application_Contain_CBI__c = 'Yes';
            update li;
            objauth.Authorization_Type__c = 'Permit';
            objauth.Template__c = objcm.id;
            objauth.Final_Decision_Matrix_Id__c = objdm.id;
            objauth.Date_Issued__c = Date.today();          
            objauth.Program_Pathway__c = plip.id;
            update objauth;
            string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
            Country__c UScountry=testdata2.newcountryus();
            Level_1_Region__c level1reg=testdata2.newlevel1region(UScountry.id);
            Level_2_Region__c level2reg=testdata2.newlevel2region(level1reg.id);
            location__c orgloc=testData2.newlocation(UScountry.id,level1reg.id,level2reg.id,li.id,olocrectypeid);
            Regulation__c objReg = testData.newRegulation('Import Requirements','Import Permit Requirements');
            
            Authorization_Junction__c objAJ = testData.newAuthorizationJunction(objauth.Id, objReg.Id);
            objAJ.Port__c = objFac.Id;
            update objAJ;
            
            //Authorization_Junction__c objAJ1 = testData.newAuthorizationJunction(objauth.Id, objReg.Id);
            //objAJ1.Port__c = objFac1.Id;
            //update objAJ1;
            Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
            
            Report_Summary__c reportSum = new Report_Summary__c();
            reportSum.Authorization__c = objauth.id;
            reportSum.Submitted_Date__c = Date.today();
            reportSum.Report_Type__c = 'Planting/Release Reports';
            insert reportSum;
            
            Applicant_Attachments__c att= testdata.newAttach(li.id);
            att.Report_Summary__c = reportSum.id;       
            String AppAttRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Report Summary').getRecordTypeId();
            att.RecordTypeId = AppAttRecordTypeId;
            update att;
            
            
            
            test.startTest();
            self_reporting__c selfReport = new self_reporting__c();
            selfReport.Start_Date__c = Date.Today();
            // selfReport.Planting_ID__c = '123456';
            selfReport.Planting_ID__c = orgloc.id;
            selfReport.Final_Volunteer_Monitoring_Report__c ='No';
            selfreport.Report_Summary__c = reportSum.id;
            selfreport.Monitoring_Period_Start__c = date.today()- 62;
            selfreport.Monitoring_Period_End__c = date.today() - 60;
            selfreport.Observation_Date__c = date.today() - 61;
            //insert selfReport;
            
            //EFL_Related_Record__c relRecords = new EFL_Related_Record__c();
            //relRecords.Self_Reporting__c = selfReport.id;
            //String relRecordTypeId = Schema.SObjectType.EFL_Related_Record__c.getRecordTypeInfosByName().get('Observations').getRecordTypeId();
            //relRecords.RecordTypeId = relRecordTypeId;
            //insert relrecords;
            
            PageReference pageRef = Page.EFLReportingSummaryPDF; 
            System.Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('version','CBIDeleted');
            System.currentPageReference().getParameters().put('id',li.id);
            System.currentPageReference().getParameters().put('Authid',objauth.id);
            System.currentPageReference().getParameters().put('consolidated','true');
            System.currentPageReference().getParameters().put('reportType','Planting Report');
            System.currentPageReference().getParameters().put('reportid',reportSum.id);
            System.currentPageReference().getParameters().put('Version','CBIDeleted');
            ApexPages.StandardController sc = new ApexPages.StandardController(li);
            EFLReportingSummaryPDFController classInst =  new EFLReportingSummaryPDFController(sc);
            test.stopTest();
        }
    }
    
    @isTest
    static void EFLReportingSummaryPDFController_TestMethod2(){
        Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];         
        system.runAs(u){          
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            CARPOL_BRS_TestDataManager testData2=new  CARPOL_BRS_TestDataManager();
            Application__c objapp = testData.newapplication();
            Authorizations__c objauth = testData.newAuth(objapp.id);
            AC__c li = TestData.newlineitem('Personal Use', objapp);
            li.Authorization__c = objauth.id;
            li.Does_This_Application_Contain_CBI__c = 'Yes';
            update li;          
            
            Report_Summary__c reportSum = new Report_Summary__c();
            reportSum.Authorization__c = objauth.id;
            reportSum.Submitted_Date__c = Date.today();
            reportSum.Report_Type__c = 'Planting/Release Reports';
            insert reportSum;
            
            Applicant_Attachments__c att= testdata.newAttach(li.id);
            att.Report_Summary__c = reportSum.id;       
            String AppAttRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Report Summary').getRecordTypeId();
            att.RecordTypeId = AppAttRecordTypeId;
            update att;
            
            string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
            Country__c UScountry=testdata2.newcountryus();
            Level_1_Region__c level1reg=testdata2.newlevel1region(UScountry.id);
            Level_2_Region__c level2reg=testdata2.newlevel2region(level1reg.id);
            location__c orgloc=testData2.newlocation(UScountry.id,level1reg.id,level2reg.id,li.id,olocrectypeid);
            
            self_reporting__c selfReport = new self_reporting__c();
            selfReport.Start_Date__c = Date.Today();
            //selfReport.Planting_ID__c = '123456';
            selfReport.Planting_ID__c = orgloc.id;
            selfReport.Final_Volunteer_Monitoring_Report__c ='No';
            selfreport.Report_Summary__c = reportSum.id;
            selfreport.Monitoring_Period_End__c = Date.today().addDays(-10);
            selfReport.Monitoring_Period_Start__c = Date.today().addDays(-20);
            selfReport.Release_Record_ID__c = orgloc.id;
            insert selfReport;
            
            EFL_Related_Record__c relRecords = new EFL_Related_Record__c();
            relRecords.Self_Reporting__c = selfReport.id;
            String relRecordTypeId = Schema.SObjectType.EFL_Related_Record__c.getRecordTypeInfosByName().get('Observations').getRecordTypeId();
            relRecords.RecordTypeId = relRecordTypeId;
            insert relrecords;
            
            /* string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
Country__c UScountry=testdata2.newcountryus();
Level_1_Region__c level1reg=testdata2.newlevel1region(UScountry.id);
Level_2_Region__c level2reg=testdata2.newlevel2region(level1reg.id);
location__c orgloc=testData2.newlocation(UScountry.id,level1reg.id,level2reg.id,li.id,olocrectypeid);*/
            
            Construct__c construct = testdata2.newconstruct(li.id);
            
            Genotype__c genotype = testdata2.newgenotype(construct.id);
            
            Phenotype__c phenotype = testdata2.newphenotype(construct.id);
            test.startTest();
            PageReference pageRef = Page.EFLReportingSummaryPDF; 
            System.Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id',li.id);
            System.currentPageReference().getParameters().put('Authid',objauth.id);
            System.currentPageReference().getParameters().put('reportid',reportSum.id);
            System.currentPageReference().getParameters().put('Version','CBIDeleted');
            ApexPages.StandardController sc = new ApexPages.StandardController(li);
            EFLReportingSummaryPDFController classInst =  new EFLReportingSummaryPDFController(sc);
            test.stopTest();
        }
    }
}