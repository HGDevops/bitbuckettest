public with sharing class EFLSelfReportingUtility {
	//get Release Locations & Related Self Reports for an authorization based on reportType
    public static list<Location__c> getRelLocationsbyAuthId(ID authorizationId, String reportType){
        list<Location__c> rellocList = new list<Location__c>();
        list<Self_Reporting__c> plantSRList = new list<Self_Reporting__c>();
        Set<Id> locIds = new Set<Id>();
        list<Location__c> relplantlocList = new list<Location__c>();
        List<Ac__c> lineitems = new List<Ac__c>([select id,Name,Application_Number__c,Does_This_Application_Contain_CBI__c,Regulated_Article__c from ac__c 
                                                 where authorization__c = :authorizationId]);
        if(!lineitems.isEmpty()){
            Id lineItemId;
            lineItemId = !lineitems.isEmpty() ? lineItems[0].id : lineItemId;
            
            string locationRecordType = [SELECT id FROM RecordType where SobjectType='Location__c' and DeveloperName = 'Release_Location'].id;
            
            if(reportType == CARPOL_Constants.PLANTING_REPORT){
                rellocList = EFLLocationRepository.selectLocsWithRelSRsByLineItemId(lineitemId, locationRecordType, getExistingSRList(authorizationId, reportType));  
            }else if(reportType == CARPOL_Constants.VOLUNTEER_MONITORING_REPORT || reportType == CARPOL_Constants.FIELD_TEST_REPORT){
                plantSRList = getExistingSRList(authorizationId, CARPOL_Constants.PLANTING_REPORT);
                if(!plantSRList.isEmpty()){
                    for (Location__c relplantloc : EFLLocationRepository.selectLocsWithRelSRsByLineItemId(lineitemId, locationRecordType, plantSRList)){
                        locIds.add(relplantloc.Id);
                    } 
                    rellocList = EFLLocationRepository.selectLocsWithRelVolSRsByLineItemId(locIds,getExistingSRList(authorizationId, CARPOL_Constants.VOLUNTEER_MONITORING_REPORT));
                }
            }
        }
     return rellocList;
    }
 
    //get SRs Information for an authorization each reportType
   public static list<Self_Reporting__c> getExistingSRList(Id authorizationId, String reportType){
       	List<Self_Reporting__c> srList = new List<Self_Reporting__c> ();
        List<Report_Summary__c> repSummList = new List<Report_Summary__c> ();
			repSummList = [Select id,Authorization__c From Report_Summary__c Where Authorization__c =: authorizationId AND Report_Type__c=: reportType];
        if(!repSummList.isEmpty()){
        	srList = EFLSelfReportingRepository.selfRepListByRSIdandRSType(repSummList, reportType);
        }
     return srList;
    }
     //W-027501 Vinod Adusumilli- Authorization Detail Section Selfreport summary page- Required Reports field // Pass the Authorization Type,Status, movement,purpose to retrieve the required reports 
    Public static string getReqreports(String Type,String Status,String MovementType,String PurposeofPermit){
       Required_Report_SelfReporting__mdt rReport;
        try{
       rReport=  [select Type__c,Status__c,MovementType__c,Required_Reports__c from Required_Report_SelfReporting__mdt where Type__c =:Type and Status__c =:Status and MovementType__c =:MovementType and Purpose_of_Permit__c =:PurposeofPermit];
        }catch(Exception ex){
            rReport = null;
        }
        if(rReport != null){
        return rReport.Required_Reports__c;
            }else
            {
                return '';
            }
    }// end W-027501
}