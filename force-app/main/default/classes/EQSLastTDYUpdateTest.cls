/************************************************
Description: Test Class for EQS_UpdateLastTDY trigger. 
Sample method to run specified run test classes.
**************************************************/
@isTest 
private class EQSLastTDYUpdateTest {
    static testMethod void validateEQSLastTDYUpdate() {    
    Contact c = new Contact(FirstName='EQS Contact', LastName='Test');
    insert c;
    EQS_Resource_Request__c er = new EQS_Resource_Request__c (EQS_Responder__c=c.Id, EQS_Rotation_Begin_Date__c=System.TODAY());
    insert er;
    }
}