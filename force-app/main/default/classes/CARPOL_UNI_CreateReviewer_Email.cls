/* The purpose of this code is to create official review records for BRS and PPQ (prefix 203, 205 and 206).                         */
/* official review records are created, where needed permits are attached, emails are sent out and apex managed sharing is applied */
//ToDo list:
/* 1.Checking for different Attributes to get State ID's # MetaData 
2.Inserting State Records 
3.Sharing State Records with Different State Public Groups 
4.Preparing State Package with Attachments using EFLOfficialReviewerPackages__mdt */
public with sharing class CARPOL_UNI_CreateReviewer_Email {
    
    private ID AuthorizationID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('ID'));
    public string officialRole {get;set;}
    public string wfname {get;set;}
    public string sproid  {get;set;}
    public string sphdid  {get;set;}
    Id BRSsproreviewerRecordTypeId;
    Id PPQsproreviewerRecordTypeId;
    Id sphdreviewerRecordTypeId;
    Reviewer__c objrev = new Reviewer__c();
    public list<Reviewer__c> listReviewRec {get;set;}
    public list<Link_Regulated_Articles__c> listRegArt {get; set;} 
    public id wfid {get;set;}
    public boolean showAttachDraftBtn{get;set;}     
    public boolean showAttachBtn{get;set;}      
    public boolean showtemplateBtn{get;set;}
    public boolean showLinks{get;set;}
    public String templateURL { get; set; }
    public String baseURL { get; set; }
    public Boolean brsview {get;set;}
    public string delrecid {get;set;}
    public Authorizations__c auth {get;set;}
    public string lineitemid {get;set;} 
    public String docLauncherFullURL {get;set;}
    public String docLauncherFullURLCBIDeleted {get;set;}
    public ApexPages.StandardController standardController {get; set;}
    Program_Prefix__c progPrefix1;
    boolean iscreaterev =  false;
    
    
    public CARPOL_UNI_CreateReviewer_Email(ApexPages.StandardController controller) {
        // JB: Commenting out param method of getting Id & using StandardController.
        // this.AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
        if(controller != null){
        	this.AuthorizationID = controller.getId();
        }
        standardController = controller;
        // JB: End of change.
        if(AuthorizationID !=null){
            auth = [SELECT Name,Program__c,BRS_Create_State_Review_Records__c,Create_SPHD_Review_Records__c,Prefix__c,Date_Issued__c,OwnerId,
                    State_letter_Template__c,Authorization_State_Letter_Content__c, Application_CBI__c, 
                    Id, Status__c, Recordtype.Name, BRS_Introduction_Type__c, Application_Type__c,
                    High_Priority_Flag__c, Reason_to_Expedite__c
                    FROM Authorizations__c 
                    WHERE Id =: AuthorizationID];
            lineitemid = [select id,name from ac__c where Authorization__c =:AuthorizationID limit 1 ].id;  
            progPrefix1 = [SELECT Id,name,Program__r.Name,Permit_PDF_Template__c From Program_Prefix__c where Name = :auth.Prefix__c Limit 1];
            
        }
        
        officialRole = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('rtype'));
        if(ApexPages.currentPage().getParameters().get('wfid') != null){
            wfid = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('wfid'));
            wfname = [select name from Workflow_Task__c where id =:wfid ].name;
        } 
        this.sproid = 'spro';
        this.sphdid = 'sphd';
        brsview = false;
        BRSsproreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('BRS State Review Record').getRecordTypeId();
        PPQsproreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('PPQ State Review Record').getRecordTypeId();
        sphdreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('State Plant Health Director Record').getRecordTypeId();  
        if(auth.Program__c == 'BRS'){ 
            brsview = true;}
        getlstReviewRec();
        getlstsphdReviewRec();
        GenerateDocLauncherURLs();
    }
    
    list<Reviewer__c> finalreviewerlist = new list<Reviewer__c>(); 
    
    public PageReference SendNotification(){
        
        if(AuthorizationID != null)
        {
            
            if(lstReviewRec1.size()>0)
            {
                for(Reviewer__c rev : lstReviewRec1){
                    finalreviewerlist.add(rev);
                }
            }
            
            if(lstsphdReviewRec1.size()>0)
            {
                for(Reviewer__c rev : lstsphdReviewRec1){
                    finalreviewerlist.add(rev); 
                }
            }                 
            Program_Prefix__c progPrefix = [SELECT Id,Program__r.Name From Program_Prefix__c where Name = :auth.Prefix__c Limit 1];
            SendNotificationToReviewer(progPrefix.Program__r.Name,auth,finalreviewerlist);  
        }
        return null;
        
    }
    
    public void SendNotificationToReviewer(String progprefix, Authorizations__c auth, List<Reviewer__c> reviewer){
        
        map<string,id> mapSPROstateids = new map<string,id>();
        //need to get the attachment for ppq differently to pass below
        for(Reviewer__c review : reviewer){
            mapSPROstateids.put(review.State_Regulatory_Official__r.State__r.SPRO_Chatter_Group_Name__c,review.State_Regulatory_Official__r.State__c);
        }
        
        //prepare email message
        string sBody='' ; 
        string sSubject='' ;
        string templateName='';
        Boolean emailnotsent;
        CollaborationGroup[] StateChattergroup = [
            select Id,Name
            from CollaborationGroup
            where Name IN :mapSPROstateids.keyset()
        ];
        
        map<string,id> StateChattergroupIdMap = new map<String,ID>();
        
        for(CollaborationGroup cg:StateChattergroup){
            StateChattergroupIdMap.put(cg.Name,cg.Id);
        } 
        
        Set<Id> RevIds = new Set<Id>();
        for(Reviewer__c rev:reviewer){
            RevIds.add(rev.Id);
        } 
        
        for(Reviewer__c rev1  : reviewer ) {
            
            //------------------------------Chatter message to State Chatter Groups--------------------------//
            if (StateChattergroup.size() > 0) {
                
                Id cgId =  StateChattergroupIdMap.get(rev1.State_Regulatory_Official__r.State__r.SPRO_Chatter_Group_Name__c);
                
                String sText = '{'+cgId+'}' +' Please review the authorization that involves your state.';
                
                ConnectApi.FeedItem fi = (ConnectApi.FeedItem)ConnectApiHelper.postFeedItemWithMentions(null,rev1.id, sText);
            }  
        }
        if(emailnotsent != true){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Sent Chatter Notification to State Chatter Groups'));
            
            Task tsk = new Task();
            tsk.WhatId = auth.id;
            tsk.Status = 'Completed';
            tsk.Subject = 'Sent Chatter notifications to each State Chatter Groups';
            insert tsk;
        }
        
    } 
    
    public  string createReviewerrecords()
    {
        list<string> stateids = new list<string>();
        list<Reviewer__c> lstrev = new list<Reviewer__c>();
        if(AuthorizationID != null)
        {   //instantiate lists
            list<Reviewer__c> lstrev2 = new list<Reviewer__c>();
            list<AC__c> lstlineitem = new list<AC__c>();
            list<Location__c> loctlist = new list<Location__c>();
            boolean chkCreated;
            Program_Prefix__c progPrefix = [SELECT Id,Program__r.Name From Program_Prefix__c where Name = :auth.Prefix__c Limit 1];
            Authorizations__c authToUpdate = (Authorizations__c)standardController.getRecord();
            System.debug('authToUpdate: ' + authToUpdate);
            if(authToUpdate.High_Priority_flag__c && String.isBlank(authToUpdate.Reason_to_Expedite__c)){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Add a reason to expedite before saving the record.'));
                    return null;
            }else {
                auth.High_Priority_flag__c = authToUpdate.High_Priority_flag__c;
                auth.Reason_to_Expedite__c = authToUpdate.Reason_to_Expedite__c;
            }
            
            if(officialRole == 'spro'){
                chkCreated = auth.BRS_Create_State_Review_Records__c;
            } else if (officialRole == 'sphd'){
                chkCreated = auth.Create_SPHD_Review_Records__c;
            }
            if(chkCreated == false || chkCreated == true)
            {
                //get line item, for PPQ will need State/Territory of Destination
                integer countlineitem = [select count() from AC__c where Authorization__c=:AuthorizationID];
                if(countlineitem>0){
                    lstlineitem = [select id,Name,Authorization__c,Introduction_Type__c,Purpose_of_the_Importation__c,Hand_Carry__c,Number_of_Labels__c,State_Territory_of_Destination__c, Delivery_Recipient_State_ProvinceLU__c,Movement_Type__c,Growing_Location_State__c from AC__c where Authorization__c=:AuthorizationID] ;
                }
                //if BRS, we need to get the locations( except Origin Record type ) to get state ids
                if(progPrefix.Program__r.Name == 'BRS'){
                    integer loctcount;
                    if (auth.Application_Type__c =='Amendment'){
                        loctcount = [select count() from Location__c  where Line_Item__c =:lstlineitem[0].Id AND Loc_Ext_ID__c=null];  
                    }
                    Else{
                        loctcount = [select count() from Location__c  where Line_Item__c =:lstlineitem[0].Id];
                    }
                    
                    
                    if(loctcount>0)
                    {
                        Id recTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
                        list<Location__c> loctlst;
                        if (auth.Application_Type__c =='Amendment'){
                            loctlst = [select id,Name,State__c,State__r.Name,Authorization__c from Location__c where Line_Item__c =:lstlineitem[0].Id AND RecordTypeID != :recTypeId AND Loc_Ext_ID__c=null];
                        }
                        Else{
                            loctlst = [select id,Name,State__c,State__r.Name,Authorization__c from Location__c where Line_Item__c =:lstlineitem[0].Id AND RecordTypeID != :recTypeId];
                        }
                        
                        if(loctlst.size()>0){
                            for(Location__c l:loctlst){
                                if(l.State__c!=null){
                                    stateids.add(l.State__c);
                                }
                            }
                        }
                    }
                }
                
                if(stateids==null || stateids.isEmpty()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'No States exist for the line item.'));
                    return null;
                }                    
                //we got our state, create the review records
                if(stateids.size()>0){
                    //KA: W-035458:Cannot create State package for an authorization
                    //get the state SPRO or SPHD contacts depending on User Type Picklist value
                    integer contactcount;
                    if(officialRole == 'spro'){
                        contactcount = [select count() from contact where Contact_checkbox__c=:true and User_Type__c = 'SPRO'];
                    }else if(officialRole == 'sphd'){
                        contactcount = [select count() from contact where Contact_checkbox__c=:true and User_Type__c ='SPHD'];
                    }   
                    if(contactcount>0){
                        list<contact> contactlst = new list<contact>();
                        if(officialRole == 'spro'){
                            contactlst = [select id,Name,Contact_checkbox__c,State__c,Email from contact where Contact_checkbox__c=:true and User_Type__c = 'SPRO' and State__c in:stateids ];
                        }else if(officialRole == 'sphd'){
                            contactlst = [select id,Name,Contact_checkbox__c,State__c,Email from contact where Contact_checkbox__c=:true and User_Type__c = 'SPHD' and State__c in:stateids ];
                        }
                        for(Contact c:contactlst)
                        {   
                            objrev = new Reviewer__c();
                            //get the correct record type
                            if(progPrefix.Program__r.Name == 'BRS'){
                                objrev.RecordTypeId = BRSsproreviewerRecordTypeId;
                            } else if(progPrefix.Program__r.Name == 'PPQ' && officialRole == 'spro'){
                                objrev.RecordTypeId = PPQsproreviewerRecordTypeId;
                            } else if(progPrefix.Program__r.Name == 'PPQ' && officialRole == 'sphd'){                             
                                objrev.RecordTypeId = sphdreviewerRecordTypeId;
                            }
                            
                            objrev.Status__c= 'Open';
                            objrev.Authorization__c = AuthorizationID;
                            //This field changes for sphd
                            if(officialRole == 'spro'){
                                objrev.State_Regulatory_Official__c = c.id;
                            } else if(officialRole == 'sphd'){
                                objrev.State_Plant_Health_Director__c = c.id;                              
                            }
                            objrev.BRS_State_Reviewer_Email__c = c.Email;
                            lstrev.add(objrev);
                        }
                        if(stateids!=null && !stateids.isEmpty() && (lstrev==null || lstrev.isEmpty())){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'State reviewers are not configured for the selected states.'));
                            return null;
                        }
                        
                        if(lstrev.size()>0){
                            insert lstrev;
                            
                            //add page message
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Successfully created Official State Review records'));

                            // Updated 5/14/2019 by JB.  Moving SOQL outof for-loop
                            map<Id, Contact> contactMap = new map<Id,Contact>();
                            set<Id> cIds = new set<Id>();
                            for(Reviewer__c objrev : lstrev){
                                if(objRev.State_Regulatory_Official__c != null){
                                    cIds.add(objRev.State_Regulatory_Official__c);
                                }
                                if(objRev.State_Plant_Health_Director__c != null){
                                    cIds.add(objRev.State_Plant_Health_Director__c);
                                }
                            }
                            if(!cIds.isEmpty()){
                                contactMap = new map<Id, Contact>([Select ID,Name,State__c,Mailing_State_Province_LR__c,State__r.Level_1_Region_Code__c 
                                                 From Contact where Id IN :cIds]);
                            }
                            
                            for(Reviewer__c objrev : lstrev){

                                //set row cause based on who is sharing State_Plant_Health_Director__c or State_Reviewer__c 
                                if(officialRole == 'spro'){
                                    // JB: removing SOQL & replacing with Map.
                                    if(contactMap.containsKey(objrev.State_Regulatory_Official__c)){
                                        Contact c = contactMap.get(objrev.State_Regulatory_Official__c);
                                    }
                                } else if(officialRole == 'sphd'){
                                    // JB: removing SOQL & replacing with Map.
                                    if(contactMap.containsKey(objrev.State_Plant_Health_Director__c)){
                                        Contact c = contactMap.get(objrev.State_Plant_Health_Director__c);                                                  
                                    }
                                }
                            }
                            //save the flag to the authorization
                            //this field changes if sphd
                            if(officialRole == 'spro'){
                                auth.BRS_Create_State_Review_Records__c = true;
                                auth.Status__c='State Review';
                            } else if(officialRole == 'sphd'){
                                auth.Create_SPHD_Review_Records__c = true;                          
                                auth.Status__c = 'State Plant Health Director Review';
                            } 
                            System.debug('Updating auth: ' + auth);
                            update auth;                                     
                        }
                    }
                    else
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning, 'No Offical State Review Records created'));
                    }                            
                } //end stateids size
                
            } 
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,'Official State Review Records are already Created.'));
            } 
            /*-----------------------------END-----------------------------------*/
        }
        
        return null;
    }
    
    public pageReference createstatepackage(){
        pageReference pg = Page.CARPOL_UNI_CreateOfficialReviewerRecords;
        pg.getParameters().put('rtype','spro');
        pg.getParameters().put('ID',AuthorizationID);
        pg.setRedirect(true);
        return pg;
    }    
    
    List<Reviewer__c> lstReviewRec1 =new List<Reviewer__c>(); 
    
    public List<Reviewer__c> getlstReviewRec()
    {
        
        for(List<Reviewer__c> rev:[SELECT Id, Name,Notes_to_State__c,BRS_State_Reviewer_Email__c,BRS_State_Reviewer_Notification__c,Requirement_Desc__c,State_Comments__c,State_Regulatory_Official__r.State__r.SPRO_Chatter_Group_Name__c,CreatedDate,
                                   State_not_Responded__c,State_has_comments__c,No_Requirements__c,State_Regulatory_Official__r.Name,State_Regulatory_Official__r.FirstName,Review_Complete__c,Reviewer__c,Authorization__c,
                                   Status__c,Reviewer_Unique_Id__c,State__c,State_Review_Duration__c,LastModifiedDate,(SELECT ID, Name FROM Attachments)  FROM Reviewer__c WHERE Authorization__c =:AuthorizationID and (RecordTypeId =: BRSsproreviewerRecordTypeId or RecordTypeId =: PPQsproreviewerRecordTypeId) order by Name DESC])
        {
            lstReviewRec1=rev;
        }
        return lstReviewRec1;
    }  
    
    List<Reviewer__c> lstsphdReviewRec1 =new List<Reviewer__c>(); 
    
    public List<Reviewer__c> getlstsphdReviewRec()
    {
        
        for(List<Reviewer__c> rev:[SELECT Id, Name,Notes_to_State__c,BRS_State_Reviewer_Email__c,BRS_State_Reviewer_Notification__c,Requirement_Desc__c,State_Comments__c,State_Plant_Health_Director__c,State_Plant_Health_Director__r.name,State_Regulatory_Official__r.State__r.SPRO_Chatter_Group_Name__c,State_Plant_Health_Director__r.State__r.SPHD_Chatter_Group_Name__c,
                                   State_not_Responded__c,State_has_comments__c,No_Requirements__c,State_Regulatory_Official__r.FirstName,Review_Complete__c,Reviewer__c,CreatedDate,
                                   Status__c,Reviewer_Unique_Id__c,State__c,State_Review_Duration__c,(SELECT ID, Name FROM Attachments )  FROM Reviewer__c WHERE Authorization__c =:AuthorizationID and RecordTypeId =: sphdreviewerRecordTypeId order by Name DESC])
        {
            lstsphdReviewRec1=rev;
        }
        return lstsphdReviewRec1;
    }    
    
    
    public PageReference cancel()
    {
        PageReference return2Auth = new PageReference('/' + AuthorizationID);
        return2Auth.setRedirect(true);
        return return2Auth;
    } 
    
    public PageReference deleteRecord(){
        string  strqurey = 'select id from Attachment where id=:delrecid';
        list<sobject> lst = database.query(strqurey);
        delete lst;
        PageReference dirpage= new PageReference('/apex/CARPOL_UNI_CreateOfficialReviewerRecords?id=' + authorizationID+'&wfid='+wfid);
        dirpage.setRedirect(true);
        return dirpage;
    }  
    
    /*************************************************************************************** 
************************************  Delete any Reviewer record **************************   
**********************************************************************************************/    
    public PageReference deleteRec(){
        string strqurey='select id from Reviewer__c where id=:delrecid';
        list<sobject> lst=database.query(strqurey);
        Delete lst;
        PageReference dirpage=new  PageReference('/apex/CARPOL_UNI_CreateOfficialReviewerRecords?id='+authorizationID);
        dirpage.setRedirect(true);
        return dirpage;
    }                
    
    public PageReference redirect(){
        PageReference dirpage;
        if(wfid!=null){
            dirpage= new PageReference('/'+wfid);
        }else {
            dirpage= new PageReference('/'+authorizationID); 
        }
        dirpage.setRedirect(true);
        return dirpage;
    }  
    
    public void updatestaterecords(){
        
        update lstReviewRec1;
    } 
    
    public void updatesphdrecords(){
        
        update lstsphdReviewRec1;
    }      

    public PageReference viewDraftPDF() { 
        String docLauncherURL = 'Doc Launcher URL';
        String scmPath = 'Sandbox';
        //String docLauncherFullURL = '';
        final String workFlowCBI = 'CBI Permit Draft';
        final String workFlowNoCBI = 'No CBI Permit Draft';
        SpringCM_Components__mdt[] springCMComonents =  [SELECT MasterLabel, Component_Value__c FROM SpringCM_Components__mdt];
        for(SpringCM_Components__mdt scmComp:springCMComonents){
            if(scmComp.MasterLabel == docLauncherURL){
                docLauncherURL = scmComp.Component_Value__c;
            } 
            else if(scmComp.MasterLabel == scmPath){
                scmPath = '/'+scmComp.Component_Value__c+'/Authorizations';
            }
        }
        PageReference pageRef;
        if(auth.Program__c == 'BRS'){
            if(auth.Application_CBI__c == 'Yes'){
                docLauncherFullURL = docLauncherURL + workFlowCBI + 
                    '?aid=9665&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
            else{
                docLauncherFullURL = docLauncherURL + workFlowNoCBI + 
                    '?aid=9665&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
            pageRef = new PageReference(docLauncherFullURL);
            pageRef.setRedirect(false);
            return pageRef;  
        }else if(auth.Program__c == 'PPQ' && progPrefix1.Permit_PDF_Template__c!=null){ 
            pageRef = new PageReference('/apex/'+progPrefix1.Permit_PDF_Template__c);
            pageRef.getParameters().put('Id',authorizationID);
            //pageRef.setRedirect(true);
            return pageRef;  
        }else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'PDF configuration is missing for the prefix '+progPrefix1.Name));
            return null;  
        }
        
        
    } 
    
    public void GenerateDocLauncherURLs() { 
        String docLauncherURL = 'Doc Launcher URL';
        String scmPath = 'Sandbox';
        String aid = 'aid'; 
        //String docLauncherFullURL = '';
        final String workFlowCBI = 'CBI Permit Draft';
        final String workFlowNoCBI = 'No CBI Permit Draft';
        final String workFlowCBIDeleted = 'CBI Deleted Permit Draft';
        SpringCM_Components__mdt[] springCMComonents =  [SELECT MasterLabel, Component_Value__c FROM SpringCM_Components__mdt];
        for(SpringCM_Components__mdt scmComp:springCMComonents){
            if(scmComp.MasterLabel == docLauncherURL){
                docLauncherURL = scmComp.Component_Value__c;
            } 
            else if(scmComp.MasterLabel == scmPath){
                scmPath = '/'+scmComp.Component_Value__c+'/Authorizations';
            }
            else if(scmComp.MasterLabel == aid){
                aid = scmComp.Component_Value__c;
            }
        }
        if(auth.Program__c == 'BRS'){
            if(auth.Application_CBI__c == 'Yes'){
                docLauncherFullURL = docLauncherURL + workFlowCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
                
                docLauncherFullURLCBIDeleted = docLauncherURL + workFlowCBIDeleted + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath; 
                
            }
            else{
                docLauncherFullURL = docLauncherURL + workFlowNoCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
        }else if(auth.Program__c == 'PPQ' && progPrefix1.Permit_PDF_Template__c!=null){ 
            docLauncherFullURL = '/apex/'+progPrefix1.Permit_PDF_Template__c + '?Id='+authorizationID;
        }else {
            docLauncherFullURL = 'PDF configuration is missing for the prefix '+progPrefix1.Name;
        }
    } 
    
    
}