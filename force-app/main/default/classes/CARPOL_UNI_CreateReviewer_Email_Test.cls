@isTest(seealldata=true)
//
public class CARPOL_UNI_CreateReviewer_Email_Test{
    static testmethod void updateauthcreaterecords(){
        User thisUser=[SELECT Id FROM User WHERE Id=:UserInfo.getUserId()];
        System.runAs(thisUser){
            CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
            Application__c objapp=testData.newapplication();
            Authorizations__c objauth=testData.newauth(objapp.id);
            //string programId = [select id from Program__c where name='PPQ'].id;
            //string progPrefixId = [select id from Prefix__c where program__c = programId limit 1];
            //objauth.Prefix__c = progPrefixId; 
            objauth.BRS_Introduction_Type__c = 'Import';
            update objauth;
            Trade_Agreement__c ta=new  Trade_Agreement__c();
            ta.name='test';
            ta.Trade_Agreement_code__c='12';
            Insert ta;
            country__c ct=new  country__c();
            ct.Name='United States of America';
            ct.country_code__c='12';
            ct.Trade_Agreement__c=ta.Id;
            Insert ct;
            Level_1_Region__c lr=new  Level_1_Region__c();
            lr.Name='Texas';
            lr.Level_1_Name__c = 'State';
            lr.country__c=ct.Id;
            Insert lr;
            level_2_Region__c l2=new  level_2_Region__c();
            l2.Name='Test';
            l2.level_1_Region__c=lr.Id;

            
            AC__c li=testData.newlineitem('Personal Use',objapp);
            
            
            
            string stateid = [select id from level_1_region__c where Name='Texas'].id;
            li.Authorization__c=objauth.id;
            li.movement_type__c = 'Transit';
            li.Delivery_Recipient_State_ProvinceLU__c = lr.id;
            li.Growing_Location_State__c = lr.id;
            li.Country__c = ct.id;
            li.State_Territory_of_Destination__c = stateid;
            Update li;
            
            Location__c objloc=new  Location__c(Name='testloc',Country__c=ct.id,state__C=lr.id,Level_2_Region__c=l2.id,GPS_1__c='GPS_1__c',GPS_2__c='GPS_2__c',GPS_3__c='GPS_3__c',GPS_4__c='GPS_4__c',GPS_5__c='GPS_5__c',GPS_6__c='GPS_6__c',Contact_Name1__c='Test',Primary_Contact_Last_Name__c='LName',Day_Phone__c='(555) 555-1212',Line_Item__c=li.id);
            objLoc.recordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Incident Location').getRecordTypeId();
            Insert objloc;
            
            
            facility__c  fac = new facility__c ();
            //fac.Id = transloc.id;
            fac.Country__c = ct.id;
            fac.State_LV1__c = lr.id;
            Insert fac;
            
            facility__c  fac1 = new facility__c ();
            //fac.Id = transloc.id;
            fac1.Country__c = ct.id;
            fac1.State_LV1__c = lr.id;
            Insert fac1;
            
            transit_locale__c transloc = new transit_locale__c();
            //transloc.Name = 'TL-000039';
            transloc.Line_Item__c = li.id;
            transloc.Port_of_Entry__c = fac.id;
            transloc.Port_of_Exit__c  = fac1.id;
            transloc.EFLArrival_Date__c = System.today();
            transloc.Date_of_Departure__c = System.today();
            Insert transloc; 
            
            test.startTest();
            
            Domain__c objProgPPQ = testData.newProgram('PPQ');
            Program_Prefix__c objPrefix = testData.newPrefix();
            Program_Prefix__c objPrefixPPQ = objPrefix.clone(false,false,false,false);
            objPrefixPPQ.Program__c = objProgPPQ.id;
            objPrefixPPQ.name='PPQ';
            objprefixPPQ.Permit_PDF_Template__c = '';
            Insert objPrefixPPQ;
            
            Domain__c objProgBRS = testData.newProgram('BRS');
            Program_Prefix__c objPrefix1 = testData.newPrefix();
            Program_Prefix__c objPrefixBRS = objPrefix.clone(false,false,false,false);
            objPrefixBRS.Program__c = objProgBRS.id;
            objPrefixBRS.name='BRS';
            Insert objPrefixBRS;
            
            signature__c TP=testData.newThumbprint();
            TP.Program_Prefix__c = objPrefixPPQ.id;
            Update TP;
            objauth.Thumbprint__c=TP.id;
            Update objauth;
            
            
            signature__c TP1=testData.newThumbprint();
            TP1.Program_Prefix__c = objPrefixBRS.id;
            Update TP1;
            objauth.Thumbprint__c=TP1.id;
            Update objauth;
            
            Account a=new  Account();
            a.Name='Test Account Bingo';
            Insert a;
            
            // RecordType cRT=[SELECT Id,Name,DeveloperName FROM RecordType WHERE DeveloperName='APHIS Efile Standard Contact' LIMIT 1];
            //  RecordType sproRT=[SELECT Id,Name,DeveloperName FROM RecordType WHERE Name='State SPRO' LIMIT 1];
            // RecordType sphdRT=[SELECT Id,Name,DeveloperName FROM RecordType WHERE Name='State SPHD' LIMIT 1];
            Contact c=new  Contact();
            c.FirstName='Global Contact'+'123';
            c.LastName='LastName'+'123';
            c.Email='123test@email.com';
            c.MailingStreet='Mailing Street'+'123';
            c.MailingCity='Mailing City'+'123';
            c.MailingState='Ohio';
            c.MailingCountry='United States';
            c.MailingPostalCode='32092';
            c.Phone='12345';
            // c.RecordTypeId=cRT.Id;
            c.AccountId=a.Id;
            c.Contact_checkbox__c = TRUE;
            c.State__c = stateid;
            //c.User_Type__c = 'SPRO';
            Insert c;
            
            
            
            
            Contact spro=new  Contact();
            spro.FirstName='Global Contact'+'123';
            spro.LastName='LastName'+'123';
            spro.Email='123test@email.com';
            spro.MailingStreet='Mailing Street'+'123';
            spro.MailingCity='Mailing City'+'123';
            spro.MailingState='Ohio';
            spro.MailingCountry='United States';
            spro.MailingPostalCode='32092';
            spro.Phone='12345';
            // spro.RecordTypeId=cRT.Id;
            spro.AccountId=a.Id;
            spro.Contact_checkbox__c = TRUE;
            spro.state__C = lr.id;
            spro.User_Type__c = 'SPRO';
            Insert spro;
            
            /*Contact sphd=new  Contact();
            sphd.FirstName='Global Contact'+'123';
            sphd.LastName='LastName'+'123';
            sphd.Email='123test@email.com';
            sphd.MailingStreet='Mailing Street'+'123';
            sphd.MailingCity='Mailing City'+'123';
            sphd.MailingState='Ohio';
            sphd.MailingCountry='United States';
            sphd.MailingPostalCode='32092';
            sphd.Phone='12345';
            // sphd.RecordTypeId=cRT.Id;
            sphd.AccountId=a.Id;
            sphd.Contact_checkbox__c = TRUE;
            sphd.state__C = lr.id;
            Insert sphd;*/
            
            // FIXME: commenting out code that is causing high # of SOQL
            // 
           Reviewer__c r=new  Reviewer__c();
            r.State_Regulatory_Official__c=c.Id;
            r.BRS_State_Reviewer_Email__c='brs@gmail.com';
            r.Authorization__c=objauth.id;//auth.id;
            r.State_Regulatory_Official__c=spro.Id;
            r.Status__c = 'Open';
            Insert r;
            /* 
            List<Reviewer__c> rList=new  List<Reviewer__c>();
            rList.add(r);   */         
            
            Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Pending');
            
            Attachment attach=new Attachment();
            Blob attachmentBody = Blob.valueOf('Some Text');  
            String attachName;
            attach.Body = attachmentBody;
            attach.Name = attachName+ System.TODAY().format()+ '.pdf';
            attach.contentType = 'application/pdf';
            attach.ParentId = r.id; 
            Insert attach; 
            
            attachment att = new attachment();
            Blob attbody= Blob.valueOf('Some Text');
            att.body = attbody;
            att.Name = 'CBI_Deleted_Draft_Permit_'+ System.TODAY().format()+ '.pdf'; 
            att.contentType = 'application/pdf';
            att.ParentId = r.Id;
            Insert att ;
            
            List<Attachment> CBIDelattach = new List<Attachment>();
            CBIDelattach.add(att);  
            
            
            
            PageReference pg=Page.CARPOL_UNI_CreateOfficialReviewerRecords;
            pg.getParameters().put('Id',objauth.Id);
            pg.getParameters().put('rtype','spro');
            pg.getParameters().put('wfid',wft.id);
            Test.setCurrentPage(pg);
            
            ApexPages.StandardController std =new  ApexPages.StandardController(objauth);
            CARPOL_UNI_CreateReviewer_Email core=new  CARPOL_UNI_CreateReviewer_Email(std);
            string isCBI = '';
            core.createReviewerrecords();
            core.createstatepackage();
            core.updatesphdrecords();
            core.cancel();
            core.createstatepackage();
            core.SendNotification();
            core.deleteRecord();
            core.deleteRec();
            core.redirect();
            core.viewDraftPDF(); 

            // FIXME: the next 3 blocks should be put into separate test methods to avoid having too much in one test.
// 
            /*            
            // PageReference pg1=Page.CARPOL_BRS_CreateOfficialReviewerRecords;
            system.currentPageReference().getParameters().put('Id',objauth.Id);
            ApexPages.StandardController std1=new  ApexPages.StandardController(objauth);
            CARPOL_UNI_CreateReviewer_Email core1=new  CARPOL_UNI_CreateReviewer_Email(std1);
            core1.createReviewerrecords();            
            system.assert(core1!=null);
            
            
            li.movement_type__c = 'Transit';
            update li;
            
            objPrefixPPQ.name = 'PPQ';
            objprefixPPQ.Permit_PDF_Template__c = '';
            update objPrefixPPQ;
            
            PageReference pg2=Page.CARPOL_UNI_CreateOfficialReviewerRecords;
            pg2.getParameters().put('ID',objauth.Id);
            pg2.getParameters().put('rtype','sphd');
            Test.setCurrentPage(pg2);
 */           
/*            
            ApexPages.StandardController std2=new  ApexPages.StandardController(objauth);
            CARPOL_UNI_Createreviewer_email core3=new  CARPOL_UNI_Createreviewer_email(std2);
            //core3.SendEmailToReviewer('PPQ',objauth,rlist);
            core3.createstatepackage();
            core3.createReviewerrecords();
            //core3.Attachstateletter();
            //core3.populateTemplate();
            core3.cancel();
            core3.createstatepackage();
            core3.updatesphdrecords();
            //core3.Attachstateletter();
            //core3.saveDraft();
            //core3.previewTemplate();
            core3.SendNotification();
            core3.redirect();
            core3.viewDraftPDF();            
            core3.showAttachDraftBtn = true;     
            core3.showAttachBtn = true;      
            core3.showtemplateBtn = true;
            core3.showLinks = true;
            core3.templateURL ='';
            core3.baseURL ='';
            core3.officialRole = 'spro';
            
            //string isCBI = '';
            
            objPrefixPPQ.name = 'BRS';
            objProgPPQ.name = 'BRS';
            update objProgPPQ;
            update objPrefixPPQ;
            
            PageReference pg3=Page.CARPOL_UNI_CreateOfficialReviewerRecords;
            pg3.getParameters().put('ID',objauth.Id);
            pg3.getParameters().put('rtype','sphd');
            Test.setCurrentPage(pg3);
*/            
            
            /*
            ApexPages.StandardController std3=new  ApexPages.StandardController(objauth);
            CARPOL_UNI_Createreviewer_email core4=new  CARPOL_UNI_Createreviewer_email(std3);
            //core3.SendEmailToReviewer('PPQ',objauth,rlist);
            core4.createstatepackage();
            core4.createReviewerrecords();
            //core3.Attachstateletter();
            //core3.populateTemplate();
            core4.cancel();
            core4.createstatepackage();
            //core3.Attachstateletter();
            //core3.saveDraft();
            //core3.previewTemplate();
            
            
            //core4.SendNotification();
            //core4.redirect();
            core4.updatesphdrecords();
            core.updatestaterecords();
            core4.viewDraftPDF();
            core4.officialRole = 'sphd';
            objPrefixBRS.name = 'BRS';
            objProgBRS.name = 'BRS';
            update objProgBRS;
            update objPrefixBRS;
*/
            test.stopTest();
        }
    }
}