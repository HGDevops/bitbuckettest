public inherited sharing class EFLPSQUtility {
    
    public static Wizard_Questionnaire__c getNextQuestionByActivityResult(ID currentQuestionID, string MovementType, ID Pathway, String ActivityResult)
    {
        Wizard_Questionnaire__c nextQuestion;
        Wizard_Selections__c selectedOptionResult  = [SELECT Wizard_Next_Question__c 
                                                      FROM Wizard_Selections__c 
                                                      WHERE Wizard_Questionnaire__c = :currentQuestionID 
                                                      AND Program_Pathway__c = :Pathway 
                                                      AND Movement_Type__c in ( :MovementType ) 
                                                      AND Activity_Processor_Result__c = :ActivityResult 
                                                      LIMIT 1];
        
        if(selectedOptionResult != null){
            nextQuestion = getQuestionDetails(selectedOptionResult.Wizard_Next_Question__c);
        }
        
        return nextQuestion;
    }
    
    public static Wizard_Questionnaire__c getNextQuestionByValue(ID currentQuestionID, string MovementType, string Pathway, String Value)
    {
        string query;
        Wizard_Questionnaire__c nextQuestion;
        Wizard_Selections__c selectedOptionResult  = [SELECT Wizard_Next_Question__c 
                                                      FROM Wizard_Selections__c 
                                                      WHERE Wizard_Questionnaire__c = :currentQuestionID 
                                                      AND Program_Pathway__c = :Pathway 
                                                      AND Movement_Type__c in ( :MovementType ) 
                                                      AND Value__c =: Value
                                                      LIMIT 1];
        
        if(selectedOptionResult != null){
            nextQuestion = getQuestionDetails(selectedOptionResult.Wizard_Next_Question__c);
        }
        return nextQuestion;
    }
    
    public static List<Wizard_Selections__c> getQuestionOptions(ID currentQuestionID, string movementType , string Pathway)
    {
        list<Wizard_Selections__c> optionsValueList = new list<Wizard_Selections__c>();
        list<Wizard_Selections__c> temp;
        //system.debug('currentQuestionID: ' + currentQuestionID);
        //system.debug('movementType: ' + movementType);
        //system.debug('Pathway: ' + Pathway);
        string query,query1,query2;
        string blank = ' ';
        
        
        query = 'SELECT Name,Value__c,Activity_Processor_Result__c,Wizard_Next_Question__c,Program_Pathway__c  FROM Wizard_Selections__c WHERE Wizard_Questionnaire__c = :currentQuestionID  AND Movement_Type__c = :blank AND Program_Pathway__c = null order by Display_Order__c ';
        //system.debug('query: '+ query);
        temp = Database.query(query);
        //system.debug('### temp 0 : ' + temp);
        if(temp != null) optionsValueList.addAll( temp );
        
        if(String.isNotBlank(movementType))
        {
            query1 = 'SELECT Name,Value__c,Activity_Processor_Result__c,Wizard_Next_Question__c,Program_Pathway__c  FROM Wizard_Selections__c WHERE Wizard_Questionnaire__c = :currentQuestionID  AND Movement_Type__c in ( :movementType ) AND Program_Pathway__c = null order by Display_Order__c';
            temp = Database.query(query1);
            //system.debug('### temp 1 : ' + temp);
            if(temp != null) optionsValueList.addAll( temp );
            
        }
        if(String.isNotBlank(Pathway))
        {
            query2 = 'SELECT Name,Value__c,Activity_Processor_Result__c,Wizard_Next_Question__c,Program_Pathway__c  FROM Wizard_Selections__c WHERE Wizard_Questionnaire__c = :currentQuestionID AND Program_Pathway__c = :Pathway AND Movement_Type__c in ( :movementType ) order by Display_Order__c';
            temp = Database.query(query2);
            //system.debug('### temp 2 : ' + temp);
            if(temp != null) optionsValueList.addAll( temp );
            
        }
        
        
        return optionsValueList;
    }
    
    private static Wizard_Questionnaire__c getQuestionDetails(id questionID)
    {
        
        Wizard_Questionnaire__c questionDetails = [SELECT Id, Question__c,Type_of_Permit__c,
                                                   Domain__c,isActive__c,Type__c,Description__c,Program_Line_Item_Pathway__c,
                                                   Link__c,Movement_Type__c,Signature__c,Signature__r.REF_Program_Name__c,
                                                   Signature__r.Line_Record_Type__c,Signature__r.Program_Prefix__c,
                                                   PSQActivity_Processor__c,Question_Classification__c,
                                                   Country_Group__c,State_Province_of_Destination__c,
                                                   (SELECT id,name 
                                                    FROM Wizard_Questionnaire__r)
                                                   FROM Wizard_Questionnaire__c 
                                                   WHERE id =: questionID
                                                   LIMIT 1];
        
        return questionDetails;
    }
    
    public static Wizard_Questionnaire__c getStartQuestion()
    {
        return [SELECT Id, Question__c,Type_of_Permit__c,Description__c,Application_Mapping_Index__c,
                Domain__c,isActive__c,Type__c,Program_Line_Item_Pathway__c,
                Link__c,Movement_Type__c,Signature__c,Signature__r.REF_Program_Name__c,
                Signature__r.Line_Record_Type__c,Signature__r.Program_Prefix__c,
                PSQActivity_Processor__c, Question_Classification__c
                
                FROM Wizard_Questionnaire__c 
                WHERE type__c =: 'PSQ START'
                LIMIT 1];
        //return new Wizard_Questionnaire__c();
    }
    
    public static Wizard_Questionnaire__c getNextQuestionByValue(ID currentQuestionID)
    {
        return [SELECT Id, Question__c,Type_of_Permit__c,Description__c,Application_Mapping_Index__c,
                Domain__c,isActive__c,Type__c,Program_Line_Item_Pathway__c,
                Link__c,Movement_Type__c,Signature__c,Signature__r.REF_Program_Name__c,
                Signature__r.Line_Record_Type__c,Signature__r.Program_Prefix__c,
                PSQActivity_Processor__c, Question_Classification__c,
                Country_Group__c,State_Province_of_Destination__c		                        
                FROM Wizard_Questionnaire__c 
                WHERE ID =:currentQuestionID
                LIMIT 1];
    }
    
    public static Wizard_Questionnaire__c getNextQuestion(ID nextQuestionID)
    {
        return [SELECT Id, Question__c,Type_of_Permit__c,Description__c,Application_Mapping_Index__c,
                Domain__c,isActive__c,Type__c,Program_Line_Item_Pathway__c,
                Link__c,Movement_Type__c,Signature__c,Signature__r.REF_Program_Name__c,
                Signature__r.Line_Record_Type__c,Signature__r.Program_Prefix__c,
                PSQActivity_Processor__c, Question_Classification__c,
                Country_Group__c,State_Province_of_Destination__c	
                FROM Wizard_Questionnaire__c 
                WHERE ID =:nextQuestionID
                LIMIT 1];
    }
    
    public static List<SelectOption> getCountryOfOrigin()
    {
        List<SelectOption> COO = new List<SelectOption>();
        for(Country__c recCountry: [Select Name from Country__c where Country_Status__c=:'Active' Order By Name]){
            COO.add(new SelectOption(recCountry.Id,recCountry.Name));
        }
        COO.add(0,new SelectOption('','--Select--'));
        return COO;
    }
    
    public static List<SelectOption> getStateOfDestination()
    {
        List<SelectOption> SOD = new List<SelectOption>();
        List<Level_1_Region__c> lvl1RegionRecords = new List<Level_1_Region__c>();
        lvl1RegionRecords = [Select Id, Name from Level_1_Region__c where Country__c in (Select id from Country__c where Country_Status__c=:'Active' and Name = :EFLGlobalConstants.getConstantValue('COUNTRY NAME USA')) Order By Name];   
        for(Level_1_Region__c recLR: lvl1RegionRecords){
            SOD.add(new SelectOption(recLR.Id,recLR.Name));
        }
        SOD.add(0,new SelectOption('','--Select--'));
        return SOD;
    }    
    
    public static Map<id, Wizard_Questionnaire__c> getQuestionsByNextQuestionIds(List<Id> nextQuestionIds)
    {
        return new Map<id, Wizard_Questionnaire__c>([SELECT Id, Question__c,Type_of_Permit__c,
                                                     Domain__c,isActive__c,Type__c,Description__c,Program_Line_Item_Pathway__c,
                                                     Link__c,Movement_Type__c,Signature__c,Signature__r.REF_Program_Name__c,
                                                     Signature__r.Line_Record_Type__c,Signature__r.Program_Prefix__c,
                                                     PSQActivity_Processor__c,Question_Classification__c,
                                                     Country_Group__c,State_Province_of_Destination__c,      
                                                     (SELECT id,name 
                                                      FROM Wizard_Questionnaire__r)
                                                     FROM Wizard_Questionnaire__c 
                                                     WHERE Id in : nextQuestionIds]);
    }
    
    public static Map<Id, Country_Junction__c> getSelectedCountryGroupMap(id countryId)
    {
        Map<Id, Country_Junction__c> selectedCountryGroupMap = new Map<Id, Country_Junction__c>();
        List<Country_Junction__c> countryJunctionList = new List<Country_Junction__c>();
        countryJunctionList = [select id, Name, Group__c from Country_Junction__c where Country__c = :countryId];
        for(Country_Junction__c cj:countryJunctionList)
        {
            selectedCountryGroupMap.put(cj.Group__c, cj);
        }
        return selectedCountryGroupMap;
    }
    
    public static List<Signature_Regulation_Junction__c> regulationsByThumbprintId(id thumbprintId)
    {
        List<Signature_Regulation_Junction__c> regulations = new List<Signature_Regulation_Junction__c>();
        regulations = [select id, Regulation__c from Signature_Regulation_Junction__c where Decision_Matrix__c IN (select id from Regulations_Association_Matrix__c where Thumbprint__c = :thumbprintId)];
        return regulations;
    }
    
    public static List<Signature_Regulation_Junction__c> regulationsByThumbprintIdAndPathway(id thumbprintId, List<string> lineitemProgramPathways)
    {
        List<Signature_Regulation_Junction__c> regulations = new List<Signature_Regulation_Junction__c>();
        regulations = [select id, Regulation__c from Signature_Regulation_Junction__c where Decision_Matrix__c IN (select id from Regulations_Association_Matrix__c where Thumbprint__c = :thumbprintId and Program_Line_Item_Pathway__r.name in :lineitemProgramPathways)];
        return regulations;
    }
    
    
}