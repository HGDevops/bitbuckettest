public inherited sharing class EFLApplicantAttachmentRepository {
    
    
    // Obtain all SOP IDs of a lineItem using line Item ID    
    public static Set<ID> selectIDsByLineItemID(id lineItemID){
        try{ 
            EFLEnforceAccessUtility.checkObjectReadAccess('Applicant_Attachments__c');
            list<Applicant_Attachments__c> SOPList = [SELECT Id
                                                      FROM Applicant_Attachments__c 
                                                      WHERE Animal_Care_AC__c =:lineItemId];
            List<Construct_Application_Junction__c> prevSOPList=[SELECT Design_Protocol_Record_SOP__c 
                                                                 FROM Construct_Application_Junction__c
                                                                 WHERE Line_Item__c =: lineItemID
                                                                 AND Design_Protocol_Record_SOP__c != NULL];
            Set<Id> SOPIDs = (new Map<Id,SObject>(SOPList)).keySet();
            Set<Id> prevSOPIDs = new set<ID>();
            for(Construct_Application_Junction__c pre : prevSOPList){
                prevSOPIDs.add(pre.Design_Protocol_Record_SOP__c);
            }
            
            prevSOPIDs.addAll(SOPIDs);  
            return prevSOPIDs;
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLApplicantAttachmentRepository.selectIDsByLineItemID()',e);                
        } 
        return null;
    }        
    
    
    // Obtain all Applicant Attachment records of a lineItem using line Item ID    
    public static list<Applicant_Attachments__c> selectByLineItemIDforClone(id lineItemID){
        try{ 
            EFLEnforceAccessUtility.checkObjectReadAccess('Applicant_Attachments__c');         
            List<Applicant_Attachments__c> applicantAttachmentList=[SELECT RecordTypeId,
                                                                    Applicant_Instructions__c,File_Name__c,
                                                                    Attachment_Type__c,Confinement_Protocols__c,
                                                                    Destination_Release_Description__c,Document_Types__c,
                                                                    File_Description__c,Final_Disposition_Description__c,
                                                                    Final_Disposition_Method__c,Internal_Only__c,NOTE_Notification__c,
                                                                    NOTE__c,Production_Design__c,Parent_Applicant_Attachment__c,
                                                                    Share_with_Applicant__c,Share_with_State__c,
                                                                    Status__c,Standard_Operating_Procedure__c,Tags__c,Total_Files__c
                                                                    FROM Applicant_Attachments__c 
                                                                    WHERE Animal_Care_AC__c =:lineItemId];
            
            return applicantAttachmentList;
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLApplicantAttachmentRepository.selectByLineItemIDforClone()',e);                
        } 
        return null;
    }     
    
    // Obtain all Applicant Attachment records of a lineItem using line Item ID    
    public static list<attachment> selectAttachmentsByList(List<Applicant_Attachments__c> applicantAttachmentList){
        try{ 
            EFLEnforceAccessUtility.checkObjectReadAccess('attachment');         
            
            List<Attachment> attachmentList=[SELECT Id,Name,
                                             ContentType,ParentId,
                                             Description,Body 
                                             FROM Attachment 
                                             WHERE ParentID in: applicantAttachmentList ];
            return attachmentList;
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLApplicantAttachmentRepository.selectAttachmentsByList()',e);                
        } 
        return null;
    }    
    
    
}