/*-----------------------------------------------------------------------  
 @Purpose: This Utility class derives Global Constants 
           across eFile from Custom meta datatype 'EFLGlobalConstant'
 ----------------------------------------------------------------------*/
public with sharing class EFLGlobalConstants {

  //Returns Constant Value from CustomMetaData Type    
   public static String getConstantValue(string constantCMDLabel){

       EFLGlobalConstant__mdt constantCMD = [Select label,
                                                      Value__c 
                                                 from EFLGlobalConstant__mdt 
                                                where label =:constantCMDLabel
                                                limit 1];        
       
       return constantCMD.Value__c;
    }
    
}