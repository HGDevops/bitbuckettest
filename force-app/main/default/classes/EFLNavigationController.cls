/**********************************************************************  
* @Related Component: Navigation
* @User Story Number :  
************************************************************************/
public with sharing class EFLNavigationController {
    
    public static boolean getisAdmin(){
       return EFLUserUtility.isAccountAdmin();
    }
    
}