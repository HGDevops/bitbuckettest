public with sharing class EFLLineItemController {
    
    //Attributes
    public Id LineItemId= (id) EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('id'));
    public EFLPreScreeningWrapper psw {get;set;}
    public EFLPSQQuestion psqq{get;set;}
    public boolean CBIVal{get; set;} //Ravee Racharla Date:9/10/2018
    public boolean disableCBIField{get; set;}
    public Boolean isRenewal {get;set;}
    private string strAppStatus; //Ravee Racharla Date:11/02/2018 W-027250 Lock/UnLock Screen
    
    public boolean renderPanel{get; set;}
    public List <AC__c> relatedLineItem{get; set;}
    public AC__c lineItemRecord{      
        get{
            if(lineItemRecord==null && LineItemId!=null){
                
                lineItemRecord  = EFLLineItemRepository.selectAllFieldsAndRelatedFieldsbyID(LineItemId);
                
                //Begin Ravee Racharla Date:9/10/2018
                
                if ( lineItemRecord != Null ) {
                    strAppStatus = lineitemRecord.Status__c; //Ravee Racharla Date:11/02/2018 W-027250 Lock/UnLock Screen
                    if ( lineItemRecord.Does_This_Application_Contain_CBI__c !=''  && 
                        lineItemRecord.Does_This_Application_Contain_CBI__c !=null ){
                            CBIVal = True; 
                        }
                    else {
                        CBIVal = False; 
                    }
                    sSelectedMovementType = lineItemRecord.Movement_Type__c; 
                }
                
                //End Ravee Racharla Date:9/10/2018
            }
            
            return lineItemRecord;
        }
        set;
    }
    
    public string currentStep
    {
        get
        {
            if(lineItemRecord.Program_Line_Item_Pathway__r.Name==EFLGlobalConstants.getConstantValue('PROGRAM_PATHWAY_ACLD'))
            {
                currentStep = 'Animal Transportation';
            }
            else
            {
                currentStep = 'Application Details';
            }
            return currentStep;
        }
        set;
    }
    
    //Ravee Racharla 10/31/2018 W-027250
    public boolean lockLineItem{
        get{
            lockLineItem = EFLLockUnlockUtility.lockLineitem(lineItemRecord);
            return lockLineItem;
        }set; 
    }
    //Ravee Racharla 10/31/2018 W-027250
    String soql;
    public string isError{get; set;}
    public list<boolean> lstRequired{get; set;}
    public list<Application_Request__c> appResList {
        get{
            return [select id,question__c, 
                    answer__c,
                    Sequence_Number__c 
                    from Application_Request__c
                    where line_Item__c =: LineItemId
                    order by createdDate] ; 
        }
        set;
    }
    
    public string instructions{
        
        get{
            if(instructions == NULL){
                
                if(lineItemRecord.Program_Line_Item_Pathway__r.Name==EFLGlobalConstants.getConstantValue('PROGRAM_PATHWAY_ACLD'))
                {
                    instructions = EFLGenericutility.getInstructions(null,null,'Animal Transportation','LineItem');
                }
                else
                {
                    instructions =  EFLGenericutility.getInstructions(null,null ,'CBI','LineItem');
                }
            } 
            return instructions; 
        }
        set;
    }
    public String chevronOptions { get{
        if (chevronOptions == null){
            list<EFLLineItemFlow__mdt> lineItemFlowList = new list<EFLLineItemFlow__mdt>();
            list<String> options = new list<String>();
            lineItemFlowList = [select ThumbPrint__c,
                                Sequence__c,
                                Program__c,
                                EFLLineItemStep__c,
                                EFLLineItemStep__r.Step__c
                                from EFLLineItemFlow__mdt
                                where ThumbPrint__c =: lineItemRecord.Thumbprint__c
                                order by Sequence__c]; 
            for(EFLLineItemFlow__mdt flow : lineItemFlowList){ 
                options.add(flow.EFLLineItemStep__r.Step__c); 
            }     
            
            //Assigning array to a string  
            chevronOptions = JSON.serialize(options); 
            return chevronOptions;
        } 
        
        return chevronOptions;
    }set;}
    
    public EFLLineItemController(ApexPages.StandardController stdController){
        LineItemId=ApexPages.currentPage().getParameters().get('id');
        relatedLineItem = new List <AC__c>();
        relatedLineItem = [SELECT id, Does_This_Application_Contain_CBI__c FROM AC__c where id =: LineItemId];
        psw = new EFLPreScreeningWrapper();
        psqq = new EFLPSQQuestion();
        psqq = psw.currentQuestion;
        
        renderPanel = True; 
        //Begin Author: Ravee Racharla Date:9/10/2018 
        getLineItemRecord(); 
        //End Author: Ravee Racharla Date:9/10/2018 
        appResList = new list<Application_Request__c>();
        
        getApplicationRequest();
        
        //Line Item Section Start//
        lstRequired = new list<boolean>(); 
        mandatoryFields = new Map<String,String>();
        mandatoryFieldsSect = new Map<String,String>(); 
        mandatoryFields1 = new Map<String,String>();  
        lstPrgItemSect = new List<lineitemSection>();
        getLineItemSectionDetails( );
    }  
    //Author:Ravee Racharla Date:9/10/2018
    public void getLineItemRecord(){
        if((lineItemRecord==null && LineItemId!=null)|| test.isRunningTest()){
            lineItemRecord  = EFLLineItemRepository.selectAllFieldsAndRelatedFieldsbyID(LineItemId);
            if (lineItemRecord!=null){
                strAppStatus = lineitemRecord.Status__c; //Ravee Racharla Date:11/02/2018 W-027250 Lock/UnLock Screen
                if ( lineItemRecord.Does_This_Application_Contain_CBI__c !=''  && 
                    lineItemRecord.Does_This_Application_Contain_CBI__c !=null ){
                        CBIVal = True; 
                    }
                else {
                    CBIVal = False; 
                }
                sSelectedMovementType = lineItemRecord.Movement_Type__c;
            }
        }
        
    }
    
    //Begin Author:Ravee Racharla  Date:9/10/2018
    public void Save(){
        //Update the record as it is already created in Proceed Application Step
        //Read values from CBI Panel 
        ac__c ac = new ac__C(); 
        ac.id = lineitemrecord.id; 
        
        if(lineItemRecord.Program_Line_Item_Pathway__r.Name==EFLGlobalConstants.getConstantValue('PROGRAM_PATHWAY_ACLD'))
        {
            //AC Fields
            ac.Transporter_Name__c = lineitemrecord.Transporter_Name__c;
            ac.Transporter_Type__c = lineitemrecord.Transporter_Type__c;
            ac.Port_of_Entry__c = lineitemrecord.Port_of_Entry__c;
            ac.Air_Transporter_Flight_Number__c = lineitemrecord.Air_Transporter_Flight_Number__c;
            ac.Proposed_date_of_arrival__c = lineitemrecord.Proposed_date_of_arrival__c;
            ac.AC_Arrival_Time__c = lineitemrecord.AC_Arrival_Time__c;
            ac.Port_of_Embarkation__c = lineitemrecord.Port_of_Embarkation__c;
            ac.My_port_is_not_listed__c = lineitemrecord.My_port_is_not_listed__c;
            ac.Port_If_Not_Listed__c = lineitemrecord.Port_If_Not_Listed__c;
            ac.Departure_Time__c = lineitemrecord.Departure_Time__c;
            ac.AC_Departure_Time__c = lineitemrecord.AC_Departure_Time__c;
        }
        else
        {
            
            if(CBIVal == false){
                ac.Does_This_Application_Contain_CBI__c  = lineitemrecord.Does_This_Application_Contain_CBI__c;
                ac.CBI_Justification__c = lineitemrecord.CBI_Justification__c; 
                ac.Status__c = 'Saved';            
            }
            else{  //All other fields 
                
                ac.CBI_Justification__c = lineitemrecord.CBI_Justification__c; 
                ac.Proposed_Start_Date__c = lineitemrecord.Proposed_Start_Date__c; 
                ac.Proposed_End_Date__c = lineitemrecord.Proposed_End_Date__c; 
                ac.Hand_Carry__c = lineitemrecord.Hand_Carry__c;
                ac.Number_of_Labels__c =lineitemrecord.Number_of_Labels__c; 
                ac.Biological_Material_present_in_Article__c =lineitemrecord.Biological_Material_present_in_Article__c;
                ac.If_yes_Please_Describe__c =lineitemrecord.If_yes_Please_Describe__c;
                ac.purpose_of_Permit__c =lineitemrecord.purpose_of_Permit__c; 
                ac.Means_of_Movement__c = lineitemrecord.Means_of_Movement__c; 
                ac.Do_you_want_to_use_a_previously_approved__c =lineitemrecord.Do_you_want_to_use_a_previously_approved__c;
                ac.Are_you_applying_for_Variance__c =lineitemrecord.Are_you_applying_for_Variance__c;
                ac.What_is_the_previously_reviewed_variance__c =lineitemrecord.What_is_the_previously_reviewed_variance__c;
                ac.How_will_variance_be_used__c =lineitemrecord.How_will_variance_be_used__c;
                ac.Applicant_Reference_Number__c =lineitemrecord.Applicant_Reference_Number__c;
                ac.Additional_Information__c = lineitemrecord.Additional_Information__c;
                ac.Application_Details_Status__c= CARPOL_Constants.READY_TO_SUBMIT;
                ac.Amendment_Description__c = lineitemrecord.Amendment_Description__c;
                ac.Renewal_Description__c = lineitemrecord.Renewal_Description__c;
                ac.skip_validation__c = false;
            }
        }
        
        try{
            update ac; 
            strAppStatus = lineitemRecord.Status__c; //Ravee Racharla Date:11/02/2018 W-027250 Lock/UnLock Screen
            CBIval = True; 
            getLineItemRecord();
            lstRequired = new list<boolean>(); 
            lstPrgItemSect = new List<lineitemSection>();
            getLineItemSectionDetails();          
            
        }catch( Exception ex) {
            String errorMsg = ex.getMessage();
            if(errorMsg.contains('Justification only required when this application contains CBI. Delete the CBI justification text to save')){
                lineitemrecord.CBI_Justification__c.adderror('Justification only required when this application contains CBI. Delete the CBI justification text to save.');
            } 
            
            if(errorMsg.contains('If this application contains CBI, you must provide a CBI justification')){
                lineitemrecord.CBI_Justification__c.adderror('If this application contains CBI, you must provide a CBI justification.');
            } 
            //Ravee 10/12/2018 This is not required as field lenght is 4000
            if(errorMsg.contains('You cannot use more than 4,000 characters')){
                lineitemrecord.CBI_Justification__c.adderror('You cannot use more than 4,000 characters');
            }  
            //CBI Justification cannot contain CBI data. Text in the field cannot contain brackets.
            if(errorMsg.contains('CBI Justification cannot contain CBI data. Text in the field cannot contain brackets.')){
                lineitemrecord.CBI_Justification__c.adderror('CBI Justification cannot contain CBI data. Text in the field cannot contain brackets.');
            }
            //KM:12/12/2018::::: START:ADD ERROR MESSAGE FOR W-027011:::::: 
            if(errorMsg.contains('Proposed Effective Date cannot be before today date.')){
                lineitemrecord.Proposed_Start_Date__c.adderror('Proposed Effective Date cannot be before today'  +'\'s ' +'date.');
            } 
            //KM:12/12/2018::::: ENDS:ADD ERROR MESSAGE FOR W-027011:::::: 
            if(errorMsg.contains('Proposed effective date cannot be earlier than today')){
                lineitemrecord.Proposed_Start_Date__c.adderror('Proposed effective date cannot be earlier than today');
            } 
            if(errorMsg.contains('Proposed effective date cannot be later than Proposed expiration date')){
                lineitemrecord.Proposed_Start_Date__c.adderror('Proposed effective date cannot be later than Proposed expiration date.');
            } 
            if(errorMsg.contains('Proposed Expiration Date must be within one year from the Proposed Effective date.')){
                lineitemrecord.Proposed_End_Date__c.adderror('Proposed End Date must be within one year from the Proposed Effective date.');
            } 
            if(errorMsg.contains('Proposed Expiration Date must be within three years from the Proposed Effective date')){
                lineitemrecord.Proposed_End_Date__c.adderror('Proposed End Date must be within three years from the Proposed Effective date.');
            }  
            if(errorMsg.contains('You have indicated you will be applying for variance. Please describe how the variance will be used')){
                lineitemrecord.How_will_variance_be_used__c.adderror('You have indicated you will be applying for variance. Please describe how the variance will be used.');
            }  
            
            if(errorMsg.contains('You have indicated that you want to use a previously reviewed variance')){
                lineitemrecord.What_is_the_previously_reviewed_variance__c.adderror('You have indicated that you want to use a previously reviewed variance. Please provide the previously reviewed variance number.');
            }  
            
            if(errorMsg.contains('If Biological Material is present in Article, reason is mandatory to be filled')){
                lineitemrecord.If_yes_Please_Describe__c.adderror('If Biological Material is present in Article, reason is mandatory to be filled.');
            }
            
            if(errorMsg.contains('Please enter Transporter Flight Number')){
                lineitemrecord.Air_Transporter_Flight_Number__c.adderror('Please enter Transporter Flight Number.');
            }
            
            if(errorMsg.contains('The foreign ports for Port of Entry and Port of Export must be different')){
                lineitemrecord.Port_of_Embarkation__c.adderror('The foreign ports for Port of Entry and Port of Export must be different.');
            }
            
            if(errorMsg.contains('Proposed date of arrival has to be a date in future.')){
                lineitemrecord.Proposed_date_of_arrival__c.adderror('Proposed date of arrival has to be a date in future.');
            }
            
            if(errorMsg.contains('Proposed date of arrival cannot be earlier than Departure Date/Time.')){
                lineitemrecord.Proposed_date_of_arrival__c.adderror('Proposed date of arrival cannot be earlier than Departure Date/Time.');
            }
            
            if(errorMsg.contains('Please type the port in \'Port If not Listed\' Text Field if you have checked \'My port is not listed\' checkbox.')){
                lineitemrecord.Port_If_Not_Listed__c.adderror('Please type the port in \'Port If not Listed\' Text Field if you have checked \'My port is not listed\' checkbox.');
            }
            
            if(errorMsg.contains('Departure Time must be earlier than Arrival Time')){
                lineitemrecord.Departure_Time__c.adderror('Departure Time must be earlier than Arrival Time.');
            }
            
            return;
        }
        
    }
    //End Author:Ravee Racharla  Date:9/10/2018
    public PageReference Cancel(){
        
        ApexPages.PageReference dirpage= new ApexPages.PageReference('/apex/EFLLineItem?id='+lineItemID);
        dirpage.setRedirect(true);
        return dirpage;
    }   
    //End Author:Ravee Racharla  Date:9/10/2018
    
    public EFLPreScreeningWrapper getApplicationRequest()
    {
        appResList = new list<Application_Request__c>();
        Wizard_Questionnaire__c psquestion; 
        
        appResList = [select question__c, answer__c,
                      Sequence_Number__c 
                      from Application_Request__c
                      where line_Item__c =: LineItemId
                      order by Sequence_Number__c]; 
        psw.QuestionList = new list<EFLPSQQuestion>(); 
        
        for(Application_Request__c ar: appResList){
            
            psqq = new EFLPSQQuestion();
            psqq.psquestion =  new Wizard_Questionnaire__c();
            psqq.psqSelectedOption = new Wizard_Selections__c();   
            psqq.psquestion.Question__c = ar.Question__c;
            psqq.psqSelectedOption.value__c = ar.Answer__c;
            psw.QuestionList.add(psqq); 
        }        
        return psw;
    } 
    
    
    //Line Item Section Begin//
    //Attributes
    /** Program Line Item Sections **/
    public List<lineitemSection> lstPrgItemSect{get;set;}
    public Integer intPrgItemSectNext;
    public decimal openSection{get;set;}
    public string Sectiontype{get;set;}
    public AC__c objLineItem{get;set;} 
    public boolean populateApplicant{get;set;}
    public string sSelectedLandOpt{get;set;}
    public id SelectedPathwayId{get;set;}
    public Map<string,string> mandatoryFields;
    public Map<string,string> mandatoryFieldsSect;     
    public Map<string,string> mandatoryFields1;// keerthi added for W-017725//
    
    public string sSelectedMovementType{get; set;} //Ravee Racharla Date:9/18/2018 W-026099
    
    @TestVisible public class lineitemSection {
        //Static Variables 
        public Program_Line_Item_Section__c liSection {get;set;}
        public List<dynamicField> liFields{get;set;}
        public Boolean addressRestrictionCheck{get;set;}
        
        public lineitemSection() {
            liSection = new Program_Line_Item_Section__c();
            liFields = new List<dynamicField>();
            addressRestrictionCheck = false;
        }
        
    }    
    
    @TestVisible public class dynamicField{
        //Static Variables 
        public string fieldname {get;set;}
        public string defaultvalue {get;set;}
        public string labelname {get;set;}
        public string inputtype {get;set;}
        public string helptext {get;set;}
        public boolean iscbi {get;set;}
        
        public string cbifieldname {get;set;}
        public boolean requiredflag {get;set;}
        public boolean isdisabled{get;set;}
        public string fieldId{get;set;}
        public boolean isDate{get;set;}
        public boolean isDateTime{get;set;}
        public string rendervalue{get;set;}
        
        
        //Ravee Racharla 10/10/2018 
        public string errMsg{get; set;}
        //Ravee Racharla 10/10/2018 
        
        public dynamicField() {
            fieldname = '';
            defaultvalue = '';
            labelname = '';
            inputtype = '';
            helptext = '';
            iscbi = false;
            
            cbifieldname = '';
            requiredflag = false;
            isdisabled = false;
            fieldId = '';
            isDate = false;
            isDateTime = false;
            rendervalue = '';
            
            errMsg = ''; 
        }
    }        
    
    public List<lineitemSection> getLineItemSectionDetails( ){
        
        //get all the sections
        if (lstRequired != null || lstRequired.size() > 0) {
            lstRequired.clear(); 
        }
        
        List<Program_Line_Item_Section__c> sections = new List<Program_Line_Item_Section__c>();
        
        sections = [SELECT ID, Name,Type_of_Permit__c,
                    Section_Order__c,
                    Include_Applicant_Information__c,
                    EFLAddMore__c, section_type__c, Program_Line_Item_Pathway__r.Name
                    FROM Program_Line_Item_Section__c 
                    WHERE Section_Order__c > 0 
                    AND Program_Line_Item_Pathway__r.Id =:lineitemrecord.Program_Line_Item_Pathway__c 
                    AND Type_of_Permit__c =: lineitemrecord.Type_of_Permit__c
                    AND STATUS__c != 'Inactive'  //author:Ravee Racharla Date:9/10/2018
                    Order By Section_Order__c]; 
       
        //Begin Author: Ravee Racharla Date:9/8/2018
        
        if (CBIVal ==null){
            getLineItemRecord();
        }
        //End Author: Ravee Racharla Date:9/8/2018
        for(Program_Line_Item_Section__c sect : sections){ 
            if(lineItemRecord.Program_Line_Item_Pathway__r.Name==EFLGlobalConstants.getConstantValue('PROGRAM_PATHWAY_ACLD'))
            {
                lstPrgItemSect.add(addNewLISection(sect,getcurrentPanel(sect.Name,sect.Section_Order__c -1)));
            }
            else
            {
                if (CBIVal == true){
                    lstPrgItemSect.add(addNewLISection(sect,getcurrentPanel(sect.Name,sect.Section_Order__c -1)));
                }
                else if  (CBIVal ==false && sect.name == 'Confidential Business Information (CBI)' ){
                    lstPrgItemSect.add(addNewLISection(sect,getcurrentPanel(sect.Name,sect.Section_Order__c -1)));
                }
            }
        }
        return lstPrgItemSect;
    }
    
    // This method is used to display current section and its Dynamic fields
    public List<dynamicField> getcurrentPanel(string progid,decimal sectionnumber){
        List<dynamicField> sectionFields = new List<dynamicField>();
        // Loop by progid = Program Pathway      
        // Order by Field_Order__c -- Field Order API is now ROW, Column_Placement is (First, Second, Span)
        // Getting list of program section item fields
        // TODO: Need to update to look at pathwayID not programId - Dawn 3-24 is this still true?
        Decimal sectorder = sectionnumber+1; 
        list<Program_Line_Item_Field__c> lstfields = new list<Program_Line_Item_Field__c>();
        lstfields = [select id,Name,Always_Required__c,Validation_Message__c,Application_Criteria__c,Application_Field_Name__c,Default_Value__c,
                     Dependent_on_Application_Field__c,Dependent_on__c,Destination_Object__c,Destination_Record_Type__c,
                     Display_Name__c,Display_Not_Listed_Checkbox__c, 
                     Field_Help_Text__c, 
                     Field_is_Prepulated__c,Field_Order__c,
                     isActive__c,Label_for_Name_Field__c,Dependent_Value__c,Prepopulate_From__c,Prepopulate_on_Page_Load__c,
                     Program_Line_Item_Section__c,Read_Only__c,Program_Line_Item_Section__r.Name,Field_API_Name__c,
                     Program_Line_Item_Section__r.Description_Help_Text__c, CBI__c, CBI_Field_API_Name__c,
                     Column_Placement__c,Program_Line_Item_Section__r.Include_Applicant_Information__c,
                     Program_Line_Item_Section__r.EFLAddMore__c, EFLAddress_Restriction__c  
                     from Program_Line_Item_Field__c 
                     where isActive__c='Yes'  
                     // and Field_API_Name__c!=null 
                     and Movement_Type__c includes (:sSelectedMovementType) //Author:Ravee Racharla Date:9/18/2018
                     and Program_Line_Item_Section__r.Section_Order__c=:sectorder 
                     and Program_Line_Item_Section__r.Program_Line_Item_Pathway__c=: lineitemrecord.Program_Line_Item_Pathway__c
                     order by Field_Order__c];
        
        //autoCheck = false;
        populateApplicant = false; 
        String CompId = '';  
        String CompName = '';  
        
        // Creating dynamic fields using itmfield variable against List <lstfields>
        for (Program_Line_Item_Field__c itmfield:lstfields ) {
            dynamicField dField = new dynamicField();
            //this gets changed later if the type of field changes
            dField.inputtype = 'inputfield';
            //check for date field
            Schema.SObjectType t = Schema.getGlobalDescribe().get('AC__c');
            Schema.DescribeSObjectResult r = t.getDescribe();
            Schema.DescribeFieldResult f = r.fields.getMap().get(itmfield.Field_API_Name__c).getDescribe();
            
            if (f.getType() == Schema.DisplayType.Date || f.getType() == Schema.DisplayType.DateTime) {
                dField.inputtype = 'inputdate';
            }
            
            if (f.getType() == Schema.DisplayType.MULTIPICKLIST) {
                dField.inputtype = 'MULTIPICKLIST';
            }
            
            // Adding help text  -- Validate where this displays. On ? mark or section help text field
            if (itmfield.Field_Help_Text__c!=null) {
                dField.helpText=itmfield.Field_Help_Text__c;
            }
            
            // Adding cbifieldname
            if (itmfield.CBI_Field_API_Name__c!=null) {
                dField.cbifieldname=itmfield.CBI_Field_API_Name__c;
            }
            
            // Adding cbi at field level
            if (itmfield.cbi__c != null) {
                dField.iscbi=itmfield.cbi__c;
            }
            
            if (itmfield.Always_Required__c) {
                lstRequired.add(true); 
            } else {
                lstRequired.add(false); 
            }
            
            //Begin Author:Ravee Racharla Date: 9/11/2018
            //Add isdisabled attribute 
            if (itmfield.Field_API_Name__c == 'Does_This_Application_Contain_CBI__c' && CBIVal == True ){
                dfield.isdisabled = true; 
                itmfield.Read_Only__c=true;
            }
            
            //Ravee Amendment Begin
            if ((itmfield.Field_API_Name__c == 'Proposed_Start_Date__c' 
                 || itmfield.Field_API_Name__c == 'Proposed_End_Date__c' 
                 || itmfield.Field_API_Name__c == 'Movement_Type__c' 
                 || itmfield.Field_API_Name__c =='Purpose_of_Permit__c') 
                && (lineItemRecord.Application_Number__r.Application_Type__c == 'Amendment' )) {
                    itmfield.Read_Only__c=true;
                }
            
            
            //Gary Renewal Begin
            //W-026305- Rajesh Potla - Modified the code to make fields editable for Renewal app
            isRenewal = false;
            Set<String> setDisabledFieldsForRenewal = new Set<String>{'Purpose_of_Permit__c','Does_This_Application_Contain_CBI__c','Proposed_Start_Date__c','Proposed_End_Date__c'};
                if (lineItemRecord.Application_Number__r.Application_Type__c == 'Renewal') {
                    if(setDisabledFieldsForRenewal.contains(itmfield.Field_API_Name__c)) itmfield.Read_Only__c = true;
                    isRenewal = true;
                }
            
            // Adding label value to displayname if filled out.
            if (itmfield.Display_Name__c!=null) {
                dField.labelname = itmfield.Display_Name__c;
            } else if (itmfield.Name!=null) {
                dField.labelname  = itmfield.Name;
            }
            // Catches bad data where no lable or display name match -- shouldn't be needed unless it catches a line label being changed when a record has not been updated to reflect the change.
            else {
                dField.labelname  = 'Label name is not provided';
            }
            
            if (itmfield.Read_Only__c && CompId != '' && itmfield.Field_API_Name__c == 'Component_Defualt_Name__c') {
                lineItemRecord.Component__c = CompId;
                lineItemRecord.Component_Defualt_Name__c= CompName;
                //dField.isdisabled= true;
            }
            
            //Jon Sadler  W-017031 12-06-17
            //if field is read-only or line item is read-only
            if(itmfield.Read_Only__c || lockLineItem==false || (lineItemRecord.Read_Only__c && itmfield.Field_API_Name__c != 'CBI_Justification__c') ) {
                
                dField.inputtype = 'inputtext'; 
                //Jon Sadler  W-017031 12-06-17
                //if this is a lookup field, then display the text value instead of the ID
                
                if (f.getType() == Schema.DisplayType.REFERENCE) {
                    dField.fieldname = itmfield.Field_API_Name__c.removeEnd('c') +'r.name';
                } else {
                    // adding default value if exists in configuration
                    if (itmfield.Default_Value__c!=null) {
                        dField.defaultvalue = itmfield.Default_Value__c; 
                    }
                    // adding expressions value as in page
                    else {
                        // If no default text provided then defaults to a field specified -- validate 
                        dField.fieldname = itmfield.Field_API_Name__c;
                        
                        /* keerthi changes start for W-017725*/
                        if (itmfield.Always_Required__c) {
                            dField.requiredFlag = true;
                            
                            if (itmfield.Display_Name__c!=null && itmfield.Display_Name__c!='') {
                                if (!mandatoryFields.containsKey(itmfield.Field_API_Name__c)) {
                                    mandatoryFields.put(itmfield.Field_API_Name__c,itmfield.Display_Name__c);
                                    mandatoryFieldsSect.put(itmfield.Field_API_Name__c,itmfield.Program_Line_Item_Section__r.Name); // Added by VV for mandatory field section name
                                    mandatoryFields1.put(itmfield.Field_API_Name__c,itmfield.Validation_Message__c);
                                }
                            } else {
                                String labelName = itmfield.Field_API_Name__c.replace('__c','');
                                if (!mandatoryFields.containsKey(itmfield.Field_API_Name__c)) {
                                    mandatoryFields.put(itmfield.Field_API_Name__c,labelName.replace('_',' '));
                                    mandatoryFieldsSect.put(itmfield.Field_API_Name__c,itmfield.Program_Line_Item_Section__r.Name); // Added by VV for mandatory field section name
                                }
                            }
                        } /* keerthi changes end */
                    }
                }
                
                //Jon Sadler  W-017031 12-06-17
                //check if date or datetime. if so, we'll format in VF
                //TODO: currently formatting to GMT. this needs to be updated client-side
                if(f.getType() == Schema.DisplayType.DATE){
                    dField.isDate = true;
                }
                if(f.getType() == Schema.DisplayType.DATETIME){
                    dField.isDateTime = true;
                }
                
                // checks if field is required only
                if(itmfield.Always_Required__c){
                    dField.requiredflag=true;
                    //read only field does not need to be in input validation
                }
                
                
                sectionFields.add(dField);
                
            } 
            
            // Creating dynamic input field
            else{
                dField.fieldname = itmfield.Field_API_Name__c;
                //infield.styleclass='lookupcss';
                
                if(itmfield.Display_Not_Listed_Checkbox__c){
                    // newasscont = 'true';
                }
                
                // Adding lookup field and lookup icon
                
                
                
                //do this instead of building custom lookup for now
                dField.fieldname = itmfield.Field_API_Name__c;
                if(itmfield.Always_Required__c){
                    dField.requiredFlag = true;
                    if(itmfield.Display_Name__c!=null && itmfield.Display_Name__c!='')
                    {
                        if(!mandatoryFields.containsKey(itmfield.Field_API_Name__c)){
                            mandatoryFields.put(itmfield.Field_API_Name__c,itmfield.Display_Name__c);
                            mandatoryFieldsSect.put(itmfield.Field_API_Name__c,itmfield.Program_Line_Item_Section__r.Name); // Added by VV for mandatory field section name
                        }  
                    }
                    else
                    {
                        String labelName = itmfield.Field_API_Name__c.replace('__c','');
                        if(!mandatoryFields.containsKey(itmfield.Field_API_Name__c)){
                            mandatoryFields.put(itmfield.Field_API_Name__c,labelName.replace('_',' '));
                            mandatoryFieldsSect.put(itmfield.Field_API_Name__c,itmfield.Program_Line_Item_Section__r.Name); // Added by VV for mandatory field section name
                        }
                    }
                    
                }                     
                //} 
                
                if(itmfield.Always_Required__c){
                    
                    
                    dField.requiredflag = true;
                    if(!mandatoryFields.containsKey(itmfield.Field_API_Name__c)){
                        mandatoryFields.put(itmfield.Field_API_Name__c,dField.labelname);
                        mandatoryFieldsSect.put(itmfield.Field_API_Name__c,itmfield.Program_Line_Item_Section__r.Name); // Added by VV for mandatory field section name
                    }
                    
                } 
                //} 
                //should this field render even or odd
                if(math.mod(sectionFields.size()+1, 2) == 0){
                    dField.rendervalue = 'even';
                } else {
                    dField.rendervalue = 'odd';
                }
                sectionFields.add(dField);
            } 
        }
        return sectionFields;
    }   
    
    public lineitemSection addNewLISection(Program_Line_Item_Section__c s,List<dynamicField> d){
        lineitemSection lis = new lineitemSection();
        lis.liSection = s;
        lis.liFields = d;
        return lis;
    }    
    //Line Item Sections End// 
    
    
}