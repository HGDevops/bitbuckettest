/**
1-11-17 VV created for testing the class EFLTransitAddBuilding
 */
@isTest
private class EFLTransitAddBuilding_Test {

    static testMethod void myUnitTest() { 
        
            String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
            CARPOL_AC_TestDataManager dataObj = new CARPOL_AC_TestDataManager ();
            dataObj .insertcustomsettings();
            Application__c  newApp =dataObj.newapplication();
            AC__c  newLine = dataObj.newLineItem('Personal Use', newApp);
            test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.EFLTransitAddBuilding')); 
            System.currentPageReference().getParameters().put('LineItemId',newLine.Id);
            EFLTransitAddBuilding obj = new EFLTransitAddBuilding();
            obj.CancelTransitLocal();
            obj.SaveTransitLocal();
            obj.Add();
            obj.addMore();
            obj.deleteTransit();
            obj.removeTransit();
            obj.removeMore();
            system.assert(obj != null);
            //obj.addMore();
        test.stopTest();
    }   
}