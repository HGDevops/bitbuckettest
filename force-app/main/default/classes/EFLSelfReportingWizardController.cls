public  with sharing class EFLSelfReportingWizardController { 
    public static final string REPORT_SUBMITTED = 'Submitted';
    public static final string PREPLANTING_NOTICE_URL_PARAM = 'pre_planting_notice';
    public string applicantName {
        get{
            return UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
           }
        set;
    }
    public id authorizationId
    {
        get{
            authorizationID = null;
            if(ApexPages.currentPage().getParameters().get('authId')!=null)
            {
                authorizationID = EFLGenericUtility.sanitizeString(EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('authId')) );
            }   
            return authorizationID; 
        }
        set;
    }
    public Authorizations__c authObj {
        get{
            return [Select id, Name,Recordtype.name, Multi_Year_Permit__c, BRS_Introduction_Type__c, BRS_Purpose_of_Permit__c,Expiration_Date__c,
                    Date_Issued__c, Effective_Date__c ,Permit_Number__c, Status__c,Authorization_Type__c,Application_Type__c, Application_CBI__c 
                    From authorizations__c Where Id=:authorizationID limit 1];
        }
        set;   
    }
    public string doesAppContainCBI 
    { 
        get{
            doesAppContainCBI = '';
            if(authObj!=null)
            {
                doesAppContainCBI = authObj.Application_CBI__c;   
            }
            return doesAppContainCBI;
        }
        set;
    }
    public id reportSumID
    {
        get{
            reportSumID = null;
            if(ApexPages.currentPage().getParameters().get('rsId')!=null)
            {
                reportSumID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('rsId'));
            }   
            return reportSumID; 
        }
        set;
    }
    public string reportType
    {
        get{
            reportType = '';
            if(ApexPages.currentPage().getParameters().get('reportType')!=null)
            {
                reportType = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('reportType'));
            }   
            return reportType; 
        }
        set;
    }
    public Report_Summary__c rs
    {
        get{
            if(reportSumId!=null)
            {
                rs =  new Report_Summary__c();
                rs = EFLSelfReportingRepository.getReportSummaryById(reportSumId);           
            }   
            
            return rs; 
        }
        set;
    }
    public boolean rsIsSubmitted {
        get{
            rsIsSubmitted = false;
            if(rs!=null)
            {
                if(rs.Status__c==REPORT_SUBMITTED) {
                    rsIsSubmitted = true;
                }   
            }
            return rsIsSubmitted;
        } 
        set;
    }
    public string repSummName 
    { 
        get{
            repSummName = '';
            if(rs!=null)
            {
                repSummName = rs.Name;   
            }
            return repSummName;
        }
        set;
    }
    public string reportTypeName
    {
        get{
            reportTypeName = '';
            if(reportType==PREPLANTING_NOTICE_URL_PARAM)
            {
                reportTypeName = reportTypeMap.get(reportType);
            }else
                reportTypeName = reportTypeMap.get(reportType);
            return reportTypeName;
        }
        set;
    }
    public id recordTypeId
    {
        get{
            if(reportType==PREPLANTING_NOTICE_URL_PARAM)
            {
            recordTypeId = EFLGenericUtility.getRecordTypeId(reportType);
            }
            else 
            {
            recordTypeId = EFLGenericUtility.getRecordTypeId(reportType);
            }   
            return recordTypeId;    
        }
        set;
    }
    public id selfReportId {get; set;}
    public string searchString{get;set;}
    public Self_Reporting__c selfReporting {get;set;}
    public List<Self_Reporting__c> selfReportingList {get;set;}
    public id newReportSummaryId {get; set;}
    public String locationKeyPrefix{get;set;}
    //Pagination
    public List<SelectOption> paginationSizeOptions{get;set;}
    public Integer noOfRecords{get; set;} 
    public Integer size{get;set;} 
    //Boolean variables
	public Boolean PrePlantingview{get;set;}
    public Boolean PreHarvestview{get;set;}
    public Boolean Cleaningview{get;set;}
    public Boolean submitBool {get;set;}
    public Boolean cbiValidation {get; set;}
    
    private Map<String, String> reportTypeMap = new Map<String, String>{
        	'pre_planting_notice' => CARPOL_Constants.PRE_PLANTING_NOTICE,
            'pre_flowering_notice' => CARPOL_Constants.PRE_FLOWERING_NOTICE,
            'harvest_destruct' => CARPOL_Constants.HARVEST_DESTRUCT,
            'cleaning_notice' => CARPOL_Constants.CLEANING_NOTICE
            };
	/*Variable and Properties - End*/ 
    
    public List<SelectOption> getPaginationSizeOptions() {
        size=10;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('25','25'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        return paginationSizeOptions;
    }
 
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
            if(con == null && rs.Id !=null) {    
                con= new ApexPages.StandardSetController(EFLSelfReportingRepository.selfRepListByRSId(reportSumID)); 
          		con.setPageSize(size);
                noOfRecords = con.getResultSize();
            }            
            return con;
        }
        set; 
    }
 	/*Constructor - Start*/
    public EFLSelfReportingWizardController(ApexPages.StandardController controller) {
       
        //Initialize boolean variables
        submitBool = False;
        cbiValidation = false;
        PrePlantingview = false;
        checkReportTypes();
        //Initialize pagination
        paginationSizeOptions = getPaginationSizeOptions();
        locationKeyPrefix = Location__c.sObjectType.getDescribe().getKeyPrefix();  
        //Load Self Reports by Report Summary Id
        if(reportSumID ==null){
                selfReportingList = EFLSelfReportingRepository.selfRepListByRSId(reportSumID);
        }else{
            selfReportingList = getselfRepList();
        }
    }
  	/*Constructor - End*/
    //Search Self Reports
    public void searchLocations(){
        try{
            if (String.isBlank(searchString)){
                con= new ApexPages.StandardSetController(EFLSelfReportingRepository.selfRepListByRSId(reportSumID));
                con.setPageSize(size);  
                noOfRecords = con.getResultSize();
                
            } else {
                selfReportingList = new List<Self_Reporting__c>();
                con= new ApexPages.StandardSetController(EFLSelfReportingRepository.searchSRByKeywordWithInReportSumID(reportSumID, searchString));     
                con.setPageSize(size);  
                noOfRecords = con.getResultSize();
                for (Self_Reporting__c sr : searchedSRs){
                    selfReportingList.add(sr);	
                }
            }
            
        }catch(Exception ex)  {
            System.debug(LoggingLevel.DEBUG,'Error caught while searching: '+ex.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'errorStr:' +ex.getMessage()));
        }   
    }    
    public List<Self_Reporting__c> searchedSRs {
        get
        {
            List<Self_Reporting__c> srList = EFLSelfReportingRepository.searchSRByKeywordWithInReportSumID(reportSumID, searchString);
            con = new ApexPages.StandardSetController(srList);
            con.setPageSize(size);  
            noOfRecords = con.getResultSize();
            List<Self_Reporting__c> resultSearchedsrList = new List<Self_Reporting__c>();
            for (Self_Reporting__c sr : (List<Self_Reporting__c>) con.getRecords()){            
                resultSearchedsrList.add(sr);
            }
            return resultSearchedsrList;
        }
    } 
    //SelfReportList bound to the page
    public List<Self_Reporting__c> getselfRepList() {
         return (List<Self_Reporting__c>) con.getRecords();
    }
    //Toggle certify box for submisssion
    public void callToggleCheckBox() {
        integer cbiCount = 0;
        integer cbiDelCount = 0;
        if(rs != null){
            Report_Summary__c temp = [SELECT Id, CBI_Count__c, CBI_Deleted_Count__c FROM Report_Summary__c WHERE Id=:rs.Id LIMIT 1];
            cbiCount = !String.isBlank(temp.CBI_Count__c) ? integer.valueOf(temp.CBI_Count__c) :0;
            cbiDelCount = !String.isBlank(temp.CBI_Deleted_Count__c) ? integer.valueOf(temp.CBI_Deleted_Count__c) :0;
        }
        if(submitBool){
            submitBool = false;
        }else{
            submitBool = True;
        }
       	if(cbiCount != cbiDelCount){
            cbiValidation = true;
        }else{
            cbiValidation = false;
        }
       	
    }
   //Create Report Summary
   public Report_Summary__c createReportSummary(){
        EFLEnforceAccessUtility.checkObjectInsertAccess('Report_Summary__c');
        Report_Summary__c reportSummary = new Report_Summary__c(Authorization__c = authorizationId,Report_Type__c = reportTypeMap.get(reportType), Status__c = 'UnSubmitted');
        insert reportSummary;
        repSummName = reportSummary.Name; // Added for W-036708
        return reportSummary;
    }     
   //Edit Self Report
   public void editForm() {
       newReportSummaryId = null;
       selfReporting = new Self_Reporting__c();
       selfReporting = EFLSelfReportingRepository.selfRepById(selfReportId);
  	
       if(selfReporting.id != null) {
           PrePlantingview = true;
       }
       checkReportTypes();
    
   }
    //Delete Self Report
    public PageReference deleterSsR() {
        if(getselfRepList().size()>1){
            EFLGenericUtility.deleteRecord(selfReportId);
            getselfRepList();  
            PageReference EFLSelfReportingWizard = new PageReference('/apex/EFLSelfReportingWizard');
            EFLSelfReportingWizard.setRedirect(true);
            EFLSelfReportingWizard.getParameters().put('authId', authorizationId);
            EFLSelfReportingWizard.getParameters().put('reportType', reportType);
            EFLSelfReportingWizard.getParameters().put('rsId', reportSumId);
            return EFLSelfReportingWizard;
        }else{
            EFLGenericUtility.deleteRecord(selfReportId);
            EFLGenericUtility.deleteRecord(reportSumId);
            
            PageReference EFLReportSummary  = Page.EFLReportSummary;
            EFLReportSummary.setRedirect(true);
            EFLReportSummary.getParameters().put('authId', authorizationId);
            EFLReportSummary.getParameters().put('reportType', reportType);
            return EFLReportSummary;
        }
        return null;
   }
   //Save Self Report
   public pageReference save() {
       try {
           EFLEnforceAccessUtility.checkObjectUpdateAccess('Self_Reporting__c');
           EFLEnforceAccessUtility.checkObjectInsertAccess('Self_Reporting__c');
           if(reportSumID == null){
               newReportSummaryId =  createReportSummary().Id;
               selfReporting.Report_Summary__c = newReportSummaryId;
               ApexPages.currentPage().getParameters().put('rsId', newReportSummaryId);
           }else if (selfReporting.Report_Summary__c == null){
               selfReporting.Report_Summary__c = reportSumID;
           }
           
           selfReporting.Authorization__c = authorizationId;
           selfReporting.RecordTypeId = recordTypeId;
           upsert selfReporting;
   
           return cancel();
       }catch(DmlException ex){
         // ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, e.getMessage()));
       }
       return null;
    } 
	//Check Report Type and set related boolean variables
    private void checkReportTypes() {
        if(reportTypeName == CARPOL_Constants.CLEANING_NOTICE){
              Cleaningview = True;
          }
          if(reportTypeName == CARPOL_Constants.HARVEST_DESTRUCT){
            PreHarvestview = True;
        }
      
    }
    //Add Self Report
    public void addform() {
        PrePlantingview = true;
        checkReportTypes();
        selfReporting = new Self_Reporting__c();
  
    }
 	//Submit Self Report
    public Pagereference doSubmit() {
        if(rs != null && rs.id != null) {
            EFLSelfReportingHelper EFLSelfReportHelper = new EFLSelfReportingHelper();
            EFLSelfReportHelper.submitFeildTestReport(rs);
        } 
        return new Pagereference('/apex/EFLReportSummary?authId='+authorizationID);
    }
    //Cancel & return to Self Report list
    public pagereference cancel(){
        PrePlantingView=false;
     
        PageReference EFLSelfReportingWizard = new PageReference('/apex/EFLSelfReportingWizard');
        EFLSelfReportingWizard.setRedirect(true);
        EFLSelfReportingWizard.getParameters().put('authId', authorizationId);
        EFLSelfReportingWizard.getParameters().put('reportType', reportType);
        if(newReportSummaryId!=null)
        { 
            EFLSelfReportingWizard.getParameters().put('rsId', newReportSummaryId);
            selfReportingList = EFLSelfReportingRepository.selfRepListByRSId(newReportSummaryId);
        }
        else
        {
            EFLSelfReportingWizard.getParameters().put('rsId', reportSumId);
            selfReportingList = EFLSelfReportingRepository.selfRepListByRSId(reportSumID);
        }
        
        return EFLSelfReportingWizard;
    }

    //Sets the size of pagination
    public PageReference refreshPageSize() {
        con = null;
        getselfRepList();
        con.setPageNumber(1);
        con.setPageSize(size);
        return null;
    }
	//SpringCM Utility
    public String fireDocLauncher(){
		String URL = EFLSpringCMUtility.buildDocLauncherURL(reportSumID, rs.Name, '', 'Self Report Attachment');
        return URL;
    }
 
}