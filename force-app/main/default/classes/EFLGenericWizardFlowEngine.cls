public class EFLGenericWizardFlowEngine implements EFLIWizardFlowEngine {
    
    
    public void getNextQuestion(EFLWizardFlow wizardFlow){
        
        EFLWizardActivity wizardActivity;
        String activityResult;
        if(wizardFlow.QuestionList.size() > 0 )
        {
            //Is the current question last of the questions already answered
            if(wizardFlow.currentQuestion.question.id == wizardFlow.QuestionList[wizardFlow.QuestionList.size()-1].question.id) 
            {
                EFLIWizardActivityProcessor activityProcessor= EFLWizardActivityFactory.getActivityProcessor(wizardFlow.currentQuestion);
                //TODO: Activity Processor is not configured for movement and Pathway
                activityProcessor.evaluateResult(wizardFlow.currentQuestion);
                EFLWizardFlowUtility.getNextUnansweredQuestion(wizardFlow) ;               
            }
            
            else
            {
                //Current question is not the last question the user answered
                //get the next answered question form the list of answered questions
                wizardActivity = wizardFlow.QuestionList[wizardFlow.currentQuestionIndex];
                wizardFlow.currentQuestionIndex += 1;
            }
        }
        else 
        {
            wizardFlow.currentQuestionIndex = 0;
            Wizard_Questionnaire__c startQuestion = EFLPSQUtility.getStartQuestion();            
            List<Wizard_Selections__c> questionOptions = EFLPSQUtility.getQuestionOptions(startQuestion.ID,'', null );
            System.debug('questionOptions: ' + questionOptions);
            wizardActivity = new EFLWizardActivity(wizardFlow,startQuestion,questionOptions); 
            
            if(wizardActivity != null){
                wizardFlow.QuestionList.add(wizardActivity);
                wizardFlow.currentQuestionIndex += 1;
            }
            
        } 
        wizardFlow.currentQuestion = wizardActivity;
        
    }
    public void getPrevQuestion(EFLWizardFlow wizardFlow){
        
    }
    public List<SelectOption> getQuestionOptions (EFLWizardFlow wizardFlow){
        return null;
    }
    public void setSelectedOption ( string selectedOption){
        
    }
    
}