@isTest(seealldata=true)
public class EFLToggleApplnPrvcyCnfrm_Test
{
   public static testMethod void EFLToggleApplnPrvcyCnfrm_method1()
    {                                 
        System.Test.StartTest();
        
        
        //Creating Account test data        
        //Id RTId = [select id From recordtype where name = 'Accounts'].Id;  
      string AccountRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
        Account objacct = new Account();
        objacct.Name = 'Global Account';
        objacct.RecordTypeId = AccountRecordTypeId;    
        insert objacct;   
        
        //Creating Contact test data
        Contact objCont = new Contact();
        objCont.FirstName = 'Global Contact';
        objcont.LastName = 'LastName';
        objcont.Email = 'test@email.com';        
        objcont.AccountId = objacct.id;
        objcont.MailingStreet = 'Mailing Street';
        objcont.MailingCity = 'Mailing City';
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '32092';    
        insert objcont;     
        //Creating User test data with contactID as Contact test data id
        Profile prof = [Select Id from Profile WHERE Name IN ('APHIS Applicant','eFile Applicant')Limit 1];
        User custusr = new User(Alias='stndt',email='testUser@gmail.com',EmailEncodingKey='UTF-8', 
            LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prof.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser2@abc.com',ContactID=objcont.Id);
        insert custusr;

        //Creating a group with name = Account test data name
        Group grp = new Group(Name='Global Account');
        insert grp;
       
        //Creating Application_Status__c test data with private application = true
        Application__c objapp = new Application__c();
        objapp.Applicant_Name__c = objcont.Id;
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
        objapp.Recordtypeid = ACAppRecordTypeId;
        objapp.Application_Status__c = 'Open';
        insert objapp;
        
                
        PageReference pageRef = Page.EFLToggleApplnPrvcyCnfrm; 
        System.Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', String.valueOf(objapp.Id));
        EFLToggleApplnPrvcyCnfrm inst = new EFLToggleApplnPrvcyCnfrm();        
        
        inst.homeRedirect();
        
        
        
        System.runAs(custusr)
        {
            inst.callingPage = 'EFLToggleApplnPrvcyCnfrm';
            inst.toggleApplicationPrivacy();
        }
        System.Test.StopTest();
        
    }
    
    public static testMethod void EFLToggleApplnPrvcyCnfrm_method2()
    {                
        System.Test.StartTest();
        
        //Creating Account test data        
        //Id RTId = [select id From recordtype where name = 'Accounts'].Id;  
       string AccountRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
        Account objacct = new Account();
        objacct.Name = 'Global Account';
        objacct.RecordTypeId = AccountRecordTypeId;    
        insert objacct;    
        
        //Creating Contact test data
        Contact objCont = new Contact();
        objCont.FirstName = 'Global Contact' ;
        objcont.LastName = 'LastName' ;
        objcont.Email = 'test@email.com';        
        objcont.AccountId = objacct.id;
        objcont.MailingStreet = 'Mailing Street' ;
        objcont.MailingCity = 'Mailing City' ;
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '32092';    
        insert objcont;
        
        //Creating Application_Status__c test data with private application = false
        Application__c objapp = new Application__c();
        objapp.Applicant_Name__c = objcont.Id;
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
        objapp.Recordtypeid = ACAppRecordTypeId;
        objapp.Application_Status__c = 'Open';
        insert objapp;
        
                
        PageReference pageRef = Page.EFLToggleApplnPrvcyCnfrm; 
        System.Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', String.valueOf(objapp.Id));
        
        EFLToggleApplnPrvcyCnfrm inst = new EFLToggleApplnPrvcyCnfrm();
        
        System.Test.StopTest();
    }
}