public inherited sharing class EFLBRSAuthCheckStatusUtility {
    public static List<EFLAuthStatusValidation__mdt> statusFromCMD;
    public static Map<string,list<string>> statusMap = new Map<string,list<string>>();
    public static Map<string,string> errorMap = new Map<string,string>();
    public static Map<string,AC__c> authLineItemMap = new Map<string,AC__c>();
    public static Map<string,list<Link_Regulated_Articles__c>> lineItemRegArticlesMap = new Map<string,list<Link_Regulated_Articles__c>>();
    public static Map<string,list<Construct__c>> lineItemConstructsMap = new Map<string,list<Construct__c>>();
    public static Map<string,list<Construct_Application_Junction__c>> lineItemDesignProtoMap = new Map<string,list<Construct_Application_Junction__c>>();
    public static Map<string,list<Location__c>> lineItemLocationMap = new Map<string,list<Location__c>>();
    public static Map<string,list<Applicant_Attachments__c>> lineItemAttachmentsMap = new Map<string,list<Applicant_Attachments__c>>();
    public static list<string> statusSet = new list<string>();
      
    public static boolean validateStatusForStageChange(Authorizations__c auth){
        string statusLabel = system.Label.Validation_Statuses;
        list<string> statuses = statusLabel.split('\\,');
        //system.debug('Statuses: ' + statuses);
        
        list<AC__c> lineItemList = [select id,Authorization__c,
        	(select id from Link_Regulated_Articles__r where status__c In :statuses),
            (select id, status__c from Article_Suppliers__r where status__c In :statuses AND Program__c != 'AC'),
        	(select id from Constructs__r where status__c In :statuses),
        	(select id from Link_Construct_Design_Protocol_Records__r where status__c In :statuses),
        	(select id from Locations__r where status__c In :statuses),
        	(select id from Applicant_Attachments__r where status__c In :statuses) from AC__c where Authorization__c = :auth.id];
        //system.debug('Line iTem list *** : ' + lineItemList);
        //system.debug('Constructs__r: ' + lineItemList[0].Constructs__r);
        	
        	if(lineItemList!=null && lineItemList.size()>0){
	            if(lineItemList[0].Constructs__r!=null && lineItemList[0].Constructs__r.size()>0){
                    //system.debug('Constructs__r: ' + lineItemList[0].Constructs__r);
	              	return true;
	            }        		
	            if(lineItemList[0].Link_Regulated_Articles__r!=null && lineItemList[0].Link_Regulated_Articles__r.size()>0){
                    //system.debug('Link_Regulated_Articles__r: '+ lineItemList[0].Link_Regulated_Articles__r);
	               return true;
	            }
                if(lineItemList[0].Article_Suppliers__r!=null && lineItemList[0].Article_Suppliers__r.size()>0){
                    //system.debug('Article_Supplier_Developer__c :' + lineItemList[0].Article_Suppliers__r);
	              	return true;
	            }
	            if(lineItemList[0].Link_Construct_Design_Protocol_Records__r!=null && lineItemList[0].Link_Construct_Design_Protocol_Records__r.size()>0){
	               //system.debug('Link_Construct_Design_Protocol_Records__r');
                    return true;
	            }
	            if(lineItemList[0].Locations__r!=null && lineItemList[0].Locations__r.size()>0){
                    //system.debug('Locations__r');
	              	return true;
	            }
	            if(lineItemList[0].Applicant_Attachments__r!=null && lineItemList[0].Applicant_Attachments__r.size()>0){
                    //system.debug('Applicant_Attachments__r');
		           return true; 
	            }
                
                //W-036993 need to query for the linked record and make sure theyre review complete
                if (auth.Stage__c == EFLGlobalConstants.getConstantValue('AUTHORIZATION STAGE APPLICATION REVIEW') ) {
                    list<AC__c> lineItems = [select id,Authorization__c
                                             from AC__c 
                                             where Authorization__c = :auth.id];
                    Set<String> lineItemIds = new Set<String>();
                    for(AC__c lineItem : lineItems){
                        lineItemIds.add(lineItem.Id);
                   
                    }
                    system.debug('Line item IDs Set ****:  '+ lineItems);
                    
                    List<Link_Regulated_Articles__c> regulatedArticles = [SELECT Id, status__c 
                                                                          FROM Link_Regulated_Articles__c 
                                                                          WHERE Line_Item__c IN :lineItemIds
                                                                          AND Status__c != 'Review Complete'];                    
                    system.debug('regulatedArticles: '+ regulatedArticles);
                    if (regulatedArticles.size() > 0){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('AuthorizationLinesNotComplete')));
                        return true;
                    }
                    List<Article_Supplier_Developer__c> ASD = [SELECT Id 
                                                               FROM Article_Supplier_Developer__c 
                                                               WHERE Line_Item__c IN :lineItemIds
                                                               AND Program__c != 'AC'
                                                               AND Status__c != 'Review Complete'];             
                    system.debug('Aricle Supplier Developer: '+ ASD);                   
                    if (ASD.size() > 0){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('AuthorizationLinesNotComplete')));
                        return true;
                    }
                    List<Construct__c> constructs = [SELECT Id 
                                                     FROM Construct__c 
                                                     WHERE Line_Item__c IN :lineItemIds
                                                     AND Status__c != 'Review Complete'];             
                    system.debug('constructs: '+ constructs);                   
                    if (constructs.size() > 0){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('AuthorizationLinesNotComplete')));
                        return true;
                    }
                    
                    List<Construct_Application_Junction__c> previouslySubmittecConstructs = [SELECT Id 
                                                                                             FROM Construct_Application_Junction__c 
                                                                                             WHERE Line_Item__c IN :lineItemIds
                                                                                             AND Status__c != 'Review Complete'];              
                    system.debug('previouslySubmittecConstructs: '+ previouslySubmittecConstructs);                  
                    if (previouslySubmittecConstructs.size() > 0){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('AuthorizationLinesNotComplete')));
                        return true;
                    }
                    
                    List<Location__c> locations = [SELECT Id 
                                                   FROM Location__c 
                                                   WHERE Line_Item__c IN :lineItemIds
                                                   AND Status__c != 'Review Complete'];                 
                    system.debug('locations: '+ locations);               
                    if (locations.size() > 0){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('AuthorizationLinesNotComplete')));
                        return true;
                    }
                }
        }
        return false;
    }
    
    public static void updateLineItemsFromAuthorizations(list<Authorizations__c> waitingAuthList){
        list<Application__c> appList = new list<Application__c>();
        list<AC__c> lineItemList = new list<AC__c>();
        for(Authorizations__c auth : [select id,status__c,Application__r.Application_Status__c,(select id,status__c,Application_Number__r.Application_Status__c from Animal_Care_AC_Authorization__r) from Authorizations__c where id IN:waitingAuthList]){
            if(auth.Animal_Care_AC_Authorization__r!=null && auth.Animal_Care_AC_Authorization__r.size()>0){
                appList.add(new Application__c(id=auth.Application__r.id,Application_Status__c=auth.Status__c,Application_Submitted__c=true));
                for(AC__c lineItems : auth.Animal_Care_AC_Authorization__r){
              lineItems.Status__c =  auth.Status__c; 
              lineItems.Application_Number__r.Application_Status__c=auth.Status__c;
            }
             lineItemList.add(auth.Animal_Care_AC_Authorization__r);   
            }
        }
        if(lineItemList.size()>0){
        update lineItemList;
        }
        if(appList.size()>0){
        update appList;
        }
    }
    
    public static void getDataForAuthStatusValidation(list<authorizations__c> authorizationList){
        for(AC__c lineItem : [select id,Authorization__c,
        	(select id,Status__c from Link_Regulated_Articles__r),
        	(select id,Status__c from Constructs__r ),
        	(select id,Status__c from Link_Construct_Design_Protocol_Records__r ),
        	(select id,Status__c from Locations__r),
                              (select id,Status__c from Applicant_Attachments__r) from AC__c where Authorization__c IN:authorizationList]){
            authLineItemMap.put(lineItem.Authorization__c, lineItem);
                                  if(lineItem.Link_Regulated_Articles__r!=null && lineItem.Link_Regulated_Articles__r.size()>0){
            lineItemRegArticlesMap.put(lineItem.Authorization__c, lineItem.Link_Regulated_Articles__r);
                                      system.debug('lineItemRegArticlesMap '+lineItemRegArticlesMap);
                                  }
                                  if(lineItem.Constructs__r!=null && lineItem.Constructs__r.size()>0){
                                      lineItemConstructsMap.put(lineItem.Authorization__c, lineItem.Constructs__r);////system.debug('lineItemConstructsMap '+lineItemConstructsMap);
                                  }
                                  if(lineItem.Link_Construct_Design_Protocol_Records__r!=null && lineItem.Link_Construct_Design_Protocol_Records__r.size()>0){
                                      lineItemDesignProtoMap.put(lineItem.Authorization__c, lineItem.Link_Construct_Design_Protocol_Records__r);////system.debug('lineItemDesignProtoMap '+lineItemDesignProtoMap);
                                  }
                                   if(lineItem.Locations__r!=null && lineItem.Locations__r.size()>0){
                                      lineItemLocationMap.put(lineItem.Authorization__c, lineItem.Locations__r);////system.debug('lineItemLocationMap '+lineItemLocationMap);
                                   }
                                  if(lineItem.Applicant_Attachments__r!=null && lineItem.Applicant_Attachments__r.size()>0){
                                       lineItemAttachmentsMap.put(lineItem.Authorization__c, lineItem.Applicant_Attachments__r);////system.debug('lineItemAttachmentsMap '+lineItemAttachmentsMap);
                                  }
        } 
    }
    
    public static void validateAuthLineItemRelatedStatus(authorizations__c newAuth){
        statusFromCMD = [SELECT MasterLabel, Statuses__c, Message__c FROM EFLAuthStatusValidation__mdt];
        
        for(EFLAuthStatusValidation__mdt m :  statusFromCMD){
            statusMap.put(m.MasterLabel,m.Statuses__c.split('\\,'));
            errorMap.put(m.MasterLabel,m.Message__c);
        }
        //system.debug('validateAuthLineItemRelatedStatus');
        boolean statusRecFound=false;
        statusSet = statusMap.get(newAuth.status__c);
        if(!lineItemRegArticlesMap.isEmpty() && lineItemRegArticlesMap.containsKey(newAuth.id) && !statusRecFound){
	        for(Link_Regulated_Articles__c lra : lineItemRegArticlesMap.get(newAuth.id)){
	            if(!statusSet.contains(lra.Status__c)){
	              statusRecFound =true;
	                break;
	            }
	        }
        }
        if(!lineItemConstructsMap.isEmpty() && lineItemConstructsMap.containsKey(newAuth.id) && !statusRecFound){
	        for(Construct__c cons : lineItemConstructsMap.get(newAuth.id)){
	            if(!statusSet.contains(cons.Status__c)){
	              statusRecFound =true;
	                break;  
	            }
	        }
        }
         if(!lineItemDesignProtoMap.isEmpty() && lineItemDesignProtoMap.containsKey(newAuth.id) && !statusRecFound){
	        for(Construct_Application_Junction__c caj : lineItemDesignProtoMap.get(newAuth.id)){
	            if(!statusSet.contains(caj.Status__c)){
	              statusRecFound =true;
	                break;  
	            }
	        }
         }
        if(!lineItemLocationMap.isEmpty() && lineItemLocationMap.containsKey(newAuth.id) && !statusRecFound){
	        for(Location__c loc : lineItemLocationMap.get(newAuth.id)){
	            if(!statusSet.contains(loc.Status__c)){
	              statusRecFound =true;
	                break;  
	            }
	        }
        }
        if(!lineItemAttachmentsMap.isEmpty() && lineItemAttachmentsMap.containsKey(newAuth.id) && !statusRecFound){
	        for(Applicant_Attachments__c aa : lineItemAttachmentsMap.get(newAuth.id)){
	            if(!statusSet.contains(aa.Status__c)){
	              statusRecFound =true;
	                break;  
	            }
	        }
        }
        if(statusRecFound){
         newAuth.addError(errorMap.get(newAuth.status__c));   
        }
    }
    //validate the inspection status which is associated with Authrization
    public static boolean validateInspectionStatus(authorizations__c auth){
        Boolean isInValid = true;
        //Get all related Inpections for the authorization.
        List<Inspection__c> relatedInspectionList = [select id 
                                                     from Inspection__c 
                                                     where authorization__c =:auth.id 
                                                     limit 100];
       if(relatedInspectionList.size() > 0){     
            list<AuthInspectionStatusValidation__mdt> inspectionStageStatuses =[select id,InspectionStage__c,InspectionStatus__c from AuthInspectionStatusValidation__mdt]; 
            Map<String, Set<AuthInspectionStatusValidation__mdt>> inspectionStageStatusMap= new Map<String, Set<AuthInspectionStatusValidation__mdt>>();
            for(AuthInspectionStatusValidation__mdt inspectionStageStatus : inspectionStageStatuses){
                if (!inspectionStageStatusMap.containsKey(inspectionStageStatus.InspectionStage__c)){
                    Set<AuthInspectionStatusValidation__mdt> newSet = new Set<AuthInspectionStatusValidation__mdt>();
                    newSet.add(inspectionStageStatus);
                    inspectionStageStatusMap.put(inspectionStageStatus.InspectionStage__c, newSet);
                }else {
                    Set<AuthInspectionStatusValidation__mdt> existingSet = inspectionStageStatusMap.get(inspectionStageStatus.InspectionStage__c);
                    existingSet.add(inspectionStageStatus);
                    inspectionStageStatusMap.put(inspectionStageStatus.InspectionStage__c, existingSet);
                }
            }
            for(Inspection__c ins : [select id,Status__c from Inspection__c where authorization__c =:auth.id and Status__c!=null ]){
                //If the metadata contains the current auth stage and the current inspection status then this is not invalid
                if(inspectionStageStatusMap.containsKey(auth.Stage__c)){
                    system.debug('Inspection Stage Statuses: ' + inspectionStageStatusMap.get(auth.Stage__c));
                    for (AuthInspectionStatusValidation__mdt inpectionStageStatus : inspectionStageStatusMap.get(auth.Stage__c)){
                        system.debug('InspectionStageStatus: ' + inpectionStageStatus);
                        system.debug('Inspection Status: '+ins.Status__c);
                        if (inpectionStageStatus.InspectionStatus__c == ins.Status__c){
                            isInValid = false;
                            break; 
                        }
                    }
                }
            }
        
    }else{
        //Setting it to false when there are no inspections available for the authorization.
          isInValid = false;
    }
        return isInValid;
        
    }
}