public class PrevSOP_Con_ChangeHistoryHandler implements IChangeHistoryHandler {
    
    private string fieldName ;
    private string fieldLabel ;
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        overwriteDefaultAssignments(newRecord,change);
        setFieldIdentifiers(newRecord);        
        Schema.SObjectType sObjectType = newRecord.getSObjectType();
        if (sObjectType != null)
        {
            //record identifier
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
               	Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        overwriteDefaultAssignments(newRecord,change);
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {
        overwriteDefaultAssignments(deletedRecord,change);
        setFieldIdentifiers(deletedRecord); 
        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.Old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
        	populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    private void overwriteDefaultAssignments(Sobject record, Change_History__c change)
    {
        Construct_Application_Junction__c caj = (Construct_Application_Junction__c)record;
        if(caj.Design_Protocol_Record_SOP__c != null)
            change.Object_Name1__c = 'Previously Reviewed SOP';
        else if(caj.Construct__c != null)
            change.Object_Name1__c = 'Previously Reviewed Construct';        
    }
    
    private void setFieldIdentifiers(Sobject record)
    {
        Construct_Application_Junction__c caj = (Construct_Application_Junction__c)record;
        if(caj.Design_Protocol_Record_SOP__c != null)
        {
            fieldName = 'SOP_Name__c';
            fieldLabel = 'SOP Name';
        }
        else if(caj.Construct__c != null)
        {
            fieldName = 'Construct_Name__c';
            fieldLabel = 'Construct Name';
        }
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        Construct_Application_Junction__c caj = (Construct_Application_Junction__c)record;
        change.Line_Item__c = caj.Line_Item__c; 
		//change.Application__c = caj.Application__c;
		change.Construct__c = caj.Construct__c;
        if(!change.Delete_Flag__c)change.Construct_Application_Junction__c = caj.id;
    }

}