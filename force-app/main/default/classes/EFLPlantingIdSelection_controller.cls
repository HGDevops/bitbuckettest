public with sharing class EFLPlantingIdSelection_controller {
    
    public Id InspectionId{get;set;}
    public Id plantingrecordtype{get;set;}
    public Inspection__c InspRecord = new Inspection__c();
    public List<Location__c> LocationList = new List<Location__c>();
    public String AuthId;
    public List<cLocation> SelfReportDisplayList {get;set;}
    public string releasestartdate{get;set;}
    public string releaseenddate{get;set;}
    
    public List< Self_Reporting__c > PlantingIdList = new List< Self_Reporting__c >();
    
    public EFLPlantingIdSelection_controller(ApexPages.StandardController controller) {
        InspectionId= EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('id') );
        try{
            if(InspectionId!=null || InspectionId!=''){
                InspRecord = [SELECT Id,Name,Authorization__c FROM Inspection__c WHERE Id=:InspectionId LIMIT 1];
            }
            
            AuthId = InspRecord.Authorization__c;
            
            //plantingrecordtype = [SELECT id from Recordtype where sobjectType='Self_Reporting__c' and developername='Planting_Report'].id;
            if(InspRecord.Authorization__c!=null || InspRecord.Authorization__c!=''){
                AuthId = InspRecord.Authorization__c;
                
                AuthId = AuthId.substring(0,AuthId.length()-3);
                LocationList = [SELECT ID,Name, State__c, Status__c, Status_Graphical__c, Inspection_Status__c FROM Location__c WHERE Authorization_Id__c =:AuthId];
                PlantingIdList = [SELECT ID,Name, Planting_ID__c,Release_Record_ID__c,Start_Date__c, Anticipated_Harvest_Destruct_Date__c, Quantity_Acres__c, Construct_Name__c,
                                  GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,GPS_Coordinates_6_Text__c,
                                  Release_Record_ID__r.Street_Add1__c, Release_Record_ID__r.Street_Add2__c, Release_Record_ID__r.Street_Add3__c, Release_Record_ID__r.Street_Add4__c,  
                                  Release_Record_ID__r.State__r.Name, Release_Record_ID__r.Country_Text__c, Release_Record_ID__r.Zip__c,
                                  Release_Record_ID__r.County_Text__c, Release_Record_ID__r.City__c, Release_Record_ID__r.Proposed_start_date__c,
                                  Release_Record_ID__r.Proposed_end_date__c, Release_Record_ID__r.Proposed_Planting__c, Release_Record_ID__r.Number_of_Acres_Text__c,
                                  Release_Record_ID__r.Location_Unique_Id__c, Release_Record_ID__r.GPS_Coordinates_2_Text__c,Release_Record_ID__r.GPS_Coordinates_3_Text__c,Release_Record_ID__r.GPS_Coordinates_4_Text__c,Release_Record_ID__r.GPS_Coordinates_5_Text__c,Release_Record_ID__r.GPS_Coordinates_6_Text__c,
                                  Line_Item__c, Line_Item__r.Release_Start_Date__c, Line_Item__r.Release_end_Date__c,
                                  Release_Record_ID__r.GPS_Coordinates_1__Latitude__s, Release_Record_ID__r.GPS_Coordinates_2__Latitude__s, Release_Record_ID__r.GPS_Coordinates_3__Latitude__s, Release_Record_ID__r.GPS_Coordinates_4__Latitude__s, Release_Record_ID__r.GPS_Coordinates_5__Latitude__s, Release_Record_ID__r.GPS_Coordinates_6__Latitude__s, 
                                  Release_Record_ID__r.GPS_Coordinates_1__Longitude__s,Release_Record_ID__r.GPS_Coordinates_2__Longitude__s,Release_Record_ID__r.GPS_Coordinates_3__Longitude__s,Release_Record_ID__r.GPS_Coordinates_4__Longitude__s,Release_Record_ID__r.GPS_Coordinates_5__Longitude__s,Release_Record_ID__r.GPS_Coordinates_6__Longitude__s,
                                  Release_Record_ID__r.GPS_Coordinates_1_Text__c,
                                  Release_Record_ID__r.Line_Item__r.Release_Start_Date__c,Release_Record_ID__r.Line_Item__r.Release_end_Date__c,
                                  Release_Record_ID__r.Release_History__c,
                                  Release_Record_ID__r.Critical_Habitat_Involved__c,Release_Record_ID__r.If_Yes_Please_Explain__c
                                  FROM Self_Reporting__c WHERE Release_Record_ID__r.Authorization_Id__c =:AuthId];
                //FROM Self_Reporting__c WHERE Release_Record_ID__r.Authorization_Id__c =:AuthId and Authorization__c =:AuthId and Is_Submitted__c= true and RecordTypeId=:plantingrecordtype];
                
                //system.debug('PlantingIdList[0].Line_Item__r.Release_Start_Date__c>>'+PlantingIdList[0].Release_Record_ID__r.Line_Item__r.Release_Start_Date__c);
                if(PlantingIdList[0].Release_Record_ID__r.Line_Item__r.Release_Start_Date__c!=null& PlantingIdList[0].Release_Record_ID__r.Line_Item__r.Release_end_Date__c!=null){
                    Date releasestart = PlantingIdList[0].Release_Record_ID__r.Line_Item__r.Release_Start_Date__c;
                    releasestartdate = releasestart.format();
                    Date releaseend = PlantingIdList[0].Release_Record_ID__r.Line_Item__r.Release_Start_Date__c;
                    releaseenddate =releaseend.format();
                    //system.debug('st>>>>'+releasestartdate);
                    //system.debug('en'+releaseenddate);
                }       
                if(LocationList.size()==0){
                    ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Info,+'No Locations associated to this Authorization'));
                }
            }else{
                List<EFL_Related_Record__c> RRList = [SELECT ID,Name,Inspection__c,Location__C FROM EFL_Related_Record__c WHERE Inspection__c=:InspectionId];
                List<Id> LocIds = new List<ID>();
                for(EFL_Related_Record__c R: RRList){
                    LocIds.add(r.Location__c);
                }
                LocationList = [SELECT ID,Name, State__c, Status__c, Status_Graphical__c, Inspection_Status__c FROM Location__c WHERE ID IN:LocIds];
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Info,+'No Authorization associated'+e));
        } 
        
        
    }
    
    public void processSelected(){
        List<Self_Reporting__c> selectedLocations = new List<Self_Reporting__c>();
        for (cLocation cq:getLocs()){
            if(cq.selected == true){
                selectedLocations.add(cq.Loc);
            }
        }
        
        List<EFL_Inspection_Questionnaire__c> inspQList = new List<EFL_Inspection_Questionnaire__c>();
        if(selectedLocations.size()>0){
            for(Self_Reporting__c q: selectedLocations){
                EFL_Inspection_Questionnaire__c inspq = new EFL_Inspection_Questionnaire__c();
                inspq.Location__c = q.Release_Record_ID__c;
                inspq.Status__c = 'Open';
                inspq.Inspection__c = InspectionId;
                inspq.Planting_Id__c=q.id;
                inspQList.add(inspq);
                
                //q.Inspection_Status__c = 'Initiated';
                
            }
            
            if(inspQList.size()>0){
                //update selectedLocations;
                insert inspQList;
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,+'Successfully created Inspection Questionnaire records'));
                
            }
            //update selectedQuotes;
            
            
            
        }else{
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Error,+'Please select atleast 1 record to update'));
            //return null;
        }
        //QuotesDisplayList=null;
        //return null;
    }
    
    public List<cLocation> getLocs(){
        if(SelfReportDisplayList==null && PlantingIdList!=null){
            SelfReportDisplayList = new List<cLocation>();
            for(Self_Reporting__c q:PlantingIdList){
                SelfReportDisplayList.add(new cLocation(q));
            }
        }
        
        return SelfReportDisplayList;
    }    
    
    
    public class cLocation {
        public Self_Reporting__c Loc {get; set;}
        public Boolean selected {get; set;}
        
        public cLocation(Self_Reporting__c c) {
            Loc = c;
            selected = false;
        }
    }
    
}