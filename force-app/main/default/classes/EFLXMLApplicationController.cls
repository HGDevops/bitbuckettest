public with sharing class EFLXMLApplicationController{
    public List<EFLXMLApplicationDataWapper> EFLXMLAppWapperList{get;set;}
    public string applicationtype {get;set;}
    public Boolean errormessage {get;set;}
    public string applicantName{get;set;} 
    public string aType ='Application';
    public Id applicant=null;
    public String reportType='';
    Id authId=null;
    public Application_Creation_Request__c acr{get;set;}
    public blob xmlbody { get; set; }
    public String fname {get;set;}
    Private String xmlAccountId;
    Private String xmlContactId;
    
    public EFLXMLApplicationController(){
        acr = new Application_Creation_Request__c();
        acr.Account__c = EFLUserUtility.userDetails.AccountId; //xmlAccountId;
        EFLXMLAppWapperList = new List<EFLXMLApplicationDataWapper>();
        showAllACR();
        ReportType();
    }
    // Added Method for W-026188 
    public void ReportType(){
        reportType= EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('reportType'));
        authId=(Id) EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('authId'));
        if(String.isBlank(reportType))
        {
            aType = 'Application';
        }
        else if (authId != null && !String.isBlank(reportType)){            
            aType = 'Self Reporting';            
            applicant = [select applicant__c from Authorizations__c where id =:authId limit 1 ].applicant__c;
            acr.ApplicantName__c = applicant;
            switch on reportType {
                when 'Planting_Report' {
                    applicationtype= 'Planting Report';
                }
                when 'Volunteer_Monitoring_Report'{
                    applicationtype= 'Volunteer Monitoring Report';
                }
                when 'Field_Test_Report' {
                    applicationtype= 'Field Test Report';
                }
            }            
        }        
    } 
    //Added for W-026188
    public PageReference backtoSummary(){
        PageReference MyNewPage;
        MyNewPage = Page.EFLReportSummary;
        MyNewPage.getParameters().put('authid',authId);
        MyNewPage.setRedirect(true);         
        return MyNewPage;
    } 
    public  PageReference uploadFile(){ 
        
        if(xmlbody !=null){
            ReportType();
            Application_Creation_Request__c appCreationReqObj = new Application_Creation_Request__c();
            if(applicationtype == null){
                errormessage=false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,' Please select if your application is for a Permit or a Notification'));
                return null;
            }
            if (acr.Account__c == null) {
                errormessage=false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,' Please select an Account Name'));
                return null;
            }
            else{        
                try{
                    
                    appCreationReqObj.Application_Type__c =applicationtype ;
                    appCreationReqObj.Type__c =aType;
                    appCreationReqObj.Status__c = 'New';
                    appCreationReqObj.ApplicantName__c = EFLUserUtility.userDetails.contactID;
                    appCreationReqObj.Account__c =acr.Account__c ;
                    insert appCreationReqObj;
                    id appId = appCreationReqObj.id;
                    
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.VersionData = xmlbody;
                    cv.Title = fname;
                    cv.PathOnClient = fname;
                    insert cv;
                    
                    // Query ContentVersion record for get ContentDocumentId.
                    ContentVersion cnt = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id limit 1];
                    // Insert ContentDocumentLink record.
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.ContentDocumentId = cnt.ContentDocumentId;
                    cdl.LinkedEntityId = appCreationReqObj.id;
                    cdl.ShareType = 'I';
                    cdl.Visibility = 'AllUsers';
                    insert cdl;
                    
                    Application_Creation_Request__c rec = new Application_Creation_Request__c(id = appId,Status__c = 'Ready');
                    upsert(rec);
                    system.debug('recStatus@@'+ rec.Status__c);
                    if(aType =='Application'){ errormessage=true;ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,' Application via XML Inserted SuccessFully'));}else{errormessage=true;ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,EFLGenericUtility.getMessage('EFLXMLSelfReportingUploadConfirm')));}
                    
                    applicationtype = '';
                    xmlbody = null;
                    PageReference MyNewPage;
                    if(aType =='Application'){
                        MyNewPage = Page.EFLXMLApplication;
                        MyNewPage.setRedirect(true);
                    }
                    else{
                        MyNewPage = Page.EFLXMLSelfReporting;
                        MyNewPage.getParameters().put('authid',authId);
                        MyNewPage.getParameters().put('reportType',reportType);
                        MyNewPage.setRedirect(true);
                    }
                    return MyNewPage;
                }catch(Exception e){
                    errormessage=false;
                    
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,EFLGenericUtility.getMessage('EFLXMLSelfReportingUploadInError')));
                    
                    return NULL;
                }
            }            
        }else{
            errormessage=false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,EFLGenericUtility.getMessage('EFLXMLSelfReportingUploadXMLMissing')));
            return null;
        }
    }
    private void showAllACR(){
        try{
            
            Map<id, Application_Creation_Request__c> appCreationReqObj = new 
                Map<id,Application_Creation_Request__c>([SELECT Id,Name, Status__c,Application_Type__c,Application__r.name,Application__c,ApplicantName__r.Name,Report_Summary__c,Report_Summary__r.Name 
                                                         FROM Application_Creation_Request__c 
                                                         ORDER BY Createddate DESC]);
            
            Set<Id> contectDocumentIds = new Set<Id>();
            Set<Id> acrIds = new Set<Id>();
            Map<Id, List<Id>> acrFileIdMap = new Map<Id, List<Id>>();
            acrIds = appCreationReqObj.keySet();
            string errorFileName='ErrorFile';
            string errorFile = '%' + errorFileName + '%';
            if(!acrIds.isEmpty()){            
                for(ContentDocumentLink contentDoc: [SELECT ContentDocumentId,LinkedEntityId 
                                                     FROM ContentDocumentLink 
                                                     WHERE LinkedEntityId IN: acrIds  ORDER BY SystemModstamp ASC ]){
                                                         //map.get linked entity id 
                                                         Application_Creation_Request__c appStatus = appCreationReqObj.get(contentDoc.LinkedEntityId);
                                                         contectDocumentIds.add(contentDoc.ContentDocumentId);
                                                         List<Id> documentIds = new List<Id>();
                                                         
                                                         documentIds.add(contentDoc.ContentDocumentId);
                                                         if(!documentIds.isEmpty()){                                       
                                                             acrFileIdMap.put(contentDoc.LinkedEntityId, documentIds);
                                                         }
                                                     }
                
                
                Map<id, ContentVersion> documentContentMap = new Map<id, ContentVersion>();
                for(ContentVersion contentVersion: [SELECT Id, Title, ContentDocumentId, CreatedDate
                                                    FROM ContentVersion 
                                                    WHERE ContentDocumentId =: contectDocumentIds
                                                    ORDER BY Createddate DESC]){
                                                        documentContentMap.put(contentVersion.ContentDocumentId, contentVersion);
                                                    }        
                for(Id acrId: acrFileIdMap.keySet()){
                    for(Id documentId: acrFileIdMap.get(acrId)){
                        EFLXMLAppWapperList.add(new EFLXMLApplicationDataWapper(documentContentMap.get(documentId), appCreationReqObj.get(acrId)));
                    }
                }
                EFLXMLAppWapperList.sort();
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        
    }
    
    // Use for show Upload History
    public class EFLXMLApplicationDataWapper implements Comparable {
        public String fileName{get;set;}
        public String status{get;set;}
        public Datetime uploadedDate{get;set;}
        //public String uploadedDate{get;set;}
        public ContentVersion errorFile{get;set;}
        public String appObj{get;set;}
        public id appObjId{get;set;}
        public id rptsumObjId{get;set;}
        public Application_Creation_Request__c appCreationReq{get;set;}
        public EFLXMLApplicationDataWapper(ContentVersion documentFile,Application_Creation_Request__c appCreationReq){
            this.fileName = documentFile.Title;
            this.status = 'Ready';
            this.uploadedDate =documentFile.CreatedDate;
            // Integer offset = UserInfo.getTimeZone().getOffset(uploadedDate);
            //uploadedDate = uploadedDate.addSeconds(offset/1000);
            
            uploadedDate=DateTime.valueOfGMT(String.valueOf(uploadedDate));
            //system.debug('uploadedDate@@'+uploadedDate);
            //String dateval =String.valueOf(uploadedDate);
            //system.debug('dateval@@'+dateval);
            // uploadedDate=uploadedDate.format(dateFormatString, timezone)
            //String uploadedDate =documentFile.CreatedDate.format('MM/dd/yyyy HH:mm:ss','America/New_York');
            this.errorFile = documentFile;
            //this.appObj = new Application__c(); //TODO Later
            this.appCreationReq = appCreationReq;
            this.appObj = appCreationReq.Application__r.name;
            this.appObjId = appCreationReq.Application__c;
            this.rptsumObjId=appCreationReq.Report_Summary__c;
        }
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            EFLXMLApplicationDataWapper compareToEmp = (EFLXMLApplicationDataWapper)compareTo;
            if (uploadedDate == compareToEmp.uploadedDate) return 0;
            if (uploadedDate < compareToEmp.uploadedDate) return 1;
            return -1;        
        }
    } 
}