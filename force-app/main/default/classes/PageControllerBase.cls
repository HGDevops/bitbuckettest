/*
    Allows interaction of component controller with page controller, when used
    with ComponentControllerBase class
*/

public with sharing virtual class PageControllerBase {
    
  public ComponentControllerBase myComponentController;
    
  public virtual ComponentControllerBase getMyComponentController() {
    return myComponentController;
  }

  public virtual void setComponentController(ComponentControllerBase compController) {
    myComponentController = compController;
  }
    
  public PageControllerBase getThis() {
    return this;
  }
    
}