global class EQSContactShareScheduled implements Schedulable {
   global void execute(SchedulableContext sc) {
      EQSBatchforContactShare c = new EQSBatchforContactShare(); 
      database.executebatch(c);
   }
}