@isTest
public class EFLLine_ApexManagedSharing_Test
{
    @testSetup
    static void initData() {
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        testDataAC.insertcustomsettings();
    }
  public static testmethod void ApexManagedSharing_testMethod1()
  {
      CARPOL_AC_TestDataManager inst=new CARPOL_AC_TestDataManager();
      Application__c  app=inst.newapplication();
      String poi='Resale';
      
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u=new user();
      u.Alias = 'standt';
      u.Email='standardTest@test.com';
      u.Username='ApexManagedSharing@test.com';
      u.LastName='TestUser';
      u.isActive=True;
      u.ProfileId=p.Id;
      u.EmailEncodingKey='UTF-8';
      u.LanguageLocaleKey='en_US';
      u.TimeZoneSidKey='America/Los_Angeles';
      u.LocaleSidKey='en_US';
      Insert u;
      
      List<AC__c> ACList=new List<AC__c>();
      AC__c acvar=inst.newLineItem(poi,app);
      ACList.add(acvar);
     
      Test.starttest();
      CARPOL_Line_ApexManagedSharing.addLineSharing(u.Id,ACList); 
      Test.stoptest();


    }
  public static testmethod void ApexManagedSharing_testMethod2()
  {
      CARPOL_AC_TestDataManager inst=new CARPOL_AC_TestDataManager();
      Application__c  app=inst.newapplication();
      String poi='Resale';
      
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u=new user();
      u.Alias = 'standt';
      u.Email='standardTest@test.com';
      u.Username='ApexManagedSharing@test.com';
      u.LastName='TestUser';
      u.isActive=True;
      u.ProfileId=p.Id;
      u.EmailEncodingKey='UTF-8';
      u.LanguageLocaleKey='en_US';
      u.TimeZoneSidKey='America/Los_Angeles';
      u.LocaleSidKey='en_US';
      Insert u;      
      List<AC__c> ACList=new List<AC__c>();      
      Test.starttest();
      CARPOL_Line_ApexManagedSharing.addLineSharing(u.Id,ACList); 
      Test.stoptest();
    }    
  public static testmethod void ApexManagedSharing_testMethod3()
  {
      CARPOL_AC_TestDataManager inst=new CARPOL_AC_TestDataManager();
      Application__c  app=inst.newapplication();
      String poi='Resale';      
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u=new user();    
      List<AC__c> ACList=new List<AC__c>();      
      Test.starttest();
      CARPOL_Line_ApexManagedSharing.addLineSharing(u.Id,ACList); 
      Test.stoptest();
    }        
}