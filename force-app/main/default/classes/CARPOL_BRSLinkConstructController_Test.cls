/**
@Author: Vijay Vellaturi
@name: CARPOL_BRSLinkConstructController_Test
@CreateDate: 9/23/15
@Description: Test class for CARPOL_BRSLinkConstructController
@Version: 1.0
@reference: CARPOL_BRSLinkConstructController(apex class)
*/
@IsTest(SeeAllData=True)
public class CARPOL_BRSLinkConstructController_Test {
    static Construct_Application_Junction__c cajObjInit;
    static Construct__c constrObj;
    Static Application__c appObj;
    Static Authorizations__c authObj;
    Static AC__c lineItemObj;
    Static AC__c lineItemObj1;
    static AC__c lineItemObj2;
    Static Link_Regulated_Articles__c regArticle;
    Static Applicant_Attachments__c appAttObj;
    static CARPOL_BRSLinkConstructController cc = new CARPOL_BRSLinkConstructController();
    
    static Construct_Application_Junction__c newCAJ(String constrId, String applId, String authId, String lItemId, String attId){
        Construct_Application_Junction__c obj = new Construct_Application_Junction__c();
        obj.Construct__c = constrId;
        obj.Application__c = applId;
        obj.Authorization__c = authId;
        obj.Line_Item__c = lItemId;
        obj.Design_Protocol_Record_SOP__c = attId;
        insert obj;
        return obj;
    }
    
    static void setupData() {
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();

		//Commented this code as this below newAuth method is failing with "EFLAuthorizationEngineFactory.EFLAuthorizationEngineException: No Authorization Engine found for Authorization: null"
        //authObj = testData.newAuth(appObj.id);
        Domain__c objProg = new Domain__c();
        objProg.Name = 'AC';
        objProg.Active__c = true;
        insert objProg;
        
        Program_Prefix__c prefix = new Program_Prefix__c();
        prefix.Program__c = objProg.Id;
        prefix.Name = '555';
        insert prefix; 
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP';
        objTP.Recordtypeid = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c = prefix.Id;
        insert objTP;
        appObj = testData.newapplication();
        
        Authorizations__c authObj=new  Authorizations__c();
        authObj.Application__c=appObj.id;
        authObj.RecordTypeID=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        //authObj.Status__c='Submitted';
        authObj.Date_Issued__c=date.today();
        authObj.Applicant_Alternate_Email__c='test@test.com';
        authObj.UNI_Zip__c='32092';
        authObj.UNI_Country__c='United States';
        authObj.UNI_County_Province__c='Duval';
        authObj.BRS_Proposed_Start_Date__c=date.today()+30;
        authObj.BRS_Proposed_End_Date__c=date.today()+40;
        authObj.CBI__c='CBI Text';
        authObj.Application_CBI__c='No';
        authObj.Applicant_Alternate_Email__c='email2@test.com';
        authObj.UNI_Alternate_Phone__c='(904) 123-2345';
        authObj.AC_Applicant_Email__c='email2@test.com';
        authObj.AC_Applicant_Fax__c='(904) 123-2345';
        authObj.AC_Applicant_Phone__c='(904) 123-2345';
        authObj.AC_Organization__c='ABC Corp';
        authObj.Means_of_Movement__c='Hand Carry';
        authObj.Biological_Material_present_in_Article__c='No';
        authObj.If_Yes_Please_Describe__c='Text';
        authObj.Applicant_Instructions__c='Make corrections';
        authObj.BRS_Number_of_Labels__c=10;
        authObj.BRS_Purpose_of_Permit__c='Importation';
        authObj.Documents_Sent_to_Applicant__c=false;
        authObj.Expiration_Timeframe_Override__c = 'Manual'; 
        authObj.Status__c = 'Approved';
        authObj.Thumbprint__c = objTP.Id;
        Insert authObj;
        AC__c lineItemObj = testData.newLineItem('test',appObj);
        AC__c lineItemObj1 = testData.newLineItem('test1', appObj);
        Regulated_Article__c regArt = testData.newRegulatedArticle(authObj.Program_Pathway__c);
        link_regulated_articles__c lra = new link_regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(lineItemObj.id, regArt.id, 'Draft');
        lineItemObj.Regulated_Article__c = regArt.id;
        regArticle = testData.newlinkRegArticle(lineItemObj.id, appObj.Id, authObj.id);
        constrObj = testData.newconstruct(lineItemObj.id, regArt);
        constrObj.Status__c = 'Review Complete';
        update constrObj;
        Applicant_Attachments__c appAttObj = testDataAC.newAttach(lineItemObj.id);
        appAttObj.Status__c = 'Review Complete';
        update appAttObj;
        Applicant_Attachments__c appAttObj2 = appAttObj.clone(false,false,false,false);
        insert appAttObj2;
        Construct_Application_Junction__c cajObj = newCAJ(constrObj.Id, appObj.Id, authObj.Id, lineItemObj.Id, appAttObj.Id);

        lineItemObj2 = [select Id from AC__c where Purpose_of_the_Importation__c = 'test' limit 1];
        cajObjInit = [select Id from Construct_Application_Junction__c 
                      where Line_Item__c =:lineItemObj2.Id];
        ApexPages.currentPage().getParameters().put('redirect','Yes');
        ApexPages.currentPage().getParameters().put('appid',lineItemObj2.id);
        ApexPages.currentPage().getParameters().put('method','edit');
        ApexPages.currentPage().getParameters().put('id',cajObjInit.id);
        cc.obj = cajObjInit;
    }
    
    @isTest
    static void testGetConstructDetails() {
        setupData();
        Test.startTest();
        List<Construct__c> conDetails = cc.getConstructDetails();
        system.assert(conDetails !=null);
        Test.stopTest();
    }
    
    @isTest
    static void testGetGenoRecordtypes() {
        setupData();
        Test.startTest();
        List<RecordType> genoRecTypes = cc.getGenoRecordtypes();
        system.assert(genoRecTypes != null);
        Test.stopTest();
    }

    @isTest
    static void testGetgenotypes() {
        setupData();
        Test.startTest();
        List<Genotype__c> genoTypes = cc.getgenotypes();
        system.assert(genotypes != null);
        Test.stopTest();
    }
        
    @isTest
    static void testGetappAttachs() {
        setupData();
        Test.startTest();
        List<Applicant_Attachments__c> attachmentsList = cc.getappAttachs();
        system.assert(attachmentsList !=null);
        cc.sop = '--None--';
        attachmentsList = cc.getappAttachs();
        System.assert(attachmentsList == null);
        Test.stopTest();
    }
    
    @isTest
    static void testGetConstructs() {
        setupData();
        Test.startTest();
        string[] ps = cc.getContructs();
        system.assert(ps != null);
        Test.stopTest();
    }
    
    @isTest
    static void testSetContructs() {
        setupData();
        Test.startTest();
        string[] ps;
        cc.setContructs(ps);
        System.assert(cc != null);
        Test.stopTest();
    }
        
    @isTest
    static void testGetSOP() {
        setupData();
        Test.startTest();
        string[] ps = cc.getSOP();
        system.assert(ps != null);
        Test.stopTest();
    }
    

    @isTest
    static void testSetSOP() {
        setupData();
        Test.startTest();
        string[] ps;
        cc.setSOP(ps);
        system.assert(cc != null);
        Test.stopTest();
    }

    @isTest
    static void testGetItems() {
        setupData();
        Test.startTest();
        CARPOL_BRSLinkConstructController cc1 = new  CARPOL_BRSLinkConstructController();
        cc1.obj = cajObjInit;
        List<SelectOption> soptions = cc1.getItems();
        system.assert(sOptions != null);
        Test.stopTest();
    }
        
    @isTest
    static void testGetSOPs() {
        setupData();
        Test.startTest();
        CARPOL_BRSLinkConstructController cc1 = new  CARPOL_BRSLinkConstructController();
        cc1.obj = cajObjInit;
        List<SelectOption> selectOpt = cc1.getSOPs();
        system.assert(selectOpt != null);
        Test.stopTest();
    }
    
    @isTest
    static void testCancel() {
        setupData();
        Test.startTest();
        CARPOL_BRSLinkConstructController ccempty=new  CARPOL_BRSLinkConstructController();
        ccempty.save();
        ccempty.cancel();
        CARPOL_BRSLinkConstructController cc=new  CARPOL_BRSLinkConstructController();
        cc.obj = cajObjInit;
        cc.cancel();
        system.assert(cc != null);
        Test.stopTest();
    }
    
    @isTest
    static void testSave() {
        setupData();
        Test.startTest();
        Construct__c constrObj = [select id from Construct__c where Line_Item__c =: lineItemObj2.Id];
        CARPOL_BRSLinkConstructController cc1 = new  CARPOL_BRSLinkConstructController();
        cc1.obj = cajObjInit;
        cc1.contructs =constrObj.id;
        cc1.save();
        system.assert(cc1 != null);
        
        //test  for constructs = '--None--'
        cc1.contructs ='--None--';
        cc1.save();
        System.assert(cc1.save() == null);
        
        //test  for conJunRec.size=0
        ApexPages.currentPage().getParameters().put('redirect','Yes');
        ApexPages.currentPage().getParameters().put('appid',null);
        ApexPages.currentPage().getParameters().put('method','edit');
        ApexPages.currentPage().getParameters().put('id',cajObjInit.id);
        CARPOL_BRSLinkConstructController cc2=new  CARPOL_BRSLinkConstructController();
        cc2.save();
        System.assert (cc2.save() != null);
        
        //Commenting below test block since cajObj.Line_Item__c can't go null in master detail
        //test for Construct_Application_Junction__c with line item null
        /*Construct_Application_Junction__c cajObj = [select Id, Line_Item__c from Construct_Application_Junction__c 
                                                    where Line_Item__c = : lineItemObj2.id ];
        cajObj.Line_Item__c = null;
        update cajObj;
        cc1.contructs =constrObj.id;
        cc1.Obj = cajObj;
        cc1.save();
        System.assert(cc1.save() != null);*/
        
        //test for sop=='--None--'
        cc1.sop = '--None--';
        cc1.save();
        System.assert(cc1.save() == null);
        Test.stopTest();
    }
    
    @isTest
    static void testSavePageRedirectNo() {
        setupData();
        Test.startTest();
        Construct_Application_Junction__c cajObj = [select Id, Line_Item__c from Construct_Application_Junction__c 
                                                    where Line_Item__c = : lineItemObj2.id ];
        ApexPages.currentPage().getParameters().put('redirect','No');
        ApexPages.currentPage().getParameters().put('appid',lineItemObj2.id);
        ApexPages.currentPage().getParameters().put('method','edit');
        ApexPages.currentPage().getParameters().put('id',cajObjInit.id);
        CARPOL_BRSLinkConstructController cc1 = new  CARPOL_BRSLinkConstructController();
        cc1.obj = cajObj;
        cc1.contructs = null;
        cc1.sop = null;
        cc1.save();
        cc1.cancel();
        System.assert(cc1 != null);
    }
    
    @isTest
    static void testSavePageRedirectYes() {
        setupData();
        Test.startTest();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        AC__c lineItemObj1 = [select Id from AC__c where Purpose_of_the_Importation__c = 'test1' limit 1];  
        Construct__c constrObj = [select id, link_regulated_article__c from Construct__c where Line_Item__c =: lineItemObj2.Id];
        link_regulated_articles__c lra = new link_regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(lineItemObj1.id, constrObj.link_regulated_article__c, 'Draft');
        //test for Constructs != null
        ApexPages.currentPage().getParameters().put('redirect','Yes');
        ApexPages.currentPage().getParameters().put('appid',lineItemObj1.Id);
        ApexPages.currentPage().getParameters().put('method','Insert');
        ApexPages.currentPage().getParameters().put('id',cajObjInit.id);
        CARPOL_BRSLinkConstructController cc1 = new  CARPOL_BRSLinkConstructController();
        cc1.obj = cajObjInit;
        cc1.contructs = constrObj.Id;
        cc1.save();
        System.assert(cc1 != null);
        
        //for SOP !=null
        ApexPages.currentPage().getParameters().put('redirect','Yes');
        ApexPages.currentPage().getParameters().put('appid',lineItemObj1.Id);
        ApexPages.currentPage().getParameters().put('method','Insert');
        ApexPages.currentPage().getParameters().put('id',cajObjInit.id);
        CARPOL_BRSLinkConstructController cc2 = new  CARPOL_BRSLinkConstructController();
        cc2.obj = cajObjInit;
        cc2.contructs = null;
        cc2.save();
        System.assert(cc2 != null);
    }

    @isTest
    static void testDocumentResource() {
        setupData();
        Test.startTest();
        CARPOL_BRSLinkConstructController.documentResource dm = new CARPOL_BRSLinkConstructController.documentResource();
        CARPOL_BRSLinkConstructController cc=new  CARPOL_BRSLinkConstructController();
        cc.obj = cajObjInit;
        dm.docID = cajObjInit.id;
        dm.isUploaded = true;
        dm.docName = 'testName';
        dm.docURL = 'testURL';
        cc.contructs = null;
        cc.save();
        Test.stopTest();
    }    
}