@isTest(seealldata=false)
private class CARPOL_UNI_ViewAssocContactsOvrride_Test {
      @IsTest static void CARPOL_UNI_ViewAssocContactsOvrde_Test() {
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 

          id p=[select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;  
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = p;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          System.runAs(usershare) {
              PageReference pageRef = Page.CARPOL_UNI_AssociatedContacts;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(apcont);
              //CARPOL_UNI_ViewAssocContactsOverride extclass = new CARPOL_UNI_ViewAssocContactsOverride(sc);
              //extclass.redirect();
              //system.assert(extclass != null);              
          }  
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_AssociatedContacts;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(apcont);
            //   CARPOL_UNI_ViewAssocContactsOverride extclassempty = new CARPOL_UNI_ViewAssocContactsOverride();              
            //   CARPOL_UNI_ViewAssocContactsOverride extclass = new CARPOL_UNI_ViewAssocContactsOverride(sc);
            //   extclass.redirect();  
            //   system.assert(extclass != null);                      
          Test.stopTest();   
      }
}