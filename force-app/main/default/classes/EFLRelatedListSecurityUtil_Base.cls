public interface EFLRelatedListSecurityUtil_Base {
    boolean canEdit(sObject o, User me, boolean defaultValue);
    boolean canDelete(sObject o, User me, boolean defaultValue);
    User getme();
}