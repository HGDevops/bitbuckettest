public with sharing class EFLProgressBarController {
    private ApexPages.StandardController controller;
    public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
    public Boolean navigation{get;set;}
    public Boolean processComplete{get;set;}
    public Boolean readyForNextStage{get;set;}
    public String currentstageName { get; set; }
    public Boolean hasError{get;set;}
    public Id authID{get;set;}
    public Authorizations__c auth{get;set;}
    public List<String> options{get; set;}  
    public static string currentChevronStep {get;set;}
    public integer currentStageIndex{get;set;}
    public list<Program_Stages__mdt> programStageList; 
    
    public string opts {get; set;} 
    public authorizations__c curAuth;  
    public EFLProgressBarController(ApexPages.StandardController controller) { 
        
        options = new List<String>();        
        authID = ApexPages.currentPage().getParameters().get('id');
        processComplete = false;
        this.controller = controller;
        shouldRedirect = false;
        //hasError = false;
        if(authID!=null){    
            curAuth = [Select id,stage__C,Ready_for_next_stage__c,Workflow_Number__c
                       from Authorizations__c where ID=:authID limit 1];
            currentstageName =curAuth.stage__C;
            readyForNextStage = curAuth.Ready_for_next_stage__c;
        }
        
        
        //To get Picklist's Values
        Schema.DescribeFieldResult fieldResult = Authorizations__c.Stage__c.getDescribe(); 
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues(); 
        
        string workflowNumber = [Select Workflow_Number__c  
                                 from Authorizations__c 
                                 where Id=:authID].Workflow_Number__c;
        
        programStageList = [select Workflow_Number__c,
                            Stage__c,
                            Sequence__c
                            from Program_Stages__mdt
                            where Workflow_Number__c =: workflowNumber
                            order by Sequence__c];  
        if(programStageList.isEmpty()){navigation = false;}
        else{
            navigation = true;
            for(Program_Stages__mdt ps : programStageList){ 
                options.add(ps.Stage__c); 
            }     
        }
        currentStageIndex = options.indexOf(currentstageName);
        //system.debug('chevronoptions'+ options.size());
        //system.debug('currentStageIndex'+ currentStageIndex);
        if(currentStageIndex == options.size()-1){navigation = false; 
                                                  if(readyForNextStage){processComplete = true;}}
        
        //Assigning array to a string  
        opts = JSON.serialize(options);
    } 
    
    public pagereference moveToNextStage()
    {
        //Authorizations__c authRec  = new Authorizations__c(id = authID,Stage__c =options.get(currentStageIndex+1));
        //update authRec;
        //
        /*updateStage(options.get(currentStageIndex+1));
return null;*/  
        
        if(EFLBRSAuthCheckStatusUtility.validateStatusForStageChange(curAuth)){
            apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('NextStageValidationMessage')));
            if(apexpages.hasMessages()){ 
                hasError = true;
                return null;
            }
        }
        else if(EFLBRSAuthCheckStatusUtility.validateInspectionStatus(curAuth)){
            apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,EFLGenericUtility.getMessage('NextStageInspectionNotCompleted')));
            if(apexpages.hasMessages()){ 
                hasError = true;
                return null; 
            }
        }
        else{
            try{
                updateStage(options.get(currentStageIndex+1));
                hasError = false;
                
            }
            catch(exception e){
                String errorMsg = e.getMessage();
                hasError = true;
                if(errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    errorMsg = errorMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', ': ');
                    //system.debug('errorMsg#### '+ errorMsg);
                    // pagereference p = apexpages.Currentpage();
                    apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,errorMsg));
                    if(apexpages.hasMessages())
                    { 
                        //system.debug('TestHasMessages');
                     hasError = true;
                     //system.debug('hasError:'+hasError);
                     return null;
                    } 
                }
            }
        }
        return null; 
        
    }
    
    public PageReference moveToFirstStage(){
        EFLBRSAuthorizationEngine validateUsers = new EFLBRSAuthorizationEngine();
        //system.debug('auth record in controller: '+ curAuth);
        //system.debug('Workflow Number: '+ curAuth.Workflow_Number__c);
        String errorMessage = validateUsers.validateRoles(curAuth);
        //system.debug('Error Message: ' + errorMessage);
        
        if(String.isBlank(errorMessage)){
            updateStage(options.get(0));
            //system.debug('options.get(0) '+ options.get(0));
            shouldRedirect = true;
            redirectUrl = controller.view().getUrl();
            return null;
        }else{
            apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,errorMessage));
            if(apexpages.hasMessages()){ 
                hasError = true;
                return null;
            }
            return null;
        }
    }
    
    /* public pagereference  completeLastStage()
{
processComplete = true;
return null;

}*/
    public void updateStage(string stageName){
        //system.debug('stageName '+stageName);
        Authorizations__c authRec  = new Authorizations__c(id = authID,Stage__c =stageName);
        update authRec;
    }
    
    // public Boolean hasErrormsgs { get { return ApexPages.hasMessages(); } }
    
}