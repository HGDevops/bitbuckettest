/**
* Author : Kishore Kumar
* Created Date : 1/18/2017
* Purpose : This is used to compare the original and amended line item(s) of an authorization.  
* Related Pages: EFLCompareAmendLineitems
*/
public with sharing class EFLCompareAmendLineitemsExtension{
    public AC__c currentline{get;set;}
    public AC__c parentline{get;set;}
    public list<sobject> currentlineItemRecords;
    public list<sobject> parentlineItemRecords;
    public map<string,string> currentlineItemMap{get;set;}
    public map<string,string> parentlineItemMap{get;set;}
    public map<string,list<string>> finallineItemMap{get;set;}
    public string authid{get;set;}
    public string authname{get;set;}
    public List<Applicant_Attachments__c> appAttachments{get;set;} 
    public List<Attachment> applicantAttachments;
    public List<Attachment> OriginalappAtt{get;set;}
    public List<Attachment> AmendedappAtt{get;set;}
    public map<id,list<Change_History__c>> changeLocationMap{get;set;}
    public map<id,list<Change_History__c>> changeLinkRegArticlesMap{get;set;}
    public map<id,list<Change_History__c>> changeConstructMap {get;set;}
    public map<string,map<string,list<Change_History__c>>> tablesToShow{get;set;}
    
    /** Constructor **/
    public EFLCompareAmendLineitemsExtension(ApexPages.StandardController stdController){
        
        currentline=[SELECT id,Name,Parent_Line_Item__c,Authorization__c,Authorization__r.name FROM AC__c WHERE id=:ApexPages.currentPage().getParameters().get('id')];
        parentline=[SELECT id,Name,Parent_Line_Item__c,Authorization__c,Authorization__r.name FROM AC__c WHERE id=:currentline.Parent_Line_Item__c];
        authid=currentline.Authorization__c;
        authname=currentline.Authorization__r.name;
        currentlineItemRecords=getlineItemDetails(currentline.Id);
        parentlineItemRecords=getlineItemDetails(currentline.Parent_Line_Item__c);
        currentlineItemMap=new  map<string,string>();
        parentlineItemMap=new  map<string,string>();
        finallineItemMap=new  map<string,list<string>>();
        AmendedappAtt=getattachments(currentline.Id);
        OriginalappAtt=getattachments(currentline.Parent_Line_Item__c);
        
    }
    
    
    /** Get all fields from line item field set TrackedFields **/
    public List<Schema.FieldSetMember> getFields(){
        return SObjectType.ac__c.FieldSets.TrackedFields.getFields();
    }
    /** Get all line item details information **/   
    public list<sobject> getlineItemDetails(Id lineId){
        ID recordId=lineId;
        DescribeSObjectResult describeResult=recordId.getSObjectType().getDescribe();
        List<string> fieldNames=new  List<string>(describeResult.fields.getMap().keySet());
        string query1='SELECT ';
        for(Schema.FieldSetMember f:this.getFields())query1+=f.getFieldPath()+', ';
        query1+='Id FROM ac__c  WHERE id =: recordId LIMIT 1';
        
        string query=' SELECT '+string.join(fieldNames,',')+' FROM '+describeResult.getName()+' WHERE '+' id =: recordId'+' LIMIT 1 ';
        List<SObject> records=Database.query(query1);
        return records;
    }
    /** Compare Original and Amended lineitem information **/   
    public pagereference CompareTwolineItemRecords(){
        Map<string,Schema.SObjectField> lineItemFieldsMap=schema.SObjectType.ac__c.fields.getMap();
        /** Go through all the lineitem fields **/
        List<string> lineItemAPIFieldNames=new  List<string>();
        for(Schema.FieldSetMember f:this.getFields())lineItemAPIFieldNames.add(f.getFieldPath());
        for(sObject parentlineRecord:parentlineItemRecords){
            for(sObject lineRecord:currentlineItemRecords){
                
                /** For each field **/
                for(string field:lineItemAPIFieldNames){
                    if(lineRecord.get(field)!=parentlineRecord.get(field)&&lineRecord.get(field)!=null&&parentlineRecord.get(field)!=null){
                        list<string> fieldvalueList=new  list<string>();
                        if(lineItemFieldsMap.get(field).getDescribe().getType()==Schema.DisplayType.REFERENCE){
                            string currentLookupValue=lookupfieldValue(lineRecord,field);
                            string parentLookupValue=lookupfieldValue(parentlineRecord,field);
                            currentlineItemMap.put(lineItemFieldsMap.get(field).getDescribe().getLabel(),currentLookupValue);
                            parentlineItemMap.put(lineItemFieldsMap.get(field).getDescribe().getLabel(),parentLookupValue);
                            fieldvalueList.add(currentLookupValue);
                            fieldvalueList.add(parentLookupValue);
                            finallineItemMap.put(lineItemFieldsMap.get(field).getDescribe().getLabel(),fieldvalueList);
                        }
                        else{
                            fieldvalueList.add(string.valueOf(lineRecord.get(field)));
                            fieldvalueList.add(string.valueOf(parentlineRecord.get(field)));
                            currentlineItemMap.put(lineItemFieldsMap.get(field).getDescribe().getLabel(),string.valueOf(lineRecord.get(field)));
                            parentlineItemMap.put(lineItemFieldsMap.get(field).getDescribe().getLabel(),string.valueOf(parentlineRecord.get(field)));
                            finallineItemMap.put(lineItemFieldsMap.get(field).getDescribe().getLabel(),fieldvalueList);
                        }
                    }
                }
            }
        }
        
        tablesToShow = new map<string,map<string,list<Change_History__c>>>{'Applicant Locations' => new map<string,list<Change_History__c>>(),
            'Applicant LinkRegulatedArticles' => new map<string,list<Change_History__c>>(),
            'Applicant Construct' => new map<string,list<Change_History__c>>(),
            'Applicant Design Protocol' => new map<string,list<Change_History__c>>(),
            'Applicant PhenoType' => new map<string,list<Change_History__c>>(),
            'Applicant GenoType' => new map<string,list<Change_History__c>>()};
                
                map<string,string> fieldAndLabelMap = new map<string,string>{'Location__c'=>'Applicant Locations',
                    'Link_Regulated_Articles__c'=>'Applicant LinkRegulatedArticles',
                    'Construct__c'=>'Applicant Construct',
                    'Design_Protocol_Record__c'=>'Applicant Design Protocol',
                    'Phenotype__c'=>'Applicant PhenoType',
                    'Genotype_Phenotype__c'=>'Applicant GenoType'};
                        
                        list<Change_History__c> changeHistoryFullList = [select id, name, Location__c, Link_Regulated_Articles__c, Construct__c,Design_Protocol_Record__c, Genotype_Phenotype__c, Phenotype__c, Field__c, Field_API_Name__c, New_Value__c, Old_Value__c from Change_History__c where Location__r.Line_Item__c = :currentline.id OR Link_Regulated_Articles__r.Line_Item__c = :currentline.id OR Construct__r.Line_Item__c = :currentline.id OR Design_Protocol_Record__r.Line_Item__c = :currentline.id OR Phenotype__r.Construct__r.Line_Item__c = :currentline.id OR Genotype_Phenotype__r.Related_Construct_Record_Number__r.Line_Item__c = :currentline.id];
        
        for(Change_History__c chng : changeHistoryFullList){
            for(string field : fieldAndLabelMap.keySet()){
                if(chng.get(field) != null){
                    map<string,list<Change_History__c>> tempMap = new map<string,list<Change_History__c>>();
                    if(tablesToShow.containsKey(fieldAndLabelMap.get(field))){
                        tempMap = tablesToShow.get(fieldAndLabelMap.get(field));
                    }
                    list<Change_History__c> tempList = new list<Change_History__c>();
                    if(tempMap.containskey(string.valueOf(chng.get(field)))){
                        tempList = tempMap.get(string.valueOf(chng.get(field)));
                    }
                    tempList.add(chng);
                    tempMap.put(string.valueof(chng.get(field)), tempList);
                    tablesToShow.put(fieldAndLabelMap.get(field),tempMap);
                }
            }
        }
        
        
        
        
        return null;
    }
    /** Obtain Original and Amended lineitem lookup field value **/ 
    public string lookupfieldValue(sobject obj,string field){
        Id lookupid=Id.valueOf(string.valueOf(obj.get(field)));
        string sObjName=lookupId.getSObjectType().getDescribe().getName();
        sObject sObjectRecord=Database.query('SELECT Id, Name FROM '+sObjName+' WHERE Id = :lookupid');
        return (string)sObjectRecord.get('Name');
    }
    /** Redirect to Authorization page **/      
    public PageReference redirect(){
        PageReference dirpage=new  PageReference('/'+authid);
        dirpage.setRedirect(true);
        return dirpage;
    }
    /** Get Attachments of the applicant attachments for both line items **/        
    public List<Attachment> getattachments(Id LineItemId){
        appAttachments=[SELECT Id,Applicant_Instructions__c,Name,Document_Types__c,File_Name__c,File_Description__c,CBI_Attachment__c,Status__c,(SELECT ID,Name FROM Attachments) FROM Applicant_Attachments__c WHERE Animal_Care_AC__c=:currentline.Id];
        applicantAttachments=new  list<Attachment>();
        for(Applicant_Attachments__c app:appAttachments)applicantAttachments.add(app.Attachments);
        return applicantAttachments;
    }
}