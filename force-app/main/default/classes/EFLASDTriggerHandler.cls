/*
 * Author: Ravee Racharla Date:11/14/2018
 * Purpose: Trigger for Article Supplier/Developer object
*/
public inherited sharing class EFLASDTriggerHandler {
    
    public static void ValidateRequiredFields(List<Article_Supplier_Developer__c> asdList)    
    {
    
    id imprectypeid  = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Importer');
    id deliveryrecptrectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Delivery');
    id exporterrectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Exporter');
    String USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;
        for (Article_Supplier_Developer__c asd : asdList){
            if (string.isBlank(asd.First_Name__c)){
                asd.First_Name__c.addError('You must enter a value.');
            }
            if (string.isBlank(asd.name)){
                asd.name.addError('You must enter a value.');
            }
            if (string.isBlank(asd.country__c)){
                asd.country__c.addError('You must enter a value.');
            }
            if ((asd.recordtypeid == imprectypeid  || asd.recordtypeid == deliveryrecptrectypeid) && string.isBlank(asd.State__c)){
                asd.State__c.addError('You must enter a value.');
            }
            if ((asd.recordtypeid == imprectypeid  || asd.recordtypeid == deliveryrecptrectypeid || asd.recordtypeid == exporterrectypeid) && string.isBlank(asd.Street_Address__c)){
                asd.Street_Address__c.addError('You must enter a value.');
            }
            if ((asd.recordtypeid == imprectypeid  || asd.recordtypeid == deliveryrecptrectypeid || asd.recordtypeid == exporterrectypeid) && string.isBlank(asd.City__c)){
                asd.City__c.addError('You must enter a value.');
            }
            if ((asd.recordtypeid == imprectypeid  || asd.recordtypeid == deliveryrecptrectypeid) && string.isBlank(asd.Postal_Code__c)){
                asd.Postal_Code__c.addError('You must enter a value.');
            }
            if (asd.Country__c== usCountryId && asd.Postal_Code__c != null){
               if(!EFLGenericUtility.ValidateZip(asd.Postal_Code__c)) asd.Postal_Code__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorFailZip'));
               
            } 
            if (asd.Country__c== usCountryId && string.isBlank(asd.State__c)){
                asd.State__c.addError('You must enter a value.');
            }
        }
        
    }
    
    public static void ACArticlSupplierProcessReadiness(List<Article_Supplier_Developer__c> asdList)
    {
      id exporterRecordTypeId = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Exporter');
      id importerRecordTypeId = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Importer');
      id deliveryRecordTypeId = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Delivery');
      id supplierRecordTypeId = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Info type');//KA:W-035904
          
        set<Id> lineItemIds = new set<Id>();
        for(Article_Supplier_Developer__c asd : asdList)
        {
            if(asd.RecordTypeId==exporterRecordTypeId || asd.RecordTypeId==importerRecordTypeId || asd.RecordTypeId==deliveryRecordTypeId || asd.RecordTypeId==supplierRecordTypeId)
            {
                lineItemIds.add(asd.Line_Item__c);
            }
        }
        
        if(!lineItemIds.isEmpty())
        {
            List<AC__c> LineItemsToUpdate = new List<AC__c>();
            List<AC__c> LineItemsWithChildren = new List<AC__c>();
            LineItemsWithChildren = [select id, Related_Contact_Status__c, 
                                     (select id, Name, RecordTypeId from Article_Suppliers__r) 
                                     from AC__c where Id in :lineItemIds];
            for(AC__c lineItem : LineItemsWithChildren)
            {
                boolean hasExporter = false;
                boolean hasImporter = false;
                boolean hasDelivery = false;
                boolean hasSupplier = false; //KA:W-035904
                for(Article_Supplier_Developer__c asd : lineItem.Article_Suppliers__r)
                {
                    if(asd.RecordTypeId==exporterRecordTypeId)
                    {
                        hasExporter = true;
                    }
                    else if(asd.RecordTypeId==importerRecordTypeId)
                    {
                        hasImporter = true;
                    }
                    else if(asd.RecordTypeId==deliveryRecordTypeId)
                    {
                        hasDelivery = true;
                    }
                    //KA:W-035904::START
                    else if(asd.RecordTypeId==supplierRecordTypeId)
                    {
                        system.debug('asd.RecordTypeId++' +asd.RecordTypeId);
                        hasSupplier = true;
                    }
                    //KA:W-035904::ENDS
                }
                if((hasExporter==true && hasImporter==true && hasDelivery==true)||hasSupplier==true)
                {
                    if(lineItem.Related_Contact_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
                    {
                      LineItemsToUpdate.add(new AC__c(id=lineItem.Id, Related_Contact_Status__c=CARPOL_Constants.READY_TO_SUBMIT));    
                    }
                }
                else
                {
                   if(lineItem.Related_Contact_Status__c!=CARPOL_Constants.YET_TO_ADD)
                    {
                        LineItemsToUpdate.add(new AC__c(id=lineItem.Id, Related_Contact_Status__c=CARPOL_Constants.YET_TO_ADD)); 
                    }
                }
            
            } 
            if(!LineItemsToUpdate.isEmpty())
            {
                update LineItemsToUpdate;
            }
        }
    }
    
}