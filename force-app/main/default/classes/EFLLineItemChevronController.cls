public with sharing class EFLLineItemChevronController { 
    
    public string thumbPrintType; //Ravee Racharla 12/7/2018
    public ID lineItemId {get;set;}
    public list<EFLLineItemFlow__mdt> lineItemFlowList;
    public static string currentChevronStep {get;set;}
    public string callStepID {get;set;}
    public string previousChevronStep;
    public string nextChevronStep;
    public integer currentSequence{get;set;}
    public integer nextSequence{get;set;}
    public integer previousSequence{get
    {
        if (previousSequence ==null){
             system.debug('lineItemRecord.thumbprint__c@@@@ '  + lineItemRecord.thumbprint__c); 
            lineItemRecord = EFLLineItemRepository.selectbyID(lineItemId);
            if(lineItemRecord.thumbprint__c !=null){
                thumbPrintType = lineItemRecord.thumbprint__r.Name;
            }
             
        /*
        	if ( lineItemRecord.Type_of_Permit__c == 'Standard Permit' ) {
            thumbPrintType = 'BRS Standard Permit';
        	}	
            if ( lineItemRecord.Type_of_Permit__c == 'Notification' ) {
                //W34606 -  Updated to display missing chevron for Notification
                thumbPrintType = lineItemRecord.thumbprint__r.Name;
            }	
            
            If(lineItemRecord.Program_Line_Item_Pathway__r.Name==EFLGlobalConstants.getConstantValue('PROGRAM_PATHWAY_ACLD'))
            {
                thumbPrintType = 'AC - Live Dogs for Resale CONUS - Permit';
            }*/
            
           // system.debug('previousSequence @@@@  ThumbPrint '  +  thumbPrintType );
           // system.debug('previousSequence @@@@  currentChevronStep '  +  currentChevronStep );
            if (thumbPrintType !=null && thumbPrintType!=''){  
                
                lineItemFlowList = getlineItemFlow(thumbPrintType);
            	totalSteps = lineItemFlowList.size(); 
                for(EFLLineItemFlow__mdt step : lineItemFlowList){
     
                     if(step.EFLLineItemStep__r.Step__c == currentChevronStep){
                         
                         currentSequence = (step.Sequence__c).intValue();
                      }
            	}
            }
           // system.debug('previousSequence @@@@  current sequence'  +  currentSequence );
         if(currentSequence!=null){
            nextSequence = currentSequence + 1;
            previousSequence = currentSequence - 1;
        }
        }
       // system.debug('previousSequence @@@@ '  + previousSequence);
        return previousSequence; 
        
      } set;}
    public integer totalSteps{get
    {  
        if(totalSteps==null){ 
            totalSteps = lineItemFlowList.size();}
        return totalSteps;
    }set;}
    
    public AC__c lineItemRecord {get
    {
      // if(lineItemRecord == null){
            lineItemRecord = EFLLineItemRepository.selectbyID(lineItemId);
            thumbPrintType = lineItemRecord.thumbprint__r.Name;
        /*//Ravee Racharla 12/7/2018
        	if ( lineItemRecord.Type_of_Permit__c == 'Standard Permit' ) {
            thumbPrintType = 'BRS Standard Permit';
        	}	
            if ( lineItemRecord.Type_of_Permit__c == 'Notification' ) {
                //W34606 -  Updated to display missing chevron for Notification
                thumbPrintType = lineItemRecord.thumbprint__r.Name;
            }	
        //Ravee Racharla 12/7/2018
      //  }*/
        return lineItemRecord;
    }set;}
 
    public id SOPRecordTypeID {get{        
        if(SOPRecordTypeID==null){
           SOPRecordTypeID = EFLGenericUtility.getRecordTypeId('Applicant Attachment SOP'); 
        }
        return SOPRecordTypeID;
    }set;}
    
    public boolean enableNavigation{
        get{
            //if(lineItemRecord.Does_This_Application_Contain_CBI__c!=null)
            system.debug('AppStatus@@'+ lineItemRecord.Application_Details_Status__c);
            if(lineItemRecord.Application_Details_Status__c  == CARPOL_Constants.READY_TO_SUBMIT)
               { enableNavigation = true; }
             return enableNavigation;
        }set;
    }

    public EFLLineItemChevronController(){
        /* Ravee Racharla 12/7/2018 Commented below code and updated the previous sequence 
        lineItemFlowList = getlineItemFlow('BRS Standard Permit');
         for(EFLLineItemFlow__mdt step : lineItemFlowList){
 
             if(step.EFLLineItemStep__r.Step__c == currentChevronStep){
                 
                 currentSequence = (step.Sequence__c).intValue();
              }
        }
         if(currentSequence!=null){
            nextSequence = currentSequence + 1;
            previousSequence = currentSequence - 1;
        }
				*/
    }
    
    public String chevronOptions { get{
        //Ravee Racharla 12/7/2018 Begin 
        if(lineItemRecord == null){
            lineItemRecord = EFLLineItemRepository.selectbyID(lineItemId);
           /* system.debug('lineItemRecord@@@@'+lineItemRecord);
           
        	if ( lineItemRecord.Type_of_Permit__c == 'Standard Permit' ) {
            	thumbPrintType = 'BRS Standard Permit';
        	}	
            if ( lineItemRecord.Type_of_Permit__c == 'Notification' ) {
                //W34606 -  Updated to display missing chevron for Notification
                thumbPrintType = lineItemRecord.thumbprint__r.Name;
            }	*/
      	}
        // system.debug('lineItemRecord.thumbprint@@@@'+lineItemRecord.thumbprint__r.Name);
        //Ravee Racharla 12/7/2018 End
        if (chevronOptions == null){
            lineItemFlowList = getlineItemFlow(lineItemRecord.thumbprint__r.Name);
            totalSteps = lineItemFlowList.size(); 
            list<String> options = new list<String>();
            for(EFLLineItemFlow__mdt flow : lineItemFlowList){ 
                 options.add(flow.EFLLineItemStep__r.Step__c); 
                 
             }  
            
             //Assigning array to a string  
             chevronOptions = JSON.serialize(options); 
            return chevronOptions;
        } 

        return chevronOptions;
    }set;
  } 
    
    
    private list<EFLLineItemFlow__mdt> getlineItemFlow(string thumbPrint){
       // system.debug('thumbPrint@@@'+thumbPrint);
        lineItemFlowList = [select ThumbPrint__c,
                            Sequence__c,
                            Program__c,
                            EFLLineItemStep__c,
                            EFLLineItemStep__r.Step__c, 
                            EFLLineItemStep__r.Component_Name__c
                            from EFLLineItemFlow__mdt
                            where ThumbPrint__c =: thumbPrint
                            order by Sequence__c]; 
            return lineItemFlowList;
       } 
    
    private EFLLineItemFlow__mdt getStepDetails(string thumbPrint,integer sequence){
        
               EFLLineItemFlow__mdt currentLineItemStep = [select ThumbPrint__c,
                                                           Sequence__c,
                                                           Program__c,
                                                           EFLLineItemStep__c,
                                                           EFLLineItemStep__r.Step__c, 
                                                           EFLLineItemStep__r.Component_Name__c
                                                           from EFLLineItemFlow__mdt
                                                           where ThumbPrint__c =: thumbPrint
                                                           and Sequence__c =: sequence]; 
            return currentLineItemStep;
       } 
    
    public pagereference Next(){
   
          EFLLineItemFlow__mdt nextStep = getStepDetails(thumbPrintType, nextSequence);
            PageReference page = new PageReference('/apex/'+nextStep.EFLLineItemStep__r.Component_Name__c);
            page.getParameters().put('LineItemId',lineItemId);
         
            if(nextStep.EFLLineItemStep__r.Component_Name__c == 'EFLSOP' ||
               nextStep.EFLLineItemStep__r.Component_Name__c == 'EFLAttachment'){
                     page.getParameters().put('RecordType',SOPRecordTypeID);
               }
        
             page.setRedirect(true);
            return page;
    }
    
    public pagereference Back(){   
       
          EFLLineItemFlow__mdt previousStep = getStepDetails(thumbPrintType, previousSequence);
           PageReference page = new PageReference('/apex/'+previousStep.EFLLineItemStep__r.Component_Name__c);
              if(previousStep.EFLLineItemStep__r.Component_Name__c == 'EFLLineItem'){
                 page.getParameters().put('Id',lineItemId); 
              }else if(previousStep.EFLLineItemStep__r.Component_Name__c == 'EFLSOP' ||
                       previousStep.EFLLineItemStep__r.Component_Name__c == 'EFLAttachment'){
                 page.getParameters().put('LineItemId',lineItemId);
                 page.getParameters().put('RecordType',SOPRecordTypeID);
              }else{
                 page.getParameters().put('LineItemId',lineItemId); 
              }
            page.setRedirect(true);
            return page;
    } 
    
    public pagereference callPage(){   

        string Step = callStepID.replace('_', ' ');
        EFLLineItemFlow__mdt previousStep =[  select ThumbPrint__c,
                                                            Sequence__c,
                                                           Program__c,
                                                           EFLLineItemStep__c,
                                                           Step__c, 
                                                           Component_Name__c
                                                           from EFLLineItemFlow__mdt
                                                           where ThumbPrint__c =: thumbPrintType
                                                             and Step__c =: Step ];
                                                               
           PageReference page = new PageReference('/apex/'+previousStep.Component_Name__c);
              if(previousStep.Component_Name__c == 'EFLLineItem'){
                 page.getParameters().put('Id',lineItemId); 
              }else if(previousStep.Component_Name__c == 'EFLSOP' ||
                       previousStep.Component_Name__c == 'EFLAttachment'){
                 page.getParameters().put('LineItemId',lineItemId);
                 page.getParameters().put('RecordType',SOPRecordTypeID);
              }else{ 
                 page.getParameters().put('LineItemId',lineItemId); 
              }
            page.setRedirect(true);
            return page;
    }    

}