@isTest
public class CARPOL_UNI_MasterApplicationTriggerTest {
     
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    static testMethod void testHappyPathBRS() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
            
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        test.startTest();
        
        system.runAs(userShare)
        {
            
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            auth = new Authorizations__c();
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            app.Application_Status__c = 'Submitted';
            app.Application_Submitted__c = false;
            update app;
        }
    }
    
    static CARPOL_AC_TestDataManager testACData = new CARPOL_AC_TestDataManager();
    static testMethod void testHappyPathAC() {
        testACData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testACData.newapplicationByStatus('Waiting on Customer');
     	Authorizations__c auth = testACData.newAuthByStatus(app.id, 'Waiting on Customer');
        AC__c lineitem = testACData.newLineItemDogs('Personal Use',app, 'Waiting on Customer', auth.id);      
        app.Application_Status__c = 'Submitted';
        update app;
        
    }
    
	static testMethod void testMoreACHappyPath() {
        testACData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testACData.newapplicationByStatus('Submitted');
     	Authorizations__c auth = testACData.newAuthByStatus(app.id, 'Submitted');
        AC__c lineitem = testACData.newLineItemDogs('Personal Use',app, 'Submitted', auth.id);  
        app.Application_Submitted__c = false;
        app.Application_Status__c = 'Waiting on Customer';
        update app;
        
    }
    
    static testMethod void testMoreACHappyPath2() {
        testACData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testACData.newapplicationByStatus('Waiting on Customer');
     	Authorizations__c auth = testACData.newAuthByStatus(app.id, 'Waiting on Customer');
        AC__c lineitem = testACData.newLineItemDogs('Personal Use',app, 'Waiting on Customer', auth.id);  
        app.Application_Submitted__c = false;
        app.Application_Status__c = 'On-Hold';
        update app;
        
    }
}