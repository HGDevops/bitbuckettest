public class EFLWizardFlow {
    public EFLWizardActivity currentQuestion {get;set;}
    public List<EFLWizardActivity> QuestionList {get;set;}
    public integer currentQuestionIndex {get; set;}
    public map<String,String> flowParameters {get; set;}
    
    public void setWizardFlowParameter (string paramName, string value){
        if(this.flowParameters != null){
            flowParameters.put(paramName,value);
            
        }
    }
}