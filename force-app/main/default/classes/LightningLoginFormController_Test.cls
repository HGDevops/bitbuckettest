@IsTest
public class LightningLoginFormController_Test {
    public static testmethod void LightningLoginFormControllerTestmethod1(){
        LightningLoginFormController.login('testusername', 'testpassword', 'teststartUrl');
        LightningLoginFormController.getIsUsernamePasswordEnabled();
        LightningLoginFormController.getIsSelfRegistrationEnabled();
        LightningLoginFormController.getSelfRegistrationUrl();
        LightningLoginFormController.getForgotPasswordUrl();
    }

}