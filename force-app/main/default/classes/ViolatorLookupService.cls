public inherited sharing class ViolatorLookupService {
    
    @future(callout = true)
    public static void lookupViolator(ID acID) 
    {       
        //system.debug('Inside violator lookup service line 6 >>>');
        try{ 
            List<AC__C> lineItems = [SELECT ID, Name, Importer_First_Name__c, Is_violator__c, Importer_Last_Name__c FROM AC__c WHERE ID = :acID LIMIT 1];
            //system.debug('Inside violator lookup service line 9');
            //if(!test.isrunningtest()){
            if(!lineItems.isEmpty()){
                AC__c lineItem = lineItems[0];
                //system.debug('Inside violator lookup service line 13>>>'+lineitem);
                //look up the user record            
                Applicant_Contact__c lastNameRecord = [SELECT ID, Name, First_Name__c FROM Applicant_Contact__c WHERE ID = :lineItem.Importer_Last_Name__c LIMIT 1];
                String firstName = lastNameRecord.First_Name__c;
                String lastName = lastNameRecord.Name;
                //system.debug('Inside violator lookup service linw 18 >>>'+lastNameRecord);
                //make the request
                //system.debug('Inside violator lookup service >>>>making before call line 20');
                String response = initiateCall(firstName, lastName);
                
                //check to see if this returned any potential violators
                if(containsResults(response)) 
                {
                    lineItem.Is_violator__c = 'Yes';
                }
                else 
                {
                    lineItem.Is_violator__c = 'No';
                }
                
                updateItems(lineItem);
            }
            // }
        } catch(Exception ae){
            Error_Logging__c log = new Error_Logging__c();  
            log.class__c = 'ViolatorLookupService';
            log.priority__c = 'ERROR';
            log.trace__c = ae.getTypeName() + '\n' + ae.getCause() + '\n' + ae.getMessage() + '\n' + ae.getLineNumber();
            insert log; 
        }
        
    }
    
    public static void updateItems(AC__C lineItemList) 
    {
        //checkRecursive.runOnce();
        update lineItemList;
    }
    
    public static String initiateCall(String firstName, String lastName) 
    {
        
        //system.debug('Inside violator lookup service line 55>>>');
        //Preparing URL with the required parameters - added by Dinesh - 4/5 
        CARPOL_URLs__c Items = CARPOL_URLs__c.getValues('ITEMS');
        String ItemsUrl = Items.URL__c;
        String url = ItemsUrl+firstName+'%20'+lastName;
        
        Map<String, String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        headers.put('USDAEAUTHID', Items.Security_Key__c);
        
        //Calling ITEMS Service
        RestClient restClient = new RestClient(url, 'GET', headers);
        return restClient.responseBody;
    }
    
    /**
* Check to see if the response contains any returned results. If there is atleast 1, this returns true
*/
    public static Boolean containsResults(String response) 
    {
        if(response != null){
            JSONParser parser = JSON.createParser(response);
            while (parser.nextToken() != null) 
            {
                //look to see if the response has a subject returned
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'subjectID')) 
                {
                    return true;
                }
            }
        }
        return false;
    }
}