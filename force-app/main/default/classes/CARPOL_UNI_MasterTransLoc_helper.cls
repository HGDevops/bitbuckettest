public with sharing class CARPOL_UNI_MasterTransLoc_helper{
  public static void insertRelatedDM(set<id> transitLocListIds){
      string rtId = [select id from recordtype where name = 'Transit Locales'].id;
      system.debug('---transitLocListIds----'+transitLocListIds);
      list<Transit_Locale__c> transitLocList = new list<Transit_Locale__c>();
      map<string, string> mapStateTL = new map<string, string>();
      map<string,string> mapStateLine = new map<string,string>();
      list<Related_Line_Decisions__c> listRelLineDec = new list<Related_Line_Decisions__c>();
      transitLocList = [select id,recordtypeid,Line_Item__c,Port_of_Entry__c,Port_of_Entry__r.State_LV1__c,Port_of_Exit__c,Port_of_Exit__r.State_LV1__c from Transit_Locale__c where id in:transitLocListIds and recordtypeid=:rtId ];
      for(Transit_Locale__c tl:transitLocList){
          string strstateid = '';
          if(tl.Port_of_Entry__r.State_LV1__c!=null && tl.Port_of_Exit__r.State_LV1__c!=null){
              strstateid = tl.Port_of_Entry__r.State_LV1__c ;
              strstateid += tl.Port_of_Exit__r.State_LV1__c;
              system.debug('----strstateid1---'+strstateid);
              mapStateTL.put(strstateid, tl.Id);
              mapStateLine.put(strstateid,tl.Line_Item__c);
          }
      }
      for(Regulations_Association_Matrix__c dm : [select id,State_of_Port_of_Entry__c,State_of_Port_of_Exit__c from Regulations_Association_Matrix__c where State_of_Port_of_Entry__c!=null and State_of_Port_of_Exit__c!=null ])
      {
          string strstateid = '';
          strstateid = dm.State_of_Port_of_Entry__c;
          strstateid += dm.State_of_Port_of_Exit__c ;
          system.debug('----strstateid2---'+strstateid);
          if(mapStateLine.containskey(strstateid)){
              Related_Line_Decisions__c objRelLineDec = new Related_Line_Decisions__c();
              objRelLineDec.Line_Item__c = mapStateLine.get(strstateid);
              objRelLineDec.Decision_Matrix__c = dm.id;
              objRelLineDec.Transit_Locale__c = mapStateTL.get(strstateid);
              listRelLineDec.add(objRelLineDec);
          }
      }
      if(!listRelLineDec.isEmpty()){
      system.debug('----listRelLineDec----'+listRelLineDec);
          insert listRelLineDec;
      }
  }
  public static void deleteRLD(set<Id> transitLocDelIds){
  system.debug('---transitLocDelIds---'+transitLocDelIds);
   List <Related_Line_Decisions__c> listRLD = new list <Related_Line_Decisions__c>();
   for (Related_Line_Decisions__c objRLD : [select id from Related_Line_Decisions__c where Transit_Locale__c in : transitLocDelIds])
       {
        listRLD.add(objRLD);
        system.debug('---objRLDId----'+objRLD.id);
       }
       system.debug('---listRLD----'+listRLD);
       if(!listRLD.isEmpty()){
        
           delete listRLD;
       }
    }
  public static void prevAddlstopsEdit(set<id> addlStopsListIds){    
    List<Transit_Locale__c> transitsToDelete = new List<Transit_Locale__c>();
    Transit_Locale__c editedTransit = [Select CreatedDate,Line_Item__c From Transit_Locale__c WHERE ID IN:addlStopsListIds][0];
    for(Transit_Locale__c transit : [Select Id,CreatedDate From Transit_Locale__c WHERE ID!=:editedTransit.Id AND CreatedDate>:editedTransit.CreatedDate AND Line_Item__c=:editedTransit.Line_Item__c]){
        transitsToDelete.add(transit);
    }
    delete transitsToDelete;
  }
  
}