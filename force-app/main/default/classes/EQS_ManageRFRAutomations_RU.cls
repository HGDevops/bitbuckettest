global class EQS_ManageRFRAutomations_RU Implements Schedulable
    {
        global void execute(SchedulableContext sc)
        {
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :sc.getTriggerId()];
            System.debug('>>>'+ct.CronExpression);
            System.debug('>>>'+ct.TimesTriggered);
            autoManageResponders_UnAvailableEndDt();
            
        }
        
        public void autoManageResponders_UnAvailableEndDt()
        {
            List<Contact> UpdResponderUEDList = new List<Contact>();
            List<Contact> ResponderList= [SELECT ID,EQS_Unavailable_Start_Date__c,EQS_Unavailable_End_Date__c From Contact Where RecordType.Name='APHIS EQS Responder' AND EQS_Responder_Status__c='Unavailable' AND EQS_Unavailable_End_Date__c=Yesterday];

            for(Contact Responder : ResponderList)
            {
              Contact tempres=new Contact(Id=Responder.Id,EQS_Responder_Status__c='Available',EQS_Unavailable_Reason__c='',EQS_Unavailable_Start_Date__c=null,EQS_Unavailable_End_Date__c=null);
              UpdResponderUEDList.add(tempres);
            }

            update UpdResponderUEDList;
        }
    }