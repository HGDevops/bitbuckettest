/**
* Author : Kishore Kumar
* Created Date : 1/24/2017
* Purpose : This is used to Compare two line items
* Related Class: EFLCompareAmendLineitemsExtension
*/
@isTest(seealldata=false)
private class EFLCompareAmendLineitemsExtensionTest {
      @IsTest static void EFLCompareAmendLineitemsExtensionTest() {
          list<string> doctype = new list<string>();
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId; 
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);

          Application__c objapp1 = testData.newapplication();
          AC__c objac1 = testData.newLineItem('Personal Use',objapp1);               
          Applicant_Attachments__c objaa1 = testData.newAttach(objac1.id);
          Attachment objattach1 = testData.newAttachment(objaa1.id);  
          objac.Parent_Line_Item__c = objac1.id;
          update objac;
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') limit 1].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest();
              PageReference pageRef = Page.EFLCompareAmendLineitems;
              Test.setCurrentPage(pageRef);          
              ApexPages.currentPage().getParameters().put('id',objac.id); 
              ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objac);
              EFLCompareAmendLineitemsExtension extclassno1 = new EFLCompareAmendLineitemsExtension(sc2);  
              extclassno1.getFields();
              extclassno1.getlineItemDetails(objac.id);
              extclassno1.CompareTwolineItemRecords();
              extclassno1.redirect();
          Test.stopTest();   
      }
}