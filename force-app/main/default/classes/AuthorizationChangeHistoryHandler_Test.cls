// FIXME: too many soql queries error on location trigger
@isTest
public class AuthorizationChangeHistoryHandler_Test {
    private static List<sObject> inputs = new List<sObject>();
    static void initializeinput(){
        BRS_HistoryData_utility data = new BRS_HistoryData_utility();
        //inputs = data.dataCreation('Authorizations__c'); //getting sObject Location__c back
    }
    
    @isTest
    static void  testpopulateNewRecordDetails()//--------passing construct record from inputs and new change history record
    {
        Test.startTest(); //-------------------------------------------------------------------------Starting Test 
        initializeinput();
        Change_History__c change = new Change_History__c();
        string fieldName  = 'Name';
        string fieldLabel = 'Name';
        AuthorizationChangeHistoryHandler testing =  new AuthorizationChangeHistoryHandler();
        // FIXME: too many soql queries error on location trigger
       // testing.populateNewRecordDetails(inputs[0],change);
        //system.assert(change.New_Entry__c=true);
        //system.assert(inputs[0].id!=null);
        Test.stopTest(); 
        
    }
    
    @isTest
    static void  testpopulateUpdateDetails()//------------passing construct record from inputs and new change history record
    {
        initializeinput();
        Change_History__c change = new Change_History__c();
        Test.startTest(); //-------------------------------------------------------------------------Starting Test 
        string fieldName  = 'Name';
        string fieldLabel = 'Name';
        AuthorizationChangeHistoryHandler testing =  new AuthorizationChangeHistoryHandler();
        // FIXME: too many soql queries error on location trigger
       // testing.populateUpdateDetails(inputs[0],inputs[1],change);
        //  system.assert(change.Location__c !=null);
        //  system.assert(change.Line_Item__c!=null);
        //system.assert(inputs[0].id!=null);
        Test.stopTest(); 
        
    }
    
    @isTest
    static void  testpopulateDeletedRecordDetails()
    {
        initializeinput();
        Change_History__c change = new Change_History__c();
        Test.startTest(); //-------------------------------------------------------------------------Starting Test 
        string fieldName  = 'Name';
        string fieldLabel = 'Name';
        AuthorizationChangeHistoryHandler testing =  new AuthorizationChangeHistoryHandler();
        // FIXME: too many soql queries error on location trigger
        //testing.populateDeletedRecordDetails(inputs[0],change);
        //   system.assert(change.name == fieldLabel);
        //  system.assert(change.Field_API_Name__c   == fieldName); 
        //system.assert(change.Delete_Flag__c = true);
        Test.stopTest(); 
        
    }
    
    
}