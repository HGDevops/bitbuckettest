@isTest
public class EFLLabelExpiryTest {
    // note: not using system.runAs since this is a test resulting from a batch process having run.
    @TestSetup
    static void makeData(){
        string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        
        Application__c app = testData.newapplication();
        
        Authorizations__c auth = testData.newAuth(app.Id);
        auth.Expiration_Date__c = Date.today().addDays(10);
        auth.BRS_Introduction_Type__c = 'Import';
        //auth.Status__c = active;
        
        Application__c app2 = testData.newapplication();
        
        Authorizations__c auth2 = testData.newAuth(app.Id);
        auth2.Expiration_Date__c = Date.today().addDays(10);
        auth2.BRS_Introduction_Type__c = 'Import';
        //auth.Status__c = active;
        
        list<Authorizations__c> auths = new list<Authorizations__c>();
        auths.add(auth);
        auths.add(auth2);
        update auths;
        
        list<Label__c> labels = new list<Label__c>();
        for(Authorizations__c a:auths){
            Label__c l = new Label__c();
            l.Authorization__c = a.Id;
            l.Status__c = active;
            labels.add(l);
        }
        insert labels;
    }
    
    @isTest
    static void testExpireLabels(){
        list<Authorizations__c> auths = [SELECT Id,BRS_Introduction_Type__c,Status__c,Expiration_Date__c  FROM Authorizations__c];
        system.debug('auths: ' + auths);
        system.assertEquals(2, auths.size());
        
        list<Label__c> labelRes = [SELECT Id, Status__c FROM Label__c WHERE Authorization__c IN :auths];
        system.assertEquals(2, labelRes.size());
        
        string voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
        for(label__c l:labelRes){
            system.assertNotEquals(l.Status__c, voided);
        }
        
        auths[0].Expiration_Date__c = Date.today().addDays(-1);
        update auths[0];
        string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
        for(Label__c label :[SELECT Id, Name, Authorization__c, Authorization__r.BRS_Introduction_Type__c, Status__c,Authorization__r.Expiration_Date__c 
                             FROM Label__c]){
                                 system.debug('label:::');
                                 system.debug(label.Authorization__r.BRS_Introduction_Type__c);
                                 system.debug(label.Status__c);
                                 system.debug(label.Authorization__r.Expiration_Date__c);
                             }
        
        //system.assertEquals(1, labels.size());
        // run the batch, the label for today should be voided.
        EFLLabelExpiryBatch b = new EFLLabelExpiryBatch();
        test.startTest();
        Database.executeBatch(b);
        test.stopTest();
        
        // chck that one of them was voided.
        labelRes = [SELECT Id, Status__c, Authorization__c FROM Label__c WHERE Authorization__c IN :auths];
        system.assertEquals(2, labelRes.size());
        integer numchecked = 0;
        for(Label__c l:labelRes){
            if(l.Authorization__c == auths[0].Id){
                numchecked += 1;
                system.assertEquals(voided, l.Status__c);
            }
            if(l.Authorization__c == auths[1].Id){
                numchecked += 1;
                system.assertNotEquals(voided, l.Status__c);
            }
        }
        // this assertion verifies all of the labels in scope were checked:
        system.assertEquals(labelRes.size(), numChecked);
    }
    
    @isTest
    static void testExpireLabels_Neg(){
        list<Authorizations__c> auths = [SELECT Id FROM Authorizations__c];
        system.assertEquals(2, auths.size());
        
        for(Authorizations__c auth: auths){
            auth.BRS_Introduction_Type__c = null;
        }
        
        update auths; // THIS SHOULD REMOVE THE AUTHS FROM THE SCOPE OF THE BATCH CLASS AS THEY'RE FOR IMPORT TYPE ONLY.
        
        list<Label__c> labelRes = [SELECT Id, Status__c FROM Label__c WHERE Authorization__c IN :auths];
        system.assertEquals(2, labelRes.size());
        string voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
        for(label__c l:labelRes){
            system.assertNotEquals(l.Status__c, voided);
        }
        
        auths[0].Expiration_Date__c = Date.today();
        update auths[0];
        
        // run the batch, the label for today should be voided.
        EFLLabelExpiryBatch b = new EFLLabelExpiryBatch();
        test.startTest();
        Database.executeBatch(b);
        test.stopTest();
        
        // chck that none of them were voided.
        labelRes = [SELECT Id, Status__c, Authorization__c FROM Label__c WHERE Authorization__c IN :auths];
        system.assertEquals(2, labelRes.size());
        integer numchecked = 0;
        for(Label__c l:labelRes){
            system.assertNotEquals(voided, l.Status__c);
        }
    }
    
    @isTest
    static void verifySchedulerClass(){
        list<Authorizations__c> auths = [SELECT Id FROM Authorizations__c];
        system.assertEquals(2, auths.size());
        
        list<Label__c> labelRes = [SELECT Id, Status__c FROM Label__c WHERE Authorization__c IN :auths];
        system.assertEquals(2, labelRes.size());
        string voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
        for(label__c l:labelRes){
            system.assertNotEquals(l.Status__c, voided);
        }
        
        auths[0].Expiration_Date__c = Date.today();
        update auths[0];
        
        Test.startTest();
        String CRON_EXP = '0 0 0 3 9 ? 2050';
        
        // Schedule the test job
        
        String jobId = System.schedule('testBasicScheduledApex', CRON_EXP, new EFLLabelExpirySchedule());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger 
                          WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
        System.assertEquals('2050-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();   
    }
}