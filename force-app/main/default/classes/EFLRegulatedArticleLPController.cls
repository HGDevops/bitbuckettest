public with sharing class EFLRegulatedArticleLPController {
    //Author:Ravee Racharla Date:9/27/2018
    //Description: This class is used for custom look up page 
    //Get Parameters
    public Id LineItemId=ApexPages.currentPage().getParameters().get('lineItemId');
    
    public AC__c lineItemRecord{get{
        return EFLLineItemRepository.selectbyID(lineItemId);
    }
                                set;}
    public list<Regulated_Article__c> results{get; set;} 
    
    public string searchString{get;set;}
    public string selectedRAId{get; set;}
    
    private string strPathwayId; 
    private string isRegulatedArticlePlant; 
    static final string PLANT = 'Plant';
    static final string YES = 'Yes';
    static final string NO = 'No';
    
    //Attributes
    static final string ACTIVE = 'Active';
    
    //Constructor 
    public EFLRegulatedArticleLPController(){
        LineItemId=ApexPages.currentPage().getParameters().get('LineItemId');
        lineItemRecord =EFLLineItemRepository.selectbyID(lineItemId);
        strPathwayId = lineItemRecord.Program_Line_Item_Pathway__c;
        isRegulatedArticlePlant = lineItemRecord.Is_your_regulated_article_a_plant__c;
        
        //system.debug('LineItemID  ' + LineItemID); 
        results = performSearch(searchString); //display default on page load 
    }
    
    
    public PageReference search(){
        runSearch();
        return null;
    }
    
    private void runSearch() {
        
        results = performSearch(searchString);               
    } 
    
    @TestVisible
    private list<Regulated_Article__c> performFind(string searchString){
        list<Regulated_Article__c> lstRA = new list<Regulated_Article__c>() ;
        if (searchString.length() > 1 ) {
            String searchStr1 = '*'+searchString+'*';
            String searchQuery = 'FIND \'' + searchStr1 + '\' IN ALL FIELDS RETURNING  Regulated_Article__c (id,Name,Scientific_Name__c,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c,Category_Group_Ref__r.Name,Category_Group_Ref__c,Createddate)';
            List<List <sObject>> searchList = search.query(searchQuery);
            lstRA = ((List<Regulated_Article__c>)searchList[0]);
        }
        return lstRA; 
    }
    private list<Regulated_Article__c> performSearch(string searchString){
        set<id> sLRAId = New set<id>(); 
        // sLRAId = getLinkedRegArticles () ;
        
        Set<Id> programpathwayIDs = new set<Id>{strPathwayId};
            Set<Id> categoryIDs = prepareCategoryIDs(isRegulatedArticlePlant);
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Regulated_Article__c');
            String soql = 'select id,Name,Scientific_Name__c,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c,Category_Group_Ref__r.Name,Category_Group_Ref__c,Createddate from Regulated_Article__c where status__c = \'' + ACTIVE + '\'';   
            if(categoryIds!=null){
                soql+= ' and Category_Group_Ref__c  in : categoryIds ';
            }
            if(programPathwayIds!=null){
                list<id> regulatedArticleIds = EFLRegulatedArticleRepository.selectRegulatedArticleIdsByPathwayIds(programPathwayIds);
                if(regulatedArticleIds!=null){ 
                    soql+= ' and id in : regulatedArticleIds ';}
            }
            /* 
if(sLRAId != null && sLRAId.size()> 0 ){
soql+= ' and id not in :sLRAId ' ; 
} */
            if(searchString != '' && searchString != null){
                soql +=  ' and (name LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) +'%\'';
                soql +=  ' or Category_Group_Ref__r.Name LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) +'%\'';
                soql +=  ' or Common_Name__c LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) +'%\'';
                soql +=  ' or Additional_Scientific_Name__c LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) +'%\'';
                soql +=  ' )' ; 
            }
            
            soql+= ' Order By Name ';
            
            list<Regulated_Article__c> listRA = Database.query(soql);  
            return listRA; 
        }catch(exception e){
            // EFLErrorLog.createErrorLog('EFLRegulatedArticleRepository.selectRegulatedArticlesByPathwayIDsandCategoryIDs()',e);                
        } 
        return null;
        /*
String soql = 'select id,Category__c, Name, Common_Name__c, Additional_Scientific_Name__c  from Regulated_Article__c where status__c = \'' + ACTIVE + '\'';
if(sLRAId != null && sLRAId.size()> 0 ){
soql+= '  AND id not IN ' +  sLRAId ; 
}
soql+= ' Order By Name ';
//system.debug(soql); 
list<Regulated_Article__c> listRA = Database.query('select id,Category__c, Name, Common_Name__c from Regulated_Article__c where status__c =:ACTIVE and id not in :sLRAId');  
return listRA; 
*/
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    //Get existing Linked Reg Articles and exclude from search
    @TestVisible
    private set<id>  getLinkedRegArticles(){
        list<Link_Regulated_Articles__c> lraList = new list<Link_Regulated_Articles__c>(
            
            [SELECT id,  Regulated_Article__c
             FROM Link_Regulated_Articles__c 
             WHERE Line_Item__c = :LineItemId]);
        set<id> lraSet = New set<id>(); 
        if( lraList != Null && lraList.size() > 0 ) {
            for (Link_Regulated_Articles__c ra  :lraList){
                lraset.add(ra.Regulated_Article__c); 
            }
        }
        return lraSet ; 
    }  
    
    private set<Id> prepareCategoryIDs(string isRegulatedArticlePlant){
        if(isRegulatedArticlePlant == YES){
            Set<Id> catIDs = new Map<Id, group__c>([SELECT Id 
                                                    FROM Group__c 
                                                    WHERE Name =: PLANT
                                                    LIMIT 1 ]).keySet();
            return catIDs; 
        }else if(isRegulatedArticlePlant == NO){
            Set<Id> catIDs = new Map<Id, group__c>([SELECT Id 
                                                    FROM Group__c 
                                                    WHERE Name !=: PLANT
                                                   ]).keySet();
            return catIDs;            
        }
        return null;
    }
}