@isTest
public class EFLManageAddlStops_Test{
    @isTest
    public static void EFLManageAddlStops_TestMethod1(){
        //Test Data
        CARPOL_AC_TestDataManager dataObj = new CARPOL_AC_TestDataManager ();
        dataObj.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c  newApp = dataObj.newapplication();
        AC__c  newLine = dataObj.newLineItem('Personal Use', newApp);
        
        //URL Params
        PageReference pgRef = Page.EFLManageAddlStops;
        Test.setCurrentPage(pgRef);
        ApexPages.currentPage().getParameters().put('lineitemid',newLine.id);
        //ApexPages.currentPage().getParameters().put('strPathway',);
        //ApexPages.currentPage().getParameters().put('ScientificName', newLine.Scientific_Name__c);
        ApexPages.currentPage().getParameters().put('portofexit',newLine.Port_of_Embarkation__c );
        ApexPages.currentPage().getParameters().put('PortofEntry',newLine.Port_of_Entry__c);
        ApexPages.currentPage().getParameters().put('DateofDep',String.valueOf(newLine.Departure_Time__c ));
        ApexPages.currentPage().getParameters().put('DateofArr',String.valueOf(newLine.Proposed_date_of_arrival__c ));

        //Constructor and Method Calls
        EFLManageAddlStops addlStopsInst = new EFLManageAddlStops();
        addlStopsInst.Add();
        addlStopsInst.Cancel();
        addlStopsInst.CancelTransitLocal();
        addlStopsInst.CancelTransitLocalInt();
        addlStopsInst.SaveTransitLocal();
        addlStopsInst.removeTransit();
        addlStopsInst.deleteTransit();
        /*
        
        addlStopsInst.deleteSubSeqTransit();
        */
        
    }
}