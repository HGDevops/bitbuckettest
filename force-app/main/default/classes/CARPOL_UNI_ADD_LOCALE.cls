public class CARPOL_UNI_ADD_LOCALE{
    
    public List<Transit_Locale__c> lstTransit  = new List<Transit_Locale__c>();
    public List<transitInnerClass> transitInner{get;set;}
    public String selectedRowIndex{get;set;}  
    public Integer count = 1;
    public string LineItem;
    public string appId;
    public String countryofOrigin; //added by Niharika
    public String ScientificName;
    
    public CARPOL_UNI_ADD_LOCALE(){
        LineItem = ApexPages.currentPage().getParameters().get('LineItemId');
        countryofOrigin = ApexPages.currentPage().getParameters().get('countryofOrigin'); //added by niharika
         ScientificName = ApexPages.currentPage().getParameters().get('ScientificName');
        AC__c objLineItem = [Select Application_Number__c From AC__c Where id=: LineItem];
        appId = objLineItem.Application_Number__c;
        transitInner = new List<transitInnerClass>();
        addMore();
        selectedRowIndex = '0';
   }
    
    public void Add(){   
        transitInner[count-1].addTransitBtn = false;
        count = count+1;
        addMore();      
    }
    
    public void addMore(){
        transitInnerClass objInnerClass = new transitInnerClass(count,countryofOrigin,ScientificName);
        transitInner.add(objInnerClass);    
        system.debug('transitInner---->'+transitInner);            
    }
     
     public class transitInnerClass{       
        public String recCount{get;set;}
        public Transit_Locale__c transit{get;set;}
        public boolean addTransitBtn{get;set;}
        //public transitInnerClass(Integer intCount){
        
        // updated by Niharika to autopopulate first record of fromcountry.
        public transitInnerClass(Integer intCount,String countryOrigin,String ScientificName){
            recCount = String.valueOf(intCount);        
            transit = new Transit_Locale__c();
           transit.Regulated_Article__c    =ScientificName;
            if(intCount == 1)
            transit.From_Country__c=countryOrigin;
            addTransitBtn = true;
        }   
    }
     
    public PageReference SaveTransitLocal(){
        PageReference pgRef;
        try{
            for(Integer j = 0;j<transitInner.size();j++)
            {
                Transit_Locale__c objTransit = transitInner[j].transit;
                objTransit.Line_Item__c = LineItem;
                lstTransit.add(objTransit);
            } 
            system.debug('lstTransit==='+lstTransit);
            insert lstTransit;
            pgRef = new PageReference('/apex/portal_application_detail?id='+appId);
            pgRef.setRedirect(True);
        }catch(Exception Ex){
            system.debug('Exception :::'+Ex);
        }
        return pgRef;
    }
    
     public PageReference CancelTransitLocal(){
        PageReference pgRef = new PageReference('/apex/portal_application_detail?id='+appId);
        return pgRef;
    }      
}