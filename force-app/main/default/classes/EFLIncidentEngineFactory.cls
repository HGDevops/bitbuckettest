public class EFLIncidentEngineFactory { 
  /*
    * Purpose: Get Incident Engine
    */ 
    public static EFLIIncidentEngine getEngine(Incident__c IncidentRecord){
       
        EFLIIncidentEngine Engine; 
        Engine = getIncidentEngine(IncidentRecord);
         
        if(Engine==null){
            throw new EFLIncidentEngineException('No Incident Engine found for Incident: ' + IncidentRecord.Name);
            
        }
        
        return Engine;
      }  
   
   /*
    * Purpose: Get Incident Engine based on program
    * Note: Enable the commented code once other Program Engines are implemented
    */     
    private static EFLIIncidentEngine getIncidentEngine(Incident__c IncidentRecord){
         if (IncidentRecord.Related_Program__c == 'BRS')
		{ 
			  return new EFLBRSIncidentEngine(); 
		}/*else if (IncidentRecord.Related_Program__c == 'AC')
		{ 
			//return new EFLACIncidentEngine();
 
		}else if (IncidentRecord.Related_Program__c == 'VS') 
		{
			//return new EFLVSIncidentEngine();
 
		}else if (IncidentRecord.Related_Program__c == 'PPQ')
		{
			//return new EFLPPQIncidentEngine();
		}*/
         else
          return new EFLBRSIncidentEngine();

    }  
    
    public class EFLIncidentEngineException extends Exception {}    

}