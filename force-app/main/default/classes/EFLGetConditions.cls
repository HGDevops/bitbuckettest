/**
*Author : Kishore Kumar
*Date Created : 2/13/2016
*Purpose : Get Conditions per line item of an application
**/
public inherited sharing class EFLGetConditions{
    
    public static string getconditions(Map<string,string> selectedAnswers,string SelectedPathwayId){
        /** Attributes **/
        List<Regulations_Association_Matrix__c> DecisionMatrixList=new  List<Regulations_Association_Matrix__c>();
        List<Regulations_Association_Matrix__c> dmList=new  List<Regulations_Association_Matrix__c>();
        list<Program_Line_Item_Pathway__c> paths=new  list<Program_Line_Item_Pathway__c>();
        list<Signature_Regulation_Junction__c> RJList=new  list<Signature_Regulation_Junction__c>();
        Set<ID> RegulationIDs=new  Set<ID>();
        Set<ID> CountryGroupIds=new  Set<ID>();
        Set<ID> FinalRAGroupIds;
        if(selectedAnswers.get('Regulated_Article_Group__c') != null && selectedAnswers.get('Regulated_Article_Group__c') != '')
            FinalRAGroupIds = (Set<ID>) Json.deserialize(selectedAnswers.get('Regulated_Article_Group__c'), Set<ID>.class);
        if(FinalRAGroupIds == null)
            FinalRAGroupIds = new Set<ID>();
        Set<String> cntryorigdiseases;
        if(selectedAnswers.get('EFLDiseaseSensitivity__c') != null && selectedAnswers.get('EFLDiseaseSensitivity__c') != '')
            cntryorigdiseases = (Set<String>) Json.deserialize(selectedAnswers.get('EFLDiseaseSensitivity__c'), Set<String>.class);
        if(cntryorigdiseases == null)
            cntryorigdiseases = new Set<String>();
        String conditionslist;
        //system.debug('disease sensitivity >>>>>>> ' + selectedAnswers.get('EFLDiseaseSensitivity__c'));
        //system.debug('component  >>>>>>> ' + selectedAnswers.get('Component__c'));
        //system.debug('regulated article group  >>>>>>> ' + selectedAnswers.get('FinalRAGroupIds'));
        
        /** Obtain the Country Junction records for the selected 
Country of Origin which helps in getting the related Country Groups **/
        List<Country_Junction__c> CountryJunctions=[SELECT Name,Country__c,Group__c FROM Country_Junction__c WHERE Country__c=:selectedAnswers.get('Country_Of_Origin__c') AND RecordType.DeveloperName = 'Group_and_Country'];
        
        for(Country_Junction__c country:CountryJunctions)
            CountryGroupIds.add(country.Group__c);
        
        /** Obtain the Program Pathway & its Parent Pathways **/
        paths=[SELECT Name,ID,All_Parent_Pathways__c,Show_Conditions_Required_for__c FROM Program_Line_Item_Pathway__c WHERE ID=:SelectedPathwayId];
        
        string res=paths[0].All_Parent_Pathways__c;
        string[] results=res.split(',');
        //system.debug('selectedAnswers>>>'+selectedAnswers);
        /**  Obtain list of Decision Matrix record for the Selection made on the Wizard  **/
        // for(String s: results) 
        // {
        dmList=[SELECT Id, EFLDiseaseSensitivity__c, Program_Line_Item_Pathway__r.EFLDiseaseValidationRequired__c,
                Program_Line_Item_Pathway__c
                From Regulations_Association_Matrix__c 
                // where Program_Line_Item_Pathway__c =: s 
                where Program_Line_Item_Pathway__c in: results 
                AND (Movement_Type__c includes (:selectedAnswers.get('Movement_Type__c')) OR Movement_Type__c = '') 
                AND (Country__c =: selectedAnswers.get('Country_Of_Origin__c') OR Country__c = '')
                AND (Level_1_Region__c =:selectedAnswers.get('State_Province_of_Origin__c') OR Level_1_Region__c = '') 
                AND (Regulated_Article__c =: selectedAnswers.get('Scientific_Name__c') OR Regulated_Article__c = '')
                AND (Intended_Use__c =:selectedAnswers.get('Intended_Use__c') OR Intended_Use__c = '')
                AND (Method_of_Transportation__c =:selectedAnswers.get('Transporter_Type__c') OR Method_of_Transportation__c = '') 
                AND (Country_Group__c IN: CountryGroupIds OR Country_Group__c='')
                AND (Breed__c =:selectedAnswers.get('Breed__c') OR Breed__c = '')
                AND (Export_State_Province__c =:selectedAnswers.get('Exporter_Mailing_State_ProvinceLU__c') OR Export_State_Province__c = '')
                AND (Level_2_Region__r.Name =:selectedAnswers.get('Exporter_Mailing_City__c') OR Level_2_Region__r.Name = '')
                AND (Thumbprint__c =:selectedAnswers.get('Thumbprint__c') OR Thumbprint__c = '')
                AND (Component__c =:selectedAnswers.get('Component__c') OR Component__c = '')
                AND (Regulated_Article_Group__c in: FinalRAGroupIds OR Regulated_Article_Group__c = '')
               ];
        DecisionMatrixList.addall(getDmListAndDiseases(dmList, cntryorigdiseases));
        // }
        //system.debug('DM List'+DecisionMatrixList);
        /** Obtain list of Conditions that are configured for each Decision matrix Record **/
        RJList=[SELECT ID,Decision_Matrix__c,Regulation__c,Regulation__r.Id,Regulation__r.Regulation_Description__c,Signature__c,Order_Number__c FROM Signature_Regulation_Junction__c WHERE Decision_Matrix__c in:DecisionMatrixList order by Order_Number__c ASC];
        
        for(Signature_Regulation_Junction__c srj:RJList)
            RegulationIDs.add(srj.Regulation__c);
        
        for(string s:RegulationIDs){
            if(conditionslist==null)
                conditionslist=s+'_';
            else conditionslist+=s+'_';
        }
        /**Return Condition ID's **/
        //system.debug ('conditionslist>>>'+conditionslist);
        return conditionslist;
    }
    
    private static List<Regulations_Association_Matrix__c> getDmListAndDiseases(List<Regulations_Association_Matrix__c> dmList, Set<String> cntryorigdiseases){
        
        //system.debug('DM DISEASE CODE >>>>>>>>>>');
        List<Regulations_Association_Matrix__c> temp = new List<Regulations_Association_Matrix__c>();
        Integer listSize = 0;
        for(Regulations_Association_Matrix__c dm: dmList){
            if(dm.Program_Line_Item_Pathway__r.EFLDiseaseValidationRequired__c && dm.EFLDiseaseSensitivity__c != '' && dm.EFLDiseaseSensitivity__c != null){
                String[] tmpString = dm.EFLDiseaseSensitivity__c.split(';');
                if(cntryorigdiseases.containsAll(tmpString)){ //make sure all disease values are in the list we got
                    if(tmpString.size() >= listSize){ //ranking for the best match
                        listSize = tmpString.size();
                        temp.add(dm);
                    }
                    
                }
            }
            else
                temp.add(dm);
        }
        return temp;
    }
    
    public static List<Authorization_Junction__c> getAuthorizationRelatedConditions(id authorizationId, string conditionType, id officialReviewerId ){
        List<Authorization_Junction__c> authRelatedConditions = new List<Authorization_Junction__c>();
        string stateType = 'State Conditions';
        string nulltype = null;
        string stateRecordTypeId = Schema.Sobjecttype.Authorization_Junction__c.getRecordTypeInfosByName().get('State Regulation Junction').getRecordTypeId(); 
        
        String soql = 'select id, is_Active__c,Title__c, Authorization__c,Type__c,Sub_Type__c,Order_Number__c,Official_Review_Record__c, Regulation_Short_Name__c,Regulation_Description__c from Authorization_Junction__c where Authorization__c = \'' + authorizationId + '\'';    
        
        if(officialReviewerId!=null){
            soql+= ' and Official_Review_Record__c = \'' + officialReviewerId + '\'';
        }
        
        if(conditionType == 'Federal Conditions'){
            soql+= ' and ( Type__c != \'' + stateType + '\'' + ' and Type__c != null ) '; 
        }else if(conditionType == 'State Conditions'){
            soql+= ' and RecordTypeId = \'' + stateRecordTypeId + '\''; 
        } 
        soql += ' Order By Sub_Type__c, Order_Number__c ';	
        authRelatedConditions = Database.query(soql);  
        return authRelatedConditions; 
    }
    
}