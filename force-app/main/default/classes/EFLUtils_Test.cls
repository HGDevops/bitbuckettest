@isTest
public class EFLUtils_Test {

    @TestSetup
    static void makeData(){
        Account account = new Account();
        account.Name = 'Test Account';
        account.Industry = 'Agriculture';
        account.Type = null;
        insert account;
        
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.AccountId = account.Id;
        insert contact;
    }

    @isTest
    public static void getAccountIndustryValue_ValueReturned(){
        Account account = [Select Id, Name, Industry From Account Limit 1];
        
        String industry = EFLUtils.getSObjectField(account, 'Industry');
        System.assertEquals(account.Industry, industry);
    }

    @isTest
    public static void getInvalidFieldValue_NullReturned(){
        Account account = [Select Id, Name, Industry From Account Limit 1];
        
        String industry = EFLUtils.getSObjectField(account, 'AccentureFederalServices');
        System.assertEquals(null, industry);
    }

    @isTest
    public static void getParentFieldValue_ValueReturned(){
        Contact contact = [Select Id, Account.Industry From Contact Limit 1];
        
        String industry = EFLUtils.getSObjectField(contact, 'Account.Industry');
        System.assertEquals(contact.Account.Industry, industry);
    }

    @isTest
    public static void getNullParentFieldValue_NullReturned(){
        Contact contact = [Select Id, Account.Type From Contact Limit 1];
        
        String accountType = EFLUtils.getSObjectField(contact, 'Account.Type');
        System.assertEquals(null, accountType);
    }

    @isTest
    public static void getThreePartValue_ValueReturned(){
        Contact contact = [Select Id, Account.Industry, Account.CreatedBy.Name From Contact Limit 1];
        
        String createdByName = EFLUtils.getSObjectField(contact, 'Account.CreatedBy.Name');
        System.assertEquals(contact.Account.CreatedBy.Name, createdByName);
    }
    
    @isTest
    public static void getInvalidTwoPartFieldValue_NullReturned(){
        Contact contact = [Select Id, Account.Industry, Account.CreatedBy.Id From Contact Limit 1];
        
        String createdById = EFLUtils.getSObjectField(contact, 'AccentureFederalServices.AccentureFederalServices');
        System.assertEquals(null, createdById);
    }

    @isTest
    public static void getPicklistValues_ValuesReturned(){

		List<Schema.PicklistEntry> values = Account.Industry.getDescribe().getPicklistValues();
        List<EFLUtils.LightningPicklistOption> picklistOptions = EFLUtils.getPicklistOptions('Account', 'Industry');
            
        System.assertEquals(values.size(), picklistOptions.size());
    }    
}