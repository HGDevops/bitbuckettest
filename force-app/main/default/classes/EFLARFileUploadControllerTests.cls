@isTest
public class EFLARFileUploadControllerTests {

    private static final string AccountName = 'Test Account';
    private static final string AccountDescription = 'Test Account Description';
    private static final string ContactLastName = 'Test';
    private static final string filterLogic = 'Test Usage Key';
    
    @TestSetup
    static void makeData(){
        // Make a parent object for Uploaded_File__c to be created under in test execution.
        Account account = new Account();
        account.Name = AccountName;
        account.Description = AccountDescription;
        insert account;
        
        Contact contact = new Contact();
        contact.LastName = ContactLastName;
        contact.AccountId = account.Id;
        insert contact;

        Uploaded_File__c uf = new Uploaded_File__c();
        uf.Name = 'Test';
        uf.ParentId__c = contact.Id;
        uf.Animal_Type__c = '';
        uf.Description__c = '';
        uf.External_File_Id__c = 'TestExtFileId';
        uf.File_Category__c = 'Exception';
        uf.File_Preview_Id__c = '';
        uf.File_Type__c = '';
        uf.Full_File_Name__c = '';
        uf.Integration_Provider__c = 'SpringCM';
        insert uf;

        uf = new Uploaded_File__c();
        uf.Name = 'Test 2';
        uf.ParentId__c = contact.Id;
        uf.Animal_Type__c = '';
        uf.Description__c = '';
        uf.External_File_Id__c = 'TestExtFileId';
        uf.File_Category__c = 'Exception';
        uf.File_Preview_Id__c = '';
        uf.File_Type__c = '';
        uf.Full_File_Name__c = '';
        uf.Integration_Provider__c = 'SpringCM';
        insert uf;
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'TestDocument',
            PathOnClient = 'TestDocument.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [Select Id, Title, LatestPublishedVersionId From ContentDocument];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = account.Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;        
    }
    
    @isTest
    static void testGetParentIds(){
        EFLAnnual_Report__c ar = new EFLAnnual_Report__c();
        insert ar;
        
        EFLRegistered_Animal__c ra = new EFLRegistered_Animal__c();
        ra.EFLAnnual_Report__c = ar.Id;
        ra.EFLSelectedForReport__c = true;
        insert ra;
        
        list<Id> ids = EFLARFileUploadController.getParentIds(ar.Id);
        system.assertEquals(2, ids.size());
        set<Id> myIds = new set<Id>();
        myIds.addAll(ids);
        system.assertEquals(true, myIds.contains(ar.Id));
        system.assertEquals(true, myIds.contains(ra.Id));
    }
    
    @isTest
    static void testGetPageData(){
        EFLAnnual_Report__c ar = new EFLAnnual_Report__c();
        insert ar;
        
        EFLRegistered_Animal__c ra = new EFLRegistered_Animal__c();
        ra.EFLAnnual_Report__c = ar.Id;
        ra.EFLSelectedForReport__c = true;
        insert ra;
        
        EFLARFileUploadController.PageData pd = EFLARFileUploadController.getPageData(ar.Id);
        
        list<Id> ids = pd.parentIds;
        system.assertEquals(2, ids.size());
        set<Id> myIds = new set<Id>();
        myIds.addAll(ids);
        system.assertEquals(true, myIds.contains(ar.Id));
        system.assertEquals(true, myIds.contains(ra.Id));
    }
    
    @isTest
    static void testSetupNewFileWithParams(){
        Account account = [Select Id, Name From Account Limit 1];
        ContentDocument document = [Select Id, Title, FileType From ContentDocument Limit 1];
        
        EFLFileListController.LightningFileUpload lfu = new EFLFileListController.LightningFileUpload();
        lfu.name = 'testSetupNewFile';
        lfu.documentId = document.Id;
        List<EFLFileListController.LightningFileUpload> lfuList = new List<EFLFileListController.LightningFileUpload>{lfu};
        String cd = JSON.serialize(lfuList);
        
        list<EFLARFileUploadController.FieldMapping> mappings = new list<EFLARFileUploadController.FieldMapping>();
        EFLARFileUploadController.FieldMapping m = new EFLARFileUploadController.FieldMapping();
        m.fieldName = 'External_File_Id__c';
        m.fieldValue = 'Test';
        mappings.add(m);
        
        EFLARFileUploadController.setupNewFileWithParams(JSON.serialize(lfuList), account.Id, filterLogic, JSON.serialize(mappings));

        List<Uploaded_File__c> ufs = [SELECT Id, Name, ParentId__c, File_Preview_Id__c, File_Type__c, File_Category__c, Description__c, External_File_Id__c
                                      FROM Uploaded_File__c 
                                      WHERE Name = :document.Title];

        System.assertEquals(1, ufs.size());
        System.assertEquals(document.Title, ufs[0].Name);
        System.assertEquals('Other', ufs[0].File_Category__c);
        System.assertEquals(account.Id, ufs[0].ParentId__c);
        System.assertEquals(document.Id, ufs[0].File_Preview_Id__c);
        System.assertEquals(document.FileType, ufs[0].File_Type__c);
        System.assertEquals('Test', ufs[0].External_File_Id__c);
    }
    
    @isTest
    static void testFieldMapping(){
        EFLARFileUploadController.FieldMapping fm = new EFLARFileUploadController.FieldMapping();
        fm.fieldName = 'test';
        fm.fieldValue = 'test';
        system.assertEquals('test',fm.fieldName);
        system.assertEquals('test',fm.fieldValue);
    }
}