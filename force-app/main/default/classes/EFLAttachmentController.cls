public with sharing class EFLAttachmentController {
    public Id lineItemId {get;set;}
    public String lineItemName {get; set;}
    public String springCMFlag {get; set;}
    
    public string instructions{
        
        get{
            if(instructions == NULL){
                //system.debug('lineItemRecord.type_of_permit__c@@'+lineItemRecord.type_of_permit__c);
                instructions =  EFLGenericutility.getInstructions(NULL,lineItemRecord.type_of_permit__c,'SOP & Attachments');  
                //system.debug('Inside instructions');
                return instructions;
            } 
            return instructions; 
        }
        set;
    }  
    
    
    
    public AC__c lineItemRecord{      
        get{
            if(lineItemRecord==null && LineItemId!=null){ 
                lineItemRecord = efllineitemrepository.selectbyID(lineItemID);
            }
            return lineItemRecord;
        }set;
    }
    //render Attributes
    public boolean renderList{get;set;}
    public boolean renderDetail{get;set;}
    public Boolean ContainsCBI {get;set;}
    public Boolean ContainsSpring {get;set;}
    
    public EFLAttachmentController(ApexPages.StandardController controller){ 
        lineItemId= EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('LineItemId'));
        //system.debug('LineItemId@@'+ LineItemId);
        renderDetail = false;
        renderList = true;  
        ContainsCBI = true;
        ContainsSpring =true;
        
        
    } 
    
    public void showDetail(){
        
        renderDetail = true;
        renderList = false; 
        //system.debug('renderDetail####'+renderDetail);
    }
    
    public void cancel(){
        renderDetail = false;
        renderList = true;
    } 
    
    public String fireDocLauncher(){
        
        String URL = EFLSpringCMUtility.buildDocLauncherURL(lineItemId, lineItemRecord.Name, '', 'Attachment');
        //system.debug('URL@@'+URL);
        return URL;
    }
    
    
}