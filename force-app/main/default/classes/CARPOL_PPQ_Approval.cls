@RestResource(urlMapping='/carpolppqattach/*')
global with sharing class CARPOL_PPQ_Approval{

     @HttpPost
    global static void sendEmail(string appid,string authid, string decisiontype) {
    
    try{
        
        PageReference pdf = new PageReference('');
        Boolean attachPermit= false;
        
        //formated Date for the final permit naming
        DateTime day = Date.Today();
        String formatedDate = day.addDays(1).format('MM-dd-YYYY');
        String[] fDate = formatedDate.split('-');
        String finalDate = fDate[2]+fDate[0]+fDate[1];
        system.debug('finalDate-->'+finalDate);
        
        Authorizations__c auth = [SELECT Id, Name, Thumbprint__r.Program_Prefix__r.name, CITES_Needed__c, Regular_Permit_Needed__c, 
                                    Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c, Application__c, Program_Pathway__r.Permit_PDF_Template__c, 
                                    Status__c, Authorization_Type__c, Thumbprint__c,RecordType.Name, Permit_Number__c, Application__r.Name, Final_Permit_Name_CBP__c  
                                    FROM Authorizations__c WHERE Id =:authid];
        Attachment atch = new Attachment();       
        
        
     //  system.debug('--appstatus---'+appstatus);
        List<Workflow_Task__c> wrkFlowTaskSTP = New List<Workflow_Task__c>();
        wrkFlowTaskSTP = [Select Id, Name,  Status__c from Workflow_Task__c where Authorization__c =:auth.id and Name = 'Issue Standard Permit' and Status__c = 'Deferred'];    
      
      system.debug('<! decisiontype '+decisiontype);
        system.debug('<! wrkFlowTaskSTP.size() '+wrkFlowTaskSTP.size());
        system.debug('<! Regular_Permit_Needed__c '+auth.Regular_Permit_Needed__c);
        system.debug('<! CITES_Needed__c '+auth.CITES_Needed__c);
        
        if((decisiontype == 'Permit' && wrkFlowTaskSTP.size()==0) || (auth.Regular_Permit_Needed__c ))   //&& auth.CITES_Needed__c
        { 
          if(auth.Id!=null)
          {
             if (auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null){ // VV added on 7-12-16 for PEQ VF for template
                 //VV added 7/14/16 to auto attach a draft permit if the applicant is a violator for PEQ only
                  if (auth.status__c == 'Issued'){
                     system.debug('It is in 1');
                     pdf= new PageReference('/apex/'+auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c);   
                     atch.name='Permit.pdf';    
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                       
                     }else{
                     //VV end additions on 7/14/16
                    pdf= new PageReference('/apex/'+auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c+'_DRAFT'); 
                    system.debug('---pdf---'+pdf);  
                    atch.name='Draft Permit.pdf';         
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                                            
                   } 
                }else if(auth.Program_Pathway__r.Permit_PDF_Template__c!=null && !auth.CITES_Needed__c){
                 system.debug('It is in 2');
                   pdf = new PageReference('/apex/'+auth.Program_Pathway__r.Permit_PDF_Template__c);
                    //atch.name='Permit.pdf';
                    if(auth.Permit_Number__c != null && auth.Permit_Number__c != ''){
                       atch.name= auth.Permit_Number__c+'_'+auth.Application__r.Name+'_'+finalDate+'.pdf';  // added by Dinesh 9/15 
                    }else{
                        atch.name= auth.Application__r.Name+'_'+finalDate+'.pdf';  // added by Dinesh 9/15          
                    }
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                             
                  
                }else if(auth.Program_Pathway__r.Permit_PDF_Template__c!=null && auth.CITES_Needed__c && auth.Thumbprint__r.Program_Prefix__r.name == '211'){
                    system.debug('It is in 3');
                   pdf = new PageReference('/apex/'+auth.Program_Pathway__r.Permit_PDF_Template__c);
                    //atch.name='Permit.pdf';     
                    if(auth.Permit_Number__c != null && auth.Permit_Number__c != ''){
                       atch.name= auth.Permit_Number__c+'_'+auth.Application__r.Name+'_'+finalDate+'.pdf';  // added by Dinesh 9/15 
                    }else{
                        atch.name= auth.Application__r.Name+'_'+finalDate+'.pdf';  // added by Dinesh 9/15          
                    }
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                             
                  } /*                 
              else if (auth.Program_Pathway__r.Permit_PDF_Template__c == null && !auth.CITES_Needed__c)
                   {
                    system.debug('It is in 4');
                    pdf = Page.CARPOL_PPQ_PermitPDF;
                    //atch.name='Permit.pdf'; 
                    if(auth.Permit_Number__c != null && auth.Permit_Number__c != ''){
                       atch.name= auth.Permit_Number__c+'_'+auth.Application__r.Name+'_'+finalDate+'.pdf';  // added by Dinesh 9/15 
                    }else{
                        atch.name= auth.Application__r.Name+'_'+finalDate+'.pdf';  // added by Dinesh 9/15          
                    }
                    pdf.getParameters().put('id',authid);
                    attachPermit = true;                                                        
                   }*/
              auth.Final_Permit_Name_CBP__c = atch.name;
              update auth;
           }
          

          // pdf.getParameters().put('id',authid);
           //attachPermit = true;
        } 
        
        else if(decisiontype == 'Letter of Denial')   
        { 
          
          pdf = Page.CARPOL_Standard_Formal_Letter;
          atch.name='Letter Of Denial.pdf';
          pdf.getParameters().put('authid',authid); // VV 3-20-16
          pdf.getParameters().put('id',appid);
          attachPermit=true;
                    
        } 
        else if(decisiontype == 'Letter of No Permit Required')   
        { 
          pdf = Page.CARPOL_Standard_Formal_Letter;
          atch.name='Letter Of No Permit Required.pdf';
          pdf.getParameters().put('id',appid);
          pdf.getParameters().put('authid',authid); // VV 3-20-16
          attachPermit=true;
        } 
        else if(decisiontype == 'Letter of No Jurisdiction')   
        { 
          pdf = Page.CARPOL_Standard_Formal_Letter;
          atch.name='Letter of No Jurisdiction.pdf';
          pdf.getParameters().put('id',appid);
          pdf.getParameters().put('authid',authid); // VV 3-20-16
          attachPermit=true;
        }  
        

        
      //  pdf.getParameters().put('id',authid);
        if(attachPermit)
        {
         system.debug('pdf is '+ pdf);
            system.debug('It is here finally');
            pdf.setRedirect(true);
            
         // Take the PDF content
            Blob b= (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContent());
            //Blob b= (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContentAsPDF());
            //Blob b= pdf.getContent();
            atch.ParentId = authid;//appid;
            atch.body = b;
            //atch.name='Permit.pdf';
            system.debug('Inserting here');
            insert atch;   
         }
        
        //CITES permit
        //check if workflow task issue ppp is set to complete - do not process if deffered or anything else
        // for Scenario 3: lookup CITES Auth
        system.debug('--ccc-decisiontype--'+decisiontype);
        List<Authorizations__c> CitiesAuth = [Select Id from Authorizations__c where Application__c =: appId and prefix__c =: '204' limit 1];
        system.debug('--ccc-CitiesAuth --'+CitiesAuth );
        if(decisiontype == 'Permit')
        {
            List<Workflow_Task__c> wrkFlowTaskPPP = new List<Workflow_Task__c>();
            wrkFlowTaskPPP = [Select Id, Name, Status__c from Workflow_Task__c  
                                                                  where Authorization__c =:auth.id
                                                                   AND Name = 'Issue Protected Plant Permit' 
                                                                   and Status__c = 'Complete'];
             system.debug('--ccc-wrkFlowTaskPPP--'+wrkFlowTaskPPP);                                                      
            if(wrkFlowTaskPPP.size()!=0 || CitiesAuth.size()!=0)
                {
                    pdf = Page.CARPOL_PPQ_CITEPermitPDF;
                    system.debug('--ccc-pdf-'+pdf); 
                    pdf.getParameters().put('id',authid);
                    // VV Added for Transit CITES 9-2-16
                    system.debug('---prefix = '+auth.Thumbprint__r.Program_Prefix__r.name); 
                    if (auth.Thumbprint__r.Program_Prefix__r.name != '204'){
                    pdf.getParameters().put('id',CitiesAuth[0].Id);
                    } //End VV updates
                    
                    pdf.setRedirect(true);
                    blob bb= (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContent());
                    system.debug('--ccc-get content-');
                    atch = new attachment();
                    atch.ParentId = (CitiesAuth.size()!=0)? CitiesAuth[0].Id : authid;//appid;
                    atch.name='CITES Permit.pdf';
                    atch.body = bb;
                
                    insert atch;   
                }
            
             //fire update on CITES auth that came over with regular permit
            //so that premit number is generated
            if(CitiesAuth.size()!=0)
            {
                CitiesAuth[0].Authorization_Type__c = 'Permit';
                CitiesAuth[0].Response_Type__c = 'Permit';
                update CitiesAuth[0];
            }
            
        }      

     }
     catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
   
        
    }    
}