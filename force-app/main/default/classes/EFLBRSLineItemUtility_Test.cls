@isTest
public class EFLBRSLineItemUtility_Test {
    id nullId = null;
    static testmethod void testUtilityMethods(){
        system.assert(EFLBRSLineItemUtility.getConstruct('Line_item__c', (string)null).size()==0);
        system.assert(EFLBRSLineItemUtility.getLocation((Id)null).size()==0);
        system.assert(EFLBRSLineItemUtility.appattach((Id)null, (Id)null).size()==0);
        system.assert(EFLBRSLineItemUtility.getRA((Id)null).size()==0);
        system.assert(EFLBRSLineItemUtility.getRevCons((Id)null).size()==0);
        system.assert(EFLBRSLineItemUtility.getRevSOP((Id)null).size()==0);
        system.assert(EFLBRSLineItemUtility.genotypes((Id)null).size()==0);
        system.assert(EFLBRSLineItemUtility.GenoRecordtypes(null).size()==0);
        system.assert(EFLBRSLineItemUtility.phenotypes(null).size()==0);
        try{system.assert(EFLBRSLineItemUtility.getArticleSuppliers('','').size()==0);}catch(exception ex){}
        try{system.assert(EFLBRSLineItemUtility.getConstruct('', new Set<Id>()).size()==0);}catch(exception ex){}
        try{system.assert(EFLBRSLineItemUtility.getLocation(new Set<Id>()).size()==0);}catch(exception ex){}
        try{system.assert(EFLBRSLineItemUtility.appattach(new Set<Id>(), null).size()==0);}catch(exception ex){}
        try{system.assert(EFLBRSLineItemUtility.getRA(new Set<Id>()).size()==0);}catch(exception ex){}
        try{system.assert(EFLBRSLineItemUtility.getRevCons(new Set<Id>()).size()==0);}catch(exception ex){}
            
    }
}