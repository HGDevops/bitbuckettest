/**
* Class containing tests for Carpol_UNI_SchemaGlobalDescribe
*/
@IsTest 
private class Carpol_UNI_SchemaGlobalDescribe_Test {
    @IsTest 
    static void testFindObjectNameFromRecordId() {
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        Contact con = testData.newcontact();
        String objectName = Carpol_UNI_SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(con.Id);
        System.assertEquals('Contact', objectName);
    }
    
    @IsTest
    static void testFindObjectNameFromPrefix() {
        String objectName = Carpol_UNI_SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix('003');
        System.assertEquals('Contact', objectName);
    }
    
    @IsTest
    static void testExceptionScenario() {
        String objectName = Carpol_UNI_SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(null);
        System.assertEquals('', objectName);
    }
}