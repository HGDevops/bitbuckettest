@RestResource(urlMapping='/createpdf/*')
global with sharing class AddPDFtoRecordREST{

  @HttpPost
    global static void doPost(String objpermitId,String parentId,String PermitNumber) {
       //list<attachment> attachmentList = new list<attachment>();
            //VF page that renders as PDF
            pageReference pdfPage = Page.CARPOL_PermitPDF2;
            // passing the parameter to the page
            pdfPage.getParameters().put('id',objpermitId);
            Attachment attachment = new Attachment();
            Blob body;
            if(!test.isRunningTest()){
                body = pdfPage.getContent();
            }else{
                body=blob.valueOf('test content');
            }
            attachment.Body = body;
            attachment.Name = 'ePermits  Permit Number '+ PermitNumber +'.pdf';
            attachment.IsPrivate = false;
            attachment.ParentId = parentId;//This is the record to which the pdf will be attached
            
            insert attachment;
        //  attachmentList.add(attachment);
       
         //insert the list of attachment
        // insert attachmentList;
    }
}