/*-----------------------------------------------------------------------  
@Purpose: This Utility class helps in handling some generic functionality across eFile
i.Obtain Recordtype ID using Custom meta datatype 'EFLRecordType'
ii.Ability to Delete any Record 
----------------------------------------------------------------------*/
// deleteMassRecords method need modification
// Also Custom Metadata need some dummy value with Record Type as NULL and we cannot do DML operation on Custom Metadata
public with Sharing class EFLGenericUtility {
    
    /*
*@purpose:  Returns recordType ID from CustomMetaData Type 
*/
    public static Id getRecordTypeId(string recordTypeCMDLabel){
        EFLRecordType__mdt recordtypeCMD = [Select label,Object_API_Name__c,
                                            Record_Type_Name__c  
                                            from EFLRecordType__mdt 
                                            where label =:recordTypeCMDLabel
                                            limit 1];
        
        SObjectType targetObjectType = Schema.getGlobalDescribe().get(recordtypeCMD.Object_API_Name__c);
        
        //Generate a map of tokens for all the Record Types for the desired object
        Map<string, schema.recordtypeinfo> recordTypeInfo = targetObjectType.getDescribe().getRecordTypeInfosByName();
        
        if(!recordTypeInfo.containsKey(recordtypeCMD.Record_Type_Name__c))
            throw new RecordTypeException('Record type "'+ recordtypeCMD.Record_Type_Name__c +'" does not exist.');
        //Retrieve the record type id by name
        return recordTypeInfo.get(recordtypeCMD.Record_Type_Name__c).getRecordTypeId();
        
    }
    
    public class RecordTypeException extends Exception{}
    
    /*
*@purpose:  Delete any Record using its ID 
*/
    public static void deleteRecord(Id deletedRecordId) {
        try{
            sObjectType sObjectType = Schema.getGlobalDescribe().get(deletedRecordId.getSObjectType().getDescribe().getName());
            string strqurey = 'select id from ' + deletedRecordId.getSObjectType().getDescribe().getName() + ' where id=:deletedRecordId';
            system.debug('objectName@@:'+ deletedRecordId.getSObjectType().getDescribe().getName());
            list < sobject > delList = database.query(strqurey);
            system.debug('delList:'+ delList);
            if(sObjectType.getDescribe().isAccessible() && sObjectType.getDescribe().isDeletable()){
                system.debug('Inside delete');
                Delete delList;
            }
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLGenericUtility.deleteRecord()',e);                
        }     
    }
    
    public static void deleteMassRecords(list<Id> deletedRecordIdList) {
        
        if(deletedRecordIdList!=null){
            string strqurey = 'select id from ' + deletedRecordIdList[0].getSObjectType().getDescribe().getName() + ' where in=:deletedRecordIdList';
            list < sobject > lst = database.query(strqurey);
            Delete lst;
        } 
    }
    
    /*
@Purpose: If contains CBI data Parse everything between [ ]  
*/    
    Public static list<sObject> hideCBIContent(list<sObject> sObjectList){
        integer intloop=0;
        string MatchTxt;
        string brackets = '[  ]';
        Pattern p=Pattern.compile('\\['+'[^\\[]*'+'\\]');
        list<sObject> hiddensObjectList = new list<sObject>() ;
        //loop through the list content and empty content within brackets 
        while(sObjectList.size()>intloop){
            Matcher pm=p.matcher(JSON.serialize(sObjectList[intloop]));
            MatchTxt=pm.replaceAll(brackets);
            sObject restoredsObject=(sObject)JSON.deserialize(MatchTxt,sObject.class);
            hiddensObjectList.add(restoredsObject);
            intloop++;
        }  
        return hiddensObjectList;
        
    }  
    
    
    /*
*Purpose: check if ID is valid
*/  
    public static Boolean isValidSalesforceId( String sfdcId, System.Type t ){
        try {
            if ( Pattern.compile( '[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}' ).matcher( sfdcId ).matches() ){
                // Try to assign it to an Id before checking the type
                Id id = sfdcId;
                
                // Use the Type to construct an instance of this sObject
                sObject sObj = (sObject) t.newInstance();
                
                // Set the ID of the new object to the value to test
                sObj.Id = id;
                
                // If the tests passed, it's valid
                return true;
            }
        }catch( Exception e ){
            EFLErrorLog.createErrorLog('EFLGenericUtility.isValidSalesforceId()',e);
        }
        
        // ID is not valid
        return false;
    }
    
    /*
*Purpose: Generate Unique Key
*/  
    public static String randomString() { 
        
        return EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0, 5);
    } 
    
    //Get message from CMD 
    public static string getMessage(string cmdLabel){
        return [select label,Message__c from EFLMessages__mdt where Label=:cmdLabel limit 1].Message__c ;
    }
    
    //Get profile name from CMD 
    public static string getProfile(string cmdLabel){
        return [select label,Profile__c from EFLUserLibrary__mdt  where Label=:cmdLabel limit 1].Profile__c;
    }
    
    /*
*Purpose: check if Trigger is Disabled
*/ 
    public static Boolean isTriggerDisabled(string triggerNameCMDLabel){
        
        return [SELECT Disabled__c
                FROM EFLTriggerSetting__mdt 
                WHERE label =:triggerNameCMDLabel
                LIMIT 1].Disabled__c; 
    } 
    
    /*
*Purpose: Provide years between two dates
*/ 
    public static integer getNumberofYears(Date startDate, Date endDate){
        
        // string noOfYears = String.valueOf(startDate.monthsBetween(endDate) / 12);
        // Integer Years = Integer.valueOf(noOfYears);
        return (startDate.monthsBetween(endDate) / 12);
    }
    
    /*
* Purpose: Determine Multi Year
*/
    public static boolean determineMultiYear(date startDate,date endDate){
        
        date newDate = startDate.addYears(1);
        if(newDate < endDate){  
            return true;
        } 
        return false;
    } 
    
    /*
* Purpose: Get Instructions for LineItem
*/ 
    public static string getInstructions(string movementType, string TypeofPermit, string section){
        
        if(movementType==NULL && TypeofPermit==NUll){
            return [select Text__c from EFLTextConfiguration__mdt 
                    where Section__c =: section
                    limit 1 ].Text__c; 
        }else if(movementType==NULL){
            return [select Text__c from EFLTextConfiguration__mdt 
                    where Type_of_Permit__c =: TypeofPermit and
                    Section__c =: section
                    limit 1 ].Text__c; 
        }else{
            return [select Text__c from EFLTextConfiguration__mdt 
                    where Movement_Type__c =: movementType and
                    Type_of_Permit__c =: TypeofPermit and
                    Section__c =: section
                    limit 1 ].Text__c;
        }
    }
    
    public static string getInstructions(string movementType, string TypeofPermit, string section, string functionalArea){
        
        if(movementType==NULL && TypeofPermit==NUll){
            return [select Text__c from EFLTextConfiguration__mdt 
                    where Section__c =: section and
                    Functional_Area__c =: functionalArea
                    limit 1 ].Text__c; 
        }else if(movementType==NULL){
            return [select Text__c from EFLTextConfiguration__mdt 
                    where Type_of_Permit__c =: TypeofPermit and
                    Section__c =: section and 
                    Functional_Area__c =: functionalArea
                    limit 1 ].Text__c; 
        }else{
            return [select Text__c from EFLTextConfiguration__mdt 
                    where Movement_Type__c =: movementType and
                    Type_of_Permit__c =: TypeofPermit and
                    Section__c =: section and 
                    Functional_Area__c =: functionalArea
                    limit 1 ].Text__c;
        }
    } 
    
    public static List<EFLTextConfiguration__mdt> getInstructionsByFunctionalArea(string functionalArea) {
        List<EFLTextConfiguration__mdt> eflConfigTextList = new List<EFLTextConfiguration__mdt>();
        eflConfigTextList = [SELECT Id
                             , Text__c
                             , Section__c
                             , Movement_Type__c 
                             FROM EFLTextConfiguration__mdt 
                             WHERE Functional_Area__c =:functionalArea];
        return eflConfigTextList;
    }
    
    // Khushbu Aggarwal(12/6/2018):Start:::genric functionalty to validate US postal/Zip Code::::  
    Public Static Boolean ValidateZip(String ZIPCode){
        Boolean result;
        string ZIPRegex = '^[0-9]{5}(?:-[0-9]{4})?$';
        // create  a new  pattern object  "zipcodePattern" with regex for ZIP code
        Pattern zipcodePattern = pattern.compile(ZIPRegex);
        // create a new Matcher object "zipcodeMatcher" with Zipcode as a parameter to compare 
        Matcher zipcodeMatcher = zipcodePattern.matcher(ZIPCode);
        //verify the match ( return true if matches otherwise "false")
        
        result = zipcodeMatcher.matches();
        return result;       
    }      
    // Khushbu Aggarwal(12/6/2018):END:::genric functionalty to validate US postal/Zip Code::::    
    
    // added 5/10/2019 by JB.  Sanitize string inputs for SOQL injection concerns.
    public static string sanitizeString(string input){
        if(string.isNotBlank(input)){
            return string.escapeSingleQuotes(input);
        }
        return null;
    }
    
    public static Boolean isFieldValueChanged(Map<Id,Sobject> oldSobjMap, Sobject newSobj, String fldVal){
        //if Trigger.OldMap is null, assume it is a new record and return true for changed value
        return (oldSobjMap == null || oldSobjMap.get(newSobj.Id).get(fldVal) != newSobj.get(fldVal));
    }
}