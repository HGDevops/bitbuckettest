public class EFLErrorLoggingBatch implements Database.Batchable<sObject>,Schedulable {
    
    public void execute(SchedulableContext ctx){
        EFLErrorLoggingBatch batch = new EFLErrorLoggingBatch(Date.today());
        Database.executebatch(batch);
    }
    
    public EFLErrorLoggingBatch(){}
    
    private static Date asOfDateQry;
    public EFLErrorLoggingBatch(Date asOfDate){
        asOfDateQry = asOfDate;
    }
    
    // The batch job starts
    public Database.Querylocator start(Database.BatchableContext bc){
        Date asOf = asOfDateQry;
        system.debug('asOfDate: ' + asOfDateQry);
        return Database.getQuerylocator([SELECT Id FROM Error_Logging__c WHERE CreatedDate < :asOfDateQry]);
    } 
    
    // The batch job executes and operates on one batch of records
    public void execute(Database.BatchableContext bc, List<sObject> scope){
        if(!scope.isEmpty()){
            DELETE scope;
        }
    }
    
    // The batch job finishes
    public void finish(Database.BatchableContext bc){
    }
}