// Test Class for CARPOL_inspection_questionnaire_option class\
// Created By - Rajesh Potla 
// Date - 5/7/2019
@isTest
private class CARPOL_inspection_questionnaire_opt_Test {
    static Id IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
    
    @testsetup
    static void setupData(){
        Domain__c objDom = new Domain__c(Name = 'BRS');
        List<sObject> testDataSet = new List<sObject>();
        insert objDom;
        
        testDataSet.add(new Program_Prefix__c(Name= 'BRS Inspection',Inspection_Related__c = true,Program__c = objDom.id));
        testDataSet.add(new Inspection__c(RecordTypeId = IncRecTypeID,Stage__c = 'Inspection Assignment',status__c='Cancelled',Reason_for_Cancellation__c = 'Testing Cancellation'
                                          ,Certified__c = FALSE));
        insert testDataSet;
        testDataSet = new List<sObject>();
        Inspection__c ins = [Select Id From Inspection__c limit 1];
        for(Integer i=1;i<11;i++){
            String answerType = (Math.mod(i,2)==0)?'Free Text':'Yes/No';
            String response = (Math.mod(i,2)==0)?'N/A':'Yes';
            testDataSet.add(new EFL_Inspection_Questionnaire_Questions__c
                            (Question__c='test question'+i,Response__c = response,Answer_Type__c = answerType, 
                             Answer__c='answer'+i,Comments2__c = 'test'+i,Inspection__c=Ins.id,Options__c='Yes,No,N/A,Unknown'));
            
        }
        insert testDataSet;
    }
    
    @isTest
    static void runTestScenario1(){
        Inspection__c oInspection = [Select Id,RecordTypeId,Stage__c,status__c,Reason_for_Cancellation__c,Certified__c
                                     From Inspection__c limit 1];
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id',oInspection.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(oInspection);
        CARPOL_inspection_questionnaire_option oInspectionOptCntrl= new CARPOL_inspection_questionnaire_option(stdCon);
        oInspectionOptCntrl.init();
        System.assertEquals(oInspection.id, oInspectionOptCntrl.questionnaireid);
        SYstem.assertEquals(10, oInspectionOptCntrl.listEIQ.size());
        System.assertEquals(null, oInspectionOptCntrl.renderButton);
        oInspectionOptCntrl.submit();
        oInspectionOptCntrl.saveQuestion();
        oInspectionOptCntrl.first();
        oInspectionOptCntrl.cancel();
        oInspectionOptCntrl.last();
        oInspectionOptCntrl.previous();
        System.assertEquals(false,oInspectionOptCntrl.hasPrevious);
        System.assertEquals(true,oInspectionOptCntrl.hasNext);
        oInspectionOptCntrl.next();
        System.assertEquals(true,oInspectionOptCntrl.hasPrevious);
        System.assertEquals(false,oInspectionOptCntrl.hasNext);
        oInspectionOptCntrl.setPaginationSize();
        oInspectionOptCntrl.submit();
        Test.stopTest();
    }
    
    @isTest
    static void runTestScenario_CertifiedException(){
        Inspection__c oInspection = new Inspection__c(RecordTypeId = IncRecTypeID,Stage__c = 'Inspection Assignment',status__c='Cancelled',Reason_for_Cancellation__c = 'Testing Cancellation'
                                                      ,Certified__c = true);
        insert oInspection;
        Integer i=0;
        EFL_Inspection_Questionnaire_Questions__c qq = new EFL_Inspection_Questionnaire_Questions__c
            (Question__c='test question'+i,Response__c = 'response'+i, 
             Answer__c='answer'+i,Comments2__c = 'test'+i,Inspection__c=oInspection.id,Options__c='test'+i);
        
        insert qq;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id',oInspection.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(oInspection);
        CARPOL_inspection_questionnaire_option oInspectionOptCntrl= new CARPOL_inspection_questionnaire_option(stdCon);
        oInspectionOptCntrl.init();
        oInspectionOptCntrl.saveQuestion();
        oInspectionOptCntrl.next();  
        oInspectionOptCntrl.submit();              
        Test.stopTest();
    }
    
    @isTest
    static void runTestScenario_PatternException(){
        Inspection__c oInspection = new Inspection__c(RecordTypeId = IncRecTypeID,Stage__c = 'Inspection Assignment',status__c='Cancelled',Reason_for_Cancellation__c = 'Testing Cancellation'
                                                      ,Certified__c = false);
        insert oInspection;
        Integer i=11;
        EFL_Inspection_Questionnaire_Questions__c qq = new EFL_Inspection_Questionnaire_Questions__c
            (Question__c='test question'+i,Response__c = 'response'+i, 
             Answer__c='answer'+i,Comments2__c = 'test[2212]{fdadada}(2323)[]{}()'+i,Inspection__c=oInspection.id,Options__c='test'+i);
        insert qq;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id',oInspection.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(oInspection);
        CARPOL_inspection_questionnaire_option oInspectionOptCntrl= new CARPOL_inspection_questionnaire_option(stdCon);
        oInspectionOptCntrl.init();
        oInspectionOptCntrl.saveQuestion();
        oInspectionOptCntrl.submit();
        oInspectionOptCntrl.next();
        Test.stopTest();
    }
    
    @isTest
    static void runTestScenario2(){
        Inspection__c oInspection = [Select Id,RecordTypeId,Stage__c,status__c,Reason_for_Cancellation__c,Certified__c
                                     From Inspection__c limit 1];
        oInspection.certified__c = false;
        update oInspection;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id',oInspection.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(oInspection);
        CARPOL_inspection_questionnaire_option oInspectionOptCntrl= new CARPOL_inspection_questionnaire_option(stdCon);
        oInspectionOptCntrl.init();
        System.assertEquals(oInspection.id, oInspectionOptCntrl.questionnaireid);
        SYstem.assertEquals(10, oInspectionOptCntrl.listEIQ.size());
        System.assertEquals(null, oInspectionOptCntrl.renderButton);
        oInspectionOptCntrl.submit();
        oInspectionOptCntrl.saveQuestion();
        oInspectionOptCntrl.first();
        oInspectionOptCntrl.cancel();
        oInspectionOptCntrl.last();
        oInspectionOptCntrl.previous();
        System.assertEquals(false,oInspectionOptCntrl.hasPrevious);
        System.assertEquals(true,oInspectionOptCntrl.hasNext);
        oInspectionOptCntrl.next();
        System.assertEquals(true,oInspectionOptCntrl.hasPrevious);
        System.assertEquals(false,oInspectionOptCntrl.hasNext);
        oInspectionOptCntrl.setPaginationSize();
        oInspectionOptCntrl.submit();
        Test.stopTest();
    }
     //@isTest
    static void runTestScenario_CommentsException(){
        Inspection__c oInspection = new Inspection__c(RecordTypeId = IncRecTypeID,Stage__c = 'Inspection Assignment',status__c='Cancelled',Reason_for_Cancellation__c = 'Testing Cancellation'
                                                      ,Certified__c = false);
        insert oInspection;
        Integer i=12;
        EFL_Inspection_Questionnaire_Questions__c qq = new EFL_Inspection_Questionnaire_Questions__c
            (Question__c='test question'+i,Response__c = 'response'+i, 
             Answer__c='answer'+i,Comments2__c = 'test comment',Inspection__c=oInspection.id,Options__c='test'+i);
         
        insert qq;
        
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id',oInspection.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(oInspection);
        CARPOL_inspection_questionnaire_option oInspectionOptCntrl= new CARPOL_inspection_questionnaire_option(stdCon);
        qq.Comments2__c = '';
        update qq;
        oInspectionOptCntrl.RelatedQuestList = new list<EFL_Inspection_Questionnaire_Questions__c>{qq};
        oInspectionOptCntrl.init();
        oInspectionOptCntrl.validateEntries();
        oInspectionOptCntrl.saveQuestion();
        oInspectionOptCntrl.submit();
        oInspectionOptCntrl.next();
        Test.stopTest();
    }
 
}