@isTest
public class EFLGetHQDetails_Test{

    private static final string AccountName = 'Test Account';
    private static final string ContactLastName = 'Test';
    private static final string sites = 'Site 1';

    @TestSetup
    static void makeData(){
        // Make a parent object for Uploaded_File__c to be created under in test execution.
        Account account = new Account();
        account.Name = AccountName;
        insert account;
        
        List<RecordType> rt = [Select Id From RecordType Where SObjectType = 'Contact' and DeveloperName='AC_R_L_Contact' Limit 1];
        Contact contact = new Contact();
        contact.LastName = ContactLastName;
        contact.AccountId = account.Id;
        contact.RecordTypeId = rt.size() > 0 ? rt[0].Id : null;
        insert contact;

		EFLRegistration__c registration = new EFLRegistration__c();
		registration.EFLAccount__c = account.Id;
        insert registration;
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = account.Id;
        annualReport.EFLContact__c = contact.Id;
        annualReport.EFLRegistration__c = registration.Id;
        annualReport.EFLSites__c = sites;
        insert annualReport;
    }

    @isTest
    public static void getAccountHQByReportId_RegistrationReturned(){
        EFLAnnual_Report__c annualReport = [Select Id, EFLRegistration__c From EFLAnnual_Report__c Limit 1];
        
        EFLRegistration__c registration = EFLGetHQDetails.getAccountHQ(annualReport.Id);
        System.assertEquals(annualReport.EFLRegistration__c, registration.Id);
    }
    
    @isTest
    public static void getAnnualReportList_RecordReturned(){
        EFLAnnual_Report__c annualReport = [Select Id, EFLRegistration__c From EFLAnnual_Report__c Limit 1];
        
        List<EFLAnnual_Report__c> annualReports = EFLGetHQDetails.getAnnRepList(annualReport.Id);
        System.assertEquals(1, annualReports.size());
        System.assertEquals(annualReport.Id, annualReports[0].Id);
    }

    @isTest
    public static void saveData_DataSaved(){
        EFLAnnual_Report__c annualReport = [Select Id, EFLRegistration__c, EFLSites__c, EFLSpringCMSites__c
                                            , EFL_Using_Federal_Funds__c, EFL_Change_In_Ownership__c
                                            From EFLAnnual_Report__c Limit 1];
        
        System.assertEquals(sites, annualReport.EFLSites__c);
        System.assertEquals(null, annualReport.EFL_Using_Federal_Funds__c);
        System.assertEquals(null, annualReport.EFL_Change_In_Ownership__c);
        
        EFLGetHQDetails.saveData(annualReport.Id, 'Site 2\nSite 3', 'Yes', 'No');
        
        annualReport = [Select Id, EFLRegistration__c, EFLSites__c, EFLSpringCMSites__c
                        , EFL_Using_Federal_Funds__c, EFL_Change_In_Ownership__c
                        From EFLAnnual_Report__c Limit 1];
        System.assertNotEquals(sites, annualReport.EFLSites__c);
        System.assert(!annualReport.EFLSites__c.contains(sites));
        System.assert(annualReport.EFLSites__c.contains('Site 2'));
        System.assert(annualReport.EFLSpringCMSites__c.contains('Site 2'));
        System.assert(!annualReport.EFLSpringCMSites__c.contains('\n'));
        System.assert(annualReport.EFLSpringCMSites__c.contains(','));
        System.assertEquals('Yes', annualReport.EFL_Using_Federal_Funds__c);
        System.assertEquals('No', annualReport.EFL_Change_In_Ownership__c);
    }

    @isTest
    public static void getPicklistValues_ValuesReturned(){
        List<EFLUtils.LightningPicklistOption> options = EFLGetHQDetails.getPicklistValues('EFLAnnual_Report__c', 'EFL_Using_Federal_Funds__c');
        System.assertNotEquals(0, options.size());
    }
    
}