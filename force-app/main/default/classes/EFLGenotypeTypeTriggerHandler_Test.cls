@isTest
public class EFLGenotypeTypeTriggerHandler_Test {
static testMethod void testEFLGenotypeTypeTriggerHandler(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
    
        List <GenotypeType__c> genoTypeTypes = new List <GenotypeType__c>();
        GenotypeType__c genotypeType = new GenotypeType__c();
        
        Application__c currentApp = testData.newapplication();
        Regulated_Article__c regArt = new  Regulated_Article__c();
        regArt.Name='Test article';
        regArt.RecordTypeID=Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        regArt.Status__c='Active';
        regArt.Category_Group_Ref__c = testData.newgroup().Id;
        insert regArt;
        RA_Scientific_Name_Junction__c raJunc = new  RA_Scientific_Name_Junction__c();
        raJunc.Regulated_Article__c = regArt.id;
        
        Authorizations__c auth = testData.newAuth(currentApp.Id);
        AC__c ac1 = testData.newLineItemBRS('Personal Use', currentApp);
        ac1.Type_of_Permit__c = 'Standard Permit';
        update ac1;
        Regulated_Article__c regArt2 = testData.newRegulatedArticle(auth.Program_Pathway__c);
       /* Link_Regulated_Articles__c linkRA = testData.newlinkRegArticle(ac1.id, currentApp.Id, auth.id);
        linkRA.Line_Item__c = ac1.id;
        linkRA.Regulated_Article__c = regArt2.id;
        upsert linkRA;
        system.debug('LinkRA set to: ' + linkRA);*/
       link_regulated_articles__c lra = new link_regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(ac1.id, regArt2.id, 'Draft');   
        Construct__c cons1 = testData.newconstruct(ac1.id, regArt2);              
        Phenotype__c phenoType = testData.newphenotype(cons1.id);
        genotypeType.Construct__c = cons1.id;
        genotypeType.Genotype_Category__c = 'Gene Knock-Out';
        genotypeType.Ready_to_Submit__c = false;
        genotypeType.Count_of_Construct_Components__c = 1;
        insert genotypeType;
        genoTypeTypes.add(genotypeType);
        Genotype__c genoType = testData.newgenotype(cons1.id, genoTypeType);
        
        
        Test.startTest();
        PageReference pageRef = Page.EFLConstruct;
        pageRef.getParameters().put('lineItemId', String.valueOf(ac1.id));
        Test.setCurrentPage(pageRef);  
        EFLConstructController conControl = new EFLConstructController();
        
        EFLGenotypeTypeTriggerHandler.ValidateRequiredFields(genoTypeTypes);	
        delete genotypeType;
        Test.stopTest();
    }
}