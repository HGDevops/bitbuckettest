@isTest(seeAlldata=true)
private class EFLWizardHelper_Test {
    
    //verify if question exists
    static testMethod void getQuestion() { 
        string movementType = 'Import';
        string question = EFLWizardHelper.getLandingOptionsQuestions(movementType);
        system.assertEquals(question,'What are you importing?');
    }  
         

}