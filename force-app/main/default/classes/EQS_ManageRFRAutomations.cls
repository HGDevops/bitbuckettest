global class EQS_ManageRFRAutomations Implements Schedulable
    {
        global void execute(SchedulableContext sc)
        {
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :sc.getTriggerId()];
            System.debug('>>>'+ct.CronExpression);
            System.debug('>>>'+ct.TimesTriggered);
            autoManageRFRs_StartDt();
            autoManageRFRs_EndDt();
            
        }
        public void autoManageRFRs_StartDt()
        {
            List<Contact> UpdResponderList = new List<Contact>();
            //List<EQS_Resource_Request__c> listRFRsL1 = [SELECT ID,EQS_Responder__c,EQS_Responder_Status__c,EQS_Rotation_Begin_Date__c,EQS_Rotation_End_Date__c,EQS_Incident_Time_Zone__c FROM EQS_Resource_Request__c WHERE (EQS_Rotation_Begin_Date__c <= Today) AND (EQS_Rotation_End_Date__c >= Today) AND EQS_Responder_Status__c!='At Incident'];
            List<EQS_Resource_Request__c> listRFRsL1 = [SELECT ID,EQS_Responder__c,EQS_Responder_Status__c,EQS_Rotation_Begin_Date__c,EQS_Rotation_End_Date__c,EQS_Incident_Time_Zone__c FROM EQS_Resource_Request__c WHERE EQS_Rotation_Begin_Date__c = Today AND EQS_Responder_Status__c!='At Incident'];

            for(EQS_Resource_Request__c rfr : listRFRsL1)
            {
              Contact tempres=new Contact(Id=rfr.EQS_Responder__c,EQS_Responder_Status__c='At Incident');
              UpdResponderList.add(tempres);
            }

            update UpdResponderList;
        }
        
        public void autoManageRFRs_EndDt()
        {
            List<Contact> UpdResponderList = new List<Contact>();
            List<EQS_Resource_Request__c> listRFRsL1 = [SELECT ID,EQS_Responder__c,EQS_Responder_Status__c,EQS_Rotation_Begin_Date__c,EQS_Rotation_End_Date__c,EQS_Incident_Time_Zone__c FROM EQS_Resource_Request__c WHERE EQS_Estimated_Available_Date__c = Today AND EQS_Responder_Status__c!='Available'];

            for(EQS_Resource_Request__c rfr : listRFRsL1)
            {
              Contact tempres=new Contact(Id=rfr.EQS_Responder__c,EQS_Responder_Status__c='Available');
              UpdResponderList.add(tempres);
            }

            update UpdResponderList;
        }

    }