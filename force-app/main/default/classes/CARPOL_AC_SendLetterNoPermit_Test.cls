// FIXME: commented out blocks of code after inserting work flow task record due to SpringCM class error.

@isTest(seealldata=true)
public class CARPOL_AC_SendLetterNoPermit_Test{
    static testMethod void testSendNoPermitLetter(){
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c auth = testData.newAuth(objapp.Id); 
        AC__c ac1 = testData.newLineItem('Personal Use',objapp,auth);        
        //AC__c ac2 = testData.newLineItem('Personal Use',objapp,auth);        
        //AC__c ac3 = testData.newLineItem('Personal Use',objapp,auth); 
        Test.startTest();             
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);           
        
        
        RecordType wtskrt = [select ID from RecordType where Name = 'Process Authorization' AND sObjectType = 'Workflow_Task__c'];
        Workflow_Task__c wtask = new Workflow_Task__c(RecordTypeId = wtskrt.Id, Name = 'Process Authorization', Authorization__c = auth.Id, Status__c = 'Complete', Program__c = 'AC');
        
        try{ // FIXME: Remove this try/catch.
            insert wtask;
            
            
            Attachment att = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = auth.Id, Name = 'LetterOfNoPermitReqired.pdf');
            insert att;
            auth.Status__c = 'Issued';
            update auth;
            Authorizations__c au = [select Status__c, Application__c, Authorization_Type__c from Authorizations__c where Id = :auth.Id];
            List<Attachment> attlst= [SELECT ID, Name, Body FROM Attachment WHERE (Name like 'LetterOf%' OR Name like 'AuthorizationLetter%') AND ParentId = : auth.Id];
            System.AssertEquals(1, attlst.size());
        }catch(Exception e){
            system.debug('exception: ' + e.getMessage());
        }
        Test.stopTest();
    }
    
    static testMethod void testSendNoPermitLetter2(){
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c auth = testData.newAuth(objapp.Id); 
        AC__c ac1 = testData.newLineItem('Personal Use',objapp,auth);        
        //AC__c ac2 = testData.newLineItem('Personal Use',objapp,auth);        
        //AC__c ac3 = testData.newLineItem('Personal Use',objapp,auth); 
        Test.startTest();             
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);           
        
        
        RecordType wtskrt = [select ID from RecordType where Name = 'Process Authorization' AND sObjectType = 'Workflow_Task__c'];
        Workflow_Task__c wtask = new Workflow_Task__c(RecordTypeId = wtskrt.Id, Name = 'Process Authorization', Authorization__c = auth.Id, Status__c = 'Complete', Program__c = 'AC');
        
        try{ // FIXME: Remove this try/catch.
            insert wtask;
            
            Attachment att = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = auth.Id, Name = 'LetterOfNoPermitReqired.pdf');
            insert att;
            
            Attachment att_permit = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = auth.Id, Name = 'Permit.pdf');
            insert att_permit;
            auth.Status__c = 'Issued';
            update auth;
            Authorizations__c au = [select Status__c, Application__c, Authorization_Type__c from Authorizations__c where Id = :auth.Id];
            List<Attachment> attlst= [SELECT ID, Name, Body FROM Attachment WHERE (Name like 'LetterOf%' OR Name like 'AuthorizationLetter%') AND ParentId = : auth.Id];
            List<Attachment> attplst= [SELECT ID, Name, Body FROM Attachment WHERE Name like 'Permit%' AND ParentId = : auth.Id];
            System.AssertEquals(1, attlst.size());
            System.AssertEquals(1, attplst.size());
        }catch(Exception e){
            system.debug('exception: ' + e.getMessage());
        }
        Test.stopTest();
    }
    
    static testMethod void testSendNoPermitLetter3(){
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        //Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c auth = testData.newAuth(objapp.Id); 
        AC__c ac1 = testData.newLineItem('Personal Use',objapp,auth);        
        //AC__c ac2 = testData.newLineItem('Personal Use',objapp,auth);        
        //AC__c ac3 = testData.newLineItem('Personal Use',objapp,auth);
        Test.startTest();             
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);
        
        
        RecordType wtskrt = [select ID from RecordType where Name = 'Process Authorization' AND sObjectType = 'Workflow_Task__c'];
        Workflow_Task__c wtask = new Workflow_Task__c(RecordTypeId = wtskrt.Id, Name = 'Process Authorization', Authorization__c = auth.Id, Status__c = 'Complete', Program__c = 'AC');
        
        try{ // FIXME: Remove this try/catch.
            insert wtask;
            
            
            Attachment att_permit = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = auth.Id, Name = 'Permit.pdf');
            insert att_permit;
            auth.Status__c = 'Issued';
            update auth;
            Authorizations__c au = [select Status__c, Application__c, Authorization_Type__c from Authorizations__c where Id = :auth.Id];
            
            List<Attachment> attplst= [SELECT ID, Name, Body FROM Attachment WHERE Name like 'Permit%' AND ParentId = : auth.Id];
            
            System.AssertEquals(1, attplst.size());
        }catch(Exception e){
            system.debug('exception: ' + e.getMessage());
        }
        Test.stopTest();
    }
}