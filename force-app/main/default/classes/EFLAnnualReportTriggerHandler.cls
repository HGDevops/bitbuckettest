public inherited sharing class EFLAnnualReportTriggerHandler extends TriggerHandler {
    
    public static final string ANNUAL_REPORT_WAITING_ON_CUSTOMER_STATUS = 'Waiting on Customer';

    public override void beforeInsert() {
        setFiscalYear();
        setErrorWhenRegistrationHasExistingAR((List<EFLAnnual_Report__c>)Trigger.new);
    }

    public override void beforeUpdate() {
        setErrorWhenRegistrationHasExistingAR((List<EFLAnnual_Report__c>)Trigger.new);
    }

    public override void afterInsert() {
        setAnnualReportCreatedFlagForRegistrations();
        addStandardAnimals();
    }
    
    public override void afterUpdate(){
        sendRescindedReasonEmail();
        setAnnualReportCreatedFlagForRegistrations();
    }
    
    public override void afterDelete(){
        setAnnualReportCreatedFlagForRegistrations();
    }
    
    public override void afterUndelete(){
        setAnnualReportCreatedFlagForRegistrations();
    }
    
    private Map<Id, EFLRegistration__c> getRegistrations(List<EFLAnnual_Report__c> annualReports, Boolean excludeCurrentARs) {
        Set<Id> registrationIds = new Set<Id>();
        Set<Id> arIdsToIgnore = new Set<Id>();

        // Create a set of registration ids that are referenced in the records being inserted
        for(EFLAnnual_Report__c ar :annualReports) {
            if(ar.EFLRegistration__c != null) {
                registrationIds.add(ar.EFLRegistration__c);
            }

            if(ar.Id != null && excludeCurrentARs == true) {
                arIdsToIgnore.add(ar.Id);
            }
        }

        return getRegistrations(registrationIds, arIdsToIgnore);
    }    
    
    private String getFiscalYear(DateTime dateTimeValue) {
        return getFiscalYear(dateTimeValue.date());
    }
    
    private String getFiscalYear(Date dateValue) {
        String fiscalYear = '';
        
        if(dateValue.month() >= 10 && dateValue.month() <= 12) {
            fiscalYear = String.valueOf(dateValue.year());
        }
        else {
            fiscalYear = String.valueOf(dateValue.year() -1);
        }
            
        return fiscalYear;
    }
    
    private void setFiscalYear() {
        String fiscalYear = getFiscalYear(Date.today());
        
        for(EFLAnnual_Report__c ar :(List<EFLAnnual_Report__c>)Trigger.new) {
            if(ar.EFLFiscal_Year__c == null) {
                ar.EFLFiscal_Year__c = fiscalYear;
            }
        }
    }
    
    private Map<Id, EFLRegistration__c> getRegistrations(Set<Id> registrationIds, Set<Id> arIdsToIgnore) {
        Map<Id, EFLRegistration__c> registrations = new Map<Id, EFLRegistration__c>();

        if(registrationIds.size() > 0) {
            registrations = new Map<Id, EFLRegistration__c>([Select Id, EFLAccount__c, EFLAnnual_Report_Created__c, EFLCancellation_Date__c
                                                             , (Select Id, EFLFiscal_Year__c, EFLRegistration__c, CreatedDate 
                                                                From Annual_Reports__r
                                                                Where Id Not In :arIdsToIgnore)
                                                  			 From EFLRegistration__c 
                                                  			 Where Id In :registrationIds]);
        }
        
		return registrations;
    }
    
    /*
     * The map here uses Registration Id as the key and the corresponding value 
     * is a map of fiscal year and the number of anual reports for that fiscal year.
     * The purpose is to keep a tally of the total number of annual reports per 
     * registration per fiscal year
     */
    private void addToMap(Map<Id, Map<String, Integer>> regIdToFYCountMap, Id regId, String arFiscalYear) {
        if(regId != null) {
            if(!regIdToFYCountMap.containsKey(regId)) {
                Map<String, Integer> fyCountMap = new Map<String, Integer>();
                regIdToFYCountMap.put(regId, fyCountMap);
            }
            
            Map<String, Integer> fyCountMap = regIdToFYCountMap.get(regId);
            String fiscalYear = String.isNotBlank(arFiscalYear) ? arFiscalYear : String.valueOf(Date.today().year());
            Integer fyCount = 0;
            if(fyCountMap.containsKey(fiscalYear)) {
	            fyCount = fyCountMap.get(fiscalYear);
            }
            fyCountMap.put(fiscalYear, fyCount +1);
            regIdToFYCountMap.put(regId, fyCountMap);
        }
    }
    
    private void setErrorWhenRegistrationHasExistingAR(List<EFLAnnual_Report__c> annualReports) {
        // This map stores the number of annual reports per fiscal year by registration Id
        Map<Id, Map<String, Integer>> regIdToFYCountMap = new Map<Id, Map<String, Integer>>();
        
        for(EFLAnnual_Report__c ar :annualReports) {
            addToMap(regIdToFYCountMap, ar.EFLRegistration__c, ar.EFLFiscal_Year__c);
        }
        
        // Get any existing annual report Ids associated with these registrations
        Map<Id, EFLRegistration__c> registrations = getRegistrations(annualReports, true);
        for(EFLRegistration__c registration :registrations.values()) {
            for(EFLAnnual_Report__c ar :registration.Annual_Reports__r) {
                addToMap(regIdToFYCountMap, registration.Id, ar.EFLFiscal_Year__c);
            }
        }
        
        for(EFLAnnual_Report__c ar :annualReports) {
            String regCancellationFY = '';
            
            if(registrations.containsKey(ar.EFLRegistration__c)) {
                EFLRegistration__c registration = registrations.get(ar.EFLRegistration__c);
                ar.EFLAccount__c = registration.EFLAccount__c;

                // If this registration has been cancelled, get the cancellation Fiscal Year
                if(registration.EFLCancellation_Date__c != null) {
                    regCancellationFY = getFiscalYear(registration.EFLCancellation_Date__c);
                }
            }
            
            if(ar.EFLRegistration__c != null) {
	            String fiscalYear = String.isNotBlank(ar.EFLFiscal_Year__c) ? ar.EFLFiscal_Year__c : String.valueOf(Date.today().year());
                Boolean arExists = false;
                
                // If an Annual Report for this Fiscal Year already exists, we need to stop creation of a new one
                if(regIdToFYCountMap.get(ar.EFLRegistration__c).get(fiscalYear) > 1) {
                    arExists = true;
                }

				// If this registration is cancelled but an AR already exists for the Cancellation FY, we need to stop creation of a new one                
                if(registrations.get(ar.EFLRegistration__c) != null 
                   && regIdToFYCountMap.get(ar.EFLRegistration__c).get(regCancellationFY) > 1) {
                    arExists = true;
                }
                
                if(arExists) {
                    ar.addError(Label.EFLACLRA_AR_Already_exists_for_FY);
                }
            }
        }
    }

    private void setAnnualReportCreatedFlagForRegistrations() {
        // Create a list of annual reports from both old and new values so that 
        // we can account for cases when annual reports are switched to different registration records
        List<EFLAnnual_Report__c> annualReports = new List<EFLAnnual_Report__c>();
        if(Trigger.new != null) {
            annualReports.addAll((List<EFLAnnual_Report__c>)Trigger.new);
        }
        if(Trigger.old != null) {
            annualReports.addAll((List<EFLAnnual_Report__c>)Trigger.old);
        }
        
		// Get the registration records along with any existing annual reports for the registrations associated with the new annual reports
        Map<Id, EFLRegistration__c> registrations = getRegistrations(annualReports, false);
        
        for(EFLRegistration__c registration :registrations.values()) {
            registration.EFLAnnual_Report_Created__c = registration.Annual_Reports__r.size() > 0 ? true : false;
        }

        if(registrations.values().size() > 0) {
            update registrations.values();
        }
    } 
    
    public void sendRescindedReasonEmail(){
        Set<String> annualReportAccountIds = new Set<String>();
        Set<String> annualReportContacts = new Set<String>();
        Map<String, String> annualReportAccountIdMap = new Map<String, String>();
        
        List<EFLAnnual_Report__c> annualReportsToEmail = new List<EFLAnnual_Report__c>();
        
        for (EFLAnnual_Report__c annualReport : (List<EFLAnnual_Report__c>)Trigger.New){
            EFLAnnual_Report__c oldReport = (EFLAnnual_Report__c)Trigger.oldMap.get(annualReport.Id);
            
            //status changed to Waiting on Customer
            if (String.isNotBlank(annualReport.Status__c) && 
                annualReport.Status__c == ANNUAL_REPORT_WAITING_ON_CUSTOMER_STATUS &&
                oldReport.Status__c != ANNUAL_REPORT_WAITING_ON_CUSTOMER_STATUS){
                    annualReportContacts.add(annualReport.EFLContact__c);
                    annualReportsToEmail.add(annualReport);
            }
        }
        
        System.debug('annualReportsToEmail: ' + annualReportsToEmail);
        System.debug('annualReportContacts: ' + annualReportContacts);
        //get the Annual_Report_Rescinded_Reason email template
        EmailTemplate emailTemplate = [SELECT Id
                                       FROM EmailTemplate
                                       WHERE DeveloperName = 'Annual_Report_Rescinded_Reason'];
        
        List<Contact> reportSignedBy = [SELECT Id, Email, AccountId
                                        FROM Contact 
                                        WHERE Id IN :annualReportContacts];
        for (Contact contact : reportSignedBy){
            annualReportAccountIds.add(contact.AccountId);
            
            //setup the AnnualReport : AccountId map
            for (EFLAnnual_Report__c annualReport : (List<EFLAnnual_Report__c>)Trigger.New){
                if (annualReport.EFLContact__c == contact.Id){
                    annualReportAccountIdMap.put(annualReport.Id, contact.AccountId);
                    break;
                }
            }
        }
        
        //get the facility admins from the annual report -> contact -> accounts
        List<Contact> facilityAdmins = [SELECT Id, Email, AccountId
                                        FROM Contact 
                                        WHERE AccountId IN :annualReportAccountIds 
                                        AND EFL_Facility_Admin__c = TRUE AND EFL_ACIS_Contact__c = null];
        
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        for (EFLAnnual_Report__c annualReport : annualReportsToEmail){
            
            //the to addresses come from the Report Signed By
            //the cc addresses come from facility admins on the account on annual report
            List<String> ccAddresses = new List<String>();
            for (Contact contact : facilityAdmins){
                if (contact.AccountId == annualReportAccountIdMap.get(annualReport.Id)){                    
            		ccAddresses.add(contact.Email);
                }
            }
            List<String> toAddresses = new List<String>();
            for (Contact contact : reportSignedBy){
                System.debug('annualReport.EFLContact__c: ' + annualReport.EFLContact__c);
                System.debug('Contact to email: ' + contact);
                if (contact.Id == annualReport.EFLContact__c){
            		toAddresses.add(contact.Email);
                }
            }
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            System.debug('CCAddresses: ' + ccAddresses);
            /*OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'animalcare@usda.gov']; //Uncomment when the emaila address is verified 
			if ( owea.size() > 0 ) {
   				 mail.setOrgWideEmailAddressId(owea.get(0).Id);
			} */
            mail.setCcAddresses(ccAddresses);    
            System.debug('ToAddresses: ' + toAddresses);
            mail.setToAddresses(toAddresses);
            mail.setBccSender(false);        
            mail.setUseSignature(false);
            mail.setTargetObjectId(annualReport.EFLContact__c);
            mail.setTreatTargetObjectAsRecipient(false);
            mail.setTemplateId(emailTemplate.Id);
            mail.setWhatId(annualReport.Id);
            mail.setSaveAsActivity(false);
            emailsToSend.add(mail);
        }
        System.debug('emailsToSend: ' + emailsToSend);
        Messaging.sendEmail(emailsToSend);
    }
    
    private void addStandardAnimals() {
        if(UserInfo.getUserType() == 'Standard') {
            List<EFLRegistered_Animal__c> regulatedAnimals = new List<EFLRegistered_Animal__c>();
            
            // Get the standard animal ids from the custom setting.  
            // The4 animals are added in the order the Ids are stored in the custom setting
            List<String> stdAnimalIds = getApplicationSettingsByName('ARStandardAnimalIds');
            
            // Get the starting sequence number from the custom setting.  If no value is set, use 0 as default
            Integer startingSequenceNumber = 0;
            EFLACLRA_Application_Settings__c config = EFLACLRA_Application_Settings__c.getInstance('ARRegulatedAnimalsStartingSeqNum');
            if(config != null) {
                if(String.isNotEmpty(config.Value__c)) {
                    startingSequenceNumber = Integer.valueOf(config.Value__c);
                }
            }

            // Get the standard animal records from EFLAnimal__c to avoid situations 
            // where an animal record is deleted without removing their Id from the custom setting
            Map<Id, EFLAnimal__c> standardAnimals = new Map<Id, EFLAnimal__c>([Select Id, Name From EFLAnimal__c Where Id In :stdAnimalIds]);
            for(EFLAnnual_Report__c ar :(List<EFLAnnual_Report__c>)Trigger.new) {
                Integer sequence = startingSequenceNumber;
                for(String stdAnimalId :stdAnimalIds) {
                    if(standardAnimals.containsKey(stdAnimalId)) {
                        EFLAnimal__c animal = standardAnimals.get(stdAnimalId);
                        EFLRegistered_Animal__c ra = new EFLRegistered_Animal__c();
                        ra.EFLAnimal__c = animal.Name;
                        ra.EFLAnimalName__c = animal.Id;
                        ra.EFLAnnual_Report__c = ar.Id;
                        ra.EFLSequence__c = sequence;
                        ra.EFLSelectedForReport__c = false;
                        ra.EFLOtherAnimal__c = animal.Name.contains('Other');
                        ra.EFLColumnBHeldNotUsed__c = 0;
                        ra.EFLColumnCUsedPainMinimized__c = 0;
                        ra.EFLColumnDUsedPainMinimized__c = 0;
                        ra.ELFColumnEPainNotMinimized__c = 0;
                        
                        regulatedAnimals.add(ra);
                        sequence += 1;
                    }
                }
            }
            
            if(!regulatedAnimals.isEmpty()) {
                insert regulatedAnimals;
            }
        }
    }
    
    private List<String> getApplicationSettingsByName(String instanceName) {
        List<String> values = new List<String>();
        
        EFLACLRA_Application_Settings__c config = EFLACLRA_Application_Settings__c.getInstance(instanceName);
        if(config != null) {
            String value = config.Value__c;
            if(String.isNotBlank(value)) {
                for(String aValue :value.split(',')) {
                    values.add(aValue.trim());
                }
            }
        }
        
        return values;
    }
}