public with sharing class EFLAmendmentAndRenewalValidationHelper { 

    public static Boolean doSelectionValidation(List<EFL_Amendment_and_Renewal_Configuration__mdt> amendmentAndRenewalConfig, Authorizations__c auth) {
        Boolean isValidProgram = false;
        Boolean isValidPathway = false;
        Boolean isValidDate = false;

        for (EFL_Amendment_and_Renewal_Configuration__mdt arc : amendmentAndRenewalConfig) {
            if (arc.Validate_Date__c && !isValidDate) {
                isValidDate = validateDate(auth, arc);
            }

            if (arc.Validate_Program__c && !isValidProgram) {
                isValidProgram = validateProgram(auth, arc);
            }

            if (arc.Validate_Pathway__c && !isValidPathway) {
                isValidPathway = validatePathway(auth, arc);
            }
        }

        if (isValidPathway && isValidProgram && isValidDate) {
            return true;
        }

        return false;
    }

    public static Boolean validateDate(Authorizations__c auth, EFL_Amendment_and_Renewal_Configuration__mdt arc) {
        if (auth.Expiration_Date__c.addDays(Integer.valueOf(arc.Number_of_Days__c)) >= Date.today() && auth.Program_Pathway__r.Name == arc.Pathway__c) {
            return true;
        }

        return false;
    }

    public static Boolean validateProgram(Authorizations__c auth, EFL_Amendment_and_Renewal_Configuration__mdt arc) {
        if (auth.Program_Pathway__r.Program__r.Name == arc.Program__c) {
            return true;
        }

        return false;
    }

    public static Boolean validatePathway(Authorizations__c auth, EFL_Amendment_and_Renewal_Configuration__mdt arc) {
        if (auth.Program_Pathway__r.Name == arc.Pathway__c) {
            return true;
        }

        return false;
    }

    public static void linkAuthorizations(Authorizations__c auth, Map<Id, Authorizations__c> authMap, Map<Id, Id> applicationtoRelatedApplicationMap) {
        if (String.isBlank(auth.Associated_Authorization__c)) {
            Id relatedApplicationId = applicationtoRelatedApplicationMap.get(auth.Application__c);

            if (relatedApplicationId != null) {
                Authorizations__c relatedAuth = authMap.get(relatedApplicationId);
                if(relatedAuth != NULL){
                    auth.Associated_Authorization__c = relatedAuth.Id;
                }            }
        }
    }

    public static Map<Id, Authorizations__c> mapAuthorizationsToRelatedApplications(List<Authorizations__c> relatedAuths) {
        Map<Id, Authorizations__c> authMap = new Map<Id, Authorizations__c>();
        for (Authorizations__c relatedAuth : relatedAuths) {
            authMap.put(relatedAuth.Application__c, relatedAuth);
        }

        return authMap;
    }

    public static Map<Id, Id> mapApplicationIdToRelatedApplicationId(List<Application__c> apps) {
        Map<Id, Id> appMap = new Map<Id, Id>();

        for (Application__c app : apps) {
            appMap.put(app.Id, app.RelatedApplication__c);
        }

        return appMap;
    }

    public static List<Application__c> queryApplications(Set<Id> appIds) {
        return [SELECT Id, RelatedApplication__c FROM Application__c WHERE Id IN :appIds];
    }

    public static List<Authorizations__c> queryAuthorizations(Set<Id> parentAppIds) {
        return [SELECT Id, Application__c FROM Authorizations__c WHERE Application__c IN :parentAppIds];
    }

    public static Set<Id> getAppIds(List<Authorizations__c> auths) {
        Set<Id> appIds = new Set<Id>();

        for (Authorizations__c auth : auths) {
            appIds.add(auth.Application__c);
        }

        return appIds;
    }

    public static Set<Id> getRelatedAppIds(List<Application__c> apps) {
        Set<Id> relatedAppIds = new Set<Id>();

        for (Application__c app : apps) {
            if (app.RelatedApplication__c != null) {
                relatedAppIds.add(app.RelatedApplication__c);
            }
        }

        return relatedAppIds;
    }

    public static List<EFL_Amendment_and_Renewal_Configuration__mdt> queryAmendmentAndRenewalMdt() {
        return [SELECT Id
                    , DeveloperName
                    , MasterLabel
                    , Number_of_Days__c
                    , Pathway__c
                    , Program__c
                    , Validate_Date__c
                    , Validate_Program__c
                    , Validate_Pathway__c 
                FROM EFL_Amendment_and_Renewal_Configuration__mdt];
    }

}