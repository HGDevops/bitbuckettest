public inherited sharing class EFLBatchCBPXMLClassHelper {
    public static Blob getDocument(String springCMdocumentId, SpringCMService sprCMService){
        List<attachment> attachToInsert = new List<attachment>();
        Attachment pdfattachment = new Attachment();
        Blob sprCMDocPDF;
        
        try{
            SpringCMDocument sprCMDoc = sprCMService.findDocumentById(springCMdocumentId,null);
            //system.debug('Api call out sprCMDoc>>' + sprCMDoc);
            if(sprCMDoc != null){
                sprCMDocPDF = sprCMService.getPDFRevision(sprCMDoc);
                //system.debug('sprCMDocPDF>>' + sprCMDocPDF);
                
                if(sprCMDocPDF!=null){
                    return sprCMDocPDF;
                }
            }
        }
        catch(Exception e)
        {
            EFLErrorLog.createErrorLog('EFLBatchCBPXMLClassHelper.getDocument()',e);   
        }
        return sprCMDocPDF;
    }
    
    //Function to create a new attachment
    public static attachment createAttachment(string Name, Id Parentid, Blob attachBody, string xml){
        Attachment att = new Attachment();
        att.Name = Name;
        att.ParentId = Parentid;
        if (String.isNotBlank(xml)){
            Blob body = Blob.valueOf(xml);
            att.body = body;
        }
        else {
            att.body = attachBody;
        }
        
        return att;
    }
    
    //function that creates the xml header for the main xml and the summary xml
    public static XmlStreamWriter createHeader(Id eflogId, string rdate){
        
        XmlStreamWriter ws =new XmlStreamWriter();
        //xml declaration
        ws.writeStartDocument('UTF-8', '1.0');
        //top level element and namespaces
        ws.writeStartElement(null,'ITDS-PGA-x:MessageEnvelope', null); 
        ws.writeDefaultNamespace('http://cbp.dhs.gov/ITDS-PGA-e');
        ws.writeNamespace('ITDS-PGA-x', 'http://cbp.dhs.gov/ITDS-PGA-x');
        ws.writeNamespace('nc', 'http://niem.gov/niem/niem-core/2.0');
        ws.writeNamespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        ws.writeAttribute('xsi', 'http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation', 'http://cbp.dhs.gov/ITDS-PGA-x ../exchange/ITDS-PGA-IWS-x.xsd');
        //message header        
        ws.writeStartElement(null, 'MessageHeader', null);
        //According to CBP, this is the schema version for this XML block
        ws.writeStartElement(null, 'MessageVersionText', null);
        ws.writeCharacters('2.79.06'); 
        ws.writeEndElement();            
        
        //The id of our integration log record for tracking
        ws.writeStartElement(null, 'MessageID', null);
        ws.writeCharacters(eflogId); 
        ws.writeEndElement();        
        
        ws.writeStartElement(null, 'MessageType', null);
        ws.writeCharacters('EFileData');
        ws.writeEndElement();                                                
        
        //Transmission date of this job  
        ws.writeStartElement(null, 'MessageSentDate', null);
        ws.writeCharacters(rdate);
        ws.writeEndElement();     
        
        ws.writeStartElement(null, 'MessageSenderID', null);
        ws.writeCharacters('APHIS-F');
        ws.writeEndElement();                                                
        
        ws.writeStartElement(null, 'MessageSenderSystemID', null);
        ws.writeCharacters('APHIS-F1');
        ws.writeEndElement();                                                
        //end MessageHeader                
        ws.writeEndElement(); 
        
        return ws;
    }
    
    //Function to create a new attachment
    public static List<attachment> createAttachmentList(List<Authorizations__c> auths, Id efllogId,  Map<String, Blob> mapBlobAuth,Decimal attachmentSizeSent){
        List<attachment> attachToInsert = new List<attachment>();
        //Run date, set here and used throughout xml generation 
        DateTime rundate = DateTime.Now();
        String rundateString = rundate.format('yyyy-MM-dd\'T\'HH:mm:ssXXX');
        String completeFileNameDate = rundate.format('yyyyMMdd');
        String fileformatDate = rundate.format('yyyyMMdd_HHmmss');
        String fileName;
        String xmlString;
        Integer countofApps = 0;
        Integer numOfApps;
        String appType;
        Blob attBody; 
        Map<string, string> mapErrors = new Map<string, string>();
        Map<string, string> mapAppType = new Map<string, string>();
        Map<string, Integer> mapAppCount = new Map<string, Integer>();
        Map<String, string> mapPDFName = new Map<String, string>();
        
        //create map with program prefix and app types  	
        for(Program_Prefix__c prefix : [Select EFLePermitApplicationType__c, Prefix__c, Name from Program_Prefix__c]){
            mapAppType.put(prefix.Name, prefix.EFLePermitApplicationType__c);
        }
        numOfApps = auths.size();
        //calls the function to create the xml header with all the necessary info as noted in the spec document
        XmlStreamWriter w = EFLBatchCBPXMLClassHelper.createHeader(efllogId, rundateString);
        //start MessageBody of main xml        
        w.writeStartElement(null, 'MessageBody', null);
        //start list of applications/permits
        w.writeStartElement(null, 'EPermitDataList', null);
        
        for(Authorizations__c auth : auths ){ 
            boolean addAttachment = true;	
            //removing charaters from the permit number so that the formating is exactly how it is supposed to be as noted in the spec document     
            String removespace = auth.Name;
            removespace= removespace.replaceAll( '\\s+', '');
            //Retrieve url from map to pass to spring cm api to get document
            
            //writing the permit number to the xml file
            w.writeStartElement(null, 'EPermitData', null);            
            
            w.writeStartElement(null, 'PermitNumber', null);
            //e-File standard for unique name, does not match ePermits naming convention             
            //if the application type is renewal or amendment we need to remove the suffix from the permit number            
            String permitNumber = auth.Permit_Number__c;
            if(String.isNotBlank(permitNumber)){  
                if (auth.Application_Type__c == 'Renewal' || auth.Application_Type__c == 'Amendment'){
                    String[] split = permitNumber.split('-');
                    permitNumber = '';
                    for (Integer i = 0; i < split.size() - 1; i++){
                        //add in the hypens if it isnt the first iteration
                        if (String.isNotBlank(permitNumber)){
                            permitNumber += '-';
                        }
                        permitNumber += split[i];
                    }
                }
                
                //system.debug('permit num ' + permitNumber);
                w.writeCharacters(permitNumber);
            }else {
                //if the permit number is not populated, that is written to the error log in the summary xml
                mapErrors.put(auth.Name, 'Error in ' + auth.name + ': Permit Number not found. ');
            }          
            w.writeEndElement();                                                
            
            //writing the permit type to the xml
            w.writeStartElement(null, 'PermitType', null);
            if(auth.Prefix_CBP__c !='' && auth.Prefix_CBP__c !=null){ 
                try{
                    //getting the permit type from the map that was created based on the auth prefix number
                    appType = mapAppType.get(auth.Prefix_CBP__c);
                    w.writeCharacters(mapAppType.get(auth.Prefix_CBP__c));
                    
                    //checking to see if this permit type is in the map that keeps count of the number of permits of each type
                    if(!mapAppCount.containsKey(appType)){    
                        //if the permit type is not in the map yet, add it in
                        mapAppCount.put(appType, 0);
                    }
                    //this is where the counter gets incremented by 1 each time a permit of that particular type is processed
                    countofApps = integer.ValueOf(mapAppCount.get(appType)) + 1;
                    //updating the count of the permit type in the map
                    mapAppCount.put(appType,countofApps);
                } catch (Exception e) {
                    //if the permit type is not found, add an error to the error map so that it can be put in the summary xml
                    mapErrors.put(auth.Name, 'Error in ' + auth.name + ': Permit Type not found. ');
                }
            }
            w.writeEndElement();                 
            
            //writing the application number to the xml
            w.writeStartElement(null, 'ApplicationNumber', null);
            if(auth.Application__c != null){
                w.writeCharacters(auth.Application__r.Name + '_' + removespace); 
            }
            w.writeEndElement();
            
            //writing the application type to the xml
            w.writeStartElement(null, 'ApplicationType', null);
            if(auth.Application_Type__c !=null){
                w.writeCharacters(auth.Application_Type__c.substring(0,1)); 
            }                      
            
            w.writeEndElement();                      
            
            //information about applicant from their contact information record
            w.writeStartElement(null, 'ApplicantInfo', null);
            //writing applicant first name to xml
            w.writeStartElement('nc', 'PersonGivenName', 'http://niem.gov/niem/niem-core/2.0');
            if(auth.Applicant__r.Firstname != null){
                w.writeCharacters(auth.Applicant__r.Firstname);   
            }
            w.writeEndElement();   
            
            //writing applicant last name to xml
            w.writeStartElement('nc', 'PersonSurName', 'http://niem.gov/niem/niem-core/2.0');
            if(auth.Applicant__r.Lastname !=null){
                w.writeCharacters(auth.Applicant__r.Lastname); 
            }
            w.writeEndElement();                                                                              
            
            //writing applicant account name to xml
            w.writeStartElement('nc', 'OrganizationName', 'http://niem.gov/niem/niem-core/2.0');
            if(auth.Applicant__r.Account.Name != null){
                w.writeCharacters(auth.Applicant__r.Account.Name); 
            }     
            w.writeEndElement();                                                                                                          
            
            w.writeEndElement();                                                                                                                      
            
            //Date permit issued
            w.writeStartElement(null, 'IssuedDate', null);
            if(auth.Date_Issued__c != null){
                w.writeCharacters(auth.Date_Issued__c.year() + '-' + formatSingleDigitString(auth.Date_Issued__c.month()) + '-' + formatSingleDigitString(auth.Date_Issued__c.day()) + 'T00:00:00-04:00');  
            }
            w.writeEndElement();                                                
            
            //Date permit expires
            w.writeStartElement(null, 'ExpirationDate', null);
            //system.debug('Expiration Date: ' + auth.Expiration_Date__c); 
            if(auth.Expiration_Date__c != null){
                Date expdate = auth.Expiration_Date__c;
                w.writeCharacters(expdate.year() + '-' + formatSingleDigitString(expdate.month()) + '-' + formatSingleDigitString(expdate.day()) + 'T23:59:59-04:00');
                
                //do not add attachment for expired permits
                if (auth.Expiration_Date__c < Date.today()){
                    addAttachment = false;
                }
            }
            w.writeEndElement();                                                
            
            //Date permit is effective
            w.writeStartElement(null, 'EffectiveDate', null);
            if(auth.Effective_Date__c != null){
                w.writeCharacters(auth.Effective_Date__c.year() + '-' + formatSingleDigitString(auth.Effective_Date__c.month()) + '-' + formatSingleDigitString(auth.Effective_Date__c.day()) + 'T00:00:00-04:00'); 
            }
            w.writeEndElement();                                                
            
            //writing the permit status to the xml
            w.writeStartElement(null, 'PermitStatus', null);
            if(auth.Status__c != null){
                if(auth.Status__c == 'Issued'){
                    w.writeCharacters(auth.Status__c.substring(0,1));
                }
                if(auth.Status__c == 'Cancelled' || auth.Status__c == 'Revoked'){ 
                    String Cancelledpermitstring = 'Cancelled';
                    w.writeCharacters(Cancelledpermitstring.substring(0,1));
                }
                if(auth.Status__c == 'Superceded'){
                    w.writeCharacters(auth.Status__c.substring(0,1));
                }
                if(auth.Status__c == 'Suspended'){
                    String Suspendedpermitstring = 'Xsuspended Permit';
                    w.writeCharacters(Suspendedpermitstring.substring(0,1));
                } if(auth.Status__c == 'Acknowledged'){
                    String Suspendedpermitstring = 'Acknowledged';
                    w.writeCharacters(Suspendedpermitstring.substring(0,1));
                }  
            }
            w.writeEndElement();                                                
            
            //formatting and writing the completed file name to the xml and putting it in a PDF name map so it can be pulled later to save as the name for the permit PDF
            w.writeStartElement(null, 'CompleteFileName', null);
            
            if (String.isNotBlank(permitNumber)){
                fileName = permitNumber + '_'+auth.Application__r.Name +'_'+removespace+'_'+completeFileNameDate+'.pdf';
            } else {
                fileName = auth.Application__r.Name +'_'+removespace+'_'+completeFileNameDate+'.pdf';
            }
            
            w.writeCharacters(fileName);   
            mapPDFName.put(auth.Name, fileName);
            
            w.writeEndElement();                                                
            
            w.writeEndElement(); 
            //system.debug('mapPDFName>>' +mapBlobAuth.get(auth.Name)); 
            
            if (mapBlobAuth.get(auth.Name) != null && addAttachment)
            {
                attBody =mapBlobAuth.get(auth.Name); 
                attachment pdfAtt = EFLBatchCBPXMLClassHelper.createAttachment(filename, efllogId, attBody, null);
                attachToInsert.add(pdfAtt);
            }
        } //end for loop
        
        w.writeEndElement();            
        w.writeEndElement(); //end MessageBody
        w.writeEndElement();  //end ITDS-PGA-x:MessageEnvelope
        
        string xml = w.getXmlString();
        xmlString = xml;
        
        w.close();
        //system.debug(xml);
        
        //write xml to log and attach it
        attachment efileAtt = EFLBatchCBPXMLClassHelper.createAttachment('eFile_'+fileformatDate+'.xml', efllogId, null, xml);
        //add the xml attachment to the list of attachments to be inserted all at once to cut down on the number of DMl statements
        attachToInsert.add(efileAtt);
        
        //calls the function to create the xml summary header with all the necessary info as noted in the spec document
        XmlStreamWriter ws = EFLBatchCBPXMLClassHelper.createHeader(efllogId, rundateString);
        //start MessageBody of summary/reconciliation file         
        ws.writeStartElement(null, 'MessageBody', null);                     
        ws.writeStartElement(null, 'EPermitReconciliationDataList', null);
        //writing the total bytes transferred to the xml
        ws.writeStartElement(null, 'EPermitTotalDataTransferred', null);
        //if there are permit PDFs sent, calculate the amount sent in KB formatted out to two decimal places
        if(attachmentSizeSent != 0)
            attachmentSizeSent = attachmentSizeSent/1000;
        ws.writeCharacters(string.valueof(attachmentSizeSent.SetSCale(2)) + ' KB');
        ws.writeEndElement();                                                                
        
        //writing the permit count list to the xml
        ws.writeStartElement(null, 'EPermitPermitCountList', null);
        
        //looping through the permit types in the map and getting the count of each
        if(mapAppCount.keySet() != null && !mapAppCount.keySet().isEmpty()){
            for(string permitMap : mapAppCount.keySet()){
                //system.debug('Begin summary xml');
                ws.writeStartElement(null, 'EPermitPermitCount', null);
                //writing the permit type to the xml
                ws.writeStartElement(null, 'PermitType', null);
                ws.writeCharacters(permitMap);
                ws.writeEndElement(); //end PermitType                                                     
                //The number of permits for a given type
                ws.writeStartElement(null, 'PermitCount', null);
                ws.writeCharacters(String.valueOf(mapAppCount.get(permitMap)));
                ws.writeEndElement(); //end PermitCount
       			ws.writeEndElement(); //end EPermitPermitCount
            }            
        } 
        
        //writing the total number of permits procesed in this transaction
        ws.writeStartElement(null, 'TotalPermitCount', null);
        ws.writeCharacters(String.valueof(numOfApps));
        ws.writeEndElement(); //end TotalPermitCount
        ws.writeEndElement(); //end EPermitPermitCountList
        
        ws.writeEndElement(); //end EPermitReconciliationDataList                                        
        ws.writeEndElement(); //end MessageBody
        ws.writeEndElement(); //end ITDS-PGA-x:MessageEnvelope
        
        String xmlsummary = ws.getXmlString();
        ws.close();
        
        Attachment attws = EFLBatchCBPXMLClassHelper.createAttachment('Reconciliation_eFile_'+fileformatDate+'.xml', efllogId, null, xmlsummary);
        attachToInsert.add(attws);
        return attachToInsert;
    }
    
    public static string formatSingleDigitString(Integer intToConvert){
        //prepend a 0 if it is a single digit string
        if (intToConvert < 10){
            return '0' + intToConvert;
        }
        return String.valueOf(intToConvert);
    }
    
    public static boolean validateStatus(Authorizations__c auth){
        return auth.Status__c == 'Issued' || 
           auth.Status__c == 'Cancelled' || 
           auth.Status__c == 'Revoked' ||
           auth.Status__c == 'Superceded' ||
           auth.Status__c == 'Suspended' ||
           auth.Status__c == 'Acknowledged' ;            
    }
    
    public static Attachment createMarkerFile(string efllogId, boolean isSuccess, Integer attachmentSizeSent){
        //calls the function to create the xml summary header with all the necessary info as noted in the spec document
        XmlStreamWriter ws = EFLBatchCBPXMLClassHelper.createHeader(efllogId, DateTime.Now().format('yyyy-MM-dd\'T\'HH:mm:ssXXX'));
        //start MessageBody of summary/reconciliation file         
        ws.writeStartElement(null, 'MessageBody', null);
        //writing the total bytes transferred to the xml
        ws.writeStartElement(null, 'EPermitTotalDataTransferred', null);
        //if there are permit PDFs sent, calculate the amount sent in KB formatted out to two decimal places
        if(attachmentSizeSent != 0){
            attachmentSizeSent = attachmentSizeSent/1000;
        }
        ws.writeCharacters(string.valueof(attachmentSizeSent) + ' KB');
        ws.writeEndElement(); 
        
        ws.writeStartElement(null, 'EFLIntegrationTransferStatus', null);
        String success = isSuccess ? 'Success' : 'Failure';
        ws.writeCharacters(success);
        ws.writeEndElement();
        
        ws.writeEndElement(); //end MessageBody
        ws.writeEndElement();  //end ITDS-PGA-x:MessageEnvelope
        
        String xmlsummary = ws.getXmlString();
        ws.close();
        //system.debug('Marker File: ' + xmlsummary);
        return EFLBatchCBPXMLClassHelper.createAttachment('Marker_eFile_' + Datetime.Now().format('yyyyMMdd_HHmmss') + '.xml', efllogId, null, xmlsummary);
    }
}