@isTest
private class EFLEnforceAccessUtility_Test {
    static testMethod void TestcheckObjectUpdateAccess() {
        
        String uniqueUserName = 'standarduser' +  EFLGenericUtility.randomString() + '@testaphis.com';
        Profile p = [SELECT Id FROM Profile WHERE Name='READ ONLY'];
        User u = new User(Alias = 'standt', Email='standarduser@testaphis.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        
        Test.startTest();
        // Run as Admin
        EFLEnforceAccessUtility.checkObjectUpdateAccess('Contact');
        EFLEnforceAccessUtility.checkObjectDeleteAccess('Contact');
        EFLEnforceAccessUtility.checkObjectInsertAccess('Contact');
        
        // Run as newuser
        System.runAs(u) {
            try{
                EFLEnforceAccessUtility.checkObjectUpdateAccess('Contact');
            } catch(Exception e) {        
                system.assert(e.getMessage() == 'Insufficient Update access to Contact Object');
                
            }            
        } 
        System.runAs(u) {
            try{                
                EFLEnforceAccessUtility.checkObjectInsertAccess('Contact');                
            } catch(Exception e) {        
                system.assert(e.getMessage() == 'Insufficient Create access to Contact Object');                
            }            
        }
        System.runAs(u) {
            try{                
                EFLEnforceAccessUtility.checkObjectDeleteAccess('Contact'); 
            } catch(Exception e) {        
                system.assert(e.getMessage() == 'Insufficient Delete access to Contact Object');                
            }            
        }        
        Test.stopTest();
    }
    
    
}