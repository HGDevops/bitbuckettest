public inherited sharing class EFLRelatedRecordTriggerHandler {
    public static void validateUniqueEFLRelatedRecord(List<EFL_Related_Record__c> relRecords){
        
        //Get Construct Record Type
        Id constructrectypeid = Schema.SObjectType.EFL_Related_Record__c.getRecordTypeInfosByName().get('Construct').getRecordTypeId();
        Map<Id,EFL_Related_Record__c> maprelRecordConstructId = new Map<Id,EFL_Related_Record__c> ();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Set<Self_Reporting__c> srIds = new Set<Self_Reporting__c>();
        Map<Id,EFL_Related_Record__c> mapSelfReporting = new Map<Id,EFL_Related_Record__c>();
        Boolean gpsExists=false;
        Boolean relRecordsExists=false;
        try{
            //Get all related records that are inserted by the execution context in Trigger.new
            for(EFL_Related_Record__c relRecord: relRecords){
                //Check for records from trigger that have a construct record type
                if(relRecord.RecordTypeId == constructrectypeid && !String.isBlank(relRecord.Self_Reporting__c)){
                    if(!trigger.isDelete){
                        relRecordsExists = true;
                    }
                    //Create map to add the related record
                    mapSelfReporting.put(relRecord.Self_Reporting__c, relRecord);
                    //Get List of construct Ids
                    maprelRecordConstructId.put(relRecord.Construct__c,relRecord);
                    
                }// end if
            }// end for loop
            //Validate if duplicate construct records exist for the same self report
            Integer relRecCount=0;
            if(!mapSelfReporting.isEmpty()){
                for(EFL_Related_Record__c relRec : [Select id,Self_Reporting__c,Construct__c from EFL_Related_Record__c 
                                                    Where Self_Reporting__c in :mapSelfReporting.keyset()]){// AND Construct__c in :maprelRecordConstructId.keyset()]
                                                           if(maprelRecordConstructId.containskey(relRec.Construct__c )){
                                                        mapSelfReporting.get(relRec.Self_Reporting__c).Construct__c.addError('This Construct has already been added to this planting/release. Select a unique construct.'); 
                                                         }
                                                        relRecCount++;
                                                    }
            }
            //Validate if atleast one GPS Coordinate Records exist for the Self Reports
            for(GPS_Coordinate__c gpsRec : [Select id,Self_Reporting__r.Ready__c from GPS_Coordinate__c 
                                            Where Self_Reporting__c in :mapSelfReporting.keyset()]){
                                                //Set Self Report Ready to true - Atleast one GPS coordinate record and alteast one construct record exists on the self Report 
                                                gpsExists=true;                                       
                                                if(relRecordsExists && gpsExists){
                                                    gpsRec.Self_Reporting__r.Ready__c = true;
                                                    srIds.add(gpsRec.Self_Reporting__r);
                                                    //mapSelfReportingIds.put(gpsRec.Self_Reporting__c,gpsRec.Id);
                                                }else if (gpsExists && relRecCount == 0){
                                                    gpsRec.Self_Reporting__r.Ready__c = false;
                                                    srIds.add(gpsRec.Self_Reporting__r);
                                                }
                                                
                                            }//end for loop
            if(!srIds.isEmpty()){
                srList.addall(srIds);
                update srList;
            }
        }
        catch(Exception e){
            //Error  log
            EFLErrorLog.createErrorLog('EFLRelatedRecordTriggerHandler.validateUniqueEFLRelatedRecord()',e);   
        }
        
    }
    
}