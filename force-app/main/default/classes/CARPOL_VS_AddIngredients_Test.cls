@isTest(seealldata=true)
public class CARPOL_VS_AddIngredients_Test {
      @IsTest static void CARPOL_VS_AddIngredients_Test() {
          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId; 
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Expiration_Timeframe__c = '365';
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          VS__c objvs = testData.newVS(objcont.Id, objapp.Id);
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_VS_AddIngredients;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objvs);
              ApexPages.currentPage().getParameters().put('id',objvs.id);

              CARPOL_VS_AddIngredients extclass = new CARPOL_VS_AddIngredients(sc);
              extclass.addRow();
              extclass.saveIngredients();
              extclass.removeRow();
              system.assert(extclass != null);
              CARPOL_VS_AddIngredients extclass2 = new CARPOL_VS_AddIngredients(sc);              
          Test.stopTest();    

      }

}