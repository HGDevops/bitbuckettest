global with sharing class CARPOL_UserInviteController  {

    public String myContacts { get; set; }

    public static User userDetails { get; set; }
    public static Contact contactDetails { get; set; }
    public static InvitedContact contactInvite {get;set;}
    public Contact con {get;set;}
    public static Boolean inviteSent {get;set;}
    public static Id previousInviteId {get; set;}
    public static  List<InvitedContact> inviteeList {get;set;}
    
    public static void  initializeData(){
       inviteeList = new List<InvitedContact>();
        inviteSent = false;
        contactInvite = new InvitedContact();
        ID userID = UserInfo.getUserID();
        userDetails = [SELECT Id, ContactID, Name, Profile.UserLicense.Name FROM User WHERE ID  =: UserID LIMIT 1];

        if(userDetails.ContactID != null)
        {	
            contactDetails = [SELECT ID, FirstName, LastName, Account.Name, Account.Id, State__r.name, Phone, HomePhone, Email, Title FROM Contact WHERE ID = : userDetails.ContactID LIMIT 1];
			
        }
    
    }
    
    public class InvitedContact{
        
        public String email {get;set;}
        public String firstName {get;set;}
        public String lastName {get;set;}
        public String status {get;set;}
        public String userName {get;set;} 
        public String oldUserId {get;set;}
        public String FedId {get;set;}
        public String newConId {get;set;}
        public String UserInviteId {get;set;}
        public String dateInvitation{get;set;}
        public String errorMsg{get;set;}
        
    }
    
        
     public static string verifyInviteeEmail(InvitedContact c){
    
        
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern myPattern = Pattern.compile(emailRegex);
        Matcher myMatcher = myPattern.matcher(c.email);
        
        if (!myMatcher.matches()) 
            return c.Email + ' is not a valid email.';
            
        return 'valid'; 


    }
      
        
    @RemoteAction
    public static String inviteUsers(List<InvitedContact> contactInvite){
    try{
        initializeData();
        if(userDetails.ContactId == null && !Test.isRunningTest())
            return 'Contct ID is null';
        List<InvitedContact> inviteeList= new List<InvitedContact>();
        List<InvitedContact> inviteeListDB= new List<InvitedContact>();
        
       
        for(InvitedContact invitee : contactInvite){ 
            String msg=verifyInviteeEmail(invitee);
            if(msg=='Valid'){
                    invitee.status='Sent'; 
                    invitee.errorMsg=msg;  
                    inviteeList.add(invitee);
                    inviteeListDB.add(invitee);

            }else{
                    invitee.status='Not Sent';
                    invitee.errorMsg=msg;
                    inviteeList.add(invitee);
            
            }
         }
           User_Invite__c userInvite = new User_Invite__c();
           userInvite.Partner_Admin__c = contactDetails.id;
           userInvite.Partner_User__c = userDetails.id;
           userInvite.Partner_Account__c = contactDetails.Account.id;
           userInvite.Invitee_List__c = JSON.serialize(inviteeListDB);
           insert userInvite;
           previousInviteId = userInvite.id;
           
           sendInviteEmails(inviteeListDB, contactDetails);
           
       
            return JSON.serialize(inviteeList);
        }
        catch(Exception e){
            return e.getStackTraceString();
        }
    }
    
    @RemoteAction
    public static String resendEmail(List<InvitedContact> contactInvite){
    try{
        initializeData();
        if(userDetails.ContactId == null)
            return 'Contct ID is null';            
         sendInviteEmails(contactInvite, contactDetails);
         
         return 'Successfully Resent Email Invitation';
    }
     catch(Exception e){
         return e.getStackTraceString();
     }
    }
    
    @RemoteAction
    public static String getPreviousInvitees(){
    try{
        
        List<InvitedContact> inviteeList=new List<InvitedContact>();
        ID userID = UserInfo.getUserID();
        userDetails = [SELECT Id, ContactID, Name, Profile.UserLicense.Name FROM User WHERE ID  = : UserID LIMIT 1];

         List<User_Invite__c> previousInviteL = [Select Id, Invitee_List__c, Partner_Account__c, Partner_Admin__c, Partner_User__c from User_Invite__c where Partner_User__c =: userDetails.Id];
            
        if(previousInviteL != null){
            for(User_Invite__c invi: previousInviteL){
                previousInviteId = invi.id;
                List<InvitedContact> singleList=(List<InvitedContact>)Json.deserialize(invi.Invitee_List__c, List<InvitedContact>.class);
                for(InvitedContact sp:singleList){
                  inviteeList.add(sp);
                 }
               
            }
           
        }
        if(inviteeList != null)
            return Json.serialize(inviteeList);
        else
            return 'worked';
       }
       catch(Exception e){
           return e.getStackTraceString();
       }
    }
    // You have been invited to join Partner Test Account's partner community. In order to accept this invitation you will 
    // need an existing customer community user with an eAuth account. If you do not have one, you can request for an eAuth here.
 
    // To accept this invitation, login to your existing or newly created customer account that matches 
    // with your eAuth account, and click here.
    public static void sendInviteEmails(List<InvitedContact> contacts, Contact admin){
        try{
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(InvitedContact c: contacts){
                List<String> sendTo = new List<String>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                sendTo.add(c.email);
                mail.setToAddresses(sendTo);
                mail.setSenderDisplayName(userDetails.Name);
                mail.setSubject('Invitation to join partner portal.');
                String communityUrl = String.ValueOf(Label.ExternalURL);
                String eAuthUrl = '<a href=\'https://identitymanager.eems.usda.gov/registration/selfRegistrationForm.aspx?level=2\'>here.</a>'; 
                String body = 'Dear ' + c.firstName + ' ' + c.lastName + ',' +
                    '<br/><br/>' + 'You have been invited to join ' + admin.Account.Name + '\'s partner community. ' +
                    'In order to accept this invitation you will need an existing customer community user with an eAuth account. ' +
                    'If you do not have one, you can request for an eAuth ' + eAuthUrl + 
                    '<br/><br/>To accept this invitation, login to your existing or newly created customer account with eAuth, and '+
                    '<a href=\'' + communityUrl + '/apex/invite_acceptance?accId=' +
                    admin.Account.Id + '&lastName=' + c.LastName + '&firstName=' + c.FirstName +
                    '&emailaddress=' + c.Email + '&userInviteId=' + previousInviteId + '\'>click here.</a>';
                mail.setHtmlBody(body);
                mails.add(mail);
            }
        
        Messaging.sendEmail(mails); //send email
        inviteSent = true;
        }catch(Exception e){
            System.Debug(e.getMessage());
        }
    }
    
  
}