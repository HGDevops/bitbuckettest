public inherited sharing class CARPOL_UNI_Components{
    public static map<string,string> allAnswersMap = new map<string,string>();
    
    public static List<SelectOption> ComponentList(string sSelArticle){
        List<SelectOption> options = new List<SelectOption>();
        List<Component__C> lstComp = [Select Id, Name from component__c 
                                      where Id IN(Select Component__c 
                                                  From RA_Component_Junction__c 
                                                  where Regulated_Article__c=:sSelArticle and status__c =: 'Active') ORDER BY Name NULLS LAST];
        if(lstComp!= null && lstComp.size()>0){
            for(Component__C comp: lstComp){
                options.add(new SelectOption(comp.Id,comp.Name));
                allAnswersMap.put(comp.Id,comp.Name);
            }
        }
        return options;
    }
    
    public static List<SelectOption> ScientificList(string sSelArticle){
        List<SelectOption> options = new List<SelectOption>();
        
        for(RA_Scientific_Name_Junction__c scienObj: [select id,RA_Scientific_Name__c,RA_Scientific_Name__r.name,Regulated_Article__r.name from RA_Scientific_Name_Junction__c
                                                      where Regulated_Article__c=:sSelArticle and Program_Pathway__c = null and status__c =: 'Active' and RA_Scientific_Name__c != null]){
                                                          
                                                          options.add(new SelectOption(scienObj.RA_Scientific_Name__c,scienObj.RA_Scientific_Name__r.Name));
                                                          
                                                          allAnswersMap.put(scienObj.RA_Scientific_Name__c,scienObj.RA_Scientific_Name__r.Name);
                                                          
                                                      }
        
        return options;
    }
}