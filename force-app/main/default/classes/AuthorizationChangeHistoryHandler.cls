//W-032142-Change History - Track Authorization Changes
public class AuthorizationChangeHistoryHandler implements IChangeHistoryHandler{
    static final string fieldName = 'Name';
    static final string fieldLabel = 'Name';    
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        Schema.SObjectType sObjectType = newRecord.getSObjectType();
        if (sObjectType != null)
        {
            System.debug('NEw Entry: '+ newRecord.get(fieldName));
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
                Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {
        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.Old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
          populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        Authorizations__c auth = (Authorizations__c)record;
      //  change.Facility__c = auth.Plant_Inspection_Station__c;
      //  change.Application__c = auth.Application__c;
        if(!change.Delete_Flag__c)change.Authorization__c = auth.id;
        
    }
}