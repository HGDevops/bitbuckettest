public class EFLWizardActivity {
    
    public Wizard_Questionnaire__c Question{get;set;} //prescreen question
    public List<Wizard_Selections__c> Options {get; set;}
    public Wizard_Selections__c EvaluatedOption {get;set;}
    public EFLWizardFlow parentFlow {get;set;}
    public String SelectedOption {get;set;}
    public String activityResult {get;set;}
    
    public EFLWizardActivity(EFLWizardFlow w, Wizard_Questionnaire__c q, List<Wizard_Selections__c> o )
    {
        this.parentFlow = w;
        this.Question = q;
        this.Options = o;
    }
    
}