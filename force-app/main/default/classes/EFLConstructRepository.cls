/*
* Purpose: Central place to access Construct/Genotypes/Phenotypes object from database with different set of filter parameters.
*/
public with sharing class EFLConstructRepository {
    
    //Constants
    static final string LESSER_THAN_EQUALTO = '<=';
    static final string ASCENDING = 'ASC';
    static final string DESCENDING = 'DESC';
    
    //Ravee Racharla 12/18/2018 W-031978 This method is used for construct cloning. Please do not add any
    //fields related to construct object. 
    // Select all PhenoTypes by Construct ID
    // Ravee Racharla - W-035205. Included Construct and LineItem ownerIds
    public static List<Phenotype__c> selectPhenotypesByConstruct(id constructID){
        return [SELECT ID,Name,Applicant_Instructions__c,Phenotypic_Category__c,Phenotype_Category_Text__c,
                Phenotype_Category_CBI__c , Phenotypic_Description__c, Construct__r.OwnerId, Construct__r.Line_Item__r.OwnerId 
                FROM Phenotype__c 
                WHERE Construct__c=:constructID
                ORDER BY CreatedDate ASC
               ]; 
    }
    //Ravee Racharla 12/18/2018 W-031978  Begin of Code 
    // Select all PhenoTypes by Construct ID for cloning. Donot add construct fields 
    public static List<Phenotype__c> selectPhenotypesByConstructForClone(id constructID){
        return [SELECT ID,Name,Applicant_Instructions__c,Phenotypic_Category__c,Phenotype_Category_CBI__c,
                Phenotypic_Description__c
                FROM Phenotype__c 
                WHERE Construct__c=:constructID
                ORDER BY CreatedDate ASC ]; 
    }
    //Ravee Racharla 12/18/2018 W-031978 End of Code 
    
    // Select  PhenoType by ID
    public static Phenotype__c selectPhenotypeByID(id PhenoID){
        return [SELECT ID,Name,Applicant_Instructions__c,Phenotypic_Category__c,Phenotype_Category_Text__c,
                Phenotypic_Description__c,Construct__c 
                FROM Phenotype__c 
                WHERE ID =:PhenoID
                ORDER BY CreatedDate ASC
               ]; 
    }
    
    // Select all Genotypes Parent by Construct ID 
    public static List<GenotypeType__c> selectParentGenotypesByConstruct(id constructID){
        return [Select Id, Name, Genotype_Category__c 
                from GenotypeType__c where Construct__c = :constructID  
                order by createdDate ASC];
    }
    
    //Ravee Racharla 12/18/2018 W-031978  Clone Construct
    public static List<Genotype__c> selectgenoTypeforClone(id genoTypeTypeId){
        
        return [SELECT ID,recordtype.Id, 
                Construct_Component__c,Construct_Component_Text__c,
                Construct_Component_Name__c,Donor_list__c,Description__c,
                Construct_Component_Sort_Order__c
                FROM Genotype__c 
                WHERE GenotypeType__c=:genoTypeTypeId
                //ORDER BY recordtype.name,
                //Construct_Component_Sort_Order__c ASC,
                //CreatedDate ASC ];
                ORDER BY Construct_Component_Sort_Order__c ASC]; 
    }
    
    // Select all Genotypes by Construct ID 
    // Ravee Racharla - W-035205. Included Construct and LineItem ownerIds
    public static List<Genotype__c> selectGenotypesByConstruct(id constructID){
        return [SELECT ID,Name,recordtype.name,recordtype.Id, 
                GenotypeType__c, GenotypeType__r.Genotype_Category__c, //W-026999
                Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                Construct_Component_Name__c,Donor_list__c,Description__c,
                Construct_Component_Sort_Order__c, Related_Construct_Record_Number__r.OwnerId, 
                Related_Construct_Record_Number__r.Line_Item__r.OwnerId
                FROM Genotype__c 
                WHERE Related_Construct_Record_Number__c=:constructID 
                ORDER BY GenotypeType__r.Name,
                Construct_Component_Sort_Order__c ASC,
                CreatedDate ASC 
               ];
    }  
    
    // Select all Genotypes by ID  
    public static Genotype__c selectGenotypeByID(id genoTypeID){
        
        Genotype__c genoTypeRecord =[SELECT ID,Name,recordtype.name,recordtype.Id, 
                                     GenotypeType__c, GenotypeType__r.Genotype_Category__c, //W-026999
                                     Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                                     Construct_Component_Name__c,Donor_list__c,Description__c,
                                     Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c,
                                     Related_Construct_Record_Number__r.Construct_s__c 
                                     FROM Genotype__c 
                                     WHERE Id =:genoTypeID 
                                     ORDER BY recordtype.name,
                                     Construct_Component_Sort_Order__c ASC,
                                     CreatedDate ASC 
                                     limit 1
                                    ];
        
        return genoTypeRecord; 
    }     
    
    // Select the Genotypes by RecordTypes for each Construct - To be deleted (DO NOT USE)
    public static List<Genotype__c> selectGenotypesbyRecordtype(id constructID,id recordTypeId){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Genotype__c');
            return [SELECT ID,Name,recordtype.name,recordtype.Id,
                    Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                    Construct_Component_Name__c,Donor_list__c,Description__c,
                    Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c,
                    Related_Construct_Record_Number__r.Construct_s__c 
                    FROM Genotype__c 
                    WHERE Related_Construct_Record_Number__c=:constructID 
                    AND recordtypeid =: recordTypeId
                    ORDER BY recordtype.name,
                    Construct_Component_Sort_Order__c ASC,
                    CreatedDate ASC
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectGenotypesbyRecordtype()',e);                
        } 
        return null;
    }        
    
    // Obtain Construct record using line Item ID    
    public static List<construct__c> selectByLineItemID(id lineItemID){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
            return [SELECT Id,Construct_s__c,Mode_of_Transformation_Text__c,Identifying_Line_s__c,
                    Name,Mode_of_Transformation__c,CreatedDate,Regulated_Article__c,
                    Total_No_of_PhenoTypes__c,
                    (SELECT ID,Name,recordtype.name,recordtype.Id,
                     Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                     Construct_Component_Name__c,Donor_list__c,Description__c,
                     Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c,
                     Related_Construct_Record_Number__r.Construct_s__c 
                     FROM Genotypes__r 
                     ORDER BY recordtype.name,
                     Construct_Component_Sort_Order__c ASC,
                     CreatedDate ASC),
                    (SELECT ID,Name,Applicant_Instructions__c,
                     Phenotype_Category_Text__c,Phenotypic_Category_Abbreviation__c,
                     Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,
                     Construct__r.Id
                     FROM Phenotypes__r
                     ORDER BY CreatedDate ASC)
                    FROM Construct__c 
                    WHERE line_item__c =:lineItemId 
                    ORDER BY CreatedDate ASC
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectByLineItemID()',e);                
        } 
        return null;
    }
    
    /* Get all Construct records    */
    public static List<construct__c> selectAllConstructs(){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
            return [SELECT Id,Construct_s__c,Mode_of_Transformation_Text__c,Identifying_Line_s__c,
                    Name,Mode_of_Transformation__c,CreatedDate,Regulated_Article__c,Action_Required__c,
                    Total_No_of_PhenoTypes__c, Status_Graphical__c, Status__c,
                    (SELECT ID,Name,Applicant_Instructions__c,
                     Phenotype_Category_Text__c,Phenotypic_Category_Abbreviation__c,
                     Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,
                     Construct__r.Id
                     FROM Phenotypes__r
                     ORDER BY CreatedDate ASC)
                    FROM Construct__c 
                    ORDER BY CreatedDate Desc limit 10000
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectAllConstructs()',e);                
        } 
        return null;
    }
    
    // Obtain Construct record using construct ID   
    public static construct__c selectByID(id constructID){
        return [SELECT Id,Construct_s__c,Link_Regulated_Article__c,Mode_of_Transformation_Text__c,Identifying_Line_s__c,Line_Item__c,
                Name,Mode_of_Transformation__c,CreatedDate,Regulated_Article__c,Corrections_Required__c, Revoked__c,Link_Regulated_Article__r.Name,Regulated_Article_CBI__c, 
                Total_No_of_PhenoTypes__c,Status_Graphical__c,status__c,Mode_of_Transformation_CBI__c,
                (SELECT ID,Name,recordtype.name,recordtype.Id,
                 Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                 Construct_Component_Name__c,Donor_list__c,Description__c,
                 Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c,
                 Related_Construct_Record_Number__r.Construct_s__c
                 FROM Genotypes__r             
                 ORDER BY recordtype.name,
                 Construct_Component_Sort_Order__c ASC,
                 CreatedDate ASC),
                (SELECT ID,Name,Applicant_Instructions__c,
                 Phenotype_Category_Text__c,Phenotypic_Category_Abbreviation__c,
                 Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,
                 Construct__r.Id
                 FROM Phenotypes__r
                 ORDER BY CreatedDate ASC)
                FROM Construct__c 
                WHERE id =:constructID
                ORDER BY CreatedDate ASC
               ];
    }
    // Obtain list of Constructs for a set of Construct Ids
    public static list<construct__c> selectByIDs(set<Id> constructIDs){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
            return [SELECT Id,Construct_s__c,Mode_of_Transformation_Text__c,Identifying_Line_s__c,Line_Item__c,
                    Name,Mode_of_Transformation__c,CreatedDate,Regulated_Article__c,Corrections_Required__c,Link_Regulated_Article__r.Name,Regulated_Article_CBI__c,
                    Total_No_of_PhenoTypes__c,Status_Graphical__c,status__c,
                    (SELECT ID,Name,recordtype.name,recordtype.Id,
                     //W-031276 - applying the new geno type datamodel based query
                     GenotypeType__c,GenotypeType__r.Genotype_Category__c,
                     Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                     Construct_Component_Name__c,Donor_list__c,Description__c,
                     Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c,
                     Related_Construct_Record_Number__r.Construct_s__c 
                     FROM Genotypes__r     
                     //W-031276 - applying the new geno type datamodel based query
                     /*ORDER BY recordtype.name,
Construct_Component_Sort_Order__c ASC,
CreatedDate ASC),*/
                     ORDER BY 
                     GenotypeType__r.Name ASC,
                     GenotypeType__c ASC,
                     Construct_Component_Sort_Order__c ASC,
                     CreatedDate ASC),
                    (SELECT ID,Name,Applicant_Instructions__c,
                     Phenotype_Category_Text__c,Phenotypic_Category_Abbreviation__c,
                     Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,
                     Construct__r.Id
                     FROM Phenotypes__r
                     ORDER BY CreatedDate ASC)
                    FROM Construct__c 
                    WHERE id IN:constructIDs 
                    ORDER BY CreatedDate ASC
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectByIDs()',e);                
        } 
        return null;
    }
    
    // Obtain all Construct IDs of a lineItem using line Item ID    
    public static Set<ID> selectIDsByLineItemID(id lineItemID){
        try{ 
            EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
            list<Construct__c> constructList = [SELECT Id
                                                FROM Construct__c 
                                                WHERE line_item__c =:lineItemId
                                                and Revoked__c =FALSE
                                                and Status__c != 'Draft'];  //Ravee Racharla 12/18/2018 W-026422
            List<Construct_Application_Junction__c> prevReviewedConstructsList=[SELECT Construct__c 
                                                                                FROM Construct_Application_Junction__c
                                                                                WHERE Line_Item__c =: lineItemID
                                                                                AND Construct__c != NULL
                                                                                AND construct__r.Revoked__c =FALSE];  //Ravee Racharla 12/18/2018 W-026422
            Set<Id> constructIDs = (new Map<Id,SObject>(constructList)).keySet();
            Set<Id> prevConstructIDs = new set<ID>();
            for(Construct_Application_Junction__c pre : prevReviewedConstructsList){
                prevConstructIDs.add(pre.Construct__c);
            }
            
            prevConstructIDs.addAll(constructIDs);  
            return prevConstructIDs;
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectByLineItemID()',e);                
        } 
        return null;
    }    
    
    // Get Constructs with the filters provided    
    public static List<Construct__c> selectConstructs(String status, String decision,
                                                      Set<Id> regulatedArticleIDs, Boolean revoked, List<String> permitTypes,
                                                      List<String> cbiTypes, Id accountId){
                                                          
                                                          try { 
                                                              EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
                                                              list<Construct__c> constructList = [SELECT Id, Name, Construct_s__c, Revoked__c
                                                                                                  FROM Construct__c 
                                                                                                  WHERE Status__c = :status 
                                                                                                  AND Decision__c = :decision 
                                                                                                  AND Link_Regulated_Article__c IN :regulatedArticleIDs
                                                                                                  AND Revoked__c = :revoked
                                                                                                  AND Line_Item__r.Type_of_Permit__c LIKE :permitTypes
                                                                                                  AND Line_Item__r.Does_This_Application_Contain_CBI__c IN :cbiTypes
                                                                                                  AND Line_Item__r.Application_Number__r.Applicant_Name__r.AccountId = :accountId];
                                                              
                                                              return constructList;
                                                          } catch(exception e) {
                                                              EFLErrorLog.createErrorLog('EFLConstructRepository.selectConstructs()', e);                
                                                          } 
                                                          return null;
                                                      }
    
    // Obtain Construct records using line Item ID    
    public static List<construct__c> selectConstructsByLineItemID(id lineItemID){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
            return [SELECT Id,Construct_s__c,Mode_of_Transformation_Text__c,Identifying_Line_s__c,Line_Item__c,
                    Name,Mode_of_Transformation__c,CreatedDate,Regulated_Article__c,Corrections_Required__c,
                    Total_No_of_PhenoTypes__c,Status_Graphical__c,status__c,Mode_of_Transformation_CBI__c,Link_Regulated_Article__r.Name,Regulated_Article_CBI__c
                    FROM Construct__c 
                    WHERE line_item__c =:lineItemId 
                    ORDER BY CreatedDate ASC
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectConstructsByLineItemID()',e);                
        } 
        return null;
    } 
    
    /* Select Moved Construct Component and Consecutive Construct Component (differed by Operator) for Ordering - Move Up/Down */
    public static List<Genotype__c> selectConstructComponentsForReOrdering(id contructComponentId, id genotypeTypeId, string moveQueryOperator)
    {
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Genotype__c');  
            integer movedRecordSortOrderNumber = (integer)[select Construct_Component_Sort_Order__c from Genotype__c where id = :contructComponentId limit 1].Construct_Component_Sort_Order__c;
            string orderDirection = ASCENDING;
            if(moveQueryOperator==LESSER_THAN_EQUALTO)
            {
                orderDirection = DESCENDING;
            }
            return  database.query('SELECT ID,Name,GenotypeType__c, GenotypeType__r.Genotype_Category__c, Construct_Component_Sort_Order__c FROM Genotype__c WHERE GenotypeType__c = :genotypeTypeId and Construct_Component_Sort_Order__c ' + moveQueryOperator + ' :movedRecordSortOrderNumber ORDER BY Construct_Component_Sort_Order__c ' + orderDirection + ' limit 2');
        }
        catch(exception e)
        {
            EFLErrorLog.createErrorLog('EFLConstructRepository.selectConstructComponentsForReOrdering()',e);                
        } 
        return null;
    }
}