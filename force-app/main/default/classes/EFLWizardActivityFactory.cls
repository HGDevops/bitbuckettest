public class EFLWizardActivityFactory {
    
       public static EFLIWizardActivityProcessor getActivityProcessor(EFLWizardActivity wizardActivity)
    {
        EFLIWizardActivityProcessor activityProcessor = null;
        String implementation;
        
        try{
            //Query CMD to get the name of the implementation for movement type and Program Pathway
            
                implementation = getImplementation (wizardActivity) ;
            
            
            if(String.isNotBlank(implementation))
            {
                Type myType=Type.forName(implementation);
                if (myType!= null)
                {
                    activityProcessor = (EFLIWizardActivityProcessor) myType.newInstance();
                }
            }
            
        }
        catch (Exception ex)
        {
            
        }
        return activityProcessor;
    }
    
  
    
    public static String getImplementation(EFLWizardActivity wizardActivity)
    {
        String implementation;
      /*  if(String.isNotBlank(MovementType) && String.isNotBlank(ProgramPathway))
        {
            EFLPSQActivityReference__mdt[] result = [ SELECT PSQActivity_Processor__c FROM EFLPSQActivityReference__mdt WHERE Movement_Type__c = :MovementType and Program_Pathway__c = :ProgramPathway and PSQActivity_Name__c = :PSQActivityName];
            if(result != null)
            {
                implementation = result[0].PSQActivity_Processor__c;
            }
        }        
*/        
        return implementation;
    }

}