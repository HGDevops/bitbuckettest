@isTest (SeeAllData=true)
private class CARPOL_VS_AphisAccount_test {

	private static testMethod void testAPHISAccount() {
	    
	    PageReference pageRef = Page.CARPOL_VS_ConfirmAPHISAccountPayment;
        Test.setCurrentPage(pageRef);
        CARPOL_VS_AphisAccount_extension controller = new CARPOL_VS_AphisAccount_extension(new ApexPages.StandardController(new Application__c()));
        
        
	    
	    Application__c a = new Application__c(APHIS_User_Fee_Account_Number__c = '3110156');
	    insert a;
	    system.debug('id ' + a.Id);

        

        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('total', '75.00');
        ApexPages.currentPage().getParameters().put('acctNum', a.APHIS_User_Fee_Account_Number__c);
        
        controller.accountstatus = 'Valid';
        controller.applicationID = a.Id;
        controller.totalAmount = '75.00';
        Test.startTest(); 
        PageReference cancel = controller.returnToPaymentPage();
        Test.setMock(HttpCalloutMock.class, new MockAphisAccountWebService());
        HttpResponse res = controller.callValdiateAccount();
        PageReference submit = controller.submitAphisAccount();
        controller.accountstatus = 'Invalid';
        PageReference submitInvalid = controller.submitAphisAccount();
        string status = controller.parseXML(res);
       // PageReference appProcessing = controller.processTrans();
        //PageReference cancel = controller.cancelPayment();
        Test.stopTest();

	}
	
	

}