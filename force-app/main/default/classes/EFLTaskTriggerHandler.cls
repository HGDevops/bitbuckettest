/**
 * Trigger Handler for the Task SObject. This class implements the EFLITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public with sharing class EFLTaskTriggerHandler 
                    implements EFLITrigger
{
    /*
     * Attributes
     */ 
    list<sObject> activityList = new list<sObject>();
    list<sObject> finalActivityList = new list<sObject>();
     list<authorizations__c> finalAuthorizationList = new list<authorizations__c>();
     list<Incident__c> finalIncidentList = new list<Incident__c>();
     list<Inspection__c> finalInspectionList = new list<Inspection__c>();
     //map<id,authorizations__c>  authTaskMap = new map<id,authorizations__c>();
    map<id,authorizations__c>  objAuthTaskMap = new map<id,authorizations__c>();
     map<id,Incident__c>  objIncdTaskMap = new map<id,Incident__c>();
     map<id,Inspection__c>  objInspTaskMap = new map<id,Inspection__c>();
    map<id,authorizations__c> sobjAuthMap  = new map<id,authorizations__c>();
     map<id,Incident__c> sobjIncdMap = new map<id,Incident__c>();
     map<id,Inspection__c> sobjInspMap = new map<id,Inspection__c>();
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
     public static Boolean TriggerDisabled = false;
 
    /*
    * Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
       if (EFLGenericUtility.isTriggerDisabled('EFLTaskTrigger'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    /*
     * Constructor
     */ 
    public EFLTaskTriggerHandler()
    {
    
     }
 
    /**
     * bulkBefore
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {   if(trigger.IsUpdate){
            set<id> taskids = new set<id>();
            set<id> taskAuthIds = new set<id>();
            set<id> taskIncdIds = new set<id>();
            set<id> taskInspIds = new set<id>();
        
            for(task t: (List<task>)trigger.new){
                //taskids.add(t.whatId);
                String sobjectType = (t.whatId).getSObjectType().getDescribe().getName();
                System.debug('SobjectType Retrieved>>>>' + sobjectType);
                if(sobjectType == 'authorizations__c'){
                   taskAuthIds.add(t.whatId); 
                }
                else if(sobjectType == 'Incident__c'){
                    taskIncdIds.add(t.whatId); 
                }
                else if(sobjectType == 'Inspection__c'){
                    taskInspIds.add(t.whatId); 
                }
            }
            
            System.debug('taskAuthIds' + taskAuthIds);
            System.debug('taskIncdIds' + taskIncdIds);
            System.debug('taskInspIds' + taskInspIds);
            //map<id,authorizations__c> auth = new map<id,authorizations__c>([select id,activity_sequence__c, stage__c, program__c,Workflow_Number__c from authorizations__c where id in:taskids]);
        if(taskAuthIds!=null && taskAuthIds.size()>0){     
         sobjAuthMap = new map<id,authorizations__c>([select id,activity_sequence__c, stage__c, program__c,Workflow_Number__c from authorizations__c where id in:taskAuthIds]);
        }
        if(taskIncdIds!=null && taskIncdIds.size()>0){     
         sobjIncdMap = new map<id,Incident__c>([select id,activity_sequence__c, stage__c,Workflow_Number__c,Related_Program__c from Incident__c where id in:taskIncdIds]);
        }
        if(taskInspIds!=null && taskInspIds.size()>0){     
         sobjInspMap = new map<id,Inspection__c>([select id,activity_sequence__c, stage__c,Workflow_Number__c,Program__c, Certified__c from Inspection__c where id in:taskInspIds]);
        }
            
        System.debug('sobjInspMap??>>>' + sobjInspMap);
           
            for(task t: (List<task>)trigger.new){
                if(sobjAuthMap!=null && sobjAuthMap.containskey(t.whatid)){
                    objAuthTaskMap.put(t.id, sobjAuthMap.get(t.whatid));
                }
                else if(sobjIncdMap!=null && sobjIncdMap.containskey(t.whatid)){
                    objIncdTaskMap.put(t.id, sobjIncdMap.get(t.whatid));
                }
                else if(sobjInspMap!=null && sobjInspMap.containskey(t.whatid)){
                    system.debug('Iterating in the Map??' + t.whatid + 'objInspTaskMap.get(t.whatid)'  +sobjInspMap.get(t.whatid));
                    objInspTaskMap.put(t.id, sobjInspMap.get(t.whatid));
                }
            }
            
            System.debug('AuthTaskMap' + objAuthTaskMap);
            System.debug('IncTaskMap' + objIncdTaskMap);
            System.debug('InspTaskMap' + objInspTaskMap);
        }
    }
 
    /**
     * bulkAfter
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter()
    {
        
 
        
    }
   
    /**
     * beforeInsert
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so)
    {
 
    }

    /**
     * beforeUpdate
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so)
    {
        authorizations__c auth ;
        Incident__c incd ;
        Inspection__c insp ;
        string stage, wfNumber;
        Task taskRecord = (Task)so;
        System.debug('TASK REC>>' +taskRecord );
        //Rajesh added
        /*string InspPrefix = Schema.SObjectType.Inspection__c.getKeyPrefix();
        Task oldtaskrecord =(Task)oldSo;
        If(oldtaskrecord.Status != taskRecord.Status && taskRecord.Status == 'Completed'&& TaskRecord.Subject == 'Review and Accept Inspection Report' &&((string) taskRecord.WhatId).startswith(InspPrefix) ){
        set <Id> inspeset = new set<Id>();
        inspeset.add(taskRecord.WhatId);
        
        
        List<Inspection__c> Inspect = new List<Inspection__c>();
        Inspect = [Select ID, Ready_For_next_Stage__c from Inspection__c where Id IN: inspeset AND Ready_For_next_Stage__c = true];
         if(Inspect.IsEmpty()){
         string taskCompletedMsg = EFLGenericUtility.getMessage('TaskShouldNotCompleted');
         taskRecord.addError(taskCompletedMsg ); 
         } 
        }*/
        //Rajesh end
        
        if(taskRecord.Status == 'Completed') {
           //Manual Tasks should not Trigger Activity Engine
           If(taskRecord.Activity_Sequence__c != null) {
               if(objAuthTaskMap.get(taskRecord.Id)!=null){
                   auth  = objAuthTaskMap.get(taskRecord.Id);
                   stage = auth.stage__c;
                   wfNumber = auth.Workflow_Number__c;
               }
               if(objIncdTaskMap.get(taskRecord.Id)!=null){
                   incd  = objIncdTaskMap.get(taskRecord.Id);
                   stage = incd.stage__c;
                   wfNumber = incd.Workflow_Number__c;
               }
               if(objInspTaskMap.get(taskRecord.Id)!=null){
                System.debug('Rec Found????');
                   insp  = objInspTaskMap.get(taskRecord.Id);
                        System.debug('Rec Found????' + insp);
                   stage = insp.stage__c;
                   wfNumber = insp.Workflow_Number__c;
               }
               System.debug('Stage>>>' +stage );
                 System.debug('wfNumber>>>' +wfNumber );
                   System.debug('taskRecord.Activity_Sequence__c.intValue()>>>' +taskRecord.Activity_Sequence__c.intValue() );
               
               Program_Activity__mdt activityCMD = EFLActivityUtility.getProgramActivity(stage, wfNumber,taskRecord.Activity_Sequence__c.intValue());
               system.debug('activityCMD '+activityCMD);
                if(EFLActivityUtility.isLastActivityForStage(activityCMD))
                        {
                            system.debug('Inside ');
                            if(auth!=null){
                            auth.Ready_for_next_stage__c = true;
                            }
                            if(incd!=null){
                            incd.Ready_for_next_stage__c = true;
                            }
                            if(insp!=null){
                            insp.Ready_for_next_stage__c = true;
                            }

                        }
                        else
                        {
                            //auth.Ready_for_next_stage__c = false;
                            if(auth!=null){
                            auth.Ready_for_next_stage__c = false;
                            }
                            if(incd!=null){
                            incd.Ready_for_next_stage__c = false;
                            }
                            if(insp!=null){
                            insp.Ready_for_next_stage__c = false;
                            }
                        }
               
           //Load Activities
               //if(authTaskMap.get(taskRecord.Id)!=null){
                   //authorizations__c auth  = authTaskMap.get(taskRecord.Id);
                            if(auth!=null){
                                system.debug('calling load activi auth' + auth);
                                EFLActivityFactory.loadActivities(auth, activityList);
                            }
                            if(incd!=null){
                                system.debug('calling load activi incd' + incd);
                                EFLActivityFactory.loadActivities(incd, activityList);
                            }if(insp!=null){
                                system.debug('calling load activi inspection' + insp);
                                EFLActivityFactory.loadActivities(insp, activityList);
                            }
                   System.debug('After Load activityList: ' + activityList); 
                   //System.debug('After Load auth: ' + auth);
                   if(!activityList.isEmpty())
                   {
                       finalActivityList.addAll(activityList);
                       if(auth!=null)finalAuthorizationList.add(auth);
                       if(incd!=null)finalIncidentList.add(incd);
                       if(insp!=null)finalInspectionList.add(insp);
                   }
                   else
                   {
                       if(auth!=null)finalAuthorizationList.add(auth);
                       if(incd!=null)finalIncidentList.add(incd);
                       if(insp!=null)finalInspectionList.add(insp);
                   }
           }
        }
    }
  
    /**
     * beforeDelete
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {         
            string taskDeletionMsg = EFLGenericUtility.getMessage('ActivityHistoryDelete');   
            so.addError(taskDeletionMsg);
    }
 
    /**
     * afterInsert
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */ 
    public void afterInsert(SObject so)
    {
    }

    /**
     * afterUpdate
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }

    /**
     * afterDelete
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */ 
    public void afterDelete(SObject so)
    {
    }
 
    /**
     * andFinally
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        
        if(finalActivityList!=null && !finalActivityList.isEmpty()){
            insert finalActivityList;
           /* if(finalAuthorizationList!=null){
                update finalAuthorizationList;
            }
            if(finalIncidentList!=null){
                update finalIncidentList;
            }
            if(finalInspectionList!=null){
                update finalInspectionList;
            }*/
        }
        if(!finalAuthorizationList.isempty())
        {
            update finalAuthorizationList;  
        }
         if(!finalIncidentList.isempty())
        {
            update finalIncidentList;  
        }
        if(!finalInspectionList.isempty())
        {
            update finalInspectionList;  
        }
        

    }
    
}