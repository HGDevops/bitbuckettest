@isTest
public class EFLTeamUtility_Test {
public static Authorizations__c auth;
    public static team__c team;
    static testMethod void test(){
    test.startTest();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='555';
        Insert objPrefix;
        Signature__c objTP=new  Signature__c();
        objTP.Name='Test AC TP Jialin';
        objTP.Recordtypeid=Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        
        objTP.Program_Prefix__c=objPrefix.Id;
        Insert objTP;
        Application__c app = testData.newapplication();
        auth = testData.newAuth(app.Id);
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
       
        Profile p = [SELECT Id FROM Profile WHERE Name='BRS Analyst'];
        
        System.RunAs(sysAdm)

         {     
            UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'BRS Program Specialist');
            insert ur;
    
            User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;
            User u2 = new User(Alias = 'stand', Email='standarduser1@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow1@testorg.com');
            insert u2;
            User u3 = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow2@testorg.com');
            insert u3;
         }
        User u4 = [SELECT Id FROM User WHERE LastName='Testing1'];
        list<team__c> team = new list<team__c>();
        team__c team1 = new team__c();
        team1.member_role__c='BRS Program Specialist';
        team1.Recordtypeid=Schema.SObjectType.Team__c.getRecordTypeInfosByName().get('BRS Team').getRecordTypeId();
        team1.Member__c = u4.id;
        team1.Authorization__c=auth.id;
        insert team1;
        
        team.add(team1);
                       
        EFLTeamUtility.loadSharing(team);
        test.stoptest();  
        
        }
    }