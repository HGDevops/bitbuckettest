@isTest(seealldata=false)


public class CustomLocationLookupControllerTest {
    public static Authorizations__c auth; 
    public static Set<Id> lineItemsIdSet;
    public static void initData(){
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
        
        Application__c app = new Application__c();
        app = testData.newapplication();
        auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
        
        Country__c country = new country__c();
        country = testDataBRS.newcountryus();
        Level_1_Region__c lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
        Level_2_Region__c lvl2Reg = new Level_2_Region__c();
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        AC__c lineItem = new AC__c();
        lineItem = testData.newLineItem('Resale/Adoption', app, auth);
        
        Id RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId(); 
        
        Location__c loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,lineItem.id,RtId);   
    }
    
    public static testmethod  void CustomLocationLookupControllerTest() {
        initData();   
        apexpages.currentpage().getparameters().put('reportType','field_test_report');
        System.currentPageReference().getParameters().put('txt','testTxt');
        apexpages.currentpage().getparameters().put('txt','field_test_report');
        apexpages.currentpage().getparameters().put('lksrch','test');        
            CustomLocationLookupController cont = new CustomLocationLookupController();
        cont.searchString = 'test';   
        cont.authId = auth.id;   
        cont.locObj = new Location__c();
        cont.results = new List<Location__c>();
        
        Test.startTest();
        List<Location__c> lstLoc = new List<Location__c>();
        PageReference pgRef  = cont.search();
        PageReference pgRef2 = cont.refreshPageSize();
        cont.getFormTag();
        
        
        cont.getTextBox();
        Test.stopTest();
        
    }
}