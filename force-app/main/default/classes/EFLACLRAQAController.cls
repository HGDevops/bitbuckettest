public with sharing class EFLACLRAQAController {
    //W-031557 to handle all server side queries/updates for EFLACLRAQAPage 
    //This class queries the first intro question & selections, then next question & selections, then previous question & selections 
    //Inserts all user selections and actions into Application request (tracking) object
    
    // NOTE: This org does not contain the utility classes for constants as found in BRS.
    // TO-DO: refactor constants to use utility class in future release after code is merged.
    public static string ACTION_START_BUTTON_CLICKED = Label.EFL_AC_LRS_Clicked_Start_button;
    public static string EXTERNAL_ID_APPENDIX = Label.EFL_AC_LRS_External_Id_Appendix;
    public static string RECORD_TYPE_WIZARD_QUESTION = Label.EFL_AC_LRA_AC_LRA_Question;
    public static string STATUS_ACTIVE = Label.EFL_AC_LRA_Active;
    public static string STATUS_PROCESSED = Label.EFL_AC_LRA_Processed;
    public static string STATUS_SELECTED = Label.EFL_AC_LRA_Selected;
    public static string RECORD_TYPE_ACLRA_APP_REQUEST = Label.EFL_AC_LRA_AC_LRA_Response;
    
    @AuraEnabled
    //When application is initialized, add a record in tracking object that user clicked start button
    public static InitResponse startActionTracking(){//Decimal customerNumber
        /* SPRINT 4 : Add a summary record */
        EFL_AC_LRA_Summary__c summaryRecord = new EFL_AC_LRA_Summary__c();
        //summaryRecord.Customer_Number__c = customerNumber;
        insert summaryRecord;
        
        EFL_AC_LRA_Summary__c summaryResult = [SELECT Id, Name FROM EFL_AC_LRA_Summary__c WHERE ID = :summaryRecord.Id];
        
        long rightNow = DateTime.now().getTime();
        string nowString = string.valueOf(rightNow);
        nowString = nowString.right(6);
        
        integer i = integer.valueOf(summaryResult.Name);
        Application_Request__c clickStartRec = 
            new Application_Request__c (
                Customer_Number__c = i,
                User_Action__c = ACTION_START_BUTTON_CLICKED,
                External_Migration_Id__c = i + EXTERNAL_ID_APPENDIX,
                EFL_AC_LRA_Summary__c = summaryRecord.Id,
                Time_Entered__c = integer.valueOf(nowString)
            );
        insert clickStartRec;
		
        InitResponse init = new InitResponse();
        init.summaryRecordId = summaryResult.Id;
        init.customerNumber = i;
        return init;
    }
    
    public class InitResponse{
        @AuraEnabled public Id summaryRecordId{get;set;}
        @AuraEnabled public integer customerNumber{get;set;}
    }
    
    @AuraEnabled
    //Handle All questiosn: get all question and it's selections of type AC_LRA_Question
    public static List<Wizard_Questionnaire__c> fetchAllQuestionSelections(){
        List <Wizard_Questionnaire__c> qstnSelections = [SELECT Id,Wizard_Question_Rich_Text__c,AC_LRA_Pathway__c,Type__c,Type_of_Options_LRA__c,Question_Number__c,External_Migration_Id__c, 
                                                         (SELECT id,Name,Value__c, Intro_Question_Result__c,EFLACLRAValue__c, Display_Order__c,
                                                          Wizard_Next_Question__c,Wizard_Questionnaire__c,Pathway_Questions_Result__c, Pathway_Questions_Status__c,
                                                          Wizard_Questionnaire__r.AC_LRA_Pathway__c 
                                                          FROM Wizard_Questionnaire__r 
                                                          WHERE Status__c = :STATUS_ACTIVE
                                                          ORDER BY Display_Order__c) 
                                                         FROM Wizard_Questionnaire__c 
                                                         WHERE RecordType.DeveloperName = :RECORD_TYPE_WIZARD_QUESTION
                                                         AND Status__c = :STATUS_ACTIVE];
        return qstnSelections;
    }
    
    @AuraEnabled
    public static list<EFL_AC_LRA_Conditional_Override__mdt> getConditionalOverrides(){
        return [SELECT Id, Next_Step__c, Selected_Answer__c, Next_Wizard_Question__c, Qualified_For__c, Overrides_Pathway__c FROM EFL_AC_LRA_Conditional_Override__mdt];
    }
    
    @AuraEnabled
    //get the record type for application request (tracking) record and send it to the CMP to use whenever a record is inserted in this obj 
    public static string fetchTrackingRT(){
        Id rtID = Schema.SObjectType.Application_Request__c.getRecordTypeInfosByName().get(EFLACLRAQAController.RECORD_TYPE_ACLRA_APP_REQUEST).getRecordTypeId();
        return rtID;
    }
    
    @AuraEnabled
    public static void upsertTrackingrec(list<Application_Request__c> trcRec){
        upsert trcRec;
    }
    
    @AuraEnabled
    public static void upsertTrackingSinglerec(string trcRecExtId, string pthwyTrcExtId){
        //Update questions
        list<Application_request__c> trcRecDel = [select id, Deleted__c from Application_request__c where External_Migration_Id__c =:trcRecExtId];
        if(!trcRecDel.isEmpty()) {
            trcRecDel[0].Deleted__c = true;
            update trcRecDel[0];
        }
        //Update pathways
        list<Application_request__c> trcRecPath = [select id, Deleted__c, pathway_status__c, Intro_Question_Status__c from Application_request__c 
                                             where External_Migration_Id__c =:pthwyTrcExtId];
        if(!trcRecPath.isEmpty()){
            if(trcRecPath[0].Intro_Question_Status__c == STATUS_PROCESSED){
                trcRecPath[0].Intro_Question_Status__c = STATUS_SELECTED;
            }
            trcRecPath[0].pathway_status__c= '';
            update trcRecPath[0];
        }
    }
    
    @auraEnabled
    public static void updateIQ1( decimal customerNumber){
        list<Application_request__c> trcIQ1List  = [select id, deleted__c from application_request__c where customer_Number__c = :customerNumber and wizard_question__r.Question_Number__c ='IQ1'];
        list<Application_request__c> updtrcIQ1List = new list<Application_request__c>();
        for (Application_request__c ar: trcIQ1List){
            ar.deleted__c = true;
            updtrcIQ1List.add(ar);            
        }
        update updtrcIQ1List;
    }

    @AuraEnabled
    public static list<String> fetchPathwayCMD(){
        list<EFL_AC_LRA_Pathways__mdt> cmdlist= new list<EFL_AC_LRA_Pathways__mdt>();
        cmdlist = [select pathway__c,sequence__c from EFL_AC_LRA_Pathways__mdt order by sequence__c ASC];
        list<String> pathwayList = new list<String>();
        for(EFL_AC_LRA_Pathways__mdt each :cmdlist){
            pathwayList.add(each.pathway__c);
        }
        return pathwayList;
    }
    
    /* SPRINT 4: This method called when the Contact Us link in the header is clicked */
    @AuraEnabled
    public static void logWelcomeActivity(Id summaryRecordId, string activityType){
        EFL_AC_LRA_Summary__c summary = new EFL_AC_LRA_Summary__c();
        summary.Id = summaryRecordId;
        summary.Clicked_Contact_Us_Header_Link__c = true;
        update summary;
    }
    
    /* SPRINT 4: This method used from the results page to track onclick activities */
    @AuraEnabled
    public static void logResultActivity(decimal customerNumber, id summaryRecordId, string actionType, string pathway){
        if(actionType == Label.EFLACLRA_Application_Kit_Clicked){
            // insert an AR record
            Application_Request__c ar = new Application_Request__c();
            ar.Customer_Number__c = customerNumber;
            ar.User_Action__c = actionType;
            ar.Pathway__c = pathway;
            ar.EFL_AC_LRA_Summary__c = summaryRecordId;
            insert ar;
        }else if(actionType == Label.EFL_AC_LRA_Clicked_Contact_Us_Link){
            // update the summary record.
            EFL_AC_LRA_Summary__c summary = new EFL_AC_LRA_Summary__c();
            summary.Id = summaryRecordId;
            summary.Clicked_Contact_Us_Results_Page__c = true;
            update summary;
        }
    }
    
    @AuraEnabled
    public static void updateSummaryLastQuestion(Id summaryRecordId, Id lastQuestionId){
        EFL_AC_LRA_Summary__c summary = new EFL_AC_LRA_Summary__c();
        summary.Id = summaryRecordId;
        summary.Last_Question_Visited__c = lastQuestionId;
        update summary;
    }
        
    @AuraEnabled
    public static void setEndPointOnSummary(Id summaryRecordId, string lastLocation){
        EFL_AC_LRA_Summary__c summary = new EFL_AC_LRA_Summary__c();
        summary.Id = summaryRecordId;
        summary.Exit_Page__c = lastLocation;
        update summary;
    }
    
    //update summary record w/time and pathways and/or qualified to complete the application
    @auraEnabled
    public static void finalizeSummary(decimal timeInMins, Id summaryRecordId, string pathways){
        list<PathwayResult> pathList = new list<PathwayResult>();
        if(pathways != null && pathways != ''){
        	pathList = (PathwayResult[])JSON.deserialize(pathways,list<PathwayResult>.class);
        }
        
        saveCompletedSummary(summaryRecordId, timeInMins, pathList);

        // update the AR's so that they aggregate start & End times for each pathway.
        list<Application_Request__c> ars = [SELECT Id, Time_Started__c, Time_Entered__c, Pathway__c, Wizard_Question__r.Type__c, Pathway_Status__c,
                                            Customer_Number__c, EFL_AC_LRA_Summary__c, Wizard_Selection__r.Intro_Question_Result__c, Intro_Question_Status__c
                                            FROM Application_Request__c 
                                            WHERE EFL_AC_LRA_Summary__c = :summaryRecordId
                                           AND Deleted__c = FALSE
                                           AND ( Wizard_Question__r.Type__c = 'START' OR Pathway_Status__c != null OR Intro_Question_Status__c = :Label.EFL_AC_LRA_Processed)];
        map<string,Application_Request__c> startRecords = new map<string,Application_Request__c>();
        map<string,Application_Request__c> endRecords = new map<string,Application_Request__c>();
        map<string,Application_Request__c> voidedRecords = new map<string,Application_Request__c>();
        map<string,Application_Request__c> iqRecords = new map<string,Application_Request__c>();
        list<Application_Request__c> toUpdate = new list<Application_Request__c>();
        set<Id> toUpdateIds = new set<Id>();
        list<Application_Request__c> toInsert = new list<Application_Request__c>();
        for(Application_Request__c ar : ars){
            if(ar.Intro_Question_Status__c == Label.EFL_AC_LRA_Processed){
                iqRecords.put(ar.Wizard_Selection__r.Intro_Question_Result__c,ar); // map of intro questions will get "time to complete"
            }else if(ar.Wizard_Question__r.Type__c == 'START'){
                startRecords.put(ar.Pathway__c,ar); // map of Start records used for time comparison
            }else if(ar.Pathway_Status__c != Label.EFLACLRA_Voided){
                endRecords.put(ar.Pathway__c,ar); // map of end records used to calculate pathway time & create negative ars
            }else if(ar.Pathway_Status__c == Label.EFLACLRA_Voided){
                voidedRecords.put(ar.Pathway__c, ar); // map of voided records (C class overriden by A or B)
            }
        }
        
        for(string s : endRecords.keySet()){
            string r = endRecords.get(s).Wizard_Selection__r.Intro_Question_Result__c;
            if(startRecords.containsKey(r) && iqRecords.containsKey(r)){
                decimal startTime = startRecords.get(r).Time_Started__c;
                //system.debug('start time: ' + startTime);
                decimal endTime = endRecords.get(s).Time_Entered__c;
                //system.debug('endTime: ' + endTime);
                if(startTime != null && endTime != null){
                    decimal pathTime = ((endTime - startTime) / 1000) / 60;
                    iqRecords.get(r).EFL_Time_to_Complete__c = pathTime.setScale(3);
                    toUpdate.add(iqRecords.get(r));
                    toUpdateIds.add(iqRecords.get(r).Id);
                }
            }
        }
        
        // go through each of the end records and create negative outcomes if needed.
        list<Application_Request__c> negativeARs = new list<Application_Request__c>();
        for(string s: endRecords.keySet()){
            Application_Request__c clonedAR;
            Application_Request__c thisAR = endRecords.get(s);
            if(thisAR.Pathway_Status__c == Label.EFLACLRA_Qualified){
                if(thisAR.Pathway__c != thisAR.Wizard_Selection__r.Intro_Question_Result__c && thisAR.Wizard_Selection__r.Intro_Question_Result__c != null){
                    // the pathway qualified for does not match the question pathway
                    // we need to create negative AR's for those not qualified for.
                    string comboString = thisAR.Pathway__c;
                    list<string> pathWayList = thisAR.Wizard_Selection__r.Intro_Question_Result__c.split(',');
                    
                    for(string str : pathWayList){
                        string thisPath = str.trim();
                    	
                        if(thisAR.Pathway__c != thisPath){
                            clonedAR = thisAR.clone(false,false,false,false);
                            clonedAR.Pathway__c = thisPath;
                    		clonedAR.Pathway_Status__c = Label.EFLACLRA_Not_Qualified;
                			toInsert.add(clonedAR);
                        }
                    }
                }
                if((thisAR.Pathway__c == Label.EFLACLRA_Class_A_License || thisAR.Pathway__c == Label.EFLACLRA_Class_B_License)
                  	&& !voidedRecords.isEmpty()
                  ){
                      // check if a voided C record exists & set it to Not Qualified;
                      if(voidedRecords.containsKey(Label.EFL_AC_LRA_Class_C_License)){
                          Application_Request__c toChange = voidedRecords.get(Label.EFL_AC_LRA_Class_C_License);
                          toChange.Pathway_Status__c = Label.EFLACLRA_Not_Qualified;
                          toUpdate.add(toChange);
                          
                          // Add C's time to complete here.
                          if(startRecords.containsKey(Label.EFL_AC_LRA_Class_C_License) && iqRecords.containsKey(Label.EFL_AC_LRA_Class_C_License)
                            && !toUpdateIds.contains(iqRecords.get(Label.EFL_AC_LRA_Class_C_License).Id)){
                              decimal startTime = startRecords.get(Label.EFL_AC_LRA_Class_C_License).Time_Started__c;
                              decimal endTime = toChange.Time_Entered__c;
                              if(startTime != null && endTime != null){
                                  decimal pathTime = ((endTime - startTime) / 1000) / 60;
                                  iqRecords.get(Label.EFL_AC_LRA_Class_C_License).EFL_Time_to_Complete__c = pathTime.setScale(3);
                                  toUpdate.add(iqRecords.get(Label.EFL_AC_LRA_Class_C_License));
                              }
                          }
                      }
                  }
            }else if(thisAR.Pathway_Status__c == Label.EFLACLRA_Not_Qualified && thisAR.Pathway__c.contains(',')){
                // this is a combo that needs to be broken out into 2 records.
                // this only happens when Status== "not qualified" since you can only qualify for single classes in an AR record.
                string comboString = thisAR.Pathway__c;
                list<string> pathWayList = thisAR.Pathway__c.split(',');
                
                // convert original AR to single outcome.
                thisAR.Pathway__c = pathWayList[0].trim();
                toUpdate.add(thisAR);
                clonedAR = thisAR.clone(false,false,false,false);
                clonedAR.Pathway__c = pathWayList[1].trim();
                clonedAR.Pathway_Status__c = Label.EFLACLRA_Not_Qualified;
                toInsert.add(clonedAR);
            }
        }
        
        if(!toUpdate.isEmpty()){
            Database.update(toUpdate, false);
        }
        
        if(!toInsert.isEmpty()){
            Database.insert(toInsert, false);
        }
    }
    
    
    public static void saveCompletedSummary(Id summaryRecordId, decimal timeInMins, list<PathwayResult> pathList){
        EFL_AC_LRA_Summary__c summary = new EFL_AC_LRA_Summary__c();
        summary.Id = summaryRecordId;
        summary.EFLAC_Time_to_Complete__c = timeInMins;
        summary.Recommended_Pathways__c = getStringFromPathways(pathList);
        summary.Completed_Application__c = TRUE;
        summary.Exit_Page__c = Label.EFLACLRA_Results_Page;
        summary.Last_Question_Visited__c = null;
        summary.Outcome_Type__c = getOutcomeType(pathList);
        update summary;
    }
    
    @TestVisible
    private static string getStringFromPathways(list<PathwayResult> pathways){
        pathways.sort();
        system.debug('pathways after sort: ' + pathways);
        list<string> pathList = new list<string>();
        for(PathwayResult p: pathways){
            if(p.pathway == Label.EFLACLRA_Class_R_Registration && p.status == Label.EFL_AC_LRA_Contact_AC){
                return Label.EFLACLRA_Class_R_Federal_Agency;
            }
            pathList.add(pathwayConversionmap.get(p.pathway));
        }
        string pathString = string.join(pathList, ' + ');
        if(pathList.size() == 1){
            pathString = Label.EFLACLRA_Class + ' ' + pathString + ' ' + Label.EFLACLRA_Needed;
        }if(pathList.size() > 1){
            pathString = Label.EFLACLRA_Classes + ' ' + pathString;
        }
        if(pathString == null || pathString == ''){
            pathString = Label.EFLACLRA_None_Needed;
        }
        return pathString;
    }
    
    public class PathwayResult implements comparable{
        @AuraEnabled public string status{get;set;}
        @AuraEnabled public string pathway{get;set;}
        public integer compareTo(Object compareTo) {
            PathwayResult compareToObj = (PathwayResult)compareTo;
            if (pathway == compareToObj.pathway) return 0;
            if (pathway > compareToObj.pathway) return 1;
            return -1;        
        }
    }
    
    @TestVisible
    private static string getOutcomeType(list<PathwayResult> pathways){
        integer num = 0;
        for(PathwayResult p: pathways){
            if(p.pathway == Label.EFLACLRA_Class_R_Registration && p.status == Label.EFL_AC_LRA_Contact_AC){
                return Label.EFLACLAR_Single;
            }
            num += 1;
        }
        if(num == 1){
            return Label.EFLACLAR_Single;
        }else if(num > 1){
            return Label.EFLACLRA_Combination;
        }
        return null;
    }
    
    // fetches page date for results page load
    @AuraEnabled
    public static ResultPageData getResultsPageData(){
        ResultPageData pd = new ResultPageData();
        return pd;
    }
    
    // class used to represent page data used by the results page.
    public class ResultPageData{
        @AuraEnabled public list<EFL_AC_LRA_All_Pathway__mdt> pathwayMDT{get;set;}
        public ResultPageData(){
            this.pathwayMDT = [SELECT Id, Pathway_Name__c, Pathway_Link__c, Output_Label__c, Hyperlink_Text__c, Pathway_Type__c, Qualified_Icon_Name__c,
                               Icon_Class__c, Icon_Text__c, Display_Order__c, Status__c, Action_Type__c
                               FROM EFL_AC_LRA_All_Pathway__mdt
                               ORDER BY Display_Order__c ASC
                              ];
        }
    }
    
    public static map<string,string> pathwayConversionmap = new map<string,string>{
        Label.EFLACLRA_Class_A_License => Label.EFLACLRA_A,
        Label.EFLACLRA_Class_B_License => Label.EFLACLRA_B,
        Label.EFL_AC_LRA_Class_C_License => Label.EFLACLRA_C,
        Label.EFLACLRA_Class_H_Registration => Label.EFLACLRA_H,
        Label.EFLACLRA_Class_R_Registration => Label.EFLACLRA_R,
        Label.EFLACLRA_Class_T_Registration => Label.EFLACLRA_T
    };
}