@isTest
public class EFLAnimalUsageController_Test{
    
    private static final string AccountName = 'Test Account';
    private static final string ContactLastName = 'Test';
    private static final string Sites = 'Site 1';
    private static final String[] StdAnimalNames = new String[] {'Dogs','Cats','Guinea Pigs','Hamsters','Rabbits','Non-Human Primates','Sheep','Pigs','Other Animals'};
    private static final Integer StdAnimalStartingSeqNum = 4;
    
    @TestSetup
    static void makeData(){
        // Add standard animals
        List<EFLAnimal__c> standardAnimals = new List<EFLAnimal__c>();
        for(String name :StdAnimalNames) {
        	EFLAnimal__c animal = new EFLAnimal__c();
            animal.Name = name;
            animal.Other_Animal__c = false;
            standardAnimals.add(animal);
        }
        insert standardAnimals;
        
        // Insert custom setting values
        String value = '';
        for(EFLAnimal__c animal :standardAnimals) {
            value = value + animal.Id + ',';
        }
        insert new EFLACLRA_Application_Settings__c(Name = 'ARStandardAnimalIds', Value__c = value.left(value.length()-1));
        insert new EFLACLRA_Application_Settings__c(Name = 'ARRegulatedAnimalsStartingSeqNum', Value__c = String.valueOf(StdAnimalStartingSeqNum));

        // Make a parent object for Uploaded_File__c to be created under in test execution.
        Account account = new Account();
        account.Name = AccountName;
        insert account;
        
        List<RecordType> rt = [Select Id From RecordType Where SObjectType = 'Contact' and DeveloperName='AC_R_L_Contact' Limit 1];
        Contact contact = new Contact();
        contact.LastName = ContactLastName;
        contact.AccountId = account.Id;
        contact.RecordTypeId = rt.size() > 0 ? rt[0].Id : null;
        contact.email = 'vijay.vellaturi@accenturefederal.com';
        insert contact;
        
        //create a facility admin
        Contact contact2 = new Contact();
        contact2.LastName = ContactLastName;
        contact2.AccountId = account.Id;
        contact2.EFL_Facility_Admin__c = true;
        contact2.RecordTypeId = rt.size() > 0 ? rt[0].Id : null;
        contact2.email = 'vijay.vellaturi@accenturefederal.com';
        insert contact2;
        
        EFLRegistration__c registration = new EFLRegistration__c();
        registration.EFLAccount__c = account.Id;
        insert registration;
        
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = account.Id;
        annualReport.EFLContact__c = contact.Id;
        annualReport.EFLRegistration__c = registration.Id;
        annualReport.EFLSites__c = Sites;
        insert annualReport;
        
        list<EFLRegistered_Animal__c> anmls = [ SELECT Id FROM EFLRegistered_Animal__c
                                              WHERE EFLAnnual_Report__c = :annualReport.Id];
        system.assertEquals(StdAnimalNames.size(), anmls.size());
    }
    
    @isTest
    static void testgetPageData(){
        EFLAnnual_Report__c ar = [SELECT Id, EFLAccount__c FROM EFLAnnual_Report__c LIMIT 1];
        test.startTest();
        EFLAnimalUsageController.PageData pd = EFLAnimalUsageController.getPageData(ar.Id);
        test.stopTest();
        system.assertEquals(ar.Id, pd.annualReportId);
        system.assertEquals(true, pd.isInternalUser);
        system.assertEquals(StdAnimalNames.size()-1, pd.animalList.size());
        system.assertEquals(false, pd.isCommunity);
        system.assertNotEquals(null, pd.otherAnimalSummary);
        system.assertNotEquals(null, pd.otherAnimalList);
    }
    
    @isTest
    static void testAddOtherAnimal(){
        EFlAnimal__c a = new EFLAnimal__c();
        a.Name = 'Test';
        insert a;
        
        EFLAnnual_Report__c ar = [SELECT Id, EFLAccount__c FROM EFLAnnual_Report__c LIMIT 1];
        
        test.startTest();
        EFLAnimalUsageController.AnimalDetail ad = EFLAnimalUsageController.addOtherAnimal(ar.Id, a.Id, a.Name);
        test.stopTest();
        system.assertEquals(a.Name, ad.name);
        system.assertEquals(a.Id, ad.record.EFLAnimalName__c);
        
        EFLAnimalUsageController.PageData pd = EFLAnimalUsageController.getPageData(ar.Id);
        system.assertEquals(1, pd.otherAnimalList.size());
    }
    
    @isTest
    static void testUpsertAnimals(){
        EFlAnimal__c a = new EFLAnimal__c();
        a.Name = 'Test';
        insert a;
        
        EFLAnnual_Report__c ar = [SELECT Id, EFLAccount__c FROM EFLAnnual_Report__c LIMIT 1];
        
        test.startTest();
        EFLAnimalUsageController.AnimalDetail ad = EFLAnimalUsageController.addOtherAnimal(ar.Id, a.Id, a.Name);
        list<EFLRegistered_Animal__c> ans = new list<EFLRegistered_Animal__c>();
        ans.add(ad.record);
        EFLAnimalUsageController.upsertAnimals(ans);
        test.stopTest();
        system.assertEquals(a.Name, ad.name);
        system.assertEquals(a.Id, ad.record.EFLAnimalName__c);
    }
    
    @isTest
    static void testDeleteThisAnimal(){
        EFlAnimal__c a = new EFLAnimal__c();
        a.Name = 'Test';
        insert a;
        
        EFLAnnual_Report__c ar = [SELECT Id, EFLAccount__c FROM EFLAnnual_Report__c LIMIT 1];
        
        EFLAnimalUsageController.AnimalDetail ad = EFLAnimalUsageController.addOtherAnimal(ar.Id, a.Id, a.Name);
        list<EFLRegistered_Animal__c> ans = new list<EFLRegistered_Animal__c>();
        ans.add(ad.record);
        EFLAnimalUsageController.upsertAnimals(ans);
        
        test.startTest();
        EFLAnimalUsageController.deleteThisAnimal(ad.record);
        test.stopTest();
        
        EFLAnimalUsageController.PageData pd = EFLAnimalUsageController.getPageData(ar.Id);
        system.assertEquals(0, pd.otherAnimalList.size());
    }
}