public with sharing class EFLTeamUtility {
    
    public static void loadSharing(list<Team__c> members){
        
        List<Authorizations__Share> teamShares = new List<Authorizations__Share>();
        
        Authorizations__Share mbrShr;
        
        for(Team__c member : members){
        mbrShr = new Authorizations__Share();
        mbrShr.ParentId = member.Authorization__c;
        mbrShr.UserOrGroupId = member.Member__c;
        mbrShr.AccessLevel = 'edit';
        mbrShr.RowCause = Schema.Authorizations__Share.RowCause.Authorization_Workflow_Team__c;
        teamShares.add(mbrShr);
    }
    
    Database.SaveResult[] teamShareInsertResult = Database.insert(teamShares,false);
      
    }

}