@isTest
public class EFLInspectionTriggerHandlerTest {
    public static Trade_Agreement__c ta;
    public static Country__c Country;
    public static Country__c USCountry;
    public static Level_1_Region__c level1;
    public static Level_2_Region__c level2;
    public static Authorizations__c auth; 
    Public static location__c loc;
    public static ac__c li2;
    public static Id olocrectypeid;
    public static Id desRecId;
    public static Id locrectypeid;
    static testMethod void testBeforeUpdate(){
        locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        desRecId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        EFLInspectionTriggerHandler InspectionTriggerHandler =new EFLInspectionTriggerHandler();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        ta = testData.newta();
        Country = testData.newcountrywithassoc();
        USCountry = testData.newcountryus();
        level1 = testData.newlevel1region(Country.id);
        level2 = testData.newlevel2region(level1.id);
        
        list<sObject> activityList = new list<sObject>();
        
        Domain__c objDom = new Domain__c();
        objDom.Name = 'BRS';
        insert objDom;
        
        Program_Prefix__c  objPre = new Program_Prefix__c();
        objPre.Name= 'BRS Inspection';
        objPre.Inspection_Related__c = true;
        objPre.Program__c = objDom.id;
        insert objPre;
        
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Application__c objapp = testData.newapplication();
        AC__c li = TestData.newlineitem('Personal Use', objapp);
        loc = testData.newlocation(Country.id,level1.id,level2.id,li.id,olocrectypeid);
        Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        Authorizations__c objauth = testData.newAuth(objapp.id);
        li.Authorization__c = objauth.id;
        update li;
        
        update objauth;
        Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
        
        EFL_Inspection_Questions_Template__c inspTmp = new EFL_Inspection_Questions_Template__c();
        inspTmp.Name='test';
        insert inspTmp;
        
        Inspection__c Ins = new Inspection__c();
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Ins.RecordTypeId = IncRecTypeID;
        Ins.Stage__c = 'Inspection Assignment';
        Ins.status__c='open';
        Ins.Program__c='BRS';
        Ins.Stage__c = 'Inspection Report';
        Ins.Activity_Sequence__c = 0;
        //Ins.certified__c = True;
        Ins.Reason_for_Cancellation__c = 'testing';
        Ins.EFL_Inspection_Questionnaire_Template__c=inspTmp.id;
        
        List<sObject> questionList= new List<sObject>();
        EFLBRSInspectionEngine  engine = new EFLBRSInspectionEngine ();
        engine.loadQuestion(Ins,questionList);
        
        EFL_Inspection_Questions_Template__c inspTmpn = new EFL_Inspection_Questions_Template__c();
        inspTmpn.Name='test';
        insert inspTmpn;
        
        User u = new User(Alias = 'standt', Email='EFLInspectionTriggerHandlerTest@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = [Select Id FROM Profile WHERE Name='System Administrator'].Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='EFLInspectionTriggerHandlerTest@testorg.com');
        insert u;
        Inspection__c Insn = new Inspection__c();
        String IncRecTypeIDn = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Insn.RecordTypeId = IncRecTypeIDn;
        Insn.Stage__c = 'Inspection Assignment';
        Insn.status__c='Cancelled';
        Insn.Reason_for_Cancellation__c = 'testing';
        Insn.Program__c='BRS';
        Insn.Activity_Sequence__c = 0;
        //Insn.certified__c = True;
        Insn.Inspection_Due_Date__c =Date.today();
        Insn.Location__c = loc.Id;
        Insn.EFL_Inspector__c = u.Id;
        Insn.EFL_Inspection_Questionnaire_Template__c=inspTmpn.id;
        insert Insn;
        //system.debug('val'+Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId());
        insert Ins;
        Inspection__c Insn1 = new Inspection__c();
        Insn1.RecordTypeId = IncRecTypeIDn;
        Insn1.Stage__c = 'Inspection Report';
        Insn1.status__c='Cancelled';
        Insn1.Reason_for_Cancellation__c = 'testing';
        Insn1.Program__c='BRS';
        Insn1.Activity_Sequence__c = 0;
        Insn1.certified__c = True;
        Insn1.Inspection_Due_Date__c =Date.today();
        Insn1.Location__c = loc.Id;
        Insn1.EFL_Inspector__c = u.Id;
        Insn1.EFL_Inspection_Questionnaire_Template__c=inspTmpn.id;
        insert Insn1;
        
        EFL_Inspection_Questionnaire_Questions__c recEIQ=new EFL_Inspection_Questionnaire_Questions__c ();
        recEIQ.Question__c='test question1';
        recEIQ.Response__c = 'response1';
        recEIQ.Answer__c='answer';
        recEIQ.Comments__c ='ytest' ;
        recEIQ.Inspection__c=Ins.id;
        recEIQ.Options__c='Finalize Questionnaire';
        insert recEIQ;
        
        
        //Create Facility
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Facility__c fac = new Facility__c();
        fac.RecordTypeId = PortsFacRecordTypeId;
        fac.Name = 'entryport';
        fac.Type__c = 'Laboratory';  
        insert fac;
        
        //Incident__c
        Incident__c incident = new Incident__c();
        incident.Requester_Name__c='testReqName';
        incident.EFL_Template__c=objcm.id;
        incident.Facility__c =fac.id;
        incident.Authorization__c= objauth.id;
        incident.EFL_Issued_Date__c = Date.today();
        incident.Related_Program__c = 'AC';
        incident.Incident_Date__c = Date.today();
        incident.Incident_Discovery_Date__c = Date.today();
        incident.Incident_Reported_Date__c = Date.today();   
        insert incident;
        // EFL_INS_Template_Question_Junction__c jnObj= new EFL_INS_Template_Question_Junction__c();
        // jnObj.EFL_Inspection_Questions_Template__c=inspTmpn.id;
        // jnObj.Inspection_Template_Questions__c=inspTmpn.id;
        // insert jnObj;
        InspectionTriggerHandler.afterDelete(Ins);
        InspectionTriggerHandler.beforeDelete(Ins);
        InspectionTriggerHandler.beforeInsert(Ins);
        InspectionTriggerHandler.afterInsert(Insn);
        InspectionTriggerHandler.beforeUpdate(Ins,Ins);
        
        /*   User u = new User(Alias = 'standt', Email='EFLInspectionTriggerHandlerTest@testorg.com', 
EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
LocaleSidKey='en_US', ProfileId = [Select Id FROM Profile WHERE Name='System Administrator'].Id, 
TimeZoneSidKey='America/Los_Angeles', UserName='EFLInspectionTriggerHandlerTest@testorg.com');
insert u;*/
        Profile p = [SELECT Id FROM Profile WHERE Name='eFile APHIS Staff'];
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        system.runas(sysAdm)
        {
            UserRole ur = new UserRole(DeveloperName = 'MyBRSPS', Name = 'BRS Program Specialist');
            Insert ur;
            UserRole ur1 = new UserRole(DeveloperName = 'MyBRSPS1', Name = 'BRS Biotechnologist');
            Insert ur1;
            UserRole ur2 = new UserRole(DeveloperName = 'MyBRSPS2', Name = 'ROP Reviewer');
            Insert ur2;
            User u1 = new User(Alias = 'standt', Email='EFLInspectionTriggerHandlerTest12@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testin12', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;
            User u2 = new User(Alias = 'stand', Email='EFLInspectionTriggerHandlerTest2@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur1.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow1@testorg.com');
            insert u2;
            User u5 = new User(Alias = 'stand', Email='EFLInspectionTriggerHandlerTest3@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing3', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur2.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow2@testorg.com');
            insert u5;
        }
		User u3 = [SELECT Id FROM User WHERE LastName='Testin12'];      
		User u4 = [SELECT Id FROM User WHERE LastName='Testing2'];
        User u6 = [SELECT Id FROM User WHERE LastName='Testing3'];
        String BRSTeamRecordTypeId = Schema.SObjectType.team__c.getRecordTypeInfosByDeveloperName().get('BRS_Team').getRecordTypeId();
        team__c team = new team__c();
        team.RecordTypeId = BRSTeamRecordTypeId;
        team.member_role__c='BRS Program Specialist';
        team.Member__c = u3.id;
        team.Authorization__c=objauth.id;
        team.Inspection__c=Ins.id;
        insert team;
        team__c team1 = new team__c();
        team1.RecordTypeId = BRSTeamRecordTypeId;
        team1.member_role__c='BRS Biotechnologist';
        team1.member__c = u4.id;
        insert team1;
		team__c team2 = new team__c();
        team2.RecordTypeId = BRSTeamRecordTypeId;
        team2.member_role__c='ROP Reviewer (Regulatory Biotechnologist 2)';
        team2.Member__c = u6.id;
        team2.Authorization__c=objauth.id;
        team2.Inspection__c=Insn1.id;
        insert team2;
      
        Test.startTest();
        try{
            Ins.Stage__c = 'Inspection Assignment';
            Ins.Inspection_Due_Date__c =Date.today();
            Ins.Location__c = loc.Id;
            Ins.EFL_Responsible_SPRO__c = objcont.Id;
            //Ins.EFL_Inspector__c = u3.Id;
            Ins.status__c='Cancelled';
            Ins.Inspector__c=objcont.Id;
            Ins.Facility__c=fac.id;
            Ins.Incident__c=incident.id;
            update Ins;
            InspectionTriggerHandler.afterupdate(Ins,Ins);
        	Insn1.Reason_for_Cancellation__c = 'testing1';
            update Insn1;
            Team__c t = [select id,Inspection__c,member_role__c from Team__c where Inspection__c = :Insn1.Id ];
            InspectionTriggerHandler.afterupdate(Insn1,Insn1);
        }catch(exception e){}
        Test.stopTest();        
        
        
        InspectionTriggerHandler.andFinally();
        
    }
    
    static testMethod void testLocationChange(){
        Id locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        CARPOL_BRS_TestDataManager tdm = new CARPOL_BRS_TestDataManager();
        tdm.insertcustomsettingsWithBRSTriggerDisabled();
        AC__c li = tdm.newlineitem('Personal Use', tdm.newapplication());
        Inspection__c insp = tdm.newInspection();
        System.assertEquals(null, [Select Id,Sharing_Account__c from Inspection__c where Id = :insp.Id].Sharing_Account__c);
        
        Location__c loc = tdm.newlocationByLineItem(li.Id, locrectypeid);        
        insp.Location__c = loc.Id;
        update insp;
        
        //Location__c loc2 = tdm.newlocationByLineItem(li.Id, locrectypeid);
        //insp.Location__c = loc2.Id;
        //update insp;
        
    //    System.assertNotEquals(null, [Select Id,Sharing_Account__c from Inspection__c where Id = :insp.Id].Sharing_Account__c);
    }
    
}