/*
 * Purpose:This is an helper class for both Internal and external wizard.
 */
public with sharing class EFLWizardHelper {
       
   //Get the question to be asked for selected movement type 
    public static string getLandingOptionsQuestions(string movementType){
          CARPOL_External_Landing__c movemenTypeMetaData = CARPOL_External_Landing__c.getInstance(movementType); 
          return movemenTypeMetaData.Question__c;    
    }   

}