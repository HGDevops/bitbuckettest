@IsTest
public class EFLApplicationRepository_Test {
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();

    @testSetup
    static void setupData(){
        testData.insertcustomsettings();
        Application__c objApp = testData.newapplication();
        AC__c ac1=testData.newLineItemBRS('Personal Use',objApp);
        ac1.Type_of_Permit__c = 'Standard Permit';
        update ac1;
        
        
       /* //Construct__c testConstruct = testData.newconstruct(ac1.Id);
        Regulated_Article__c regArticle = new Regulated_Article__c();
        insert regArticle;
        Construct__c testConstruct = new Construct__c();
        testConstruct.Line_Item__c = ac1.Id;
        testConstruct.Link_Regulated_Article__c = regArticle.Id;
        testConstruct.Construct_s__c = 'Test';
        testConstruct.Mode_of_Transformation__c = '	Direct Injection';
        insert testConstruct;*/
        
        
     	//Commented this code as this below newAuth method is failing with "EFLAuthorizationEngineFactory.EFLAuthorizationEngineException: No Authorization Engine found for Authorization: null"
        //Authorizations__c authObj = testData.newAuth(objApp.id);
        Domain__c objProg = new Domain__c();
        objProg.Name = 'AC';
        objProg.Active__c = true;
        insert objProg;
        
        Program_Prefix__c prefix = new Program_Prefix__c();
        prefix.Program__c = objProg.Id;
        prefix.Name = '555';
        insert prefix; 
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP';
        objTP.Recordtypeid = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c = prefix.Id;
        insert objTP;
        //appObj = testData.newapplication();
        
        Authorizations__c authObj=new  Authorizations__c();
        authObj.Application__c=objApp.id;
        authObj.RecordTypeID=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        //authObj.Status__c='Submitted';
        authObj.Date_Issued__c=date.today();
        authObj.Applicant_Alternate_Email__c='test@test.com';
        authObj.UNI_Zip__c='32092';
        authObj.UNI_Country__c='United States';
        authObj.UNI_County_Province__c='Duval';
        authObj.BRS_Proposed_Start_Date__c=date.today()+30;
        authObj.BRS_Proposed_End_Date__c=date.today()+40;
        authObj.CBI__c='CBI Text';
        authObj.Application_CBI__c='No';
        authObj.Applicant_Alternate_Email__c='email2@test.com';
        authObj.UNI_Alternate_Phone__c='(904) 123-2345';
        authObj.AC_Applicant_Email__c='email2@test.com';
        authObj.AC_Applicant_Fax__c='(904) 123-2345';
        authObj.AC_Applicant_Phone__c='(904) 123-2345';
        authObj.AC_Organization__c='ABC Corp';
        authObj.Means_of_Movement__c='Hand Carry';
        authObj.Biological_Material_present_in_Article__c='No';
        authObj.If_Yes_Please_Describe__c='Text';
        authObj.Applicant_Instructions__c='Make corrections';
        authObj.BRS_Number_of_Labels__c=10;
        authObj.BRS_Purpose_of_Permit__c='Importation';
        authObj.Documents_Sent_to_Applicant__c=false;
        authObj.Expiration_Timeframe_Override__c = 'Manual'; 
        authObj.Status__c = 'Approved';
        authObj.Thumbprint__c = objTP.Id;
        Insert authObj;
        
        Regulated_Article__c regArt = testData.newRegulatedArticle(authObj.Program_Pathway__c);
        link_Regulated_articles__c lra = new link_Regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(ac1.Id, regArt.id, 'Draft');
        Construct__c testConstruct = testData.newconstruct(ac1.Id, regArt);
        PhenoType__c phenoTypeExpected = testData.newphenotype(testConstruct.Id);
        //GenoType__c objGenoType = testData.newgenotype(testConstruct.id);
        GenotypeType__c objGenoType = new GenotypeType__c();
        objGenoType.Construct__c = testConstruct.id;
        objGenoType.Genotype_Category__c = 'Wild Type';
        insert objGenoType;
        
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        Applicant_Attachments__c appAttObj = testDataAC.newAttach(ac1.id);
        Construct_Application_Junction__c objCAJ = new Construct_Application_Junction__c();
        objCAJ.Construct__c = testConstruct.Id;
	    objCAJ.Application__c = objApp.Id;
	    objCAJ.Authorization__c = authObj.Id;
	    objCAJ.Line_Item__c = ac1.Id;
	    objCAJ.Design_Protocol_Record_SOP__c = appAttObj.Id;
        insert objCAJ;
    }

    @isTest
    static void testSelectApplicationById() {
        testData.insertcustomsettings();
        Application__c objApp = [select Id from Application__c limit 1];
        Test.startTest();
        Application__c selectedApp = EFLApplicationRepository.selectApplicationById(objApp.Id);
        Application__c selectedApp2 = EFLApplicationRepository.selectApplicationById(null);
        Test.stopTest();
    }    
}