@isTest//(seealldata=true)
private class CARPOL_RandomInspTrigger_test {
     static String AccountRecordTypeId;
    static String ImpapcontRecordTypeId;
    static String PortsFacRecordTypeId;
    static String ACAppRecordTypeId;
    static String ACauthRecordTypeId;
    static String ACRegRecordTypeId;
    static Account objacct;
    static Contact objcont;
    static Program_Line_Item_Pathway__c objAcPthway;
    static Applicant_Contact__c apcont;
    static Applicant_Contact__c apcont2;
    static Facility__c fac;
    static Facility__c fac2;
    static Signature__c thumb;
    static Regulation__c newreg1;
    static Regulation__c newreg2;
    static Signature_Regulation_Junction__c srj1;
    static Signature_Regulation_Junction__c srj2;
    static Application__c objapp;
    static List<Application__c> appList;
    static AC__c ac1;
    static Regulation__c objreg1;
    static Regulation__c objreg2;
    static Regulation__c objreg3;
    static Attachment attach;
    static Authorizations__c objauth;
    static Authorization_Junction__c objauthjun1;
    static Authorization_Junction__c objauthjun2;
    static Authorization_Junction__c objauthjun3;
    private static testMethod void testRandomInspTrigger() {
        ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        
        
        
        
        List<EFL_Random_Inspection_Prefix__c> PrefixesToInsert = new List<EFL_Random_Inspection_Prefix__c>();
        Map<string,string> URLMap = new Map<string,string>{'555'=>'Organisms and Vectors','556'=>'Organisms and Vectors', '557'=>'Organisms and Vectors'};
        for(string x : URLMap.keyset()){
        EFL_Random_Inspection_Prefix__c cu = EFL_Random_Inspection_Prefix__c.getValues(x);
        if(cu == null){
            cu = new EFL_Random_Inspection_Prefix__c(name = x,EFL_Program_Pathway_Name__c  = URLMap.get(x));
            PrefixesToInsert.add(cu);
        }
    }
        if(PrefixesToInsert.size()>0){
        insert PrefixesToInsert;
    }
        
        objacct = testData.newAccount(AccountRecordTypeId); 
        objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        objAcPthway = new Program_Line_Item_Pathway__c();
        objAcPthway.Program__c = testData.newProgram('VS').Id;
        objAcPthway.Name = 'Organisms and Vectors';
        objAcPthway.Status__c = 'Active';  
        System.assert(objAcPthway != null);     
        insert objAcPthway;
          
        apcont = testData.newappcontact(); 
        apcont2 = testData.newappcontact();
        fac = testData.newfacility('Domestic Port');  
        fac2 = testData.newfacility('Foreign Port');
        thumb = testData.newThumbprint();
        thumb.name = 'PPQ--CITES--Fast Track';
        update thumb;
        newreg1 = testData.newRegulation('Import Requirements','Test');
        newreg2 = testData.newRegulation('Additional Information','Test');        
        srj1 = testData.newSRJ(thumb.id,newreg1.id);
        srj2 = testData.newSRJ(thumb.id,newreg2.id);             
        objapp = testData.newapplication();
        objapp.Signature__c = thumb.id;
        update objapp;
        Test.startTest();
        appList = new List<Application__c>();
        appList.add(objapp);
        ac1 = testData.newLineItem('Personal use',objapp);        
        ac1.Program_Line_Item_Pathway__c = objAcPthway.ID;
        ac1.Line_Item_Type_Hidden_Flag__c = 'Product';
        ac1.Facility_Building_Type__c = 'Building';
        objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        attach = testData.newattachment(ac1.Id);           
        objauth = testData.newAuth(objapp.Id); 
        system.debug('objauth program>>'+objauth.Thumbprint__c );
        ac1.Authorization__c = objauth.id;
        ac1.type_of_permit__c = 'Notification';
        update ac1;
        objapp.Application_Status__c = 'Submitted';
        update objapp;
        objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        Domain__c Program = new Domain__c();
        Program.Name = 'VS';
        insert Program;
        
        Program_Prefix__c Prefix = new Program_Prefix__c();
        Prefix.Name = '555';
        Prefix.Program__c = Program.id;
        insert Prefix;
        
        Signature__c TP = new Signature__c();
        TP.Name = 'VS TP';
        TP.Program_Prefix__c = Prefix.id;
        insert TP;
              
       
        Authorizations__c newauth = new Authorizations__c();
        newauth.Thumbprint__c = TP.id;
        newauth.Status__c = 'Issued';
        newauth.Application__c = objapp.Id;
        newauth.workflow_Number__c='150';
        insert newauth; 
        Authorizations__c newauth2 = new Authorizations__c();
        newauth2.Thumbprint__c = TP.id;
        newauth2.Application__c = objapp.Id;
        newauth2.Status__c = 'Issued';
        insert newauth2;
        Authorizations__c newauthx = new Authorizations__c();
        newauthx.Thumbprint__c = TP.id;
        newauthx.Application__c = objapp.Id;
        newauthx.Status__c = 'Issued';
        insert newauthx;
        
        
        //----------------PPQ 
        Domain__c ProgramPPQ = new Domain__c();
        ProgramPPQ.Name = 'PPQ';
        insert ProgramPPQ;
        
        Program_Prefix__c PrefixPPQ = new Program_Prefix__c();
        PrefixPPQ.Name = '556';
        PrefixPPQ.Program__c = ProgramPPQ.id;
        insert PrefixPPQ;
        
        Signature__c TPPPQ = new Signature__c();
        TPPPQ.Name = 'PPQ TP';
        TPPPQ.Program_Prefix__c = PrefixPPQ.id;
        insert TPPPQ;
        
        Authorizations__c newauthPPQ = new Authorizations__c();
        newauthPPQ.Thumbprint__c = TPPPQ.id;
        newauthPPQ.Application__c = objapp.Id;
        newauthPPQ.Status__c = 'Issued';
        
        insert newauthPPQ;
        Authorizations__c newauthPPQ2 = new Authorizations__c();
        newauthPPQ2.Thumbprint__c = TPPPQ.id;
        newauthPPQ2.Status__c = 'Issued';
        newauthPPQ2.Application__c = objapp.Id;
        insert newauthPPQ2;
        
        
        
        //-----------------AC
       
        Domain__c ProgramAC = new Domain__c();
        ProgramAC.Name = 'AC';
        insert ProgramAC;
        
        Program_Prefix__c PrefixAC = new Program_Prefix__c();
        PrefixAC.Name = '557';
        PrefixAC.Program__c = ProgramAC.id;
        insert PrefixAC;
        
        Signature__c TPAC = new Signature__c();
        TPAC.Name = 'AC TP';
        TPAC.Program_Prefix__c = PrefixAC.id;
        insert TPAC;
        
        Authorizations__c newauthAC = new Authorizations__c();
        newauthAC.Thumbprint__c = TPAC.id;
        newauthAC.Status__c = 'Issued';
        newauthAC.Application__c = objapp.Id;
        newauthAC.workflow_Number__c='150';
        insert newauthAC;
        Authorizations__c newauthAC2 = new Authorizations__c();
        newauthAC2.Thumbprint__c = TPAC.id;
        newauthAC2.Application__c = objapp.Id;
        newauthAC2.Status__c = 'Issued';
        insert newauthAC2;
        
        
       // test.startTest();
        EFL_Random_Inspection__c NewInsp = new EFL_Random_Inspection__c();
        NewInsp.Prefix_Number__c = 555;
        NewInsp.Percentage_of_defined_population__c = 99;
        insert NewInsp;
        EFL_Random_Inspection__c NewInsp2 = new EFL_Random_Inspection__c();
        NewInsp2.Prefix_Number__c = 556;
        NewInsp2.Percentage_of_defined_population__c = 99;
        insert NewInsp2;
        EFL_Random_Inspection__c NewInsp3 = new EFL_Random_Inspection__c();
        NewInsp3.Prefix_Number__c = 557;
        NewInsp3.Percentage_of_defined_population__c = 99;
        insert NewInsp3;
        test.stopTest();
    }
}