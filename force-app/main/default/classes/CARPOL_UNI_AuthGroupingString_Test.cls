@istest
public class CARPOL_UNI_AuthGroupingString_Test{
 public static testmethod void groupStringtest(){
  Domain__c objProg = new Domain__c();
     objProg.Name = 'VS';
     objProg.Active__c = true;
     insert objProg;

    Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
    objParentPathway.Program__c = objProg.Id;
    objParentPathway.Name = 'Live Animals';
    objParentPathway.Status__c = 'Active';  
    System.assert(objParentPathway != null);     
    insert objParentPathway;
    Program_Line_Item_Section__c prgsect = new Program_Line_Item_Section__c();
    prgsect.Name = 'Importer';
    prgsect.Section_Order__c = 1;
    prgsect.Destination_Object__c = 'Line Item';
    prgsect.Program_Line_Item_Pathway__c = objParentPathway.id;
    insert prgsect;
    Program_Line_Item_Field__c prglnfld  = new Program_Line_Item_Field__c();
    prglnfld.Name = 'Importer Last Name'; 
    prglnfld.isActive__c = 'Yes';  
    prglnfld.Program_Line_Item_Section__c = prgsect.id;
    prglnfld.EFLAddress_Restriction__c = 'No Restriction';
    insert prglnfld;
    Authorization_Grouping_Fields__c authgrpstr = new Authorization_Grouping_Fields__c();  
    authgrpstr.Program_Line_Item_Pathway__c = objParentPathway.id;
    authgrpstr.Program_Line_Item_Field__c= prglnfld.id;
    insert authgrpstr; 
    system.debug('$$$authgrpstr = '+authgrpstr);
    CARPOL_AC_TestDataManager testInst = new CARPOL_AC_TestDataManager();
    testInst.insertcustomsettings();
    Application__c objapp = testInst.newapplication();    
    AC__c ac = testInst.newLineItem('Resale/Adoption',objapp);  
    Applicant_Contact__c appcon = testInst.newappcontact();         
    ac.Importer_Last_Name__c = appcon.id; 
    ac.Program_Line_Item_Pathway__c =  objParentPathway.id;  
    update ac; 
    list<ac__c> lineitems = new list<ac__c>();
    lineitems.add(ac);
    System.assert(CARPOL_UNI_AuthGroupingString.groupString(lineitems)!=null);  
 }
}