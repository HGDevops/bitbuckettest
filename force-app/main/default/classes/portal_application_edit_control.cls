public with sharing class portal_application_edit_control {
    public Id applicationID{get;set;}
    public Application__c application{get;set;}
    public ApexPages.StandardController controller{get;set;}
    public boolean messageVisible{get;set;}
    static final string APPLICANT_TRANSFER = 'Applicant Transfer';
    
    public boolean canEditApplicant{
        get{
            if(canEditApplicant == null){
                Id userProfileId = UserInfo.getProfileId();
                Profile p = [SELECT Name FROM Profile WHERE Id = :userProfileId];
                if(p.Name == 'eFile Applicant Plus'){
                    canEditApplicant = true;
                }else{
                    canEditApplicant = false;
                }
            }
            return canEditApplicant;
        }set;}
    
    public portal_application_edit_control(ApexPages.StandardController controller) {
        
        applicationID=ApexPages.currentPage().getParameters().get('id');
        application=(Application__c)controller.getRecord();
        
        messageVisible=false;
        
    }
    
    //updating application and transferring ownership
    public PageReference UpdatedApplication()
    {
        Savepoint sp = Database.setSavepoint();
        try 
        {
            update application;              
            PageReference pr = new PageReference('/'+application.id);
            return pr;
            
        }
        catch(exception ex){
            Database.rollback(sp);
            EFLErrorLog.createErrorLog('portal_application_edit_control.UpdatedApplication()',ex);   
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error: ' + ex.getMessage()));
            return null;             
        }
        return null;
    }    
    
    
}