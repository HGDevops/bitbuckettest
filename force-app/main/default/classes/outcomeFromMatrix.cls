public class outcomeFromMatrix{
    
    public List<CARPOL_Outcome_Matrix__c> omList; //list to hold the incoming custom setting records
    public Map<String,CARPOL_Outcome_Matrix__c> alreadyCheckedMap = new Map<String,CARPOL_Outcome_Matrix__c>(); //map to hold previously checked combinations
    
    //Constructor
    //accepts a custom setting of type CARPOL Outcome Matrix 
    public outcomeFromMatrix(List<CARPOL_Outcome_Matrix__c> omInList){
        omList = omInList;
    }
    
    //Method called from outside
    //pass in the two initial outcomes, and return the combined outcome if found
    public CARPOL_Outcome_Matrix__c getOutcome(String outcome1,String outcome2){
        
        String stringsCombined = outcome1+outcome2;
        
        //if we've already checked this combination there's no need to iterate
        if(alreadyCheckedMap.containskey(stringsCombined)){
            return alreadyCheckedMap.get(stringsCombined);
        }
        
        //check for an outcome matrix record that matches the two input outcomes and return it
        for(CARPOL_Outcome_Matrix__c omVar : omList){
            if(omVar.Outcome_One__c == outcome1 && omVar.Outcome_Two__c == outcome2){ 
                alreadyCheckedMap.put(stringsCombined,omVar);
                return omVar;
            }
            else if(omVar.Outcome_One__c == outcome2 && omVar.Outcome_Two__c == outcome1){
                alreadyCheckedMap.put(stringsCombined,OmVar);
                return omVar;
            }
        }
        
        return null;
    }
}