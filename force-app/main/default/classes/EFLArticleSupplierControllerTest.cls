@isTest
public class EFLArticleSupplierControllerTest {
   public static Article_Supplier_Developer__c artsuppl;
   public static Article_Supplier_Developer__c artsuppl1;
   public static Article_Supplier_Developer__c artsuppl2;
   public static Article_Supplier_Developer__c artsuppl3;
   public static Domain__c prog;
   public static Program_Line_Item_Pathway__c plip;
   public static Program_Prefix__c pp;
   public static Signature__c tp;
   public static Trade_Agreement__c ta;
   public static Country__c Country;
   public static Country__c USCountry;
   public static Level_1_Region__c level1;
   public static Level_2_Region__c level2;
   public static Authorizations__c auth;
   public static Application__c objApp ;
   public static Contact con;
   public static Id artsupplrectypeid ;
   public static Id imprectypeid ;
   public static Id deliveryrecptrectypeid ;
   public static Id exprectypeid ;
   static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
             
   public static void setup() {
   
     artsupplrectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Info type');
     imprectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Importer');
     deliveryrecptrectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Delivery');
     exprectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Exporter');
    
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
         objApp = testData.newapplication();
         con = testData.newContact();
         prog = testData.newProgram('test');
         plip = testData.newCaninePathway();
         pp = testData.newPrefix();
         tp = testData.newThumbprint();
         ta = testData.newta();
        AC__c ac1 = testData.newLineItemBRS('Personal Use',objApp);
        ac1.Type_of_Permit__c = 'Standard Permit';
        update ac1;
        Country = testData.newcountrywithassoc();
         USCountry = testData.newcountryus();
         level1 = testData.newlevel1region(Country.id);
         level2 = testData.newlevel2region(level1.id);
         auth = testData.newAuth(objApp .Id);
                 
         Level_1_Region__c USState = testData.newlevel1region(USCountry.id);
         Level_2_Region__c USCounty= testData.newlevel2region(USState.id);
         
        artsuppl = new Article_Supplier_Developer__c();
        artsuppl.First_Name__c = 'Test';
        artsuppl.Name = 'last';
        artsuppl.Line_Item__c = ac1.Id;
        artsuppl.RecordtypeId = imprectypeid;
        artsuppl.Country__c = USCountry.Id;
        artsuppl.State__c = USState.Id;
        artsuppl.Level_2_Region__c = USCounty.Id;
        artsuppl.City__c = 'Delaware';
        artsuppl.Street_Address__c = '123 test';
        artsuppl.Postal_Code__c = '20456';
        insert artsuppl;
        
        artsuppl.First_Name__c = 'First';
        update artsuppl;
        
        artsuppl1 = new Article_Supplier_Developer__c();
        artsuppl1.First_Name__c = 'Test';
        artsuppl1.Name = 'last';
        artsuppl1.Line_Item__c = ac1.Id;
        artsuppl1.RecordtypeId = artsupplrectypeid;
        artsuppl1.Country__c = USCountry.Id;
        artsuppl1.State__c = USState.Id;
        artsuppl1.Level_2_Region__c = USCounty.Id;
        artsuppl1.City__c = 'Delaware';
        artsuppl1.Street_Address__c = '123 test';
        artsuppl1.Postal_Code__c = '20456';
        insert artsuppl1;


    }

    public static testMethod void InitializeClass_Test() {
    Test.StartTest();
        setup();
        AC__c lineItem = [SELECT Id, Application_Number__c FROM Ac__c LIMIT 1];

        PageReference pRef = Page.EFLArticleSupplier;
       Test.setCurrentPage(pRef);
        ApexPages.CurrentPage().getParameters().put('LineItemId', lineItem.Id);
        Article_Supplier_Developer__c asd = new Article_Supplier_Developer__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(asd );
        EFLArticleSupplierController eflArtSupCtrl = new EFLArticleSupplierController(sc);
        eflArtSupCtrl.recordtypeid= imprectypeid;
        eflArtSupCtrl.lineItemId = lineItem.Id;
        boolean testLockLine = eflArtSupCtrl .lockLineItem;
        string testInst = eflArtSupCtrl .instructions;
        string testsubCountry = eflArtSupCtrl .subCountryId;
        string testsubState = eflArtSupCtrl .subStateId;
        string testsubCounty = eflArtSupCtrl .subCountyId;
        boolean testrenderList = eflArtSupCtrl .renderList;
        boolean testrenderDetail =  eflArtSupCtrl .renderDetail;
        boolean testEnableApp = eflArtSupCtrl .enableApplicantInstructions;
        Boolean showArticleView = eflArtSupCtrl .showArticleView ;
        boolean renderRTDetail = eflArtSupCtrl .renderRTDetail ;
        Boolean regArtEnableDisable = eflArtSupCtrl.regArtEnableDisable;
        Boolean testprogram = eflArtSupCtrl.program;
        string testcurrentStep = eflArtSupCtrl.currentStep;
        eflArtSupCtrl.asdID = artsuppl.Id;
        eflArtSupCtrl.subStateId = '';
        eflArtSupCtrl.init();
        eflArtSupCtrl.getInstructions();
        eflArtSupCtrl.getApplicationInstructions();
        eflArtSupCtrl.setLockLineItem();
        eflArtSupCtrl.program();
        eflArtSupCtrl.getIsFieldRequired();
        eflArtSupCtrl.getIsFieldReadonly();
        eflArtSupCtrl.getRelatedLineItem();
        eflArtSupCtrl.getLineItemRecord();
        eflArtSupCtrl.getASDList();
        eflArtSupCtrl.showArticleDetailView();
        eflArtSupCtrl.displayRTSelectionPage();
        eflArtSupCtrl.setRecordtype();
        eflArtSupCtrl.saveRTSelection();
        //list<SelectOption> getRecordTypes = eflArtSupCtrl.getRecordTypes();
        eflArtSupCtrl.showDetail();
        eflArtSupCtrl.populateApplicantInfo();
        eflArtSupCtrl.cancel();
        eflArtSupCtrl.updateASD();
        eflArtSupCtrl.processCountrySelection();
      map<string,string> locMap = new map<string, string>();
           eflArtSupCtrl.delrecid = artsuppl.id;
           string s = artsuppl.id;
           s = s.substring(0,3);
           locMap.put(s,'Article_Supplier_Developer__c');
           eflArtSupCtrl.sobjectkeys = locMap;
           eflArtSupCtrl .deleteRecord();
           eflArtSupCtrl.asdRecord = artsuppl; 
       
       Test.Stoptest(); 

    }
    
    public static testMethod void testHappyPath() {
       
        testData.insertcustomsettingsWithBRSTriggerDisabled();
            
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        test.startTest();
        
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Open', 1);
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            auth = new Authorizations__c();
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
        	PageReference pRef = Page.EFLArticleSupplier;
            Test.setCurrentPage(pRef);
            ApexPages.CurrentPage().getParameters().put('LineItemId', lineItem.Id);
            Article_Supplier_Developer__c asd = new Article_Supplier_Developer__c();
            ApexPages.StandardController sc = new ApexPages.StandardController(asd );
            EFLArticleSupplierController eflArtSupCtrl = new EFLArticleSupplierController(sc);
            try{eflArtSupCtrl.getRecordTypes(); }catch(exception ex){} 
            eflArtSupCtrl.recordtypeid= EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Importer');
        	try{eflArtSupCtrl.saveRTSelection();}catch(exception ex){}
        	EFLArticleSupplierRepository.selectASDByLineItemIDForClone(LineItem.id);
        	try{EFLArticleSupplierRepository.selectASDByLineItemID(null);}catch(exception ex){}
       		try{EFLArticleSupplierRepository.selectASDByLineItemIDForClone(null);}catch(exception ex){}
       		try{EFLArticleSupplierRepository.selectByID(null);}catch(exception ex){}
        
        Test.Stoptest(); 
    }

  
}