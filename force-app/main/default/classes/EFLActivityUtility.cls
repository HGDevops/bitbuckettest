public with sharing class EFLActivityUtility {
    
    public static Task populateTask(sObject parentRecord, Program_Activity__mdt programActivityCMD,ID OwnerID)
    {
           Task taskRecord = new Task();                          
            taskRecord.whatId = parentRecord.Id;
            taskRecord.subject = programActivityCMD.Subject__c;
            taskRecord.stage__c = programActivityCMD.Program_Stage__r.Stage__c;
            taskRecord.Activity_Sequence__c = programActivityCMD.Activity_Sequence__c;
            taskRecord.Type = programActivityCMD.Activity_Type__c;
            taskRecord.RecordTypeId = EFLGenericUtility.getRecordTypeId('TASK Standard');
            if(OwnerID!=null){
                taskRecord.OwnerId = OwnerID;
            } 
       
        return taskRecord;
    } 
     //break into 2 methods - DEAD METHOD
     public static list<team__c> getTeamMembers(ID parentRecordID)
     { 
         string query;
         string sObjName = parentRecordID.getSObjectType().getDescribe().getName();
        if (sObjName == 'Authorizations__c')
            query = 'select Member__c,member_role__c from Team__c where authorization__c =: parentRecordID';
        else if (sObjName == 'Inspection__c')
             query = 'select Member__c,member_role__c from Team__c where inspection__c =: parentRecordID';
         else if (sObjName == 'Incident__c')
             query = 'select Member__c,member_role__c from Team__c where Incident__c =: parentRecordID';
         
         return database.query(query); 
         
     }
    
     public static Program_Activity__mdt getProgramActivity(string stage,string workflowNumber, integer activitySequence)
     {
         List<Program_Activity__mdt> pam = new List<Program_Activity__mdt>() ;
         if(stage != null)
         {
         System.debug('stage: ' + stage);
          System.debug('workflowNumber: ' + workflowNumber);
          System.debug('activitySequence: ' + activitySequence);
              pam =  [SELECT Activity_Sequence__c,Activity_Type__c,
                        Assigned_To__c,Due_Date__c,
                        Priority__c,Program_Stage__c,Subject__c,
                        Program_Stage__r.Stage__c,Last_Activity_for_Stage__c
                   FROM Program_Activity__mdt
                  WHERE Program_Stage__r.Stage__c =:  stage
                    AND Program_Stage__r.Workflow_Number__c =: workflowNumber
                    AND Activity_Sequence__c =: activitySequence ];  
         }
         if(pam.isEmpty())
         {
         return null;
         }
         return pam[0];
     }
    
    
    public static boolean validateTeam(){
        
        return false;
    }
    
    public static boolean validateActivityTask(){
        
        return false;
    }

    public static boolean needsTaskCreation(authorizations__c authRecordOld, authorizations__c authRecord){
        
        if(authRecordOld.Stage__c != authRecord.stage__c){ 
            return true;
        } 
        return false;
    }    
    
    public static boolean isLastActivityForStage(Program_Activity__mdt pam){
        system.debug('Last_Activity_for_Stage__c '+ pam.Last_Activity_for_Stage__c);
        if(pam.Last_Activity_for_Stage__c){
            return true;
        } 
        return false;
    } 
    
    //Validate Roles for any Activity Process
   /* public String validateRoles(sObject sObjectRecord,list<team__c> teamMembersList,List<String> currentRequiredRoles){ 
          //Validate the Roles Logic - which is currently recides in class EFLBRSAuthorizationEngine
         return NULL;
    }*/
    
    public String validateRoles(sObject sObjectRecord,list<team__c> teamMembersList,List<String> currentRequiredRoles){
        String errorMessage = null;
        String errorBody;
        system.debug('EFLACTIVITYUTILITY Called &&&&&');
        system.debug('EFLACTIVITYUTILITY teamMembersList ## ' + teamMembersList);
        system.debug('EFLACTIVITYUTILITY currentRequiredRoles ### ' + currentRequiredRoles);
       //  list<team__c> teamMembersList = new list<team__c>();
      //  teamMembersList = EFLTeamRepository.selectByAuthorizationID(authRecord.ID); 
        ID ownerID;
        
        Schema.sObjectType soType = sObjectRecord.getSObjectType();
        
        if (soType == Authorizations__c.sObjectType)
		{
			errorBody = [Select Message__c from EFLMessages__mdt where MasterLabel = 'Authorization Role Validation' limit 1][0].Message__c; 
		}       
        else if (soType == Inspection__c.sObjectType)
		{
			errorBody = [Select Message__c from EFLMessages__mdt where MasterLabel = 'Inspection Role Validation' limit 1][0].Message__c;
		} 
        else if (soType == Incident__c.sObjectType)
        {
            errorBody = [Select Message__c from EFLMessages__mdt where MasterLabel = 'Incident Role Validation' limit 1][0].Message__c;
        }
      
        map<String,boolean> TeamMemberMap = new map<String,boolean>();
        for(team__c currentMember : teamMembersList){
            if(!String.isBlank(currentMember.Member__c))
               { 
                 TeamMemberMap.put(currentMember.Member_Role__c.trim(),true);
               }
            system.debug('TeamMemberMap!!!!!!! ' + TeamMemberMap);
         } 
        string missedRoles = '';
            for(String reqRole : currentRequiredRoles){

                if(TeamMemberMap.get(reqRole.trim()) == null){
                    if(missedRoles!=''){
                         missedRoles = missedRoles + ',' + reqRole;
                    }else{
                        missedRoles = reqRole;
                    }
                }
                
            }
        if(missedRoles!=''){
            errorMessage = 'Error: Add ' + missedRoles + ' ' + errorBody;
                //' before continuing to the Application Review Stage.';
        }
              
        return errorMessage;
    }
    
}