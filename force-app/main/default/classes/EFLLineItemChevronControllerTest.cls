@isTest
public class EFLLineItemChevronControllerTest {
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    @istest  
  	public static void testCheveronHappyPath(){
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        user portalUser = new User();
        portalUser = EFLUserTestDataFactory.getUser('eFile Applicant');
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        Application__c app = new Application__c();
        app = testData.newapplication();
        Program_Line_Item_Pathway__c plip = new Program_Line_Item_Pathway__c();
        plip = testData.newBRSPathway();
        Regulated_Article__c ra = new Regulated_Article__c();
        ra = testData.newRegulatedArticle(plip.id);
        AC__c lineItem = new AC__c();
        lineItem = testData.newLineItemBRSByRA('Personal Use', app, ra.Id);       
        Signature__c brsThumbprint = testData.newBRSThumbprint();
        brsThumbprint.Name='BRS Standard Permit';
        update brsThumbprint;
        lineItem.Application_Details_Status__c  = CARPOL_Constants.READY_TO_SUBMIT;
        lineItem.Thumbprint__c = brsThumbprint.id;
        update lineItem;
        EFLLineItemChevronController chevronController = new EFLLineItemChevronController();
        chevronController.lineItemId = lineItem.id;
        EFLLineItemChevronController.currentChevronStep = 'Constructs';
        integer previousSequence = chevronController.previousSequence;
        chevronController.totalSteps = null;
        integer totalSteps = chevronController.totalSteps;
        id SOPRecordTypeID = chevronController.SOPRecordTypeID;
        boolean enableNavigation = chevronController.enableNavigation;
        String chevronOptions = chevronController.chevronOptions;
        chevronController.Next();
        chevronController.Back();
        chevronController.callStepID = 'Constructs';
        chevronController.callPage();
    }

}