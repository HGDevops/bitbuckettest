/**
* Author : Kishore Kumar
* Created Date : 12/12/2016
* Purpose : Utility class for Exception logging messages
*/
public inherited sharing class EFLErrorLog {
    static final string DMLEXCEPTION = 'Dml';
    public static void createerrorlog(String priority, String className, String message) { 
        insert (new Error_Logging__c(priority__c=priority,class__c=className,Trace__c=message));
    }
    
    public static void createErrorLog(string className,exception exp) { 
        string DMLMessage='';
        if(exp.getTypeName().contains(DMLEXCEPTION)){
            Integer numErrors = exp.getNumDml();
            for(Integer i=0;i<numErrors;i++) {
                + exp.getDmlFieldNames(i);
                DMLMessage= + exp.getDmlMessage(i);
            }           
            
        }else{
            DMLMessage = exp.getMessage();
        }
        string exceptionMessage = 'Exception type caught: ' + exp.getTypeName() + '\n' + 
            'Message: '+ DMLMessage + '\n' + 
            'Cause: ' + exp.getCause() + '\n' +
            'Line number: '+ exp.getLineNumber() + '\n' +
            'Stack trace: ' + exp.getStackTraceString();        
        
        insert (new Error_Logging__c(priority__c='Error',class__c=className,Trace__c=exceptionMessage));
    }    
    
}