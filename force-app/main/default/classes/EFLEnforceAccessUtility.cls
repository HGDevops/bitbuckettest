/*-----------------------------------------------------------------------  
 @Purpose: Apex generally runs in system context; Apex doesn't enforce  
           object-level and field-level permissions by default,we enforce 
           these permissions by explicitly calling below sObject 
           describe result methods.
 ----------------------------------------------------------------------*/
public with sharing class EFLEnforceAccessUtility {
    static final string INSUFFICENT_UPDATE_ACCESS = 'Insufficient Update access to ';
    static final string INSUFFICENT_DELETE_ACCESS = 'Insufficient Delete access to ';
    static final string INSUFFICENT_INSERT_ACCESS = 'Insufficient Create access to ';
    static final string INSUFFICENT_READ_ACCESS = 'Insufficient Read access to ';
    static final string INFO = 'INFO';
    static final string OBJECTCHAR = ' Object';
   
    // Check Object Update Access
    public static void checkObjectUpdateAccess(string objectAPIName){
        checkObjectReadAccess(objectAPIName);
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(objectAPIName);
         
             if(!sObjectType.getDescribe().isUpdateable()){
                 throwerror(INSUFFICENT_UPDATE_ACCESS+sObjectType.getdescribe().getlabel()+OBJECTCHAR);
             }  
    }

    // Check Object Delete Access
    public static void checkObjectDeleteAccess(string objectAPIName){
        checkObjectReadAccess(objectAPIName);
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(objectAPIName);
         
             if(!sObjectType.getDescribe().isDeletable()){ 
                  throwerror(INSUFFICENT_DELETE_ACCESS+sObjectType.getdescribe().getlabel()+OBJECTCHAR);
             } 

    } 
     public class accessException extends Exception{}

    // Check Object Create Access
    public static void checkObjectInsertAccess(string objectAPIName){
        checkObjectReadAccess(objectAPIName);
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(objectAPIName);
         
             if(!sObjectType.getDescribe().isCreateable()){ 
                 throwerror(INSUFFICENT_INSERT_ACCESS+sObjectType.getdescribe().getlabel()+OBJECTCHAR);
             } 
    } 

    // Check Object Accessiable to read 
    public static void checkObjectReadAccess(string objectAPIName){
       
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(objectAPIName);
         
             if(!sObjectType.getDescribe().isAccessible()){  
                  throwerror(INSUFFICENT_READ_ACCESS+sObjectType.getdescribe().getlabel()+OBJECTCHAR);
             } 
    } 
    
    //Log error and Throw error message
    Private static void throwerror(string errorMessage){
                   throw new accessException(errorMessage);
    }

}