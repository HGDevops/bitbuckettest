/**
* Class containing tests for LightningSelfRegisterController
*/
@IsTest public with sharing class LightningSelfRegisterControllerTest {
    @IsTest static void testRegistration() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
        
        Account objacct = new Account();
        objacct.Name = 'Global Account Test';
        objacct.RecordTypeId = AccountRecordTypeId;
        insert objacct;
        
        LightningSelfRegisterController controller = new LightningSelfRegisterController();
        //controller.username = 'test@force.com';
        //controller.email = 'test@force.com';
        //controller.communityNickname = 'test';
        // registerUser will always return null when the page isn't accessed as a guest user
        //System.assert(controller.registerUser() == null);    
        
        //controller.password = 'abcd1234';
        //controller.confirmPassword = 'abcd123';
        LightningSelfRegisterController.selfRegister('Test' ,'User', 'test@force.com', 'Abcd05152018', 'Abcd05152018', objacct.id, '', null, '', false);
        LightningSelfRegisterController.selfRegister('Test' ,'', 'test@force.com', 'Abcd05152018', 'Abcd05152018', objacct.id, '', null, '', false);        
        LightningSelfRegisterController.selfRegister('Test' ,'User', '', 'Abcd05152018', 'Abcd05152018', objacct.id, '', null, '', false);      
        LightningSelfRegisterController.selfRegister('Test' ,'User', 'test@force.com', 'Abcd05152018', 'Abcd0515201', objacct.id, '', null, '', true);
        //LightningSelfRegisterController.selfRegister('Test' ,'User', 'test@force.com', 'abcd1234', 'abcd1234', objacct.id, '', null, '', true);
        User guestUser = [select id, name from User where Profile.UserLicense.Name LIKE 'Guest%' LIMIT 1];
        Test.startTest();
        System.Runas(guestUser) {
            LightningSelfRegisterController.selfRegister('Test' ,'User', 'test@force.com', 'Abcd05152018', 'Abcd05152018', objacct.id, '', null, '', false);
        }
        Test.stopTest();
        User u = [SELECT id, firstname FROM User WHERE firstName = 'Test' Limit 1];
        system.assert(u.firstName == 'Test');
        system.assert(controller != null);        
       
    }
    
   // @IsTest 
   static void testGetExtraFields() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
        List<Map<String,Object>> extraFields = LightningSelfRegisterController.getExtraFields('Test_Field_Set_1');
        System.assertNotEquals(extraFields.size(), 0);
        String extraFieldsString = JSON.serialize(extraFields);
        Account objacct = new Account();
        objacct.Name = 'Global Account Test';
        objacct.RecordTypeId = AccountRecordTypeId;
        insert objacct;  
        Test.startTest();
        LightningSelfRegisterController.selfRegister('Test' ,'User', 'test@force.com', 'Abcd05152018', 'Abcd05152018', objacct.id, '', extraFieldsString, '', true); 
        Test.stopTest();
    } 
    
    @IsTest 
    static void testSiteAsContainerEnabled() { 
        //String networkId = Network.getNetworkId(); 
        Network net = [SELECT Id, name FROM Network where name = 'APHIS Customer Community' Limit 1];
        String LoginURL = network.getLoginUrl(net.id);
        system.debug('loginURL=== '+ LoginURL);
        Test.startTest();
        //Boolean ret = LightningSelfRegisterController.siteAsContainerEnabled(LoginURL);
        //Test.stopTest();
        //system.assert(ret==false);        
    }
  
}