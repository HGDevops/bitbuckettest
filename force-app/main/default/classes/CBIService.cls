public interface CBIService {
    //List<String> getCBIData(String lineItemId);
    AC__c getCBIData(AC__c objLineItemObj);
    AC__c getCBIDeletedData(AC__c objLineItemObj); 
}