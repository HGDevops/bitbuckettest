public with sharing class EFLProgramPrefixUtility {
    
    // Returns list of Required Roles for each Label in the Program Prefix
    public static List<String> getRequiredRoles(string labelName){
        List<String> requiredRolesList = new List<String>();
        string requireRolesString;
        List<EFLPermitPrefixNumber__mdt> roles = new List<EFLPermitPrefixNumber__mdt>();
        
        if(labelName != null){        
        roles = [SELECT Required_Roles__c from EFLPermitPrefixNumber__mdt where Label =: labelName];
            
        requiredRolesList = roles[0].Required_Roles__c.split(';'); 
        }
         return requiredRolesList;        
    }
}