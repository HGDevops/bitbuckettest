public class EFLGetHQDetails {
    @auraEnabled
    public static EFLRegistration__c getAccountHQ(string annualReportId){
        EFLAnnual_Report__c rep = [Select EFLRegistration__r.id, EFLRegistration__r.ELFHQStreet_Address__c,EFLRegistration__r.EFLHQCity__c
                                   , EFLRegistration__r.EFLHQState__c,EFLRegistration__r.EFLHQPhone__c,EFLRegistration__r.ELFHQZip__c
                                   From EFLAnnual_Report__c 
                                   Where id=:annualReportId];
        return rep.EFLRegistration__r;
    }
    
    @auraEnabled
    public static list<EFLAnnual_report__c> getAnnRepList(string annRepId){
        return [Select id, EFLSites__c,Status__c, EFL_Using_Federal_Funds__c, EFL_Change_In_Ownership__c
                From EFLAnnual_Report__c
                Where id=:annRepId];
    }
    
    @auraEnabled
    public static void saveData(string annRepId,string sites, string usingFederalFunds, string changeInOwnership){
        if(annRepId != null && sites !=null ){
            eflAnnual_report__c rep = [Select Id, EFLSites__c, EFL_Using_Federal_Funds__c, EFL_Change_In_Ownership__c
                                       From EFLAnnual_Report__c
                                       Where id=:annrepId];
            rep.eflSites__c = sites;
            String modifiedSiteData = sites.replace('\n',',');
            rep.EFLSpringCMSites__c = modifiedSiteData;
            rep.EFL_Using_Federal_Funds__c = usingFederalFunds;
            rep.EFL_Change_In_Ownership__c = changeInOwnership;
            update rep;
        }
    }
    
    @auraEnabled
    public static List<EFLUtils.LightningPicklistOption> getPicklistValues(string sObjectName, string fieldName){
        return EFLUtils.getPicklistOptions(sObjectName, fieldName);
    }

}