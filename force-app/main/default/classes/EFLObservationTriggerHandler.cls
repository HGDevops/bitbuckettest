//Developer Name: Peter Tran
//Creation Date: 03/18/2019
//File Name: EFLObservationTriggerHandler.apxc
//sObject Affected: Observation__c
//Client: United States Department of Agriculture
//Client Project Name: APHIS CARPOL
//Contractor: Accenture Federal Services

/*****************************
*  Developer Documentation  *
*****************************
* _________________________
* Trigger Handler Description
* 
* Changelog
* V1.0: Creation of EFLObservationTriggerHandler.apxc
* - File created in DevShare on 03/18/2019
* 
*/

//Begin class declaration
public inherited sharing class EFLObservationTriggerHandler {
    
    //This method performs data validation for an Observation__c record on whether Observation_Date__c is unique
    public static void checkObservationStartDate(List<Observation__c> triggerNew) {
        
        //If the trigger has fired in the real context
        //system.debug('Begin checkObservationStartDate(List<Observation__c> triggerNew)');
        Id prrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
        //Get list of existing Observation__c records with its Observation_Date__c
        List<Observation__c> existingRecords = [SELECT Id, Observation_Date__c,self_Reporting__c FROM Observation__c];
        
        //For all existing records in the Observation__c sObject
        for(Observation__c e : existingRecords) {
            //For all records that were inserted by the execution context in Trigger.new
            for(Observation__c o : triggerNew) {
                if(e.Observation_Date__c == o.Observation_Date__c && e.self_Reporting__c == o.self_Reporting__c) {
                    //system.debug('There is a duplicate Observation Date for Record ID: ' + e.Id);
                    //Add two field level validations to make sure that the user is not entering data that has the same start date and location
                    o.Observation_Date__c.addError('There are record(s) where the Observation Date is the same.');
                } else {
                    //Add this particular record to insert into the database
                    //system.debug('This record is okay to insert.');
                } //end else 
            } //end for inner loop
        } //End outer for loop
    } //end method checkObservationStartDate()           
  
}//end class