@isTest
private class EFLFormatUtility_Test {
    
    static testMethod void TestformatAddress() {
        CARPOL_AC_TestDataManager acData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = acData.AccountRecordTypeId; 
        Account acc = acData.newAccount(AccountRecordTypeId);
        acc.BillingCity = 'Philadelphia';
        acc.BillingStreet = '123 Test Street';
        acc.BillingState = 'Virginia';
        acc.BillingPostalCode = '19000';
        update acc;
        acc = [SELECT BillingAddress, BillingStreet, BillingCity, BillingState, BillingPostalCode  FROM Account WHERE ID=: acc.Id Limit 1];
        Test.startTest();
        String addr = EFLFormatUtility.formatAddress(acc.BillingAddress);
        Test.stopTest();
       
        system.assert(addr.containsIgnoreCase(acc.BillingStreet + '<br/> ' + acc.BillingCity + ', VA ' + acc.BillingPostalCode));       
    }
    
    static testMethod void TestformatDate() {
        Date dateNow = Date.today();
        String dateFormat = 'M/d/yyyy';
        
        Test.startTest();
        String dateTimeNow = EFLFormatUtility.formatDate(dateNow, dateFormat);              
        // Check negative case when date is null
        String retVal = EFLFormatUtility.formatDate(null, dateFormat);
        Test.stopTest();
        
        system.assert(dateNow.format() == dateTimeNow);
        system.assert(retVal == null);
    }

}