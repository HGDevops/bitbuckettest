public with sharing class EFLARPStateOfDestination implements EFLIActivityResultProcessor{
    
    //State Of Destination Dynamic options (apex) based options
    //PQS Result to identify placeholder option to move forward to next question
    public boolean processResult(EFLPreScreeningWrapper psw){ 
        
        //Get nextQuestionIds from currentquestion options
        List<Id> nextQuestionIds = new List<Id>();
        for(Wizard_Selections__c ws : psw.currentQuestion.psqOptions)
        {
            nextQuestionIds.add(ws.Wizard_Next_Question__c);
        }
        
        //Fetchquestions by nextQuestionIds
        Map<id, Wizard_Questionnaire__c> questionMap = new Map<id, Wizard_Questionnaire__c>();
        questionMap = EFLPSQUtility.getQuestionsByNextQuestionIds(nextQuestionIds);
        
        //Previous Question Select value - CountryId
        //id selectedCountryId = (Id)psw.QuestionList[psw.QuestionList.size()-2].SelectedOption;
        id selectedCountryId = (Id)psw.QuestionList[psw.currentQuestionIndex-2].SelectedOption;
            
        //Current Question Select value - StateId
        id selectedStateId = (Id)psw.currentQuestion.SelectedOption;   
        
        //Group Map of the above selected CountryId belongs
        Map<Id, Country_Junction__c> selectedCountryGroupMap = new Map<Id, Country_Junction__c>(); 
        selectedCountryGroupMap = EFLPSQUtility.getSelectedCountryGroupMap(selectedCountryId);
        
        for(integer i=0; i<psw.currentQuestion.psqOptions.size(); i++)
        {
            Wizard_Questionnaire__c nextQuestionOnThisOption = new Wizard_Questionnaire__c();
            nextQuestionOnThisOption = questionMap.get(psw.currentQuestion.psqOptions[i].Wizard_Next_Question__c);
            if(nextQuestionOnThisOption.Country_Group__c!=null && nextQuestionOnThisOption.State_Province_of_Destination__c!=null)
            {
                if(selectedCountryGroupMap.get(nextQuestionOnThisOption.Country_Group__c)!=null && selectedStateId == nextQuestionOnThisOption.State_Province_of_Destination__c)
                {
                    //static option name and value field values are set here
                    psw.currentQuestion.psqOptions[i].Name = psw.currentQuestion.SelectedOptionText;
                    psw.currentQuestion.psqOptions[i].Value__c = psw.currentQuestion.SelectedOption;
                    //psw selectedOption set here
                    psw.currentQuestion.psqSelectedOption = psw.currentQuestion.psqOptions[i];
                    return true;
                }
            }
            else if(nextQuestionOnThisOption.State_Province_of_Destination__c!=null)
            {
                if(selectedStateId == nextQuestionOnThisOption.State_Province_of_Destination__c)
                {
                    //static option name and value field values are set here
                    psw.currentQuestion.psqOptions[i].Name = psw.currentQuestion.SelectedOptionText;
                    psw.currentQuestion.psqOptions[i].Value__c = psw.currentQuestion.SelectedOption;
                    //psw selectedOption set here
                    psw.currentQuestion.psqSelectedOption = psw.currentQuestion.psqOptions[i];
                    return true;
                }   
            }
            else if(nextQuestionOnThisOption.Country_Group__c==null && nextQuestionOnThisOption.State_Province_of_Destination__c==null)
            {
                	//static option name and value field values are set here
                    psw.currentQuestion.psqOptions[i].Name = psw.currentQuestion.SelectedOptionText;
                    psw.currentQuestion.psqOptions[i].Value__c = psw.currentQuestion.SelectedOption;
                    //psw selectedOption set here
                    psw.currentQuestion.psqSelectedOption = psw.currentQuestion.psqOptions[i];
                    return true;
            }
        }
        return false;
       
    }
    
    //PQS Dynamic options to move for Activity Processor - Apex based options
    public List<SelectOption> loadOptions(EFLPreScreeningWrapper psw){
		return EFLPSQUtility.getStateOfDestination();
    }
}