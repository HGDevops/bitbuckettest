/**
 * Purpose: Handles the clone Functionality
**/ 
public with sharing class EFLCloneApplicationController{
    
   private ApexPages.StandardController controller {get; set;}
   @TestVisible private application__c CurrentApp {get;set;}
    public EFLCloneApplicationController(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
         CurrentApp = (application__c)controller.getRecord();
    }
    
    public PageReference cloneApplication() {
        
            application__c newapp;
           // EFLCloneApplicationUtility createapp = new EFLCloneApplicationUtility();
            EFLCloneApplicationHandler createapp = new EFLCloneApplicationHandler();
        
             Savepoint sp = Database.setSavepoint();
        try{
           newapp = New Application__c(); 
           newapp = createapp.CloneApplication(CurrentApp.id,NULL,'Clone Button');  
                        
        }catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }
          return new PageReference('/'+newapp.id);
    }
    
    //Go Back to Application
    public PageReference goBackApplication()
    {
        PageReference applicationDetailsRedirect = new PageReference('/' + CurrentApp.id);
        applicationDetailsRedirect.setRedirect(true);
        return applicationDetailsRedirect;
    }

}