//**IN PROGRESS: Consolidating both location trigger handlers **//
public inherited sharing class EFLLocationTriggerHandler implements EFLITrigger {
    public static Boolean TriggerDisabled = false;
    public static Set<Id> lineItemIds {get; set;}
    public static Map<Id, String> lineItemIdNameMap {get; set;}    
    string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
    string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
    string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
    string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();  
    
    public Boolean IsDisabled(){
        if (EFLGenericUtility.isTriggerDisabled('CARPOL_BRS_LocationsTrigger'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    public EFLLocationTriggerHandler(){
        
    }
    
    public void bulkBefore(){           
        list<Location__c> lstloc  = trigger.new;
        list<string> lstall = new list<string>();
        
        for(Sobject l:Trigger.New){
            Location__c loc = (Location__c)l;
            if(loc.Line_Item__c!=null ){
                lstall.add(loc.Line_Item__c);
            }
        }
        /* // KK: Commented this as its hitting validations acros multiple areas
        for(AC__c ac:[select id,Name,RecordType.Name,Status__c,Purpose_of_the_Importation__c,
                      (select id,RecordType.Name from locations__r) 
                      from AC__c 
                      where id in:lstall]){
                          if(ac.Status__c=='Submitted')
                          {
                              lstloc[0].addError('Application is submitted you cannot insert new Location');
                          } 
                          
                      } */
        
        validate(Trigger.New, Trigger.Old); 
    }
    
    public void bulkAfter(){
        if (Trigger.isDelete){
            //processLocationReadiness(Trigger.Old); 
        } else {
            //processLocationReadiness(Trigger.New); 
        }        
    }
    
    public void beforeInsert(SObject so){
    }
    
    public void beforeUpdate(SObject oldSo, SObject so){
    }
    
    public void beforeDelete(SObject so){
    }
    
    public void afterInsert(SObject so){
    }
    
    public void afterUpdate(SObject oldSo, SObject so){
    }
    
    public void afterDelete(SObject so){
    }
    
    public void andFinally(){
    }
    
    public static void validate(List<Location__c> newLocations, List<Location__c> oldLocations){
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Location__c.getRecordTypeInfosByName();
        String USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;  
        
        lineItemIds = new Set<Id>();
        
        for (Location__c loc : newLocations){
            lineItemIds.add(loc.Line_Item__c);
        }
        
        lineItemIdNameMap = new Map<Id,String>();
        
        lineItemIdNameMap = movementByLineItems(lineItemIds);
        
        validateRequiredFields(newLocations,rtMapByName);
        validateFieldFormat(newLocations,USCountryID);
        validateRecord(newLocations,rtMapByName,USCountryID);
    }
    
    public static void validateRequiredFields(List<Location__c> locations, Map<String,Schema.RecordTypeInfo> rtMapByName){     
        
        //W-031437 - Required Field Validation Fix
        id originRtId = rtMapByName.get('Origin Location').getRecordTypeId();
        id destinationRtId = rtMapByName.get('Destination Location').getRecordTypeId();
        id originDestinationRtId = rtMapByName.get('Origin and Destination Location').getRecordTypeId();
        id releaseRtId = rtMapByName.get('Release Sites Location').getRecordTypeId();
        
        string requiredMessage = 'You must enter a value';
        
        for (Location__c loc : locations){
            
            // Name required field validation accross all movement type and location record type
            if (loc.Name == null){
                loc.Name.addError(requiredMessage);
            }
            
            //Country required field validation accross all movement type and location record type
            if (loc.Country__c == null){
                loc.Country__c.addError(requiredMessage);
            }
            
            //I - Import, IM - Interstate Movement, IMR - Interstate Movement and Release & R - Release
            string movementType = '';
            if(lineItemIdNameMap.get(loc.Line_Item__c)=='Import'){movementType = 'I';}
            if(lineItemIdNameMap.get(loc.Line_Item__c)=='Interstate Movement'){movementType = 'IM';}
            if(lineItemIdNameMap.get(loc.Line_Item__c)=='Interstate Movement and Release'){movementType = 'IMR';}
            if(lineItemIdNameMap.get(loc.Line_Item__c)=='Release'){movementType = 'R';}
            
            //O - Origin, D - Destination, OD - Origin Destination & R - Release
            string recordType = '';
            if (loc.RecordTypeId == originRtId){recordType = 'O';}
            if (loc.RecordTypeId == destinationRtId){recordType = 'D';}
            if (loc.RecordTypeId == originDestinationRtId){recordType = 'OD';}
            if (loc.RecordTypeId == releaseRtId){recordType = 'R';}
            
            //State and County Required Validations 
            //system.debug('movementType@@'+ movementType);
            //system.debug('recordType@@'+ recordType);
            
            if(isStateCountyRequired(movementType, recordType)==true)
            {
                //system.debug('isStateCountyRequired@@');
                if(loc.State__c==null)
                {
                    //system.debug('StateRequired@@');
                    loc.State__c.addError(requiredMessage);
                }
                if(loc.Level_2_Region__c==null)
                {
                    //system.debug('CountyRequired@@');
                    loc.Level_2_Region__c.addError(requiredMessage); 
                }                
            }
            
            //Previously APHIS Inspected by Required Validation
            if ((recordType == 'D' || recordType == 'OD') && loc.Inspected_by_APHIS__c == null){
                loc.Inspected_by_APHIS__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorRequiredAphis'));
            }
            
            //Release Special Fields Required Validation
            if(isReleaseFieldsRequired(movementType, recordType)==true)
            {
                //Location Unique Field Required Validation 
                if(loc.Location_Unique_Id__c==null)
                {
                    loc.Location_Unique_Id__c.addError(requiredMessage); 
                }
                //Proposed Planting Required Validation 
                if(loc.Proposed_Planting__c==null)
                {
                    loc.Proposed_Planting__c.addError(requiredMessage); 
                }
                //Number of Acres Required Validation 
                if(loc.Number_of_Acres__c==null)
                {
                    loc.Number_of_Acres__c.addError(requiredMessage); 
                } 
                //Critical Habitat Involved Required Validation 
                if(loc.Critical_Habitat_Involved__c==null)
                {
                    loc.Critical_Habitat_Involved__c.addError(requiredMessage); 
                }
                //Critical Habitat Involved - If Yes Please Explain Required Validation 
                if(loc.Critical_Habitat_Involved__c=='Yes' && loc.If_Yes_Please_Explain__c==null)
                {
                    loc.If_Yes_Please_Explain__c.addError('Provide details about the critical habitat.'); 
                }
                //Release History Required Validation 
                if(loc.Release_History__c==null)
                {
                    loc.Release_History__c.addError(requiredMessage); 
                } 
            }
            
        }
    }
    
    public static void validateFieldFormat(List<Location__c> locations, String usCountryId){     
        //system.debug('Inside the validateFieldFormat() for EFLLocationTriggerHandler');
        for (Location__c loc : locations){
            if (loc.Country__c == usCountryId && loc.Zip__c != null ){
                Pattern zipcodePattern = pattern.compile('[0-9]{5}(?:-[0-9]{4})?$');
                Matcher zipcodeMatcher = zipcodePattern.matcher(loc.Zip__c);
                if (!zipcodeMatcher.matches()){
                    loc.Zip__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorFailZip'));
                }                
            }
        }
    }
    
    public static void validateRecord(List<Location__c> locations, Map<String,Schema.RecordTypeInfo> rtMapByName, String usCountryId){
        
        //get release site locations  related to same application
        Map<String,Location__c> lineitemIDLocationUniqueIdMap = new Map<String,Location__c>();
        
        for (Location__c loc : [Select Id,Location_Unique_Id__c,Line_Item__r.Id,Line_Item__r.Movement_Type__c, Line_Item__c from Location__c 
                                where (RecordTypeId =:rtMapByName.get('Release Sites Location').getRecordTypeId() and Location_Unique_Id__c != null )
                                and  Line_Item__c in :lineItemIds]){
                                    lineitemIDLocationUniqueIdMap.put(loc.Line_Item__c + loc.Location_Unique_Id__c, loc);
                                }
        for (Location__c loc : locations){
            //system.debug('usCountryId@@'+ usCountryId);
            if (loc.Country__c == usCountryId && loc.RecordTypeId == rtMapByName.get('Origin Location').getRecordTypeId() && lineItemIdNameMap.get(loc.Line_Item__c) == 'Import'){
                //system.debug('InsideCountry@@Loop');
                loc.Country__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorCountryNotUS'));
            }
            if (loc.Proposed_Planting__c != null && !(loc.Proposed_Planting__c > 0 && loc.Proposed_Planting__c <= 99)){
                loc.Proposed_Planting__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorProposedReleases'));
            }
            if (loc.Number_of_Acres__c != null && loc.RecordTypeId == rtMapByName.get('Release Sites Location').getRecordTypeId() && !(loc.Number_of_Acres__c  >= 0.001 && loc.Number_of_Acres__c  < 1000) ){
                loc.Number_of_Acres__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorAcreage'));
            }
            
            if (loc.Location_Unique_Id__c != null && loc.RecordTypeId == rtMapByName.get('Release Sites Location').getRecordTypeId() )
            {     
                //Alphanumeric and dash validation
                Pattern uniqIdPattern = pattern.compile('[A-Za-z0-9-]+');
                Matcher uniqIdMatcher = uniqIdPattern.matcher(loc.Location_Unique_Id__c);
                if (!uniqIdMatcher.matches()){
                    loc.Location_Unique_Id__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorSiteIdInvalid'));
                }
                
                //Unique Validation    
                string currentLocLineItemUniqueId = loc.Line_Item__c + loc.Location_Unique_Id__c; 
                if (lineitemIDLocationUniqueIdMap.get(currentLocLineItemUniqueId)!=null){ 
                    //insert scenario
                    If(loc.id==null)
                    {
                        loc.Location_Unique_Id__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorSiteIdUnique'));    
                    }
                    //update scenario 
                    else
                    {
                        if(loc.id != lineitemIDLocationUniqueIdMap.get(currentLocLineItemUniqueId).id)
                        {
                            loc.Location_Unique_Id__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorSiteIdUnique'));   
                        }
                    }
                }
                
            }
        }        
    }
    
    public static Map<Id,String> movementByLineItems(Set<Id> lineItemIds)
    {
        Map<Id,String> lineItemIdNameMap = new Map<Id,String>();
        for (AC__c lineItem : [Select Id,Name,Movement_Type__c from AC__c where Id in :lineItemIds]){
            lineItemIdNameMap.put(lineItem.Id,lineItem.Movement_Type__c);
        }
        return lineItemIdNameMap;
    }
    
    public static boolean isStateCountyRequired(string movementType, string recordType)
    {
        //I - Import, IM - Interstate Movement, IMR - Interstate Movement and Release & R - Release
        //O - Origin, D - Destination, OD - Origin Destination & R - Release
        boolean isRequired = false;
        if((recordType == 'O' && movementType != 'I') || recordType == 'D' || recordType == 'OD' || recordType == 'R'){
            isRequired = true;
        } 
        //system.debug('isRequired@@'+ isRequired);
        return isRequired;
        
    }
    
    public static boolean isReleaseFieldsRequired(string movementType, string recordType)
    {
        //I - Import, IM - Interstate Movement, IMR - Interstate Movement and Release & R - Release
        //O - Origin, D - Destination, OD - Origin Destination & R - Release
        boolean isRequired = false;
        if((movementType == 'IMR' || movementType == 'R') && (recordType == 'R')){
            isRequired = true;
        } 
        return isRequired;
    }
    
    public static void processLocationReadiness(List<location__c> locationList){
        list<ID> BRSlineitemlst = new list<ID>();
        list<AC__c> listlineitemToUpdate = new list<AC__c>();
        list<AC__c> lineitemlistafter = new list<AC__c>();
        map<id,list<material__c>> locMaterialMap = new map<id,list<material__c>>();
        map<id,list<GPS_Coordinate__c>> gpsCoordinatesMap = new map<id,list<GPS_Coordinate__c>>();
        string incidentrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Incident Location').getRecordTypeId();
        string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();  
        boolean incidentrectype;
        boolean olocrectype;
        boolean rslocrectype;
        boolean dlocrectype;
        boolean oanddlocrectype;
        boolean materialExists,gpsExists;
        Integer oanddCount = 0;
        
        List<Id> objLineItemIds = new List<Id>();
        for(Location__c obj : locationList){
            objLineItemIds.add(obj.Line_Item__c);
        } 
        
        
        for(Location__c l:[select id,Line_Item__c from Location__c where id IN:locationList])                          
        {
            BRSlineitemlst.add(l.Line_Item__c); 
        }
        
        
        set<id> locationIdSet = new set<id>();
        if(!BRSlineitemlst.isEmpty()){
            lineitemlistafter = [SELECT ID,RecordType.Name,movement_type__c,Type_of_Permit__c,Location_Status__c ,status__c,Construct_Status__c,SOP_Status__c,Regulated_Article_Status__c,(select id,recordtypeid from Locations__r) From AC__c WHERE ID in: BRSlineitemlst];
        }else{
            lineitemlistafter = [SELECT ID,RecordType.Name,movement_type__c,Type_of_Permit__c,Location_Status__c ,status__c,Construct_Status__c,SOP_Status__c,Regulated_Article_Status__c,(select id,recordtypeid from Locations__r) From AC__c WHERE ID in: objLineItemIds];
        }
        
        for(AC__c a : lineitemlistafter){
            if(!a.Locations__r.isEmpty()){
                for(Location__c lc:a.Locations__r){
                    locationIdSet.add(lc.id);
                }                
            }
        }
        
        for(Location__c l:[select id,Line_Item__c,(select id,name from Materials__r),(select id,name from GPS_Coordinates__r) from Location__c where id IN:locationIdSet])                          
        {
            locMaterialMap.put(l.id,l.Materials__r);
            gpsCoordinatesMap.put(l.id,l.GPS_Coordinates__r);
        }
        
        
        for (AC__c a : lineitemlistafter) {
            olocrectype = false;               //origin location
            oanddlocrectype = false;           // origin and destination
            dlocrectype = false;               //destination
            rslocrectype = false;              // release
            materialExists = true;
            gpsExists = true;
            if(a.Locations__r!=null && a.Locations__r.size() > 0){
                for(Location__c lc:a.Locations__r){
                    if(lc.recordtypeid == olocrectypeid)
                        olocrectype = true;
                    if(lc.recordtypeid == oanddlocrectypeId){
                        oanddlocrectype = true;
                        oanddcount++;
                    }
                    if(lc.recordtypeid == dlocrectypeId)
                        dlocrectype = true;
                    if(lc.recordtypeid == locrectypeid)
                        rslocrectype = true;
                    if(lc.recordtypeid==incidentrectypeid)
                        incidentrectype = true;
                    
                    
                    if((lc.recordtypeid == dlocrectypeId || lc.recordtypeid == oanddlocrectypeId ) && materialExists){
                        if((locMaterialMap.get(lc.id)!=null && locMaterialMap.get(lc.id).size()>0) ){
                            materialExists = true;  
                        }else{
                            materialExists = false;
                        }
                    }
                    if(lc.recordtypeid == locrectypeid && gpsExists){
                        if((gpsCoordinatesMap.get(lc.id)!=null && gpsCoordinatesMap.get(lc.id).size()>0) ){
                            gpsExists = true;
                        }else{
                            gpsExists = false;
                        }
                    }
                }
                if(a.Movement_Type__c=='Import' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')&& (dlocrectype == true && olocrectype==true) && materialExists){
                    a.Location_Status__c = 'Ready to Submit';
                    listlineitemToUpdate.add(a);  
                }
                else if(a.Movement_Type__c=='Interstate Movement' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')
                        &&(
                            (dlocrectype == true && olocrectype==true) || 
                            (dlocrectype == true && oanddlocrectype==true) ||
                            (olocrectype==true && oanddlocrectype==true) ||  
                            (oanddlocrectype==true && oanddcount >=2)
                        )
                        && materialExists){
                            a.Location_Status__c = 'Ready to Submit';
                            listlineitemToUpdate.add(a);  
                        }
                else if(a.Movement_Type__c=='Interstate Movement and Release' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')
                        &&(
                            (dlocrectype == true && olocrectype==true) || 
                            (dlocrectype == true && oanddlocrectype==true) ||
                            (olocrectype==true && oanddlocrectype==true) ||  
                            (oanddlocrectype==true && oanddcount >=2)
                        )
                        && rslocrectype && materialExists && gpsExists){
                            a.Location_Status__c = 'Ready to Submit';
                            listlineitemToUpdate.add(a);  
                        } else if(a.Movement_Type__c=='Release' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')
                                  && (rslocrectype && gpsExists)){
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a);  
                                  }else {
                                      a.Location_Status__c = 'Yet to Add';
                                      listlineitemToUpdate.add(a);
                                  }
            }else{
                a.Location_Status__c = 'Yet to Add';
                listlineitemToUpdate.add(a);
            }
            
        }
        
        if(listlineitemToUpdate!=null && listlineitemToUpdate.size()>0){
            checkForStatus(listlineitemToUpdate);
            update listlineitemToUpdate;
        }                             
    }
    
    public static void checkForStatus(List<AC__c>  lineItemlist){
        
        for(AC__c lineItem: lineItemlist){
            if(lineItem.status__c != 'Waiting on Customer' && lineItem.status__c != 'Submitted')
            { 
                if( lineItem.Construct_Status__c == 'Ready to Submit' && lineItem.SOP_Status__c== 'Ready to Submit' && 
                   lineItem.Regulated_Article_Status__c == 'Ready to Submit' && lineItem.Location_Status__c== 'Ready to Submit')                     
                {
                    if (!(lineItem.Status__c == 'Voided' || lineItem.Status__c == 'No jurisdiction' || lineItem.Status__c == 'Permit not required'  || lineItem.Status__c == 'Draft' || lineItem.status__c == 'Waiting on Customer' || lineItem.status__c == 'Cancelled'|| lineItem.status__c == 'Disapproved' || lineItem.status__c == 'Withdrawn')){ //12/18/17 VV Updated to add Withdrawn per US-17053    
                        
                        lineItem.Status__c = 'Ready to Submit';
                    }
                }
                else 
                {
                    if(lineItem.Status__c!='Draft') 
                        lineItem.Status__c = 'Saved';  
                    
                } 
            }
        }
    }
    
}