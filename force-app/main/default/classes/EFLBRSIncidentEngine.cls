public with sharing class EFLBRSIncidentEngine 
                    implements EFLIIncidentEngine {
  
/*
* Purpose: load Activities 
*/
    public void loadActivities(incident__c incidentRecord,list<sObject> activityList){
        
        //Populate Next Tasks and increment Sequence of inspection
        
        list<team__c> teamMembersList = new list<team__c>();
        
        teamMembersList = EFLTeamRepository.selectByIncidentID(incidentRecord.ID); 
        ID ownerID;
        
        system.debug('team member list: ' + teamMembersList);
                
        if(!teamMembersList.isEmpty()){
            system.debug('incidentRecord '+ incidentRecord);
            integer taskSequence = incidentRecord.Activity_Sequence__c==null?1:incidentRecord.Activity_Sequence__c.intvalue();
            system.debug('taskSequence '+ taskSequence);
            Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(incidentRecord.stage__c, incidentRecord.Workflow_Number__c,taskSequence + 1);
            system.debug('currentActivityCMD '+ currentActivityCMD);
           
                if(currentActivityCMD != NULL )
                {                
                for(team__c t : teamMembersList){
                        system.debug('EachTeamMember *** ' + t);
                    if(currentActivityCMD.Assigned_To__c == t.member_role__c){
                        system.debug('EachTeamMember Inside Loop *** ' + t);
                        ownerID = t.Member__c;
                        break;
                    }
                } 
                if(ownerID == null){ 
                    
                    trigger.new[0].addError('Please Assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
                }
                activityList.add(EFLActivityUtility.populateTask( incidentRecord, currentActivityCMD, ownerID ));
                system.debug('Before Activity_Sequence__c '+incidentRecord.Activity_Sequence__c);
                incidentRecord.Activity_Sequence__c = incidentRecord.Activity_Sequence__c + 1;
                system.debug('After Activity_Sequence__c '+incidentRecord.Activity_Sequence__c);
                    system.debug('activityList '+activityList);
            }       
        }
        
        else{
               trigger.new[0].addError('Please assign Team Members.');            
        }
    }
   

}