@isTest
public class EFLRelatedRecordTriggerTest {
    public static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    public static Regulated_Article__c regArt;
    public static Construct__c testConstruct;   
    public static Construct__c testConstruct1;
    public static AC__c ac1;
    Public static GenotypeType__c genoTypeType;
    Public Static Genotype__c genoType; 
    private static id olocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin');
    private static id dlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Destination');
    private static id oanddlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin and Destination');
    private static id releaserectypeid=EFLGenericUtility.getrecordtypeId('Location Release Sites');

    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static Id RtId;
    static Location__c loc;
    
    @testSetup
    static void setupData(){
        testData.insertcustomsettings();
        Application__c objApp = testData.newapplication();
        Authorizations__c auth = new Authorizations__c();
        auth = testData.newAuth(objApp.id);
        ac1=testData.newLineItemBRS('Personal Use',objApp);
        ac1.Type_of_Permit__c = 'Standard Permit';
        update ac1;
        Program_Line_Item_Pathway__c plip=testData.newCaninePathway();
        regArt=new  Regulated_Article__c();
        regArt.Name='Test article';
        regArt.RecordTypeID=Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        regArt.Status__c='Active';
        regArt.Category_Group_Ref__c=testdata.newgroup().Id;
        regArt.Program_Pathway__c=plip.id;
        Insert regArt; 
        
        testConstruct = testData.newconstruct(ac1.Id,regArt);
        testConstruct1 = testData.newconstruct(ac1.Id,regArt);
        
        country = testData.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testData.newlevel2region(lr.id);

        loc = testData.newlocation(country.id,lr.id,lvl2Reg.id,ac1.id,olocrectypeid);
        
        Report_Summary__c rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        //rs.Report_Type__c = 'Planting Report';
        rs.Report_Type__c = 'Planting/Release Reports';
        rs.Status__c = 'UnSubmitted';
        insert rs;        
      
        
        Id prrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
        Self_Reporting__c sr1 = new Self_Reporting__c();            
        sr1.RecordTypeID =  prrectypeid;
        sr1.Start_Date__c = Date.valueof('2010-03-27');
        sr1.Release_Record_ID__c = loc.Id; //record ID for Dallas
        sr1.Report_Summary__c = rs.Id; //record ID for RS-00001012
        sr1.Planting_ID__c = loc.Id;
        sr1.Quantity_Acres__c = 1;
        insert sr1;
        System.debug('self report in test classs>>'+ sr1);
        
        //Create EFL_Related_Record__c dummy records
        Id constructrectypeid = Schema.SObjectType.EFL_Related_Record__c.getRecordTypeInfosByName().get('Construct').getRecordTypeId();
        EFL_Related_Record__c relRec = new EFL_Related_Record__c();
        relRec.RecordTypeId =constructrectypeid;
        relRec.Self_Reporting__c=sr1.Id;
        relRec.Construct__c=testConstruct.Id;
        insert RelRec;
        
        
        
        
        GPS_Coordinate__c gpsRec = new GPS_Coordinate__c();
        gpsRec.Self_Reporting__c = sr1.Id;
        gpsRec.Location__c=loc.Id;
        insert gpsRec;
        
        //Create EFL_Related_Record__c dummy records
        EFL_Related_Record__c relRec1 = new EFL_Related_Record__c();
        relRec1.RecordTypeId =constructrectypeid;
        relRec1.Self_Reporting__c=sr1.Id;
        relRec1.Construct__c=testConstruct1.Id;
        insert RelRec1;
     
    }
    //This test method creates several Self_Reporting__c dummy records with two Self_Reporting__c records containing the same custom fields Release_Record_Id__c and Start_Date__c
    @isTest
    static void validateUniqueEFLRelatedRecordTest() {
        System.debug('EFL_RelatedRecordTriggerTest: Start of test method validateUniqueEFLRelatedRecord().');
        System.debug('EFL_RelatedRecordTrigger: Creating EFL_RelatedRecord dummy records.');
        
        //Begin test   
        Test.startTest();
            System.debug('EFL_Related_Record__c: Test execution context has begun.');             
            List<Self_Reporting__c> srList = [Select Id from Self_Reporting__c limit 10];
        
            System.debug('EFL_RelatedRecordTriggerTest: Test execution context has ended.' + srList);
        Test.stopTest();
    
    }//End test method
     @isTest
    static void validateEFLRelatedRecordDeletionTest() {
         
        //Begin test   
        Test.startTest();
            System.debug('EFL_Related_Record__c: Test execution context has begun.');
            //Insert the Self_Reporting__c records into the database
                List<EFL_Related_Record__c> eflList = [Select Id from EFL_Related_Record__c limit 10];
                if(eflList.size()>0){
                    delete eflList;
                }
        Test.stopTest();
        
     }//End test method 
    
} //End class declaration