@isTest
private class LineItemChangeHistoryHandler_test {
    
    private static List<sObject> inputs = new List<sObject>();

    @isTest
    static void  testpopulateNewRecordDetails()
    {
   
        CARPOL_AC_TestDataManager acDataMgr = new CARPOL_AC_TestDataManager();
        acDataMgr.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = acDataMgr.newapplication();
        
        AC__c lineItem = acDataMgr.newlineItem('Adoption', app);
        
        Change_History__c change = new Change_History__c();
        
        string fieldName  = 'Name';
        string fieldLabel = 'Name';
        LineItemChangeHistoryHandler testing =  new LineItemChangeHistoryHandler();
        List<AC__c> Linelist = new List<AC__c>();
        Linelist = [SELECT name, id, authorization__c,Application_Number__c FROM AC__c WHERE Id=: lineItem.Id];
        lineList.add(lineList[0].clone(false));
        Test.startTest();  
        testing.populateNewRecordDetails(Linelist[0],change);

       
        testing.populateDeletedRecordDetails(Linelist[0],change);
        testing.populateUpdateDetails(Linelist[0],lineList[1],change);
        Test.stopTest(); 
        system.assert(change.New_Entry__c=true);
        system.assert(change.name==fieldLabel);
        system.assert(change.Field_API_Name__c !=null);
        system.assert(change.Line_Item__c !=null);
        system.assert(change.Application__c!=null);
        
    }    
    

}