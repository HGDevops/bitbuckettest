public interface EFLIWizardFlowEngine {
    
    void getNextQuestion(EFLWizardFlow wizardFlow);
    void getPrevQuestion(EFLWizardFlow wizardFlow);
    List<SelectOption> getQuestionOptions (EFLWizardFlow wizardFlow);
    void setSelectedOption ( string selectedOption);
    
}