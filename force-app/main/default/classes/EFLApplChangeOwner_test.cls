@istest
public class EFLApplChangeOwner_test{
    private testmethod static void testchangeowner(){
        CARPOL_BRS_TestDataManager  testdatamanager = new CARPOL_BRS_TestDataManager();
        testdatamanager.insertcustomsettingsWithBRSTriggerDisabled();
        String AccountRecordTypeId = testDatamanager.AccountRecordTypeId;
        account accnt = testdatamanager.newAccount(AccountRecordTypeId );
        contact cont1 = testdatamanager.newContact();
        application__c appln = testdatamanager.newapplication();
        appln.Applicant_Name__c = cont1.id;
        update appln;
        System.Debug('Account Created>>>'+accnt);
        contact cont2 = new contact(lastname = 'TestLast', firstname = 'TestFirst', accountid = accnt.id);
        insert cont2;
        test.starttest();
        system.debug('>>>> appId= '+appln.id);
        test.setcurrentpage(page.EFLApplChangeOwner);
        ApexPages.StandardController sc = new ApexPages.StandardController(appln);
        EFLApplChangeOwner inst = new EFLApplChangeOwner(sc);
        appln.Applicant_Name__c = cont2.id;
        system.debug('>>>> cont2-id = '+cont2.id);
        inst.currApp = appln;
        inst.save();
        inst.cancel();
        test.stoptest();
    }
}