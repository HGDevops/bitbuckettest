/*
Author:  Dawn Sangree
Creation Date: 4-11-2017
Usage:  Replaces SOQL chaining in external and internal wizard code by getting all possible matching wizard questions based on inputs and ranks them to determine the right one to use.

Single public class getWizardStartQA
variables:
  Program_Line_Item_Pathway__c: SelectedPathwayId
  Movement_Type__c: sSelectedLandOpt
  Type__c: Static, we are always looking for a start question so TYPE Like Start%
  Regulated_Article__c: fieldAnswerMap.get('Scientific_Name__c')
  Regulated_Article_Group__c: FinalRAGroupIds
  //TODO: Consolidate in main wizard code to use sSelCountry, remove setCountries if it's not needed
  Country of Origin: setCountries (should use sSelCountry)
  Country_Of_Origin_Not_Equal_To__c: sSelCountry
  Country_Group__c: :CountryGroupIDs
  Country_of_Destination__c: sSelCountryExport
  Level_1_Region__c (State of Destination): sSelReg1
  State_Province_of_Destination__c: sSelDestState
  Intended_Use__c: questionId (IS THIS CORRECT?)
  Component__c: sSelComponent
  EFLDiseaseSensitivity__c: list of diseases passed in
  //Not currently wired in (do not include for now but note they exist)
  Port of Entry
  Port of Exit
  Country of Transit
  //TODO: There will be new ones needed to support CITIES
  Country of Export and Regulated Article - Scientific Name  
*/
//wizardQARecord = CARPOL_UNI_WizardQA.getWizardStartQA(SelectedPathwayId, sSelectedLandOpt, fieldAnswerMap.get('Scientific_Name__c'), idDP,  String selectedIntendedUseID, CountryGroupIDs, setCountries, fieldAnswerMap.get('State_Territory_of_Destination__c'), sSelDestState, FinalRAGroupIds, , sSelComponent);

public with sharing class EFLgetWizardStartQA{
    //variables: Pathway, Movement Type, Type of Start/Endpoint, Regulated Article, Regulated Article Group, Selected Country of Origin Set, Country of Origin String, Country Group, Country of Destination, State of Destination, Intended Use, Component, Disease Sensitivity, Do we need to test for disease flag
    public static Wizard_Questionnaire__c getWizardStartQA(String SelectedPathwayId, String sSelectedLandOpt, String sSelRegArt, 
        Set<ID> FinalRAGroupIds, String sSelCountry, Set <ID> CountryGroupIDs, String sSelCountryExport, String sSelReg1, 
        String sSelDestState, String questionId, String sSelComponent, Set<String> cntryorigdiseases, Boolean diseaseValidationRequired){
 
        //TODO:Possible use a map to keep track of what came in with values instead of all the null or nothing code
        System.debug('Required to check diseases >>>>> ' + diseaseValidationRequired + 'And the diseases are: ' + cntryorigdiseases);
        System.debug('regulated artcile group: ' + FinalRAGroupIds);
        
        if(questionId != null | questionId != ''){
            try{
                Wizard_Questionnaire__c temp = [SELECT ID, Name, Type__c,Type_of_Permit__c, Type_of_Options__c,State_of_Destination_Not_Equal_To__c, Question__c, 
                    Signature__c, Document_Linked__c, link__c , Level_1_Region__c,Child_Program_Pathway__c from Wizard_Questionnaire__c WHERE status__c = 'Active'  and ID =:questionID LIMIT 1];
                return temp;
            }
            catch(Exception e){
                System.debug('Reached the first catch block ' + 'Question Id: ' + questionId +'   '+ e.getMessage());
                
            }
        }    
        //Start string for dynamic SOQL.
        //TODO:  Why returning State_of_Destination_Not_Equal_To__c and Level_1_Region__c?  Can these be removed from returned fields?
        String strWQQry = 'SELECT ID, Name, Regulated_Article__c, Regulated_Article_Group__c, Country_of_Origin__c, Country_Group__c, Country_of_Destination__c, Level_1_Region__c, '
                + 'State_Province_of_Destination__c, Intended_Use__c, Component__c, Type__c, Type_of_Options__c,State_of_Destination_Not_Equal_To__c, Question__c, Signature__c,Document_Linked__c,'
                + 'Type_of_Permit__c , link__c,Child_Program_Pathway__c';
                
        if(diseaseValidationRequired)
            strWQQry += ', EFLDiseaseSensitivity__c from Wizard_Questionnaire__c';
        else
            strWQQry += ' from Wizard_Questionnaire__c';
        
        //Standard items we must always include in WHERE clause
        strWQQry += ' WHERE (Status__c = \'Active\') AND (Program_Line_Item_Pathway__c=:SelectedPathwayId) AND (Movement_Type__c includes (:sSelectedLandOpt)) AND (Type__c LIKE \'Start%\')'; 
    
        //Build out the rest of the dynamic SOQL using expected inputs
        //if we collected a value we want records where that value exists in the field or else it was blank
        //if we didn't collect a value at all we want records where that field is specifically blank
        System.debug('CAME EFLgetWizardStartQA');
        
        //Regulated Article
        if(sSelRegArt == null | sSelRegArt == ''){
            strWQQry += ' AND Regulated_Article__c = null';
        } else {
            strWQQry += ' AND (Regulated_Article__c =: sSelRegArt OR Regulated_Article__c = null)';
        }
        
        //Regulated Article Groups
        if(FinalRAGroupIds.size() < 1){
            strWQQry += ' AND Regulated_Article_Group__c = null';
        } else {
            strWQQry += ' AND (Regulated_Article_Group__c in: FinalRAGroupIds OR Regulated_Article_Group__c = null)';
        }       
    
        //Country of Origin
        if(sSelCountry== null | sSelCountry == ''){
            strWQQry += ' AND Country_of_Origin__c = null';
        } else {
            strWQQry += ' AND (Country_of_Origin__c =: sSelCountry OR Country_of_Origin__c = null)';
        }    
    
        //Only one where the negative will cause a record to be kicked out of the list of results returned
        //Country Of Origin Not Equal To
        if(sSelCountry == null | sSelCountry == ''){
            strWQQry += ' AND Country_Of_Origin_Not_Equal_To__c = null';
        } else {
            strWQQry += ' AND (Country_Of_Origin_Not_Equal_To__c !=: sSelCountry OR Country_Of_Origin_Not_Equal_To__c = null)';
        }        
    
        //Country Group
        if(CountryGroupIDs.size() < 1){
            strWQQry += ' AND Country_Group__c = null';
        } else {
            strWQQry += ' AND (Country_Group__c in: CountryGroupIDs OR Country_Group__c = null)';
        }    
    
        //Country of Destination
        if(sSelCountryExport == null | sSelCountryExport == ''){
            strWQQry += ' AND (Country_of_Destination__c = null)';
        } else {
            strWQQry += ' AND (Country_of_Destination__c =: sSelCountryExport or Country_of_Destination__c = null)';
        }
      
        // Level 1 Region (State of Destination)
        if(sSelReg1 == null | sSelReg1 == ''){
            strWQQry += ' AND (Level_1_Region__c = null)';
        } else {
            strWQQry += ' AND (Level_1_Region__c =: sSelReg1 or Level_1_Region__c = null)';
        }
    
        //TODO: State Province of Destination
        if(sSelDestState == null | sSelDestState == ''){
            strWQQry += ' AND (State_Province_of_Destination__c = null)';
        } else {
            strWQQry += ' AND (State_Province_of_Destination__c =: sSelDestState or State_Province_of_Destination__c = null)';
        }
    
        // TODO: Intended_Use__c
        if(questionId == null | questionId == ''){
            strWQQry += ' AND (Intended_Use__c = null)';
        } else {
            strWQQry += ' AND (Intended_Use__c =: questionId or Intended_Use__c = null)';
        }
        
        //Even though we want to collect a regulated article before we collect component, that business logic is handled on the interface
        //For the purpose of this work we don't care if the ra was collected or not, if we collected a component we want to return a record
        //TODO: Component
        if(sSelComponent == null | sSelComponent == ''){
            strWQQry += ' AND (Component__c = null)';
        } else {
            strWQQry += ' AND(Component__c =: sSelComponent or Component__c = null)';
        }
        
        //TODO: EFLDiseaseSensitivity__c
        
        //TODO: Execute the Dynamic SOQL
        List<Wizard_Questionnaire__c> wizardQuestions;
        List<Wizard_Questionnaire__c> wizardQsAfterDiseases;
        try{
            System.debug('This is the query string: \n' + strWQQry);
            wizardQuestions = Database.query(strWQQry);
        }catch(Exception e){System.debug('Reached the Catch block---------> ' + e.getMessage() );}
            
        //TODO:  Verify we returned at least one record, if we returned only one, skip ranking and send it back to the interface
        if(wizardQuestions != null && wizardQuestions.size() > 1){
            
            //Ranking Logic
            System.debug('Found many Wizard Question Record');
            if(diseaseValidationRequired){
                wizardQsAfterDiseases = getWizardListAfterDiseases(wizardQuestions, cntryorigdiseases);
                return RankingLogic(wizardQsAfterDiseases, diseaseValidationRequired);
            }
            else
                return RankingLogic(wizardQuestions, diseaseValidationRequired);
            
        }else if(wizardQuestions != null && wizardQuestions.size() == 1){
            
            System.debug('Found exactly 1 Wizard Question Record');
            return wizardQuestions[0];
        
        }else{
            System.debug('Found NO Wizard Question Records');
            return new Wizard_Questionnaire__c(Question__c = 'Something went wrong, please contact your administrator!'); //will add attributes later
        }
        
        //TODO:  A note about the above, we have been discussing there being a default question on each pathway such that the text on it would state something like, there has been an error please call XXXX.
        //       Using this new code, we could set such a WQ on each pathway so it is Active, has the Pathway associated and has all the available Movement Types thus ensuring this code will always return at least one record
        //       While all that needs to be set up, build a method that creates WQ record with the field needed and return it on this call if nothing is returned in SOQL, the WQ only needs to exist in memory, do not save
    
        return null;
    }
    
    //TODO:  Build ranking logic
    //       Regulated article is more imporant than Regulated Article Groups, give it a higher number
    //       Country of Origin is more important than Country Group, give it a higher number
    //       Country of Origin Not Equal to does not need to be ranked because it should have caused records to be kicked out not included
    //       Null values should have a weight of 0
    //       Most other values are equal in weight, if more of them appear a higher total will prevail, let's start by assigning them all 100
    //       Examine if we end up with lots of the same number, if we do we may then need to scale the 100's based on our observations
    //       We may eventually have to add some sort of logic to determine a winner if we have multiple country groups that end up weighing the same
    
    //TODO:  Sort results map by ranking highest value to lowest and return winning WQ
    private static Wizard_Questionnaire__c RankingLogic(List<Wizard_Questionnaire__c> wizardQuestions, Boolean diseaseValidationRequired){
        
        Map<Wizard_Questionnaire__c, Integer> wqMap = new Map<Wizard_Questionnaire__c, Integer>();
        Integer counter = 0;
        Integer highestValue = 0;
        Wizard_Questionnaire__c temp;
        System.debug(wizardQuestions);
        for(Wizard_Questionnaire__c wq: wizardQuestions){
            counter = 0;
            if(wq.Regulated_Article__c != null)
                counter += 500;
            if(diseaseValidationRequired && wq.EFLDiseaseSensitivity__c != null)
                counter += 450;
            if(wq.Country_of_Origin__c != null)
                counter += 350;
            if(wq.Country_Group__c != null)
                counter += 200;
            if(wq.Regulated_Article_Group__c != null)
                counter += 100;
            if(wq.Country_of_Destination__c != null )
                counter += 100; 
            if(wq.Level_1_Region__c != null)
                counter += 100;
            if(wq.State_Province_of_Destination__c != null)
                counter += 100;
            if(wq.Intended_Use__c != null)
                counter += 100;
            if(wq.Component__c != null)
                counter += 100;
                
            wqMap.put(wq, counter); //We don't actually need the map for this ranking logic. Just for debugging
            System.debug('inside for loop---> ' + wq + ': ' + counter);
            if(counter > highestValue){
                temp = wq;
                highestValue = counter;
            }
                
        }
        System.debug('CAME TO RANKING LOGIC');
        System.debug(wqMap);
        
        return temp;
    }
    
    private static List<Wizard_Questionnaire__c> getWizardListAfterDiseases(List<Wizard_Questionnaire__c> wizardQuestions, Set<String> cntryorigdiseases){
        
        System.debug('CAME TO THE DISEASE CODE >>>>>>>>>>');
        List<Wizard_Questionnaire__c> temp = new List<Wizard_Questionnaire__c>();
        Integer listSize = 0;
        for(Wizard_Questionnaire__c wq: wizardQuestions){
            if(wq.EFLDiseaseSensitivity__c != null && wq.EFLDiseaseSensitivity__c != ''){
                String[] tmpString = wq.EFLDiseaseSensitivity__c.split(';');
                if(cntryorigdiseases.containsAll(tmpString)){ //make sure all disease values are in the list we got
                    if(tmpString.size() >= listSize){ //ranking for the best match
                        listSize = tmpString.size();
                        temp.add(wq);
                    }
                    
                }
            }
            else
                temp.add(wq);
        }
        return temp;
    } 
    
}