@isTest(seealldata=false)
private class CARPOL_Response_Letter_controller_Test {

    public static CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
    static Application__c objapp;
    static AC__c li;
    static Authorizations__c objauth;
    static Facility__c fac;
    static String FacRecordTypeId;
    static Communication_Manager__c objcm;
    static Incident__c incident;
    static string attachmentName;
    static attachment objAttach;
    static List<attachment> objAttachList = new List<attachment>();
    
    public static void init(Date issuedDate,integer numAttachments){
        testData.insertcustomsettings();
               
        objapp = testData.newapplication();
        li = testData.newlineitem('Personal Use', objapp);
        objauth = testData.newAuth(objapp.id);
               
        fac = new Facility__c();
        FacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Inspection Station').getRecordTypeId();
        fac.RecordTypeId = FacRecordTypeId; 
        fac.Name = 'Test Facility';
        insert fac;
        
        objcm = testData.newCommunicationmanager('Permit');
        
        incident = new Incident__c();
        incident.Requester_Name__c='testReqName';
        incident.EFL_Template__c=objcm.id;
        incident.Facility__c =fac.id;
        incident.Authorization__c= objauth.id;
        incident.EFL_Issued_Date__c = issuedDate;
        incident.Related_Program__c = 'AC';
        incident.Incident_Date__c = Date.today();
        incident.Incident_Discovery_Date__c = Date.today();
        incident.Incident_Reported_Date__c = Date.today();
        insert incident;
        attachmentName = 'Incident Letter' + System.TODAY().format();
        attachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
        
        if(numAttachments > 10){
            numAttachments = 10;
        }
        for(integer i = 1; i<=numAttachments; i++){
            objAttach = testData.newAttachment(incident.id);    
            objAttach.name = attachmentName;
            objAttachList.add(objAttach);
        }
        if (objAttachList.size()>0){
            update objAttachList;
        }
    }    
    
    public static testMethod void test1(){        
        
        date issuedDate = date.Today();
        init(issuedDate,1);
        test.startTest();  
        ApexPages.StandardController sc = new ApexPages.StandardController(incident);
        CARPOL_Response_Letter_controller cont = new CARPOL_Response_Letter_controller(sc);
        cont.incident = incident;
        cont.templateURL = '';
        cont.selectedTemplateType = 'Permit';
        cont.baseURL = '';
        cont.populateTemplate();  
        cont.saveDraft();
        cont.getTemplates();
        cont.attachLetter();
        test.stopTest();
      }
      public static testMethod void test2(){        
        
        date issuedDate = date.Today()-1;
        init(issuedDate,2);
        test.startTest();  
        ApexPages.StandardController sc = new ApexPages.StandardController(incident);
        CARPOL_Response_Letter_controller cont = new CARPOL_Response_Letter_controller(sc);
        cont.saveDraft();
        cont.attachLetter();
        test.stopTest();
      }
}