public with sharing class OrderInspectionQuestionnaireQuestions{
    
    public List<EFL_Inspection_Questionnaire_Questions__c> qustionsList{get;set;} 
    Id inspID;
    
    private ApexPages.StandardController sctrl;
    public OrderInspectionQuestionnaireQuestions(ApexPages.StandardController controller) {
        this.sctrl = controller;
        init();
    }
    
    public PageReference back()
    {
        PageReference cancel = sctrl.cancel();
        return cancel;
    }
    
    public void init(){
        
        inspID= ApexPages.currentPage().getParameters().get('id');
        qustionsList = [SELECT ID,NAME,Question__c,Answer__c,Response__c,Comments__c,Options__c,Order__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE Inspection__c=:inspID ORDER BY Order__c ASC];
        
    }
}