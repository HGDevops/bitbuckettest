/*
 * Purpose: Central place to access Team object from database with different set of filter parameters.
*/
public with sharing class EFLTeamRepository {


 // Select all Team Members by Authorization ID
     public static list<team__c> selectByAuthorizationID(ID parentRecordID)
     {          
      try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
        return [SELECT Id,Member__c,
                       member_role__c
                   FROM Team__c 
                  WHERE authorization__c =:parentRecordID 
               ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLTeamRepository.selectByAuthorizationID()',e);                
           } 
         return null;
      }  
    
  // Select all Team Members by Inspection ID
     public static list<team__c> selectByInspectionID(ID parentRecordID)
     {          
      try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
        return [SELECT Id,Member__c,
                       member_role__c
                   FROM Team__c 
                  WHERE inspection__c =:parentRecordID 
               ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLTeamRepository.selectByInspectionID()',e);                
           } 
         return null;
      }   
    
  // Select all Team Members by Incident ID
     public static list<team__c> selectByIncidentID(ID parentRecordID)
     {          
      try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Construct__c');
        return [SELECT Id,Member__c,
                       member_role__c
                   FROM Team__c 
                  WHERE incident__c =:parentRecordID 
               ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLTeamRepository.selectByIncidentID()',e);                
           } 
         return null;
      }     
    
    
}