@isTest
public class EFLRelatedListController_Test {
    
    @TestSetup
    static void makeData(){
        Account account = new Account();
        account.Name = 'Test Account';
        insert account;
        
        Contact contact1 = new Contact();
        contact1.FirstName = 'Test';
        contact1.LastName = 'Test 1';
        contact1.AssistantName = 'Assistant 1';
        contact1.AssistantPhone = '(999) 555-1212';
        contact1.Department = 'Department 1';
        contact1.AccountId = account.Id;

        Contact contact2 = new Contact();
        contact2.FirstName = null;
        contact2.LastName = 'Test 2';
        contact2.AccountId = account.Id;

        insert new List<Contact>{contact1, contact2};
    }

    @isTest
    public static void deleteRecordsWithServiceClass_RecordsDeleted(){
        Map<Id, Contact> contacts = new Map<Id, Contact>([Select Id From Contact]);
        System.assertEquals(2, contacts.keySet().size());
        
        EFLRelatedListController.deleteRecords(new List<Id>(contacts.keySet()), 'Contact', 'EFLAR_RelatedListServices', true);
        
        contacts = new Map<Id, Contact>([Select Id From Contact]);
        System.assertEquals(0, contacts.keySet().size());
    }

    @isTest
    public static void deleteRecordsWithoutServiceClass_RecordsDeleted(){
        Map<Id, Contact> contacts = new Map<Id, Contact>([Select Id From Contact]);
        System.assertEquals(2, contacts.keySet().size());
        
        EFLRelatedListController.deleteRecords(new List<Id>(contacts.keySet()), 'Contact', null, true);
        
        contacts = new Map<Id, Contact>([Select Id From Contact]);
        System.assertEquals(0, contacts.keySet().size());
    }

    @isTest
    public static void getPageDataWithoutUsageKey_NoDataReturned(){
        Account account = [Select Id From Account Limit 1];
        
        EFLRelatedListController.PageData pd = EFLRelatedListController.getPageData(account.Id, null);
        System.assertEquals(null, pd);
    }

    @isTest
    public static void getPageDataWithUsageKey_DataReturned(){
        Account account = [Select Id From Account Limit 1];
        Contact contact = [Select Id From Contact Where Department = 'Department 1' Limit 1];
        
        EFLRelatedListController.PageData pd = EFLRelatedListController.getPageData(account.Id, 'Test EFL AR Community Contact Table');
        System.assertNotEquals(null, pd);
        System.assertNotEquals(null, pd.config);
        System.assertEquals(1, pd.records.size());
        System.assertEquals(contact.Id, pd.records[0].Id);
        System.assertEquals(3, pd.records[0].fields.size());
        System.assertEquals('AssistantName', pd.records[0].fields[0].apiName);
        System.assertEquals('Assistant Name', pd.records[0].fields[0].label);
        System.assertEquals('Assistant 1', pd.records[0].fields[0].value);
        System.assertEquals('AssistantPhone', pd.records[0].fields[1].apiName);
        System.assertEquals('Assistant Phone', pd.records[0].fields[1].label);
        System.assertEquals('(999) 555-1212', pd.records[0].fields[1].value);
        System.assertEquals('Department', pd.records[0].fields[2].apiName);
        System.assertEquals('Department', pd.records[0].fields[2].label);
        System.assertEquals('Department 1', pd.records[0].fields[2].value);
        
        // Add new record
        Contact contact3 = new Contact(LastName = 'Test 3', FirstName = 'Test 3', Department = 'Department 3', AccountId = account.Id);
        insert contact3;
        contact3 = [Select Id, Name, AssistantName, AssistantPhone, Department From Contact Where Id = :contact3.Id Limit 1];
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        pd.addRecord(contact3, pd.config.EFL_Related_List_Fields__r, schemaMap, true, true);
        System.assertEquals(2, pd.records.size());
    }

    @isTest
    public static void getPageDataWithUsageKeyWithoutServiceClass_DataReturned(){
        Account account = [Select Id From Account Limit 1];
        Contact contact = [Select Id From Contact Where Department = 'Department 1' Limit 1];
        
        EFLRelatedListController.PageData pd = EFLRelatedListController.getPageData(account.Id, 'Test EFL AR Contact w/o Service Class');
        System.assertNotEquals(null, pd);
        System.assertNotEquals(null, pd.config);
        System.assertEquals(1, pd.records.size());
        System.assertEquals(contact.Id, pd.records[0].Id);
    }
}