@isTest(seealldata=true)
private class CARPOL_UNI_Auth_Junction_WF_Test{
    @IsTest
    static void CARPOL_UNI_Auth_Junction_WF_Test(){
        string ImpapcontRecordTypeId=Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        string PortsFacRecordTypeId=Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        string ACAppRecordTypeId=Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        string ACauthRecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        string ACRegRecordTypeId=Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        string ACLIRecordTypeId=Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
       
        CARPOL_AC_TestDataManager testData=new  CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        //Account objacct=testData.newAccount(AccountRecordTypeId);
        Contact objcont=testData.newcontact();
        //breed__c objbrd=testData.newbreed();
        Applicant_Contact__c apcont=testData.newappcontact();
        //Applicant_Contact__c apcont2=testData.newappcontact();
        Facility__c fac=testData.newfacility('Domestic Port');
        //Facility__c fac2=testData.newfacility('Foreign Port');
        Application__c objapp=testData.newapplication();
        AC__c ac1=testData.newLineItem('Personal Use',objapp);
        //AC__c ac2=testData.newLineItem('Personal Use',objapp);
        //AC__c ac3=testData.newLineItem('Personal Use',objapp);
        Regulation__c objreg1=testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2=testData.newRegulation('Additional Information','Commercial Consignment Requirements');
        //Regulation__c objreg3=testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');
       
        
        Attachment attach=testData.newattachment(ac1.Id);
        Domain__c objProg=new  Domain__c();
        objProg.Name='AC';
        objProg.Active__c=true;
        Insert objProg;
        Program_Prefix__c prefix=new  Program_Prefix__c();
        prefix.Program__c=objProg.Id;
        prefix.Name='555';
        Insert prefix;
        
        Signature__c objTP=new  Signature__c();
        objTP.Name='Test AC TP';
        objTP.Recordtypeid=Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c=prefix.Id;
        Insert objTP;
        
        Program_Workflow__c pw=new  Program_Workflow__c();
        pw.name='Check Violator';
        pw.Program_Prefix__c=prefix.id;
        pw.Assign_to_Queue__c='AC Reviewer';
        pw.Program__c=objProg.id;
        pw.Active__c='Yes';
        pw.Task_Record_Type__c='Standard';
        pw.process_type__c='fast track';
        Insert pw;
        
        Program_Workflow__c pw1=new  Program_Workflow__c();
        pw1.name='Review Potential Violator';
        pw1.Program_Prefix__c=prefix.id;
        pw1.Assign_to_Queue__c='AC Reviewer';
        pw1.Program__c=objProg.id;
        pw1.Active__c='Yes';
        pw1.Task_Record_Type__c='Standard';
        pw1.process_type__c='fast track';
        Insert pw1;
       
        Program_Workflow__c pw2=new  Program_Workflow__c();
        pw2.name='Process Authorization';
        pw2.Program_Prefix__c=prefix.id;
        pw2.Assign_to_Queue__c='AC Reviewer';
        pw2.Program__c=objProg.id;
        pw2.Active__c='Yes';
        pw2.Task_Record_Type__c='Standard';
        pw2.process_type__c='fast track';
        Insert pw2;
        
        Program_Workflow__c pw3=new  Program_Workflow__c();
        pw3.name='Issue Authorization';
        pw3.Program_Prefix__c=prefix.id;
        pw3.Assign_to_Queue__c='AC Reviewer';
        pw3.Program__c=objProg.id;
        pw3.Active__c='Yes';
        pw3.Task_Record_Type__c='Standard';
        pw3.process_type__c='fast track';
        Insert pw3;
        Test.startTest();
        Authorizations__c objauth=testData.newAuth(objapp.Id);
        objauth.Thumbprint__c=objTP.id;
        objauth.prefix2__c='555';
        objauth.Application_Type__c='Renewal';//backed off for sake of coverage
        objauth.Expiration_Date__c=system.today();
        objauth.Expiration_Timeframe__c='20';
        objauth.Application__c = objapp.id;
        Update objauth;
        ac1.Authorization__c=objauth.id;
        ac1.status__c='Waiting on Customer';
        Update ac1;
        List<AC__c> lstauth=new  List<AC__c>();
        //ac2.Authorization__c=objauth.id;
        //ac2.status__c='Waiting on Customer';
        //Update ac2;

        lstauth.add(ac1);
        Map<Authorizations__c,List<AC__c>> mapauth=new  Map<Authorizations__c,List<AC__c>>();
        mapauth.put(objauth,lstauth);
        //Test.startTest();
        system.debug('#### objauth = '+objauth);
        Workflow_Task__c wft = new Workflow_Task__c();
        if(objauth!=null){
            wft=testData.newworkflowtask('Check Violator',objauth,'Pending');
            wft.Status_Categories__c='Customer Feedback';
            wft.Process_Type__c='Fast Track';
            Update wft;
            }
        Map<Id,Workflow_Task__c> mapId=new  Map<Id,Workflow_Task__c>();
        mapId.put(wft.Id,wft);
        Program_Line_Item_Pathway__c plip=testData.newCaninePathway();
        Regulations_Association_Matrix__c objdm=new  Regulations_Association_Matrix__c();
        objdm.Program_Line_Item_Pathway__c=plip.id;
        Insert objdm;
        
        Required_Documents__c rqdocs=new  Required_Documents__c();
        rqdocs.Decision_Matrix__c=objdm.id;
        rqdocs.Required_Docs__c='Health;Test';
        Insert rqdocs;
        
        Required_Documents__c rqdocs2=new  Required_Documents__c();
        rqdocs2.Decision_Matrix__c=objdm.id;
        rqdocs2.Required_Docs__c='Health';
        Insert rqdocs2;
       
        Related_Line_Decisions__c objrld=new  Related_Line_Decisions__c();
        objrld.Line_Item__c=ac1.id;
        objrld.Decision_Matrix__c=objdm.id;
        Insert objrld;
        
        //Related_Line_Decisions__c objrld1=new  Related_Line_Decisions__c();
        //objrld1.Line_Item__c=ac2.Id;
        //objrld1.Decision_Matrix__c=objdm.id;
        //Insert objrld1;
       
        //Related_Line_Decisions__c objrld2=new  Related_Line_Decisions__c();
        //objrld2.Line_Item__c=ac3.Id;
        //objrld2.Decision_Matrix__c=objdm.id;
        //Insert objrld2;
        
        Authorization_Junction__c objauthjun1=testData.newAuthorizationJunction(objauth.Id,objreg1.Id);
        //Authorization_Junction__c objauthjun2=testData.newAuthorizationJunction(objauth.Id,objreg2.Id);
        //Authorization_Junction__c objauthjun3=testData.newAuthorizationJunction(objauth.Id,objreg3.Id);
        Signature_Regulation_Junction__c objSRJ=testData.newSRJ(objTP.id,objreg1.id);
        objSRJ.Decision_Matrix__c=objdm.id;
        Update objSRJ;
        
        //Signature_Regulation_Junction__c objSRJ1=testData.newSRJ(objTP.id,objreg2.id);
        //objSRJ1.Decision_Matrix__c=objdm.id;
        //Update objSRJ1;
       
        //Signature_Regulation_Junction__c objSRJ2=testData.newSRJ(objTP.id,objreg3.id);
        //objSRJ2.Decision_Matrix__c=objdm.id;
        //Update objSRJ2;
       
        Amendment_Renewal__c objAR=new  Amendment_Renewal__c();
        //objAR.Parent_Line_Item__c=ac2.id;
        objAR.Parent_Line_Item__c=ac1.id;
        objAR.Parent_Authorization__c=objauth.id;
        objAR.Child_Line_Item__c=ac1.Id;
        objAR.Child_Authorization__c = objauth.id;
        objAR.Parent_Application__c = objapp.id;
        objAR.Child_Application__c = objapp.id;
        objAR.Recordtypeid=Schema.SObjectType.Amendment_Renewal__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        Insert objAR;
        
            CARPOL_UNI_Auth_Junction_WF extclass=new  CARPOL_UNI_Auth_Junction_WF();
            extclass.createAuthorizationJunctionRecord(mapauth);
            system.assert(extclass!=null);

        Test.stopTest();
    }
}