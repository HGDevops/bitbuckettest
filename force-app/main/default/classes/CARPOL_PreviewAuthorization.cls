/*
 *Purpose: Provides formula content dynamically generated Letters in eFile.
*/
public with sharing class CARPOL_PreviewAuthorization{
  
    public string templateURL{get;set;}
    public string letterType{get;set;}
    public string selectedTemplateType{get;set;}
    public string selectedTemplate{get;set;}
    public SelectOption[] allTemplateOptions{get;set;}
    public string reviewerComments{get;set;}
    public string baseURL{get;set;}
    public Authorizations__c authorization{get;set;}
    //public Workflow_Task__c wtask{get;set;} 
    public user wtaskowner{get;set;} 
    public boolean showAttachDraftBtn{get;set;}
    public boolean showAttachBtn{get;set;}
    public boolean showtemplateBtn{get;set;}
    public boolean showLinks{get;set;}
    public id lineitemid{get;set;}
    
    //Location parameters
    public list<Location__c> listlocations{get;set;}
    id olocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin');
    id dlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Destination');
    id oanddlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin and Destination');
    id releaserectypeid=EFLGenericUtility.getrecordtypeId('Location Release Sites');
    private string OriginLocation = ''; 
    private string OriginandDestLocation = '';
    private string DestinationLocation = '';
    private string releaseSiteLocation = '';
    
    public string RegulatedArticle{get;set;}
    public string StateRegulatoryOfficialslist{get;set;}
    private string locationlist;
    public string dateToday{get;set;}
    public list<Reviewer__c> listReviewRec{get;set;}
    public list<Link_Regulated_Articles__c> listRegArt{get;set;}
    //public id wfid{get;set;}
    public string eSignLiveURL{get;set;}
    public AC__c LineItem{get;set;}
    public string currenttime {get;set;}
    
    private string applicantAddress;
    
   
    
    public CARPOL_PreviewAuthorization(ApexPages.StandardController controller){
          
        if(Datetime.now().hour()<13)
            currenttime = String.valueOf(Datetime.now().hour())+':'+String.valueOf(Datetime.now().minute())+' AM';
         else
            {
            Integer hour = Integer.valueOf(Datetime.now().hour())-12;
            currenttime = String.valueOf(hour)+':'+String.valueOf(Datetime.now().minute())+' PM';
            }
       /* wfid=apexpages.currentpage().getparameters().get('tId');
        if(wfid==null)
        wfid=apexpages.currentpage().getparameters().get('wfid'); 

        if(wfid!=null)
          wtask=[SELECT Id,Comments__c,OwnerId,Owner.Name,Owner.Phone,Owner.Email,Program__c,Status__c,name FROM Workflow_Task__c WHERE Id=:wfid LIMIT 1];
          */      
        showAttachDraftBtn=false;
        showAttachBtn=true;
        showtemplateBtn=true;
        showLinks=true;

        dateToday = datetime.now().format('EEEE')+', '+datetime.now().format('MMMM')+' '+String.valueOf(Datetime.now().day())+', '+datetime.now().format('YYYY');
        baseURL=URL.getSalesforceBaseUrl().toExternalForm(); 
        this.authorization=(Authorizations__c)controller.getRecord();
      
        authorization=[SELECT ID,Name,Authorization_Type__c,AC_Applicant_Name__c,Application__r.Name,Applicant_Organization__c, Authorized_User__r.FirstName,applicant__r.MailingAddress,
                               Authorized_User__r.LastName,AC_Applicant_Address__c,Template__c,Date_Issued__c,Effective_Date__c,Expiry_Date__c,Bio_Tech_Reviewer__r.Name,
                               Bio_Tech_Reviewer__c,Bio_Tech_Reviewer__r.Phone,Bio_Tech_Reviewer__r.Email,Thumbprint__c,BRS_Introduction_Type__c,Thumbprint__r.Regulated_Article__r.name,
                               Thumbprint__r.From_Country__r.name,Thumbprint__r.REF_Program_Name__c,Applicant_Reference_Number__c,Thumbprint__r.Line_Record_Type__c,Interstate_Movement_Expiration_Date__c,Expiration_Date__c,
                               Thumbprint__r.Regulated_Article__r.Scientific_Name__c, applicant__r.Mailing_Street_LR__c, applicant__r.Mailing_City_LR__c,applicant__r.State_Code__c,applicant__r.Mailing_Zip_Postalcode_LR__c
                          FROM Authorizations__c 
                         WHERE ID=:apexpages.currentpage().getparameters().get('id') LIMIT 1]; 
        
        if(authorization==null)ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please select a related Authorization.'));
        else{
            authorization.Date_Issued__c=System.Today();
            letterType=authorization.Authorization_Type__c;
            templateURL='apex/CARPOL_AuthorizationLetter?id='+authorization.ID; //+'&tId='+wtask.Id;
            //Added for on-hold assistance Importer = consignee, Exporter = CBP,  Arrival_Time__c = actual arrival (on BoL), Proposed_date_of_arrival__c  = expected arrival (on airwaybill)
            LineItem = [select Importer_last_name__c,Importer_last_name__r.name,Importer_first_name__c,Exporter_Email__c, Exporter_Phone__c, Exporter_Fax__c, Airway_Bill_Number__c,Bill_of_Lading__c, Container__c,Vessel__c,
                              Tracking_Number__c,Country_of_origin__c,Country_of_origin__r.name, Scientific_Name__c,Scientific_Name__r.name, Arrival_Time__c,Proposed_date_of_arrival__c,Air_Transporter_Flight_Number__c, 
                              Application_Number__c, Application_Number__r.name, port_of_entry__r.city__c, port_of_entry__r.State_LV1__r.name 
                         from ac__c 
                        where Authorization__c =:authorization.id 
                        limit 1];
        }
       
//----------------------Start BRS Specific Functionality------------------------------// 
        OriginLocation='';
        OriginandDestLocation='';
        DestinationLocation='';
        RegulatedArticle='';
        StateRegulatoryOfficialslist='';
        locationlist='';
        lineitemid=[SELECT Id FROM AC__c WHERE Authorization__c=:authorization.Id LIMIT 1].Id;
         
//--location Information--//
        listlocations = EFLLocationUtility.getSortedLocations(lineitemid);
//--Regulated Articles--//
        listRegArt=[SELECT Id,Name,Regulated_Article__r.Name FROM Link_Regulated_Articles__c WHERE Line_Item__c=:lineitemid];
//--Official Review Records--//
        listReviewRec=[SELECT Id,Name,Notes_to_State__c,
                              BRS_State_Reviewer_Email__c,
                              BRS_State_Reviewer_Notification__c,
                              Requirement_Desc__c,State_Comments__c,
                              State_not_Responded__c,State_has_comments__c,
                              No_Requirements__c,State_Regulatory_Official__r.FirstName,Review_Complete__c,
                              Reviewer__c,Status__c,Reviewer_Unique_Id__c,State__c 
                         FROM Reviewer__c 
                        WHERE Authorization__c=:authorization.Id];
        
       if(listlocations.size()>0){
            locationlist = getLocationInfo(); 
         }
        if(listRegArt.size()>0){
            for(Link_Regulated_Articles__c lRA:listRegArt)RegulatedArticle+=lRA.Regulated_Article__r.Name+'; ';
        }
        if(listReviewRec.size()>0){
            for(Reviewer__c sor:listReviewRec)StateRegulatoryOfficialslist+=sor.State_Regulatory_Official__r.FirstName+','+sor.State__c+'; ';
        }
//----------------------End BRS Specific Functionality------------------------------// 
    } 

    //Get Location Information    
    private string getLocationInfo(){

        string locationInfo = '';
         if(listlocations.size()>0){
            for(Location__c lc:listlocations){
                if(lc.recordtypeid==olocrectypeid 
                && authorization.BRS_Introduction_Type__c == EFLGlobalConstants.getConstantValue('MOVEMENT TYPE IMPORT') && !OriginLocation.Contains(lc.Country__r.name))
                {   OriginLocation+=lc.Country__r.name+'; ';
                }else if(lc.recordtypeid==olocrectypeid && !OriginLocation.Contains(lc.State__r.name)){
                    OriginLocation+=lc.State__r.name+'; ';
                }else if(lc.recordtypeid==oanddlocrectypeId && !OriginandDestLocation.Contains(lc.State__r.name)){
                    OriginandDestLocation+=lc.State__r.name+'; ';
                }else if(lc.recordtypeid==dlocrectypeId && !DestinationLocation.Contains(lc.State__r.name)){
 					DestinationLocation+=lc.State__r.name+'; ';
                }else if(lc.recordtypeid==releaserectypeid && !releaseSiteLocation.Contains(lc.State__r.name)){
                    releaseSiteLocation+=lc.State__r.name+'; ';
                }  
            }
            if(OriginLocation!='')
                 locationInfo+='Origin Location - '+OriginLocation+'<br/>';
            if(OriginandDestLocation!='')
                locationInfo+='Origin and Destination Location - '+OriginandDestLocation+'<br/>';
            if(DestinationLocation!='')
                locationInfo+='Destination Location - '+DestinationLocation+'<br/>';
             if(releaseSiteLocation!='')
                 locationInfo+='Release Site Location - '+releaseSiteLocation+'<br/>';
        }
        return locationInfo;
    }
    
    //Populate template information
    public PageReference populateTemplate(){ 
        //Replacing Variables 
        try{
            if(authorization.Template__c!=null){
                authorization.Authorization_Letter_Content__c=[SELECT ID,Name,Content__c FROM Communication_Manager__c WHERE ID=:authorization.Template__c LIMIT 1].Content__c;
                authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplicantFirstName} {!ApplicantLastName}',authorization.Authorized_User__r.FirstName+' '+authorization.Authorized_User__r.LastName);
                if(authorization.AC_Applicant_Address__c!=null){
                   authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplicantAddress}',EFLFormatUtility.formatAddress(authorization.applicant__r.MailingAddress));
                }else{
                   authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplicantAddress}','');
                   }
                authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplicationNumber}',authorization.Application__r.Name);
                authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!IssuedDate}',EFLFormatUtility.formatDate(authorization.Date_Issued__c, 'MM/dd/yyyy')); 
                if(authorization.Name!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!AuthorizationNotificationNumber}',authorization.Name);
                if(authorization.Applicant_Reference_Number__c !=null)
                   authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!Applicant_Reference_Number}',authorization.Applicant_Reference_Number__c); 
                //if(wtask.Comments__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskComments}',wtask.Comments__c);
                //if(wtask.Owner.Name!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwner}',wtask.Owner.Name);
                //if(wtask.Owner.Phone!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwnerPhone}',wtask.Owner.Phone);
                //if(wtask.Owner.Email!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwnerEmail}',wtask.Owner.Email);
                if(authorization.Thumbprint__r.Regulated_Article__r.name!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpRegulatedArticle}',authorization.Thumbprint__r.Regulated_Article__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpRegulatedArticle}','');
                if(authorization.Thumbprint__r.From_Country__r.name!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpFromCountry}',authorization.Thumbprint__r.From_Country__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpFromCountry}','');
                if(authorization.Thumbprint__r.REF_Program_Name__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpProgramName}',authorization.Thumbprint__r.REF_Program_Name__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpProgramName}','');
                if(authorization.Thumbprint__r.Line_Record_Type__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpLineRecordType}',authorization.Thumbprint__r.Line_Record_Type__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpLineRecordType}','');
                if(authorization.Thumbprint__r.Regulated_Article__r.Scientific_Name__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpRAScientificName}',authorization.Thumbprint__r.Regulated_Article__r.Scientific_Name__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!tpRAScientificName}','');
                if(authorization.BRS_Introduction_Type__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!Movementtype}',authorization.BRS_Introduction_Type__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!Movementtype}','');
                if(locationlist!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!locationlist}',locationlist);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!locationlist}','');
                if(RegulatedArticle!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!RegulatedArticle}',RegulatedArticle);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!RegulatedArticle}','');
                if(StateRegulatoryOfficialslist!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!StateRegulatoryOfficialslist}',StateRegulatoryOfficialslist);
                else{
                    authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!StateRegulatoryOfficialslist}','');
                }
                if(authorization.Effective_Date__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!EffectiveDate}',EFLFormatUtility.formatDate(authorization.Effective_Date__c, 'MM/dd/yyyy'));
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!EffectiveDate}','');  
                
                if(authorization.Expiration_Date__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ExpirationDate}', EFLFormatUtility.formatDate(authorization.Expiration_Date__c, 'MM/dd/yyyy'));
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ExpirationDate}',''); 

                if(authorization.Interstate_Movement_Expiration_Date__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!MovementExpirationDate}',EFLFormatUtility.formatDate(authorization.Interstate_Movement_Expiration_Date__c, 'MM/dd/yyyy'));
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!MovementExpirationDate}',''); 
                // for on-hold assistance
                if(lineitem.Exporter_Email__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!CBPEmail}',lineitem.Exporter_Email__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!CBPEmail}','');  
                if(lineitem.Exporter_Phone__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!CBPphone}',lineitem.Exporter_Phone__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!CBPphone}','');  
                if(lineitem.Exporter_Fax__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!CBPFax}',lineitem.Exporter_Fax__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!CBPFax}','');  
                if(lineitem.Airway_Bill_Number__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!AirwayBillNumber}',lineitem.Airway_Bill_Number__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!AirwayBillNumber}','');  
                if(lineitem.Proposed_date_of_arrival__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ExpDateofArr}',string.valueOf(lineitem.Proposed_date_of_arrival__c.format()));
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ExpDateofArr}','');  
                if(lineitem.Bill_of_Lading__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!BillofLading}',lineitem.Bill_of_Lading__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!BillofLading}','');  
                if(lineitem.Arrival_Time__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ActDateofArr}',string.valueof(lineitem.Arrival_Time__c.format()));
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ActDateofArr}','');  
                if(lineitem.Container__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!Container}',lineitem.Container__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!Container}','');  
                if(lineitem.Vessel__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!Vessel}',lineitem.Vessel__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!Vessel}','');  
                if(lineitem.Country_of_origin__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!COO}',lineitem.Country_of_origin__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!COO}','');  
                if(lineitem.Scientific_Name__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!RA}',lineitem.Scientific_Name__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!RA}','');  
                if(lineitem.Importer_last_name__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ImpFstName}',lineitem.Importer_last_name__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ImpFstName}','');  
                if(lineitem.Importer_first_name__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ImpLastName}',lineitem.Importer_first_name__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ImpLastName}','');  
                if(lineitem.Air_Transporter_Flight_Number__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!FlightNum}',lineitem.Air_Transporter_Flight_Number__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!FlightNum}','');  
                if(lineitem.application_Number__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplNum}',lineitem.Application_Number__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplNum}','');       
                authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!FormattedDateToday}',dateToday);                 
                if(authorization.Applicant_Organization__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplOrg}',authorization.Applicant_Organization__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplOrg}','');   
                if(lineitem.Port_of_entry__r.city__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!portofEntryCity}',lineitem.Port_of_entry__r.city__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!portofEntryCity}','');   
                if(lineitem.port_of_entry__r.State_LV1__r.name!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!portofEntryState}',lineitem.port_of_entry__r.State_LV1__r.name);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!portofEntryState}','');   
                authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!timeNotified}',currenttime);                
                if(authorization.applicant__r.Mailing_Street_LR__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplStreet}',authorization.applicant__r.Mailing_Street_LR__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplStreet}','');   
                if(authorization.applicant__r.Mailing_City_LR__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplCity}',authorization.applicant__r.Mailing_City_LR__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplCity}','');   
                if(authorization.applicant__r.State_Code__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplState}',authorization.applicant__r.State_Code__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplState}','');   
                if(authorization.applicant__r.Mailing_Zip_Postalcode_LR__c!=null)authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplZip}',authorization.applicant__r.Mailing_Zip_Postalcode_LR__c);
                else authorization.Authorization_Letter_Content__c=authorization.Authorization_Letter_Content__c.replace('{!ApplZip}','');   
                
                   
                Update authorization;
                previewTemplate();
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Template populated successfully.'));
                PageReference pref=new  PageReference('/apex/CARPOL_PreviewAuthorization?id='+authorization.ID);
                pref.setRedirect(false);
                return pref;
            }
            else ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please select a template to be populated.'));
             PageReference pref=new  PageReference('/apex/CARPOL_PreviewAuthorization?id='+authorization.ID);
                pref.setRedirect(false);
                return pref;
        }
        catch(Exception e){
                 ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'+e));
                 PageReference pref=new  PageReference('/apex/CARPOL_PreviewAuthorization?id='+authorization.ID);
                pref.setRedirect(false);
                return pref;
        }
        
    }
    
    //Provides cancel feature and redirect to authorization record
    public PageReference cancel(){
         
        PageReference return2Auth=new  PageReference('/'+authorization.ID);
        return2Auth.setRedirect(true);
        return return2Auth;
    }
    
    // Attach Letter to Parent record
    public PageReference attachLetter(){
        try{
            string attachmentName;
            PageReference pdfAuthorizationLetter=Page.CARPOL_AuthorizationLetter;
            attachmentName='Authorization Letter_'+System.TODAY().format();
            if(letterType=='Notification'&&letterType!=''){
                attachmentName='Acknowledgement Letter_'+System.TODAY().format();
            }
 
            if(letterType=='Letter of Denial'){
                pdfAuthorizationLetter=Page.CARPOL_AuthorizationLetter;
                attachmentName='Denial Letter_'+System.TODAY().format();
            } 

            pdfAuthorizationLetter.getParameters().put('id',authorization.ID);
            //pdfAuthorizationLetter.getParameters().put('tId',wtask.Id);
            Blob pdfAuthorizationLetterBlob;
            if(Test.IsRunningTest())pdfAuthorizationLetterBlob=Blob.valueOf('UNIT.TEST');
            else{
                
                pdfAuthorizationLetterBlob=pdfAuthorizationLetter.getContent();
                //pdfAuthorizationLetterBlob=Blob.valueOf('Hello World');
            }
            string tempAttachmentName=attachmentName.substring(0,attachmentName.length())+'%';
            
            List<Attachment> prevAttachments=[SELECT Id,Name FROM Attachment WHERE ParentID=:authorization.ID AND Name LIKE:tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size()>0){
                if(prevAttachments.size()==1)attachmentName=attachmentName+'_v2';
                else{
                    for(Integer i=0;i<prevAttachments.size();i++){
                        if(i==prevAttachments.size()-1){
                            tempAttachmentName=string.ValueOf(prevAttachments[i].Name);
                            
                            tempAttachmentName=tempAttachmentName.substring(0,tempAttachmentName.length()-4);
                            Integer versionNumber=Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v')+2,tempAttachmentName.length()))+1;
                            
                            attachmentName=tempAttachmentName.substring(0,tempAttachmentName.indexOf('_v')+2)+versionNumber;
                            
                        }
                    }
                }
            }
            attachmentName=attachmentName+'.pdf';
            Attachment attachment=new  Attachment(parentId=authorization.ID,name=attachmentName,body=pdfAuthorizationLetterBlob);
            Insert attachment;
            String pageName = null;
            
            if(letterType!='Notification'&&letterType!='Letter of Denial'){
                // call esignlive code here and set eSignLiveURL value
                pageName = pdfAuthorizationLetter.getUrl().substringAfter('/apex/').substringBefore('?');
                system.debug('pageName=== ' + pageName);
                if (String.isNotBlank(pageName) && System.Label.EFLeSignLive == 'Yes') {
                    eSignLiveURL = EFLCreatePackageSign.PackageAutoCreate((string)authorization.Id, authorization.Name, attachment.Id, pageName);
                }
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Authorization Letter attached successfully.'));
            }
            else if(letterType=='Letter of Denial'){
                // call esignlive code here and set eSignLiveURL value
                pageName = pdfAuthorizationLetter.getUrl().substringAfter('/apex/').substringBefore('?');
                system.debug('pageName=== ' + pageName);
                if (String.isNotBlank(pageName) && System.Label.EFLeSignLive == 'Yes') {
                    eSignLiveURL = EFLCreatePackageSign.PackageAutoCreate((string)authorization.Id, authorization.Name, attachment.Id, pageName);
                }
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Denial Letter attached successfully.'));
            }
            else {
                // call esignlive code here and set eSignLiveURL value
                pageName = pdfAuthorizationLetter.getUrl().substringAfter('/apex/').substringBefore('?');
                system.debug('pageName=== ' + pageName);
                if (String.isNotBlank(pageName) && System.Label.EFLeSignLive == 'Yes') {
                    eSignLiveURL = EFLCreatePackageSign.PackageAutoCreate((string)authorization.Id, authorization.Name, attachment.Id, pageName);
                }
                            
             ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Acknowledgement Letter attached successfully.'));
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'+e));
        }
        return null;
    }
      
    //Attach Draft letter to parent record
    /*public PageReference attachDraftLetter(){
        showLinks=true;
        try{
            
            string draftattachmentName;
            PageReference pdfAuthorizationLetter=Page.CARPOL_AuthorizationLetter;
            draftattachmentName='Draft Authorization Letter_'+System.TODAY().format();
            pdfAuthorizationLetter.getParameters().put('id',authorization.ID);
            //pdfAuthorizationLetter.getParameters().put('tId',wtask.Id);
            Blob pdfAuthorizationLetterBlob;
 
            if(Test.IsRunningTest())pdfAuthorizationLetterBlob=Blob.valueOf('UNIT.TEST');
            else{
                pdfAuthorizationLetterBlob=pdfAuthorizationLetter.getContent();
            }
            string tempAttachmentName=draftattachmentName.substring(0,draftattachmentName.length())+'%';
            
            //List<Attachment> prevAttachments=[SELECT Id,Name FROM Attachment WHERE ParentID=:wtask.Id AND Name LIKE:tempAttachmentName ORDER BY CreatedDate];
            List<Attachment> prevAttachments=[SELECT Id,Name FROM Attachment WHERE Name LIKE:tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size()>0){
                if(prevAttachments.size()==1)draftattachmentName=draftattachmentName+'_v2';
                else{
                    for(Integer i=0;i<prevAttachments.size();i++){
                        if(i==prevAttachments.size()-1){
                            tempAttachmentName=string.ValueOf(prevAttachments[i].Name);
                            
                            tempAttachmentName=tempAttachmentName.substring(0,tempAttachmentName.length()-4);
                            Integer versionNumber=Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v')+2,tempAttachmentName.length()))+1;
                            
                            draftattachmentName=tempAttachmentName.substring(0,tempAttachmentName.indexOf('_v')+2)+versionNumber;
                        }
                    }
                }
            }
            draftattachmentName=draftattachmentName+'.pdf';
            Attachment attachment=new  Attachment(parentId=wtask.Id,name=draftattachmentName,body=pdfAuthorizationLetterBlob);
            Insert attachment;
            
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Authorization Draft Letter attached successfully.'));
        }
        catch(Exception e){
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'+e));
        }
        return null;
    }*/
    
    //Save's the current changes on the letter
    public PageReference saveDraft(){
        try{
            if(authorization.Date_Issued__c<System.Today())ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Date Issued cannot be in the past.'));
            else{
                Update authorization;
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Draft saved successfully.'));
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'+e));
        }
        return null;
    }
    
    //Ability to preview the letter
    public PageReference previewTemplate(){
        try{
            //templateURL='/apex/CARPOL_AuthorizationLetter?id='+authorization.ID+'&tId='+wtask.Id;
            templateURL='/apex/CARPOL_AuthorizationLetter?id='+authorization.ID;

        }
        catch(Exception e){
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'+e));
        }
        return null;
    }
     
}