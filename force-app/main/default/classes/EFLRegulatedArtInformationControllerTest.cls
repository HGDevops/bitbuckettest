@isTest
public class EFLRegulatedArtInformationControllerTest{ 

   public static RegulatedArticle_Information__c Rif;
   public static RegulatedArticle_Information__c Rif1;
   public id currentRecordId;
   public static  Breed__c brd1;
   public static AC__c ac1;
  public static void setupdata() {
  CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
    testData.insertcustomsettings();
    Application__c objApp = testData.newapplication();
     ac1 = testData.newLineItem('Personal Use',objApp);      
     Breed__c brd = testdata.newbreed(); 
     brd1 = testdata.newbreed(); 
     brd1.Name = 'Other';
     update brd1;
    
     Rif = new RegulatedArticle_Information__c();
     Rif .Dog_Name__c = 'Orange';
     Rif .Date_of_Birth__c = date.today()-10;
     Rif .Color__c = 'Red';
     Rif .Sex__c = 'Female';
     Rif .Breed__c = brd.Id;
     Rif .Line_Item__c = ac1.Id;
     insert Rif;
     
     
     Rif1 = new RegulatedArticle_Information__c();
     Rif1 .Dog_Name__c = 'Plum';
     Rif1 .Date_of_Birth__c = date.today()-20;
     Rif1 .Color__c = 'Red';
     Rif1 .Sex__c = 'Female';
     Rif1 .Breed__c = brd1.Id;
     Rif1 .Line_Item__c = ac1.Id;
     Rif1 .Breed_Description__c = 'test';
     insert Rif1;
     
     Rif1 .Dog_Name__c = 'Rainbow';
     update Rif1;
     
      }
    public static testMethod void AllGetMethod_Test(){
    Test.StartTest();
    setupdata();
    AC__c lineItem = [SELECT Id, Application_Number__c FROM Ac__c LIMIT 1];

    PageReference pRef = Page.EFLRegulatedArticleInformation;
    Test.setCurrentPage(pRef);
    ApexPages.CurrentPage().getParameters().put('LineItemId', lineItem.Id);
    RegulatedArticle_Information__c asd = new RegulatedArticle_Information__c();
    ApexPages.StandardController sc = new ApexPages.StandardController(asd);
    EFLRegulatedArticleInformationController regartinfCtrl = new EFLRegulatedArticleInformationController(sc);
    regartinfCtrl.RAINFList = EFLregulatedarticleinformationRepository.selectRAINFByLineItemID(lineItem.Id);
    regartinfCtrl.RAINFRecord = EFLregulatedarticleinformationRepository.selectByID(Rif.ID);  
    EFLregulatedarticleinformationRepository.selectRAINFByLineItemIDForClone(lineItem.Id);
    EFLregulatedarticleinformationRepository.selectRAINFByLineItemIDAndWildCardSearchFilter(lineItem.Id,'test');
     string wildCardSearchFilter = 'test';
     boolean testLockLine = regartinfCtrl.lockLineItem;
     string testInst = regartinfCtrl.instructions;
     boolean testdetectedChanges = regartinfCtrl.detectedChanges;
     boolean testdcrptnrequired =  regartinfCtrl.dcrptnrequired;
     String testselectedbreed = regartinfCtrl.selectedbreed;
     String testconfirmationMessage = regartinfCtrl.confirmationMessage;
     String URL = regartinfCtrl.URL;
     boolean testrenderList = regartinfCtrl.renderList;
     boolean testrenderDetail =  regartinfCtrl.renderDetail;
     boolean testrenderSpringCM = regartinfCtrl.renderSpringCM;
     Boolean testdisableUpload =  regartinfCtrl.disableUpload;
     Boolean testshowRegulatedArticleView = regartinfCtrl.showRegulatedArticleView;
    regartinfCtrl .lineItemId = lineItem.Id;
    regartinfCtrl.delrecid = Rif.id;
    regartinfCtrl.selectedbreed = 'Other';
    regartinfCtrl .showRegulatedArticleDetailView();
    regartinfCtrl .showDetail();
    regartinfCtrl .cancel();
    regartinfCtrl .updateRAINF();
    regartinfCtrl.deleteRecord();
    try{regartinfCtrl.fireDocLauncher();}catch(exception ex){}
     regartinfCtrl.RAINFRecord.Dog_Name__c = 'test';
     regartinfCtrl.RAINFRecord.Date_of_Birth__c = date.today()-24;
     regartinfCtrl.RAINFRecord.Color__c = 'blue';
     regartinfCtrl.RAINFRecord.Sex__c = 'male';
     regartinfCtrl.RAINFRecord.Breed__c = brd1.Id;
     regartinfCtrl.RAINFRecord.Line_Item__c = ac1.Id;
     regartinfCtrl.RAINFRecord.Breed_Description__c = 'test';
    regartinfCtrl .detectChanges();
    regartinfCtrl .Descriptionrender();
    regartinfCtrl.getInstructions();
    regartinfCtrl .showListView();


    Test.StopTest();
  }
    
  public static testMethod void testHappyPath(){
    Test.StartTest();
    setupdata();
    AC__c lineItem = [SELECT Id, Application_Number__c FROM Ac__c LIMIT 1];

    PageReference pRef = Page.EFLRegulatedArticleInformation;
    Test.setCurrentPage(pRef);
    ApexPages.CurrentPage().getParameters().put('LineItemId', lineItem.Id);
    RegulatedArticle_Information__c asd = new RegulatedArticle_Information__c();
    ApexPages.StandardController sc = new ApexPages.StandardController(asd);
    EFLRegulatedArticleInformationController regartinfCtrl = new EFLRegulatedArticleInformationController(sc);
    regartinfCtrl.RAINFList = EFLregulatedarticleinformationRepository.selectRAINFByLineItemID(lineItem.Id);
    regartinfCtrl.RAINFID = Rif.ID;  
    regartinfCtrl .showListView();
	regartinfCtrl.RAINFID = null; 
    regartinfCtrl.RAINFRecord = new RegulatedArticle_Information__c();   
    regartinfCtrl.RAINFRecord.Dog_Name__c = 'test';
     regartinfCtrl.RAINFRecord.Date_of_Birth__c = date.today()-365;
     regartinfCtrl.RAINFRecord.Color__c = 'blue';
     regartinfCtrl.RAINFRecord.Sex__c = 'Male';
     regartinfCtrl.RAINFRecord.Breed__c = brd1.Id;
     regartinfCtrl.RAINFRecord.Line_Item__c = ac1.Id;
     regartinfCtrl.RAINFRecord.Breed_Description__c = 'test';  
    regartinfCtrl.updateRAINF();  
    regartinfCtrl.RAINFRecord.Dog_Name__c = 'test1';
    regartinfCtrl.detectChanges();  
    regartinfCtrl.RAINFRecord.Date_of_Birth__c = date.today()-366;
    regartinfCtrl.detectChanges();  
    regartinfCtrl.RAINFRecord.Color__c = 'blue1';
    regartinfCtrl.detectChanges(); 
     regartinfCtrl.RAINFRecord.Breed__c = brd1.Id;
    regartinfCtrl.detectChanges(); 
    Test.StopTest();  
  }

 
}