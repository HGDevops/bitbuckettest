public Interface EFLIActivityResultProcessor {
    List<SelectOption> loadOptions(EFLPreScreeningWrapper psw);
    boolean processResult(EFLPreScreeningWrapper psw);

}