@isTest
public class CARPOL_LineItemAppTriggerHelperTest {
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    static testmethod void testUtilityMethods(){
        testData.insertcustomsettingsWithBRSTriggerDisabled();
            
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        test.startTest();
        
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Open', 1);
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            auth = new Authorizations__c();
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Submitted');
        Link_Regulated_Articles__c lra = new Link_Regulated_Articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(lineitem.Id, ra.Id, 'Draft');
        Article_Supplier_Developer__c asd = new Article_Supplier_Developer__c();
        asd = testdata.newArticlSupplierByLineItem(LineItem.id);
        asd.Status__c = 'Draft';
        update asd;
        construct__c construct = new construct__c();
        construct = testData.newconstructByLIIdLRAId(lineitem.id, ra.Id, 'Draft');
        construct.Status__c = 'Draft';
        update construct;
        /*Phenotype__c phenotype = new Phenotype__c();
        phenotype = testdata.newphenotype(construct.id);
        GenotypeType__c genotypeType = new GenotypeType__c(Genotype_Category__c='Empty Transformation Vector', Construct__c=construct.id);
        insert genotypeType;
        genotype__c genotype = new genotype__c();
        genotype = testdata.newgenotype(construct.id, genotypeType);*/
        Construct_Application_Junction__c caj = new Construct_Application_Junction__c();
        caj = testdata.newPreConstructByLIIdConIdStatus(LineItem.id, construct.Id, 'Draft');
        location__c location = new location__c();
        location =  testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Draft');
        Map<Id, AC__c > lineItemMap = new Map<Id, AC__c >();
        lineItemMap.put(lineItem.id,lineItem);
        CARPOL_LineItemApplicationTriggerHelper.updateLineitemChildRecords(lineItemMap);
        test.stopTest();        
    }
}