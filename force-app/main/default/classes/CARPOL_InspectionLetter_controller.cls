public with sharing class CARPOL_InspectionLetter_controller {
    
    public String baseURL { get; set; }
    public String letterType { get; set; }
    public Inspection__c inspection { get; set; }
    
    
    public CARPOL_InspectionLetter_controller(ApexPages.StandardController controller) {
        
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        this.inspection = (Inspection__c)controller.getRecord();
        if(!Test.isrunningTest())
            inspection = [select id, name, Compliance_letter_content__c, Inspector_Email__c, Communication_Manager_Template__c  FROM Inspection__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
    }
    
    public PageReference populateTemplate()
    {
        // System.Debug('<<<<<<< Template : ' + incident. EFL_Template__c + ' >>>>>>>');
        try
        {
            if(inspection.Communication_Manager_Template__c != null)
            {
                inspection.Compliance_letter_content__c = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID =:inspection.Communication_Manager_Template__c LIMIT 1].Content__c;
                
                
                update inspection;
                //previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Template populated successfully.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a template to be populated.'));
            }
        }
        catch(Exception e)
        {
            //System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a valid template to be populated.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference attachLetter()
    {
        try{
            
            String attachmentName;
            PageReference pdfComplianceLetter = Page.CARPOL_InspectionLetterPDF;
            attachmentName = 'Inspection Letter_' + System.TODAY().format();
            
            
            pdfComplianceLetter.getParameters().put('id', inspection.ID);
            //pdfComplianceLetter.getParameters().put('tId', wtask.Id);
            Blob pdfComplianceLetterBlob;
            
            if (Test.IsRunningTest())
            {
                pdfComplianceLetterBlob =Blob.valueOf('UNIT.TEST');
            }
            else
            {
                //System.Debug('<<<<<<< Page about to be loaded : ' + pdfComplianceLetter + ' >>>>>>');
                pdfComplianceLetterBlob = pdfComplianceLetter.getContent();
                
            }
            
            
            String tempAttachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
            //System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :inspection.ID AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size() > 0)
            {
                if(prevAttachments.size() == 1){attachmentName = attachmentName + '_v2';}
                else
                {
                    for(Integer i = 0; i < prevAttachments.size(); i++ )
                    {
                        if(i == prevAttachments.size() - 1)
                        {
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');
                            attachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;
                            System.Debug('<<<<<<< New Attachment Name : ' + attachmentName + ' >>>>>>');
                        }
                    }
                }
            }
            
            attachmentName = attachmentName + '.pdf';
            Attachment attachment = new Attachment(parentId = inspection.ID, name = attachmentName, body = pdfComplianceLetterBlob);
            insert attachment;
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Inspection Letter attached successfully.'));
            
            
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    
    public PageReference attachSignLetter()
    {
        try{
            
            String attachmentName;
            PageReference pdfComplianceLetter = Page.CARPOL_InspectionLetterPDF;
            attachmentName = 'Inspection Letter_' + System.TODAY().format();
            
            
            pdfComplianceLetter.getParameters().put('id', inspection.ID);
            //pdfComplianceLetter.getParameters().put('tId', wtask.Id);
            Blob pdfComplianceLetterBlob;
            
            if (Test.IsRunningTest())
            {
                pdfComplianceLetterBlob =Blob.valueOf('UNIT.TEST');
            }
            else
            {
                //System.Debug('<<<<<<< Page about to be loaded : ' + pdfComplianceLetter + ' >>>>>>');
                pdfComplianceLetterBlob = pdfComplianceLetter.getContent();
                
            }
            
            
            String tempAttachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
            //System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :inspection.ID AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size() > 0)
            {
                if(prevAttachments.size() == 1){attachmentName = attachmentName + '_v2';}
                else
                {
                    for(Integer i = 0; i < prevAttachments.size(); i++ )
                    {
                        if(i == prevAttachments.size() - 1)
                        {
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');
                            attachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;
                            System.Debug('<<<<<<< New Attachment Name : ' + attachmentName + ' >>>>>>');
                        }
                    }
                }
            }
            
            attachmentName = attachmentName + '.pdf';
            Attachment attachment = new Attachment(parentId = inspection.ID, name = attachmentName, body = pdfComplianceLetterBlob);
            insert attachment;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Inspection Letter attached successfully.'));
            
            
        }
        catch(Exception e)
        {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference saveDraft()
    {
        try{
            
            update inspection;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft saved successfully.'));
            
        }
        catch(Exception e)
        {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    
}