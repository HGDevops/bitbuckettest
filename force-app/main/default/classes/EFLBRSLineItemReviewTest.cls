@IsTest
public class EFLBRSLineItemReviewTest {
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    static testMethod void testHappyPath() {
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        user portalUser = new User();
        portalUser = EFLUserTestDataFactory.getUser('eFile Applicant');
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        Application__c app = new Application__c();
        app = testData.newapplication();
        Program_Line_Item_Pathway__c plip = new Program_Line_Item_Pathway__c();
        plip = testData.newBRSPathway();
        Regulated_Article__c ra = new Regulated_Article__c();
        ra = testData.newRegulatedArticle(plip.id);
        AC__c lineItem = new AC__c();
        lineItem = testData.newLineItemBRSByRA('Personal Use', app, ra.Id);
        Link_Regulated_Articles__c lra = new Link_Regulated_Articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(lineitem.Id, ra.Id, 'Draft');
        Article_Supplier_Developer__c asd = new Article_Supplier_Developer__c();
        asd = testdata.newArticlSupplierByLineItem(LineItem.id);
        construct__c construct = new construct__c();
        construct = testData.newconstructByLIIdLRAId(lineitem.id, ra.Id, 'Saved');
        Phenotype__c phenotype = new Phenotype__c();
        phenotype = testdata.newphenotype(construct.id);
        GenotypeType__c genotypeType = new GenotypeType__c(Genotype_Category__c='Empty Transformation Vector', Construct__c=construct.id);
        insert genotypeType;
        genotype__c genotype = new genotype__c();
        genotype = testdata.newgenotype(construct.id, genotypeType);
        Construct_Application_Junction__c caj = new Construct_Application_Junction__c();
        caj = testdata.newPreConstructByLIIdConIdStatus(LineItem.id, construct.Id, 'Draft');
        location__c location = new location__c();
        location =  testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Saved');
        test.startTest();
        ApexPages.currentPage().getParameters().put('LineItemId',LineItem.Id);
        EFLBRSLineItemReviewController reviewController = new EFLBRSLineItemReviewController();
    //    string lineitemtext = reviewController.lineitemtext;
        List<string> allInstructions = new List<String>();
        allInstructions = reviewController.allInstructions;
        EFLBRSLineItemReviewController.instructionsWrapper insWrapper = new EFLBRSLineItemReviewController.instructionsWrapper();
        List<EFLBRSLineItemReviewController.instructionsWrapper> instructionsWrapperList = new List<EFLBRSLineItemReviewController.instructionsWrapper>();
        instructionsWrapperList = reviewController.instructionsWrapperList;
        string certifyStatementInstruction = reviewController.certifyStatementInstruction;
        string noArticlesText = reviewController.noArticlesText;
        String noLocationEntriesText = reviewController.getNoLocationEntriesText();
        List<Link_Regulated_Articles__c> linkRAList = reviewController.linkRAList;
        List<String> regulatedArticleFieldList = reviewController.regulatedArticleFieldList;
        Map<string, string> RegulatedArticleHeaderMap = reviewController.RegulatedArticleHeaderMap;
        reviewController.getconstructActionRequired();
        reviewController.refreshPageSize();
        List<String> LocationFieldList = reviewController.LocationFieldList;
        reviewController.getLocationActionRequired();
        reviewController.getFilteredArticleSupplierRecords();
        reviewController.searchArticleSuppliers();
        reviewController.searchString = 'Test';
        reviewController.searchArticleSuppliers();
        reviewController.refreshArticlePageSize();
        reviewController.searchString = '';
        reviewController.getConstructs();
        reviewController.getSubmittedConstructs();
        reviewController.showAll();
        reviewController.searchString = 'Test';
        reviewController.getConstructs();
        reviewController.getSubmittedConstructs();
        reviewController.showAll();
        reviewController.refreshSubPageSize();
        reviewController.searchString = '';
        reviewController.showAllPSCUsed();
        reviewController.searchString = 'Test';
        reviewController.showAllPSCUsed();
        reviewController.searchString = '';
        reviewController.getFilteredLocationWrappers();
        reviewController.searchLocations();
        reviewController.searchString = 'Test';
        reviewController.getFilteredLocationWrappers();
        reviewController.searchLocations();
        reviewController.refreshLocationPageSize();
        List<Location__c> LocationList = reviewController.LocationList;
        reviewController.docLauncherCBI();
        reviewController.docLauncherCBIDel();
        reviewController.docLauncherNoCBI();
        reviewController.certifyAction();
        reviewController.redirectToApplicationPDF();
        reviewController.redirectToCBIDeletedApplicationPDF();
        reviewController.getPageController();
        lra.Status__c = 'Draft';
        update lra;
        lineItem.Status__c = 'Saved';
        update lineItem;
        construct.Status__c = 'Waiting on Customer';
        update construct;
        location.Status__c = 'Waiting on Customer';
        update location;
        caj.Status__c = 'Denied';
        update caj;
        lineItem.Certification__c = 'Agree';
        lineItem.Status__c = 'Waiting on Customer';
        lineItem.Type_of_Permit__c = 'Notification';
        update lineItem;
       allInstructions = reviewController.allInstructions;
        instructionsWrapperList = new List<EFLBRSLineItemReviewController.instructionsWrapper>();
        instructionsWrapperList = reviewController.instructionsWrapperList;
        reviewController.resetCertification();
        reviewController.redirectToApplicationReview();
        lineItem.Status__c = 'Ready to Submit';
        update lineItem;
        reviewController.lineItemRecord.Type_of_Permit__c = 'Notification';
        reviewController.lineItemRecord.Status__c = 'Waiting on Customer';
        reviewController.hasInstruction = true;
        allInstructions = reviewController.allInstructions;
        reviewController.resetCertification();
        reviewController.docLauncherCBIDel();
        reviewController.docLauncherNoCBI();
        reviewController.docLauncherCBI();
        
        ApexPages.currentPage().getParameters().put('id',LineItem.Id);
        EFLBRSLineItemCertification certify = new EFLBRSLineItemCertification();
        certify.submitLineItem();
        test.stopTest();
        
    }
    
}