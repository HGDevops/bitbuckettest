public with sharing class CARPOL_BRS_PermitDates_extension {

    public Authorizations__c auth {get; set;}
    public String AuthorizationID = ApexPages.currentPage().getParameters().get('id');
    public Boolean showSaveDateBtn{get;set;}
    public Date auDate {get;set;}
    public Date origExp {get;set;}
    public String test{get;set;}
    PageReference parentPage;
    public CARPOL_BRS_PermitDates_extension(ApexPages.StandardController stdController) {
        showSaveDateBtn = false;
        if(ApexPages.currentPage().getParameters().get('saved')=='true')
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
        parentPage= ApexPages.currentPage();
        this.auth = (Authorizations__c)stdController.getRecord();        
        if(auth.ID != NULL){
            auth = [SELECT Effective_Date__c, pathway_exp_days__c,Expiration_Timeframe__c,Program_Pathway__r.Effective_Date_Equals__c,Date_Issued__c,Manually_Set_Expiration__c,Expiration_Timeframe_Override__c,Manually_Set_Effective__c, Expiration_Date__c,Expiry_Date__c FROM Authorizations__c WHERE ID =: auth.ID];
            auth.date_Issued__c = system.today();
            //origExp = auth.Expiration_Date__c;
            origExp = auth.Expiration_Date__c ;

        }
        getAuDate();
        /*if(auth.pathway_exp_days__c!=null && auth.Expiry_Date__c ==null){
            auth.Expiry_Date__c = System.Today().AddDays(integer.valueOf(auth.pathway_exp_days__c));
        }else if(auth.Expiry_Date__c == null) {
                 auth.Expiry_Date__c = System.Today().AddDays(30); }
              
        if(auth.Effective_Date__c == null) {       
            auth.Effective_Date__c = System.Today();}*/
     }





    public PageReference save(){
            try{
                        PageReference pgRef = parentPage;
                        //pgRef.getParameters().put('id',auth.id);
                        pgRef.getParameters().put('saved','true');
                        pgRef.setRedirect(true);
                        if(showSaveDateBtn == TRUE){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
                        }
                        showSaveDateBtn =false;
                       //system.debug('++++ au.Expiration_Date__c before Save: '+ auth.Expiration_Date__c );
                        /*if(auth.Expiration_Timeframe_Override__c != 'Manual'){
                            auth.Expiration_Date__c = origExp;
                        }
                       //system.debug('++++ au.Expiration__c before Save reverted: '+ auth.Expiration_Date__c );
                        update auth;*/
                        if(auth.Expiration_Timeframe_Override__c != 'Manual'){
                            origExp = auDate;
                        }
                        else{
                            origExp = auth.Expiration_Date__c ;
                        }
                        getAuDate();
                        update auth;

                       //system.debug('++++ au.Expiration__c after Save: '+ auth.Expiry_Date__c);
                        ApexPages.StandardController sc = new ApexPages.StandardController(auth);
                        CARPOL_BRS_PermitDates_extension ext = new CARPOL_BRS_PermitDates_extension (sc);
                        return pgRef;
        }
        catch(exception Ex){
            return null;
        
        }
    }
    /*
    
    {
         
                try{
                        
                        if(showSaveDateBtn == TRUE){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
                        }
                        showSaveDateBtn =false;
                        if(auth.Expiration_Timeframe_Override__c != 'Manual'){
                            auth.Expiry_Date__c = origExp;
                        }
                        update auth;
                        if(auth.Expiration_Timeframe_Override__c != 'Manual'){
                            origExp = auDate;
                        }
                        else{
                            origExp = auth.Expiry_Date__c;
                        }
                        
        }
        catch(exception Ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        
        }
                    return null;

        
      */  
        /*try{
            showSaveDateBtn=false;   // VV added for 17737 to show the Save Dates button dynamically
            upsert auth;
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
             
            return null;
        }
        catch(Exception e)
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        } */
    //}    
    public pageReference getShowSaveDateBtn(){ // VV added for 17737 to show the Save Dates button dynamically
        if(!showSaveDateBtn){showSaveDateBtn=TRUE;}
        return null;
    }
        public pageReference getAuDate(){
        Integer ExpirationTimeframe;
                    auDate =  Date.newInstance(auth.Effective_Date__c.Year(),auth.Effective_Date__c.month(), auth.Effective_Date__c.day());
                                     
                    if(auth.Expiration_Timeframe_Override__c == 'Manual'){
                    }                    
                    else if(auth.Expiration_Timeframe_Override__c == 'Pathway' || auth.Expiration_Timeframe_Override__c == null){
                        //ExpirationTimeframe = integer.valueOf(auth.pathway_exp_days__c);
                        ExpirationTimeframe = integer.valueOf(auth.Expiration_Timeframe__c); 
                        auDate = auDate.addDays(ExpirationTimeframe);
                    }
                    else{
                    if(auth.Expiration_Timeframe_Override__c !=null){
                        ExpirationTimeframe = integer.valueOf(auth.Expiration_Timeframe_Override__c.substringBefore(' '));
                        if(auth.Expiration_Timeframe_Override__c.contains('Month')){
                            auDate = auDate.addMonths(ExpirationTimeframe);
                        }
                        else if(auth.Expiration_Timeframe_Override__c.contains('Year')){
                            auDate = auDate.addYears(ExpirationTimeframe);
                        }
                    } 
                    }
                    getShowSaveDateBtn();
                    return null;
                    }

// keerthi changes for W-017319 //
 public void saveIssuedDt(){
         if (auth.status__c != EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS ISSUED')){
          auth.date_Issued__c = system.today();
          auth.Effective_Date__c = system.today();
          //update au;
          } 
     if(auth.Expiration_Timeframe_Override__c != 'Manual'){
                            origExp = auDate;
                        }
                        else{
                            origExp = auth.Expiration_Date__c;
                        }
                        getAuDate();
                       // update auth;
 Try{
EFLEnforceAccessUtility.checkObjectUpdateAccess('Authorizations__c');
UPDATE auth;
  }catch(exception e){
EFLErrorLog.createErrorLog('CARPOL_BRS_PermitDates_extension.saveIssuedDt()',e);
}
 
 }   // keerthi changes for W-017319 end// 

}