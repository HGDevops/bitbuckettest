public with sharing class CARPOL_BRSLinkConstructController{
    public Id LineItemId{get;set;}
    public Construct_Application_Junction__c obj{get;set;}
    public Id strPathwayId{get;set;}
    public string redirect{get;set;}
    public string applid{get;set;}
    public string contructs{get;set;}
    public string alerttype{get;set;}
    public string sop{get;set;}
    public Boolean showInstructions{get;set;}
    public Id sopid;
    public Id constjuncId;
    public string method;
    public string[] ps=new string[]{};
        public Construct_Application_Junction__c objconstjun{get;set;}
    public Construct_Application_Junction__c objconstJunEdit = new Construct_Application_Junction__c();
    //public string constructId{get;set;}
    
    
    public CARPOL_BRSLinkConstructController(){
        LineItemId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('appid'));
        strPathwayId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('strPathwayId'));
        redirect = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('redirect'));
        constjuncId = EFLGenericUtility.sanitizeString(apexpages.currentPage().getParameters().get('id'));
        method = EFLGenericUtility.sanitizeString(apexpages.currentPage().getParameters().get('method'));
        if (constjuncId != null){
            objconstJunEdit = [select id, Design_Protocol_Record_SOP__c from Construct_Application_Junction__c where id =: constjuncId limit 1];
        }
        applid = EFLGenericUtility.sanitizeString(ApexPages.CurrentPage().getParameters().get('appid'));
        objconstjun=new  Construct_Application_Junction__c();
        objconstjun.Line_Item__c=applid;
        sopid = objconstJunEdit.Design_Protocol_Record_SOP__c;
        sop=sopid;
        showInstructions = true;
        
        
        //constructId=ApexPages.currentPage().getParameters().get('prevconstId');
        //contructs=ApexPages.currentPage().getParameters().get('prevconstId');
    }
    public List<Construct__c> getConstructDetails() {
        
        return EFLBRSLineItemUtility.getConstruct('id', contructs);
        //if (constructId == null) return EFLBRSLineItemUtility.getConstruct('id', 'a0Er0000000RAVe');
        //return EFLBRSLineItemUtility.getConstruct('id', constructId);
    }
    public List<RecordType> getGenoRecordtypes() {
        
        //return [SELECT ID,Name,Line_Item__c,Name__c,Construct_s__c,Mode_of_Transformation__c,CreatedDate from Construct__c where id='a0Er0000000RAVe']; 
        //return EFLBRSLineItemUtility.getConstruct('id', contructs);
        //if (constructId == null) return EFLBRSLineItemUtility.getConstruct('id', 'a0Er0000000RAVe');
        
        return EFLBRSLineItemUtility.GenoRecordtypes(contructs);
        
        //return EFLBRSLineItemUtility.GenoRecordtypes((Id)constructId);
        
    }
    public List<Genotype__c> getgenotypes() {      
        // return EFLBRSLineItemUtility.genotypes(contructs);
        return EFLConstructRepository.selectGenotypesByConstruct(contructs); 
    } 
    public List<Applicant_Attachments__c> getappAttachs() {
        if (sop != '--None--'){
            return EFLBRSLineItemUtility.getAppAttachmentsBRS(sop);
        }
        else
            return null;
        //if (constructId == null) return EFLBRSLineItemUtility.getConstruct('id', 'a0Er0000000RAVe');
        //return EFLBRSLineItemUtility.getConstruct('id', constructId);
    }
    /**********    
public PageReference reloadPage(){
PageReference pagref= new  PageReference('/apex/carpol_brslinkconstruct');
pagref.getParameters().put('appid', applid);
pagref.getParameters().put('redirect','yes');        
pagref.getParameters().put('prevconstId', contructs);

pagref.setRedirect(true);

return pagref;
}
**************/
    public string[] getContructs(){
        return ps;
    }
    
    public void setContructs(string[] ps){
        this.ps=ps;
    }
    
    public string[] getSOP(){
        return ps;
    }
    
    public void setSOP(string[] ps){
        this.ps=ps;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> op = new List<SelectOption>();
        op.add(new SelectOption('--None--', '--None--'));
        List<Construct__c> constructList = EFLConstructUtility.getPreviouslyReviewedConstructs(lineitemid);
        for (Construct__c c : constructList) {
            op.add(new SelectOption(c.Id, c.Construct_s__c));
        }
        
        return op;
    }
    
    public List<SelectOption> getSOPs(){
        
        List<SelectOption> sopSelect=new  List<SelectOption>();
        sopSelect.add(new  SelectOption('--None--','--None--'));
        map<Id, Construct_Application_Junction__c> mapPrevSOPList = new map<Id, Construct_Application_Junction__c>();
        list<Construct_Application_Junction__c> sopDesignRec = [select Id,
                                                                Design_Protocol_Record_SOP__c 
                                                                from Construct_Application_Junction__c 
                                                                where Line_Item__c =: applid];
        list<Applicant_Attachments__c> SOPlist=new  list<Applicant_Attachments__c>();
        
        SOPlist=[SELECT Id,
                 Name,
                 File_Name__c 
                 FROM Applicant_Attachments__c 
                 WHERE Status__c=:'Review Complete' 
                 AND Decision__c=:'Authorized'];
        
        for(Construct_Application_Junction__c s:sopDesignRec){
            mapPrevSOPList.put(s.Design_Protocol_Record_SOP__c, s);
        }
        
        for(Applicant_Attachments__c s:SOPlist){
            string fName = s.File_Name__c;
            if(!(mapPrevSOPList.containsKey(s.id))){
                if (fName != null) {
                    sopSelect.add(new  SelectOption(s.Id,s.Name + ' - ' + fName));
                }
                else{
                    sopSelect.add(new  SelectOption(s.Id,s.Name));
                }
            }
            
        }
        sop=sopid;
        return sopSelect;
    }
    
    
    
    
    public PageReference cancel(){
        if(redirect=='yes'){
            PageReference pg=new  PageReference('/apex/CARPOL_LineItemPageForApplicant'); 
            pg.getParameters().put('LineId',LineItemId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }
        PageReference pagref=new  PageReference('/'+applid);
        return pagref;
    }
    
    public PageReference save(){
        if(contructs=='--None--'||sop=='--None--'){
            alerttype = 'danger';
            ApexPages.addmessage(new  ApexPages.message(ApexPages.severity.ERROR,'Please select a value from the dropdown'));
            return null;
        }
        if(contructs!=null){
            list<Construct_Application_Junction__c> conJunRec = [select Id 
                                                                 from Construct_Application_Junction__c 
                                                                 where Line_Item__c =: applid 
                                                                 and Construct__c =: contructs];
            if(contructs=='--None--'){
                alerttype = 'danger';
                ApexPages.addmessage(new  ApexPages.message(ApexPages.severity.ERROR,'Please select a value from the dropdown'));
                return null;
            }else if(conJunRec.size() > 0){
                alerttype = 'danger';
                ApexPages.addmessage(new  ApexPages.message(ApexPages.severity.ERROR,'This construct has already been added. Please select another and try again.'));
                return null;
            } else{
                objconstjun.Construct__c=contructs;
            }
            if(method == 'edit'){
                objconstJunEdit.Construct__c = contructs;
                update objconstJunEdit;
            }
            else{
                Insert objconstjun;
            }
            
            AC__c objLineItem = [select Id, construct_status__c from AC__c where id =: LineItemId limit 1];
            objLineItem.construct_status__c = 'Ready to Submit';
            update objLineItem;
        }
        else if(sop!=null){
            
            list<Construct_Application_Junction__c> sopDesignRec = [select Id 
                                                                    from Construct_Application_Junction__c 
                                                                    where Line_Item__c =: applid 
                                                                    and Design_Protocol_Record_SOP__c =: SOP];
            if(sop=='--None--'){
                alerttype = 'danger';
                ApexPages.addmessage(new  ApexPages.message(ApexPages.severity.ERROR,'Please select a value from the dropdown'));
                return null;
            }
            else if (sopDesignRec.size() > 0){
                alerttype = 'danger';
                ApexPages.addmessage(new  ApexPages.message(ApexPages.severity.ERROR,'This SOP has already been added. Please select another and try again.'));
                return null;
            }
            else {
                objconstjun.Design_Protocol_Record_SOP__c=SOP;
            }
            
            if(method == 'edit'){
                objconstJunEdit.Design_Protocol_Record_SOP__c = sop;
                update objconstJunEdit;
            }
            else{
                Insert objconstjun;
            }
            
        }
        
        if(redirect=='yes'){
            PageReference pg=new  PageReference('/apex/CARPOL_LineItemPageForApplicant');
            pg.getParameters().put('LineId',LineItemId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }
        PageReference pagref=new  PageReference('/'+applid);
        return pagref;
    }
    
    /**
* Document Wrapper : to collect document information
**/ 
    public class documentResource{
        public ID docID{get;set;}
        public Boolean isUploaded{get;set;}
        public string docName{get;set;}
        public string docURL{get;set;}
    }
}