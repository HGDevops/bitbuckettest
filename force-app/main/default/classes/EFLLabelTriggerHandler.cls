public class EFLLabelTriggerHandler implements EFLITrigger {

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
* Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        if (EFLGenericUtility.isTriggerDisabled('EFLLabelTrigger'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    /*
* Constructor
*/ 
    public EFLLabelTriggerHandler()
    {
        
    }
    
    /**
* bulkBefore
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
        
    }
    
    /**
* bulkAfter
* This method is called prior to execution of an AFTER trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkAfter()
    {
        system.debug('entered label bulk after');
        list<Label__c> labelsVoided = new list<Label__c>();
        list<Id> authIds = new list<Id>();
        map<Id, set<decimal>> voidedAuthLabels = new map<Id, set<decimal>>();
        if(trigger.IsUpdate){
            string voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
            string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
            list<Label__c> newLabels = (list<Label__c>) trigger.new;
            map<Id, Label__c> oldMap = (map<Id, Label__c>)trigger.oldmap;
            
            for(Label__c lab : newLabels){
                if(lab.Status__c == voided && oldMap.get(lab.Id).Status__c == active){
                    labelsVoided.add(lab);
                    if(lab.Authorization__c != null){
                        if(!voidedAuthLabels.containsKey(lab.Authorization__c)){
                            voidedAuthLabels.put(lab.Authorization__c, new set<decimal>());
                        }
                    	voidedAuthLabels.get(lab.Authorization__c).add(lab.Label_Number__c);
                    }
                }
            }
            system.debug('voidedAuthLabels: ' + voidedAuthLabels);
            EFLLabelServices.EMAILS_INVOKED = voidedAuthLabels.size();
            if(!voidedAuthLabels.isEmpty()){
                // this is a list of authorizations & corresponding labels that need to be emailed to applicants.
                EFLLabelServices.sendLabelExpirationEmails(JSON.serialize(voidedAuthLabels));
            }
        }
    }
    
    
    /**
* beforeInsert
* This method is called iteratively for each record to be inserted during a BEFORE
* trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
*/
    public void beforeInsert(SObject so)
    {
    }
    
    /**
* beforeUpdate
* This method is called iteratively for each record to be updated during a BEFORE
* trigger.
*/
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
* beforeDelete
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete(SObject so)
    {
        
    }
    
    /**
* afterInsert
* This method is called iteratively for each record inserted during an AFTER
* trigger. Always put field validation in the 'After' methods in case another trigger
* has modified any values. The record is 'read only' by this point.
*/ 
    public void afterInsert(SObject so)
    {
       
    }
    
    /**
* afterUpdate
* This method is called iteratively for each record updated during an AFTER
* trigger.
*/
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
* afterDelete
* This method is called iteratively for each record deleted during an AFTER
* trigger.
*/ 
    public void afterDelete(SObject so)
    {
    }
    
    /**
* andFinally
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    public void andFinally()
    {
        
    }
}