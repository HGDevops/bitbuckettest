public inherited sharing Class EFLContactRepository
{
    
    /* @purpose: returns Contacts record for Current User's Direct Account.               
*/     
    public static List<Contact> selectActiveUserContactsByCurrentUserDirectAccount(){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            System.debug('selectActiveUserContactsByCurrentUserDirectAccount.EFLUserUtility.contactDetails.AccountId' + EFLUserUtility.contactDetails.AccountId);
            return [SELECT ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, 
                    Email, (select Id, AccountId, ContactId, isDirect from AccountContactRelations ORDER BY CreatedDate ASC), 
                    (select Id, isActive, contactId from users where isActive = true ORDER BY CreatedDate DESC limit 1) FROM Contact 
                    WHERE Id in (select contactId from AccountContactRelation 
                                 where AccountId = :EFLUserUtility.contactDetails.AccountId) 
                    AND Id in (select contactId from user where isActive = true)
                   ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactRepository.selectActiveUserContactsByCurrentUserDirectAccount()',e);                
        } 
        return null;
    }
    
    /* @purpose: returns Contact/User/ACR record for ContactId.               
*/    
    public static contact selectContactAccountRelationsByContactId(id contactId){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            return [SELECT ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, 
                    (select Id, AccountId, ContactId, isDirect from AccountContactRelations ORDER BY CreatedDate ASC),
                    (select id, username, contactId, AccountId, FirstName, LastName, usertype, profileid, isActive, 
                     profile.name from Users where isActive = true ORDER BY CreatedDate DESC limit 1)
                    FROM Contact WHERE Id = :contactId limit 1
                   ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactRepository.selectContactAccountRelationsByContactId()',e);                
        } 
        return null;
    }
    
    /* @purpose: returns Contacts record by username.               
*/     
    public static List<Contact> selectContactsUsersAccountRelationByUserNames(List<string> usernames){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            return [select ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, Email,  
                    (select id, username, contactId, AccountId, FirstName, LastName, usertype, profileid, isActive, 
                     profile.name from Users where isActive = true ORDER BY CreatedDate DESC limit 1), 
                    (select Id, AccountId, ContactId, isDirect from AccountContactRelations ORDER BY CreatedDate ASC) 
                    from contact where 
                    id in (select contactId from user where isActive = true and username in :usernames)
                   ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactRepository.selectContactsUserAccountRelationByUserName()',e);                
        } 
        return null;
    }
    
    /* @purpose: returns ContactId for singleEmailMessage.settargetObjectId.               
*/    
    public static id contactIdForsettargetObjectId(){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            return [SELECT ID, Email FROM Contact WHERE email != null limit 1].id;
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactRepository.selectContactAccountRelationsByContactId()',e);  
            throw e;
        } 
    }
    
}