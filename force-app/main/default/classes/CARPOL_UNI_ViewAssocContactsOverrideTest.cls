@IsTest
public class CARPOL_UNI_ViewAssocContactsOverrideTest {
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    @isTest
    static void testOverride() {
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        system.runAs(usershare)
        {
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            ApexPages.StandardController standrdcontroller = new ApexPages.StandardController(associatedContact);
            CARPOL_UNI_ViewAssocContactsOverride AssocitatedContactViewOverride =  new CARPOL_UNI_ViewAssocContactsOverride(standrdcontroller);
            AssocitatedContactViewOverride.redirect();
        }
        	Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
       		ApexPages.StandardController standrdcontroller = new ApexPages.StandardController(associatedContact);
            CARPOL_UNI_ViewAssocContactsOverride AssocitatedContactViewOverride =  new CARPOL_UNI_ViewAssocContactsOverride(standrdcontroller);
            AssocitatedContactViewOverride.redirect();
        
    }

}