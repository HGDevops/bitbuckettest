public inherited sharing class EFLAccountRepository {
/* @Purpose: Returns Accounts record for Current User's Direct Account.               
   @Author: Ravee Racharla  @Date:4/5/2019	
*/
  public static List<Account> selectActiveUserAccountsByCurrentUserDirectAccount(){
      try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Account');
            return [SELECT ID, Name, (Select  AccountId, ContactId, IsDirect from AccountContactRelations)   from Account
                   ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLAccountRepository.selectActiveUserAccountsByCurrentUserDirectAccount()',e);                
        } 
        return null;
      
      
  }
    
}