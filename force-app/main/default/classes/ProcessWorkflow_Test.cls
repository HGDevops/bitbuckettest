@Istest
public class ProcessWorkflow_Test {
    public static testMethod void ProcessWorkflowMethod1(){
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'test';
        email.fromName = 'test test';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'user@acme.com';
        // setup controller object
        ProcessWorkFlow catcher = new ProcessWorkFlow();
        catcher.handleInboundEmail(email, envelope);
    
    }

}