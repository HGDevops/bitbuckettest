@isTest(seealldata=true)
private class EFLManageLabelsExtensionPDF_Test {
     @IsTest static void EFLManageLabelsExtensionPDF_Test() {
      
     CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
     testData.insertcustomsettings();
     Application__c objapp = testData.newapplication();
     Authorizations__c objauth = testData.newAuth(objapp.Id); 
     Label__c onelabel = new Label__c();
     onelabel.Authorization__c = objauth.id;
     onelabel.Name = '140-testlabel1';
     insert onelabel;
     objauth.Label_Colors__c = 'Red and White';
     update objauth;
     Test.startTest();
     PageReference pageRef = Page.CARPOL_BRS_PDF_Labels;
     Test.setCurrentPage(pageRef);
     ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
     EFLManageLabelsExtensionPDF EFLml =  new EFLManageLabelsExtensionPDF(sc);
     EFLml.getlabelList();
     EFLml.addlabels();
     EFLml.labelfinallist1();
     PageReference pgr1 = EFLml.viewDraftPDF();      
     PageReference pgr2 = EFLml.attachPDF();      
     PageReference pgr3 = EFLml.cancel();      
     PageReference pgr4 = EFLml.Createlabels();      
     PageReference pgr5 = EFLml.sendlabels();      
     PageReference pgr6 = EFLml.Redirect();      
     PageReference pgr7 = EFLml.deleteRec();      
     PageReference pgr8 = EFLml.voidlabels();  
     String labelguidanceContent = EFLml.labelGuidanceContent;
     Test.stopTest();
      }
      
@IsTest static void EFLManageLabelsExtensionPDFBRS_Test() {
      
     CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
     testData.insertcustomsettings();
     Application__c objapp = testData.newapplication();
     Authorizations__c objauth = testData.newAuth(objapp.Id); 
     Label__c onelabel = new Label__c();
     onelabel.Authorization__c = objauth.id;
     onelabel.Name = '140-testlabel1';
     insert onelabel;
     objauth.Label_Colors__c = 'Black and White';
     objauth.Status__c = 'Approved';
     objauth.Expiration_Date__c = Date.Today();
     update objauth;
     
     Test.startTest();
     PageReference pageRef = Page.CARPOL_BRS_PDF_Labels;
     Test.setCurrentPage(pageRef);
     ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
     EFLManageLabelsExtensionPDF EFLml =  new EFLManageLabelsExtensionPDF(sc);
     EFLml.getlabelList();
     EFLml.addlabels();
     EFLml.btntype='All';
     EFLml.labelfinallist1();
     PageReference pgr1 = EFLml.viewDraftPDF();      
     PageReference pgr2 = EFLml.attachPDF();      
     PageReference pgr3 = EFLml.cancel();      
     PageReference pgr4 = EFLml.Createlabels();      
     PageReference pgr5 = EFLml.sendlabels();      
     PageReference pgr6 = EFLml.Redirect();      
     PageReference pgr7 = EFLml.deleteRec();      
     PageReference pgr8 = EFLml.voidlabels();  
     String labelguidanceContent = EFLml.labelGuidanceContent;
     //add labels and create them
     EFLml.AdditionalLabels = 2;
     EFLml.labelcolor = 'Blue and White';
     pgr4 = EFLml.Createlabels();
     EFLml.authorization = new Authorizations__c();
     EFLml.sobjectkeys = new Map<string, string>();
     Test.stopTest();
      }      
      
   }