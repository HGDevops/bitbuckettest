/**
 * This class asserts logic for EFLUploadedFileTrigger & EFLUploadedFileTriggerHandler.
 * The trigger uses Custom Metadata to map values from the parent record to the Uploaded_File__c record created BEFORE INSERT
 */

@isTest
public class EFLUploadedFileTriggerTests {

    private static final string AccountName = 'Test Account';
    private static final string AccountDescription = 'Test Account Description';
    private static final string ContactLastName = 'Smith';
    
    @TestSetup
    static void makeData(){
        // Make a parent object for Uploaded_File__c to be created under in test execution.
        Account account = new Account();
        account.Name = AccountName;
        account.Description = AccountDescription;
        insert account;
        
        Contact contact = new Contact();
        contact.LastName = ContactLastName;
        contact.AccountId = account.Id;
        insert contact;
    }
    
    @isTest
    static void insertMultipleRecordsForTwoObjects_FieldsMapped(){
        Account account = [Select Id, Name, Description From Account Limit 1];
 		Contact contact = [Select Id, Name From Contact Limit 1];
        
        // Insert two upload file records for an account and one for a contact 
        List<Uploaded_File__c> uploadedFiles = new List<Uploaded_File__c>();
        Uploaded_File__c ufForAccount1 = new Uploaded_File__c(Name = 'ufForAccount1', ParentId__c = account.Id);
        Uploaded_File__c ufForAccount2 = new Uploaded_File__c(Name = 'ufForAccount2', ParentId__c = account.Id);
        Uploaded_File__c ufForContact = new Uploaded_File__c(Name = 'ufForContact', ParentId__c = contact.Id);
        uploadedFiles.add(ufForAccount1);
        uploadedFiles.add(ufForAccount2);
        uploadedFiles.add(ufForContact);
        insert uploadedFiles;
        
        Map<Id, Uploaded_File__c> insertedRecords = new Map<Id, Uploaded_File__c>([Select Id, Name, Animal_Type__c, Description__c From Uploaded_File__c]);
        
        System.assertEquals(AccountName, insertedRecords.get(ufForAccount1.Id).Animal_Type__c);
        System.assertEquals(AccountDescription, insertedRecords.get(ufForAccount1.Id).Description__c);
        System.assertEquals(AccountName, insertedRecords.get(ufForAccount2.Id).Animal_Type__c);
        System.assertEquals(AccountDescription, insertedRecords.get(ufForAccount2.Id).Description__c);
        System.assertEquals(ContactLastName, insertedRecords.get(ufForContact.Id).Animal_Type__c);
        System.assertEquals('N/A', insertedRecords.get(ufForContact.Id).Description__c);
    }

    @isTest
    static void insertFullBatch_FieldsMapped(){
        Account account = [Select Id, Name, Description From Account Limit 1];
 		Contact contact = [Select Id, Name From Contact Limit 1];
        
        // Insert two upload file records for an account and one for a contact 
        List<Uploaded_File__c> uploadedFiles = new List<Uploaded_File__c>();
        for(Integer i = 0; i < 110; i++) {
	        Uploaded_File__c ufForAccount = new Uploaded_File__c(Name = 'ufForAccount', ParentId__c = account.Id);
	        uploadedFiles.add(ufForAccount);
        }
        for(Integer i = 0; i < 110; i++) {
 	       Uploaded_File__c ufForContact = new Uploaded_File__c(Name = 'ufForContact', ParentId__c = contact.Id);
	        uploadedFiles.add(ufForContact);
        }
        insert uploadedFiles;
        
        List<Uploaded_File__c> insertedRecords = [Select Id, Name, Animal_Type__c, Description__c From Uploaded_File__c];

        Integer verified = 0;
        for(Uploaded_File__c uf :insertedRecords) {
            if (uf.Name == 'ufForAccount') {
                System.assertEquals(AccountName, uf.Animal_Type__c);
                System.assertEquals(AccountDescription, uf.Description__c);
                verified += 1;
            }
            else {
                System.assertEquals(ContactLastName, uf.Animal_Type__c);
                System.assertEquals('N/A', uf.Description__c);
                verified += 1;
            }
        }

        System.assertEquals(220, verified);
    }

	@isTest
    static void deleteUploadedFileTest(){
        Account account = [Select Id, Name, Description From Account Limit 1];
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        // Insert upload file record for an account
        Uploaded_File__c uploadedFile = new Uploaded_File__c(Name = 'ufForAccount1', 
                                                              ParentId__c = account.Id,
                                                              File_Preview_Id__c = documents[0].Id
                                                             );
        insert uploadedFile;
        
        uploadedFile.Is_Deletable__c = true;
        uploadedFile.Is_Deleted__c = true;
        uploadedFile.External_File_Id__c = null;
        update uploadedFile;
        
        List<Uploaded_File__c> retrievedFiles = [SELECT Id FROM Uploaded_File__c];
        System.assertEquals(0, retrievedFiles.size());
        
        documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE Id = :documents[0].Id];
        System.assertEquals(0, documents.size());
    }  
    
    @isTest
    static void deleteUploadedFileMultipleFilesTest(){
        Account account = [Select Id, Name, Description From Account Limit 1];        
 		Contact contact = [Select Id, Name From Contact Limit 1];
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;
        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'Test2',
            PathOnClient = 'Test2.jpg',
            VersionData = Blob.valueOf('Test2 Content'),
            IsMajorVersion = true
        );
        insert contentVersion2;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        // Insert upload file record for an account
        Uploaded_File__c uploadedFile = new Uploaded_File__c(Name = 'ufForAccount1', 
                                                              ParentId__c = account.Id,
                                                              File_Preview_Id__c = documents[0].Id
                                                             );
        // Insert two upload file records for an account and one for a contact 
        List<Uploaded_File__c> uploadedFiles = new List<Uploaded_File__c>();
        Uploaded_File__c ufForAccount1 = new Uploaded_File__c(Name = 'ufForAccount1', ParentId__c = account.Id,
                                                              File_Preview_Id__c = documents[0].Id);
        Uploaded_File__c ufForContact = new Uploaded_File__c(Name = 'ufForContact', ParentId__c = contact.Id,
                                                              File_Preview_Id__c = documents[1].Id);
        uploadedFiles.add(ufForAccount1);
        uploadedFiles.add(ufForContact);
        insert uploadedFiles;
        
        //set deleted fields and update
        ufForAccount1.Is_Deletable__c = true;
        ufForAccount1.Is_Deleted__c = true;
        ufForAccount1.External_File_Id__c = null;
        ufForContact.Is_Deletable__c = true;
        ufForContact.Is_Deleted__c = true;
        ufForContact.External_File_Id__c = null;
        uploadedFiles = new List<Uploaded_File__c>();
        uploadedFiles.add(ufForAccount1);
        uploadedFiles.add(ufForContact);
        update uploadedFiles;
        
        List<Uploaded_File__c> retrievedFiles = [SELECT Id FROM Uploaded_File__c];
        System.assertEquals(0, retrievedFiles.size());
        
        documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(0, documents.size());
    }  
}