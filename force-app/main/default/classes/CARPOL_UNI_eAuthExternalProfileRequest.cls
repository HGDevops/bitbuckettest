Public without sharing class CARPOL_UNI_eAuthExternalProfileRequest{ 
    
    List<SelectOption> profiles = new List<SelectOption>();
    public String profileList { get; set; }
    public static List<SelectOption> profiles { get; set; }
    public String selectedValue { get; set; }
    
    
    public CARPOL_UNI_eAuthExternalProfileRequest(ApexPages.StandardController controller) {

    }

    
   public List<SelectOption> getProfileOptions() {
        system.debug('inside get profiles function');
          //List<SelectOption> options = new List<SelectOption>();
           profiles = new List<SelectOption>();
           profiles.add(new SelectOption('None','None'));
           // W-037841 removed Broker Preparer option
       	   //profiles.add(new SelectOption('Broker Preparer','Broker Preparer'));
           // W-025549 added two more options 
           profiles.add(new SelectOption('State Reviewer','State Reviewer'));
           profiles.add(new SelectOption('State Site Inspector','State Site Inspector'));
            system.debug('options '+ profiles);
           // return options;
        return profiles;
    }
    
    public PageReference submitRequest() {
        system.debug('selected profile ' + selectedValue);
       if (selectedValue !='None'){
        insertBrokerPreparer(selectedValue);
        // W-025549 customized msg to reflect the profile chosen
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, ' Your request for '+ selectedValue +' access has been submitted.');
         ApexPages.addMessage(myMsg);
        }
        return null;
    }

    public void insertBrokerPreparer(string profileRequest) {     
        
        Id que = [select Id from Group where Name = 'Broker Preparer Access' and Type = 'Queue' limit 1].id;
           
        Id userId = UserInfo.getUserID();
        user currentUser = [select id, contactid, FirstName, LastName, Email, Phone from user where id =: userid];
        contact con = new contact();
        system.debug('In the class > '+currentuser.contactid);
        if(currentuser.contactid != null )
        {
             con = [select id, Name, accountid from contact where id =: currentuser.contactid];
        }else{
            user newcurrentuser = [SELECT Id, ContactID, Name, Profile.UserLicense.Name FROM User WHERE Profile.UserLicense.Name = 'Customer Community Login' AND ContactId != null LIMIT 1];
           system.debug('inside null currentuser > '+newcurrentuser.ContactID);
           Id conid = newcurrentuser.ContactID;
            system.debug('inside null conid > '+conid);
             con = [SELECT ID, Name,accountid  FROM Contact WHERE ID =: conid limit 1];
             //con = [SELECT ID, Name,accountid  FROM Contact WHERE ID =: '003r0000003njixAAA' limit 1];
            system.debug('<inside null con> '+con);
        }
        system.debug('After the null check > '+con);
        account accnt ;
 
          accnt = [select id from account where id =: con.accountid];

        Preparer_Request__c brokPrep = new Preparer_Request__c();
        brokPrep.First_Name__c = currentuser.FirstName;
        brokPrep.Last_Name__c = currentuser.LastName;
        brokPrep.OwnerId = que;
        brokPrep.RecordTypeID =  Schema.Sobjecttype.Preparer_Request__c.getRecordTypeInfosByName().get('Profile Request').getRecordTypeId();
        system.debug('record type ' + brokPrep.RecordTypeID);
        brokPrep.Contact_Email__c = currentUser.Email;
        brokPrep.Contact_Phone__c = currentUser.Phone;
        brokPrep.Profile_Requested__c = profileREquest;
        brokPrep.Applicant_Account1__c = accnt.id;
        brokPrep.Preparer_Broker_Name__c = con.id;
        brokPrep.EFL_External_User__c = currentuser.id;
        //Assign to queue
        brokprep.OwnerId = [Select Queue.Id from QueueSObject where Queue.Name = 'Broker Preparer Access'].Queue.Id;
        insert brokPrep;
        system.debug('### brokprep created'+brokPrep.id);
        system.debug('record type ' + brokPrep.RecordType );
        //submitForApproval(userid, brokPrep.Id);
        
    }
    
   /* public void submitForApproval(Id userId, Id brokerPrepId) {
       
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(brokerPrepId);
        
        // Submit on behalf of a specific submitter
        approvalRequest.setSubmitterId(userId); 
        
        // Submit the record to specific process and skip the criteria evaluation
        approvalRequest.setProcessDefinitionNameOrId('EFL_Change_Profile_Request_Approval');
        approvalRequest.setSkipEntryCriteria(true);
        
        // Submit the approval request for the profile request
        Approval.ProcessResult result = Approval.process(approvalRequest);
        

    } */
 }