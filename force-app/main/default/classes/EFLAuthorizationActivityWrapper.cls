public inherited sharing class EFLAuthorizationActivityWrapper {
    
    public authorizations__c authRecord{get;set;} 
    public list<Task> newTasks{get;set;}  
    
}