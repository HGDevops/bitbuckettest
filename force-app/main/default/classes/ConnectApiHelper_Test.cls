@isTest(SeeAllData=true)
public class ConnectApiHelper_Test{
    static testMethod void ConnectApiHelper_TestMethod1(){ 
        //Subject for the Chatter Feed Post
        Opportunity o = new Opportunity();
        o.Name = 'Test';
        o.StageName = 'New';
        o.Amount = 10;
        o.CloseDate = Date.today();
        insert o;
        
        ID SubjectId = o.id;
        
        //Creating the feed
        FeedItem feed = new FeedItem();
        feed.parentid= SubjectId;
        feed.body = 'Hello';
        Insert feed;  
        
        //fetching a User to put in the body of the post
        Id UserId = [Select Id From User Where isActive=true Limit 1].id; 
               
        //creating the message for the post with mentions
        String message = 'Hello {'+UserId+'}, have you seen the group?'; 
        //creating the message for the post with image
        String img = '{img:'+feed.id+':description}';  
        
        //Creating a ConnectApi.FeedBody element of Text Segment type
        ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.MessageSegment msgSegment = new ConnectApi.TextSegment();        
        msgSegment.text = 'This is the text of the feed body';
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments;                                
              
        //Passing plain text data as feed body
        ConnectApiHelper.postFeedItemWithMentions('internal',String.valueOf(SubjectId),message);
        //Passing image as feed body
        ConnectApiHelper.postFeedItemWithRichText('internal',String.valueOf(SubjectId),img);
        //Passing plain text data as feed body
        ConnectApiHelper.postCommentWithMentions('internal',String.valueOf(feed.id),message);                                                
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);
    }

    static testMethod void ConnectedApiHelper_TestMethod2(){
        Opportunity o = new Opportunity();
        o.Name = 'Test';
        o.StageName = 'New';
        o.Amount = 10;
        o.CloseDate = Date.today();
        insert o;
        ID SubjectId = o.id;
        //Passing messgae with html tags as feed body
        ConnectApiHelper.postFeedItemWithMentions('internal',String.valueOf(SubjectId),'<p>Hello, have you seen the group?</p>');
        
        //Creating a ConnectApi.FeedBody element of Mention Segment type
        /*ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.MessageSegment msgSegment = new ConnectApi.MentionSegment();        
        msgSegment.text = 'This is the text of the feed body';
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments; 
        
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);*/
    }
    
     static testMethod void ConnectedApiHelper_TestMethod3(){ 
        //Creating a ConnectApi.FeedBody element of Hashtag Segment type       
        ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.MessageSegment msgSegment = new ConnectApi.HashtagSegment();        
        msgSegment.text = 'This is the text of the feed body';
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments; 
        
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);
    }
    
    static testMethod void ConnectedApiHelper_TestMethod4(){  
        //Creating a ConnectApi.FeedBody element of Link Segment type      
        ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.MessageSegment msgSegment = new ConnectApi.LinkSegment();        
        msgSegment.text = 'This is the text of the feed body';
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments; 
        
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);
    }
    static testMethod void ConnectedApiHelper_TestMethod5(){ 
        ////Creating a ConnectApi.FeedBody element of MarkupBegin Segment type       
        ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.MessageSegment msgSegment = new ConnectApi.MarkupBeginSegment();        
        msgSegment.text = 'This is the text of the feed body';
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments; 
        
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);
    }
    static testMethod void ConnectedApiHelper_TestMethod6(){
        //Creating a ConnectApi.FeedBody element of MarkupEnd Segment type        
        ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.MessageSegment msgSegment = new ConnectApi.MarkupEndSegment();        
        msgSegment.text = 'This is the text of the feed body';
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments; 
        
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);
    }
    static testMethod void ConnectedApiHelper_TestMethod7(){  
        //Creating a ConnectApi.FeedBody element of Inline Image Segment type      
        ConnectApi.FeedBody feedbody = new ConnectApi.FeedBody();
        List<ConnectApi.MessageSegment> msgsegments = new List<ConnectApi.MessageSegment>();
        ConnectApi.InlineImageSegment msgSegment = new ConnectApi.InlineImageSegment();        
        msgSegment.altText = 'This is the text of the feed body';
        msgSegment.url = 'https://www.desk.com/themes/bolt/assets/img/logo-desk@2x.png';
        msgSegment.fileExtension = 'png';
        
        ConnectApi.FilePreviewCollection filePrevs = new ConnectApi.FilePreviewCollection();
        filePrevs.url = 'https://www.desk.com/themes/bolt/assets/img/logo-desk@2x.png';      
        msgSegment.thumbnails=filePrevs;
        msgSegments.add(msgSegment);
        feedbody.messageSegments = msgSegments; 
        
        ConnectApiHelper.createFeedItemInputFromBody(feedbody);
        ConnectApiHelper.createCommentInputFromBody(feedbody);
    }
}