public without sharing class EFLContactUtility {
    
    /* @purpose: update contact record in without sharing context. */
    public static void UpdateContact(contact con)
    {
        try
        {
            update con;
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.UpdateContact()',e);             
            throw e;
        } 
    }
    
    /* @purpose: delete ACR record in without sharing context. */
    public static void DeleteAccountContactRelation(AccountContactRelation acr)
    {
        try
        {
            delete acr;
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.DeleteAccountContactRelation()',e);   
            throw e;
        } 
    }
    /* @purpose: delete ACR record in without sharing context. */
    public static void DeleteAccountContactRelationByList(list<AccountContactRelation> lacr)
    {
        try
        {
            delete lacr;
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.DeleteAccountContactRelation()',e);   
            throw e;
        } 
    }
    
    /* @purpose: transfering ownership by replacing user/contact lookups based on custom metadata setup - EFLOwnershipTransferSetup__mdt */
    // appIds should be left null for system wide app/child records transfer
    public static void transferOwnership(user sourceUser, user targetUser, string transferType, set<Id> appIds)
    {
        try
        {
            //get OwnershipTransferSetup records mapped by objectAPIName(key)
            Map<string, EFLOwnershipTransferSetup__mdt> setupMap = new Map<string, EFLOwnershipTransferSetup__mdt>();
            setupMap = EFLContactUtility.getOwnershipTransferSetupMap(transferType);
            //Looping OwnershipTransferSetup Map object by object
            for(string objectAPIName : setupMap.keyset())
            {
                //Each Object - OwnershipTransfer Setup record
                EFLOwnershipTransferSetup__mdt setup = setupMap.get(objectAPIName);
                //Form query for each objecttype
                if(setup.UserLookupsToTransfer__c!=null || setup.ContactLookupsToTransfer__c!=null)
                {
                    string query = 'SELECT id FROM ' + objectAPIName + ' WHERE ';
                    List<string> userFieldAPINames = new List<string>();
                    List<string> contactFieldAPINames = new List<string>();
                    query = query + '( ';
                    //Adding UserLookups filter - where conditions to get only records related to sourceUser
                    if(setup.UserLookupsToTransfer__c!=null)
                    {
                        userFieldAPINames = setup.UserLookupsToTransfer__c.split(',');
                        query = addFilters(query,userFieldAPINames,sourceUser.Id);
                    }
                    //Adding ContactLookups filter - where conditions to get only records related to sourceUser.contactId
                    if(setup.ContactLookupsToTransfer__c!=null)
                    {
                        contactFieldAPINames = setup.ContactLookupsToTransfer__c.split(',');
                        query = addFilters(query,contactFieldAPINames,sourceUser.ContactId);
                    } 
                    //Adding Application Applicant Contact Field filter - Applies to certians objects varies by metadata
                    //where conditions to get only records related to sourceUser.contactId
                    if(setup.ApplicationApplicantContactFieldPath__c!=null)
                    {
                        query = query + ' OR ' + setup.ApplicationApplicantContactFieldPath__c + ' = \'' + sourceUser.ContactId + '\' ';
                    }
                    query = query + ' ) ';
                    //App filter
                    if(appIds!=null && setup.ApplicationFieldPath__c!=null)
                    {
                        query = query + ' AND ' + setup.ApplicationFieldPath__c + ' IN :appIds';
                    }
                    //Setting multiple sobject record updates by looping as sobject list
                    List<SObject> sObjectsToUpdate = new List<SObject>();
                    
                    // NOTE: running SOQL in for-loop is OK in this scenario as it's a dynamic query
                    // and is limited by custom metadata type list.  This is how we avoid hard-coding the SOQL for related objects.
                    for(sObject sObj : database.query(query)) 
                    { 
                        SObject sObjToUpdate = sObj.Id.getSObjectType().newSObject(sObj.Id);
                        //setting userlookup field values with targerUser.Id
                        for(string userFieldAPIName : userFieldAPINames)
                        {
                            sObjToUpdate.put(userFieldAPIName, targetUser.Id); 
                        }
                        //setting userlookup field values with targerUser.contactId
                        for(string contactFieldAPIName : contactFieldAPINames)
                        {
                            sObjToUpdate.put(contactFieldAPIName, targetUser.ContactId);
                        }
                        sObjectsToUpdate.add(sObjToUpdate);
                    }
                    //sobject list update
                    if(!sObjectsToUpdate.isEmpty())
                    {
                        update sObjectsToUpdate;
                    }
                }
            }
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.transferOwnership()',e);    
            throw e;            
        }
    }
    
    /* Get User/Contact owned records filters */
    public static string addFilters(string query, List<string> fieldAPINames, id filterId)
    {
        try
        {
            for(integer i = 0; i<fieldAPINames.size(); i++)
            {
                if(query.endsWithIgnoreCase(' WHERE ( '))
                {
                    query = query + fieldAPINames[i]; 
                }
                else
                {
                    query = query + ' OR ' + fieldAPINames[i]; 
                }
                query = query + + ' = \'' + filterId + '\' ';
            }
        }
        catch(exception e)
        {
            EFLErrorLog.createErrorLog('EFLContactUtility.transferOwnership()',e);  
            throw e;
        }
        return query;
    }
    
    /* Get Ownership Transfer Setup Mappping from Custom Metadata*/
    public static Map<string, EFLOwnershipTransferSetup__mdt> getOwnershipTransferSetupMap(string TransferType)
    {
        Map<string, EFLOwnershipTransferSetup__mdt> setupMap = new Map<string, EFLOwnershipTransferSetup__mdt>();
        try
        {
            for(EFLOwnershipTransferSetup__mdt setup: [Select label, DeveloperName, Object_API_Name__c,
                                                       ApplicationApplicantContactFieldPath__c, ApplicationFieldPath__c,
                                                       ContactLookupsToTransfer__c, Transfer_Type__c, 
                                                       UserLookupsToTransfer__c from EFLOwnershipTransferSetup__mdt 
                                                       where Transfer_Type__c =:TransferType])
            {
                setupMap.put(setup.Object_API_Name__c, setup);
            }
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.transferOwnership()',e);          
            throw e;
        }
        return setupMap;
    }
    
    //RR 4/18 - begin
    public static contact selectContactAccountRelationsByContactId(id contactId){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            return [SELECT ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, 
                    (select Id, AccountId, ContactId, isDirect,Personal__c from AccountContactRelations ORDER BY CreatedDate ASC),
                    (select id, username, contactId, AccountId, FirstName, LastName, usertype, profileid, isActive, 
                     profile.name from Users where isActive = true ORDER BY CreatedDate DESC limit 1)
                    FROM Contact WHERE Id = :contactId limit 1
                   ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.selectContactAccountRelationsByContactId()',e);                
        } 
        return null;
    }
    /**
* Returns the personal contact related to an account of type, applicant.
* Particularly useful because business contacts will have a direct relationship to a business account - 
* but indirect to their own personal account.
* @param - accountId - the id of the personal account 
* */
    public static Id getPersonalContactId(id accountId){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Account');
            List<AccountContactRelation> acr = [SELECT ID, AccountId, ContactId,isDirect,Personal__c 
                    from AccountContactRelation
                    where AccountId = :accountId and Personal__c = true 
                    limit 1
                   ];
            if (acr.isEmpty()){//unable to find personal__c flag, then get ACR with "direct" flag
                acr = [SELECT ID, AccountId, ContactId,isDirect,Personal__c 
                    from AccountContactRelation
                    where AccountId = :accountId and isDirect = true 
                    limit 1
                   ];
            }
            
            if (!acr.isEmpty() ) return acr[0].ContactId;            
        }
        catch(exception e){
            System.debug(LoggingLevel.INFO,'===>Error: '+e.getMessage());
            EFLErrorLog.createErrorLog('EFLContactUtility.selectContactAccountRelationsByAccountId()',e);                
        } 
        return null;
    }
    
    public static List<AccountContactRelation> contactAccountRelationsByContactAndParentAccount(Id contactId, Id parentAccountId){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');            
            return [select Id, AccountId, ContactId, isDirect,Personal__c from AccountContactRelation where ContactId = :contactId
                    and AccountId in (Select Id from Account where ParentId = :parentAccountId)];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.contactAccountRelationsByContactIdAndParentAccount()',e);                
        } 
        return null;
    }
    
    public static List<Contact> selectActiveUserContactsByCurrentUserDirectAccount(){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            return selectActiveUserContactsByCurrentAccount(EFLUserUtility.contactDetails.AccountId);
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.selectActiveUserContactsByCurrentUserDirectAccount()',e);                
        } 
        return null;
    }
    
    public static List<Contact> selectActiveUserContactsByCurrentAccount(Id accountId){
        try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            return [SELECT ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, 
                    Email, (select Id, AccountId, ContactId, isDirect from AccountContactRelations ORDER BY CreatedDate ASC), 
                    (select Id, isActive, contactId from users where isActive = true ORDER BY CreatedDate DESC limit 1) FROM Contact 
                    WHERE Id in (select contactId from AccountContactRelation 
                                 where AccountId = :accountId) 
                    AND Id in (select contactId from user where isActive = true)
                   ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactUtility.selectActiveUserContactsByCurrentAccount()',e);                
        } 
        return null;
    }
    //RR 4/18 - end
    
    /*
* Ravee Racharla - 4/19 W-035281
* Retrieve all the applications created by the user and shared with an account
* This is usefull while de-activating an user from a sub/child account. Ownership of records 
* and related records of only these applications will be trasnferred
* TODO: Should we move this to appropriate Utility class?
*/
    public static Set<Id> getUserApplications(User currentUser, Id subAccountId){
        List<Application__c> applications = [Select Id from Application__c where OwnerId = :currentUser.Id and 
                                             Sharing_Account__c = :subAccountId];
        Set<Id> appIdSet;
        if(applications == null)
            return null;
        else if (applications.size() > 0){
            appIdSet = new Set<id>();
            for (Application__c app : applications){
                appIdSet.add(app.Id);
            }
            return appIdSet;
        }
        return null;
    }
    
    
}