public with sharing class EFLACLineItemReviewController {
    
    /*Variable, Constants and Properties*/
    
    //Common Variable
    public Integer size{get;set;} 
    public List<SelectOption> paginationSizeOptions{get;set;} 
    public string searchString{get;set;}
    private static final string LINEITEM_ID = 'LineItemId';
    private static final string AC_LINEITEM_REVIEW = 'AC LineItem Review';
    private static final string ANIMAL_TRANSPORTATION = 'Animal Transportation';
    private static final string IMPORTER_EXPORTER_RECIPIENT = 'Importer/Exporter/Recipient';
    private static final string REGULATED_ARTICLE = 'Regulated Article';
    private static final string ATTACHMENT = 'Attachment';
    
    public Id lineItemID {
        get {
            lineItemID = (id)ApexPages.currentPage().getParameters().get(LINEITEM_ID);
            return lineItemID;
        }
        set;
    }
    
    public AC__c lineItemRecord {
        get {
            if(lineItemRecord==null){
                lineItemRecord = EFLLineItemRepository.selectbyID(LineItemId);
            }
            return lineItemRecord;
        }
        set;
    }
    
    //Animal Transportation
    public EFLLineItemController animalTransportation {get; set;}
    
    //Article supplier
    public List<Article_Supplier_Developer__c> ArticleSuppliers {get; set;}
    
    //Regulated Article
    public list<RegulatedArticle_Information__c> allRegulatedArticle {get;set;}
    public List<RegulatedArticle_Information__c> filteredRegulatedArticle{get;set;}
    public Integer noOfRegulatedArticleRecords {get;set;}
    
    //Continue
    public PageReference redirectToApplicationReview() {
        pagereference appReviewPage = page.portal_application_detail;
        appReviewPage.getParameters().put('id', lineItemRecord.Application_Number__c);
        appReviewPage.setRedirect(true);
        return appReviewPage;
    }    
    
    /*Constructors*/
    public EFLACLineItemReviewController()
    {
        size=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        getArticleSuppliers();
        getAnimalTransportationFieldData();
    }
    
    /*Instruction Logic - Start */
    
    public List<string> allInstructions {
        get
        {
            List<string> allInstructions = new List<string>();
            Map<string, string> lineitemhelpMap = new Map<string, string>();
            lineitemhelpMap = getInstructionMap();
            
            if(lineItemRecord.Application_Details_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
            {
                allInstructions.add(lineitemhelpMap.get(ANIMAL_TRANSPORTATION));
            }
            
            if(lineItemRecord.Related_Contact_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
            {
                allInstructions.add(lineitemhelpMap.get(IMPORTER_EXPORTER_RECIPIENT));
            }
            
            if(lineItemRecord.Regulated_Article_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
            {
                allInstructions.add(lineitemhelpMap.get(REGULATED_ARTICLE));
            }
            
            if(lineItemRecord.Document_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
            {
                allInstructions.add(lineitemhelpMap.get(ATTACHMENT));
            }
            
            return allInstructions;
        }
        set;
    }
    
    private Map<string, string> getInstructionMap() {
        Map<string, string> lineitemhelpMap = new Map<string, string>();
        List<EFLTextConfiguration__mdt> eflConfigTextList = new List<EFLTextConfiguration__mdt>();
        eflConfigTextList = EFLGenericUtility.getInstructionsByFunctionalArea(AC_LINEITEM_REVIEW);
        for (EFLTextConfiguration__mdt eflTextCon : eflConfigTextList) {
            lineitemhelpMap.put(eflTextCon.Section__c, eflTextCon.Text__c);
        }
        return lineitemhelpMap;
    }
    
    /*Instruction Logic - End */
    
    /*Animal Transportation Logic - Start */
    
    public void getAnimalTransportationFieldData() {
        
        ApexPages.StandardController sc = new ApexPages.StandardController(lineItemRecord);
        ApexPages.currentPage().getParameters().put('id', lineItemID);
        animalTransportation = new EFLLineItemController(sc);
    }
    
    /*Animal Transportation Logic - End */
    
    
    /* Importer/Exporter/Delivery logic - start */
    
    public void getArticleSuppliers(){
        ArticleSuppliers = new List<Article_Supplier_Developer__c>();
        ArticleSuppliers = EFLArticleSupplierRepository.selectASDByLineItemID(lineItemID);
    }
    
    /* Importer/Exporter/Delivery logic - End */
    
    /* Animal Information logic - start */
    
    public ApexPages.StandardSetController setRegulatedArticle {
        get {
            if(setRegulatedArticle == null) {
                List<RegulatedArticle_Information__c> raList = new List<RegulatedArticle_Information__c>();
                raList = EFLregulatedarticleinformationRepository.selectRAINFByLineItemID(lineItemID);
                setRegulatedArticle = new ApexPages.StandardSetController(raList);
                setRegulatedArticle.setPageSize(size);  
                noOfRegulatedArticleRecords = setRegulatedArticle.getResultSize();
            }
            return setRegulatedArticle;
        }
        set;
    }
    
    public ApexPages.StandardSetController setFilteredRegulatedArticle {
        get {
            if(searchString.length()>0) {
                string searchstr = '%' + searchString +'%';
                List<RegulatedArticle_Information__c> raList = new List<RegulatedArticle_Information__c>();
                raList = EFLregulatedarticleinformationRepository.selectRAINFByLineItemIDAndWildCardSearchFilter(lineItemID, searchstr);
                setFilteredRegulatedArticle = new ApexPages.StandardSetController(raList);
                setFilteredRegulatedArticle.setPageSize(size);  
                noOfRegulatedArticleRecords = setFilteredRegulatedArticle.getResultSize();
            }
            return setFilteredRegulatedArticle;
        }
        set;
    }
    
    public List<RegulatedArticle_Information__c> getAllRegulatedArticleRecords() {
        allRegulatedArticle = new List<RegulatedArticle_Information__c>();
        allRegulatedArticle = (List<RegulatedArticle_Information__c>) setRegulatedArticle.getRecords();
        return allRegulatedArticle;
    }
    
    public List<RegulatedArticle_Information__c> getFilteredRegulatedArticleRecords() {
        if (filteredRegulatedArticle== null ) {
            return getAllRegulatedArticleRecords();
        } else {
            return filteredRegulatedArticle;
        }
    }
    
    public void searchRegulatedArticles(){
        if (searchString.length() == 0) {
            filteredRegulatedArticle= null;
        } else {
            filteredRegulatedArticle= new List<RegulatedArticle_Information__c>();
            filteredRegulatedArticle= (List<RegulatedArticle_Information__c>) setFilteredRegulatedArticle.getRecords();
        }
    }
    
    //Changes the size of pagination
    public PageReference refreshRegulatedArticlePageSize() {
        setRegulatedArticle.setPageSize(size);
        return null;
    }
    
    /* Animal Information logic - end */
    
    
}