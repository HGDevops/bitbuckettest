@isTest
public class Authorization_Workflow_Team_Share_Test {
    static testmethod void method1(){
        Profile p = [SELECT Id FROM Profile WHERE Name='BRS Analyst'];
        UserRole r = [SELECT Id FROM UserRole WHERE Name = 'BRS Program Specialist'];
        User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
        insert u1;
        User u2 = new User(Alias = 'stand', Email='standarduser1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow1@testorg.com');
        insert u2;
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        system.runas(sysAdm)
        {
            team__c team = new team__c();
            team.RecordTypeId = Schema.SObjectType.Team__c.getRecordTypeInfosByDeveloperName().get('BRS_Team').getRecordTypeId();
            team.member_role__c='BRS Program Specialist';
            team.member__c = u1.id;
            insert team;
            team.RecordTypeId = Schema.SObjectType.Team__c.getRecordTypeInfosByDeveloperName().get('BRS_Team').getRecordTypeId();
            team.member_role__c='BRS Program Specialist';
            team.member__c = u2.id;
            update team;
        }
    }
}