@isTest
public class EFLIncidentUtility_test{
    
    static testMethod void test(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        //EFLInspectionActivityHandler inspectionActivityHandler = new EFLInspectionActivityHandler();
        list<sObject> activityList = new list<sObject>();
        Domain__c objDom = new Domain__c();
         objDom.Name = 'BRS';
         insert objDom;
         
         Program_Prefix__c  objPre = new Program_Prefix__c();
         objPre.Name= 'BRS Inspection';
         objPre.Inspection_Related__c = true;
         objPre.Program__c = objDom.id;
         insert objPre;
         
          Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
          Application__c objapp = testData.newapplication();
       Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        Authorizations__c objauth = testData.newAuth(objapp.id);
         
         
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Facility__c fac = new Facility__c();
        fac.RecordTypeId = PortsFacRecordTypeId;
        fac.Name = 'entryport';
        fac.Type__c = 'Laboratory';  
        insert fac;
        
          //Incident__c
         Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncidentRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncidentRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today(); 
        objInc.Related_Program__c = 'BRS';
       insert objInc;
       	
      /*  Profile p = [SELECT Id FROM Profile WHERE Name='eFile APHIS Staff'];
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        system.runas(sysAdm)
        {
            UserRole ur = new UserRole(DeveloperName = 'MyBRSPS', Name = 'BRS Program Specialist');
            insert ur;
            
            User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;

        }
        User u2 = [SELECT Id FROM User WHERE LastName='Testing'];
	       team__c team = new team__c();
          team.recordtypeId = Schema.SObjectType.Team__c.getRecordTypeInfosByName().get('BRS Team').getRecordTypeId();
          team.member_role__c='BRS Program Specialist';
          team.Member__c = u2.id;
            team.Authorization__c=objauth.id;
            insert team;
            
        task tsk2 = new task();
        tsk2.whatid=objInc.id;
        insert tsk2;
        tsk2.Status = 'Completed';
        tsk2.Activity_Sequence__c = 1;
        update tsk2;
                         
        
        EFLIncidentUtility.needsTaskCreation(objInc,objInc); */
    }

}