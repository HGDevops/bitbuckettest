global inherited sharing class EFLDeactivateCustUsers implements Schedulable{
    // Purpose: To deactivate the customer community users who have accepted the invitation from their Partner Account Admin and that partner contact has an active user. 
    //
    public set<id> UseridSet = new set<id>();
    public Map<ID,ID> contactIdMap = new Map<ID,ID>();
    
    global void execute(SchedulableContext sc)
    {
        callSchedule();
    }
    public void callSchedule(){            
        list<user> useridstodeactivate = new  list<user>();
        List<User> newUsersWithCon = new List<User>();
        Set<id> finalUsertodeactivate = new set<Id>();
        list<User_Invite__c> ui  = [SELECT id, Invitee_List__c FROM User_Invite__c];
        // parse json string for each invite so that the status can be checked and the userid can be obtained.
        //List<CARPOL_UserInviteController.InvitedContact> inviteeList = (List<CARPOL_UserInviteController.InvitedContact>) Json.deserialize(ui.Invitee_List__c, List<CARPOL_UserInviteController.InvitedContact>.class);
        List<List<CARPOL_UserInviteController.InvitedContact>> inviteeList = new List<List<CARPOL_UserInviteController.InvitedContact>> ();
        for (User_invite__c uilist: ui){
            List<CARPOL_UserInviteController.InvitedContact> inviteeInstance = (List<CARPOL_UserInviteController.InvitedContact>) Json.deserialize(uilist.Invitee_List__c, List<CARPOL_UserInviteController.InvitedContact>.class);
            inviteeList.add(inviteeInstance);
        }
        if (inviteeList != null){ 
            //get userid from the records that are having status of accepted
            for (List<CARPOL_UserInviteController.InvitedContact> ic: inviteeList){ // for outer list
                for(Integer i=0;i<ic.size();i++)   //for inner list
                {
                    if(ic[i].newconid!=null && ic[i].status== 'Accepted'){
                        contactIdMap.put(ic[i].olduserid,ic[i].newconid);
                    }                                  
                } 
            }  
            //get userlist with accepted invite and a active user record              
            newusersWithCon = [Select Id,Name,ContactID from User WHERE isActive = true and ContactID IN:contactIdMap.values()];     
        }
        
        for (user ou:newusersWithCon){
            for (List<CARPOL_UserInviteController.InvitedContact> ic: inviteeList){
                for(Integer i=0;i<ic.size();i++){
                    if(ou.ContactID == ic[i].newconid) //match new contact id from user record and Map from UI record
                        finalUsertodeactivate.add(ic[i].olduserid); //if matches shortlist that user to deactive
                }
            }
        }
        //Deactivate user record
        if (finalUsertodeactivate!=null){
            useridstodeactivate = [select id,isactive,username  from user where id in :finalUsertodeactivate and isactive = true];
        } 
        if (useridstodeactivate != null){
            for(user u: useridstodeactivate ){
                u.isactive = false;
                u.username = '!'+u.username;
            }
            update useridstodeactivate;
        }
    }
}