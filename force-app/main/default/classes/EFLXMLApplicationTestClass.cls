@isTest
public class EFLXMLApplicationTestClass {
    @TestSetup static void setup(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        
        id AccountRecordTypeId = testData.AccountRecordTypeId;
        List<RecordType> acRecordtypes = [Select Name, Id From RecordType where sObjectType='Account' AND name='Preparer'];
        List<RecordType> conRecordtypes = [Select Name, Id From RecordType where sObjectType='Contact' AND name='Preparer'];
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Account accObj = new Account();
        accObj.RecordTypeId = AccountRecordTypeId;
        accObj.Name = 'TestAccount';
        insert accObj;
        
        Contact conObj = new Contact();
        //conObj.RecordTypeId = conRecordtypes[0].Id;
        conObj.FirstName = 'test';
        conObj.LastName = 'testing';
        conObj.Phone = '984533';
        conObj.Email = 'test@gmail.com';
        conObj.AccountId = accObj.Id;
        insert conObj;
        
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        
        Applicant_Contact__c associatedContact = new Applicant_Contact__c();
        associatedContact = testData.newappcontact();
        
        associatedContact.Account__c = newAccount.id;
        update associatedContact;
        
        user usershare = new User();
        usershare.Username ='efileapplicant12321@test.com';
        usershare.LastName = 'efileapplicant12321';
        usershare.Email = 'efileapplicant12321@test.com';
        usershare.alias = 'efil2314';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        Application__c appObj = new Application__c();
        appObj.Applicant_Name__c = conObj.Id;
        insert appObj;
        

        Application_Creation_Request__c appCreationReqObj = new Application_Creation_Request__c();
        appCreationReqObj.Application_Type__c = 'Permit';
        appCreationReqObj.Status__c = 'New';
        appCreationReqObj.ApplicantName__c = appObj.Applicant_Name__c;
        appCreationReqObj.Account__c = conObj.AccountId;
        insert appCreationReqObj;
        
            
        // Insert ContentVersion record
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.VersionData = Blob.valueof('Some random String');
        cv.Title = 'File';
        cv.PathOnClient = 'File';
        insert cv;
        
        // Query ContentVersion record for get ContentDocumentId.
        ContentVersion cnt = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id limit 1];
        
        // Insert ContentDocumentLink record.
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cnt.ContentDocumentId;
        cdl.LinkedEntityId = appCreationReqObj.id;
        cdl.ShareType = 'I';
        cdl.Visibility = 'AllUsers';
        insert cdl;
        
    }
    
    @IsTest static void tstMethod(){
        User usershare = [select id,name from user where Username ='efileapplicant12321@test.com' limit 1];
        EFLXMLApplicationController EFLXMLController;
        system.runAs(usershare)
        {
         EFLXMLController = new EFLXMLApplicationController();
          }
        EFLXMLController.applicationtype ='Permit';
        EFLXMLController.fname = 'test';
        Application_Creation_Request__c acr = [SELECT Id, ApplicantName__c,Account__c FROM Application_Creation_Request__c];
      //  EFLXMLController.applicantName = acr;
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        System.debug('ApplicantName__c >>' + acr);
        EFLXMLController.uploadFile();
      
    }
    
    @IsTest static void tstMethod2(){
          User usershare = [select id,name from user where Username ='efileapplicant12321@test.com' limit 1];
         system.runAs(usershare)
        {
        EFLXMLApplicationController EFLXMLController = new EFLXMLApplicationController();
        EFLXMLController.applicationtype ='Notification';
        EFLXMLController.fname = 'test';
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        EFLXMLController.uploadFile();
        
        }
        
    }
    @IsTest static void tstMethod3(){
        
          User usershare = [select id,name from user where Username ='efileapplicant12321@test.com' limit 1];
         system.runAs(usershare)
        {
        EFLXMLApplicationController EFLXMLController = new EFLXMLApplicationController();
        EFLXMLController.applicationtype =NULL;
        EFLXMLController.fname = 'Test';
        EFLXMLController.xmlbody = NULL;
        EFLXMLController.uploadFile();
        }
    }
    @IsTest static void tstMethod4(){
          User usershare = [select id,name from user where Username ='efileapplicant12321@test.com' limit 1];
       
        Application_Creation_Request__c acr = [SELECT Id, Status__c, ApplicantName__c FROM Application_Creation_Request__c];
  //        system.runAs(usershare)
        {
        // Insert ContentVersion record
        ContentVersion cvTwo = new ContentVersion();
        cvTwo.ContentLocation = 'S';
        cvTwo.VersionData = Blob.valueof('Some random String - second time');
        cvTwo.Title = 'File';
        cvTwo.PathOnClient = 'File';
        insert cvTwo;
        
        // Query ContentVersion record for get ContentDocumentId.
        ContentVersion cnt = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cvTwo.Id limit 1];
        
        // Insert ContentDocumentLink record.
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cnt.ContentDocumentId;
        cdl.LinkedEntityId = acr.id;
        cdl.ShareType = 'I';
        cdl.Visibility = 'AllUsers';
        insert cdl;
             EFLXMLApplicationController EFLXMLController;
     //    system.runAs(usershare)
        {
         EFLXMLController = new EFLXMLApplicationController();
        }
        EFLXMLController.applicationtype =NULL;
        
        acr.Status__c='Error';
        update acr;
  //      EFLXMLController.applicantName = acr;
        EFLXMLController.fname = 'Test';
        EFLXMLController.xmlbody = NULL;
        EFLXMLController.uploadFile();
        }
    }
    @IsTest static void tstMethod5(){
  User usershare = [select id,name from user where Username ='efileapplicant12321@test.com' limit 1];
         
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newauth(objapp.id);

        objauth.Applicant__c	= objcont.Id;
        update objauth;
        EFLXMLApplicationController EFLXMLController;
        system.runAs(usershare)
        {
         EFLXMLController = new EFLXMLApplicationController();
        }
       // EFLXMLController.applicationtype ='Notification';
        EFLXMLController.aType = 'Self Reporting';
        Test.setCurrentPageReference(new PageReference('EFLXMLSelfReporting'));
        System.currentPageReference().getParameters().put('authId',objauth.Id);
        System.currentPageReference().getParameters().put('reportType','Planting_Report');
        EFLXMLController.fname = 'test';
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        EFLXMLController.uploadFile();
        system.debug('SOQL'+[select id,Type__c,Application_Type__c from Application_Creation_Request__c where  ApplicantName__c =:objcont.Id]);
        system.debug('SOQL1'+[select id,Type__c,Application_Type__c from Application_Creation_Request__c]);

        //system.assertEquals('Planting Report', [select Application_Type__c from Application_Creation_Request__c where (Account__c =: objacct.Id OR ApplicantName__c =:objcont.Id)  and Application_Type__c ='Planting Report'].Application_Type__c  );       
		//system.assertEquals(2, [select Application_Type__c from Application_Creation_Request__c].size());
        
        System.currentPageReference().getParameters().put('reportType','Volunteer_Monitoring_Report');
        EFLXMLController.fname = 'test';
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        EFLXMLController.uploadFile();
        //system.assertEquals('Volunteer Monitoring Report', [select Application_Type__c from Application_Creation_Request__c where ApplicantName__c =:objcont.Id  and Application_Type__c ='Volunteer Monitoring Report'].Application_Type__c  );
 		//system.assertEquals(2, [select id from Report_Summary__c ].size());
 		
        System.currentPageReference().getParameters().put('reportType','Field_Test_Report');
        EFLXMLController.fname = 'test';
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        EFLXMLController.uploadFile();
     //   system.assertEquals('Field Test Report', [select Application_Type__c from Application_Creation_Request__c where ApplicantName__c =:objcont.Id  and Application_Type__c ='Field Test Report'].Application_Type__c);
 		//system.assertEquals(3, [select Application_Type__c from Application_Creation_Request__c where ApplicantName__c =:objcont.Id].size());
        
        System.currentPageReference().getParameters().put('reportType','Test');
        EFLXMLController.applicationtype=null;
        EFLXMLController.fname = 'test';
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        EFLXMLController.uploadFile();
 		//system.assertEquals(3, [select Application_Type__c from Application_Creation_Request__c ].size());

        
        EFLXMLController.backtoSummary();
        
    }
     @IsTest static void tstMethodNew(){
        EFLXMLApplicationController EFLXMLController;
 //        EFLXMLApplicationController.EFLXMLApplicationDataWapper rapper= new EFLXMLApplicationController.EFLXMLApplicationDataWapper();

         EFLXMLController = new EFLXMLApplicationController();

        EFLXMLController.applicationtype ='Permit';
        EFLXMLController.fname = 'test';
        Application_Creation_Request__c acr = [SELECT Id, ApplicantName__c,Account__c FROM Application_Creation_Request__c];
        system.debug('acr'+ acr);
   //      EFLXMLController.applicantName = acr;
        EFLXMLController.xmlbody = Blob.valueof('Some random String');
        System.debug('ApplicantName__c >>' + acr);
        EFLXMLController.uploadFile();
   

     }
}