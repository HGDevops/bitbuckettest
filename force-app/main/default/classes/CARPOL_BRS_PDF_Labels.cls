public with sharing class CARPOL_BRS_PDF_Labels {
  public Authorizations__c objauth {get;set;}
     
  public void setauthid (String s) {
    authid = s;
    system.debug('----set authid---'+authid);
  }
    
  public String getauthid() {
  system.debug('----get authid---'+authid);
    return authid ;
  }
  
   public list<Label__c> getauthlabels(){
        system.debug('----authid---'+authid);
          if(authid!='' && authid!=null)
       objauth = [select id,Name,BRS_Hand_Carry_For_Importation_Only__c,Plant_Inspection_Station__r.Address_1__c,Plant_Inspection_Station__r.Address_2__c,Plant_Inspection_Station__r.City__c,Plant_Inspection_Station__r.State_Code__c,Plant_Inspection_Station__r.Zip__c,Plant_Inspection_Station__r.Country_Code__c from Authorizations__c where id=:authid limit 1];
       for(Label__c l:[select id,Name,Applicant_Name__c,City__c,Destination_County__c,Origin_County__c,Destination_Address__c,Destination_Address__r.Name,Label_Number__c,Notification_Expiration_Date__c,Notification_Issue_Date__c,
                       Authorization__c,Authorization__r.Name,Notification_Status__c,Organization__c,Origin_Address__c,Origin_Address__r.Name,Ports_of_Arrival__c,QRcode_Barcode__c,Destination_State__c,Origin_State__c,
                       Street_Address1__c,Street_Address2__c,Street_Address3__c,Used__c,Zipcode__c from Label__c where Authorization__c=:authid])
         {
             if(l.id!=null)            
                 labelslst.add(l);            
         }
       if(labelslst.size()>0){
        return labelslst;
       }
       return null;
   }
  
  public Component.Apex.pageblock getLabelspdflst() {
    Component.Apex.pageblock pgblock = new Component.Apex.pageblock();
    Component.Apex.PageBlockSection pgsect = new Component.Apex.PageBlockSection();
    Component.Apex.Iframe frame1 = new Component.Apex.Iframe();
    //pgsect.columns='1';
        for(Label__c l:labelslst){
            Component.Apex.PageBlockSection oppanel = new Component.Apex.PageBlockSection();
            oppanel.columns=1;
           // oppanel.style='background: repeating-linear-gradient(45deg,#606dbc,#606dbc 10px, #465298 10px,#465298 20px);';
            Component.Apex.Outputlabel lab1 = new Component.Apex.Outputlabel();
            lab1.value='This Package Contains  OMB NO. 0579-0085';
            lab1.style = 'font-size:12px;font-align:center;';
            oppanel.childComponents.add(lab1);
            Component.Apex.Outputlabel lab2 = new Component.Apex.Outputlabel();
            lab2.value='GENETICALLY ENGINEERED ORGANISMS';
            lab2.style = 'font-size:15px;font-weight:bold;';
            oppanel.childComponents.add(lab2);
            Component.Apex.Outputlabel lab3 = new Component.Apex.Outputlabel();
            lab3.value='DO NOT OPEN EXCEPT IN THE PRESENCE OF AN APHIS';
            lab3.style = 'font-size:12px;font-weight:bold;';
            oppanel.childComponents.add(lab3);
            Component.Apex.Outputlabel lab4 = new Component.Apex.Outputlabel();
            lab4.value='INSPECTOR OR DESIGNATED REPRESENTATIVE OF USDA.';
            lab4.style = 'font-size:12px;font-weight:bold;';
            oppanel.childComponents.add(lab4);
            Component.Apex.Outputlabel lab5 = new Component.Apex.Outputlabel();
            lab5.value='DELIVER TO';
            lab5.style = 'font-size:23px;font-weight:bold;align:right;';
            oppanel.childComponents.add(lab5);
            Component.Apex.Outputlabel lab6 = new Component.Apex.Outputlabel();
            lab6.value='U.S. DEPARTMENT OF AGRICULTURE';
            lab6.style = 'font-size:15px;font-weight:bold;';
            oppanel.childComponents.add(lab6);
            Component.Apex.Outputlabel lab7 = new Component.Apex.Outputlabel();
            lab7.value='ANIMAL AND PLANT HEALTH INSPECTION SERVICE';
            lab7.style = 'font-size:15px;font-weight:bold;';
            oppanel.childComponents.add(lab7);
             Component.Apex.Outputlabel lab8 = new Component.Apex.Outputlabel();
            lab8.value='PLANT PROTECTION AND QUARANTINE';
            lab8.style = 'font-size:15px;font-weight:bold;';
            oppanel.childComponents.add(lab8);
            Component.Apex.Outputlabel lab9 = new Component.Apex.Outputlabel();
            lab9.value='Label #'+l.Name;
            lab9.style = 'font-size:12px;font-weight:bold;';
            oppanel.childComponents.add(lab9);
             Component.Apex.Outputlabel lab10 = new Component.Apex.Outputlabel();
            lab10.value='Plant Inspection Station: '+l.Origin_Address__c;
            lab10.style = 'font-size:12px;font-weight:bold;';
            oppanel.childComponents.add(lab10);
            Component.Apex.Outputlabel lab11 = new Component.Apex.Outputlabel();
            lab11.value='Expires: '+l.Notification_Expiration_Date__c;
            lab11.style = 'font-size:12px;font-weight:bold;';
            oppanel.childComponents.add(lab11);
            Component.Apex.Outputlabel lab12 = new Component.Apex.Outputlabel();
            lab12.value='';
            lab12.style = 'font-size:12px;font-weight:bold;';
            oppanel.childComponents.add(lab12);
            pgsect.childComponents.add(oppanel);
        }       
        pgblock.childComponents.add(pgsect);
        return pgblock;
    }
    
    public string authid {get;set;}
    
    public list<Label__c> labelslst {get;set;}
    public CARPOL_BRS_PDF_Labels(){
      //authid = ApexPages.currentPage().getParameters().get('authid');
       objauth = new Authorizations__c();
       labelslst = new list<Label__c>();     
    }
}