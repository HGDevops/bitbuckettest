@IsTest(SeeAllData=true)
private class EFLCreatePackageSignTest {
    static testMethod void validatemyPackageAutoCreate() {
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Authorizations__c authHandler = new Authorizations__c();
        authHandler.AC_Applicant_Email__c = 'testapplicant@aphis.gov';
        authHandler.thumbprint__c = testData.newthumbprint().id;
        insert authHandler;
        
        // Create Attachment and add it to authHandler
        Attachment attach=new Attachment();     
        attach.Name='Permit for Dog';
        Blob bodyBlob=Blob.valueOf('Unit Test Body');
        attach.body=bodyBlob;
        attach.parentId=authHandler.id;
        insert attach;
        
        ESL__Convention__c conHandler = new ESL__Convention__c(name = 'Convention');
        insert conHandler;
        ESL__Signer_Label__c sigHandler = new ESL__Signer_Label__c(name = 'Signer Label_Test', ESL__Convention__c = conHandler.Id);
        insert sigHandler;

        String returnURL = EFLCreatePackageSign.PackageAutoCreate(authHandler.Id, 'AUTHNAME', '', 'Convention');
        System.debug(returnURL != null);
        returnURL = EFLCreatePackageSign.PackageAutoCreate(authHandler.Id, 'AUTHNAME', attach.id, 'Convention');
        System.debug(returnURL != null);
      
    }
}