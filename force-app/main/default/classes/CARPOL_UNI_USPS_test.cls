@isTest()
private class CARPOL_UNI_USPS_test {    
    private static testMethod void testAddressValidation() {            
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        Country__c country = new Country__c();
        country.Country_Code__c = 'US';
        country.Name = 'United States of America';
        insert country;
        
        Level_1_Region__c state = new Level_1_Region__c ();
        state.Level_1_Name__c  = 'State';
        state.Name = 'Alabama (AL)';
        state.Country__c = country.id;
        state.Level_1_Region_Code__c = 'AL';
        insert state;
        
        level_2_Region__c objL2R=new level_2_Region__c(Name='Baltimore',level_1_Region__c=state.Id,Level_2_Region_Status__c = 'Active');
        Insert objL2R;
        
        String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        Contact objcont = testData.newcontact();
        
        Test.startTest();

        Applicant_Contact__c appcont = new Applicant_Contact__c();
        appcont.First_Name__c = objcont.FirstName;
        appcont.Name = objcont.LastName;
        appcont.Account__c = objcont.AccountId;
        appcont.Email_Address__c = 'apcont@test.com';
        appcont.Mailing_Country__c = 'United States';
        appcont.Mailing_Country_LR__c = country.Id;
        appcont.Mailing_State_LR__c = state.Id;
        appcont.Mailing_State__c = 'Alabama';
        appcont.Mailing_County__c = objL2R.Id;
        appcont.Mailing_Zip_Postal_Code__c = '35005';
        appcont.EFL_Business_Name__c = 'Test';
        appcont.RecordTypeId = apcontRecordTypeId;
        Test.setMock(HttpCalloutMock.class, new AddressVerifyMockImpXML());
        insert appcont;
        
        CARPOL_UNI_USPS_integration_class.validate('123 Main st', 'TestCity', '12345', state.Id, appcont.Id);

        Test.stopTest();
        
        Applicant_Contact__c updatedapcont = [select ID,Address_Verified__c FROM Applicant_Contact__c Where id = :appcont.Id];
        System.assert(updatedapcont.Address_Verified__c == true);
    }
}