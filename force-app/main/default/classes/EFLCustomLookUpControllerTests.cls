@isTest
public class EFLCustomLookUpControllerTests {

    @TestSetup
    static void makeData(){
        
    }
    
    @isTest
    static void testInitController(){
        // test instance for AC AR is : EFL AR Animal Usage Table
        EFL_Custom_Lookup_Instance__mdt mdt = [SELECT Id, Usage_Key__c,sObject_to_Search__c, Service_Class_Extension__c,Order_By__c,Number_of_Results_to_Display__c
                                               FROM EFL_Custom_Lookup_Instance__mdt 
                                               LIMIT 1];
        User u = getARGuestUser();
        system.runAs(u){
            test.startTest();
            EFLCustomLookUpController.PageData pdTest = EFLCustomLookUpController.initController(mdt.Usage_Key__c);
            test.stopTest();
            system.assertEquals(mdt.sObject_to_Search__c, pdTest.sObjectName);
            system.assertEquals(mdt.Service_Class_Extension__c, pdTest.utilClassName);
            system.assertEquals(mdt.Order_By__c, pdTest.sortOrder);
            system.assertEquals(mdt.Number_of_Results_to_Display__c, pdTest.resultListSize);
        }
    }

    @isTest
    static void testFetchLookUpValues(){
        EFL_Custom_Lookup_Instance__mdt mdt = [SELECT Id, Usage_Key__c,sObject_to_Search__c, Service_Class_Extension__c,Order_By__c,Number_of_Results_to_Display__c
                                               FROM EFL_Custom_Lookup_Instance__mdt 
                                               LIMIT 1];
        sObject sObj = Schema.getGlobalDescribe().get(mdt.sObject_to_Search__c).newSObject();
        sObj.put('Name','Test');
        sObj.put('Species_Name__c', 'Test');
        sObj.put('Guidance_Text__c','Test');
        
        User admin = EFLACTestServices.getAdminUser();
        UserRole r = [SELECT Id FROM UserRole LIMIT 1];
        admin.UserRoleId = r.Id;
        system.runAs(admin){
        	insert sObj;
        }
        
         User u = getARGuestUser();
        system.runAs(u){
            test.startTest();
            EFLCustomLookUpController.PageData pdTest = EFLCustomLookUpController.initController(mdt.Usage_Key__c);
            //String searchKeyWord, String ObjectName, string pageData
            list<EFLCustomLookUpController.LookupItem> items = EFLCustomLookUpController.fetchLookUpValues('Test', mdt.sObject_to_Search__c, JSON.serialize(pdTest));
            system.assertEquals(1, items.size());
            test.stopTest();
        }
    }
    
    private static User getARGuestUser(){
        return EFLACTestServices.getCommunityUser('ACIS_Community');
    }
}