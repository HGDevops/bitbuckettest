public without sharing class CARPOL_Auth_ApexManagedSharing {
    
    public static Boolean addAuthSharing(Id publicGroupId, List<Authorizations__c> Authorizations){
       
        List<sObject> shares = new List<sObject>();
        
        try{
            
                for(Authorizations__c auth: Authorizations){
                    sObject dynObject= Schema.getGlobalDescribe().get('Authorizations__Share').newSObject();
                    dynObject.put(Schema.Authorizations__Share.ParentId, auth.id);
                    dynObject.put(Schema.Authorizations__Share.UserOrGroupId, publicGroupId);
                    dynObject.put(Schema.Authorizations__Share.AccessLevel, 'Edit');
                    dynObject.put(Schema.Authorizations__Share.RowCause, Schema.Authorizations__Share.RowCause.PartnerContact__c);
                    shares.add(dynObject);
                    
               
            }
            
            
        }
        catch(Exception e){
            System.debug('Applications >>> ' + e.getMessage());
        }
        
        if(shares.size() == 0)
            return false;
        else{
            insert shares;
            system.debug('Succesfully Inserted');
            return true;
        }
        
    }
    
    
}