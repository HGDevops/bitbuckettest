public with sharing class EFLRegistrationController {
    @AuraEnabled
    public static String getCurrentUser(){    
        User loggedinUser = [select id,FirstName,Name from user where Id=:Userinfo.getUserId() LIMIT 1];
        String userName = loggedinUser.Name;
        return userName;
    }
    
    @AuraEnabled public static PageData getPageData(){
        list<EFLRegistration__c> regs = getRegistrations();
        PageData pd = new PageData(regs);
        return pd;
    }

    @AuraEnabled
    public static list<EFLRegistration__c> getRegistrations(){
        Id conId = [select id,contactId from user where Id=:Userinfo.getUserId()].contactId;
        Id accountId = [select accountId from contact where Id=:conId].accountId;
        Integer currentYear = Integer.valueOf(System.Today().year());
        list<EFLRegistration__c> conRegList = [SELECT Id,EFLStatus__c,EFLDue_Date__c,EFLNumber_of_Days_Left__c, EFLOrganizationName__c,
                                               EFLRegistrationNumber__c,EFLRegistrationType__c,EFLAnnual_Report_Created__c, EFLCancellation_Date__c,
                                               (SELECT id,Name, status__c,EFL_Locked__c, EFLFiscal_Year__c from Annual_Reports__r 
                                                WHERE EFLFiscal_Year__c != null
                                                order by eflfiscal_year__c DESC, LastModifiedDate DESC)
                                               from EFLRegistration__c where EFLAccount__c=:accountId
                                              ORDER BY EFLStatus__c]; 
        return conRegList;
    }

    @AuraEnabled
    public static String saveNewAnnualReport(String registrationId){
        String anRepId ='';
        EFLRegistration__c reg = new EFLRegistration__c();
        reg = [Select Id,EFLAnnual_Report_Created__c, EFLAccount__c FROM EFLRegistration__c WHERE ID=:registrationId];
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = reg.EFLAccount__c;
        annualReport.Status__c='Draft';
        annualReport.EFLRegistration__c = reg.id;
        Database.insert(annualReport);
        anRepId = annualReport.id;
        return annualReport.id;
    }
    @auraEnabled
    public static eflAnnual_report__c getARStatus(string registrationId){
        return [select Id, status__c from eflAnnual_report__c where Id =:registrationId limit 1];
    }
    @AuraEnabled
    public static String getConType(){
        Id conId = [select id,contactId from user where Id=:Userinfo.getUserId()].contactId; 
        contact type = [select Id,firstName,lastName,account.name, EFL_Role__c from contact where Id=:conId];  
        return type.EFL_Role__c;
    }
    
    public class PageData{
        @AuraEnabled public list<Registration> registrations{get;set;}
        public PageData(list<EFLRegistration__c> regs){
            registrations = new list<Registration>();
            for(EFLRegistration__c r : regs){
                registrations.add(new Registration(r));
            }
        }
    }

    public class Registration{
        @AuraEnabled public EFLRegistration__c record{get;set;}
        @AuraEnabled public EFLAnnual_Report__c currentAR{get;set;}
        @AuraEnabled public List<EFLAnnual_Report__c> allReports{get;set;}
        @AuraEnabled public boolean showActionButton{get;set;}
        public Registration(EFLRegistration__c r){
            Integer currentFY = Date.today().month() >= 10 ? Date.today().year() + 1 : Date.today().year();
            Integer activeFY = currentFY - 1;
            boolean hasCurrent = false;
            
            this.record = r;
            this.allReports = new List<EFLAnnual_Report__c>();
            this.showActionButton = true;
            if(r.EFLStatus__c != 'Active' && r.EFLCancellation_Date__c != null && r.EFLCancellation_Date__c.year() <= activeFY -1){
                this.showActionButton = false;
            }
            
            if(!r.Annual_Reports__r.isEmpty() && r.Annual_Reports__r[0].EFLFiscal_Year__c == String.valueOf(activeFY)){
                this.currentAR = r.Annual_Reports__r[0];
                hasCurrent = true;
            }
            
            for(EFLAnnual_Report__c ar : r.Annual_Reports__r){
                if(hasCurrent == false || ar.Id != this.currentAR.Id){
                    this.allReports.add(ar);
                }
            }
        }
    }
}