public with sharing class InspectionStageProgressBarController {
    
    private ApexPages.StandardController controller;
    public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
    public Boolean processComplete{get;set;}
    public string opts {get; set;}
    public Id recId{get;set;}
    public List<String> options{get; set;}  
    public list<Program_Stages__mdt> programStageList;
    public list<InspectionStageStatusCriteria__mdt> InspStageStatgeList;
    string prvStage;
    public string currentStage{get;set;}
    public string status{get;set;}
    public Inspection__c obj;
    public Boolean navigation{get;set;}  
    public Boolean readyForNextStage{get;set;}
    public integer currentStageIndex{get;set;}
    public Boolean hasError{get;set;}
    public Boolean isActivityCompleted{get;set;}
    
    
    
    public InspectionStageProgressBarController(ApexPages.StandardController controller){
        isActivityCompleted = false;
        options = new List<String>();
        recId = ApexPages.currentPage().getParameters().get('id');
        List<Task> lstTask = [select id from task where status != 'Completed' and whatId =: recId];
        if(lstTask.size() > 0)isActivityCompleted = false;
        else isActivityCompleted = true;
        obj=[select id,Stage__c,status__c, Workflow_Number__c,Ready_for_next_stage__c from Inspection__c where id=:recId];
        currentStage=obj.Stage__c;
        status=obj.status__c;
        processComplete = false;
        this.controller = controller;
        shouldRedirect = false;
        readyForNextStage = false;
        readyForNextStage = obj.Ready_for_next_stage__c;
        hasError = false;
        
        
        programStageList = [select Workflow_Number__c, Stage__c,Sequence__c from Program_Stages__mdt where object_type__c='Inspection__c' order by Sequence__c];  
        for(Program_Stages__mdt ps : programStageList){ 
            options.add(ps.Stage__c); 
        }
        opts = JSON.serialize(options);
        
        if(programStageList.isEmpty()){navigation = false;}
        else{
            navigation = true  ;    
        }
        currentStageIndex = options.indexOf(currentStage);
        if(currentStageIndex == options.size()-1){navigation = false; 
                                                  if(readyForNextStage){processComplete = true;}}
    }
    
    public PageReference moveToFirstStage(){
        Inspection__c objIns  = new Inspection__c(id = recId,Stage__c =options.get(0));
        EFLActivityUtility validateUsers = new EFLActivityUtility();        
        List<Team__c> teamMembersList = new List<Team__c>();
        List<String> currentRequiredRoles = new List<String>();
        hasError = false;
        
        
        teamMembersList = EFLTeamRepository.selectByInspectionID(obj.ID);
        //system.debug('TeamMemberList from Progress Bar Controller @@@@@@ ' + teamMembersList);
        currentRequiredRoles = EFLProgramPrefixUtility.getRequiredRoles(obj.Workflow_Number__c);
        //system.debug('RequiredRoleList from Progress Bar Controller @@@@@@ ' + currentRequiredRoles);
        String errorMessage = validateUsers.validateRoles(obj, teamMembersList, currentRequiredRoles);
        //system.debug('hasError value: ' + hasError);
        
        if(String.isBlank(errorMessage)){
            //system.debug('There are no errors*****');
            updateStage(options.get(0));
            ////system.debug('options.get(0) '+ options.get(0));
            shouldRedirect = true;
            redirectUrl = controller.view().getUrl();
            return null;
        }else{
            //system.debug('ErrorMessage#### '+ errorMessage);
            apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,errorMessage));
            if(apexpages.hasMessages()){
                hasError = true;
                //system.debug('hasError value if error: ' + hasError);
                return null;
            }
        }
        
        //update objIns;
        return null;
    }
    
    
    public pagereference moveToNextStage()
    {
        try
        {
            ////system.debug('va'+options.get(currentStageIndex+1) );//
            InspStageStatgeList=[select id,Stage__c,Status__c,Error_Message__c from InspectionStageStatusCriteria__mdt];
            prvStage=options.get(currentStageIndex);
            Map<String,InspectionStageStatusCriteria__mdt> mapInspMeta = new Map<String,InspectionStageStatusCriteria__mdt>();
            for(InspectionStageStatusCriteria__mdt rec:InspStageStatgeList){
                mapInspMeta.put(rec.Stage__c,rec);
            }
            if(mapInspMeta.containsKey(prvStage) && mapInspMeta.get(prvStage).Status__c != obj.status__c){
                ////system.debug('error msz'+ rec.Error_Message__c);
                //obj.adderror(mapInspMeta.get(prvStage).Error_Message__c);
                apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,'Error: ' + mapInspMeta.get(prvStage).Error_Message__c));
                hasError = true;
                return null;
            }else{
                string curStage=options.get(currentStageIndex+1);
                string status='';
                if(curStage=='Inspection Report'){
                    status='Inspection Pending';  
                }
                if(curStage=='Compliance Evaluation'){
                    status='In Review';  
                }
                Inspection__c objIns  = new Inspection__c(id = recId,Stage__c =options.get(currentStageIndex+1),Status__c=status);
                update objIns;
                
            }
            return null; 
        }
        catch(exception e){
            String errorMsg = e.getMessage();
            hasError = true;
            //system.debug('errorMsg#### '+ errorMsg);
            if(errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                errorMsg = errorMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', ': ');
                //system.debug('errorMsg#### '+ errorMsg);
                // pagereference p = apexpages.Currentpage();
                apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,'Error: ' + errorMsg));
                if(apexpages.hasMessages())
                { //system.debug('TestHasMessages');
                 hasError = true;
                 ////system.debug('hasError:'+hasError);
                 return null;
                } 
            }
        }
        return null;
    }
    
    public void updateStage(string stageName){
        //system.debug('stageName '+stageName);
        string status='';
        if(stageName=='Inspection Assignment'){
            status='Open';
        }
        Inspection__c inspectionRecord  = new Inspection__c(id = recId,Stage__c =stageName,Status__c=status);
        update inspectionRecord;
    }
}