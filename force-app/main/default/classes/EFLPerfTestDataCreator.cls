public class EFLPerfTestDataCreator {
  public static void InsertTestData(Integer startNumber, Integer endNumber, String emailPrefix, string emailSuffix, string environment){
        //AC_AR1@accenturefederal.com
        List<Account> accountsToInsert = new List<Account>();
        for (Integer i = startNumber; i <= endNumber; i++){
            Account newAccount = new Account();
            newAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AC_R_L_Account').getRecordTypeId();
            newAccount.Name = 'Test AC_AR Account ' + i;
            accountsToInsert.add(newAccount);
        }
        insert accountsToInsert;
                
        //contact fields: account, first name, last name, role (Institutional Official), email address
        List<Contact> contactsToInsert = new List<Contact>();
        integer contactCount = startNumber;
        for (Account account : accountsToInsert){
            Contact newContact = new Contact();
            newContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
            newContact.FirstName = 'AC_AR' + contactCount;
            newContact.LastName = 'Test';
            newContact.AccountId = account.Id;
            newContact.EFL_Role__c = 'Institutional Official';
            newContact.Email = emailPrefix + contactCount + emailSuffix;
            contactsToInsert.add(newContact);
            contactCount++;
        }
        insert contactsToInsert;        
        
        //registration fields APHIS Registration Number (99-T-9950 start), Active = Y, Account
        List<EFLRegistration__c> registrationsToInsert = new List<EFLRegistration__c>();
    integer registrationCount = 1;        
        for (Account account : accountsToInsert){
            EFLRegistration__c newRegistration = new EFLRegistration__c();
            newRegistration.EFLAccount__c = account.Id;
            newRegistration.EFLActive__c = 'Y';
            newRegistration.EFLRegistrationNumber__c = '99-T-99';
            if (registrationCount < 10){
              newRegistration.EFLRegistrationNumber__c = newRegistration.EFLRegistrationNumber__c  + 0 + registrationCount;
            }else {
              newRegistration.EFLRegistrationNumber__c = newRegistration.EFLRegistrationNumber__c + registrationCount;                
            }
            registrationsToInsert.add(newRegistration);
        }
        insert registrationsToInsert;
        
        List<User> usersToInsert = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Report Preparer'];        
        for (Contact contact : contactsToInsert){
            User user = new  User();
      user.Alias = (contact.FirstName + contact.lastName).left(8).trim();
            user.Email = contact.Email;
            user.EmailEncodingKey='UTF-8';
            user.LastName = contact.LastName;
            user.FirstName = contact.FirstName;
            user.LanguageLocaleKey = 'en_US'; 
            user.LocaleSidKey='en_US'; 
            user.ProfileId = p.Id;
            user.TimeZoneSidKey='America/Los_Angeles';
            user.Username = contact.Email + environment;
            user.ContactId = contact.Id;
            usersToInsert.add(user);
        }
        insert usersToInsert;
    }
}