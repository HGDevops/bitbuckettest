public class CARPOL_BRS_NEPA_Decision_DocumentPDFCont{
    //Auth_Related_Records__c    
    public Auth_Related_Records__c authRelatedRecordDisplay{get;set;}
    public list<wrapFieldsWithApiAndLabels> fieldApisWrapList{get;set;}
    private boolean isMovementTypeRelease;
    private boolean isMovementTypeReleaseAndInterState;
    
    public CARPOL_BRS_NEPA_Decision_DocumentPDFCont(){
        isMovementTypeRelease = false;
        isMovementTypeReleaseAndInterState = false;
        if(apexpages.currentpage().getparameters().get('MovementType') != null){
            string tempStr = apexpages.currentpage().getparameters().get('MovementType');
            if(tempStr == 'Release'){
                isMovementTypeRelease = true;
            }
            else if(tempStr == 'Interstate Movement and Release'){
                isMovementTypeReleaseAndInterState = true;
            }
        }
        fieldApisWrapList = fillWrapperMethod();
        authRelatedRecordDisplay = new Auth_Related_Records__c();
        string pdfId = apexpages.currentpage().getparameters().get('id');
        if(pdfId != null && pdfId != ''){
            string AllFields = '';
            for(wrapFieldsWithApiAndLabels api : fieldApisWrapList){
                AllFields += ', ' + api.apiName;
            }
            string queryStr = 'select id, name, Authorization__c, Authorization__r.BRS_Introduction_Type__c, Authorization__r.name ' + AllFields + ' from Auth_Related_Records__c where Id =: pdfId';
            list<Auth_Related_Records__c> AuthList = database.query(queryStr);
            if(AuthList != null && AuthList.size() > 0){
                authRelatedRecordDisplay = AuthList[0];
            }
        }
    }
    
    public list<wrapFieldsWithApiAndLabels> fillWrapperMethod(){
        list<wrapFieldsWithApiAndLabels> tempList = new list<wrapFieldsWithApiAndLabels>();
        integer i = 0;
        
        tempList.add(new wrapFieldsWithApiAndLabels('X1_Does_this_Document_contain_CBI__c',' Does this Document contain CBI? If so, please indicate the information that is CBI using brackets [.......].','1',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X2_Is_the_recipient_a_plant__c',' Is the recipient a plant?','2',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X3_Is_duration_greater_than_one_year__c',' Is the duration of the release or movement requested greater than one year?','3',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X4_Is_plant_a_noxious_weed_species__c',' Is the plant on the list of federally-recognized noxious weed species? ','4',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X5_Derived_from_an_animal_or_human_virus__c',' Is the introduced genetic material derived from an animal virus or human virus?','5',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X5a_REQUIRED_COMMENTS_Q5__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('Is_introduced_genetic_material_known__c',' Is the function of all introduced genetic material known? ','6',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X7_Is_likely_to_cause_disease__c',' Does the expression of the genetic material result in the production of a substance known or likely to cause disease in humans or non-target animals, including threatened or endangered species?','7',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X7a_REQUIRED_COMMENTS_Q7__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X8_Does_Gen_mat_result_in_plant_disease__c',' Does the expression of the genetic material result in plant disease?','8',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X9_Can_result_in_any_infectious_entity__c',' Does the expression of the genetic material result in the production of any infectious entity? ','9',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X10_Can_be_toxic_to_non_target_organisms__c',' Does the expression of the genetic material result in the production of substances known or likely to be toxic to non-target organisms, including threatened or endangered species, known or likely to feed or live on the plant? ','10',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X10a_REQUIRED_COMMENTS_Q10__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X11_Intended_for_phar_or_industrial_use__c',' Does the expression of the genetic material result in the production of a substance intended for pharmaceutical or industrial use? ','11',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X12_Non_coding_regulatory_sequences__c',' If plant virus-derived sequences have been used, are they non-coding, regulatory sequences of known function? ','12',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X12a_REQUIRED_COMMENTS_Q12__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X13_Sequences_from_coding_regions__c',' If plant virus-derived sequences from coding regions have been used, are they sense or anti-sense constructs derived from plant viruses prevalent in and endemic to the area where the introduction will occur, infecting plants of the same species? ','13',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X13a_REQUIRED_COMMENTS_Q13__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X14_Cell_to_cell_movement_of_the_virus__c',' If plant virus-derived sequences from coding regions have been used do they encode a functional noncapsid gene product responsible for cell-to-cell movement of the virus? ','14',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X14a_REQUIRED_COMMENTS_Q14__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X15_Foreign_DNA_in_the_plant_genome__c',' Has the genetic material been introduced via a method (e.g., Agrobacterium transformation orbiolistics) that results in the stable incorporation of the foreign DNA in the plant genome? ','15',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X16_Article_qualify_s_for_Notification__c',' Does this regulated article qualify for Notification?','16',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X16a_REQUIRED_COMMENTS_Q16__c','','Comments',false));
        
        
        if(isMovementTypeRelease || isMovementTypeReleaseAndInterState){
            tempList.add(new wrapFieldsWithApiAndLabels('R1_Impact_of_proposed_release__c', + ' Does the incremental impact of the proposed release, when added to other past, present, and reasonably foreseeable future actions (regardless of what agency or person undertakes such actions), have a potential for significant environmental impact? ','17',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R1a_REQUIRED_COMMENTS_QR1__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R2_Involve_approved_vet_biologic__c', + ' Does the proposed release involve a licensed or approved veterinary biologic (vaccines, bacterins, antisera, diagnostic kits, and other products of biological origin) that has been subsequently shown to be unsafe? ','18',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R2a_REQUIRED_COMMENTS_QR2__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R3_Is_unlicensed_vet_biological_product__c', + ' Is the proposed release a previously unlicensed veterinary biological product to be shipped for field testing which contains live microorganisms, and will it be used for in vitro diagnostic testing? ','19',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R3a_REQUIRED_COMMENTS_QR3__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R4_Release_a_genetically_eng_organism__c',  + ' Is this a confined field release of (a) genetically engineered organism(s)? (based on an examination of design protocols? ','20',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R4a_REQUIRED_COMMENTS_QR4__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R5_Involve_new_species_organisms__c',  + ' Do the products involve new species or organisms? ','21',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R5a_REQUIRED_COMMENTS_QR5__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R6_Modifications_that_raise_new_issues__c',  + ' Are there new or novel modifications that raise new issues? ','22',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R6a_REQUIRED_COMMENTS_QR6__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R7_Significant_impact_to_Human_Environ__c', + ' Does the proposed release have the potential to affect "significantly" the quality of the "human environment" as those terms are defined at 40 CFR 1508.27 and 1508.14?<br/><br/>For example consider:<br/><br/>1. Familiarity with the crop<br/><br/>2. Familiarity with the phenotype<br/><br/>3. Familiarity with the gene product<br/><br/>4. Size of the proposed release<br/><br/>5. Proven effectiveness of confinement measures<br/><br/>6. Existence of sexually compatible wild relatives of the crop<br/><br/>7. Crop traits that would impact confinement, such as seed dispersal mechanisms, seed dormancy, and whether the crop plant is a perennial. ','23',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R7a_REQUIRED_COMMENTS_QR7__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R8_Within_Reservation_Lands__c', + ' Does the action area of the proposed release occur within reservation lands for a Federally Recognized Tribe? If so, was the Tribe contacted and consultation offered? If so, did consultation occur? ','24',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R9_Is_plant_sexually_compatible__c',  + ' Is the genetically engineered plant sexually compatible with any federally listed threatened or endangered species or species proposed for listing that could be found within the release site or the area requiring monitoring? ','25',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R10_Release_in_critical_habitat__c',  + ' Release in critical habitat?Is the release site and action area within designated critical habitat for a listed threatened or endangered species or within habitat proposed for designation? ','26',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R10a_REQUIRED_COMMENTS_QR10__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R11_Can_cause_disease_in_humans__c', + ' Does the expression of the genetic material result in the production of a substance known or likely to cause disease in humans or non-target animals, including threatened or endangered species? ','27',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R11a_REQUIRED_COMMENTS_QR11__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R12_Is_toxic_to_non_target_organisms__c',  + ' Does the expression of the genetic material result in the production of substances known or likely to be toxic to non-target organisms, including threatened or endangered species, known or likely to feed or live on the plant? ','28',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R12a_REQUIRED_COMMENTS_QR12__c','','Comments',false));
            tempList.add(new wrapFieldsWithApiAndLabels('R13_Effects_to_TES_or_critical_habitat__c',  + ' Are there any other aspects of the action that could result in effects to TES or critical habitat that have not been considered in this decision document? ','29',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R14_Has_NO_EFFECT_on_Critical_Habitat__c',  + ' Has APHIS reached a determination that this release would have no effect on listed species and designated critical habitat, and is unlikely to jeopardize the continued existence of a proposed species or adversely modify proposed critical habitat? ','30',true));
            tempList.add(new wrapFieldsWithApiAndLabels('R14a_REQUIRED_COMMENTS_QR14__c','','Comments',false));
        }
        
        
        if(!isMovementTypeRelease){
            tempList.add(new wrapFieldsWithApiAndLabels('M1_Potential_for_enviornmental_Impact__c', + ' Does the incremental impact of the proposed movement, when added to other past, present, and reasonably foreseeable future actions (regardless of what agency or person undertakes such actions), have the potential for significant environmental impact? ','31',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M2_Unsafe_licensed_approved_biologic__c', + ' Does the proposed movement involve a licensed or approved biologic that has been subsequently shown to be unsafe, or will it be used at substantially higher dosage levels or for substantially different applications or circumstances than in the use for which the product was previously approved? ','32',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M3_Contains_live_microorganisms__c', + ' Is the proposed movement for a previously unlicensed veterinary biological product to be shipped for field testing which contains live microorganisms, and will it be used for in vitro diagnostic testing? ','33',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M4_New_species_or_organisms_new_issues__c', + ' Does the proposed movement involve a confined field release of genetically engineered organisms or products that involve new species or organisms or novel modifications that raise new issues? ','34',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M5_Effect_quality_of_human_environment__c', + ' Does the proposed movement have the potential to affect significantly the quality of the human environment as those terms are defined at 40 CFR 1508.27 and 1508.14? ','35',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M6_Between_contained_facilities__c', + ' Has APHIS determined that the movement proposed is between contained facilities? (Under notification, regulated articles must be shipped in a way that viable material is unlikely to be disseminated while in transit) ','36',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M7_Measures_used_to_avoid_impact__c', + ' All movements of regulated articles are authorized only when measures are used to avoid or minimize impacts to the human environment. Has APHIS determined that these measures are in place? ','37',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M8_ESA_Assessment_Movement__c', + ' Has APHIS-BRS determined that the importation or interstate movement of regulated articles this notification, following the performance standards under §340.3(c), would have no effect on listed species or species proposed on designated critical habitat? ','38',true));
            tempList.add(new wrapFieldsWithApiAndLabels('M8a_REQUIRED_COMMENTS_QM8__c','','Comments',false));
        }
        
        
        tempList.add(new wrapFieldsWithApiAndLabels('X17_Categorical_exclusions_under_NEPA__c',  + ' Is this notification eligible for categorical exclusion under NEPA? ','39',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X17a_REQUIRED_COMMENTS_Q17__c','','Comments',false));
        tempList.add(new wrapFieldsWithApiAndLabels('X18_Do_exceptions_to_exclusions_apply__c',  + ' Do any of the exceptions to categorical exclusion apply? ','40',true));
        tempList.add(new wrapFieldsWithApiAndLabels('X18a_REQUIRED_COMMENTS_Q18__c','','Comments',false));
        
        return tempList;
    }
    
    public class wrapFieldsWithApiAndLabels{
        
        public string apiName{get;set;}
        public string Label{get;set;}
        public string numbering{get;set;}
        public boolean nextRowHide{get;set;}
        
        public wrapFieldsWithApiAndLabels(string apiName, string Label, string numbering, boolean nextRowHide){
            this.apiName = apiName;
            this.Label = label;
            this.numbering = numbering;
            this.nextRowHide = nextRowHide;
        }
    }
}