public class EFLUtils {
    
    public static string getSObjectField(SObject sObj, string fieldpath){
		Object value = getSObjectFieldObject(sObj, fieldPath);

        string fieldValue = null;
        if (value != null) {
			fieldValue = string.valueOf(value);
        }
     
        return fieldValue;
    }
    
    public static object getSObjectFieldObject(SObject sObj, string fieldpath){
        try{
            if(sObj == null) return null;
            
            string[] pathParts = fieldpath.split('[.]',2);
            if(pathParts.size() == 1){
                if(sObj.get(fieldPath) != null){
                    return sObj.get(fieldPath);
                }else{
                    return null;
                }
            }
            try{
                if(sObj.getSObject(pathParts[0]) == null){return null;}
            }catch(exception e){
                return null;
            }
            return getSObjectField(sObj.getSObject(pathParts[0]),pathParts[1]);
        }catch(exception e){
            system.debug('exception: ' + e.getMessage());
            return null;
        }
    }
    
    public static list<LightningPicklistOption> getPicklistOptions(string sObjectName, string fieldName){
        list<LightningPicklistOption> values = new list<LightningPicklistOption>();
            
        List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(new List<String>{sObjectName});
        
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(new LightningPicklistOption(entry.getValue(), entry.getLabel()));
                }
            }
        }
        
        return values;
    }
    
    public class LightningPicklistOption{
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string value{get;set;}
        public LightningPicklistOption(string lab, string v){
            this.label = lab;
            this.value = v;
        }
    }
    
    public static boolean getCommunityContext(){
        return Site.getSiteId() != null;
    }
}