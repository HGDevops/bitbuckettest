/**
* An apex page controller that exposes the site forgot password functionality
*/
@IsTest
public with sharing class LightningForgotPasswordControllerTest {
    
    //@IsTest
    public static void testLightningForgotPasswordWithValidUser() {
        User usr = EFLUserTestDataFactory.getUser('APHIS Applicant');
        try {
            LightningForgotPasswordController controller = new LightningForgotPasswordController();
            LightningForgotPasswordController.forgotPassowrd(usr.Username, '/apex/CARPOL_HomePage');
        } catch (Exception e) {
            
        }
    }
    
    @IsTest
    public static void testLightningForgotPasswordWithInvalidUser() {
        User usr = EFLUserTestDataFactory.getUser('APHIS Applicant');
        try {
            LightningForgotPasswordController controller = new LightningForgotPasswordController();
            LightningForgotPasswordController.forgotPassowrd('test2323.com', '/apex/CARPOL_HomePage');
        } catch (Exception e) {
            
        }
    }    
    
    @IsTest
    public static void testLightningForgotPasswordWithNullValues() {
        User usr = EFLUserTestDataFactory.getUser('APHIS Applicant');
        try {
            LightningForgotPasswordController controller = new LightningForgotPasswordController();
            LightningForgotPasswordController.forgotPassowrd(null, null);
        } catch (Exception e) {
            
        }
    }    
}