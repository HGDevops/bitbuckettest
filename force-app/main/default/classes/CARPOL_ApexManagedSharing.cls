public without sharing class CARPOL_ApexManagedSharing {
    
    // This method is meant to share existing records for a newly created partner user, 
    // where the partner user has accepted to join an organization and had a customer user before.
    // The purpose of this code is to transfer the existing application of this customer user
    // to the newly created partner user via apex Managed sharing. 
    // OBJECT LIST: Currently doing this for APPLICATIONS, LINEITEMS and AUTHORIZATIONS. If you
    // add more objects, please add to this list up here for readability. 
    public static void addSharingForUser(Id partnerUserId){
        User u = [select id, name, ContactId, Applications_Shared__c from User where id =: partnerUserId];
        
        if(u.Applications_Shared__c == false){ //only runs the code once for each Partner user
            
            List<sObject> shares = new List<sObject>();
            List<Application__c> applications;
            List<AC__c> lineItems;
            List<Authorizations__c> auths;
            
            try{
                applications = [select Id from Application__c where Applicant_Name__c =: u.ContactId];
                for(Application__c app: applications){
                    sObject dynObject= Schema.getGlobalDescribe().get('Application__Share').newSObject();
                    dynObject.put(Schema.Application__Share.ParentId, app.id);
                    dynObject.put(Schema.Application__Share.UserOrGroupId, partnerUserId);
                    dynObject.put(Schema.Application__Share.AccessLevel, 'Edit');
                    dynObject.put(Schema.Application__Share.RowCause, Schema.Application__Share.RowCause.PartnerContact__c);
                    shares.add(dynObject);
                }
            
                try{    
                    lineItems = [select Id from AC__c where Application_Number__r.Applicant_Name__c =: u.ContactId];
                    for(AC__c app: lineItems){
                        sObject dynObject= Schema.getGlobalDescribe().get('Ac__share').newSObject();
                        dynObject.put(Schema.Ac__share.ParentId, app.id);
                        dynObject.put(Schema.Ac__share.UserOrGroupId, partnerUserId);
                        dynObject.put(Schema.Ac__share.AccessLevel, 'Edit');
                        dynObject.put(Schema.Ac__share.RowCause, Schema.Ac__share.RowCause.PartnerContact__c);
                        shares.add(dynObject);
                    }
                }
                catch(Exception e){
                    System.debug('LineItems >>> ' + e.getMessage());
                }
                
                try{    
                    auths = [select Id from Authorizations__c where Application__r.Applicant_Name__c =: u.ContactId];
                    for(Authorizations__c app: auths){
                        sObject dynObject= Schema.getGlobalDescribe().get('Authorizations__share').newSObject();
                        dynObject.put(Schema.Authorizations__share.ParentId, app.id);
                        dynObject.put(Schema.Authorizations__share.UserOrGroupId, partnerUserId);
                        dynObject.put(Schema.Authorizations__share.AccessLevel, 'Edit');
                        dynObject.put(Schema.Ac__share.RowCause, Schema.Authorizations__share.RowCause.PartnerContact__c);
                        shares.add(dynObject);
                    }
                }
                catch(Exception e){
                    System.debug('Authoriztions >>> ' + e.getMessage());
                }
            }
            catch(Exception e){
                System.debug('Applications >>> ' + e.getMessage());
            }
                
            if(shares.size() != 0){
                insert shares;
                u.Applications_Shared__c = true;
                update u;
            }
        }
    }
    
    //This is an internal method that is used to remove access from the public group
    //of the associated partner user. We only remove share records that have a rowCause as 'Manual'. 
    //Please do not remove any other rowCause as you need a share record for the Partner User 
    //to be able to see their own applicaitons. This record, however, will have a rowCause = 'PartnerContact__c'.
    private static void makeApplicationPrivate(Id appId){
        
        List<sObject> shareList = new List<sObject>();
        List<Ac__c> lineItemList;
        List<Authorizations__c> authList;
        
        try{
            shareList.addAll(Database.query('select id from Application__Share where RowCause = \'Manual\' and parentId =: appId'));
                    
            try{
                lineItemList = [select Id from AC__c where Application_Number__c =: appId]; 
                if(lineItemList != null){
                    List<Id> temp = new List<id>();
                    for(AC__c li: lineItemList)
                        temp.add(li.Id);
                        
                    shareList.add(Database.query('select id from AC__share where RowCause = \'Manual\' and parentId in: temp'));
                }
            }
            catch(Exception e){
                System.debug('line item >>> ' + e.getMessage());
            }

            try{
                authList = [select Id from Authorizations__c where Application__c =: appId];
                if(authList != null){
                    List<Id> temp = new List<id>();
                    for(Authorizations__c au: authList)
                        temp.add(au.Id);
                        
                    shareList.add(Database.query('select id from Authorizations__share where RowCause = \'Manual\' and parentId in: temp'));
                }
            }
            catch(Exception e){
                System.debug('authorization >>> ' + e.getMessage());
            }
        }
        catch(Exception e){
            System.debug('application >>> ' + e.getMessage());
        }
        
        if(shareList != null && shareList.size() > 0)
            delete shareList;
    }
    
    //This method creates the share records to give access to the public group (Org users)associated 
    //to the applicant (Partner User). A share record with a rowCause = 'Manual' needs to be inserted. 
    private static void makeApplicationPublic(Id appId, Id publicGroupId){
        
        List<sObject> shares = new List<sObject>();
        List<Application__c> applications;
        List<AC__c> lineItems;
        List<Authorizations__c> auths;
        //apps
        sObject appShare = Schema.getGlobalDescribe().get('Application__Share').newSObject();
        appShare.put(Schema.Application__Share.ParentId, appId);
        appShare.put(Schema.Application__Share.UserOrGroupId, publicGroupId);
        appShare.put(Schema.Application__Share.AccessLevel, 'Edit');
        appShare.put(Schema.Application__Share.RowCause, Schema.Application__Share.RowCause.Manual);
        shares.add(appShare);
        //lineItems
        try{    
            lineItems = [select Id from AC__c where Application_Number__c =: appId];
            for(AC__c app: lineItems){
                sObject dynObject= Schema.getGlobalDescribe().get('Ac__share').newSObject();
                dynObject.put(Schema.Ac__share.ParentId, app.id);
                dynObject.put(Schema.Ac__share.UserOrGroupId, publicGroupId);
                dynObject.put(Schema.Ac__share.AccessLevel, 'Edit');
                dynObject.put(Schema.Ac__share.RowCause, Schema.Ac__share.RowCause.Manual);
                shares.add(dynObject);
            }
        }
        catch(Exception e){
            System.debug('LineItems >>> ' + e.getMessage());
        }
        //auths
        try{    
            auths = [select Id from Authorizations__c where Application__c =: appId];
            for(Authorizations__c app: auths){
                sObject dynObject= Schema.getGlobalDescribe().get('Authorizations__share').newSObject();
                dynObject.put(Schema.Authorizations__share.ParentId, app.id);
                dynObject.put(Schema.Authorizations__share.UserOrGroupId, publicGroupId);
                dynObject.put(Schema.Authorizations__share.AccessLevel, 'Edit');
                dynObject.put(Schema.Ac__share.RowCause, Schema.Ac__share.RowCause.Manual);
                shares.add(dynObject);
            }
        }
        catch(Exception e){
            System.debug('Authoriztions >>> ' + e.getMessage());
        }
        
        if(shares.size() != 0)
            insert shares;
        
    }
    
    //Method that has external visibility and is used to toggle the privacy on an application
    //for a Partner user. 
    public static String toggleApplicationPrivacy(Id appId, Id publicGroupId){
        
        Application__c app = getApplication(appId);
        String status;
        if(app != null){
            if(app.Private_Application__c == true){
                app.Private_Application__c = false;
                makeApplicationPublic(appId, publicGroupId);
                status = 'Public';
            }
            else{
                app.Private_Application__c = true;
                makeApplicationPrivate(appId);
                status = 'Private';
            }
                
            update app;
        }
        
        return status;
        
    }
    
    //Utility method
    private static Application__c getApplication(Id appId){
        
        Application__c app;
        try{
            app = [select id, name, Applicant_Name__c, Private_Application__c from Application__c where id=: appId];
            return app;
        }
        catch(Exception e){
            System.debug('There was an error ' + e.getMessage());
        }
        
        return null;
    }
    

}