@isTest(seealldata=true)
private class CARPOL_UNI_GenericHistoryController_Test {
       @IsTest static void CARPOL_UNI_GenericHistoryController_Test() {
       	
       	Test.startTest(); 
       	
       	  
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
       
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id);        
        AC__c ac = testData.newLineItem('Personal Use',objapp,objauth);    
        CARPOL_UNI_DisableTrigger__c dt  = new CARPOL_UNI_DisableTrigger__c();
        dt.Name = 'Test';
        dt.disable__c = false;
        insert dt;   
        
        Change_History__c change = new Change_History__c();
        change.Name  = 'Status';  
	    change.Field_API_Name__c = 'Status__c';
	    change.New_Value__c = 'Submitted';
	    change.Old_Value__c = 'Ready';
	    change.Line_Item__c = ac.Id;
	    insert change;
           
       CARPOL_UNI_GenericHistoryController genericHistory = new CARPOL_UNI_GenericHistoryController();
	    genericHistory.myObject = ac;
	    genericHistory.lookupField = 'Line_Item__c';
	    
	    genericHistory.getObjectHistory();
	    CARPOL_UNI_GenericHistoryController.returnFieldLabel('Status__c');
	    //negative test
	    CARPOL_UNI_GenericHistoryController.returnFieldLabel('Test');
	    Test.stopTest(); 
	     
       }
}