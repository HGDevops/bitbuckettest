public with sharing class CARPOL_VS_ConfirmAPHISAccountPayment {
    
    public ID applicationID;
    public string totalAmount;
    public string acctNum;
    
    public CARPOL_VS_ConfirmAPHISAccountPayment(ApexPages.StandardController controller) {
        
        applicationID = controller.getId();
        totalAmount = EFLGenericUtility.sanitizeString(ApexPages.CurrentPage().getparameters().get('total'));
        //system.debug('total ' + totalAmount);
        acctNum = EFLGenericUtility.sanitizeString(ApexPages.CurrentPage().getparameters().get('acctNum'));
    }
    
    public PageReference processTrans()
    {
        //system.debug('total ' + totalAmount);
        //system.debug('appid ' + applicationID);
        //system.debug('acctNum ' + acctNum);
        List<Transaction__c> trans = [Select Id From Transaction__c where Application__c =: applicationID LIMIT 1];
        
        if (trans.isEmpty()){
            
            
            //Create Transaction instance for the process and APHIS Account related info
            Transaction__c tran = new Transaction__c();
            tran.Application__c = applicationID;
            tran.Transaction_Amount__c = Decimal.valueOf(totalAmount);
            tran.Transaction_Date_Time__c = System.NOW();
            tran.Payment_Type__c = 'APHIS Account';
            tran.APHIS_Account_Number__c = acctNum;
            
            insert tran; //insert this data into the transaction
            
            PageReference appProcessing = new PageReference('/apex/UpdateApplication?id=' + applicationID);
            appProcessing.setRedirect(true);
            return appProcessing;
            
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'The application cannot be submitted again as it has already been paid previously.'));
            return null;
            
        }
    }
    
    public PageReference cancelPayment() {
        PageReference ref = new PageReference('/apex/CARPOL_VS_ApplicationFee_Payment?id=' + applicationID);
        ref.setRedirect(true);
        return ref;
    }
    
}