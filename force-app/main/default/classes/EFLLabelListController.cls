public without sharing class EFLLabelListController {
    public list<string> labelNumbers{
        get{
            string LABEL_STATUS_VOIDED = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
            labelNumbers = new list<string>();
            if(authId != null){
                string aId = authId;
                for(Label__c l : [SELECT Name FROM Label__c WHERE Authorization__c = :aId AND Status__c = :LABEL_STATUS_VOIDED ANd LastModifiedDate = TODAY]){
                    labelNumbers.add(l.Name);
                }
            }
            return labelNumbers;
        }
        set{
            /*labelNumbers = new list<decimal>();
if(authId != null){
string aId = authId;
for(Label__c l : [SELECT Label_Number__c FROM Label__c WHERE Authorization__c = :aId]){
labelNumbers.add(l.Label_Number__c);
}
}*/
        }
    }
    public string authId{
        get;
        set{
            authId = value;
        }
    }
    public EFLLabelListController(){
    }
}