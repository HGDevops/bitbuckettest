@istest
public class EFLTaskTriggerHandlerTest {
    public static Authorizations__c auth;
    public static Task task;
    public static Task task1;
    public static Task task2;
    public static  Incident__c objInc;
    public static  Inspection__c Ins;
    static testmethod void runtest(){
        EFLTaskTriggerHandler tskhandler = new EFLTaskTriggerHandler();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='555';
        Insert objPrefix;
        Signature__c objTP=new  Signature__c();
        objTP.Name='Test AC TP Jialin';
        objTP.Recordtypeid=Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c=objPrefix.Id;
        Insert objTP;
        Application__c app = testData.newapplication();
        auth = testData.newAuth(app.Id);
        
        
        
         /*auth.RecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId(); 
        auth.BRS_Introduction_Type__c='Interstate Movement';
        auth.thumbprint__c = objTP.id;
        auth.effective_date__c=system.today();
        auth.Expiry_Date__c = system.today().adddays(250);
        auth.BRS_Proposed_Start_Date__c=system.today();
        auth.BRS_Proposed_End_Date__c = system.today().adddays(250);
        auth.Application__c=app.id;
        auth.status__c = 'Submitted';
        auth.stage__c='Permit Package';
        auth.workflow_Number__c='150';
        auth.Authorization_Type__c='Permit';
        auth.Expiration_Timeframe_Override__c='1 Month';
        insert auth;*/
        Profile p = [SELECT Id FROM Profile WHERE Name='BRS Analyst'];
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        system.runas(sysAdm)
        {
            UserRole ur = new UserRole(DeveloperName = 'MyBRSPS', Name = 'BRS Program Specialist');
            insert ur;
            UserRole ur1 = new UserRole(DeveloperName = 'MyBRSBio', Name = 'BRS Biotechnologist');
            insert ur1;
            User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;
            User u2 = new User(Alias = 'stand', Email='standarduser1@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur1.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow1@testorg.com');
            insert u2;
        }
        User u3 = [SELECT Id FROM User WHERE LastName='Testing'];
        User u4 = [SELECT Id FROM User WHERE LastName='Testing1'];
        String BRSTeamRecordTypeId = Schema.SObjectType.team__c.getRecordTypeInfosByDeveloperName().get('BRS_Team').getRecordTypeId();
        team__c team = new team__c();
        team.member_role__c='BRS Program Specialist';
        team.RecordTypeId = BRSTeamRecordTypeId;
        team.Member__c = u3.id;
        team.Authorization__c=auth.id;
        insert team;
        team__c team1 = new team__c();
        team1.member_role__c='BRS Biotechnologist';
        team1.RecordTypeId = BRSTeamRecordTypeId;
        team1.member__c = u4.id;
        insert team1;
        
        task tsk = new task();
        tsk.whatid=auth.id;
        //tsk.Activity_Sequence__c = 1;
        insert tsk;
      
     /*   tsk.Status = 'Completed';
        tsk.Activity_Sequence__c = 1;
        map<id,Authorizations__c>  objAuthTaskMap = new map<id,Authorizations__c>();
        objAuthTaskMap.put(tsk.Id,auth);
        tskhandler.objAuthTaskMap=objAuthTaskMap;
        update tsk;*/
        try{
     tskhandler.beforeUpdate(tsk,tsk);
        delete tsk;
        }
        catch(exception e){
            
        }
      
         EFL_Inspection_Questions_Template__c inspTmp = new EFL_Inspection_Questions_Template__c();
         inspTmp.Name='test';
         insert inspTmp;

         Inspection__c Ins = new Inspection__c();
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Ins.RecordTypeId = IncRecTypeID;
        Ins.Stage__c = 'Inspection Assignment';
        Ins.status__c='open';
        Ins.Program__c='BRS';
        Ins.Stage__c = 'Inspection Report';
        Ins.Activity_Sequence__c = 0;
        //Ins.certified__c = True;
        Ins.Reason_for_Cancellation__c = 'testing';
        Ins.EFL_Inspection_Questionnaire_Template__c=inspTmp.id;
        insert Ins;
        
        task tsk1 = new task();
        tsk1.whatid=Ins.id;
        insert tsk1;
     /*    tsk1.Status = 'Completed';
        tsk1.Activity_Sequence__c = 1;
       map<id,Inspection__c>  objInspTaskMap = new map<id,Inspection__c>();
        objInspTaskMap.put(tsk1.Id,Ins);
        tskhandler.objInspTaskMap=objInspTaskMap;

        Update tsk1;*/
        try{
        tskhandler.beforeUpdate(tsk1,tsk1);
        delete tsk1;
        }
        catch(exception e){
            
        } 
        
     /*   Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncidentRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncidentRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today(); 
        objInc.Related_Program__c = 'BRS';
        objInc.Activity_Sequence__c = 0;
        objInc.Ready_For_next_Stage__c  = true;
        insert objInc; */
        
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncidentRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncidentRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today(); 
        objInc.Activity_Sequence__c = 0;
        insert objInc;
        
        integer taskSequence = objInc.Activity_Sequence__c==null?1:objInc.Activity_Sequence__c.intvalue();
        Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(objInc.stage__c, objInc.Workflow_Number__c,taskSequence + 1);
       EFLActivityUtility.isLastActivityForStage(currentActivityCMD);  
        
        task tsk2 = new task();
        tsk2.whatid=objInc.id;
        tsk2.Activity_Sequence__c = 1;
        insert tsk2;
        
     /*   map<id,Incident__c>  objIncdTaskMap = new map<id,Incident__c>();
        objIncdTaskMap.put(tsk2.Id,objInc);
        tskhandler.objIncdTaskMap=objIncdTaskMap;
        tsk2.Status = 'Completed';
        update tsk2;*/
        try{
        tskhandler.beforeUpdate(tsk2,tsk2);
        }
        catch(exception e){
            
        } 
        tskhandler.beforeInsert(tsk); 
        tskhandler.afterDelete(tsk);
        tskhandler.andFinally();
        
    }
}