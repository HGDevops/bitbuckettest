public with sharing class CARPOL_inspection_questionnaire_option {
    public Map<String,List<SelectOption>> mapOptions{get;set;}
    public Map<String,String> mapResponses{get;set;}
    public List<EFL_Inspection_Questionnaire_Questions__c> listEIQ{get;set;}
    public EFL_Inspection_Questionnaire_Questions__c Questionnaire {get;set;}
    public boolean renderButton {get;set;}
    @testvisible List<EFL_Inspection_Questionnaire_Questions__c> RelatedQuestList{get;set;}
    public List<EFL_Inspection_Questionnaire_Questions__c> RLQToBeUpd;//=new List<EFL_Inspection_Questionnaire_Questions__c>();//list with valid updates only
    @testvisible List<EFL_Inspection_Questionnaire_Questions__c> toBeRemoved=new List<EFL_Inspection_Questionnaire_Questions__c>();//error records to be removed from  RelatedQuestList   
    List<EFL_Inspection_Questionnaire_Questions__c> questions =new List<EFL_Inspection_Questionnaire_Questions__c>();
    // String country = null;
    Public Integer noOfRecords{get; set;}
    List<EFL_Inspection_Questionnaire_Questions__c> questionnairequestions = new List<EFL_Inspection_Questionnaire_Questions__c>();
    public Id questionnaireid{get;set;}
    public Integer Size;
    public Integer selectedSize{Set;get;}
    public EFL_Inspection_Questionnaire__c objEIQ;
    public boolean bIsDisabled{get;set;}
    public boolean hasError{get;set;}
    public Boolean updated = true;
    public boolean IsCommunityUser {get; set;}
    User  currentUsr = new User ();
    public string InspectURL{get;set;}
    public string domain; 
    Inspection__c Ins = new Inspection__c();
    public CARPOL_inspection_questionnaire_option(ApexPages.StandardController controller) {
        hasError = false;
        IsCommunityUser = false;
        //Questionnaire =  (EFL_Inspection_Questionnaire_Questions__c)Controller.getRecord();
        //  Public id netid = ConnectApi.Communities.getCommunities();   
        
        string domain = ApexPages.currentPage().getHeaders().get('Host');
        currentUsr = [SELECT id, name, Usertype FROM User where Id =: userinfo.getuserid()];
        if( currentUsr.Usertype != 'PowerCustomerSuccess'){
            IsCommunityUser = false;
        }
        if( currentUsr.Usertype == 'PowerCustomerSuccess'){
            IsCommunityUser = true;
        }
        questionnaireid = controller.getId();
        domain = ApexPages.currentPage().getHeaders().get('Host');
        InspectURL = 'https://'+ domain + '/Collaborator/s/detail/'+questionnaireid; 
        //system.debug('cance url' +InspectURL );
        Ins = [Select ID, Certified__c from Inspection__c where ID =:questionnaireid];
        init();
    }
    
    public void init()
    {  
        
        Size = 5;
        questionnaireid = ApexPages.currentPage().getParameters().get('id');
        /*objEIQ=[select id,Status__c,Responses_Submitted__c from EFL_Inspection_Questionnaire__c where id=:questionnaireid ];
renderButton = true;
if(objEIQ.status__c=='Responses Submitted'){
renderButton = false;
}
bIsDisabled=objEIQ.Responses_Submitted__c;*/
        listEIQ=new List<EFL_Inspection_Questionnaire_Questions__c>();
        mapOptions=new Map<String,List<SelectOption>>(); 
        mapResponses=new Map<String,String>();
        Set<Id> setQuesIds=new Set<Id>();
        //system.debug('List of Questions size is >>'+[SELECT ID,NAME,Question__c,Answer__c,Response__c,Comments2__c,Options__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE Inspection__c=:questionnaireid].size());
        for(EFL_Inspection_Questionnaire_Questions__c recEIQ:[SELECT ID,NAME,Question__c,Answer__c,Response__c,Comments2__c,Options__c,order__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE Inspection__c=:questionnaireid order by order__c asc])
        {
            if(recEIQ.Options__c!=null)
            {
                listEIQ.add(recEIQ);
                mapResponses.put(recEIQ.Id,'');
                setQuesIds.add(recEIQ.Id);
                if(recEIQ.Options__c!=null){
                    for(String sOpt:recEIQ.Options__c.split(';'))
                    {
                        List<SelectOption> optionsTemp =mapOptions.get(recEIQ.Id);
                        if(optionsTemp==null)
                        {
                            optionsTemp =new List<SelectOption>();
                            mapOptions.put(recEIQ.Id,optionsTemp);
                        }
                        optionsTemp.add(new SelectOption(sOpt,sOpt));
                    }
                }
            }
        }
        RelatedQuestList=getRelatedQuestList();
    }
    public pagereference validateEntries(){
        RLQToBeUpd = new List<EFL_Inspection_Questionnaire_Questions__c>();
        for(EFL_Inspection_Questionnaire_Questions__c ref : RelatedQuestList){
            EFL_Inspection_Questionnaire_Questions__c newRef = new EFL_Inspection_Questionnaire_Questions__c(Id = ref.Id,Question__c = ref.Question__c, Response__c = ref.Response__c, Answer__c = ref.Answer__c, Comments2__c = ref.Comments2__c, Order__c = ref.Order__c, Answer_Type__c = ref.Answer_Type__c);
            RLQToBeUpd.add(newRef);
        }
                hasError = false;
                Pattern p = Pattern.compile('\\[.*?\\]');
        if(RelatedQuestList!=null && RelatedQuestList.size()>0){
              //for(EFL_Inspection_Questionnaire_Questions__c qst : RLQToBeUpd ){
              for (integer i=0;i< RLQToBeUpd.size();i++ ){
                String comments = RLQToBeUpd[i].Comments2__c;
                string AnsType = RLQToBeUpd[i].Answer_Type__c;
                string response = RLQToBeUpd[i].response__c;
                Matcher m = p.matcher(comments);
                ////system.debug('tes22'+m.find());
                if(ins.Certified__c == true){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Responses not saved. Once Inspection is Certified, can not be edited'));
                    hasError = true;
                    break;                    
                }
                if(m.find()) {
                    //system.debug('tes22'+'1222');
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Remove brackets from Inspection Questionnaire.'));
                    hasError = true;
                    RLQToBeUpd.remove(i);
                    //toBeRemoved.add(RLQToBeUpd[i]); 
                    break;                    
                }
                
                if(AnsType == 'Free Text' && comments == ''){
                    //system.debug('text vaues' +AnsType +comments );                    
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'All Free Text questions must include a comment'));
                    hasError = true;
                    RLQToBeUpd.remove(i);
                    break;                    
                }
                if(AnsType == 'Yes/No' && response == null){
                    apexpages.addmessage(new apexpages.message(apexpages.severity.ERROR,'Please select an answer for Yes/No questions'));
                    hasError = true;
                    RLQToBeUpd.remove(i);                 
                    break;
                }

            } 
        }
         /*if (hasError){
                   PageReference tempPage = ApexPages.currentPage();            
                   tempPage.setRedirect(true);
                   return tempPage;
               }else{ */
        //RelatedQuestList = RLQToBeUpd;
        //RelatedQuestList.add();
        return null;
        //}
       
    }
    public pagereference saveQuestion()
    {
        if(RelatedQuestList!=null && RelatedQuestList.size()>0){
            validateEntries();
            update RLQToBeUpd;            
            if(!hasError){
                //system.debug('usertype= '+currentUsr.Usertype); 
                apexpages.addmessage(new apexpages.message(apexpages.severity.CONFIRM,'Responses saved.')); 
                update RLQToBeUpd;
                //User  currentUsr = [SELECT id, name, Usertype FROM User where Id =: userinfo.getuserid()];
                if( currentUsr.Usertype == 'PowerCustomerSuccess' || test.isRunningTest()){
                    //system.debug ('Logged in user: customer user');  
                    string domain = ApexPages.currentPage().getHeaders().get('Host'); 
                    //system.debug('system domain url: ' +domain);
                    string base = 'https://'+ domain +'/Collaborator/s/sfdcpage/%2Fapex%2FCARPOL_inspection_questionnaire_option%3F%26id%3D'; 
                    
                    
                    PageReference pg =  new PageReference(base+questionnaireid);
                    pg.setRedirect(true);                    
                    //system.debug('return url:' +pg);
                    return pg;                    
                } 
                if( currentUsr.Usertype != 'PowerCustomerSuccess'){
                    //system.debug ('Logged in user: Normal user');
                    
                    return null;
                }                
                //  return null;          
                
            }
        }
        return null;

        
    }
    public pagereference save()
    {
        validateEntries();
        if(RelatedQuestList!=null && RelatedQuestList.size()>0){
            
            update RLQToBeUpd;
            //init();
            // RelatedQuestList=getRelatedQuestList();    
            /*Questionnaire.Status__c = 'Responses Submitted';
update Questionnaire;*/
        }
        
        /*  PageReference pg = Page.thank_you;
pg.setRedirect(true);
return pg; */
        
        PageReference pg = Page.thank_you;
        pg.setRedirect(true);
        return pg;
        //return null;
    }
    
    //public pagereference save1()
    //{
    // if(questions!=null && questions.size()>0){
    //   update questions;
    //   init();
    //  questions=getqs();    
    /*Questionnaire.Status__c = 'Responses Submitted';
update Questionnaire;*/
    //  }
    
    /*PageReference pg = Page.thank_you;
pg.setRedirect(true);
return pg;*/
    //  return null;
    //}
    
    public pagereference submit()
    {
        try{
            
            save();
            Integer iCnt=0;
            Boolean allow = true;
            for(EFL_Inspection_Questionnaire_Questions__c recEIQ:[SELECT ID,NAME,Question__c,Answer__c,Response__c,Comments2__c, order__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE EFL_Inspection_Questionnaire__c=:questionnaireid])
            { 
                if(recEIQ.Response__c!=null)
                {
                    iCnt++;
                }
            } 
            if(iCnt!=listEIQ.size())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'All the questions must be completed before the submission for approval')); 
                bIsDisabled=false;
                //objEIQ.Responses_Submitted__c=false;
                //update objEIQ;
                allow = false;
            }
            else{
                bIsDisabled=true;
                //objEIQ.Responses_Submitted__c=true;
                //update objEIQ;
                allow = true;
                
            }
            
            if(allow){
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId(questionnaireid);
                Approval.ProcessResult result = Approval.process(req1);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Questionnaire Successfully Submitted'));
                bIsDisabled=true;
                // objEIQ.Responses_Submitted__c=true;
                //update objEIQ;
            }
            return null;  
            
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This record is currently in an approval process. A record can be in only one approval process at a time'+ex)); 
            //objEIQ.Responses_Submitted__c=false; 
            bIsDisabled=false;
            return null; 
            
        }
    }
    
    //To set new Size for the pagination 
    public void setPaginationSize(){
        Size = 5 ;
    }
    
    
    public ApexPages.StandardSetController setcon {
        
        get {
            if(setcon == null) {
                string queryString = 'SELECT ID,NAME,Response__c,Question__c,Answer__c,Comments2__c,Options__c,Answer_Type__c, order__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE Inspection__C=:questionnaireid Order by Order__c ASC';
                //1-28-2018: Modified Query to Add Answer_Type__c.Added by Rajesh Potla
                setcon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                // sets the number of records in each page set
                setcon.setPageSize(5);
                noOfRecords = setCon.getResultSize();
            }
            return setcon;
        }
        set;
    }
    
    
    //testing mass
    //public List<EFL_Inspection_Questionnaire_Questions__c> getqs() {
    // questionnairequestions= [SELECT ID,NAME,Question__c,Response__c,Comments2__c,Answer__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE EFL_Inspection_Questionnaire__c=:questionnaireid];
    
    // for (EFL_Inspection_Questionnaire_Questions__c objRQ : questionnairequestions())
    // {
    //     EFL_Inspection_Questionnaire_Questions__c recRQ=new EFL_Inspection_Questionnaire_Questions__c(ID=objRQ.Id);
    //     recRQ.Question__c=objRQ.Question__c;
    //     recRQ.Answer__c=objRQ.Answer__c;
    //     recRQ.Comments__c =objRQ.Comments__c ;
    //     questions.add(recRQ);
    // }
    // questions.addall(questionnairequestions);
    // return questionnairequestions;
    
    // }
    
    
    
    //To Display Queried records on Visualforce page 
    public List<EFL_Inspection_Questionnaire_Questions__c> getRelatedQuestList(){
        //System.debug('Check getRelatedQuestListxxxxx: ' + RelatedQuestList);
        if(!hasError) {
        RelatedQuestList= new List<EFL_Inspection_Questionnaire_Questions__c>();
        //system.debug('<>< Setcon get records >>>> '+(List<EFL_Inspection_Questionnaire_Questions__c>)setcon.getRecords());
        for (EFL_Inspection_Questionnaire_Questions__c objRQ : (List<EFL_Inspection_Questionnaire_Questions__c>)setcon.getRecords())
        {
            EFL_Inspection_Questionnaire_Questions__c recRQ=new EFL_Inspection_Questionnaire_Questions__c(ID=objRQ.Id);
            recRQ.Question__c=objRQ.Question__c;
            //Rajesh - W-034124 
            if(objRQ.Answer_Type__c=='Free Text'){
                recRQ.Response__c = 'N/A';
            }else{
                recRQ.Response__c = objRQ.Response__c;
            }
            recRQ.Answer__c=objRQ.Answer__c;
            //   recRQ.Comments__c =objRQ.Comments__c ;
            recRQ.Comments2__c =objRQ.Comments2__c ;
             recRQ.Order__c =objRQ.Order__c ;
            recRQ.Answer_Type__c=objRQ.Answer_Type__c; //1-28-2018: Added by Rajesh Potla
            RelatedQuestList.add(recRQ);
        }
        //system.debug('<> Related Quest list>>>'+RelatedQuestList);
        }
        return RelatedQuestList;
    }
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return setcon.getHasNext();
        }
        set;
    }
    
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return setcon.getHasPrevious();
        }
        set;
    }
    
    
    // returns the first page of records
    public void first() {
       // saveQuestion();
       validateEntries();
        System.debug('Check hasError: ' + hasError);
        System.debug('Check RelatedQuestList: ' + RelatedQuestList);
        upsert RLQToBeUpd;        
        if(hasError == false){
            setcon.first();            
        }
    }
    
    // returns the last page of records
    public void last() {
        //updated=false;
        validateEntries();
        //saveQuestion();
        upsert RLQToBeUpd;       
        if(hasError == false){
            setcon.last();
        }
        
        //PageReference pg =  new PageReference('/apex/CARPOL_inspection_questionnaire_option?id='+questionnaireid);
        //pg.setRedirect(true);
        //return pg;
        
    }
    
    // returns the previous page of records
    public void previous() {
        validateEntries();
        //saveQuestion();
        upsert RLQToBeUpd;
        if(hasError == false){
            setcon.previous();
        }
    }
    
    // returns the next page of records
    public void next() {
        validateEntries();
        //saveQuestion(); // VV added for W-035908
        upsert RLQToBeUpd; 
        if(hasError == false){
            setcon.next();
        }
    }
    
    
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        setcon.cancel();
    }
}