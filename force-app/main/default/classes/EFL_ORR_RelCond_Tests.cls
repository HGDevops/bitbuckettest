@isTest(SeeAllData=true)
public class EFL_ORR_RelCond_Tests {
	private static Reviewer__c reviewer;
    static void makeData(){
        CARPOL_BRS_TestDataManager dm = new CARPOL_BRS_TestDataManager();
        Contact objcont = dm.newcontact();
        Application__c app = dm.newapplication();
        Authorizations__c auth = dm.newAuth(app.id);
        
        reviewer = new Reviewer__c();
        reviewer.Authorization__c = auth.Id;
        reviewer.State_Regulatory_Official__c = objcont.Id;
        insert reviewer;
    }
    
    @isTest
    static void pleaseGiveMeCodeCoverage(){
        makeData();
        ApexPages.StandardController controller = new ApexPages.StandardController(reviewer);
        EFL_ORR_RelCond c = new EFL_ORR_RelCond(controller);
        c.gotoRelatedCondPage();
        c.backToRR();
        c.authId = null;
        c.gotoRelatedCondPage();
    }
}