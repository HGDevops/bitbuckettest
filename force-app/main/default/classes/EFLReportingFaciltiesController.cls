public class EFLReportingFaciltiesController {
@AuraEnabled
    public static list<EFLRegistered_Site__c> getAccountSites(string registrationId){
       list<EFLRegistered_Site__c> siteList = [select EFLAccount_Site__r.Name,EFLAccount_Site__r.EFLAddress_1__c, EFLAccount_Site__r.EFLCity__c, EFLAccount_Site__r.EFLState_Code__c, EFLAccount_Site__r.EFLZip__c, EFLAccount_Site__r.EFLTelephone__c from EFLRegistered_Site__c where EFLRegistration__c=:registrationId];
       
        return siteList;
    }
}