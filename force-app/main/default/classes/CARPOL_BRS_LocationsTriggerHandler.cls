public inherited sharing class CARPOL_BRS_LocationsTriggerHandler {
    //Date 060619 Consolidate the code and move to EFLLocationTriggerHandler Todo 
    public static void processLocationReadiness(List<location__c> locationList){
        //system.debug('related contact1++');
        list<ID> BRSlineitemlst = new list<ID>();
        list<AC__c> listlineitemToUpdate = new list<AC__c>();
        list<AC__c> lineitemlistafter = new list<AC__c>();
        map<id,list<material__c>> locMaterialMap = new map<id,list<material__c>>();
        map<id,list<GPS_Coordinate__c>> gpsCoordinatesMap = new map<id,list<GPS_Coordinate__c>>();
        string incidentrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Incident Location').getRecordTypeId();
        string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();  
        boolean incidentrectype;
        boolean olocrectype;
        boolean rslocrectype;
        boolean dlocrectype;
        boolean oanddlocrectype;
        boolean materialExists,gpsExists;
        Integer oanddCount = 0;
        
        List<Id> objLineItemIds = new List<Id>();
        for(Location__c obj : locationList){
            objLineItemIds.add(obj.Line_Item__c);
        } 
        
        
        for(Location__c l:[select id,Line_Item__c from Location__c where id IN:locationList])                          
        {
            BRSlineitemlst.add(l.Line_Item__c); 
        }
        
        
        set<id> locationIdSet = new set<id>();
        if(!BRSlineitemlst.isEmpty()){
            lineitemlistafter = [SELECT ID,RecordType.Name,movement_type__c,Type_of_Permit__c,Location_Status__c ,status__c,Construct_Status__c,SOP_Status__c,Regulated_Article_Status__c,Related_Contact_Status__c,(select id,recordtypeid from Locations__r) From AC__c WHERE ID in: BRSlineitemlst];
        }else{
            lineitemlistafter = [SELECT ID,RecordType.Name,movement_type__c,Type_of_Permit__c,Location_Status__c ,status__c,Construct_Status__c,SOP_Status__c,Regulated_Article_Status__c,Related_Contact_Status__c,(select id,recordtypeid from Locations__r) From AC__c WHERE ID in: objLineItemIds];
        }
        
        for(AC__c a : lineitemlistafter){
            if(!a.Locations__r.isEmpty()){
                for(Location__c lc:a.Locations__r){
                    locationIdSet.add(lc.id);
                }                
            }
        }
        
        for(Location__c l:[select id,Line_Item__c,(select id,name from Materials__r),(select id,name from GPS_Coordinates__r) from Location__c where id IN:locationIdSet])                          
        {
            locMaterialMap.put(l.id,l.Materials__r);
            gpsCoordinatesMap.put(l.id,l.GPS_Coordinates__r);
        }
        
        for (AC__c a : lineitemlistafter) {
            olocrectype = false;               //origin location
            oanddlocrectype = false;           // origin and destination
            dlocrectype = false;               //destination
            rslocrectype = false;              // release
            materialExists = true;
            gpsExists = true;
            if(a.Locations__r!=null && a.Locations__r.size() > 0){
                for(Location__c lc:a.Locations__r){
                    if(lc.recordtypeid == olocrectypeid)
                        olocrectype = true;
                    if(lc.recordtypeid == oanddlocrectypeId){
                        oanddlocrectype = true;
                        oanddcount++;
                    }
                    if(lc.recordtypeid == dlocrectypeId)
                        dlocrectype = true;
                    if(lc.recordtypeid == locrectypeid)
                        rslocrectype = true;
                    if(lc.recordtypeid==incidentrectypeid)
                        incidentrectype = true;
                    
                    
                    if((lc.recordtypeid == dlocrectypeId || lc.recordtypeid == oanddlocrectypeId ) && materialExists){
                        if((locMaterialMap.get(lc.id)!=null && locMaterialMap.get(lc.id).size()>0) ){
                            materialExists = true;  
                        }else{
                            materialExists = false;
                        }
                    }
                    if(lc.recordtypeid == locrectypeid && gpsExists){
                        if((gpsCoordinatesMap.get(lc.id)!=null && gpsCoordinatesMap.get(lc.id).size()>=4) ){ //KA:W-038112
                            gpsExists = true;
                        }else{
                            gpsExists = false;
                        }
                    }
                }
                if(a.Movement_Type__c=='Import' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')&& (dlocrectype == true && olocrectype==true) && materialExists){
                    a.Location_Status__c = 'Ready to Submit';
                    listlineitemToUpdate.add(a);  
                }
                else if(a.Movement_Type__c=='Interstate Movement' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')
                        &&(
                            (dlocrectype == true && olocrectype==true) || 
                            (dlocrectype == true && oanddlocrectype==true) ||
                            (olocrectype==true && oanddlocrectype==true) ||  
                            (oanddlocrectype==true && oanddcount >=2)
                        )
                        && materialExists){
                            a.Location_Status__c = 'Ready to Submit';
                            listlineitemToUpdate.add(a);  
                        }
                else if(a.Movement_Type__c=='Interstate Movement and Release' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')
                        &&(
                            (dlocrectype == true && olocrectype==true) || 
                            (dlocrectype == true && oanddlocrectype==true) ||
                            (olocrectype==true && oanddlocrectype==true) ||  
                            (oanddlocrectype==true && oanddcount >=2)
                        )
                        && rslocrectype && materialExists && gpsExists){
                            a.Location_Status__c = 'Ready to Submit';
                            listlineitemToUpdate.add(a);  
                        } else if(a.Movement_Type__c=='Release' && (a.Type_of_Permit__c=='Standard Permit'|| a.Type_of_Permit__c=='Notification')
                                  && (rslocrectype && gpsExists)){
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a);  
                                  }else {
                                      a.Location_Status__c = 'Yet to Add';
                                      listlineitemToUpdate.add(a);
                                  }
            }else{
                a.Location_Status__c = 'Yet to Add';
                listlineitemToUpdate.add(a);
            }
        }
        
        if(listlineitemToUpdate!=null && listlineitemToUpdate.size()>0){
            checkForStatus(listlineitemToUpdate);
            update listlineitemToUpdate;
        }                             
    }
    
    public static void checkForStatus(List<AC__c>  lineItemlist){
        
        for(AC__c lineItem: lineItemlist){
            if(lineItem.status__c != 'Waiting on Customer' && lineItem.status__c != 'Submitted')
            { 
              //  system.debug('related contact++' +lineItem.Related_Contact_Status__c);
                if( lineItem.Construct_Status__c == 'Ready to Submit' && lineItem.SOP_Status__c== 'Ready to Submit' && 
                   lineItem.Regulated_Article_Status__c == 'Ready to Submit' && lineItem.Location_Status__c== 'Ready to Submit' && (lineItem.Related_Contact_Status__c == 'Ready to Submit' || lineitem.Related_Contact_Status__c == NULL))                     
                {
                    if (!(lineItem.Status__c == 'Voided' || lineItem.Status__c == 'No jurisdiction' || lineItem.Status__c == 'Permit not required'  || lineItem.Status__c == 'Draft' || lineItem.status__c == 'Waiting on Customer' || lineItem.status__c == 'Cancelled'|| lineItem.status__c == 'Disapproved' || lineItem.status__c == 'Withdrawn')){ //12/18/17 VV Updated to add Withdrawn per US-17053    
                        
                        lineItem.Status__c = 'Ready to Submit';
                    }
                }
                else 
                {
                    if(lineItem.Status__c!='Draft') 
                        lineItem.Status__c = 'Saved';  
                    
                } 
            }
        }
    }
}