@isTest (SeeAllData=true)
//Auth trigger calls a class (CARPOL_UNI_SendIssuedAttachment) that calls connectedAPI
private class EFLOverrideTest {
    @IsTest static void EFLOverrideTest() {
        
        ///Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        //User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True  LIMIT 1];
        //User u2 = [SELECT ID,Name FROM User WHERE isActive=True AND usertype = 'PowerPartner' LIMIT 1];
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();  
        Contact objcont = testData.newcontact();
        User usershare = new User();
        usershare = new User();
        usershare = EFLUserTestDataFactory.getUser('eFile Applicant');        
        id contactIdforSharing = [select contactid from user where id = :usershare.id limit 1].contactid;
        
        CARPOL_AC_TestDataManager testDataP = new CARPOL_AC_TestDataManager();  
        Contact objcontP = testDataP.newcontact();
        User usershareP = new User();
        usershareP = EFLUserTestDataFactory.getUser('Customer Community Plus');        
        
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp); 
        ac.OwnerId = usershare.id;
        update ac;
        objapp.Applicant_Name__c = contactIdforSharing;
        update objapp;
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.RecordTypeID = ACauthRecordTypeId;
            update objauth;
        ac.Authorization__c = objauth.Id;
            update ac;

        system.runAs(usershare){
            PageReference pageRef = new PageReference('/apex/CARPOL_UNI_ViewAuthorization');
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',ac.id);    
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
            CARPOL_UNI_Override orclass = new CARPOL_UNI_Override(sc);
            orclass.viewRedirect();
            orclass.addRedirect();   
        }
        
        system.runAs(usershareP){
            PageReference pageRef = new PageReference('/apex/CARPOL_UNI_ViewAuthorization');
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',objauth.id);   
            apexpages.currentpage().getparameters().put('Include_App_ID__c', 'true'); 
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(usershareP);
            CARPOL_UNI_Override orclass = new CARPOL_UNI_Override(sc);
            orclass.viewRedirect();
            orclass.addRedirect();   
        }
    }
        @IsTest static void EFLOverrideTest2() {
        
        ///Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        //User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True  LIMIT 1];
        //User u2 = [SELECT ID,Name FROM User WHERE isActive=True AND usertype = 'PowerPartner' LIMIT 1];
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();  
        Contact objcont = testData.newcontact();
        User usershare = new User();
        usershare = EFLUserTestDataFactory.getUser('eFile Applicant');        
        id contactIdforSharing = [select contactid from user where id = :usershare.id limit 1].contactid;
        
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp); 
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.RecordTypeID = ACauthRecordTypeId;
            update objauth;
        ac.Authorization__c = objauth.Id;
            update ac;
        ac.OwnerId = usershare.id;
        update ac;
        objapp.Applicant_Name__c = contactIdforSharing;
        update objapp;    
        system.runAs(usershare){
            PageReference pageRef = new PageReference('/apex/CARPOL_UNI_ViewApplicationLineItem');
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',ac.id);    
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
            CARPOL_UNI_Override orclass = new CARPOL_UNI_Override(sc);
            orclass.viewRedirect();
            orclass.addRedirect();   
        }

    }
    
     @IsTest static void EFLOverrideTest3() {
        
        ///Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        //User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True  LIMIT 1];
        //User u2 = [SELECT ID,Name FROM User WHERE isActive=True AND usertype = 'PowerPartner' LIMIT 1];
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();  
        Contact objcont = testData.newcontact();
        User usershare = [Select ID FROM User Where UserType='Standard' AND isActive=true LIMIT 1];
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp); 
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.RecordTypeID = ACauthRecordTypeId;
            update objauth;
        ac.Authorization__c = objauth.Id;
            update ac;
            
        system.runAs(usershare){
            PageReference pageRef = new PageReference('/apex/CARPOL_UNI_AddRegulation');
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',objauth.id);    
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
            CARPOL_UNI_Override orclass = new CARPOL_UNI_Override(sc);
            orclass.viewRedirect();
            orclass.addRedirect();   
        }

    }
}