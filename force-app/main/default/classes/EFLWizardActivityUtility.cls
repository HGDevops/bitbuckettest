public class EFLWizardActivityUtility {
    
    public static List<SelectOption> getOptions(EFLWizardActivity wizardActivity)
    {
        List<SelectOption> options = new List<SelectOption>();
        for(Wizard_Selections__c opt : wizardActivity.Options)
        {
            options.add(new SelectOption(opt.Id,opt.Name));
        }
        return options;
    }
    
    
    public static boolean setSelectedOptionByActivityResult(EFLWizardActivity wizardActivity)
    {
        if(string.isNotBlank(wizardActivity.activityResult))
        {
            for(Wizard_Selections__c opt : wizardActivity.Options)
            {
                if(opt.Activity_Processor_Result__c == wizardActivity.activityResult)
                {
                    wizardActivity.EvaluatedOption = opt;
                    return true;
                }
            }
        }
        return false;
    }
    
    public static boolean setSelectedOptionByValue(EFLWizardActivity wizardActivity)
    {
        if( String.isNotBlank( wizardActivity.SelectedOption) ){
       
            for(Wizard_Selections__c opt : wizardActivity.Options)
            {
                if(opt.id == wizardActivity.SelectedOption)
                {
                   wizardActivity.EvaluatedOption = opt;
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public static boolean isActivityProcessorDependent(EFLWizardActivity wizardActivity)
    {
        for(Wizard_Selections__c opt : wizardActivity.Options)
        {
            if(string.isNotBlank(opt.Activity_Processor_Result__c))
            {
                return true;
            }
        }
        return false;
    }

}