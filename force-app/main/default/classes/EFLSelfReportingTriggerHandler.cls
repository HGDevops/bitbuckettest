//Developer Name: Peter Tran, Manju Yeddanpalli
//Creation Date: 03/18/2019
//File Name: EFLSelfReportingTriggerHandler.apxc
//sObject Affected: Self_Reporting__c
//Client: United States Department of Agriculture
//Client Project Name: APHIS CARPOL
//Contractor: Accenture Federal Services

    /*****************************
     *  Developer Documentation  *
     *****************************
     * _________________________
     * Trigger Handler Description
     * 
     * Changelog
     * V1.0: Creation of EFLSelfReportingTriggerHandler.apxc
     * - File created in DevShare on 03/18/2019
     * 
     */

//Begin class declaration
public inherited sharing class EFLSelfReportingTriggerHandler {
    
    Public static final string FIELD_TEST_REPORT = 'Field Test Report';
    
    //This method checks if a Self_Reporting__c record is already present for this location and Start Date
    public static void checkForExistingRecordLocationAndStartDate(List<Self_Reporting__c> triggerNew) {
   // Commented for Bug W-036859
    /*    System.debug('Begin checkForExistingRecordLocationAndStartDate()');
        Id prrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
        //Get list of existing Self_Reporting__c records with its associated start date and location data
        List<Self_Reporting__c> existingRecords = [SELECT Id, Start_Date__c, Release_Record_ID__c, Report_Summary__c FROM Self_Reporting__c];
        
        //For all existing records in the Self_Reporting__c sObject
        for(Self_Reporting__c e : existingRecords) {
            //For all records that were inserted by the execution context in Trigger.new
            for(Self_Reporting__c i : triggerNew) {
                //If the start date and location data in the existing record matches the start date and location data in the Trigger.new record
                if(e.Start_Date__c == i.Start_Date__c && i.RecordTypeId == prrectypeid &&
                   e.Release_Record_ID__c == i.Release_Record_ID__c &&
                  e.Report_Summary__c == i.Report_Summary__c) {
                    System.debug('There is a duplicate record for Self_Reporting__c Record ID: ' + e.Id);
                       //Add two field level validations to make sure that the user is not entering data that has the same start date and location
                 // Commented for Bug W-036859            i.Start_Date__c.addError('There are record(s) where the Start Date is the same.');
                 // Commented for Bug W-036859            i.Release_Record_ID__c.addError('There are record(s) where the Release Location is the same.');
                } else {
                    System.debug('This record is ready for insert.');
                } //end else
        	} //End inner for loop
        }//End outer for loop
   */ } //end method checkForExistingRecordLocationAndStartDate()
    
    public static void checkUniquePlantingId(List<Self_Reporting__c> triggerNew) {
        System.debug('Begin checkUniquePlantingId');
        Map<String,Self_Reporting__c> mapPlantingReport = new Map<String,Self_Reporting__c>();
        Map<String,Self_Reporting__c> mapAuthId = new Map<String,Self_Reporting__c>();
        //Get the Record type Id for "Planting Report" type on Self Reporting
        Id prrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
        //For all records that were inserted by the execution context in Trigger.new
        for(Self_Reporting__c sr: triggerNew){
            //Check for "Planting Report" Record Type and create a map with planting Id from self report record
            if(sr.RecordTypeId == prrectypeid){
                mapPlantingReport.put(sr.Planting_ID__c,sr);
                mapAuthId.put(sr.Authorization__c,sr);
            }
        }
        if(!mapPlantingReport.isEmpty()){
            //Get list of existing Self_Reporting__c records with the Planting Ids from the map
            for(Self_Reporting__c selfReport : [Select id,Planting_ID__c,Authorization__c,Is_No_Planting__c from Self_Reporting__c 
                                                Where Planting_ID__c in :mapPlantingReport.keyset() and Authorization__c in :mapAuthId.keyset()]){
            	
				//Error Validation thrown when there is a self report with the same planting ID is found
                                                    if(mapPlantingReport.get(selfReport.Planting_ID__c).Is_No_Planting__c){
                                                        mapPlantingReport.get(selfReport.Planting_ID__c).Planting_ID__c.addError('The No Planting ID provided has been used in a previous planting record. A unique No Planting ID must be entered.');
                                                    }else{
                mapPlantingReport.get(selfReport.Planting_ID__c).Planting_ID__c.addError('The Planting ID provided has been used in a previous planting record. A unique Planting ID must be entered for every planting/release.');
                                                    }
             }
        }
    }
    
    //This is common on beforeInsert Method where validations and other required logics can be performed by recordType
    public static void onBeforeInsert(List<Self_Reporting__c> newSelfReports)
    {
        //Feild Test Record Type Id
    	id fieldTestRecordTypeId = EFLGenericUtility.getRecordTypeId(FIELD_TEST_REPORT);
        set<Id> locationIds = new set<Id>();
        map<Id, set<Date>> locationIdPlantingDatesMap = new map<Id, set<Date>>();
        set<Id> authIds = new set<Id>();
		for(Self_Reporting__c selfReporting:newSelfReports)
		{
			if(selfReporting.recordTypeId == fieldTestRecordTypeId)
            {
				locationIds.add(selfReporting.Release_Record_ID__c);
                authIds.add(selfReporting.Authorization__c);
			}
        }
        EFLSelfReportingHelper EFLSelfReportHelper = new EFLSelfReportingHelper(authIds);
        locationIdPlantingDatesMap = EFLSelfReportHelper.locationIdplantingDatesMap(authIds, locationIds);
        
        for(integer i = 0; i<newSelfReports.size(); i++)
        {
            //Feild Test Report Related Logic
            if(newSelfReports[i].recordTypeId == fieldTestRecordTypeId)
            {
				EFLSelfReportHelper.validateFieldTestReport(true, newSelfReports[i], new Map<string, string>(), locationIdPlantingDatesMap.get(newSelfReports[i].Release_Record_ID__c));
            }
        }
    }
    
    //This is common on beforeInsert Method where validations and other required logics can be performed by recordType
    public static void onBeforeUpdate(List<Self_Reporting__c> newSelfReports, List<Self_Reporting__c> oldSelfReports)
    {
        //Feild Test Record Type Id
    	id fieldTestRecordTypeId = EFLGenericUtility.getRecordTypeId(FIELD_TEST_REPORT);
        set<Id> locationIds = new set<Id>();
        map<Id, set<Date>> locationIdPlantingDatesMap = new map<Id, set<Date>>();
        set<Id> authIds = new set<Id>();
		for(Self_Reporting__c selfReporting:newSelfReports)
		{
			if(selfReporting.recordTypeId == fieldTestRecordTypeId)
            {
				locationIds.add(selfReporting.Release_Record_ID__c);
                authIds.add(selfReporting.Authorization__c);
			}
        }
        EFLSelfReportingHelper EFLSelfReportHelper = new EFLSelfReportingHelper();
        locationIdPlantingDatesMap = EFLSelfReportHelper.locationIdplantingDatesMap(authIds, locationIds);
        
        for(integer i = 0; i<newSelfReports.size(); i++)
        {
            //Feild Test Report Related Logic
            if(newSelfReports[i].recordTypeId == fieldTestRecordTypeId)
            {
				EFLSelfReportHelper.validateFieldTestReport(true, newSelfReports[i], new Map<string, string>(), locationIdPlantingDatesMap.get(newSelfReports[i].Release_Record_ID__c));
            }
        }
    }
    
    // This method will be called on both after insert and after update
    // This will update No Reporting Flag on location when Planting/Release Reports is added or updated
    public static void UpdateNoPlantingFlag(List<Self_Reporting__c> newSelfReports, Map<Id, Self_Reporting__c> oldSelfReports) {
        Set<Self_Reporting__c> plantingReleaseReports = new Set<Self_Reporting__c>();
        Map<Id, Location__c> selfReportingLocations = new Map<Id, Location__c>();
        List<Location__c> sRLocationsUpdateNoPlanting = new List<Location__c>();
        Location__c parentLocaiton;
        Self_Reporting__c sRPRSOld;
        Set<Id> selfReportingLocationIds = new Set<Id>();
        for(Self_Reporting__c sR:newSelfReports){
            system.debug('sR.ID: '+sR.Id);
            system.debug('sR.recordtypeid: '+sR.recordTypeId);
            // Filter Planting/Release Reports only
            if(Schema.getGlobalDescribe().get('Self_Reporting__c').getDescribe().getRecordTypeInfosById().get(sR.recordTypeId).getName() == 'Planting/Release Reports'){
                plantingReleaseReports.Add(sR);
                selfReportingLocationIds.Add(sR.Release_Record_ID__c);
            }
        }
        // Create parent location Map
        if(selfReportingLocationIds.size()>0){
            for(Location__c loc:[SELECT Id, No_Planting_Flag__c FROM Location__c WHERE Id IN :selfReportingLocationIds]){
                selfReportingLocations.Put(loc.Id, loc);
            }
        }
        // When After Insert
        if(oldSelfReports == null){
            for(Self_Reporting__c sRPRS:plantingReleaseReports){
                parentLocaiton = selfReportingLocations.get(sRPRS.Release_Record_ID__c);
                // When No Planting record added and location no planting flag is off
                if(sRPRS.Is_No_Planting__c && !parentLocaiton.No_Planting_Flag__c){
                    parentLocaiton.No_Planting_Flag__c = true;
                    sRLocationsUpdateNoPlanting.Add(parentLocaiton);
                }
                // When Planting record added and location no planting flag is on
                else if(!sRPRS.Is_No_Planting__c && parentLocaiton.No_Planting_Flag__c){
                    parentLocaiton.No_Planting_Flag__c = false;
                    sRLocationsUpdateNoPlanting.Add(parentLocaiton);
                }
            }
        }
        else
            // When After Update
        {
            for(Self_Reporting__c sRPRS:plantingReleaseReports){
                parentLocaiton = selfReportingLocations.get(sRPRS.Release_Record_ID__c);
                sRPRSOld = oldSelfReports.get(sRPRS.Id);
                // When planting record updated to No Planting record and location no planting flag is off
                if((sRPRS.Is_No_Planting__c && !sRPRSOld.Is_No_Planting__c) && !parentLocaiton.No_Planting_Flag__c){
                    parentLocaiton.No_Planting_Flag__c = true;
                    sRLocationsUpdateNoPlanting.Add(parentLocaiton);
                }
                // When No planting record updated to Planting record and location no planting flag is on
                else if((!sRPRS.Is_No_Planting__c && sRPRSOld.Is_No_Planting__c) && parentLocaiton.No_Planting_Flag__c){
                    parentLocaiton.No_Planting_Flag__c = false;
                    sRLocationsUpdateNoPlanting.Add(parentLocaiton);
                }
            } 
        }
        if(sRLocationsUpdateNoPlanting.Size() > 0){
            update sRLocationsUpdateNoPlanting;
            system.debug('updated number of records:'+sRLocationsUpdateNoPlanting.size());
        }
    }
}