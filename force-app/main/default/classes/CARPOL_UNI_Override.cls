/**
* Author : AFS
* Created Date : 10/13/2016
* Purpose : This handles Override across the internal and community pages 
*/
public with sharing class CARPOL_UNI_Override{

 String recordId;
 String strurl;
 String strUrlAttributes;
 String UserType;
 Map<String,string>parametersMap = new Map<string,string>();
 List<String> UserTypeList;
 Set<String>UserTypeSet = new Set<String>();
 PageReference customPage;
  PageReference customPage2;
 EFL_VF_Override__c pageOverride;
 String attributes='';
 AC__c LineItem; 
 String appId;
 
    public CARPOL_UNI_Override(ApexPages.StandardController controller) {
         recordId = controller.getId();
         strurl = ApexPages.currentPage().getUrl();
         strurl = strurl.split('apex/')[1]; 
         strUrlAttributes = strurl.split('\\?')[1];
         strUrlAttributes = '?'+strUrlAttributes;
         integer iend = strurl.indexOf('?');
         strurl = strurl.substring(0 , iend);
         parametersMap = ApexPages.currentPage().getParameters();
         UserType = UserInfo.getUserType();
         pageOverride = EFL_VF_Override__c.getInstance(strurl);
         UserTypeList = pageOverride.EFL_User_type__c.split(';');
         UserTypeSet.addall(UserTypeList);
         
         
    }

    public PageReference viewRedirect() {
        //system.debug('enter viewredirect');
        if(UserTypeSet.contains(UserType)){
             for(String s:parametersMap.keyset()){
                 attributes = attributes+s+'=';
                 attributes=attributes+parametersMap.get(s)+'&';
               }
               
             attributes=attributes.removeEndIgnoreCase('&');
             String hostname = ApexPages.currentPage().getHeaders().get('Host');
             String optyURL2;
             if(UserType=='PowerPartner'){
                       optyURL2 = 'https://'+hostname+'/st/apex/'+pageOverride.EFL_Redirect_page__c+'?'+attributes;
                       //system.debug(':ine 48>>>>>>>>'+optyURL2);
                       //Below is the Exception for Line Item
                       if(pageOverride.Include_App_ID__c){
                           LineItem = [select application_number__c,Program_Line_Item_Pathway__c from AC__C where id=:recordID Limit 1];
                           appId = LineItem.application_number__c;
                           optyURL2 = 'https://'+hostname+'/st/apex/'+pageOverride.EFL_Redirect_page__c+'?'+attributes+'&appId='+appId;
                           //system.debug('Final URL is >>>'+optyURL2);
                       }
             }else{
                       optyURL2 = 'https://'+hostname+'/apex/'+pageOverride.EFL_Redirect_page__c+'?'+attributes;
                       //Below is the Exception for Line Item
                       if(pageOverride.Include_App_ID__c){
                           LineItem = [select application_number__c,Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.Program__r.Name 
                                         from AC__C 
                                         where id=:recordID 
                                         Limit 1];
            
                           appId = LineItem.application_number__c;
                            if(LineItem.Program_Line_Item_Pathway__r.Program__r.Name == 'BRS' && pageOverride.EFL_Redirect_page__c == 'CARPOL_UNI_LineItem'){
                               pageOverride.EFL_Redirect_page__c = 'EFLLineItem';
                               optyURL2 = 'https://'+hostname+'/apex/'+pageOverride.EFL_Redirect_page__c+'?'+attributes;
                            }else{
                               optyURL2 = 'https://'+hostname+'/apex/'+pageOverride.EFL_Redirect_page__c+'?'+attributes+'&appId='+appId;
                            } 
                         }
                }
             
             customPage = new pagereference(optyURL2);
             customPage.setRedirect(true);
             return customPage;
          }
             else{
             String hostname = ApexPages.currentPage().getHeaders().get('Host');
             String optyURL2 = 'https://'+hostname+'/'+recordID +'?nooverride=1';
             customPage = new pagereference(optyURL2);
             customPage.setRedirect(true);
             return customPage;
             }
         
     }
     
     public PageReference addRedirect(){
          
         if(UserTypeSet.contains(UserType)){
             
             if(UserType=='PowerPartner'){
                      customPage = new PageReference('/st'+'/apex/'+pageOverride.EFL_Redirect_page__c);
             }else{
                      customPage = new PageReference('/'+'/apex/'+pageOverride.EFL_Redirect_page__c);
                }
             
           }
             else{ 
                 
                 if((pageOverride.EFL_Calling_page__c == '' || pageOverride.EFL_Calling_page__c == null) && (pageOverride.ObjectAPIName__c !='' && pageOverride.ObjectAPIName__c !=null)){
                       string objectApiName = pageOverride.ObjectAPIName__c;
                       Map<String, Schema.SObjectType> sObjectmap  = Schema.getGlobalDescribe() ;
                       String keyPrefix = sObjectmap.get(objectApiName).getDescribe().getKeyPrefix();
                       customPage = new PageReference('/'+keyPrefix+'/e'+'?nooverride=1');
                  }else{
                       customPage = new PageReference('/'+pageOverride.EFL_Calling_page__c+'/e'+'?nooverride=1');
                   }
                 }
                 
                 for(String s:parametersMap.keyset()){
                     if(s.contains('core.apexpages.devmode.url') || s.contains('save_new') || s.contains('sfdc.override')){
                        }else{
                           customPage.getParameters().put(s, parametersMap.get(s)); 
                        }
                   }
                   
                 customPage.setRedirect(true);
                 return customPage;
             
     }
    
 }