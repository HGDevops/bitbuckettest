/*
 * Author: keerthi Jakkaraju Date:03/07/2019
 * Purpose: Central place to access Regulated Article Information object from database with different set of filter parameters.
*/
public inherited sharing class EFLregulatedarticleinformationRepository {

    // Obtain RegulatedArticle Information records using line Item ID 
    // RegulatedArticle_Information__c
    public static List<RegulatedArticle_Information__c> selectRAINFByLineItemID(id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('RegulatedArticle_Information__c');
           return [SELECT id,Name,Age__c,Breed__r.Name,Breed_Description__c,Color__c,Date_of_Birth__c,Dog_Name__c,Microchip_Number__c,Other_identifying_information__c,Sex__c,Tattoo_Number__c,EnableRAUpdateAction__c ,EnableRADisplayAction__c,EnableRADeleteAction__c,Status__c,Age_yr_months__c,Status_Graphical__c, Document_Status__c
                      FROM RegulatedArticle_Information__c
                     WHERE Line_Item__c =:lineItemID];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLregulatedarticleinformationRepository.selectRAINFByLineItemID()',e);                
        } 
         return null;
    }
    
    public static List<RegulatedArticle_Information__c > selectRAINFByLineItemIDForClone(id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('RegulatedArticle_Information__c');
           return [SELECT Name, Age__c,Breed__r.Name,Breed_Description__c,Color__c,Date_of_Birth__c,Dog_Name__c,Microchip_Number__c,Other_identifying_information__c,Sex__c,Tattoo_Number__c,EnableRAUpdateAction__c ,EnableRADisplayAction__c,EnableRADeleteAction__c,Status__c,Age_yr_months__c,Status_Graphical__c, Document_Status__c
                      FROM RegulatedArticle_Information__c
                     WHERE Line_Item__c =:lineItemID];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLregulatedarticleinformationRepository.selectRAINFByLineItemIDForClone()',e);                
        } 
         return null;
    }
    
     // Obtain RegulatedArticle Information record using id  
     // RegulatedArticle_Information__c
    public static RegulatedArticle_Information__c selectByID(id rainfID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('RegulatedArticle_Information__c');
           return [SELECT id,Name, Age__c,Breed__r.Name,Breed_Description__c,Color__c,Date_of_Birth__c,Dog_Name__c,Line_Item__c,Microchip_Number__c,Other_identifying_information__c,Sex__c,Tattoo_Number__c,EnableRAUpdateAction__c ,EnableRADisplayAction__c,EnableRADeleteAction__c,Status__c,Age_yr_months__c,Status_Graphical__c, Document_Status__c
                      FROM RegulatedArticle_Information__c
                     WHERE ID =:rainfID];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLregulatedarticleinformationRepository.selectByID()',e);                
        } 
         return null;
    }
    
    // Obtain RegulatedArticle Information records using line Item ID and searchfilter
    // RegulatedArticle_Information__c
    public static List<RegulatedArticle_Information__c> selectRAINFByLineItemIDAndWildCardSearchFilter(id lineItemID, string wildCardSearchFilter){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('RegulatedArticle_Information__c');
           return [SELECT id,Name,Age__c,Breed__r.Name,Breed_Description__c,Color__c,Date_of_Birth__c,Dog_Name__c,Microchip_Number__c,Other_identifying_information__c,Sex__c,Tattoo_Number__c,EnableRAUpdateAction__c ,EnableRADisplayAction__c,EnableRADeleteAction__c,Status__c,Age_yr_months__c,Status_Graphical__c, Document_Status__c
                      FROM RegulatedArticle_Information__c
                      WHERE (Line_Item__c=:lineItemID) AND
                      (Name LIKE :wildCardSearchFilter
                      OR Dog_Name__c LIKE :wildCardSearchFilter
                      OR Breed__r.Name LIKE :wildCardSearchFilter 
                      OR Color__c LIKE :wildCardSearchFilter
                      OR Age_yr_months__c LIKE :wildCardSearchFilter
                      OR Sex__c LIKE :wildCardSearchFilter
                      OR Document_Status__c LIKE :wildCardSearchFilter 
                      ) 
                  ];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLregulatedarticleinformationRepository.selectRAINFByLineItemID()',e);                
        } 
         return null;
    }
    
  }