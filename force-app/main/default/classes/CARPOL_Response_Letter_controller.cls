public with sharing class CARPOL_Response_Letter_controller {
    
    public Incident__c incident { get; set; }
    public String templateURL { get; set; }
    public String selectedTemplateType { get; set; }
    public SelectOption[] allTemplateOptions { get; set; }
    public String baseURL { get; set; }
    public CARPOL_Response_Letter_controller(ApexPages.StandardController controller) {
        this.incident = (Incident__c)controller.getRecord();
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        if(!Test.isRunningTest()){
            incident=[SELECT Id,Name, Action__c, EFL_APHIS_Incident_Id__c, EFL_Template__c,EFL_Issued_Date__c,EFL_Compliance_letter_content__c,Application__c, EFL_Associated_Incidents__c, Authorization__c,Authorization__r.Name, EFL_Compliance_officer_Queue__c,  Facility__c, Facility__r.Name, Inspection__c, Inspection__r.Scheduled_Date_of_Inspection__c, Inspection__r.Inspector__r.Name ,Program_Compliance_officer__c,Program_Compliance_officer__r.Name, Related_Program__c, Requester_email__c, Requester_Name__c FROM Incident__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
        }
        
    }
    
    public PageReference populateTemplate()
    {
        // System.Debug('<<<<<<< Template : ' + incident. EFL_Template__c + ' >>>>>>>');
        try
        {
            if(incident.EFL_Template__c != null)
            {
                incident.EFL_Compliance_letter_content__c = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID =:incident.EFL_Template__c LIMIT 1].Content__c;
                
                //Replacing Variables
                if (incident.Requester_Name__c != null && incident.Requester_Name__c != 'null')
                    incident.EFL_Compliance_letter_content__c = incident.EFL_Compliance_letter_content__c.replace('{!Responsible Party}', incident.Requester_Name__c);
                if (incident.Facility__r.Name != null && incident.Facility__r.Name != 'null')
                    incident.EFL_Compliance_letter_content__c = incident.EFL_Compliance_letter_content__c.replace('{!Facility}', incident.Facility__r.Name);
                
                if (incident.Authorization__r.Name != null && incident.Authorization__r.Name != 'null'){
                    incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!AuthorizationNumber}', incident.Authorization__r.Name);
                }else{
                    incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!AuthorizationNumber}', ' ');
                }
                if (incident.Program_Compliance_officer__r.Name != null && incident.Program_Compliance_officer__r.Name != 'null')
                    incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!ProgramComplianceofficer}', incident.Program_Compliance_officer__r.Name);
                
                update incident;
                previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Template populated successfully.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a template to be populated.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a valid template to be populated.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    
    public PageReference saveDraft()
    {
        try{
            if(incident.EFL_Issued_Date__c < System.Today())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Date Issued cannot be in the past.'));
            }
            else
            {
                update incident;
                //previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft saved successfully.'));
            }
        }
        catch(Exception e)
        {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    
    
    
    
    public PageReference attachLetter()
    {
        try{
            
            String attachmentName;
            PageReference pdfComplianceLetter = Page.CARPOL_IncidentLetter;
            attachmentName = 'Incident Letter' + System.TODAY().format();
            
            
            pdfComplianceLetter.getParameters().put('id', incident.ID);
            //pdfComplianceLetter.getParameters().put('tId', wtask.Id);
            Blob pdfComplianceLetterBlob;
            
            if (Test.IsRunningTest())
            {
                pdfComplianceLetterBlob =Blob.valueOf('UNIT.TEST');
            }
            else
            {
                
                pdfComplianceLetterBlob = pdfComplianceLetter.getContent();
                
            }
            
            
            String tempAttachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
            //System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :incident.ID AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size() > 0)
            {
                if(prevAttachments.size() == 1){attachmentName = attachmentName + '_v2';}
                else
                {
                    for(Integer i = 0; i < prevAttachments.size(); i++ )
                    {
                        if(i == prevAttachments.size() - 1)
                        {
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);
                            //System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;
                            //System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');
                            attachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;
                            //System.Debug('<<<<<<< New Attachment Name : ' + attachmentName + ' >>>>>>');
                        }
                    }
                }
            }
            
            attachmentName = attachmentName + '.pdf';
            Attachment attachment = new Attachment(parentId = incident.ID, name = attachmentName, body = pdfComplianceLetterBlob);
            insert attachment;
            //System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Compliance Letter attached successfully.'));
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    
    public PageReference getTemplates()
    {
        if(selectedTemplateType != null)
        {
            //System.Debug('<<<<<<< Getting all the templates and generating preview >>>>>>>');
            allTemplateOptions = new List<SelectOption>();
            List<Communication_Manager__c> tempTemplates = [SELECT ID, Name FROM Communication_Manager__c WHERE Type__c = :selectedTemplateType];
            for (Communication_Manager__c template : tempTemplates ) 
            {allTemplateOptions.add(new SelectOption(template.ID, template.Name));}
            
        }
        return null;
    }
    
    public PageReference previewTemplate()
    {
        try{
            
            templateURL = '/apex/CARPOL_IncidentLetter?id=' + incident.ID;
            
        }
        catch(Exception e)
        {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
}