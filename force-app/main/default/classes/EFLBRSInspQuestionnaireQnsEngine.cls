public with sharing class EFLBRSInspQuestionnaireQnsEngine implements EFLInspQuestionnaireQnsEngine{
	
	public static map<id, integer> inspectionQuestionCountMap = new map<id, integer>();
	public static map<id, list<EFL_Inspection_Questionnaire_Questions__c>> inspectionQuestionMap = new map<id, list<EFL_Inspection_Questionnaire_Questions__c>>();
	
	public static void reorderInspectionQuestions(list<EFL_Inspection_Questionnaire_Questions__c> questionnaireList){
		integer i=1;
		for(EFL_Inspection_Questionnaire_Questions__c qnRec: questionnaireList){			
			qnRec.order__c = i++;
		}		
	}
	
	public static void setOrder(EFL_Inspection_Questionnaire_Questions__c questionnaireQnRec){
		
		if(inspectionQuestionCountMap!=null && inspectionQuestionCountMap.containsKey(questionnaireQnRec.inspection__c)){
            // Rajesh Potla - Fix for W-034540
            Integer newOrder=inspectionQuestionCountMap.get(questionnaireQnRec.inspection__c) + 1;
			questionnaireQnRec.order__c = newOrder;
            inspectionQuestionCountMap.put(questionnaireQnRec.inspection__c,newOrder);
		}
	}
	
	public static void fetchQnCountForInspections(set<id> inspectionIdSet){
		//inspectionQuestionCountMap =  new map<id, integer>();
		list<inspection__c> inspectionList = [select id, (select id from Inspection_Questionnaire_Questions__r) from inspection__c where id in :inspectionIdSet];
		if(!inspectionList.isEmpty()){
			for(inspection__c inspRec: inspectionList){
				if(inspRec.Inspection_Questionnaire_Questions__r!=null){
					inspectionQuestionCountMap.put(inspRec.id, inspRec.Inspection_Questionnaire_Questions__r.size());
				}else{
					inspectionQuestionCountMap.put(inspRec.id, 0);
				}
			}
		}
	}
	
	public static void fetchQnsForReorder(set<id> inspectionIdSet){
		//inspectionQuestionMap =  new map<id, list<EFL_Inspection_Questionnaire_Questions__c>>();
		list<inspection__c> inspectionList = [select id, (select id, order__c from Inspection_Questionnaire_Questions__r order by order__c asc) from inspection__c where id in :inspectionIdSet];
		if(!inspectionList.isEmpty()){
			for(inspection__c inspRec: inspectionList){
				if(!inspRec.Inspection_Questionnaire_Questions__r.isEmpty()){
					inspectionQuestionMap.put(inspRec.id, inspRec.Inspection_Questionnaire_Questions__r);
				}
			}
		}
	}
    
}