/**
 * Class ChangeRecordFactory
 *
 * Used to instantiate and execute Change Record Handlers associated with sObjects.
 */
public with sharing class ChangeRecordFactory
{
    /**
     * Public static method to create and execute a Change Record handler
     *
     * Arguments:   Schema.sObjectType soType - Object type to process (SObject.sObjectType)
     *
     * Throws a Exception if no handler has been coded.
     */
    public static void loadObjectSpecificChanges(SObject oldValue, SObject newValue, Change_History__c change)
    {
        System.debug('Inside loadObjectSpecificChanges');
        IChangeHistoryHandler handler;
        
       // Get a handler appropriate to the object being processed
       if(oldValue != null)
           handler = getHandler(oldValue);
        else
           handler = getHandler(newValue); 
        
        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler != null)
        {
            // Execute the handler to record object specific changes
            execute(handler,oldValue,newValue,change);
        }
        
        
    }
    
    /**
     * private static method to control the execution of the handler
     *
     * Arguments:   IChangeHistoryHandler handler - A Trigger Handler to execute
     */ 
    private static void execute(IChangeHistoryHandler handler, SObject oldValue, SObject newValue, Change_History__c change)
    {
        // After Trigger
        if (Trigger.isAfter)
        {
            System.debug('Inside execute + isAfter');
                    
            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete)
            {
                System.debug('Inside execute + isAfter  + isDelete ');
                handler.populateDeletedRecordDetails(oldValue, change);
                
            }
            // Iterate through the records to be inserted passing them to the handler.
            else if (Trigger.isInsert)
            {
                handler.populateNewRecordDetails(newValue, change);
                
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate)
            {
                handler.populateUpdateDetails(newValue, oldValue, change);
            }
        }

    }
    
    /**
     * private static method to get the appropriate handler for the object type.
     * Modify this method to add any additional handlers.
     *
     * Arguments:   Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
     *
     * Returns:     IChangeHistoryHandler - A chabge history handler if one exists or null.
     */
    private static IChangeHistoryHandler getHandler(SObject obj)
    {
        Schema.sObjectType soType = obj.getSObjectType();
        System.debug('object type' +soType);
        if (soType == Phenotype__c.sObjectType)
        {
            return new PhenotypeChangeHistoryHandler();
        }
        else if (soType == Location__c.sObjectType)
        {
            return new LocationChangeHistoryHandler();
        }
        else if (soType == Material__c.sObjectType)
        {
            return new MaterialChangeHistoryHandler();
        }
        else if (soType == Related_Contact__c.sObjectType)
        {
            return new RelatedContactChangeHistoryHandler();
        }
        else if (soType == GPS_Coordinate__c.sObjectType)
        {
            return new GPSChangeHistoryHandler();
        }
        else if (soType == AC__c.sObjectType)
        {
            return new LineItemChangeHistoryHandler();
        }
        else if (soType == Link_Regulated_Articles__c.sObjectType)
        {
            return new LRAChangeHistoryHandler();
        }
        else if (soType == Article_Supplier_Developer__c.sObjectType)
        {
            return new ArticleSupplierChangeHistoryHandler();
        }
        else if (soType == Construct__c.sObjectType)
        {
            return new ConstructChangeHistoryHandler();
        }
        else if (soType == Genotype__c.sObjectType)
        {
            return new GenotypeChangeHistoryHandler();
        }
       /* else if (soType == Applicant_Attachments__c.sObjectType)
        {
            return new SOPChangeHistoryHandler();
        }*/
        else if (soType == Construct_Application_Junction__c.sObjectType)
        {
            return new PrevSOP_Con_ChangeHistoryHandler();
        }
        else if (soType == Authorization_Junction__c.sObjectType)
        {
            return new RelatedConitionsChangeHistoryHandler();
        }
       //KM:W-032142-Change History - Track Authorization Change
        else if (soType == Authorizations__c.sObjectType)
        {
            return new AuthorizationChangeHistoryHandler();
        }
       //KM:W-032142-Change History - Track Authorization Change
        else
        {
            system.debug('No Trigger Handler registered for Object Type: ' + soType);
        }
        
        return null;
    }


}