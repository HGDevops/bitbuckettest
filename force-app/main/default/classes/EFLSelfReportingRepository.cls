/*
 * Purpose: Central place to access Self Reporting object from database with different set of filter parameters.
*/ 
public with sharing class EFLSelfReportingRepository {
    // Obtain Self Report records using Report Summary ID    
    public static List<Self_Reporting__c> selfRepListByRSId(Id rsId) {
       try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Self_Reporting__c');
            return [Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,GPS_Coordinates_1__c,Is_Submitted__c,Is_No_Planting__c,
                    No_Monitoring_Report__c,GPS_Coordinates_2__c,GPS_Coordinates_3__c,Release_Record_ID__r.Location_Unique_Id__c,Release_Record_ID__r.name,Release_Record_ID__r.County_Text__c,
                    Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Anticipated_Harvest_Destruct_Date__c,Planting_ID__c,
                    Monitoring_Period_End__c,Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c,
                    Deleterious_Effects__c,Crop_Observations__c,GPS_Coordinates_4__c,GPS_Coordinates_5__c,GPS_Coordinates_6__c,
                    GPS_Coordinates_1__Latitude__s,GPS_Coordinates_2__Latitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_4__Latitude__s,
                    GPS_Coordinates_5__Latitude__s,GPS_Coordinates_6__Latitude__s,Longitude_6__c,Longitude_5__c,Longitude_4__c,Explanation__c,
                    Unexpected_Effects__c,Deleterious_Effects_Data__c,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Longitude__s,
                    GPS_Coordinates_3__Longitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Longitude__s,
                    Latitude_1__c,Latitude_2__c,Latitude_3__c,Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,Quantity_Acres_Text__c,Quantity_Acres_CBI__c,
                    Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c, Latitude_4__c,Latitude_5__c,Latitude_6__c,
                    In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, How_was_material_disposed__c,
                    Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, Off_Field_Description__c, 
                    Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c, 
                    Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c ,
                    Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Cleaning_Date__c,Description__c,(Select id from Related_Records__r) 
                    from Self_Reporting__c 
                    Where Report_Summary__c=:rsId];
       }catch(exception e){
           EFLErrorLog.createErrorLog('EFLSelfReportingRepository.selfRepListByRSId()',e);                
       } 
        return null;
    }
    // Obtain Self Report records using Report Summary ID    
    public static Self_Reporting__c selfRepById(String srId) {
       try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Self_Reporting__c');
            return [Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,GPS_Coordinates_1__c,Is_Submitted__c,Is_No_Planting__c,
                    No_Monitoring_Report__c,GPS_Coordinates_2__c,GPS_Coordinates_3__c,Release_Record_ID__r.Location_Unique_Id__c,Release_Record_ID__r.Name,Release_Record_ID__r.County_Text__c,
                    Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Anticipated_Harvest_Destruct_Date__c,Planting_ID__c,
                    Monitoring_Period_End__c,Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c,Authorization__c,
                    Deleterious_Effects__c,Crop_Observations__c,GPS_Coordinates_4__c,GPS_Coordinates_5__c,GPS_Coordinates_6__c,
                    GPS_Coordinates_1__Latitude__s,GPS_Coordinates_2__Latitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_4__Latitude__s,
                    GPS_Coordinates_5__Latitude__s,GPS_Coordinates_6__Latitude__s,Longitude_6__c,Longitude_5__c,Longitude_4__c,Explanation__c,
                    Unexpected_Effects__c,Deleterious_Effects_Data__c,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Longitude__s,
                    GPS_Coordinates_3__Longitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Longitude__s,
                    Latitude_1__c,Latitude_2__c,Latitude_3__c,Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,Quantity_Acres_CBI__c,
                    Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c, Latitude_4__c,Latitude_5__c,Latitude_6__c,
                    In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, How_was_material_disposed__c,
                    Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, Off_Field_Description__c, 
                    Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c, 
                    Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c ,
                    Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Cleaning_Date__c,Description__c,
                    Unexpected_Effects_Picklist__c, Deleterious_Effects_Picklist__c, Unexpected_Effects_CBI__c, Deleterious_Effects_CBI__c,
                    Is_monitoring_volunteers_required_FT__c, Is_monitoring_flowering_FT_site__c, Did_Flowering_Occur__c, 
                    Is_flowering_authorized__c, Required_to_submit_flowering_report__c 
                    from Self_Reporting__c  
                    Where Id=:srId];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.selfRepListById()',e);                
        } 
        return null;
    }
    // Obtain Self Report records using Report Summary ID & ReportType
    public static List<Self_Reporting__c> selfRepListByRSIdandRSType(List<Report_Summary__c> repSummList, string reportType) {
       try{
            return [Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,Is_Submitted__c,Is_No_Planting__c,
                    No_Monitoring_Report__c,Release_Record_ID__r.Location_Unique_Id__c,Release_Record_ID__r.Name,
                    Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Anticipated_Harvest_Destruct_Date__c,Planting_ID__c,
                    Monitoring_Period_End__c,Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c,
                    Deleterious_Effects__c,Crop_Observations__c,Explanation__c,Unexpected_Effects__c,Deleterious_Effects_Data__c,
                    Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,Quantity_Acres_CBI__c,
                    Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c, 
                    In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, How_was_material_disposed__c,
                    Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, Off_Field_Description__c, 
                    Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c, 
                    Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c ,
                    Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Description__c,(Select id from Related_Records__r)
                    from Self_Reporting__c 
                    Where Report_Summary__c in :repSummList and RecordType.Name = :reportType];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.selfRepListByRSId()',e);                
        } 
        return null;
    }
    // Search Self Report records using Search keyword within the Report Summary ID 
    public static List<Self_Reporting__c> searchSRByKeywordWithInReportSumID(Id rsId, string searckKey){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Self_Reporting__c');
            searckKey = '%' + searckKey + '%';
            return [Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,GPS_Coordinates_1__c,Is_Submitted__c,Is_No_Planting__c,
                    No_Monitoring_Report__c,GPS_Coordinates_2__c,GPS_Coordinates_3__c,Release_Record_ID__r.Location_Unique_Id__c,
                    Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Release_Record_ID__r.Name,Release_Record_ID__r.County_Text__c,
                    Anticipated_Harvest_Destruct_Date__c,Planting_ID__c,Monitoring_Period_End__c,Deleterious_Effects__c,
                    Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c,Crop_Observations__c,
                    GPS_Coordinates_4__c,GPS_Coordinates_5__c,GPS_Coordinates_6__c,GPS_Coordinates_1__Latitude__s,GPS_Coordinates_2__Latitude__s,
                    GPS_Coordinates_3__Latitude__s,GPS_Coordinates_4__Latitude__s,GPS_Coordinates_5__Latitude__s,GPS_Coordinates_6__Latitude__s,Longitude_6__c,
                    Longitude_5__c,Longitude_4__c,Explanation__c,Unexpected_Effects__c,Deleterious_Effects_Data__c,GPS_Coordinates_1__Longitude__s,
                    GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Longitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Longitude__s,
                    GPS_Coordinates_6__Longitude__s,Latitude_1__c,Latitude_2__c,Latitude_3__c,Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,
                    Quantity_Acres_CBI__c,Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c,
                    Latitude_4__c,Latitude_5__c,Latitude_6__c,In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, 
                    How_was_material_disposed__c, Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, 
                    Off_Field_Description__c, Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c, 
                    Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c ,
                    Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Cleaning_Date__c,Description__c,(Select id from Related_Records__r) 
                    from Self_Reporting__c 
                    Where Report_Summary__c=:rsId 
                        and
                        (
                         Name like :searckKey or
                         Release_Record_Id__r.Name like :searckKey or
                         Release_Record_ID__r.level_2_region__r.name like :searckKey or
                         Release_Record_ID__r.state__r.name like :searckKey or
                         Release_Record_Id__r.Location_Unique_Id__c  like :searckKey )];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.searchSRByKeywordWithInReportSumID()',e);                
        } 
        return null;
    }
    //Release LocationIds for Planting Report
    public static List<Location__c> releaseLocationsByAuth(Id autorizationId)
    {
        List<Location__c> plantedLocations = new List<Location__c>(); 
        try
        {
        EFLEnforceAccessUtility.checkObjectReadAccess('Authorizations__c');
        EFLEnforceAccessUtility.checkObjectReadAccess('AC__c');
        EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
        plantedLocations = [select id from Location__c where 
                            recordTypeId =: EFLGenericUtility.getRecordTypeId('Location Release Sites') 
                            and line_item__c in (select id from AC__c where Authorization__c = :autorizationId)
                         /*   and id in (select Release_Record_ID__c from Self_Reporting__c 
                                       where Authorization__c = :autorizationId
                                       and recordtype.name = :CARPOL_Constants.PLANTING_REPORT
                                      )*/
                           ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.plantedReleaseLocationsByAuth()',e);   
            throw e;
        } 
        return plantedLocations;
    }
    
    //Planted LocationIds for Field Test Report
    public static List<Location__c> plantedReleaseLocationsByAuth(Id autorizationId)
    {
        List<Location__c> plantedLocations = new List<Location__c>(); 
        try
        {
        EFLEnforceAccessUtility.checkObjectReadAccess('Authorizations__c');
        EFLEnforceAccessUtility.checkObjectReadAccess('AC__c');
        EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
        plantedLocations = [select id from Location__c where 
                            recordTypeId =: EFLGenericUtility.getRecordTypeId('Location Release Sites') 
                            and line_item__c in (select id from AC__c where Authorization__c = :autorizationId)
                            and id in (select Release_Record_ID__c from Self_Reporting__c 
                                       where Authorization__c = :autorizationId
                                       and recordtype.name = :CARPOL_Constants.PLANTING_REPORT
                                       and Is_No_Planting__c = false
                                      )
                           ];
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.plantedReleaseLocationsByAuth()',e);   
            throw e;
        } 
        return plantedLocations;
    }
    
    //Planted LocationIds for Field Test Report
    public static Database.QueryLocator getFieldTestLocationsAndReportsByPlantedLocations(List<Location__c> plantedLocations, id recordTypeId, id reportSummaryId)
    {
        List<Location__c> fieldTestLocationsAndReports = new List<Location__c>(); 
        try
        {
        EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
        EFLEnforceAccessUtility.checkObjectReadAccess('Self_Reporting__c');
        string Query = 'SELECT ID,Name ,RecordType.Name,Contact_Name1__c, '+
                    'Contact_Name2__c,Street_Add1__c,Street_Add2__c, '+
                    'Street_Add3__c,Street_Add4__c,City__c,Country_Text__c, '+
                    'Day_Phone_2__c,Zip__c,Other_Material_Types__c, '+
                    'Proposed_Planting__c,Number_of_Acres_Text__c, '+
                    'Primary_Contact_Last_Name__c,Contact_Address1__c, '+
                    'Primary_State_Text__c,Primary_Country_Text__c, '+
                    'Contact_Zip__c,Primary_County_Text__c,Contact_City__c, '+
                    'Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c, '+
                    'Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c, '+
                    'Secondary_Country_Text__c,Secondary_Contact_County__c, '+
                    'Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c,  '+
                    'Line_Item__c,Description__c,Applicant_Instructions__c, '+
                    'Country__r.Name,Level_2_Region__r.name, County_Name__c,	'+
                    'Line_Item__r.Application_Number__r.Application_Type__c, '+
                    'Status__c,State__r.Name,State_Name__c,Status_Graphical__c,CreatedDate, '+
                    'Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c, '+
                    'GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s, '+
                    'GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s, '+
                    'GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s, '+
                    'GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s, '+
                    'Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c, '+
                    'GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c, '+
                    'If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c, '+
                    'Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, '+
                    'Material_Type4__c, Material_Type5__c, Material_Type6__c, '+
                    'Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c, '+
                    'Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, '+
                    'Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, '+ 
                    'Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, '+ 
                    'Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, '+ 
                    'Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, '+ 
                    'Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c, '+
                    'Critical_Habitat_Involved_Text__c, Inspected_by_APHIS__c,Zip_CBI__c, Critical_Habitat_Involved_CBI__c, '+                  
                    'Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c, '+
                    'Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,Action_Required__c, '+
                    'EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Number_of_Proposed_Releases_CBI_Text__c,(select id,name,GPS_Coordinates_Text__c from GPS_Coordinates__r), '+
                    '(Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,Is_Submitted__c,Is_No_Planting__c, '+
                    'No_Monitoring_Report__c,Release_Record_ID__r.Location_Unique_Id__c,Release_Record_ID__r.Name, '+
                    'Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Anticipated_Harvest_Destruct_Date__c,Planting_ID__c, '+
                    'Monitoring_Period_End__c,Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c, '+
                    'Deleterious_Effects__c, Unexpected_Effects_Picklist__c, Deleterious_Effects_Picklist__c, Crop_Observations__c,Explanation__c,Unexpected_Effects__c,Deleterious_Effects_Data__c, '+
                    'Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,Quantity_Acres_CBI__c,Quantity_Acres_Text__c, '+
                    'Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c,  '+
                    'In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, How_was_material_disposed__c, '+
                    'Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, Off_Field_Description__c, '+ 
                    'Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c,  '+
                    'Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c , '+
                    'Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Description__c, Unexpected_Effects_CBI__c, Deleterious_Effects_CBI__c,  '+
            		'Is_monitoring_volunteers_required_FT__c, Is_monitoring_flowering_FT_site__c, Did_Flowering_Occur__c, '+
            		'Is_flowering_authorized__c, Required_to_submit_flowering_report__c ' +
                    'from Self_Reporting__r where recordtypeId = \''+recordTypeId+'\' ';
                    if(reportSummaryId!=null)
                    {
                        Query = Query + ' and Report_Summary__c = \'' + reportSummaryId+'\' ';
                    }
                    else
                    {
                        Query = Query + ' and Report_Summary__c = null ';
                    }
                    Query = Query +') '+
                    'from Location__c where Id in :plantedLocations ORDER by State_Name__c ASC, County_Name__c ASC, Name ASC, Location_Unique_Id__c ASC'; 
        return Database.getQueryLocator(Query);
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.getFieldTestLocationsAndReportsByPlantedLocations()',e);   
            throw e;
        } 
    }    
    
    //Get summary record
    public static Report_Summary__c getReportSummaryById(id reportSummaryId)
    {
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Report_Summary__c');
            return [select id , name,Status__c,Authorization__c,Equipment__c,Description__c,Submitted_Date__c,PDF__c,CBI_PDF_VF__c,
                    CBI_Deleted_PDF_VF__c,Certify_and_Submit__c,Report_Type__c, CBI_Count__c, CBI_Deleted_Count__c
                    from Report_Summary__c where id =: reportSummaryId limit 1]; 
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.getReportSummaryById()',e);                
        } 
        return null;
    }
    
    //Get observations by self report id
    public static List<Observation__c> getRelatedObservationsBySelfReportId(id selfReportId)
    {
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Observation__c');
            return [Select id, Number_of_Volunteers__c,Number_of_Volunteers_CBI__c, Units__c, Action_Taken__c, Observation_Date__c, Number_of_Volunteers_Text__c,
                Self_Reporting__c, Comments__c from Observation__c Where Self_Reporting__c=:selfReportId]; 
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.getRelatedObservationsBySelfReportId()',e);                
        } 
        return null;
    }
    
    //Get observation by self report id
    public static Observation__c getObservationById(id observationId)
    {
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Observation__c');
            return [Select id, Number_of_Volunteers__c,Number_of_Volunteers_CBI__c, Units__c, Action_Taken__c, Observation_Date__c, Number_of_Volunteers_Text__c,
                Self_Reporting__c, Comments__c from Observation__c Where id=:observationId limit 1]; 
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.getObservationById()',e);                
        } 
        return null;
    }
    
    //Validate observation exist on all Annual FieldTest Report By ReportSummaryId
    public static boolean observationExistOnAllAnnualFieldTestReportByReportSummary(id reportSummaryId)
    {
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Observation__c');
            List<Self_Reporting__c> selfReports = new List<Self_Reporting__c>();
            selfReports = [Select id, Observation_Count__c
                           from Self_Reporting__c Where Report_summary__c=:reportSummaryId  
                           and Field_Test_Report_Type__c = 'Annual (only applicable for multi-year permits)'
                           and Observation_Count__c = 0 
                           and (Is_monitoring_volunteers_required_FT__c='Yes'
                           		or Is_flowering_authorized__c='Yes' 
                                Or Is_flowering_authorized__c='No')
                          ]; 
            if(selfReports.isEmpty())
            {
                return true;
            }
            else
            {
                return false;
            }
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingRepository.getRelatedObservationsBySelfReportId()',e);                
        } 
        return null;
    }
    
}