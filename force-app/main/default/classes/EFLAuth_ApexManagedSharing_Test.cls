@isTest
private class EFLAuth_ApexManagedSharing_Test {
    static testMethod void TestaddAuthSharing() {
        List<Authorizations__c> authList = new List<Authorizations__c>();
        List<Application__c> appList = new List<Application__c>();
        Group gp = new Group(Name = 'TestaddAuthSharing');
        insert gp;
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        
        Test.startTest();
        CARPOL_Auth_ApexManagedSharing.addAuthSharing(gp.Id, authList);
        CARPOL_App_ApexManagedSharing.addAppSharing(gp.Id, appList);
        authList.add(objauth);
        appList.add(objapp);
        
        CARPOL_Auth_ApexManagedSharing.addAuthSharing(gp.Id, authList);
         CARPOL_App_ApexManagedSharing.addAppSharing(gp.Id, appList);
        Test.stopTest();
        List<Authorizations__Share> authShareList = [SELECT Id, parentId, UserOrGroupId, AccessLevel 
                                                     FROM Authorizations__Share
                                                     WHERE UserOrGroupId =: gp.Id];
        system.assert(authShareList[0].UserOrGroupId == gp.Id);
    }
    
}