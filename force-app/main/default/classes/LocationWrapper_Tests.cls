// created to get coverage on LocationWrapper by JB 5/2019.

// note: using generic assertions as business logic is unclear.  This is part of code coverage effort and
// unit tests should be reviewed to include meaningful assertions.
@isTest
public class LocationWrapper_Tests {

    @isTest
    static void getCoverage(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Id locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        testData.insertcustomsettings();
        Application__c app = testData.newapplication(); 
        AC__c li = testData.newLineItem('Adoption', app);
        
        Location__c loc = testData.newlocationByLineItem(li.Id,locrectypeid);
        list<Location__c> locRes = [SELECT Id, RecordType.Name FROM Location__c WHERE Id = :loc.Id];
        system.assertEquals(1, locRes.size());
        system.assertEquals('Release Sites Location', locRes[0].RecordType.Name);
        Material__c mat = testData.newMaterial(loc.Id);
        
        test.startTest();
        LocationWrapper lc = new LocationWrapper(loc);
        lc.location = [SELECT Id, RecordType.Name, Location_Unique_Id__c, Number_of_Acres_Text__c FROM Location__c WHERE Id = :loc.Id];
        string s = lc.information;
        system.assertNotEquals(null, s);
        test.stopTest();
    }
    
    @isTest
    static void getMoarCoverage(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Id locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();
        testData.insertcustomsettings();
        Application__c app = testData.newapplication(); 
        AC__c li = testData.newLineItem('Adoption', app);
        
        Location__c loc = testData.newlocationByLineItem(li.Id,locrectypeid);
        Material__c mat = testData.newMaterial(loc.Id);
        
        test.startTest();
        LocationWrapper lc = new LocationWrapper(loc);
        lc.location = [SELECT Id, RecordType.Name, Location_Unique_Id__c, Number_of_Acres_Text__c FROM Location__c WHERE Id = :loc.Id];
        string s = lc.information;
        system.assertNotEquals(null, s);
        test.stopTest();
    }
}