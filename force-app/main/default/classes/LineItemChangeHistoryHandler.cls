public class LineItemChangeHistoryHandler implements IChangeHistoryHandler {
    
    static final string fieldName = 'Name';
    static final string fieldLabel = 'Name';
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        Schema.SObjectType sObjectType = newRecord.getSObjectType();
        if (sObjectType != null)
        {
            //record identifier
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
                Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {

        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.Old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
        	populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        AC__c li = (AC__c)record;
        change.Application__c = li.Application_Number__c;
        if(!change.Delete_Flag__c)change.Line_Item__c = li.id;
        
    }

}