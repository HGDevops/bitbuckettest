@IsTest
public class EFLConstructUtility_Test {
  
    public static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    public static Regulated_Article__c regArt;
    public static Construct__c testConstruct;   
    public static AC__c ac1;
    Public static GenotypeType__c genoTypeType;
    Public Static Genotype__c genoType; 
    public static Country__c country;
    public static Level_1_Region__c lr;
    public static Level_2_Region__c lvl2Reg;
    
    @testSetup
    static void setupData(){
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c objApp = testData.newapplication();
        ac1=testData.newLineItemBRS('Personal Use',objApp);
        ac1.Type_of_Permit__c = 'Standard Permit';
        update ac1;
        Program_Line_Item_Pathway__c plip=testData.newCaninePathway();
        regArt=new  Regulated_Article__c();
        regArt.Name='Test article';
        regArt.RecordTypeID=Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        regArt.Status__c='Active';
        regArt.Category_Group_Ref__c=testdata.newgroup().Id;
        regArt.Program_Pathway__c=plip.id;
        Insert regArt; 
        
        link_regulated_articles__c lra = new link_regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(ac1.id, regArt.id, 'Draft');
        
        testConstruct = testData.newconstruct(ac1.Id,regArt);
        PhenoType__c phenoTypeExpected = testData.newphenotype(testConstruct.Id);
        
        genoTypeType = new GenotypeType__c();
        genoTypeType.Construct__c = testConstruct.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;
        
        genoType = testData.newgenotype(testConstruct.id, genoTypeType);
       
        country = testData.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
        
        lvl2Reg = testData.newlevel2region(lr.id);
        //Commented this code as this below newAuth method is failing with "EFLAuthorizationEngineFactory.EFLAuthorizationEngineException: No Authorization Engine found for Authorization: null"
        //Authorizations__c authObj = testData.newAuth(objApp.id);
        Domain__c objProg = new Domain__c();
        objProg.Name = 'AC';
        objProg.Active__c = true;
        insert objProg;
        
        Program_Prefix__c prefix = new Program_Prefix__c();
        prefix.Program__c = objProg.Id;
        prefix.Name = '555';
        insert prefix; 
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP';
        objTP.Recordtypeid = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c = prefix.Id;
        insert objTP;
        //appObj = testData.newapplication();
        
        Authorizations__c authObj=new  Authorizations__c();
        authObj.Application__c=objApp.id;
        authObj.RecordTypeID=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        //authObj.Status__c='Submitted';
        authObj.Date_Issued__c=date.today();
        authObj.Applicant_Alternate_Email__c='test@test.com';
        authObj.UNI_Zip__c='32092';
        authObj.UNI_Country__c='United States';
        authObj.UNI_County_Province__c='Duval';
        authObj.BRS_Proposed_Start_Date__c=date.today()+30;
        authObj.BRS_Proposed_End_Date__c=date.today()+40;
        authObj.CBI__c='CBI Text';
        authObj.Application_CBI__c='No';
        authObj.Applicant_Alternate_Email__c='email2@test.com';
        authObj.UNI_Alternate_Phone__c='(904) 123-2345';
        authObj.AC_Applicant_Email__c='email2@test.com';
        authObj.AC_Applicant_Fax__c='(904) 123-2345';
        authObj.AC_Applicant_Phone__c='(904) 123-2345';
        authObj.AC_Organization__c='ABC Corp';
        authObj.Means_of_Movement__c='Hand Carry';
        authObj.Biological_Material_present_in_Article__c='No';
        authObj.If_Yes_Please_Describe__c='Text';
        authObj.Applicant_Instructions__c='Make corrections';
        authObj.BRS_Number_of_Labels__c=10;
        authObj.BRS_Purpose_of_Permit__c='Importation';
        authObj.Documents_Sent_to_Applicant__c=false;
        authObj.Expiration_Timeframe_Override__c = 'Manual'; 
        authObj.Status__c = 'Approved';
        authObj.Thumbprint__c = objTP.Id;
        Insert authObj;
        
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        Applicant_Attachments__c appAttObj = testDataAC.newAttach(ac1.id);
        Construct_Application_Junction__c objCAJ = new Construct_Application_Junction__c();
        objCAJ.Construct__c = testConstruct.Id;
        objCAJ.Application__c = objApp.Id;
        objCAJ.Authorization__c = authObj.Id;
        objCAJ.Line_Item__c = ac1.Id;
        objCAJ.Design_Protocol_Record_SOP__c = appAttObj.Id;
        insert objCAJ;
    }

    public static testMethod void testGetPreviouslyReviewedConstructs() {
        setupData();
        Test.startTest();
        
            List<Construct__c> actualConstructs = EFLConstructUtility.getPreviouslyReviewedConstructs(ac1.Id);
        Test.stopTest();
    }  
}