public without sharing class CARPOL_Line_ApexManagedSharing {
    
    public static Boolean addLineSharing(Id publicGroupId, List<AC__c>Lines){
       
        List<sObject> Lineshares = new List<sObject>();
        
        try{
           
           system.debug('Inside line apex sharing');
               for(AC__c Line: Lines){
                        sObject LinedynObject= Schema.getGlobalDescribe().get('AC__Share').newSObject();
                        LinedynObject.put(Schema.AC__Share.ParentId, Line.id);
                        LinedynObject.put(Schema.AC__Share.UserOrGroupId, publicGroupId);
                        LinedynObject.put(Schema.AC__Share.AccessLevel, 'Edit');
                        LinedynObject.put(Schema.AC__Share.RowCause, Schema.AC__Share.RowCause.PartnerContact__c);
                        Lineshares.add(LinedynObject);
                    }
            
        }
        catch(Exception e){
            System.debug('Lines >>> ' + e.getMessage());
        }
        
        if(Lineshares.size() == 0)
            return false;
        else{
            insert Lineshares;
            system.debug('Succesfully Inserted');
            return true;
        }
        
       /*
       List<AC__c> Lines= [SELECT ID,Name FROM AC__c WHERE Application_Number__c IN:ApplicationIDs]; 
            if(Lines.size()!=0){
               for(AC__c Line: Lines){
                        sObject LinedynObject= Schema.getGlobalDescribe().get('AC__Share').newSObject();
                        LinedynObject.put(Schema.AC__Share.ParentId, Line.id);
                        LinedynObject.put(Schema.AC__Share.UserOrGroupId, publicGroupId);
                        LinedynObject.put(Schema.AC__Share.AccessLevel, 'Edit');
                        LinedynObject.put(Schema.AC__Share.RowCause, Schema.AC__Share.RowCause.PartnerContact__c);
                        Lineshares.add(LinedynObject);
                    }
            }*/
            
    }
    

}