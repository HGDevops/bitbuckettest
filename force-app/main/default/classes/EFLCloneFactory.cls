/**
 * Class EFLCloneFactory
 *
 * Used to instantiate and execute clone Record Handlers associated with sObjects.
 */
public with sharing class EFLCloneFactory {

    //Clones the record
	public static list<sObject> cloneRecord(string Program, string actionType,sObject record,application__c applicationReference,authorizations__c authRecord )
	{
           EFlCloneInterface handler; 
           handler = getHandler(Program);
      
        if(handler!=null){
            return handler.cloneRecord(record,applicationReference, actionType, authRecord);
        }
        return null; 
	}   

    //Gets the Handler based on the line item Program
    private static EFLCloneInterface getHandler(string Program)
    {
		if (Program == 'BRS')
		{
			return new EFLCloneBRSLineItemHandler();
		}else if (Program == 'VS')
		{ 
			return new EFLCloneVSLineItemHandler();
		}else if (Program == 'AC')
		{
			return new EFLCloneACLineItemHandler();
		}else if (Program == 'PPQ')
		{
			return new EFLClonePPQLineItemHandler();
		}
        return null;
    }
   
}