@isTest
public class EFLInspectionEngineFactoryTest {
    
    static testMethod void testGetEngine(){
        Domain__c objDom = new Domain__c();
         objDom.Name = 'BRS';
         insert objDom;
         
         Program_Prefix__c	objPre = new Program_Prefix__c();
         objPre.Name= 'BRS Inspection';
         objPre.Inspection_Related__c = true;
         objPre.Program__c = objDom.id;
         insert objPre;
         
    	Inspection__c Ins = new Inspection__c();
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
    	Ins.RecordTypeId = IncRecTypeID;
        Ins.Stage__c = 'Inspection Assignment';
        Ins.status__c='Cancelled';
        Ins.Reason_for_Cancellation__c = 'testing';
         system.debug('val'+Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId());
		insert Ins;
        
        EFLInspectionEngineFactory.getEngine(Ins);
    }

}