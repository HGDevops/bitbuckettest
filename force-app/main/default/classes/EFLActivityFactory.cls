public with sharing class EFLActivityFactory {

	public static void loadActivities(SObject parentObject, list<sObject> activityList) 
    {
      
         EFLIActivityInterface Handler;
       
         handler = getHandler(parentObject);
         
        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
		if (handler != null)
		{
			// Execute the handler w.r.t. parent object
			execute(handler,parentObject,activityList);
		}        
    }    
    
    /**
	 * private static method to control the execution of the handler
	 **/
    private static void execute(EFLIActivityInterface handler, SObject parentObject, list<SObject> activityList)
    {
        
       // After Trigger
		if (Trigger.isBefore && Trigger.isUpdate)
		{            
            handler.populateActivityRecords(parentObject, activityList);
        }
         
    }
    
    private static EFLIActivityInterface getHandler(SObject obj)
    {
        Schema.sObjectType soType = obj.getSObjectType();
        
		if (soType == Authorizations__c.sObjectType)
		{
			return new EFLAuthorizationActivityHandler(); 
		}       
        else if (soType == Inspection__c.sObjectType)
		{
			return new EFLInspectionActivityHandler();
		} 
        else if (soType == Incident__c.sObjectType)
        {
            return new EFLIncidentActivityHandler();
        }
       /* else if (soType == Task.sObjectType)
		{
			return new EFLTaskActivityHandler();
		} */       
        return null;  
    }
}