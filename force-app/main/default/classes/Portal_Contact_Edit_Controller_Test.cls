@isTest
private class Portal_Contact_Edit_Controller_Test
{
  static testMethod void Portal_Contact_Edit_Controller_Test() {

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
           String AccountRecordTypeId = testData.AccountRecordTypeId; 
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);
          Applicant_Contact__c objacc = testData.newappcontact();
          
          User usershare = new User();
          usershare = EFLUserTestDataFactory.getUser('eFile Applicant');   

              ApexPages.StandardController stdControllerNo = new ApexPages.StandardController(objcont);
              Portal_Contact_Edit_Controller objAssNo = new Portal_Contact_Edit_Controller(stdControllerNo);
              objAssNo.cont = objcont;
      		  //objAssNo.Save();

              ApexPages.currentPage().getParameters().put('id',objcont.id);                                                                                                             
              ApexPages.StandardController stdController = new ApexPages.StandardController(objcont);
              Portal_Contact_Edit_Controller objAss = new Portal_Contact_Edit_Controller(stdController);
              objAss.Save();
              objAss.getEditPage();
              objAss.controller = new ApexPages.StandardController(objcont);
              System.assert(objAss != null);        
  
  }
}