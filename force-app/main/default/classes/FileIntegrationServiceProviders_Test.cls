@isTest
public class FileIntegrationServiceProviders_Test {

    @isTest
    public static void getDefaultValue_OneValueReturned(){
        FileIntegrationServiceProviders fisp = new FileIntegrationServiceProviders();
        
        VisualEditor.DataRow dataRow = fisp.getDefaultValue();
        
        System.assertEquals('-none-', dataRow.getLabel());
        System.assertEquals('', dataRow.getValue());
    }
    
    @isTest
    public static void getValues_TwoValuesReturned(){
        FileIntegrationServiceProviders fisp = new FileIntegrationServiceProviders();
        
        VisualEditor.DynamicPickListRows dataRows = fisp.getValues();
        
        //System.assertEquals(2, dataRows.size());
        System.assertEquals('-none-', dataRows.get(0).getLabel());
        /*System.assertEquals('', dataRows.get(0).getValue());
        System.assertEquals('SpringCMTest', dataRows.get(1).getLabel());
        System.assertEquals('SpringCMTest', dataRows.get(1).getValue());*/
    }
    
}