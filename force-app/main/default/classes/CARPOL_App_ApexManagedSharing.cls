public without sharing class CARPOL_App_ApexManagedSharing {
    
    public static Boolean addAppSharing(Id publicGroupId, List<Application__c> applications){
       
        List<sObject> shares = new List<sObject>();
        
        try{
            
                for(Application__c app: applications){
                    sObject dynObject= Schema.getGlobalDescribe().get('Application__Share').newSObject();
                    dynObject.put(Schema.Application__Share.ParentId, app.id);
                    dynObject.put(Schema.Application__Share.UserOrGroupId, publicGroupId);
                    dynObject.put(Schema.Application__Share.AccessLevel, 'Edit');
                    dynObject.put(Schema.Application__Share.RowCause, Schema.Application__Share.RowCause.PartnerContact__c);
                    shares.add(dynObject);
                    
               
            }
            
            
        }
        catch(Exception e){
            System.debug('Applications >>> ' + e.getMessage());
        }
        
        if(shares.size() == 0)
            return false;
        else{
            insert shares;
            system.debug('Succesfully Inserted');
            return true;
        }
        
    }
    
    /*
    List<sObject> Lineshares = new List<sObject>();
    List<AC__c> Lines= [SELECT ID,Name FROM AC__c WHERE Application_Number__c IN:ApplicationIDs]; 
    if(Lines.size()!=0){
       for(AC__c Line: Lines){
                sObject dynObject= Schema.getGlobalDescribe().get('AC__Share').newSObject();
                dynObject.put(Schema.AC__Share.ParentId, Line.id);
                dynObject.put(Schema.AC__Share.UserOrGroupId, publicGroupId);
                dynObject.put(Schema.AC__Share.AccessLevel, 'Edit');
                dynObject.put(Schema.AC__Share.RowCause, Schema.AC__Share.RowCause.PartnerContact__c);
                shares.add(dynObject);
            }
    }
    
    */
    

}