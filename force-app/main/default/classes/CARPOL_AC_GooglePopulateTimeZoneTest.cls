@isTest(seealldata=false)
    public class CARPOL_AC_GooglePopulateTimeZoneTest {
    
    static Facility__c objFacil1;
    static Facility__c objFacil2;
    static List<Id> facilIDList;
    
    public static void init() { 
        CARPOL_AC_TestDataManager testData=new  CARPOL_AC_TestDataManager();
                                 
        objFacil1 = testData.newFacility('test');
        objFacil2 = testData.newFacility('test');
        facilIDList = new List<Id>{objFacil1.id,objFacil2.id};
        testData.insertcustomsettings();
            
    }
    
    public static testmethod void test1(){
        init();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        CARPOL_AC_GooglePopulateTimeZone.CARPOL_AC_CallGoogleGeoAddress(facilIDList);        
        CARPOL_AC_GooglePopulateTimeZone.GeoResult gr = new CARPOL_AC_GooglePopulateTimeZone.GeoResult();
        gr.toDisplayString();
    }

    public static testmethod void test2(){
        init();
        CARPOL_AC_GooglePopulateTimeZone.CARPOL_AC_CallGoogleGeoAddress(facilIDList);
    }
        
    public class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"foo":"bar"}');
        res.setStatusCode(200);
        return res;
    }
}        
}