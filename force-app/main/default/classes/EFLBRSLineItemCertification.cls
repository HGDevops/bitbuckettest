public with sharing class EFLBRSLineItemCertification {
    
    public Id lineItemID{
        get{
            lineItemID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
            return lineItemID;
        }set;
    }
    
    public AC__c lineItemRecord{            
        get{
            if(lineItemRecord==null){
                lineItemRecord = EFLLineItemRepository.selectbyID(LineItemId);
            }
            return lineItemRecord;
        }
        set;
    }
    
    public PageReference submitLineItem() {
        AC__c lineItemCertified = new AC__c();
        lineItemCertified.id = lineItemID;
        lineItemCertified.Certification__c = 'Agree';
        update lineItemCertified;
        PageReference applicationReview = new PageReference('/apex/portal_application_detail?id='+lineItemRecord.Application_Number__c);
        applicationReview.setRedirect(true);
        return applicationReview;
    }
    
}