public class EFLApplicationXMLController {

    public EFLApplicationXMLController(){
        
              
    }
     public string applicationtype {get;set;}
     public blob xmlbody { get; set; }
     public  PageReference uploadFile()
     {
         system.debug('Inside UploadFile');
         system.debug('xmlbody@@'+ xmlbody);
         if(xmlbody !=null)
         {
             system.debug('applicationtype@@'+applicationtype);
             if(applicationtype == null)
             {
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,' Please select if your application is for a Permit or a Notification'));
                 return null;
             }
			return null;             
         }
         else
         {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,' Please select an XML file to validate'));
            return null;
         }
     }
}