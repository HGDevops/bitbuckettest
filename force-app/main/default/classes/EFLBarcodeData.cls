public class EFLBarcodeData {
    
    public string inputData {get;set;}
    public decimal maxWidth {get;set;}
    public decimal maxHeight {get;set;}
    public List<EFLBarcodeDigit> bars;
    public string barcodeHTML {get;set;}
    public List<integer> encodedData;
       
    public EFLBarcodeData()
    {
        bars = new List<EFLBarcodeDigit>();
        encodedData = new List<integer>();
    }

}