public with sharing class csstest_control{
    
    public csstest_control() {
        
    }
    
    public string chatterPageString{get;set;}
    public Id applicationID{get;set;}
    public string Renewal{get;set;}
    public List<AC__c> AC;
    public string lineItemIdToBeDeleted{get;set;}
    public string lineItemIdToBeWithdrawn{get;set;}
    public Boolean isAgree{get;set;}
    public Boolean disableButton{get;set;}
    public Boolean CVBflag{get; set;}
    public Boolean NotCVBflag{get; set;}
    public Id selectedAuthid{get; set;}
    public Id selectedAuthidother{get; set;}
    //  public list<AC__c> lineitems{get;set;}
    public boolean allowAppWithdraw{get;set;}
    public string parentlineitemid {get;set;}
    public string regArticlePackageId {get;set;}
    public List<wraplnlst> wrlnlst=new List<wraplnlst>();
    public set<Id> newlnid=new set<Id>();
    public Boolean showCloneBtn{get; set;}
    public Boolean showEditBtn{get; set;} //KA-W-025627
    public boolean validateBRSLineitem;
    Public boolean lineItemsCerified {get; set;}
    public boolean permitIssued{get; set;}
    //AC LiveDog LineItem RecordtypeId
    Id acLiveDogRecordtypeId = EFLGenericUtility.getRecordTypeId('AC Line Item Live Dogs');
    public boolean isACLiveDogApp {get; set;}
    public boolean isACLiveDogAndReadyToSubmit {get; set;}
    Public user u = [select id,name,usertype from user where id =: UserInfo.getUserId()];
    public Double offset{
        get{
            TimeZone tz = UserInfo.getTimeZone();
            //Milliseconds to Day
            return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
        }
    }
    
    public string lineitemhelptext{
        get{
            lineitemhelptext =   [select Text__c from EFLTextConfiguration__mdt 
                                  where Program__c ='BRS' and Functional_Area__c='Application' and 
                                  Section__c='Certify Statement' and User_Type__c='Applicant'].Text__c;
            return lineitemhelptext;
        }
        set;}
    
    public csstest_control(ApexPages.StandardController controller){        
        applicationID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
        chatterPageString = 'chatterpage?id='+applicationID;
                
        if(String.isNotBlank(apexpages.currentpage().getparameters().get('regArticlePackageId'))){
            regArticlePackageId = EFLGenericUtility.sanitizeString(apexpages.currentpage().getparameters().get('regArticlePackageId'));
        }
        if(String.isNotBlank(apexpages.currentpage().getparameters().get('parentlineitemid'))){
            parentlineitemid = EFLGenericUtility.sanitizeString(apexpages.currentpage().getparameters().get('parentlineitemid'));
        }
        
        // Start RP 
        validateBRSLineitem = false;
        permitIssued = false;
        isAgree=false;
        disableButton=true;
        allowAppWithdraw=false;
        Renewal = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('Renewal'));
        List<Application__c> lst=[SELECT Id,Applicant_Name__r.name,Renewal_Application__c,Renewal_Proposed_Start_Date__c,Renewal_Proposed_End_Date__c,Agreed_and_certified__c,Application_Status__c FROM Application__c WHERE ID=:applicationID];
        if(lst.size()>0){
            for(Application__c a:lst){
              if(a.Renewal_Application__c==true){
                Renewal='Renewal';
              }
              //KA:W-025627
                if(a.Applicant_Name__r.name == u.name || u.userType == 'PowerCustomerSuccess' ){
               showEditBtn = true; 
              }
        }
        }
        
        List<authorizations__c> appauth=[SELECT id,status__c,application__r.application_status__c,
                                         Authorization_Type__c, Program__c 
                                         FROM authorizations__c 
                                         WHERE Application__c=:applicationID];//1-30-17 VV added code to show Withdraw app btn only when no auth under app is issued.
        if(!appauth.isempty()){
            for(authorizations__c aa:appauth){
                if(aa.Application__r.Application_Status__c=='Submitted'&&aa.status__c=='Issued')allowAppWithdraw=false;
                else allowAppWithdraw=True;
                if(aa.Application__r.Application_Status__c=='Withdrawn')allowAppWithdraw=false;
                if(aa.Authorization_Type__c == 'Letter Of Denial')allowAppWithdraw=false;
                if(aa.status__c=='Issued')permitIssued = true;
            }
        }
        //else{// if there are no auths, you can withdraw app
        //     allowAppWithdraw=true;
            //showCloneBtn=false;  Ravee Racharla 12/14/2018 W-026413
        //}
        if(lst.size()>0){
        if(lst[0].application_status__c=='Withdrawn'){
            allowAppWithdraw=false;
        }}
        // showCloneBtn=true;
        if(appauth.size() > 0) {
            //  showCloneBtn = isCloneBtnAllowed(appAuth);  Ravee Racharla 12/14/2018 W-026413
        }
        
    }
    public List<Attachment> getattach(){
        return [SELECT Name,Description,ContentType,CreatedById,CreatedBy.Name,
                CreatedDate,LastModifiedByID,LastModifiedBy.Name,Id,ParentId FROM Attachment WHERE ParentId=:ApexPages.currentPage().getParameters().get('ID') order by CreatedDate Desc];
    }
    public List<AC__c> getAC(){
        lineItemsCerified = true;
        isACLiveDogAndReadyToSubmit = true;
        isACLiveDogApp = true;
        AC=new List<AC__c>();
        AC= [SELECT ID,Name,Certification__c,Regulated_Article_Status__c,Construct_Status__c,Location_Status__c,SOP_Status__c,
             Purpose_of_the_Importation__c,Authorization__r.Status__c, EFL_CVB_Product_Name__c ,Application_Number__c,Application_Number__r.Application_Status__c,
             Status__c,Color__c,RecordTypeID, Sex__c,Domain__r.name,Domain__c,Authorization__c,Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.name, 
             program_Line_Item_Pathway__r.Program__c,Program_Line_Item_Pathway__r.Program__r.name,Thumbprint__c,Scientific_name__c,Scientific_name__r.name,
             RA_Scientific_name__r.Name, Application_Details_Status__c, Related_Contact_Status__c, Document_Status__c, OwnerId,
             (SELECT Regulated_Article__c,Regulated_Article__r.name,Scientific_Name__c FROM Link_Regulated_Articles__r) 
             FROM AC__c WHERE Application_Number__c=:applicationID AND (Line_Item_Type_Hidden_Flag__c='product' OR Line_Item_Type_Hidden_Flag__c='')];
        for(AC__c a:AC){  // VV added 5-5-17 to pass flags to show different certify statements
            if(a.Program_Line_Item_Pathway__r.Name == 'Veterinary Biological Products'){
                CVBflag=true;
            }else{       
                NotCVBflag=true;
                showCloneBtn=true;
                if (a.Status__c=='Draft'){
                    showCloneBtn=false;
                }
            }
            
           
            if(a.Certification__c != 'Agree')
            {
                lineItemsCerified = false;
            }
            if(a.Regulated_Article_Status__c != 'Ready to Submit' || a.Construct_Status__c != 'Ready to Submit' || a.Location_Status__c != 'Ready to Submit' || a.SOP_Status__c != 'Ready to Submit')
            {
                lineItemsCerified = false;
            }
            
            //AC Live Dogs
            if(a.recordtypeId == acLiveDogRecordtypeId)
            {
                if(a.Application_Details_Status__c != CARPOL_Constants.READY_TO_SUBMIT || a.Related_Contact_Status__c != CARPOL_Constants.READY_TO_SUBMIT || a.Regulated_Article_Status__c != CARPOL_Constants.READY_TO_SUBMIT || a.Document_Status__c != CARPOL_Constants.READY_TO_SUBMIT)
                {
                    isACLiveDogAndReadyToSubmit = false;
                }
            }
            else
            {
                isACLiveDogApp = false;
                isACLiveDogAndReadyToSubmit = false;
            }
            
        } 
        
        return AC;
    }
    
    // Adding Action Items of BRS line items 
    public PageReference ActionItems(){
        PageReference actionitempg = new PageReference('/apex/CARPOL_BRS_WaitingPage');
        list<AC__c> lineitemlist= [select id,name,Application_Number__c,Program_Line_Item_Pathway__c from AC__C where Application_Number__c=:applicationID];
        //system.debug('!!!!!!!!lineitemlist'+lineitemlist);
        for(AC__c a:lineitemlist ){
            actionitempg.getParameters().put('Id',a.Id);
            actionitempg.getParameters().put('appId',a.Application_Number__c);
            actionitempg.getParameters().put('strPathwayId',a.Program_Line_Item_Pathway__c);
        }
        
        //system.debug('!!!!!!actionitempg'+actionitempg);
        actionitempg.setRedirect(true);
        
        return actionitempg;
    }
    
    // Adding Action Items of BRS line items 
    /*public PageReference BRSItemDetails(){
PageReference actionitempg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');
list<AC__c> lineitemlist= [select id,name,Application_Number__c,Program_Line_Item_Pathway__c from AC__C where Application_Number__c=:applicationID];
for(AC__c a:lineitemlist ){
actionitempg.getParameters().put('LineId',a.Id);
actionitempg.getParameters().put('appId',a.Application_Number__c);
actionitempg.getParameters().put('strPathwayId',a.Program_Line_Item_Pathway__c);
}

actionitempg.setRedirect(true);

return actionitempg;
}   */
    
    public PageReference BRSItemDetails(){
        PageReference actionitempg = new PageReference('/apex/EFLLineItem');
        list<AC__c> lineitemlist= [select id,name,Application_Number__c,Program_Line_Item_Pathway__c from AC__C where Application_Number__c=:applicationID];
        for(AC__c a:lineitemlist ){
            actionitempg.getParameters().put('Id',a.Id);
            //actionitempg.getParameters().put('appId',a.Application_Number__c);
            //actionitempg.getParameters().put('strPathwayId',a.Program_Line_Item_Pathway__c);
        }
        
        actionitempg.setRedirect(true);
        
        return actionitempg;
    }
    
    
    public List<AC__c> getProductIngredients(){
        return [SELECT ID,Name,Purpose_of_the_Importation__c,Status__c,Color__c,Sex__c,Authorization__c,Thumbprint__c,Scientific_name__c,Scientific_name__r.name,Regulated_Article_PackageId__c,Regulated_Article_PackageId__r.Product_Name__c,RA_Scientific_name__r.Name FROM AC__c WHERE Application_Number__c=:applicationID AND Line_Item_Type_Hidden_Flag__c='Ingredient'];
    }
    public List<Authorizations__c> getAuth(){
        return [SELECT ID,Name,Authorization_Type__c,Status__c,Fee_Amount__c FROM Authorizations__c WHERE Application__c=:applicationID];
    }
    //Receipt
    public List<Transaction__c> getTrx(){
        return [SELECT ID,Name,Pay_Gov_Token__c,Pay_gov_Tracking_ID__c,Payment_Type__c,Transaction_Amount__c,Transaction_Date_Time__c FROM Transaction__c WHERE Application__c=:applicationID];
    }
    public List<Change_History__c> gethistory(){
        return [SELECT CreatedDate,CreatedBy.Name,CreatedById,Id,Name,New_Value__c,Old_Value__c FROM Change_History__c WHERE Application__c=:applicationID];
    }
    //VV 10/11/15
    /*  public PageReference editAssocContact() {
String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Associated_Contact_Create?id='+applicationID);
dirpage.setRedirect(true);
return dirpage;

}*/
    //Added by Subbu -3/23
    public PageReference deleteLineItem(){
        //system.debug('Line item to be deleted '+lineItemIdToBeDeleted);
        if(lineItemIdToBeDeleted!=null&&lineItemIdToBeDeleted!=''){
            AC__c lineItemObj=new  AC__c();
            lineItemObj.Id=lineItemIdToBeDeleted;
            Delete lineItemObj;
        }
        PageReference dirpage=new  PageReference('/'+applicationID);
        dirpage.setRedirect(true);
        return dirpage;
    }
    
    
    // VV added for line item withdraw functionality
    public Pagereference withdrawApplication()
    {
        lineItemIdToBeWithdrawn = AC[0].id;
        if(lineItemIdToBeWithdrawn!=null&&lineItemIdToBeWithdrawn!=''){
            AC__c lineItemObj = new AC__c();
            lineItemObj=[SELECT id,Application_Number__c,Status__c FROM AC__c WHERE id=:lineItemIdToBeWithdrawn];//new AC__c();
            lineItemObj.status__c='Withdrawn';
            string authId=[SELECT Authorization__c FROM AC__c WHERE id=:lineItemIdToBeWithdrawn].Authorization__c;
            list<Authorizations__c> listAuth=new  list<Authorizations__c>();
            for(Authorizations__c a:[SELECT id,Status__c FROM Authorizations__c WHERE id=:authId])listAuth.add(a);
            integer reviewCount=[SELECT count() FROM Reviewer__c WHERE Authorization__c=:authId];
            if(reviewCount==0 && (!listAuth.isEmpty()&&listAuth[0].status__c!='Issued')){
                system.debug('1++'+listAuth[0].status__c);
                system.debug('test++');
                Application__c applicationObj = new Application__c(id=lineItemObj.Application_Number__c, Application_Status__c = 'Withdrawn');
                listAuth[0].status__c='Withdrawn'; 
                Update listAuth;
                Update lineItemObj;                
                Update applicationObj;                
            }
            else if(listAuth.isEmpty())
            {
                Update lineItemObj;
            }
            else{
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Error,+'Withdraw on this line item is not allowed as either Authorization is Issued or a State package has been created'));
                return null;
            }
        }
        return null;
    }
    public PageReference withdrawLineItem(){
        if(lineItemIdToBeWithdrawn!=null&&lineItemIdToBeWithdrawn!=''){
            AC__c lineItemObj = new AC__c();
            system.debug('lineItemIdToBeWithdrawn++'+lineItemIdToBeWithdrawn);
            lineItemObj=[SELECT id,Application_Number__c,Status__c FROM AC__c WHERE id=:lineItemIdToBeWithdrawn];//new AC__c();
            lineItemObj.status__c='Withdrawn';
            string authId=[SELECT Authorization__c FROM AC__c WHERE id=:lineItemIdToBeWithdrawn].Authorization__c;
            list<Authorizations__c> listAuth=new  list<Authorizations__c>();
            System.debug('authId++'+authId);
            for(Authorizations__c a:[SELECT id,Status__c FROM Authorizations__c WHERE id=:authId])listAuth.add(a);
            integer reviewCount=[SELECT count() FROM Reviewer__c WHERE Authorization__c=:authId];
            System.debug('reviewCount++'+reviewCount);
         //   System.debug('listAuth[0].status__c++'+listAuth[0].status__c);
            system.debug('listAuth[0]++'+listAuth);
            system.debug('listAuth.isEmpty()++'+listAuth.isEmpty());
            if(reviewCount==0&&(!listAuth.isEmpty()&&listAuth[0].status__c!='Issued')){
                Application__c applicationObj = new Application__c(id=lineItemObj.Application_Number__c, Application_Status__c = 'Withdrawn');
                listAuth[0].status__c='Withdrawn'; 
                Update listAuth;
                Update lineItemObj;                
                Update applicationObj;
                PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
                pageRef.setRedirect(true);
                return pageRef;
            }
            else if(listAuth.isEmpty())
            {
                Update lineItemObj;
            }
            else{
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Error,+'Withdraw on this line item is not allowed as either Authorization is Issued or a State package has been created'));
                return null;
            }
        }
        /*  PageReference dirpage= new PageReference('/' + applicationID);
dirpage.setRedirect(true); */
        return null;
    }
    public PageReference withdrawAuth(){
        if(lineItemIdToBeWithdrawn!=null&&lineItemIdToBeWithdrawn!=''){
            AC__c lineItemObj = new AC__c();
            lineItemObj=[SELECT id,Application_Number__c,Status__c FROM AC__c WHERE id=:lineItemIdToBeWithdrawn];//new AC__c();
            lineItemObj.status__c='Withdrawn';
            string authId=[SELECT Authorization__c FROM AC__c WHERE id=:lineItemIdToBeWithdrawn].Authorization__c;
            list<Authorizations__c> listAuth=new  list<Authorizations__c>();
            for(Authorizations__c a:[SELECT id,Status__c FROM Authorizations__c WHERE id=:authId])listAuth.add(a);
            integer reviewCount=[SELECT count() FROM Reviewer__c WHERE Authorization__c=:authId];
            if(reviewCount==0 &&(!listAuth.isEmpty()&&listAuth[0].status__c!='Issued')){
                Application__c applicationObj = new Application__c(id=lineItemObj.Application_Number__c, Application_Status__c = 'Withdrawn');
                listAuth[0].status__c='Withdrawn'; 
                Update listAuth;
                Update lineItemObj;                
                Update applicationObj;
                PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
                pageRef.setRedirect(true);
                return pageRef;
            }
            else if(listAuth.isEmpty())
            {
                Update lineItemObj;
            }
        }
        return null;
    }
    //END VV 10/11/15
    public PageReference dir(){
        PageReference dirpage=new  PageReference('/'+applicationID);
        dirpage.setRedirect(true);
        return dirpage;
    }
    public PageReference redirectPage(){//Vijay added 1-10-16
        return null;
    }
    public pagereference sendEmailToApplicant(){
        //system.debug('sendEmailToApplicant  ' + applicationID);
        if (applicationID==null || applicationID ==''){
            applicationID=ApexPages.currentPage().getParameters().get('id');
        }
        application__C app = [select id,Send_Mail_To_Applicant__c from application__C where id= :applicationid];
        
        app.Send_Mail_To_Applicant__c  = True; 
        update app; 
        //system.debug('sendEmailToApplicant  ' + app);
        return Null; 
    }
    
    public boolean displayPopup {get; set;}    
    
    public void closePopup()
    {       
        displayPopup = false;   
    }
    // Auth withdraw functionality    
    public Pagereference showPopup()
    {    
        
        if(selectedAuthid == null ){
            selectedAuthidother=ApexPages.currentpage().getParameters().get('said'); 
        }  
        set<Id> lnid=new set<Id>();
        if(selectedAuthid != null){
            Authorizations__c authwithdraw= [SELECT ID,Name,Authorization_Type__c,Status__c,Fee_Amount__c,(select Id,Name,Authorization__c,Status__c FROM Animal_Care_AC_Authorization__r) FROM Authorizations__c WHERE Id=:selectedAuthid limit 1];
            
            if(authwithdraw.Animal_Care_AC_Authorization__r.size() > 0){
                for(AC__c ln:authwithdraw.Animal_Care_AC_Authorization__r){
                    if(ln.Id != null && ln.Status__c != 'Withdrawn'){
                        lnid.add(ln.Id);
                    }
                }       
            }
            //if there is only one line item for an auth, set line item, Auth status to withdraw and  set the WF tasks to Deferred
            if(lnid.size() > 0 && lnid != null && lnid.size() == 1){
                authwithdraw.Status__c='Withdrawn';
                update authwithdraw;
                List<AC__c> WthdrAuthln = [select Id, name, status__c from AC__c where Id = :lnid];
                for(AC__C l:wthdrAuthln){
                    l.Status__c ='Withdrawn';
                }
                List<Workflow_Task__c> WthdrAuthWft = [select Id, name, status__c from Workflow_Task__c where Authorization__c = :selectedAuthid];
                for(Workflow_Task__c wt:WthdrAuthWft){
                    wt.Status__c = 'Deferred';
                    
                }
                if(WthdrAuthln != null){
                    update WthdrAuthln;
                }
                if(WthdrAuthWft != null){
                    update WthdrAuthWft;
                }
                Pagereference pff2=new Pagereference('/apex/Portal_Authorization_Detail2?id='+selectedAuthid);
                pff2.setredirect(true);
                return pff2;
            }
            
            if(lnid.size()>1){
                Pagereference pf=new Pagereference('/apex/EFLSelectLinetoWithdraw?said='+selectedAuthid);  
                pf.setredirect(true);
                return pf;
                
            }
            
        }
        
        if(selectedAuthidother != null){
            
            Authorizations__c authwithdraw= [SELECT ID,Name,Authorization_Type__c,Status__c,Fee_Amount__c,(select Id,Name,Authorization__c,Status__c FROM Animal_Care_AC_Authorization__r) FROM Authorizations__c WHERE Id=:selectedAuthidother limit 1];
            
            if(authwithdraw.Animal_Care_AC_Authorization__r.size() > 0){
                for(AC__c ln:authwithdraw.Animal_Care_AC_Authorization__r){
                    if(ln.Id != null && ln.status__c != 'Withdrawn'){
                        lnid.add(ln.Id);
                    }
                }       
            }
            
            List<AC__c> WthdrAuthln2 = [select Id, name, status__c,Intended_Use__r.name,Regulated_Article__r.name,Country_Of_Origin__r.name from AC__c where Id = :lnid];
            for(AC__c wtdln:WthdrAuthln2){
                wrlnlst.add(new wraplnlst(false,wtdln));
            }
            
            
        }
        return null;
    }  
    // if there are more than one line item for an auth, ask applicant to select which lines to withdraw
    // if applicant selects all the lines ==> set line items, Auth status to withdraw and  set the WF tasks to Deferred
    // if applicant selects some lines ==> set those line items to Withdrawn
    // if applicant selects none ==>throw error
    public Pagereference withdrawmethod(){
        
        
        for(wraplnlst wline:wrlnlst){
            if(wline.checkselect==true){
                newlnid.add(wline.lnrec.Id);
            }
        }
        if(newlnid == null || newlnid.size() == 0){
            Apexpages.addmessage(new apexpages.message(apexpages.severity.Error,'Please select at least one Line Item'));
            return null;
        }
        
        if(newlnid != null){
            List<AC__c> WthdrAuthln = [select Id, name, status__c from AC__c where Id = :newlnid];
            for(AC__C l:wthdrAuthln){
                l.Status__c ='Withdrawn';
            }
            
            if(WthdrAuthln != null){
                update WthdrAuthln;
            }
            // Show withdraw btn only when Auth status <> Withdrawn
            boolean wfhfunenable=false;
            boolean withdrawstatus=false;
            Authorizations__c authwithdraw= [SELECT ID,Name,(select Id,Authorization__c,Status__c FROM Animal_Care_AC_Authorization__r) FROM Authorizations__c WHERE Id=:selectedAuthidother limit 1];
            if(authwithdraw.Animal_Care_AC_Authorization__r.size() > 0){
                for(AC__c lnstatus:authwithdraw.Animal_Care_AC_Authorization__r){
                    if(lnstatus.Status__c == 'Withdrawn'){
                        wfhfunenable=true;
                    }
                    else{
                        withdrawstatus=true;
                    }
                }       
            }
            if(wfhfunenable == true && withdrawstatus==false){
                authwithdraw.Status__c='Withdrawn';
                update authwithdraw; 
                List<Workflow_Task__c> WthdrAuthWft = [select Id, name, status__c from Workflow_Task__c where Authorization__c = :selectedAuthidother];
                for(Workflow_Task__c wt:WthdrAuthWft){
                    wt.Status__c = 'Deferred';
                }
                if(WthdrAuthWft != null){
                    update WthdrAuthWft;
                }
            }
        }
        // once completed send applicant to the auth portal page
        Pagereference pff2=new Pagereference('/apex/Portal_Authorization_Detail2?id='+selectedAuthidother);
        pff2.setredirect(true);
        return pff2;
    }
    
    @TestVisible public class wraplnlst{
        public Boolean checkselect{get; set;}
        public AC__C lnrec{get; set;}
        public wraplnlst(boolean bc,AC__C l){
            checkselect=bc;
            lnrec=l;
        }
    }
    
    // Check if there is any Auth associated with Application with Auth Status = 'Issued'
    // Return true if there is atleast one no match
    @TestVisible private boolean isCloneBtnAllowed(List<Authorizations__c> authList){
        //Boolean flag = false;
        EFLCloneBtn__mdt [] cloneData = [SELECT label, AuthStatus__c FROM EFLCloneBtn__mdt];
        
        for(Authorizations__c a: authList){
            for(EFLCloneBtn__mdt cdata: cloneData){                                
                if(a.Program__c == cdata.label && a.Status__c != null) {
                    if (cdata.AuthStatus__c.containsIgnoreCase(a.Status__c) == true) {
                        return true;
                    }else {
                        return false;
                    } 
                }
            }
        }                
        return true;
    }  
    //KA::W-026020:: Delete Application in open status 
    public without sharing class AdminContext{
        public boolean deleteApplicationAsAdmin(string appID){
            if (String.isNotBlank(appID)){
                Application__c a = new Application__c(id = appID);
                delete a;            
            }
            return true;
        }
        
    }
    
    @RemoteAction 
    public static boolean  deleteApplication(string appID){
        
        return new AdminContext().deleteApplicationAsAdmin(appID);
    } 
    
   
}