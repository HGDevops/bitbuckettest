@IsTest
private with sharing class Carpol_UNI_UfsWSDLTest
{
    private static testMethod void testAll()
    {
        new Carpol_UNI_UfsWSDL.receiveTransactionFault_element();
        new Carpol_UNI_UfsWSDL.Payment_element();
        new Carpol_UNI_UfsWSDL.receiveTransactionResponse_element();
        new Carpol_UNI_UfsWSDL.Receipt_element();
        new Carpol_UNI_UfsWSDL.Message_element();
        new Carpol_UNI_UfsWSDL.Charge_element();
        new Carpol_UNI_UfsWSDL.ReceiveUFSTransactionHttpSoap11Endpoint();
    
    }
    
    
    private class WebServiceMockImpl implements WebServiceMock
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
            {
                if(request instanceof Carpol_UNI_UfsWSDL.Message_element)
                response.put('response_x', new Carpol_UNI_UfsWSDL.receiveTransactionResponse_element());
                return;
            }
    }       
      private static testMethod void coverMethods()
    {
        
        Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        new Carpol_UNI_UfsWSDL.ReceiveUFSTransactionHttpSoap11Endpoint().receiveTransaction(null, null, null);
    }
}