/**
* Author : Kishore Kumar
* Created Date : 4/13/2015
* Purpose : This is used to edit State Review Records
* Related Pages: CARPOL_BRS_StateReviewRecords  
*/
public with sharing class CARPOL_BRS_StateReviewRecords{

    public Id ReviewerId{get;set;}
    public list<Reviewer__c> Rev {get;set;}
    public boolean showInstructions {get;set;}
    public boolean showFSIS{get;set;}
    public id authorizationid{get;set;}
    public Reviewer__c obj{get;set;}
    boolean stateRequirementsExist;

    public CARPOL_BRS_StateReviewRecords(ApexPages.StandardController controller) {

      ReviewerId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
      obj=(Reviewer__c)controller.getRecord();
      String FSISrecordtypeid = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('FSIS Review Record').getRecordTypeId();
      showFSIS = false;
      stateRequirementsExist = false;
      Rev = [SELECT Authorization__c,Id, 
                    Authorization__r.name,Requirement_Desc__c, 
                    Authorization__r.High_Priority_flag__c,State_has_no_comments__c,
                    Authorization__r.Total_Regulations__c,No_Requirements__c,
                    Authorization__r.Total_Applicant_Agreed__c,Type_of_application__c,
                    Authorization__r.Remaining_Applicant_Agreement_Needed__c,
                    Authorization__r.Status_Graphic__c,
                    Authorization__r.Reason_to_Expedite__c,
                    recordtypeid 
               FROM Reviewer__c 
              where Id =:ReviewerId] ;      
       authorizationid = Rev[0].Authorization__c;
      if(Rev[0].Authorization__r.High_Priority_flag__c == true)
           showInstructions = true;  
           
      if(rev[0].recordtypeid == FSISrecordtypeid) {
         showFSIS = true; 
      }    
     getRelatedConditions();   
    }
/** 
 * List all Attachments 
**/ 
    public List<Attachment> getattach(){
 
       return [SELECT   Name, Description, ContentType, CreatedById, Id, ParentId FROM Attachment WHERE ParentId = : ReviewerId];
    }
/** 
 * List Chatter Attachments 
**/     
    public List<ContentDocumentLink> getChatterattach(){
        
        return [SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId, ContentDocument.Title, ContentDocument.CreatedById, ContentDocument.LastModifiedDate FROM ContentDocumentLink WHERE LinkedEntityId =: ReviewerId];
        
    }
    
/** 
 * List applicant Attachments 
**/    
     public List<Applicant_Attachments__c> getAppAttach(){
            
       return [SELECT Attachment_Type__c,Document_Types__c,Authorization__c,Id,Name FROM Applicant_Attachments__c WHERE Authorization__c = : Rev[0].Authorization__c and Attachment_Type__c='State Package' ];
    }
    
    /*public PageReference Save()
    {
       // upsert acct;
        PageReference pg= new PageReference ('/'+ReviewerId);
        pg.setRedirect(true);
        return pg;
        
    }*/
    
    public PageReference getEditPage()
    {
     /*   //String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
       // system.debug('+++BaseUrl+++'+BaseUrl);
      //  PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Account_Edit?id='+accountID);
        PageReference dirpage= new PageReference('/apex/CARPOL_BRS_StateReviewEditRecords?id='+ReviewerId);
        dirpage.setRedirect(true);
        return dirpage;*/  
        return null;
    }  
/** 
 * Submit Responses
**/     
    public PageReference SubmitReponses() { 
        //Added Logic to update values of State comments,State has no reqmt,State has no comments when submit button is clicked W-017419-NU
            reviewerId= EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
            obj.id=reviewerId;
            Update obj;
         list<Reviewer__c> revList = [select id,Status__c from Reviewer__c where Id =: ReviewerId];
         revList[0].Status__c = 'Completed';
         update revList;
         Pagereference pg=new PageReference('/st/');
         pg.setRedirect(true);
         return pg;
    }
/** 
 * Line Items of Authorization
**/ 
    public List<AC__c> getlinesList(){
         if(authorizationid !=null)
           return [SELECT ID,Name,Scientific_Name__r.Name,Regulated_article__r.Name,Status__c,Color__c,Sex__c,Number_of_Labels__c,Signature__c,Program_Line_Item_Pathway__r.Program__r.Name FROM AC__c WHERE Authorization__c=:authorizationid];
        else  return null;
    } 
    
/** 
 * Authorization Related Federal Conditions
**/     
      public List<Authorization_Junction__c> getRelatedConditions(){
         List<Authorization_Junction__c> authRelatedConditions = new List<Authorization_Junction__c>();
         if(authorizationid !=null){
            authRelatedConditions= EFLGetConditions.getAuthorizationRelatedConditions(authorizationId,'Federal Conditions',null);
            return authRelatedConditions;
          }
        else{
           return null; 
        }  
    }    
    
/** 
 * Authorization Related State Requirements
**/     
      public List<Authorization_Junction__c> getRelatedStateRequirements(){
         List<Authorization_Junction__c> authRelatedConditions = new List<Authorization_Junction__c>();
         if(authorizationId !=null){
            authRelatedConditions= EFLGetConditions.getAuthorizationRelatedConditions(authorizationId,'State Conditions',Rev[0].id);
           
            if(authRelatedConditions!=null && !authRelatedConditions.isEmpty()){
                stateRequirementsExist = true;
            }
            return authRelatedConditions;
          }
        else{
           return null; 
        }  
    } 
    
/**
* save method 
**/     
    public PageReference saveAndCongrat(){
          if(obj.No_Requirements__c== true &&  stateRequirementsExist == true){
                  ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'State should not have any Requirements entered, Please empty State Requirements.');
                  ApexPages.addMessage(message); 
                   return null; 
                   }
           if(obj.State_has_no_comments__c== true && (obj.State_Comments__c!= '' && obj.State_Comments__c!= null)){
                  ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'State should not have any comments entered, Please empty State Comments.');
                  ApexPages.addMessage(message); 
                   return null; 
                   }                   
            reviewerId= EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
            obj.id=reviewerId;
            Update obj;
            PageReference congratsPage=Page.CARPOL_BRS_StateReviewRecords;
            congratsPage.getParameters().put('id',Rev[0].id);
            congratsPage.setRedirect(true);
            return congratsPage;
    }
    
}