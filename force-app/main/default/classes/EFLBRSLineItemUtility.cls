public inherited sharing class EFLBRSLineItemUtility {
    /**** Retrieve Construct to Line Items ****/
    public static  List<Article_Supplier_Developer__c> getArticleSuppliers(String fieldApiName, String fieldValue){
        
        String soqlQuery = 'SELECT ID,Name,Alternate_Phone__c,Alternate_Phone_CBI__c,Applicant_Instructions__c,City__c,Country__c,Country__r.name,State__r.name,Country_CBI__c,Country_Text__c,Email__c,'+
            'Email_Address_CBI__c,First_Name__c,Line_Item__c,Organization_Name__c,Phone__c,Phone_CBI__c,Postal_Code__c,Postal_Code_CBI__c,State__c,State_CBI__c,State_Text__c,Status__c, Status_Graphical__c,Street_Address__c'+
            ' FROM Article_Supplier_Developer__c WHERE ' + fieldApiName + ' =:fieldValue' ;
        //system.debug('!!!!!!articleSuppliersoqlQuery'+soqlQuery);
        return Database.query(soqlQuery); 
    }
    
    /**** Retrieve Construct to Line Items ****/
    public static  List<Construct__c> getConstruct(String fieldApiName, String fieldValue){
        
        String soqlQuery = 'SELECT ID,Name,Line_Item__c,Authorization_Type__c,Movement_Type__c,Name__c,Construct_s__c,CreatedDate,Corrections_Required__c,Mode_of_Transformation__c,Mode_of_Transformation_Text__c,Status__c,Status_Graphical__c,Total_No_of_PhenoTypes__c,Link_Regulated_Article__c,Link_Regulated_Article__r.name,' +  
            '(SELECT id,Name,Description__c,Construct_Component__c,Construct_Component_Text__c,Donor__C,Construct_Component_Name__c,Genotype__c,Related_Construct_Record_Number__c,Record_Type_Text__c,Donor_2__c,Donor_3__c,Donor_4__c,Donor_5__c,Donor_List__c,RecordType.Name,recordtype.Id,Construct_Component_Sort_Order__c FROM Genotypes__r order by recordtype.name,Construct_Component_Sort_Order__c ASC,CreatedDate ASC),' + 
            '(SELECT id, Name, Phenotype_Category_Text__c,Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,Construct__r.Id FROM Phenotypes__r) ' +
            'FROM Construct__c WHERE ' + fieldApiName + ' =:fieldValue' ;
        //system.debug('!!!!!!soqlQuery'+soqlQuery);
        return Database.query(soqlQuery); 
    }
    
    /**** BULKIFIED Retrieve Construct to Line Items ****/
    public static  List<Construct__c> getConstruct(String fieldApiName, Set<Id> fieldValue){
        
        String soqlQuery = 'SELECT ID,Name,Line_Item__c,Authorization_Type__c,Movement_Type__c,Name__c,Construct_s__c,CreatedDate,Corrections_Required__c,Mode_of_Transformation__c,Mode_of_Transformation_Text__c,Status__c,Status_Graphical__c,Total_No_of_PhenoTypes__c,Link_Regulated_Article__c,Link_Regulated_Article__r.name,' +  
            '(SELECT id,Name,Description__c,Construct_Component__c,Construct_Component_Text__c,Donor__C,Construct_Component_Name__c,Genotype__c,Related_Construct_Record_Number__c,Record_Type_Text__c,Donor_2__c,Donor_3__c,Donor_4__c,Donor_5__c,Donor_List__c,RecordType.Name,recordtype.Id,Construct_Component_Sort_Order__c FROM Genotypes__r order by recordtype.name, Construct_Component_Sort_Order__c ASC,CreatedDate ASC),' + 
            '(SELECT id, Name, Phenotype_Category_Text__c,Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,Construct__r.Id FROM Phenotypes__r) ' +
            'FROM Construct__c WHERE ' + fieldApiName + ' IN:fieldValue' ;
        //system.debug('!!!!!!soqlQuery'+soqlQuery);
        return Database.query(soqlQuery); 
    }
    
    /**** Retrieve Locations to Line Items ****/    
    public static  List<Location__c> getLocation(Id LineItemId){
        
        return [SELECT ID,Name,Contact_Name1__c, 
                Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                Day_Phone_2__c,Zip__c,Other_Material_Types__c,
                Proposed_Planting__c,Number_of_Acres_Text__c,
                Primary_Contact_Last_Name__c,Contact_Address1__c,
                Primary_State_Text__c,Primary_Country_Text__c,
                Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                Secondary_Country_Text__c,Secondary_Contact_County__c,
                Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                Line_Item__c,Description__c,Applicant_Instructions__c,
                RecordType.Name,Country__r.Name,Level_2_Region__r.name,
                Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,
                Material_Type1__c, Material_Type2__c, Material_Type3__c, Material_Type4__c, Material_Type5__c, Material_Type6__c, 
                Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c
                FROM Location__c 
                WHERE Line_Item__c=:LineItemId 
                order by recordTypeId];
    }
    /**** BULKIFIED Retrieve Locations to Line Items ****/    
    public static  List<Location__c> getLocation(Set<Id> LineItemId){
        
        return [SELECT ID,Name,Contact_Name1__c, 
                Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                Day_Phone_2__c,Zip__c,Other_Material_Types__c,
                Proposed_Planting__c,Number_of_Acres_Text__c,
                Primary_Contact_Last_Name__c,Contact_Address1__c,
                Primary_State_Text__c,Primary_Country_Text__c,
                Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                Secondary_Country_Text__c,Secondary_Contact_County__c,
                Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                Line_Item__c,Description__c,Applicant_Instructions__c,
                RecordType.Name,Country__r.Name,Level_2_Region__r.name,
                Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,
                Material_Type1__c, Material_Type2__c, Material_Type3__c, Material_Type4__c, Material_Type5__c, Material_Type6__c, 
                Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c
                FROM Location__c 
                WHERE Line_Item__c IN :LineItemId 
                order by recordTypeId];
    }
    
    /**** Retrieve Self Operating Procedure & Attachments to Line Items ****/ 
    //Changed limit of attachments inner query from 1 to 2 since now there can be two attachments because of the new CBI-Deleted screen - Shannon
    public static  List<Applicant_Attachments__c> appattach(Id LineItemId, id rectypeid){
        return [SELECT ID,Name,Animal_Care_AC__c,Attachment_Type__c,RecordType.Name,Document_Types__c,File_Name__c,Status__c,
                Standard_Operating_Procedure__r.Name,Status_Graphical__c,Total_Files__c,CBI_Attachment__c,Applicant_Instructions__c,
                (SELECT ID,Name, parentID FROM Attachments LIMIT 2)
                FROM Applicant_Attachments__c 
                WHERE Animal_Care_AC__c=:LineItemId 
                AND recordTypeId=:rectypeid];
    }
    /**** BULKIFIED Retrieve Self Operating Procedure & Attachments to Line Items ****/ 
    //Changed limit of attachments inner query from 1 to 2 since now there can be two attachments because of the new CBI-Deleted screen - Shannon
    public static  List<Applicant_Attachments__c> appattach(Set<Id> LineItemId, id rectypeid){
        return [SELECT ID,Name,Animal_Care_AC__c,Attachment_Type__c,RecordType.Name,Document_Types__c,File_Name__c,Status__c,
                Standard_Operating_Procedure__r.Name,Status_Graphical__c,Total_Files__c,CBI_Attachment__c,Applicant_Instructions__c,
                (SELECT ID,Name, parentID FROM Attachments LIMIT 2)
                FROM Applicant_Attachments__c 
                WHERE Animal_Care_AC__c IN :LineItemId 
                AND recordTypeId=:rectypeid];
    }
    
    /**** Retrieve Regulated Articles related to Line Items ****/   
    public static  List<Link_Regulated_Articles__c> getRA(Id LineItemId){
        return [SELECT Id,Status__c,Status_Graphical__c,Name,Line_Item__c,Regulated_Article__r.name,Regulated_Article__r.Category_Group_Ref__r.name,
                Regulated_Article__r.Additional_Scientific_Name__c,Regulated_Article__r.Additional_Common_Name__c,Common_Name__c,
                Scientific_Name__c,Cultivar_and_or_Breeding_Line__c,CreatedDate,Corrections_Required__c
                FROM Link_Regulated_Articles__c WHERE Line_Item__c=:LineItemId];
    }
    
    /**** BULKIFIED Retrieve Regulated Articles related to Line Items ****/
    public static  List<Link_Regulated_Articles__c> getRA(Set<Id> LineItemIds){
        
        return [SELECT Line_Item__c,Id,Status__c,Status_Graphical__c,Name,Regulated_Article__r.name,Regulated_Article__r.Category_Group_Ref__r.name,
                Regulated_Article__r.Additional_Scientific_Name__c,Regulated_Article__r.Additional_Common_Name__c,Common_Name__c,
                Scientific_Name__c,Cultivar_and_or_Breeding_Line__c,CreatedDate,Corrections_Required__c
                FROM Link_Regulated_Articles__c WHERE Line_Item__c IN :LineItemIds];
    }
    
    /**** Retrieve Previously approved constructs to Line Items ****/   
    public static  List<Construct_Application_Junction__c> getRevCons(Id LineItemId){ 
        return [SELECT Id,Name,Line_Item__c,Construct__c,Construct__r.Construct_s__c, Status__c,Status_Graphical__c,Construct_Status__c, Status_new_Graphical__c, Applicant_Instructions__c,
                Construct__r.Mode_of_Transformation__c, Construct__r.Link_Regulated_Article__c,Construct__r.Regulated_Article__c,Construct__r.Mode_of_Transformation_Text__c 
                FROM Construct_Application_Junction__c WHERE Line_Item__c=:LineItemId AND Design_Protocol_Record_SOP__c=null AND Construct__c!= NULL ORDER By CreatedDate DESC];
    }
    
    /**** BULKIFIED Retrieve Previously approved constructs to Line Items ****/   
    
    public static  List<Construct_Application_Junction__c> getRevCons(Set<Id> LineItemId){
        
        return [SELECT Id,Name,Line_Item__c,Construct__c,Construct__r.Construct_s__c, Status__c,Status_Graphical__c,Construct_Status__c, Status_new_Graphical__c, Applicant_Instructions__c,
                Construct__r.Mode_of_Transformation__c, Construct__r.Link_Regulated_Article__c,Construct__r.Mode_of_Transformation_Text__c  
                FROM Construct_Application_Junction__c WHERE Line_Item__c IN :LineItemId AND Design_Protocol_Record_SOP__c=null AND Construct__c!= NULL];
    }
    
    /**** Retrieve Previously approved SOP's  related to Line Items ****/   
    public static  List<Construct_Application_Junction__c> getRevSOP(Id LineItemId){
        return [SELECT Id,Name,Line_Item__c,Design_Protocol_Record_SOP__c,Design_Protocol_Record_SOP__r.File_Name__c,CreatedDate,Applicant_Instructions__c,
                Design_Protocol_Record_Status__c,Design_Protocol_SOP_Graphical_Status__c,Status__c,Construct_Status__c,Status_new_Graphical__c,Design_Protocol_Record_SOP__r.CBI_Attachment__c
                FROM Construct_Application_Junction__c
                WHERE Line_Item__c=:LineItemId AND Construct__c=null];
    }
    /**** BULKIFIED Retrieve Previously approved SOP's  related to Line Items ****/   
    public static  List<Construct_Application_Junction__c> getRevSOP(Set<Id> LineItemId){
        return [SELECT Id,Name,Line_Item__c,Design_Protocol_Record_SOP__c,Design_Protocol_Record_SOP__r.File_Name__c,CreatedDate,Applicant_Instructions__c,
                Design_Protocol_Record_Status__c,Design_Protocol_SOP_Graphical_Status__c,Status__c,Construct_Status__c,Status_new_Graphical__c,Design_Protocol_Record_SOP__r.CBI_Attachment__c
                FROM Construct_Application_Junction__c
                WHERE Line_Item__c IN :LineItemId AND Construct__c=null];
    }
    
    /**
* Phenotype Method: Used to get all related Phenotypes
**/     
    public static List<Phenotype__c> phenotypes(Id ConstructID){
        return [SELECT Id, Name, Applicant_Instructions__c, Phenotypic_Category__c, Phenotype_Category_Text__c,
                Phenotypic_Description__c, Construct__c, Construct__r.Construct_s__c 
                FROM Phenotype__c 
                WHERE Construct__c=:ConstructID];        
    }
    
    /**
* Genotype Method: Used to get all related genotypes
**/     
    public static List<Genotype__c> genotypes(Id ConstructID){
        return [SELECT ID,Name,recordtype.name,recordtype.Id,Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                Construct_Component_Name__c,Donor_list__c,Description__c,Related_Construct_Record_Number__c,
                Related_Construct_Record_Number__r.Construct_s__c
                FROM Genotype__c 
                WHERE Related_Construct_Record_Number__c=: constructID ORDER BY recordtype.name];        
    }
    
    /**
* Genotype recordtype Method: Used to obtain genotypes recordtypes
**/     
    public static List<RecordType> GenoRecordtypes(Id ConstructID){
        return [SELECT DeveloperName,Name,Id FROM RecordType WHERE SObjectType='Genotype__c' AND Id in:getRecordtypeIds(ConstructID)];
    }
    
    /**
* Applicant Attacment Method: Used to obtain applicant attachments for BRS
**/     
    public static List<Applicant_Attachments__c> getAppAttachmentsBRS(Id appAttId){
        return [SELECT Id,Applicant_Instructions__c,Name,Document_Types__c,File_Name__c,File_Description__c,CBI_Attachment__c,Status__c, Status_Graphical__c,(SELECT ID,Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Id =: appAttId];
    }   
    
    
    
    /********** PRIVATE METHODS ***************************/
    /**
* Genotpe Record Type Method: Used to obtain genotype recordtype IDs
**/     
    private static list<Id> getRecordtypeIds(Id ConstructID){
        list<Id> recIdList=new  list<Id>();
        for(Genotype__c g:genotypes(ConstructID))recIdList.add(g.recordtype.Id);
        return recIdList;
    }
}