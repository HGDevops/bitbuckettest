/*
 Created: 3/29/2018
 Author: Dawn Sangree
 Purpose: Back button functionality for wizard code.  Captures all the flags set during the wizard execution so they can be returned to their immediately previous state upon selecting Back button.
 Note:  Broken up into three lists because only 32 parameters can be passed into a single method.
*/

public with sharing class EFLWizardFlagHistoryWrapper{

    //lists to hold the flag history
    public list<WizardFlagHistory1> WizardFlagHistoryList1{ get; set; }
    public list<WizardFlagHistory2> WizardFlagHistoryList2{ get; set; }
    public list<WizardFlagHistory3> WizardFlagHistoryList3{ get; set; }  
    
    public WizardFlagHistory1 lastWizardHistory1{get; set;}      
    public WizardFlagHistory2 lastWizardHistory2{get; set;}
    public WizardFlagHistory3 lastWizardHistory3{get; set;}        
    
    public EFLWizardFlagHistoryWrapper(){
        WizardFlagHistoryList1 = new list<WizardFlagHistory1>();
        WizardFlagHistoryList2 = new list<WizardFlagHistory2>();
        WizardFlagHistoryList3 = new list<WizardFlagHistory3>();                
    }
    
    //class to pull up the last wizardflaghistorylist entries to send back to controller class so flags can be restored
    public WizardFlagHistory1 getWizardFlagHistory1(){
        //remove the last history state snapshot
        WizardFlagHistoryList1.remove(WizardFlagHistoryList1.size()-1);

        //pass back the previous state snapshot
        WizardFlagHistory1 lastHistory1 = WizardFlagHistoryList1.get(WizardFlagHistoryList1.size()-1);
        return lastHistory1;
    }
    
    public WizardFlagHistory2 getWizardFlagHistory2(){
        WizardFlagHistoryList2.remove(WizardFlagHistoryList2.size()-1);    
        WizardFlagHistory2 lastHistory2 = WizardFlagHistoryList2.get(WizardFlagHistoryList2.size()-1);
        return lastHistory2;
    }
        
    public WizardFlagHistory3 getWizardFlagHistory3(){
        WizardFlagHistoryList3.remove(WizardFlagHistoryList3.size()-1);
        WizardFlagHistory3 lastHistory3 = WizardFlagHistoryList3.get(WizardFlagHistoryList3.size()-1);
        return lastHistory3;
    }    
    
    public void setWizardFlagHistory1(
        Boolean bCompSelection, Boolean bRegArticle, Boolean bRegScreenCmpltd, Boolean bSearchClicked, Boolean bShowACWizard, Boolean bShowArticle, 
        Boolean bShowCountry, Boolean bShowCountryDestn, Boolean bShowCountryExport, Boolean bShowCountrytransit, Boolean bshowNoheader, 
        Boolean bShowPermit, Boolean bShowPermitCR, Boolean bShowPermitLD, Boolean bShowPermitLNJ, Boolean bShowPermitNPR, Boolean bShowRegion, 
        Boolean bShowRegion2, Boolean bShowStateDestination, Boolean firstQuestion, Boolean isChild, Boolean isPickList, Boolean isRadio, 
        Boolean isScientfic, Boolean isSealedProduct, Boolean isShowIntendedUse, Boolean isWizardQuestion, Boolean showAdditionalInformation, 
        Boolean showImportRequirements, Boolean ShowInfo, Boolean showLOD, Boolean showNext){
                                   
        WizardFlagHistoryList1.add(new WizardFlagHistory1(bCompSelection, bRegArticle, bRegScreenCmpltd, bSearchClicked, bShowACWizard, 
                                                          bShowArticle, bShowCountry, bShowCountryDestn, bShowCountryExport, bShowCountrytransit, 
                                                          bshowNoheader, bShowPermit, bShowPermitCR, bShowPermitLD, bShowPermitLNJ, bShowPermitNPR, 
                                                          bShowRegion, bShowRegion2, bShowStateDestination, firstQuestion, isChild, isPickList, isRadio, 
                                                          isScientfic, isSealedProduct, isShowIntendedUse, isWizardQuestion, showAdditionalInformation, 
                                                          showImportRequirements, ShowInfo, showLOD, showNext));
    }
    
    //creates snapshots of all the flags.  Only 32 parameters allowed per class so broken up into multiple lists
    public class WizardFlagHistory1{
        public Boolean flag_bCompSelection;
        public Boolean flag_bRegArticle;
        public Boolean flag_bRegScreenCmpltd;
        public Boolean flag_bSearchClicked;
        public Boolean flag_bShowACWizard;
        public Boolean flag_bShowArticle;
        public Boolean flag_bShowCountry;
        public Boolean flag_bShowCountryDestn;
        public Boolean flag_bShowCountryExport;
        public Boolean flag_bShowCountrytransit;
        public Boolean flag_bshowNoheader;
        public Boolean flag_bShowPermit;
        public Boolean flag_bShowPermitCR;
        public Boolean flag_bShowPermitLD;
        public Boolean flag_bShowPermitLNJ;
        public Boolean flag_bShowPermitNPR;
        public Boolean flag_bShowRegion;
        public Boolean flag_bShowRegion2;
        public Boolean flag_bShowStateDestination;
        public Boolean flag_firstQuestion;
        public Boolean flag_isChild;
        public Boolean flag_isPickList;
        public Boolean flag_isRadio;
        public Boolean flag_isScientfic;
        public Boolean flag_isSealedProduct;
        public Boolean flag_isShowIntendedUse;
        public Boolean flag_isWizardQuestion;
        public Boolean flag_showAdditionalInformation;
        public Boolean flag_showImportRequirements;
        public Boolean flag_ShowInfo;
        public Boolean flag_showLOD;
        public Boolean flag_showNext;
        
        //method call to set the wizard history
          public WizardFlagHistory1(Boolean bCompSelection, Boolean bRegArticle, Boolean bRegScreenCmpltd, Boolean bSearchClicked, Boolean bShowACWizard, Boolean bShowArticle, 
                                   Boolean bShowCountry, Boolean bShowCountryDestn, Boolean bShowCountryExport, Boolean bShowCountrytransit, Boolean bshowNoheader, 
                                   Boolean bShowPermit, Boolean bShowPermitCR, Boolean bShowPermitLD, Boolean bShowPermitLNJ, Boolean bShowPermitNPR, Boolean bShowRegion, 
                                   Boolean bShowRegion2, Boolean bShowStateDestination, Boolean firstQuestion, Boolean isChild, Boolean isPickList, Boolean isRadio, 
                                   Boolean isScientfic, Boolean isSealedProduct, Boolean isShowIntendedUse, Boolean isWizardQuestion, Boolean showAdditionalInformation, 
                                   Boolean showImportRequirements, Boolean ShowInfo, Boolean showLOD, Boolean showNext
                                   ){
                                
            flag_bCompSelection = bCompSelection;
            flag_bRegArticle = bRegArticle;
            flag_bRegScreenCmpltd = bRegScreenCmpltd;
            flag_bSearchClicked = bSearchClicked;
            flag_bShowACWizard = bShowACWizard;
            flag_bShowCountry = bShowCountry;
            flag_bShowCountryDestn = bShowCountryDestn;
            flag_bShowCountryExport = bShowCountryExport;
            flag_bShowCountrytransit = bShowCountrytransit;
            flag_bshowNoheader = bshowNoheader;
            flag_bShowPermit = bShowPermit;
            flag_bShowPermitCR = bShowPermitCR;
            flag_bShowPermitLD = bShowPermitLD;
            flag_bShowPermitLNJ = bShowPermitLNJ; 
            flag_bShowPermitNPR = bShowPermitNPR;
            flag_bShowRegion = bShowRegion;
            flag_bShowRegion2 = bShowRegion2;
            flag_bShowStateDestination = bShowStateDestination;
            flag_firstQuestion = firstQuestion;
            flag_isChild = isChild;
            flag_isPickList = isPickList;
            flag_isRadio = isRadio;
            flag_isScientfic = isScientfic;
            flag_isSealedProduct = isSealedProduct;
            flag_isShowIntendedUse = isShowIntendedUse;
            flag_isWizardQuestion = isWizardQuestion;
            flag_showAdditionalInformation = showAdditionalInformation;
            flag_showImportRequirements = showImportRequirements;
            flag_ShowInfo = ShowInfo;
            flag_showLOD = showLOD;
            flag_showNext = showNext;
        }
    }    
    
    public void setWizardFlagHistory2(
        Boolean ShowNextWizardQuestionsOne, Boolean showNJLetter, Boolean showNPRLetter, Boolean showProductType, Boolean showRegulations, Integer questionNo, 
        String currentQuestStr,  String optionType, String parentpathwayId, String programPathwayID, String questionId, String questionType, String selectedAnswerId, 
        String sSelArticle, String sSelCat, String sSelCountry, String sSelCountryExport, String sSelDestState, String sSelProduct, String sSelProductType, 
        String sSelReg1, String sSelScient, String sSrhArticle, String thumbPrintResponseID, String thumbPrintResponseType, String wizardQuestion, String wqTPId,
        String sSelectedLandOpt, String sSelComponent, String selectedPortOptionsEntry, String selectedPortOptionsExit, String sSelTransitCountry){
                                   
        WizardFlagHistoryList2.add(new WizardFlagHistory2(ShowNextWizardQuestionsOne, showNJLetter, showNPRLetter, showProductType, showRegulations, questionNo, 
                                                          currentQuestStr,  optionType, parentpathwayId, programPathwayID, questionId, questionType, selectedAnswerId, 
                                                          sSelArticle, sSelCat, sSelCountry, sSelCountryExport, sSelDestState, sSelProduct, sSelProductType, 
                                                          sSelReg1, sSelScient, sSrhArticle, thumbPrintResponseID, thumbPrintResponseType, wizardQuestion, wqTPId,
                                                          sSelectedLandOpt, sSelComponent, selectedPortOptionsEntry, selectedPortOptionsExit, sSelTransitCountry));
    }    
    
    //creates snapshots of all the flags.  Only 32 parameters allowed per class so broken up into multiple lists
    public class WizardFlagHistory2{
        public Boolean flag_ShowNextWizardQuestionsOne;
        public Boolean flag_showNJLetter;
        public Boolean flag_showNPRLetter;
        public Boolean flag_showProductType;
        public Boolean flag_showRegulations;
        public Integer flag_questionNo;
        public String flag_currentQuestStr;  
        public String flag_optionType;
        public String flag_parentpathwayId;
        public String flag_programPathwayID;
        public String flag_questionId;
        public String flag_questionType;
        public String flag_selectedAnswerId;
        public String flag_sSelArticle;
        public String flag_sSelCat;
        public String flag_sSelComponent;
        public String flag_sSelCountry;
        public String flag_sSelCountryExport;
        public String flag_sSelDestState;
        public String flag_sSelProduct;
        public String flag_sSelProductType;
        public String flag_sSelReg1;
        public String flag_sSelScient;
        public String flag_sSelectedLandOpt;
        public String flag_sSrhArticle;
        public String flag_selectedPortOptionsEntry;
        public String flag_selectedPortOptionsExit;        
        public String flag_sSelTransitCountry;
        public String flag_thumbPrintResponseID;
        public String flag_thumbPrintResponseType;
        public String flag_wizardQuestion;
        public String flag_wqTPId;     
               
        //method call to set the wizard history
          public WizardFlagHistory2(Boolean ShowNextWizardQuestionsOne, Boolean showNJLetter, Boolean showNPRLetter, Boolean showProductType, Boolean showRegulations, Integer questionNo, 
                                    String currentQuestStr,  String optionType, String parentpathwayId, String programPathwayID, String questionId, String questionType, String selectedAnswerId, 
                                    String sSelArticle, String sSelCat, String sSelCountry, String sSelCountryExport, String sSelDestState, String sSelProduct, String sSelProductType, 
                                    String sSelReg1, String sSelScient, String sSrhArticle, String thumbPrintResponseID, String thumbPrintResponseType, String wizardQuestion, String wqTPId, 
                                    String sSelectedLandOpt, String sSelComponent, String selectedPortOptionsEntry, String selectedPortOptionsExit, String sSelTransitCountry){
                                
            flag_ShowNextWizardQuestionsOne = ShowNextWizardQuestionsOne;
            flag_showNJLetter = showNJLetter;
            flag_showNPRLetter = showNPRLetter;
            flag_showProductType = showProductType;
            flag_showRegulations = showRegulations;
            flag_questionNo = questionNo;
            flag_currentQuestStr = currentQuestStr;  
            flag_optionType = optionType;
            flag_parentpathwayId = parentpathwayId;
            flag_programPathwayID = programPathwayID;
            flag_questionId = questionId;
            flag_questionType = questionType;
            flag_selectedAnswerId = selectedAnswerId;
            flag_sSelArticle = sSelArticle;
            flag_sSelCat = sSelCat;
            flag_sSelCountry = sSelCountry;
            flag_sSelCountryExport = sSelCountryExport;
            flag_sSelDestState = sSelDestState;
            flag_sSelProduct = sSelProduct;
            flag_sSelProductType = sSelProductType;
            flag_sSelReg1 = sSelReg1;
            flag_sSelScient = sSelScient;
            flag_sSrhArticle = sSrhArticle;
            flag_thumbPrintResponseID = thumbPrintResponseID;
            flag_thumbPrintResponseType = thumbPrintResponseType;
            flag_wizardQuestion = wizardQuestion;
            flag_wqTPId = wqTPId; 
            flag_sSelectedLandOpt = sSelectedLandOpt;
            flag_sSelComponent = sSelComponent;
            flag_selectedPortOptionsEntry = selectedPortOptionsEntry;
            flag_selectedPortOptionsExit = selectedPortOptionsExit;
            flag_sSelTransitCountry = sSelTransitCountry;
        }
    }

    public void setWizardFlagHistory3(
        Map<String, String> allAnswersTraceMap, Map<String, String> fieldAnswerMap, Map<String, String> selectedAnswerIdMap, Map<String, String> allAnswersMap, List<regulated_article__c> listRSA, 
        List<Program_Line_Item_Pathway__c> progLineItemPathwayRecords, List<Program_Line_Item_Pathway__c> ProgramLineItem, List<Id> RAIdList, 
        List<SelectOption> selValSelectOptions, List<SelectOption> ScientificList, Set<String> setAnsTrace, Wizard_Questionnaire__c objwizardquestion, Wizard_Questionnaire__c wizardQARecord, Boolean isAnswerTrace,
        String SelectedPathwayId, Set<ID> FinalRAGroupIds, Set<ID> RAGroupIds, Set<Id> CountryGroupIds){
                                   
        WizardFlagHistoryList3.add(new WizardFlagHistory3(allAnswersTraceMap, fieldAnswerMap, selectedAnswerIdMap, allAnswersMap, listRSA, 
                                                          progLineItemPathwayRecords, ProgramLineItem, RAIdList, 
                                                          selValSelectOptions, ScientificList, setAnsTrace, objwizardquestion, wizardQARecord,
                                                          isAnswerTrace, SelectedPathwayId, FinalRAGroupIds, RAGroupIds, CountryGroupIds));
    }  
    
    //creates a single snapshot of all the flags
    public class WizardFlagHistory3{
        //all the flags as properties
        public Map<String, String> flag_allAnswersTraceMap;
        public Map<String, String> flag_fieldAnswerMap;        
        public Map<String, String> flag_selectedAnswerIdMap;
        public Map<String, String> flag_allAnswersMap;
        //flag_answerTrace;
        public List<regulated_article__c> flag_listRSA;
        public List<Program_Line_Item_Pathway__c> flag_progLineItemPathwayRecords;
        public List<Program_Line_Item_Pathway__c> flag_ProgramLineItem;
        public List<Id> flag_RAIdList;
        public List<SelectOption> flag_selValSelectOptions;
        public List<SelectOption> flag_ScientificList;        
        public Set<String> flag_setAnsTrace;
        public Wizard_Questionnaire__c flag_objwizardquestion;
        public Wizard_Questionnaire__c flag_wizardQARecord;
        public Boolean flag_isAnswerTrace;
        public String flag_SelectedPathwayId;
        public Set<ID> flag_FinalRAGroupIds;
        public Set<ID> flag_RAGroupIds; 
        public Set<ID> flag_CountryGroupIds;                

        //method call to set the wizard history
        public WizardFlagHistory3(Map<String, String> allAnswersTraceMap, Map<String, String> fieldAnswerMap, Map<String, String> selectedAnswerIdMap, Map<String, String> allAnswersMap, List<regulated_article__c> listRSA, 
                                  List<Program_Line_Item_Pathway__c> progLineItemPathwayRecords, List<Program_Line_Item_Pathway__c> ProgramLineItem, List<Id> RAIdList, 
                                  List<SelectOption> selValSelectOptions, List<SelectOption> ScientificList, Set<String> setAnsTrace, Wizard_Questionnaire__c objwizardquestion, Wizard_Questionnaire__c wizardQARecord,
                                  Boolean isAnswerTrace, String SelectedPathwayId, Set<ID> FinalRAGroupIds, Set<ID> RAGroupIds, Set<Id> CountryGroupIds){
                                
            flag_allAnswersTraceMap = new Map<String, String>(allAnswersTraceMap);
            flag_fieldAnswerMap = new Map<String, String>(fieldAnswerMap);
            flag_selectedAnswerIdMap = new Map<String, String>(selectedAnswerIdMap);
            flag_allAnswersMap = new Map<String, String>(allAnswersMap);
            flag_listRSA = new List<regulated_article__c>(listRSA);
            flag_progLineItemPathwayRecords = new List<Program_Line_Item_Pathway__c>(progLineItemPathwayRecords);
            flag_ProgramLineItem = new List<Program_Line_Item_Pathway__c>(ProgramLineItem);
            flag_RAIdList = new List<Id>(RAIdList);
            flag_selValSelectOptions = new List<SelectOption>(selValSelectOptions);
            flag_ScientificList = new List<SelectOption>(ScientificList);
            flag_setAnsTrace = setAnsTrace;
            flag_objwizardquestion = objwizardquestion;
            flag_wizardQARecord = wizardQARecord;
            flag_isAnswerTrace = isAnswerTrace;
            flag_SelectedPathwayId = SelectedPathwayId;
            flag_FinalRAGroupIds = new Set<Id>(FinalRAGroupIds);
            flag_RAGroupIds = new Set<Id>(RAGroupIds);
            flag_CountryGroupIds = new Set<ID>(CountryGroupIds);            
        }        
    } 
}