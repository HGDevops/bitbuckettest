public inherited sharing class CARPOL_UNI_ApplicationHelperClass {
 // we are using new functionalityto capture workflows under authorization and the below code needs to be terminated.   
  /*  public static void WithdrawAuthWFs(set<ID> appIds){
        try{
            list <authorizations__c> lstAuth = new list <authorizations__c>();
            list <Workflow_Task__c> lstWF = new list <Workflow_Task__c>();
            set<string> auths = new set<string>();
            for( authorizations__c auth: [select id, status__c from Authorizations__c where Application__c in :appids])
            {
                auth.status__c = 'Withdrawn';
                lstAuth.add(auth);
                auths.add(auth.id);
            }
            
            system.debug('---auths--- '+auths);
            for ( Workflow_Task__c wftasks : [select id, status__c from Workflow_Task__c where Authorization__c in :auths and status__c <> 'Complete'])
            {
                system.debug('---wftasks --- '+wftasks );
                wftasks.status__c = 'Deferred';
                lstWF.add(wftasks);
            }    
            if (!lstWF.isempty())
            {
                update lstWF;
            }
            if (!lstAuth.isempty()){
                update lstAuth;
            }            
        }catch(exception e){
            system.debug('Excetion occured in CARPOL_UNI_ApplicationHelperClass'+e);
            
        }
    }  */
    //KA::W-026020::Delete the related object records associated with application
    public static void deleteAllAssociateRecord(set<ID> appIds){
        List<AC__c> lineItemsList = new List<AC__c>();
        
        if(!appIds.isEmpty()){
            
            lineItemsList = [SELECT Id,Application_Number__c FROM AC__c WHERE Application_Number__c IN:appIds];
            
            if(!lineItemsList.isEmpty()){
                
                delete lineItemsList;
                
            }
            
            
        }
        
    }
    //KA::W-026020::ENDS:::
 
  
}