@isTest(seealldata=true)
private class EFLOutcomeLetterUtility_test{
static testMethod void UnitTestAuthenticated() {       
            Id ProfileId = [SELECT Id FROM Profile WHERE Name = 'APHIS Applicant'].Id;
            User u = [Select Id, IsPortalEnabled, ContactId From User where IsPortalEnabled = true AND isActive = true AND ProfileId =: ProfileId Limit 1];
            
            Domain__c objProg = new Domain__c();
            objProg.Name = 'AC';
            objProg.Active__c = true;
            insert objProg;
         
            Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
            objParentPathway.Program__c = objProg.Id;
            objParentPathway.Name = 'Live Animals';
            objParentPathway.Status__c = 'Active';  
            objParentPathway.Letter_Expiration_Timeframe__c = 10;
            insert objParentPathway;           
        System.runAs(u) {
            String cookieString = '';
            Map<String,String> fieldAnswerMap = new Map<String, String>();
            fieldanswerMap.put('test','test');
            String SelectedPathwayId = objParentPathway.id;     
            String wqTPId = '';
            String processId = '22';      
            Test.startTest();       
                PageReference pRef = EFLOutcomeLetterUtility.generateOutcomeLetter(cookieString, fieldAnswerMap, SelectedPathwayId, wqTPId, processId );      
                system.assert(pRef !=null);      
            Test.stopTest();
        }
    }
    
 static testMethod void UnitTestAnonymous() {       
            Id ProfileId = [SELECT Id FROM Profile WHERE Name = 'APHIS Customer Community Profile'].Id;
            User u = [Select Id From User where ProfileId =: ProfileId Limit 1];
            
            Domain__c objProg = new Domain__c();
            objProg.Name = 'AC';
            objProg.Active__c = true;
            insert objProg;
         
            Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
            objParentPathway.Program__c = objProg.Id;
            objParentPathway.Name = 'Live Animals';
            objParentPathway.Status__c = 'Active';  
            objParentPathway.Letter_Expiration_Timeframe__c = 10;
            insert objParentPathway;           
        System.runAs(u) {
            String cookieString = '';
            Map<String,String> fieldAnswerMap = new Map<String, String>();
            fieldanswerMap.put('test','test');
            String SelectedPathwayId = objParentPathway.id;     
            String wqTPId = '';
            String processId = '22';      
            Test.startTest();       
                PageReference pRef = EFLOutcomeLetterUtility.generateOutcomeLetter(cookieString, fieldAnswerMap, SelectedPathwayId, wqTPId, processId );      
                system.assert(pRef !=null);      
            Test.stopTest();
        }
    }   

}