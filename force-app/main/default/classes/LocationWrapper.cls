public inherited sharing class LocationWrapper {
    public Location__c location {get;set;}
    
    public LocationWrapper(Location__c loc){
        this.location = loc;
    }
    
    public String information {        
        get {
            system.debug('this.location.RecordType.Name: ' + this.location.RecordType.Name);
            String info = '';
            if(this.location.RecordType.Name == 'Origin and Destination Location' || this.location.RecordType.Name == 'Destination Location'){
                List<Material__c> materials = [Select Id,Quantity__c,Unit_of_Measure__c,Material__c,Other_Material__c,Material_CBI__c,Material_Type_Text__c from Material__c where Location__r.Id =: this.location.Id];
                if (!materials.isEmpty()){
                    info += '<b>Material Information:</b><br/>';
                    for (Material__c mat : materials){
                        info += mat.Material_Type_Text__c + '<br/>';
                    } 
                }
                
            } else if (this.location.RecordType.Name == 'Release Sites Location'){
                info += '<b>Location Unique ID: </b>' + this.location.Location_Unique_Id__c + '<br/>';
                info += '<b>Number of Acres: </b>' + this.location.Number_of_Acres_Text__c + '<br/>';
            } 
            return info;
        }
        set;            
    }
}