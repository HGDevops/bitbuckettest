/***************************************************************************
* This test class is for the EFLLocationRepository class which queries
* the location object based on the LineItemID
****************************************************************************/
@isTest
public class EFLLocationRepository_test {
    static Country__c testCountry;
    static Level_1_Region__c testLevelRegion1;    
    static Level_2_Region__c testLevelRegion2;
    static Application__c application;    
    static AC__c lineItem;
    
    static void initData(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testDataManager = new CARPOL_AC_TestDataManager();
		testData.insertcustomsettings();
		testDataManager.insertcustomsettings();        
        testCountry = testData.newcountryus();
        testLevelRegion1 = testData.newlevel1region(testCountry.Id);
        testLevelRegion2 = testData.newlevel2region(testLevelRegion1.Id);
        application = testDataManager.newApplication();
        lineItem = testData.newLineItemBRS('Release',application);
    }
    static Location__c createLocation(String recordType){
        Location__c loc1 = new Location__c(Name='testloc', Country__c=testCountry.Id, state__C= testLevelRegion1.Id,
                                           Level_2_Region__c = testLevelRegion2.Id, 
                                           RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId(),
                                           GPS_1__c = 'GPS_1__c',
                                           GPS_2__c = 'GPS_2__c',
                                           GPS_3__c = 'GPS_3__c',
                                           GPS_4__c = 'GPS_4__c',
                                           GPS_5__c = 'GPS_5__c',
                                           GPS_6__c = 'GPS_6__c',
                                           Contact_Name1__c = 'Test',
                                           Primary_Contact_Last_Name__c = 'LName', 
                                           Day_Phone__c = '555-555-1212' ,
                                           Status__c='Review Completed' ,
                                           Applicant_Instructions__c='Test Instructions',
                                           Unit_of_Measure_CBI__c=true,
                                           Material_Type_CBI__c=true,
                                           Primary_Country_CBI__c=true,
                                           Primary_State_CBI__c=true,
                                           Secondary_Country_CBI__c=true,
                                           Secondary_County_CBI__c = true,
                                           Secondary_State_CBI__c=true,
                                           Line_Item__c=lineItem.Id
                                          );
        return loc1;
    }

    @isTest
    static void testFindLocationByLineItemId() {
        initData();
        Location__c loc1 = createLocation('Origin Location');
        //insert loc1;
        Test.startTest();
        //EFLLocationRepository.selectByLineItemID(lineItem.Id);
        EFLLocationRepository.selectByLineItemID(null);
        //checking to see if line item has value
        system.assert(lineItem != null);
        Test.stopTest();
    }
    
    @isTest
    static void testFindLocationByselectByID() {
        initData();
        Location__c loc1 = createLocation('Origin Location');
        //insert loc1;
        Test.startTest();
        //EFLLocationRepository.selectByLineItemID(lineItem.Id);
        try{EFLLocationRepository.selectByID(null);}catch(exception ex){}
        //checking to see if line item has value
        system.assert(lineItem != null);
        Test.stopTest();
    }
    
    
        @isTest
    static void testFindLocationByLineItemId11() {
        initData();
        Location__c loc1 = createLocation('Origin Location');
        //insert loc1;
        Test.startTest();
        EFLLocationRepository.selectByLineItemID(lineItem.Id);
        //EFLLocationRepository.selectByLineItemID(null);
        //checking to see if line item has value
        system.assert(lineItem != null);
        Test.stopTest();
    }
    @isTest
    static void testCloneLocationByLineItemId() {
        initData();
        Location__c loc1 = createLocation('Destination Location');
        //insert loc1;
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        //EFLLocationRepository.selectByLineItemIDforClone(lineItem.Id);
        EFLLocationRepository.selectByLineItemIDforClone(null);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    
    @isTest
    static void testLocationByLineItemId() {
        initData();
        Location__c loc1 = createLocation('Destination Location');
        //insert loc1;
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        //EFLLocationRepository.selectByLineItemIDforClone(lineItem.Id);
        EFLLocationRepository.selectByLineItemID(null);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    
    
    @isTest static void testFindLocationByLineItemId2(){
        initData();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'EFLLocationRepository@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Test.startTest();
          try{
              EFLLocationRepository.selectByLineItemID(RecordTypeId);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
    
    @isTest static void testFindLocationByLineItemId3(){
        initData();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Test.startTest();
          try{
              EFLLocationRepository.selectByLineItemIDforClone(RecordTypeId);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
    
    @isTest
    static void testselectLocsWithRelSRsByLineItemId() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
        //insert loc1;
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectLocsWithRelSRsByLineItemId(lineItem.Id,'Destination Location',srList);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectLocsWithRelSRsByLineItemId1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Test.startTest();
          try{
              EFLLocationRepository.selectLocsWithRelSRsByLineItemId(lineItem.Id,'Origin Location',srList);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
     @isTest
    static void testselectLocsWithRelVolSRsByLineItemId() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
        Set<Id> locList = new Set<Id>();
        locList.add(loc1.Id);
        //insert loc1;
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        //EFLLocationRepository.selectByLineItemIDforClone(lineItem.Id);
        EFLLocationRepository.selectLocsWithRelVolSRsByLineItemId(locList,srList);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectLocsWithRelVolSRsByLineItemId1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Set<Id> locList = new Set<Id>();
          locList.add(loc1.Id);
          Test.startTest();
          try{
              EFLLocationRepository.selectLocsWithRelVolSRsByLineItemId(locList,srList);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
    
       @isTest
    static void testsearchLocByKeywordWithInLineItemID() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.searchLocByKeywordWithInLineItemID(lineItem.id,'test');
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testsearchLocByKeywordWithInLineItemID1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
      
          Test.startTest();
          try{
              EFLLocationRepository.searchLocByKeywordWithInLineItemID(lineItem.id,'test');
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
    
       @isTest
    static void testselectContactsByLocationsforClone() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
        Set<Id> locList = new Set<Id>();
        locList.add(loc1.Id);
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectContactsByLocationsforClone(locList);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectContactsByLocationsforClone1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Set<Id> locList = new Set<Id>();
          locList.add(loc1.Id);
          Test.startTest();
          try{
              EFLLocationRepository.selectContactsByLocationsforClone(locList);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
        @isTest
    static void testselectContactsByLocationIDforClone() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
    
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectContactsByLocationIDforClone(loc1.id);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectContactsByLocationIDforClone1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
      
          Test.startTest();
          try{
              EFLLocationRepository.selectContactsByLocationIDforClone(loc1.id);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
    
          @isTest
    static void testselectGPSByLocationsforClone() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
        Set<Id> locList = new Set<Id>();
        locList.add(loc1.Id);
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectGPSByLocationsforClone(locList);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectGPSByLocationsforClone1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Set<Id> locList = new Set<Id>();
          locList.add(loc1.Id);
          Test.startTest();
          try{
              EFLLocationRepository.selectGPSByLocationsforClone(locList);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
        @isTest
    static void testselectGPSByLocationIDforClone() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
    
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectMaterialsByLocationIDforClone(loc1.id);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectGPSByLocationIDforClone1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
      
          Test.startTest();
          try{
              EFLLocationRepository.selectGPSByLocationIDforClone(loc1.id);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
         @isTest
    static void testselectMaterialsByLocationsClone() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
        Set<Id> locList = new Set<Id>();
        locList.add(loc1.Id);
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectMaterialsByLocationsClone(locList);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectMaterialsByLocationsClone1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
          Set<Id> locList = new Set<Id>();
          locList.add(loc1.Id);
          Test.startTest();
          try{
              EFLLocationRepository.selectMaterialsByLocationsClone(locList);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
        @isTest
    static void testselectMaterialsByLocationIDforClone() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Location__c loc1 = createLocation('Destination Location');
    
        Test.startTest();
        system.debug('LineItem value is'+lineItem.id);
        EFLLocationRepository.selectMaterialsByLocationIDforClone(loc1.id);
        //checking to see if line item has value
        system.assert(lineItem !=null);
        Test.stopTest();
    }
    @isTest
    static void testselectMaterialsByLocationIDforClone1() {
        initData();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
          Id RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
          Location__c loc1 = createLocation('Origin Location');
      
          Test.startTest();
          try{
              EFLLocationRepository.selectMaterialsByLocationIDforClone(loc1.id);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
}