@isTest(seeAllData=false)
public class EFLRegulatedArticleRepository_Test {
    public static ac__c li;
    public Id lineItemID;
    public static Contact con;
    public static Application__c app;
    public static Authorizations__c auth;
    public static Domain__c prog;
    public static Program_Line_Item_Pathway__c plip;
    public static Program_Prefix__c pp;
    public static Signature__c tp;
    public static Trade_Agreement__c ta;
    public static Country__c Country;
    public static Country__c USCountry;
    public static Level_1_Region__c level1;
    public static Level_2_Region__c level2;
    public static Regulated_Article__c ra;
    public static Link_Regulated_Articles__c lra;
    public static Link_Regulated_Articles__c lra1;
    public static Construct__c construct;
    public static Construct_Application_Junction__c caj;
    public static void initData(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        con = testData.newContact();
        app = testData.newapplication(); 
        auth = testData.newAuth(app.id);
        prog = testData.newProgram('test');
        plip = testData.newCaninePathway();
        pp = testData.newPrefix();
        tp = testData.newThumbprint();
        ta = testData.newta();
        Country = testData.newcountrywithassoc();
        USCountry = testData.newcountryus();
        level1 = testData.newlevel1region(Country.id);
        level2 = testData.newlevel2region(level1.id);
        auth = testData.newAuth(app.Id);
        li = testData.newLineItemBRS('Personal Use', app); 
        lra = testData.newlinkRegArticle(li.id, app.id,auth.id); 
        lra.Corrections_Required__c='';
        update lra;
        ra =  testData.newRegulatedArticle(plip.id);
        lra1 = new Link_Regulated_Articles__c();
        lra1.id = lra.id;
        lra1.Regulated_Article__c = ra.id;
        update lra1;
        
        construct = testData.newconstruct(li.id,ra);
        caj = new Construct_Application_Junction__c();
        caj.Application__c = app.id;
        caj.Authorization__c = auth.id;
        caj.Construct__c = construct.id;
        caj.Line_Item__c = li.id;
        insert caj;
        
           }  
    public static testMethod void selectRegulatedArticleIdsByPathwayIds() {
        List <Program_Line_Item_Pathway__c>  Prflist = [select Id from Program_Line_Item_Pathway__c];
         Set<Id> plipId= new Set<Id>();
        for(Program_Line_Item_Pathway__c plip1: Prflist) {
            plipId.add(plip1.Id);
        }
        Test.startTest();
         EFLRegulatedArticleRepository.selectRegulatedArticleIdsByPathwayIds(plipId);
         EFLRegulatedArticleRepository.selectRegulatedArticlesByPathwayIDsandCategoryIDs(plipId,null);
        Test.stopTest();
    }
    }