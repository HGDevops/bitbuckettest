@isTest(SeeallData=false)
public class EFLAnnualReportTriggerHandler_Test {
    private static final string AccountName = 'Test Account';
    private static final string ContactLastName = 'Test';
    private static final string Sites = 'Site 1';
    private static final String[] StdAnimalNames = new String[] {'Dogs','Cats','Guinea Pigs','Hamsters','Rabbits','Non-Human Primates','Sheep','Pigs','Other Animals'};
    private static final Integer StdAnimalStartingSeqNum = 4;

    @TestSetup    
    static void makeData(){
        // Add standard animals
        List<EFLAnimal__c> standardAnimals = new List<EFLAnimal__c>();
        for(String name :StdAnimalNames) {
        	EFLAnimal__c animal = new EFLAnimal__c();
            animal.Name = name;
            animal.Other_Animal__c = false;
            standardAnimals.add(animal);
        }
        insert standardAnimals;
        
        // Insert custom setting values
        String value = '';
        for(EFLAnimal__c animal :standardAnimals) {
            value = value + animal.Id + ',';
        }
        insert new EFLACLRA_Application_Settings__c(Name = 'ARStandardAnimalIds', Value__c = value.left(value.length()-1));
        insert new EFLACLRA_Application_Settings__c(Name = 'ARRegulatedAnimalsStartingSeqNum', Value__c = String.valueOf(StdAnimalStartingSeqNum));

        // Make a parent object for Uploaded_File__c to be created under in test execution.
        Account account = new Account();
        account.Name = AccountName;
        insert account;
        
        List<RecordType> rt = [Select Id From RecordType Where SObjectType = 'Contact' and DeveloperName='AC_R_L_Contact' Limit 1];
        Contact contact = new Contact();
        contact.LastName = ContactLastName;
        contact.AccountId = account.Id;
        contact.RecordTypeId = rt.size() > 0 ? rt[0].Id : null;
        contact.email = 'vijay.vellaturi@accenturefederal.com';
        insert contact;
        
        //create a facility admin
        Contact contact2 = new Contact();
        contact2.LastName = ContactLastName;
        contact2.AccountId = account.Id;
        contact2.EFL_Facility_Admin__c = true;
        contact2.RecordTypeId = rt.size() > 0 ? rt[0].Id : null;
        contact2.email = 'vijay.vellaturi@accenturefederal.com';
        insert contact2;
        
        EFLRegistration__c registration = new EFLRegistration__c();
        registration.EFLAccount__c = account.Id;
        insert registration;
        
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = account.Id;
        annualReport.EFLContact__c = contact.Id;
        annualReport.EFLRegistration__c = registration.Id;
        annualReport.EFLSites__c = Sites;
        insert annualReport;
    }
    
    @isTest
    public static void SendRescindEmail(){
        EFLAnnual_Report__c annualReport = [Select Id, EFLRegistration__c From EFLAnnual_Report__c Limit 1];   
        annualReport.Status__c = 'Waiting on Customer';
        annualReport.Rescinded_Reason__c= 'NEED VALUES';
        Test.startTest();
            update annualReport;
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.debug('Invocations: ' + invocations);
		system.assertNotEquals(0, invocations, 'An email should be sent');
        system.assertEquals(annualReport.Status__c, 'Waiting on Customer');
    }
    
    @isTest
    public static void createTwoAnnualReports_StandardAnimalsAdded() {
		Contact contact = [Select Id, AccountId From Contact Limit 1];

        List<EFLRegistration__c> registrations = new List<EFLRegistration__c>();
        for (Integer i=0; i<2; i++) {
            EFLRegistration__c registration = new EFLRegistration__c();
            registration.EFLAccount__c = contact.AccountId;
            registrations.add(registration);
        }
		insert registrations;
                
        List <EFLAnnual_Report__c> annualReports = new List<EFLAnnual_Report__c>();
        for (Integer i=0; i<2; i++) {
            EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
            annualReport.EFLAccount__c = contact.AccountId;
            annualReport.EFLContact__c = contact.Id;
            annualReport.EFLRegistration__c = registrations[i].Id;
            annualReports.add(annualReport);
        }
        insert annualReports;
        
        // Get the list of regulated animals for the inserted annual reports
        List<EFLRegistered_Animal__c> regulatedAnimals = [Select Id, EFLAnimal__c, EFLAnimalName__c
														  , EFLAnnual_Report__c, EFLSequence__c, EFLSelectedForReport__c
                                                          , EFLOtherAnimal__c, EFLColumnBHeldNotUsed__c
                                                          , EFLColumnCUsedPainMinimized__c, EFLColumnDUsedPainMinimized__c
                                                          , ELFColumnEPainNotMinimized__c
                                                    	  From EFLRegistered_Animal__c
                                                    	  Where EFLAnnual_Report__c = :annualReports[0].Id 
                                                          	 Or EFLAnnual_Report__c = :annualReports[1].Id
                                                          Order By EFLAnnual_Report__c, EFLSequence__c];

        // Assert that the records are added as expected
        System.assertEquals(18, regulatedAnimals.size());
        
        Integer index = 0;
        for(Integer i=0; i<2 ;i++) {
            Integer sequenceNumber = StdAnimalStartingSeqNum;
            for(Integer j=0; j<9 ;j++) {
                EFLRegistered_Animal__c ra = regulatedAnimals[index];
                index += 1;
                System.assert(String.isNotBlank(ra.EFLAnimalName__c));
                System.assert(ra.EFLAnnual_Report__c == annualReports[0].Id || ra.EFLAnnual_Report__c == annualReports[1].Id);
                System.assertEquals(StdAnimalNames[j], ra.EFLAnimal__c);
                System.assertEquals(j+sequenceNumber, ra.EFLSequence__c);
                System.assertEquals(false, ra.EFLSelectedForReport__c);
                System.assertEquals(ra.EFLAnimal__c.contains('Other'), ra.EFLOtherAnimal__c);
                System.assertEquals(0, ra.EFLColumnBHeldNotUsed__c);
                System.assertEquals(0, ra.EFLColumnCUsedPainMinimized__c);
                System.assertEquals(0, ra.EFLColumnDUsedPainMinimized__c);
                System.assertEquals(0, ra.ELFColumnEPainNotMinimized__c);
            }
        }
    }
    
    @isTest
    public static void updateAnnualReport_RegistrationRecordUpdated() {
		EFLRegistration__c registration = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport = [Select Id, EFLAccount__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration.Id Limit 1];
        
        // Assert that the flag is set to true since the registration already has an annual report
        System.assertEquals(true, registration.EFLAnnual_Report_Created__c);

		// Delete the annual report
		delete annualReport;
		registration = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
        System.assertEquals(false, registration.EFLAnnual_Report_Created__c);

		// Undelete the annual report        
        undelete annualReport;
		registration = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
        System.assertEquals(true, registration.EFLAnnual_Report_Created__c);

        // Move the annuial report to a different registration
        EFLRegistration__c registration2 = new EFLRegistration__c();
        registration2.EFLAccount__c = annualReport.EFLAccount__c;
        insert registration2;
        annualReport.EFLRegistration__c = registration2.Id;
        update annualReport;

        // Asseret that the flag is cleared on the old registration
		registration = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Where Id = :registration.Id Limit 1];
        System.assertEquals(false, registration.EFLAnnual_Report_Created__c);

        // Assert that the flag is set on the new registration
		registration2 = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Where Id = :registration2.Id Limit 1];
        System.assertEquals(true, registration2.EFLAnnual_Report_Created__c);
    }

    @isTest
    public static void testSingleAnnualReportPerRegistration_ErrorReturnedWhenMultipleARs() {
		EFLRegistration__c registration1 = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport1 = [Select Id, EFLAccount__c, EFLContact__c, EFLSites__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration1.Id Limit 1];

        // Create another registration
        EFLRegistration__c registration2 = new EFLRegistration__c();
        registration2.EFLAccount__c = annualReport1.EFLAccount__c;
        insert registration2;
        
        // Create two annual reports
        List<EFLAnnual_Report__c> annualReports = new List<EFLAnnual_Report__c>();
        EFLAnnual_Report__c annualReport2 = new EFLAnnual_Report__c();
        annualReport2.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport2.EFLContact__c = annualReport1.EFLContact__c;
        annualReport2.EFLRegistration__c = registration2.Id;
        annualReports.add(annualReport2);
        EFLAnnual_Report__c annualReport3 = new EFLAnnual_Report__c();
        annualReport3.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport3.EFLContact__c = annualReport1.EFLContact__c;
        annualReport3.EFLRegistration__c = registration2.Id;
        annualReports.add(annualReport3);
        List<Database.SaveResult> saveResults = Database.insert(annualReports, false);
        
        // Both annual reports should fail to insert since they are for the same fiscal year
        for(Database.SaveResult sr :saveResults) {
            System.assertEquals(false, sr.isSuccess());
        }

        // Insert one of the annual reports for registration 1 (it should fail too since registration 1 already has an annual report)
        annualReport2.EFLRegistration__c = registration1.Id;
		Database.SaveResult saveResult = Database.insert(annualReport2, false);
        System.assertEquals(false, saveResult.isSuccess());

		// Insert annual report 3 (it should be successful now that the registration has only one AR)      
		saveResult = Database.insert(annualReport3, false);
        System.assertEquals(true, saveResult.isSuccess());
        
        // Try to update a report by trying it to a registration that already has an AR (the update should fail)
        annualReport3.EFLRegistration__c = registration1.Id;
		saveResult = Database.update(annualReport3, false);
        System.assertEquals(false, saveResult.isSuccess());

        // Insert annual report 1 (it should be successful since the corresponding registration has only one AR)      
        annualReport1.EFLSites__c = 'Test Sites';
		saveResult = Database.update(annualReport1, false);
        System.assertEquals(true, saveResult.isSuccess());
    }
    
    @isTest
    public static void addARWithoutFiscalYear_FiscalYearSet() {
		EFLRegistration__c registration1 = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport1 = [Select Id, EFLAccount__c, EFLContact__c, EFLSites__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration1.Id Limit 1];

        // Create another registration
        EFLRegistration__c registration2 = new EFLRegistration__c();
        registration2.EFLAccount__c = annualReport1.EFLAccount__c;
        insert registration2;
        
        // Create an annual reports
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport.EFLContact__c = annualReport1.EFLContact__c;
        annualReport.EFLRegistration__c = registration2.Id;
        insert annualReport;

        String fiscalYear = String.valueOf(Date.today().month() >= 10 && Date.today().month() <= 12 ? Date.today().year() : Date.today().year() -1);
        
        annualReport = [Select Id, EFLFiscal_Year__c From EFLAnnual_Report__c Where Id = :annualReport.Id Limit 1];
        System.assertEquals(fiscalYear, annualReport.EFLFiscal_Year__c);
    }    
    
    @isTest
    public static void addARWithFiscalYear_FiscalYearNotOverwritten() {
		EFLRegistration__c registration1 = [Select Id, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport1 = [Select Id, EFLAccount__c, EFLContact__c, EFLSites__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration1.Id Limit 1];

        // Create another registration
        EFLRegistration__c registration2 = new EFLRegistration__c();
        registration2.EFLAccount__c = annualReport1.EFLAccount__c;
        insert registration2;
        
        // Create an annual reports
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport.EFLContact__c = annualReport1.EFLContact__c;
        annualReport.EFLRegistration__c = registration2.Id;
        annualReport.EFLFiscal_Year__c = '1969';
        insert annualReport;

        annualReport = [Select Id, EFLFiscal_Year__c From EFLAnnual_Report__c Where Id = :annualReport.Id Limit 1];
        System.assertEquals('1969', annualReport.EFLFiscal_Year__c);
    }
    
    @isTest
    public static void addARForCancelledRegWithNoARForCancellationFY_arCreated() {
		EFLRegistration__c registration = [Select Id, EFLAnnual_Report_Created__c, EFLCancellation_Date__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport1 = [Select Id, EFLAccount__c, EFLContact__c, EFLSites__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration.Id Limit 1];

        // Mark the registration as cancelled
        registration.EFLCancellation_Date__c = Datetime.newInstance(Date.today().year() -3, 5, 1, 9, 30, 0);
        update registration;
        
        // Create an annual report
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport.EFLContact__c = annualReport1.EFLContact__c;
        annualReport.EFLRegistration__c = registration.Id;
        annualReport.EFLFiscal_Year__c = String.valueOf(Date.today().year() -2);
        Database.SaveResult sr = database.insert(annualReport, false);

        System.assertEquals(true, sr.isSuccess());
    }    

    @isTest
    public static void addARForCancelledRegWithAnExistingARForCancellationFY_arNotCreated() {
		EFLRegistration__c registration = [Select Id, EFLAnnual_Report_Created__c, EFLCancellation_Date__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport1 = [Select Id, EFLAccount__c, EFLContact__c, EFLSites__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration.Id Limit 1];

        // Mark the registration as cancelled
        registration.EFLCancellation_Date__c = Datetime.now();
        update registration;
        
        // Create an annual report
        EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
        annualReport.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport.EFLContact__c = annualReport1.EFLContact__c;
        annualReport.EFLRegistration__c = registration.Id;
        Database.SaveResult sr = database.insert(annualReport, false);

        System.assertEquals(false, sr.isSuccess());
    }

    @isTest
    public static void testNewARForCancelledRegistrationWithNoARForThisFY_ARCreatedForThisFY() {
		EFLRegistration__c registration1 = [Select Id, EFLActive__c, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
		EFLAnnual_Report__c annualReport1 = [Select Id, EFLAccount__c, EFLContact__c, EFLSites__c From EFLAnnual_Report__c Where EFLRegistration__c = :registration1.Id Limit 1];

        annualReport1.EFLFiscal_Year__c = '2017';
        update annualReport1;
        
        registration1.EFLActive__c = 'N';
        update registration1;
		registration1 = [Select Id, EFLActive__c, EFLCancellation_Date__c, EFLAnnual_Report_Created__c From EFLRegistration__c Limit 1];
        System.assertNotEquals(null, registration1.EFLCancellation_Date__c);
        
        // Create a new annual report
		System.debug('The real test starts here');
        EFLAnnual_Report__c annualReport2 = new EFLAnnual_Report__c();
        annualReport2.EFLAccount__c = annualReport1.EFLAccount__c;
        annualReport2.EFLContact__c = annualReport1.EFLContact__c;
        annualReport2.EFLRegistration__c = registration1.Id;
        Database.SaveResult saveResult = Database.insert(annualReport2, false);
        
        System.assertEquals(true, saveResult.isSuccess());

        String expectedFY = String.valueOf((Date.today().month() >= 10) ? Date.today().year() : Date.today().year() -1);
		annualReport2 = [Select Id, EFLFiscal_Year__c From EFLAnnual_Report__c Where Id = :annualReport2.Id Limit 1];
        System.assertEquals(annualReport2.EFLFiscal_Year__c, expectedFY);
    }
}