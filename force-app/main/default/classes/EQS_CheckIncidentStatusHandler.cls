public class EQS_CheckIncidentStatusHandler
{
  public static void beforeCloseRFRCheck(List<Case> incidentList)
  {
        Set<Id> setIncidents = new Set<Id>();
        Map<ID, Schema.RecordTypeInfo> rtCaseMap = Schema.SObjectType.Case.getRecordTypeInfosById();
        
        for (Case incident: incidentList) 
        {
            String IncidentRTName=rtCaseMap.get(incident.RecordTypeId).getName();
            System.debug('-->:Check1'+' Status:'+incident.Status+' RecType:'+IncidentRTName);
            //System.debug('-->:'+incident);
            if(IncidentRTName == 'APHIS EQS Incident' && incident.Status == 'Closed')
            {
                //System.debug('-->:Check2');
                //setIncidents .add(incident.Id);
                List<EQS_Resource_Request__c> PedingRFRList=[Select Id,EQS_Position__r.EQS_Incident__c,Name,EQS_Responder__c,EQS_Responder_Status__c,EQS_Rotation_Begin_Date__c,EQS_Last_TDY__c from EQS_Resource_Request__c Where EQS_Position__r.EQS_Incident__c=:incident.Id AND EQS_Last_TDY__c !=null Order By EQS_Last_TDY__c Desc limit 1];
            
                if(PedingRFRList.size()>0 && PedingRFRList[0].EQS_Last_TDY__c >= Date.today() )
                 {
                   incident.addError('You cannot Close this Incident. ' + 'There are still Open/Active RFR(s) under this Incident');
                 }
            }//-----
            
         }//For loop  
   }
}