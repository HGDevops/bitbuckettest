public with sharing class EFLARPCountryOfOrigin implements EFLIActivityResultProcessor{
    
    //County of Origin Dynamic options (apex) based options
    //PQS Result to identify placeholder option to move forward to next question
    public boolean processResult(EFLPreScreeningWrapper psw){             
        try
        {
            //EFLPSQActivityReference__mdt record - PSQActivity00001
            //Country of destination currently has only one option proceeding forward to state of destination
            //SO directly setting without any need for logic  
            psw.currentQuestion.psqOptions[0].Name = psw.currentQuestion.SelectedOptionText;
            psw.currentQuestion.psqOptions[0].Value__c = psw.currentQuestion.SelectedOption;
            psw.currentQuestion.psqSelectedOption = psw.currentQuestion.psqOptions[0];
            return true;
        }
        catch(exception ex)
        {
            EFLErrorLog.createErrorLog('EFLARPCountryOfOrigin.processResult()',ex);
            return false;
        }
    }
    
    //PQS Dynamic options to move for Activity Processor - Apex based options
    public List<SelectOption> loadOptions(EFLPreScreeningWrapper psw){
        return EFLPSQUtility.getCountryOfOrigin();
    }
    
}