public with sharing class OfficialReviewRecordTriggerHandler{
    //need an static boolean so this only gets run once per trigger
    public static Boolean EMAILS_SENT = false;
    
    public static void sendCompletedReviewsEmail(List<Reviewer__c> updatedReviews){
        if (!EMAILS_SENT){
            Set<String> authIds = new Set<String>();
            for (Reviewer__c review : updatedReviews){
                authIds.add(review.Authorization__c);
            }
            
            List<AggregateResult> nonCompletedReviews = [SELECT Authorization__c, Count(Id) 
                                                         FROM Reviewer__c 
                                                         WHERE Authorization__c IN :authIds 
                                                         AND Status__c != 'Completed'
                                                         GROUP BY Authorization__c];
            
            //if there is an aggregate result there are non-completed reviews so remove that auth id - no email for it
            for (AggregateResult aggregate : nonCompletedReviews){
                authIds.remove((String)aggregate.get('Authorization__c'));
            }
            
            List<String> memberRoles = new List<String>{'BRS Biotechnologist', 'BRS Program Specialist'};
            List<Team__c> biotechnologistsToEmail = [SELECT Id, Authorization__c, Member__c, Member_Email__c 
                                                     FROM Team__c 
                                                     WHERE Authorization__c IN :authIds 
                                                     AND Member_Role__c IN :memberRoles];
            
            EmailTemplate template = [SELECT Id 
                                      FROM EmailTemplate 
                                      WHERE DeveloperName = 'EFL_State_Notification_to_Reviewer'];
            
            //need a contact id to send even though it doesnt get used, salesforce functionality
            Contact contact = [SELECT Id FROM Contact LIMIT 1];
            
            //create list of emails to send
            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            for (String authId : authIds){
                for (Team__c biotech : biotechnologistsToEmail){
                    Messaging.SingleEmailMessage newEmail = new Messaging.SingleEmailMessage();
                    newEmail.setTargetObjectId(contact.Id);
                    newEmail.setTreatTargetObjectAsRecipient(false);
                    
                    newEmail.setTemplateId(template.Id);
                    newEmail.setWhatId(authId);
                    newEmail.setToAddresses(new List<String>{biotech.Member_Email__c});
                    newEmail.setSaveAsActivity(false);
                    emails.add(newEmail);
                }
            }
            
            Messaging.sendEmail(emails);
            
            //set email sent
            EMAILS_SENT = true;
        }
    }
}