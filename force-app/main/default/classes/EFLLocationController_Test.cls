@isTest
private class EFLLocationController_Test {
   
   
   public static ac__c li;
   public static list<location__c> loclist;
   public Id lineItemID;
   public Id locationID;
   public static Contact con;
   public static Application__c app;
   public static Domain__c prog;
   public static Program_Line_Item_Pathway__c plip;
   public static Program_Prefix__c pp;
   public static Signature__c tp;
   public static Trade_Agreement__c ta;
   public static Country__c Country;
   public static Country__c USCountry;
   public static Level_1_Region__c level1;
   public static Level_2_Region__c level2;
   public static Authorizations__c auth; 
   Public static location__c loc;
   public static ac__c li2;
   public static ac__c li3;
    
   public static Material__c rm;
   public static Related_Contact__c rc;
   public static Related_Contact__c rc1;
   
   public static String VAR_RELATED_CONTACT = 'relatedContact';
   public static String VAR_RELATED_MATERIAL = 'relatedMaterial';
   public static String VAR_RELATED_GPS = 'relatedGps';
   public static String VAR_RELATED_MODE_EDIT = 'edit';
   public static String VAR_RELATED_MODE_LIST = 'list';
   public static String VAR_RELATED_MODE_CREATE = 'create';
   public static Id olocrectypeid;
   public static Id desRecId;
   public static Id locrectypeid;
   public static Id oanddlocrectypeId;
   public static Id incidentrectypeid; 
    
   static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
  @istest
  public static void insertMaterialTriggerDisabler()
  {
      if(CARPOL_UNI_DisableTrigger__c.getInstance('EFLLocationRelatedMaterialTrigger')==null)
      {
        CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLLocationRelatedMaterialTrigger'; 
        chDt.Disable__c = true; 
        insert chDt;
      }
  }
   
  @istest  
  public static void initData(){
  
      locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
      olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
      desRecId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
      oanddlocrectypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();
      incidentrectypeid =Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Incident Location').getRecordTypeId();
      
         testData.insertcustomsettingsWithBRSTriggerDisabled();
         insertMaterialTriggerDisabler(); 
         con = testData.newContact();
         app = testData.newapplication(); 
         prog = testData.newProgram('test');
         plip = testData.newCaninePathway();
         pp = testData.newPrefix();
         tp = testData.newThumbprint();
         ta = testData.newta();
         Country = testData.newcountrywithassoc();
         USCountry = testData.newcountryus();
         level1 = testData.newlevel1region(Country.id);
         level2 = testData.newlevel2region(level1.id);
         auth = testData.newAuth(app.Id);
         li = testData.newLineItemBRS('Personal Use', app);
                
         li2 = new ac__c();
         li2.id=li.id;
         li2.Movement_Type__c = 'Interstate Movement';
         li2.Type_of_Permit__c = 'Standard Permit';
         li2.Location_Status__c = 'Yet to Add';
         update li2;
      
         li3 = new ac__c();
         li3.id=li.id;
         li3.Movement_Type__c = 'Interstate Movement';
         li3.Type_of_Permit__c = 'Standard Permit';
         li3.Location_Status__c = 'Ready to Submit';
         li3.Regulated_Article_Status__c = 'Ready to Submit' ;
         li3.SOP_Status__c = 'Ready to Submit';
         li3.Construct_Status__c = 'Ready to Submit'; 
         update li3;
         
     	loc = testData.newlocation(Country.id,level1.id,level2.id,li.id,olocrectypeid);
         
         Level_1_Region__c USState = testData.newlevel1region(USCountry.id);
         Level_2_Region__c USCounty= testData.newlevel2region(USState.id);
         Location__c loc1 = testData.newlocation(USCountry.id,USState.id,USCounty.id,li.id,locrectypeid);
         Location__c loc2 = testData.newlocation(USCountry.id,USState.id,USCounty.id,li2.id,oanddlocrectypeId); 
         Location__c loc3 = testData.newlocation(USCountry.id,USState.id,USCounty.id,li3.id,desRecId); 
         Location__c loc4 = testData.newlocation(USCountry.id,USState.id,USCounty.id,li.id,incidentrectypeid); 
         Location__c objloc=new  Location__c(Name='testloc',Zip__c='92505',Country__c=loc1.Country__c,
                  state__c=loc1.state__c,Level_2_Region__c=loc1.Level_2_Region__c,GPS_1__c='GPS_1__c',
                  GPS_2__c='GPS_2__c',GPS_3__c='GPS_3__c',GPS_4__c='GPS_4__c',GPS_5__c='GPS_5__c',
                  GPS_6__c='GPS_6__c',Contact_Name1__c='Test',Primary_Contact_Last_Name__c='LName',
                  GPS_Co_ordinates_CBI__c = false,Day_Phone__c='(555) 555-1212',Line_Item__c=loc1.Line_Item__c,
                  RecordTypeId=loc1.RecordTypeId,Status__c='Waiting on Customer',Inspected_by_APHIS__c='Yes',
                  Location_Unique_Id__c='abc123');
           insert objloc;       
         
         rc = new Related_Contact__c();
         rc.Location__c = loc.id;
         rc.First_Name__c= 'abc';
         rc.Email__c= 'abc@gmail.com';
         rc.Name = 'xyz';
         rc.Phone__c = '(555) 555-1212';
         insert rc;
         
         rc1 = new Related_Contact__c();
         rc1.Location__c = loc.id;
         rc1.Country__c = USCountry.id;
         rc.Zip__c = '12345';
         rc1.First_Name__c= 'abc';
         rc1.Email__c= 'abc@gmail.com';
         rc1.Phone__c = '(555) 555-1212';
         rc1.Name = 'xyz';
         insert rc1;
         
         rc1.First_Name__c = 'priya';
         update rc1;
      
         GPS_Coordinate__c gps = new GPS_Coordinate__c();
         gps = testData.newGPSByLocation(loc.id);
      
         Material__c mat = new Material__c();
         mat = testData.newMaterial(loc.id);   
            
  }
    
   public static testMethod void AllGetMethod_Test(){
        
        initData();
        
        test.startTest();
        
          ApexPages.currentPage().getParameters().put('lineItemID',li2.id);
          
          EFLLocationController EFL = New EFLLocationController();
          EFL.recordTypeId = olocrectypeid;
          
          EFL.LineitemID = li2.id;
          Ac__c testvalue = EFL.lineItemRecord; 
          string selCon = EFL.selectedContactId;
          string selMat = EFL.selectedMaterialId;
          string isDisMat = EFL.isDisplayOtherMaterial;
          string selGPS = EFL.selectedGpsId;
          boolean testLockLine = EFL.lockLineItem;
          boolean testEnableApp = EFL.enableApplicantInstructions;
          boolean isLoc = EFL.isLocRelatedEditMode; 
          boolean isMaterial = EFL.isShowMaterialListPanel; 
          boolean isGPS = EFL.isShowGPSListPanel; 
          boolean isGPSEdit = EFL.isShowGpsEditPanel;
          boolean isRelSec = EFL.isShowReleaseSection;
          boolean dayPhone = EFL.dayphonerequired;
          boolean emailReq = EFL.emailrequired;
          boolean showLoc = EFL.showLocationView;
          string isRelease = EFL.IsandReleasemvttype;
          string desLoc = EFL.DestLoc;
          string origloc = EFL.OrigDestLoc;
          string RelLoc = efl.RelLoc;
          string testInst = EFL.instructions;
          EFL.getPageController();
          EFL.getLocationsMap();
          EFL.getIsCountryFieldRequired();
          EFL.getIsCountryFieldReadonly();
          EFL.getIsStateFieldRequired();
          EFL.getIsCountyFieldRequired();
          EFL.getIsLocationUniqueIdFieldRequired();
          EFL.getIsLocationInspectedFieldRequired();
          EFL.getFederalCBIText(); 
          EFL.getAllLocationWrappers();
          EFL.getFilteredLocationWrappers();
          EFL.movementtype = 'Release';
           EFL.getRecordTypes();
           
           EFL.searchString = 'Test';
           EFL.showAll();
           map<string,string> locMap = new map<string, string>();
           EFL.delrecid = loc.id;
           string s = loc.id;
           s = s.substring(0,3);
           locMap.put(s,'location__c');
           EFL.sobjectkeys = locMap;
           PageReference p = EFL.deleteRecord();
           EFL.locationRecord = loc; 
           PageReference p1 = EFL.deleteLocation(); 
           PageReference p2 = EFL.refreshPageSize(); 
           EFL.saveRTSelection();
           EFL.saveLocationInfo();
           PageReference p3 = EFL.cancelLocationInfo(); 
           EFL.action = 'Save';
           EFL.action();
           EFL.sobj = 'relatedGps';
           EFL.action();
           EFL.sobj = 'relatedContact';
           EFL.action = 'create';
           EFL.action();
           string s1 = loc.id;
           s1 = s1.substring(0,3);
           locMap.put(s1,'location__c');
           EFL.sobjectkeys = locMap;
           EFL.deleteRec();
           EFL.delrecid = rc.id;
                
           EFL.displayRelatedPage(VAR_RELATED_GPS,VAR_RELATED_MODE_EDIT);
           EFL.displayRelatedPage(VAR_RELATED_CONTACT, VAR_RELATED_MODE_LIST);
       
           
        test.stopTest();
    }
    
    static testMethod void testAppLineItemLocation() {
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        insertMaterialTriggerDisabler();
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticle(plip.id);
        Location__c loc= new Location__c();
        
        test.startTest();
        
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Saved');
            
            
            loc= new Location__c();
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Saved');
            
          Related_Contact__c rc = new Related_Contact__c();
            rc = testdata.newRelatedContact(loc.id);  
             
          Material__c mat = new Material__c();
            mat = testdata.newMaterial(loc.id);  
        
          GPS_Coordinate__c gps = new GPS_Coordinate__c();
            gps = testdata.newGPSByLocation(loc.id);
        
            ApexPages.currentPage().getParameters().put('lineItemID',LineItem.id);
          
            EFLLocationController EFL = New EFLLocationController();
            EFL.recordTypeId = desRecId;     
            EFL.getenableActionRequired();
           EFL.getERRMESSAGE();
          EFL.displayRTSelectionPage();
          //List<LocationWrapper> searchedLocations = new List<LocationWrapper>();
      EFL.locationID = loc.id;
            EFL.editLocation();
            EFL.setDisabledLocationId(Loc); 
          EFL.showLocationDetailView();
      EFL.searchString = 'Test';
          EFL.searchLocations();
      EFL.getRelatedContacts();
      EFL.getRelatedMaterials();
      EFL.getRelatedGpsCoordinates();
            //Contact detail
      EFL.delrecid = rc.id;
          EFL.viewContact();
          EFL.editContact();
          EFL.displayRelatedViewPage('Related_Contact__c', 'Test');
      //Material Detail;
          EFL.delrecid = mat.id;
          EFL.editMaterial();
          //GPS Detail;
          EFL.delrecid = gps.id;
          EFL.editGPS();
            LineItem.Status__c = 'Saved';
            update LineItem;
          Loc.Status__c = 'Review Complete';
          update Loc;
          EFL.setDisabledLocationId(Loc); 
          
            loc.recordtypeId = EFLGenericUtility.getRecordTypeId('Location Origin and Destination');
            update loc;          
          
        test.stopTest();
        
    }
    
    static testMethod void testMoreAppLineItemLocation() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        insertMaterialTriggerDisabler();
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticle(plip.id);
        Location__c loc= new Location__c();
        
        test.startTest();
        
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            
            loc= new Location__c();
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            
          Related_Contact__c rc = new Related_Contact__c();
            rc = testdata.newRelatedContact(loc.id);  
             
          Material__c mat = new Material__c();
            mat = testdata.newMaterial(loc.id);  
        
          GPS_Coordinate__c gps = new GPS_Coordinate__c();
            gps = testdata.newGPSByLocation(loc.id);
        
            ApexPages.currentPage().getParameters().put('lineItemID',LineItem.id);
          
            EFLLocationController EFL = new EFLLocationController();
        
          EFL.movementtype = 'Import';
          list<SelectOption> getRecordTypes = EFL.getRecordTypes();
        
          EFL.movementtype = 'Interstate Movement';
          getRecordTypes = EFL.getRecordTypes();
        
          EFL.movementtype = 'Interstate Movement and Release';
          getRecordTypes = EFL.getRecordTypes();
          
          EFL.searchString = '';
            EFL.showAll();
            EFL.searchLocations();
            EFL.lineItemID = null;
            EFL.searchLocations();
        
          EFL.lineItemID = LineItem.id;
          
          
        test.stopTest();
        
    }
    
    static testMethod void testAppLineItemLocationDelete() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        insertMaterialTriggerDisabler();
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticle(plip.id);
        Location__c loc= new Location__c();
        
        test.startTest();
        
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            
            loc= new Location__c();
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            
          Related_Contact__c rc = new Related_Contact__c();
            rc = testdata.newRelatedContact(loc.id);  
             
          Material__c mat = new Material__c();
            mat = testdata.newMaterial(loc.id);  
        
          GPS_Coordinate__c gps = new GPS_Coordinate__c();
            gps = testdata.newGPSByLocation(loc.id);
        
            ApexPages.currentPage().getParameters().put('lineItemID',LineItem.id);
          
            EFLLocationController EFL = New EFLLocationController();
            EFL.recordTypeId = desRecId;     
          EFL.locationID = loc.id;
            
          EFL.delrecid = rc.id;
          EFL.sobj = 'relatedContact';
          EFL.action = 'save';
          EFL.action();
          
          EFL.delrecid = mat.id;
            EFL.relatedMaterial = mat;
            EFL.sobj = 'relatedMaterial';
          EFL.action = 'save';
          EFL.action();
        
          EFL.delrecid = gps.id;
          EFL.sobj = 'relatedGps';
          EFL.relatedGPSCoord = gps;
          EFL.action = 'save';
          EFL.action();
        
          EFL.delrecid = rc.id;
          EFL.sobj = 'relatedContact';
          EFL.action = 'create';
          EFL.action();
          
          EFL.delrecid = mat.id;
          EFL.sobj = 'relatedMaterial';
          EFL.action = 'create';
          EFL.action();
        
          EFL.delrecid = gps.id;
          EFL.sobj = 'relatedGps';
          EFL.action = 'create';
          EFL.action();
        
          EFL.delrecid = rc.id;
          EFL.sobj = 'relatedContact';
          EFL.deleteRecord();
          
          EFL.delrecid = mat.id;
          EFL.sobj = 'relatedMaterial';
           try{EFL.deleteRecord();}catch(exception ex){}
        
          /*EFL.delrecid = gps.id;
          EFL.sobj = 'relatedGps';
          try{EFL.deleteRecord();}catch(exception ex){}*/
    }
    
    
}