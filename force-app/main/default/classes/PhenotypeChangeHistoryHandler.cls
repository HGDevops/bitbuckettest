public class PhenotypeChangeHistoryHandler implements IChangeHistoryHandler {
    
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        Schema.SObjectType sObjectType = newRecord.getSObjectType();
        if (sObjectType != null)
        {
            //record identifier
            String fieldName  = 'Phenotypic_Category__c';
            String fieldLabel = 'Phenotypic_Category';
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
                Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {
        String fieldName  = 'Phenotypic_Category__c';
        String fieldLabel = 'Phenotypic_Category';
        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.Old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
        	populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        Phenotype__c pht = (Phenotype__c)record;
        change.Construct__c = pht.Construct__c;
        change.Line_Item__c = pht.line_item__c;
        if(!change.Delete_Flag__c)change.Phenotype__c = pht.id;
        
    }

}