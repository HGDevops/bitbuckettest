// Test Class for EFLRandomInspectionHandler class
// Created By - Rajesh Potla 
// Date - 5/7/2019
@isTest
private class EFLRandomInspectionHandler_Test {
   
    private static void init(){
        List<EFL_Random_Inspection_Prefix__c> PrefixesToInsert = new List<EFL_Random_Inspection_Prefix__c>();
        Map<string,string> URLMap = new Map<string,string>{'555'=>'Organisms and Vectors','556'=>'Organisms and Vectors', '557'=>'Organisms and Vectors'};
        for(string x : URLMap.keyset()){
        EFL_Random_Inspection_Prefix__c cu = EFL_Random_Inspection_Prefix__c.getValues(x);
        if(cu == null){
            cu = new EFL_Random_Inspection_Prefix__c(name = x,EFL_Program_Pathway_Name__c  = URLMap.get(x));
            PrefixesToInsert.add(cu);
        }
        }
        insert PrefixesToInsert;
        
        Domain__c Program = new Domain__c(Name = 'VS');
        insert Program;
        
        Program_Prefix__c programPrefix = new Program_Prefix__c(Name = '555',Program__c = Program.id);
        insert programPrefix;
        
        Signature__c TP = new Signature__c();
        TP.Name = 'VS TP';
        TP.Program_Prefix__c = programPrefix.Id;
        insert TP;
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Application__c oApp = testData.newapplication();
        List<Authorizations__c> auths = new List<Authorizations__c>();
        auths.add(new Authorizations__c(workflow_Number__c='150',Status__c='Issued',Thumbprint__c = TP.id,Application__c = oApp.Id));
        auths.add(new Authorizations__c(Status__c='Issued',Thumbprint__c = TP.id,Application__c = oApp.Id));
        auths.add(new Authorizations__c(Status__c='Issued',Thumbprint__c = TP.id,Application__c = oApp.Id));
        insert auths;
       
    }
    
    @isTest
    static void runTestScneraioForException(){
        Test.startTest();
        List<EFL_Random_Inspection__c> randomInspectionsList = new List<EFL_Random_Inspection__c>();
       	randomInspectionsList.add(new EFL_Random_Inspection__c(Prefix_Number__c=555,Percentage_of_defined_population__c=99,Description__c='Test'));
        randomInspectionsList.add(new EFL_Random_Inspection__c(Prefix_Number__c=556,Percentage_of_defined_population__c=99,Description__c='Test'));
        randomInspectionsList.add(new EFL_Random_Inspection__c(Prefix_Number__c=557,Percentage_of_defined_population__c=99,Description__c='Test'));
       	EFLRandomInspectionHandler.createRandomInspectionRecord(randomInspectionsList);
        
        EFLRandomInspectionHandler.createRandomInspectionRecord(randomInspectionsList);
        Test.stopTest();
    }
    
    @isTest
    static void runTestScneraio1(){
        init();
        List<EFL_Random_Inspection__c> randomInspectionsList = new List<EFL_Random_Inspection__c>();
       	randomInspectionsList.add(new EFL_Random_Inspection__c(Prefix_Number__c=555,Percentage_of_defined_population__c=99,Description__c='Test'));
        randomInspectionsList.add(new EFL_Random_Inspection__c(Prefix_Number__c=556,Percentage_of_defined_population__c=99,Description__c='Test'));
        randomInspectionsList.add(new EFL_Random_Inspection__c(Prefix_Number__c=557,Percentage_of_defined_population__c=99,Description__c='Test'));
       	
        try{
            
        EFLRandomInspectionHandler.createRandomInspectionRecord(randomInspectionsList);
        }catch (Exception e){
            
        }
    }

}