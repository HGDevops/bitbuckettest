// Test Class for EFLRandomInspectionHandler class
// Created By - Rajesh Potla 
// Date - 5/7/2019
@isTest
private class EFLBRSInspQuestionnaireQnsEngine_Test {

    @testsetup
    static void setupData(){
        EFL_Inspection_Questions_Template__c inspTmpn = new EFL_Inspection_Questions_Template__c();
         inspTmpn.Name='test';
         insert inspTmpn;
        
        Inspection__c Insn = new Inspection__c();
        String IncRecTypeIDn = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Insn.RecordTypeId = IncRecTypeIDn;
        Insn.Stage__c = 'Inspection Assignment';
        Insn.status__c='Cancelled';
        Insn.Reason_for_Cancellation__c = 'testing';
        Insn.Program__c='BRS';
        Insn.Activity_Sequence__c = 0;
        Insn.EFL_Inspection_Questionnaire_Template__c=inspTmpn.id;
        insert Insn;
    }
    
    @isTest
    static void runTest(){
        Inspection__c oInspection = [Select Id From Inspection__c limit 1];
        EFL_Inspection_Questionnaire_Questions__c recEIQ=new EFL_Inspection_Questionnaire_Questions__c(
            Inspection__c=oInspection.id,Question__c='test question1',Response__c = 'response1',
            Answer__c='answer',Options__c='Finalize Questionnaire',Comments__c ='c-test');
        List<EFL_Inspection_Questionnaire_Questions__c> questionsList = new List<EFL_Inspection_Questionnaire_Questions__c>();
        questionsList.add (new EFL_Inspection_Questionnaire_Questions__c(Inspection__c=oInspection.id,Question__c='test question1',Response__c = 'response1',
            Answer__c='answer',Options__c='Finalize Questionnaire',Comments__c ='c-test'));
        questionsList.add (new EFL_Inspection_Questionnaire_Questions__c(Inspection__c=oInspection.id,Question__c='test question2',Response__c = 'response2',Answer__c='answer2',Options__c='Finalize Questionnaire',Comments__c ='c-test2'));
        questionsList.add(recEIQ);
        
        Set<Id> inspectionIdSet = new Set<Id>{oInspection.id};
        
        Test.startTest();
        EFLBRSInspQuestionnaireQnsEngine oEngine = new EFLBRSInspQuestionnaireQnsEngine();
        EFLBRSInspQuestionnaireQnsEngine.reorderInspectionQuestions(questionsList);
        
        EFLBRSInspQuestionnaireQnsEngine.inspectionQuestionCountMap = new map<id, integer>();
        EFLBRSInspQuestionnaireQnsEngine.inspectionQuestionCountMap.put(oInspection.id,0);
        EFLBRSInspQuestionnaireQnsEngine.setOrder(recEIQ);
        
        EFLBRSInspQuestionnaireQnsEngine.fetchQnCountForInspections(inspectionIdSet);
        EFLBRSInspQuestionnaireQnsEngine.fetchQnsForReorder(inspectionIdSet);
       	Test.stopTest();
    }
}