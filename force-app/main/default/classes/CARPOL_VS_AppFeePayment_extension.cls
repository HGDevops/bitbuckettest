/**
* Author :  
* Created Date : 01/05/2016
* Purpose :  Payment processing logic
*/
public with sharing class CARPOL_VS_AppFeePayment_extension {
    
    public String paymentType{get;set;}
    public String APHISUserFeeAccountNumber {get;set;}
    public Boolean showUserFeeAccountNumber { get; set; }
    public Boolean showMailInCheck {get;set;}
    public Boolean showMoneyOrder {get;set;}
    public ID applicationID;
    Public Application__c app;
    public Application__c application { get; set; }
    public List<AC__c> lineItems { get; set; }
    public List<Fee__c> ItemFee { get; set; }
    public List<Related_Fees__c> relatedFees { get; set; }
    public List<Regulations_Association_Matrix__c> regDm { get; set; }
    public Map<String, List<ItemFee>> thumbPrintItems {get; set;}
    public Map<String, Decimal> permitTotals {get; set;}
    public Decimal totalAmount {get; set; }
    public String strToken;
    public boolean runThis;
    public Boolean isEsigned {get; set;}
    public String checkNumber {get; set;}
    public Boolean checkNumEntered {get; set;}
    public String Program {get;set;}
    public Boolean acceptPayment {get;set;}
    
    private ApexPages.StandardController appController;
    integer StopQuery = 0;
    
    public CARPOL_VS_AppFeePayment_extension(ApexPages.StandardController controller) 
    {
        
        appController = controller;
        this.app = (Application__c)controller.getRecord();
        
        //instantiate variables
        ItemFee = new List<Fee__c>();
        isEsigned = false;
        thumbPrintItems = new Map<String, List<ItemFee>>();//This map will be used to group and fetch Line Item data
        permitTotals = new Map<String, Decimal>();
        totalAmount = 0.0;
        
        // jb: commenting out parameter.get & using StandardController instead.
        //applicationID = ApexPages.CurrentPage().getparameters().get('id');
        applicationID = controller.getId();
        strToken = EFLGenericUtility.sanitizeString( ApexPages.CurrentPage().getparameters().get('token') );
        showUserFeeAccountNumber = false;
        showMailInCheck = false;
        showMoneyOrder = false;
        runthis = true;
        
    }
    
    public class ItemFee{
        
        public AC__c lineItem {get;set;}
        public String fee {get;set;}
        public Signature__c sig {get;set;}
        
        public ItemFee(AC__c li, String d, Signature__c s){
            lineItem = li;
            fee = d;
            sig = s;
        }
        
    }
    
    public PageReference computePaymentSummary()
    {
        //System.debug('step 1');
        if(applicationID != null)
        {
            //System.debug('step 2');
            lineItems = [SELECT ID, Name, Total_Fees__c, Authorization_Group_String__c,Program_Line_Item_Pathway__r.Name,Program_Line_Item_Pathway__r.Program__r.Name,
                         Transporter_Type__c, Item_Fee__c, RecordType.Name, Thumbprint__c, Thumbprint__r.Fee_Amount__c FROM AC__C 
                         WHERE Application_Number__c =: applicationID and Line_Item_Type_Hidden_Flag__c != 'Ingredient' ];//query line items for desired data
            if(lineItems == null || lineItems.size() == 0)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Line items associated with this Application.'));
            }
            else
            {
                Set<String> authStrings = new Set<String>();
                for(AC__c item: lineItems)
                {
                    authStrings.add(item.Authorization_Group_String__c); //the grouping criteria for the permits
                }
                
                //System.debug('Line Item size' + lineItems.size());
                ///////////////////////////////////////////////////////////
                // added 5/8/2019 by JB: moving SOQL outside of for-loop
                list<Id> thumbPrintIds = new list<Id>();
                
                for(AC__c item: lineItems){
                    thumbPrintIds.add(item.Thumbprint__c);
                }
                map<Id, Signature__c> acThumbprintMap = new map<Id, Signature__c>([select id, Fee_Amount__c from Signature__c where id IN :thumbPrintIds]);
                // END OF ADD
                for (String auth: authStrings) 
                {
                    //System.debug('>>> auth String ' + auth);
                    List<ItemFee> tempItems = new List<ItemFee>();
                    Decimal tempFees = 0.00;
                    Signature__c thumbPrint = new Signature__c();
                    Integer counter = 0;
                    
                    
                    for(AC__c item: lineItems) //create a list of line Items for each auth grouping
                    {
                        if(counter < 1){ //only need to set it once
                            Program = item.Program_Line_Item_Pathway__r.Program__r.Name;
                            counter++;
                        }
                        
                        if(item.Authorization_Group_String__c == auth && item.Thumbprint__c != null)
                        {
                            // added 5/8/2019 by JB
                            thumbPrint = acThumbprintMap.get(item.Thumbprint__c);
                            //thumbPrint.id = item.Thumbprint__c;
                            //thumbPrint = [select id, Fee_Amount__c from Signature__c where id =: thumbPrint.id];
                            //END OF ADD
                            tempItems.add(new ItemFee(item, String.ValueOf(item.Thumbprint__r.Fee_Amount__c), thumbPrint));
                            tempFees = item.Thumbprint__r.Fee_Amount__c;
                            
                            //System.debug('>>Item Fee size ' + tempItems.size());
                            //System.debug('Line Item id ' + item);
                            //System.debug('>>> Values in the ItemFee List '+ tempItems);
                        }
                    }
                    
                    tempFees.setScale(2);
                    thumbPrintItems.put(auth, tempItems); //Add each grouping into the map
                    permitTotals.put(auth, tempFees);
                    totalAmount += tempFees;
                    //System.debug('>>>> print item size '+ thumbPrintItems.size());
                    //System.debug('These are values in the map' + thumbPrintItems);
                }
                
                totalAmount = totalAmount.setScale(2);
                
                if(totalAmount == 0)
                    return processAuthorization();
            }
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No valid Application ID found.'));
        }
        
        return null;
    }
    
    public PageReference processAuthorization()
    {
        PageReference appProcessing = new PageReference('/apex/UpdateApplication?id=' + applicationID);
        appProcessing.setRedirect(true);
        return appProcessing;
    }
    
    public PageReference processPayGov()
    {
        if(paymentType == 'Electronically (pay.gov)')
        {
            try
            {
                CARPOL_ProcessPaymentPayGov temp = new CARPOL_ProcessPaymentPayGov();
                // if(totalAmount != 0)
                // {
                if(applicationID != null)
                    //if(tran.ID != null)
                {
                    
                    if (runthis) {
                        strToken = temp.startOnlineCollection(String.valueOf(totalAmount), applicationID);
                    }
                    if(strToken != 'NoToken' && strToken != 'Error')
                    {
                        //System.debug('strToken is noToken >>>>>>>>>>>>>>');
                        //Create Transaction instance for the process and Pay.gov related info
                        Transaction__c tran = new Transaction__c();
                        tran.Application__c = applicationID;
                        tran.Transaction_Amount__c = totalAmount;
                        tran.Transaction_Date_Time__c = System.NOW();
                        tran.Payment_Type__c = 'Pay.gov';
                        tran.Pay_Gov_Token__c = strToken;
                        insert tran; //insert this data into the application
                        //System.debug('transaction object has been creation >>>>>>');
                        PageReference payGovRedirect = new PageReference('https://qa.pay.gov/tcsonline/payment.do?token=' + strToken + '&tcsAppID=TCSAPHISEFILE');
                        payGovRedirect.setRedirect(true);
                        //return strToken;
                        return payGovRedirect;
                    }
                    //if token is not generated
                    else
                    {
                        PageReference payGovRedirectSomethingWentWrong = new PageReference('/apex/CARPOL_SomethingWentWrong');
                        payGovRedirectSomethingWentWrong.setRedirect(true);
                        return payGovRedirectSomethingWentWrong;
                    }
                }
                else
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Transaction was not created.'));
                    return null;
                }
                
            }
            catch(Exception e)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception : ' + e));
                return null;       
            }
        }
        else if(paymentType == 'Mail-in Check/Money Order')
        {
            Transaction__c tran = new Transaction__c();
            tran.Application__c = applicationID;
            tran.Transaction_Amount__c = totalAmount;
            tran.Transaction_Date_Time__c = System.NOW();
            tran.Payment_Type__c = 'Check/Money Order';
            tran.Check_Number__c = checkNumber;
            insert tran; //insert this data into the application
            PageReference appProcessing = new PageReference('/apex/UpdateApplication?id=' + applicationID);
            appProcessing.setRedirect(true);
            return appProcessing;
        }
        else if(paymentType == 'APHIS Credit Account')
        {
            PageReference ref = new PageReference('/apex/CARPOL_VS_AphisAccount?Id=' + applicationID + '&amount=' + totalAmount);
            ref.setRedirect(true);
            return ref;
        }
        else
            return null;
        
    }
    
    public List<SelectOption> getPaymentTypeOptions() 
    {
        
        List<SelectOption> paymentTypes = new List<SelectOption>();
        
        // paymentTypes.add(new SelectOption('Mail-in Check/Money Order', 'Mail-in Check/Money Order'));
        // paymentTypes.add(new SelectOption('APHIS Credit Account', 'APHIS Credit Account'));
        // paymentTypes.add(new SelectOption('Electronically (pay.gov)', 'Electronically (pay.gov)'));
        if(Program == 'VS'){
            paymentTypes.add(new SelectOption('Electronically (pay.gov)', 'Electronically (pay.gov)'));
            paymentTypes.add(new SelectOption('Mail-in Check/Money Order', 'Mail-in Check/Money Order'));
            paymentTypes.add(new SelectOption('APHIS Credit Account', 'APHIS Credit Account')); //APHIS Credit Account
        }
        
        
        else if(Program == 'PPQ' && lineItems[0].Program_Line_Item_Pathway__r.Name == 'Protected Plant Permit'){
            paymentTypes.add(new SelectOption('Electronically (pay.gov)', 'Electronically (pay.gov)'));
            paymentTypes.add(new SelectOption('Mail-in Check/Money Order', 'Mail-in Check/Money Order'));
        }
        
        
        else{
            paymentTypes.add(new SelectOption('Electronically (pay.gov)', 'Electronically (pay.gov)'));
        }
        
        
        return paymentTypes;
    }
    
    public PageReference doCancel()
    {
        return appController.cancel();
    }
    
    public void isCheckNumEntered(){
        if(checkNumber == null || checkNumber == '')
            checkNumEntered = false;
        else
        {
            if(acceptPayment)
                checkNumEntered = true;
            else
                checkNumEntered = false;
        }
        
    }
    
    public PageReference paymentTypeChangeAction()
    {
        
        if(paymentType == 'Mail-in Check/Money Order')
        {
            showMailInCheck = true;
        }
        else if (paymentType != 'Mail-in Check/Money Order')
        {
            showMailInCheck = false;
        }
        
        
        return null;
    }
    
}