@istest
public class EFLAccountRepository_test{
public static testmethod void EFLAccountRepository_test(){   
        //Creating Account test data                
       // Id RTId = [select id From recordtype where name = 'Accounts'].Id; 
    string AccountRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
        Account objacct = new Account();
        objacct.Name = 'Test Account';
       // objacct.RecordTypeId = RTId;   
        objacct.RecordTypeId = AccountRecordTypeId;          
        insert objacct; 
        system.debug('objacct='+objacct);
        
        //Creating Contact test data
        Contact objCont = new Contact();
        objCont.FirstName = 'Global Contact';
        objcont.LastName = 'LastName';
        objcont.Email = 'test@email.com';
        objcont.AccountId = objacct.id;        
        objcont.MailingStreet = 'Mailing Street';
        objcont.MailingCity = 'Mailing City';
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '32092';    
        insert objcont; 
        system.debug('objcont='+objcont);
        //Creating User test data with contactID as Contact test data id
        
       /* Profile prof = [Select Id from Profile WHERE Name IN ('APHIS Applicant','eFile Applicant')Limit 1];
        User custusr = new User(Alias='stndt',email='testUser@gmail.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                       LocaleSidKey='en_US', ProfileId = prof.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduserqwert@testorg.com',
                       ContactID=objcont.Id);
        insert custusr; */
        String uniqueUserName = 'standarduser' +  EFLGenericUtility.randomString() + '@testaphis.com';
        Profile p = [SELECT Id FROM Profile WHERE Name='APHIS Standard Platform User'];
        User u = new User(Alias = 'standt', Email='standarduser@testaphis.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        System.runAs(u){
        Test.startTest();
        try{EFLAccountRepository.selectActiveUserAccountsByCurrentUserDirectAccount();}catch(system.exception e){
        EFLErrorLog.createErrorLog('EFLAccountRepository_test.EFLAccountRepository_test()',e); }
         
        test.stoptest();
        }
         }
         }