/**
* Class containing tests for CARPOL_UNI_CBPScheduable class
*/
@isTest(seealldata=false)
private class CARPOL_UNI_CBPScheduable_Test {

    @IsTest static void testCARPOL_UNI_CBPScheduable() {
        CARPOL_UNI_CBPScheduable cbpSched = new CARPOL_UNI_CBPScheduable();
		String schedMon5AM = '0 00 05 ? * 2'; 

        Test.startTest();
        String jobId = System.schedule('CARPOL_UNI_CBPScheduable - Monday 5AM', schedMon5AM, cbpSched);
        Test.stopTest();

        System.assert(jobId != null);
    }
}