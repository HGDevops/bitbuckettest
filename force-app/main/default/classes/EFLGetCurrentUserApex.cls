public class EFLGetCurrentUserApex {
	@AuraEnabled
    public static contact getConInfo(){
     Id conId = [select id,contactId from user where Id=:Userinfo.getUserId()].contactId; 
     if(!Test.isRunningTest())
     	return  [select Id,firstName,lastName,account.name from contact where Id=:conId];  
     else
        return null;
    } 
}