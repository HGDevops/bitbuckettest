public class EFLConstructWrapper {

    public Boolean checked{ get; set; }
    public construct__c constructRecord { get; set;}
    public string PhenoCategory {get;set;}
    public Construct_Application_Junction__c subConstructRecord {get;set;}
    public EFLConstructWrapper(){
        constructRecord = new construct__c();
        subConstructRecord = new Construct_Application_Junction__c();
        checked = false;
    }
     
}