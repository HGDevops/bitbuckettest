public class GenotypeChangeHistoryHandler implements IChangeHistoryHandler {
    
    static final string fieldName = 'Component_Name__c';
    static final string fieldLabel = 'Component Name';
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        Schema.SObjectType sObjectType = newRecord.getSObjectType();

        if (sObjectType != null)
        {
            //record identifier
            system.debug('newRecord>>>>>'+newRecord);
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
                Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {

        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.Old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
        	populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        Genotype__c gnt = (Genotype__c)record;
        change.Construct__c = gnt.Related_Construct_Record_Number__c; 
        change.line_Item__c = gnt.line_Item__c; 
		if(!change.Delete_Flag__c)change.Genotype__c = gnt.id;        
    }

}