@isTest
public class EFLAuthorizationActivityWrapper_Tests {

    @isTest
    static void codeCoveragePlease(){
        CARPOL_BRS_TestDataManager dm = new CARPOL_BRS_TestDataManager();
        dm.insertcustomsettings();
        Application__c app = dm.newapplication();
        Authorizations__c auth = dm.newAuth(app.Id);
        Task t = new Task();
        t.WhatId = auth.Id;
        insert t;
        list<Task> tasks = new list<Task>();
        tasks.add(t);
        test.startTest();
        EFLAuthorizationActivityWrapper meh = new EFLAuthorizationActivityWrapper();
		meh.authRecord = auth;
        meh.newTasks = tasks;
        test.stopTest();
    }
}