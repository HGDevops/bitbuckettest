@isTest(SeeAllData=false)
private class EFLBarcodeDataController_Test{
    static testMethod void UnitTest() {
        EFLBarcodeDataController barcode= new EFLBarcodeDataController();
        barcode.barcodeText = 'abcdefghijklmnopqrstuvwxyz1234567890';
        barcode.barcodeType = 'CODE128';
        Test.startTest();
        barcode.getBarcodeHTML();
        system.assert(barcode.barcodeText == 'abcdefghijklmnopqrstuvwxyz1234567890');
        barcode.barcodeType='CODE39';
        barcode.getBarcodeHTML();
        //this type doesn't exist, testing generator error code
        barcode.barcodetype='CODE30';
        barcode.getBarcodeHTML();
        Test.stopTest();
    }
}