/*
 * Purpose: Central place to access Line Item object from database with different set of filter parameters.
*/
public with sharing class EFLLineItemRepository {

    //Get line item details for set of line item Ids
    public static list<AC__c> selectbyIDs(set<id> lineItemIDs){
 
          list<AC__c> lineItemlist;
        if(lineItemlist==null){
          try{
             EFLEnforceAccessUtility.checkObjectReadAccess('AC__c');
             lineItemlist = [SELECT AC_Manager_Comments__c,AC_Unique_Id__c,Additional_Information__c,Age_in_months__c,Age_yr_months__c,Read_Only__c,
                                    Air_Transporter_Flight_Number__c,Applicant_Instructions__c,Application_Duration__c,Application_Number__c,
                                    Arrival_Date_and_Time__c,Arrival_Time__c,Authorization_Content__c,Authorization__c,Type_of_Permit__c,Do_you_want_to_use_a_previously_approved__c, 
                                    Biological_Material_present_in_Article__c,Breed_Description__c,Breed_Name__c,Breed__c,Is_your_regulated_article_a_plant__c,
                                    CBI_Justification__c,Clerk_Comments__c,Color__c,Communication_Letter__c,Contains_Applicant_Attachments__c,
                                    Country_and_Locality_Information__c,Country_Of_Origin__c,Courtesy_Justification__c,CreatedById,Are_you_applying_for_Variance__c,
                                    CreatedDate,Date_of_Birth__c,DeliveryRecipient_Email__c,DeliveryRecipient_Fax__c,DeliveryRecipient_First_Name__c,
                                    DeliveryRecipient_Last_Name__c,DeliveryRecipient_Mailing_City__c,DeliveryRecipient_Mailing_CountryLU__c,How_will_variance_be_used__c,
                                    DeliveryRecipient_Mailing_Street__c,DeliveryRecipient_Mailing_Zip__c,DeliveryRecipient_Phone__c,What_is_the_previously_reviewed_variance__c,
                                    DeliveryRecipient_USDA_License_Number__c,DeliveryRecipient_USDA_Registration_Num__c,DeliveryRec_USDA_License_Expiration_Date__c,
                                    DeliveryRec_USDA_Regist_Expiration_Date__c,Delivery_Recipient_State_ProvinceLU__c,Deny_Resubmit__c,
                                    Departure_Date_and_Time__c,Departure_Time__c,Does_This_Application_Contain_CBI__c,Domain__c,Enforcement_Officer_Comments__c,
                                    Exporter_Email__c,Exporter_Fax__c,Exporter_First_Name__c,Exporter_Last_Name__c,Exporter_Mailing_City__c,
                                    Exporter_Mailing_CountryLU__c,Exporter_Mailing_State_ProvinceLU__c,movement_type__c,Exporter_Mailing_Street__c,
                                    Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,F_Importer_First_Name__c,Hand_Carry__c,Health_Certificate_Attached__c,
                                    Id,If_yes_Please_Describe__c,Importer_Email__c,Importer_Fax__c,Importer_First_Name__c,Importer_Last_Name__c,
                                    Importer_Mailing_City__c,Importer_Mailing_CountryLU__c,Importer_Mailing_State_ProvinceLU__c,Importer_Mailing_Street__c,
                                    Importer_Mailing_ZIP_code__c,Importer_Phone__c,Importer_USDA_License_Expiration_Date__c,Importer_USDA_License_Number__c,
                                    Importer_USDA_Registration_Exp_Date__c,Importer_USDA_Registration_Number__c,Intended_Use__c,Introduction_Type__c,
                                    IsDeleted,Is_violator__c,LastModifiedById,Locked__c,
                                    Means_of_Movement__c,Microchip_Number__c,Move_out_Hawaii__c,Name,Number_of_Labels__c,Number_of_New_Design_Protocols__c,
                                    Number_of_Release_Sites__c,Other_identifying_information__c,OwnerId,Picture__c,Port_of_Embarkation__c,Port_of_Entry_State__c,Port_of_Entry__c,Processed_anyway_that_change_its_nature__c,Program_Line_Item_Pathway__c,Proposed_date_of_arrival__c,Proposed_End_Date__c,Proposed_Start_Date__c,Purpose_of_Permit__c,Purpose_of_the_Importation__c,Quarantine_Facility__c,Rabies_Vaccination_Certificate_Attached__c,RecordTypeId,RecordType.Name,Rejected_Date__c,Renew_Authorization__c,Reviewer_Comments__c,Seed_be_planted_or_propagated__c,Sex__c,Status__c,Street_Address__c,SystemModstamp,Tattoo_Number__c,Test_Field__c,Thumbprint__c,
                                    Time_Zone_of_Arriving_Place__c,Time_Zone_of_Departing_Place__c,Transporter_Name__c,Transporter_Type__c,Treatment_available_in_Country_of_Origin__c,USDA_Expiration_Date__c,USDA_License_Expiration_Date__c,USDA_License_Number_Format__c,USDA_License_Number__c,USDA_Registration_Number_Format__c,USDA_Registration_Number__c,Vet_Agreement_Attached__c,Will_the_Seed_be_processed_in_any_way_th__c 
                               FROM AC__c 
                              WHERE ID IN:lineItemIDs];
            return lineItemlist;
            }catch(exception e){
                          EFLErrorLog.createErrorLog('EFLLineItemRepository.selectbyIDs()',e);                
            } 
        }
        return null;
     } 

    //Get line item details for an line item ID
    public static AC__c selectbyID(id lineItemID){
       AC__c lineItemDetails;
       if(lineItemDetails==null){
          try{
             EFLEnforceAccessUtility.checkObjectReadAccess('AC__c');
              lineItemDetails = [SELECT owner.name,authorization__r.status__c,AC_Manager_Comments__c,Agree__c,AC_Unique_Id__c,Additional_Information__c,Age_in_months__c,Age_yr_months__c,Read_Only__c, 
                                        Air_Transporter_Flight_Number__c,Applicant_Instructions__c,Application_Duration__c,Application_Number__c,Application_Number__r.Name, 
                                        Arrival_Date_and_Time__c,Arrival_Time__c,Authorization_Content__c,Authorization__c,Type_of_Permit__c,Do_you_want_to_use_a_previously_approved__c ,
                                        Biological_Material_present_in_Article__c,Breed_Description__c,Breed_Name__c,Breed__c,Is_your_regulated_article_a_plant__c,
                                        CBI_Justification__c,Clerk_Comments__c,Color__c,Communication_Letter__c,Contains_Applicant_Attachments__c,Regulated_Article_Status__c,location_status__c,SOP_Status__c,Construct_Status__c,
                                        Country_and_Locality_Information__c,Country_Of_Origin__c,Courtesy_Justification__c,CreatedById,Are_you_applying_for_Variance__c,
                                        CreatedDate,Date_of_Birth__c,DeliveryRecipient_Email__c,DeliveryRecipient_Fax__c,DeliveryRecipient_First_Name__c,Application_Number__r.Applicant_Name__r.Name,
                                        DeliveryRecipient_Last_Name__c,DeliveryRecipient_Mailing_City__c,DeliveryRecipient_Mailing_CountryLU__c,How_will_variance_be_used__c,
                                        DeliveryRecipient_Mailing_Street__c,DeliveryRecipient_Mailing_Zip__c,DeliveryRecipient_Phone__c,What_is_the_previously_reviewed_variance__c,
                                        DeliveryRecipient_USDA_License_Number__c,DeliveryRecipient_USDA_Registration_Num__c,DeliveryRec_USDA_License_Expiration_Date__c,
                                        DeliveryRec_USDA_Regist_Expiration_Date__c,Delivery_Recipient_State_ProvinceLU__c,Deny_Resubmit__c,thumbprint__r.Name,Thumbprint__r.REF_Program_Name__c,
                                        Departure_Date_and_Time__c,Departure_Time__c,Does_This_Application_Contain_CBI__c,Domain__c,Enforcement_Officer_Comments__c,
                                        Exporter_Email__c,Exporter_Fax__c,Exporter_First_Name__c,Exporter_Last_Name__c,Exporter_Mailing_City__c,
                                        Exporter_Mailing_CountryLU__c,Exporter_Mailing_State_ProvinceLU__c,movement_type__c,Exporter_Mailing_Street__c,
                                        Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,F_Importer_First_Name__c,Hand_Carry__c,Health_Certificate_Attached__c,
                                        Id,If_yes_Please_Describe__c,Importer_Email__c,Importer_Fax__c,Importer_First_Name__c,Importer_Last_Name__c,
                                        Importer_Mailing_City__c,Importer_Mailing_CountryLU__c,Importer_Mailing_State_ProvinceLU__c,Importer_Mailing_Street__c,
                                        Importer_Mailing_ZIP_code__c,Importer_Phone__c,Importer_USDA_License_Expiration_Date__c,Importer_USDA_License_Number__c,
                                        Importer_USDA_Registration_Exp_Date__c,Importer_USDA_Registration_Number__c,Intended_Use__c,Introduction_Type__c,
                                        IsDeleted,Is_violator__c,Locked__c, Certification__c, Application_Number__r.Application_Type__c, Amendment_Description__c,Renewal_Description__c,
                                        Means_of_Movement__c,Microchip_Number__c,Move_out_Hawaii__c,Name,Number_of_Labels__c,Number_of_New_Design_Protocols__c,
                                        Number_of_Release_Sites__c,Other_identifying_information__c,OwnerId,Program_Line_Item_Pathway__r.Name,Picture__c,Port_of_Embarkation__c,Port_of_Entry_State__c,Port_of_Entry__c,Processed_anyway_that_change_its_nature__c,Program_Line_Item_Pathway__c,Proposed_date_of_arrival__c,Proposed_End_Date__c,Proposed_Start_Date__c,Purpose_of_Permit__c,Purpose_of_the_Importation__c,Quarantine_Facility__c,Rabies_Vaccination_Certificate_Attached__c,RecordTypeId,RecordType.Name,Rejected_Date__c,Renew_Authorization__c,Reviewer_Comments__c,Seed_be_planted_or_propagated__c,Sex__c,Status__c,Street_Address__c,SystemModstamp,Tattoo_Number__c,Test_Field__c,Thumbprint__c,
                                        Time_Zone_of_Arriving_Place__c,Time_Zone_of_Departing_Place__c,Transporter_Name__c,Transporter_Type__c,Treatment_available_in_Country_of_Origin__c,USDA_Expiration_Date__c,USDA_License_Expiration_Date__c,USDA_License_Number_Format__c,USDA_License_Number__c,USDA_Registration_Number_Format__c,USDA_Registration_Number__c,Vet_Agreement_Attached__c,Application_Details_Status__c,Will_the_Seed_be_processed_in_any_way_th__c, Attachment_Status__c, SOP_Count__c,
                                        Related_Contact_Status__c, Document_Status__c,SpringCM_CBI_Flag__c, Sharing_Account__c
                                   FROM AC__c 
                                  WHERE ID =:lineItemID 
                                 limit 1
                                 ]; 
                 return lineItemDetails;
            }catch(exception e){
                          EFLErrorLog.createErrorLog('EFLLineItemRepository.selectbyID()',e);                
            }     
        }
        return null;
     } 
    
    //Get line item details by Application ID and Used for ***CLONING Application ONLY***
    public static list<AC__c> selectbyApplicationID(id applicationID){
 
       list<AC__c> lineItemlist;
       if(lineItemlist==null){
          try{
             EFLEnforceAccessUtility.checkObjectReadAccess('AC__c');
              lineItemlist = [SELECT AC_Arrival_Time__c  , AC_Departure_Time__c, AC_Manager_Comments__c, AC_Unique_Id__c, Acknowledge__c, 
                                      Addition_Material_with_article_Other_CBI__c, Additional_Information__c, Additional_Information_for_Cell_Cultures__c, 
                              Additional_material_CBI__c, Additional_material_with_article__c, Additional_material_with_article_CBI__c, 
                              Additional_Material_with_article_Other__c, Age__c, Age_in_months__c, Age_yr_months__c, Air_Transporter_Flight_Number__c, 
                              Airway_Bill_Number__c, Animal_Name__c, Animal_Product_Safety_Test_Required__c, Animal_Use__c, animals_exposed_to_the_imported_material__c, 
                              Antigens_Genetic_Inserts__c, any_derivative_of_the_imported_material__c, Applicant_Instructions__c, Applicant_Reference_Number__c, 
                              Application_Duration__c, Approved_Facility__c, Approved_Facility_CBI__c, Approved_Name_Description__c, Are_you_applying_for_Variance__c, 
                              Arrival_Date_and_Time__c, Arrival_Time__c, Article_for_Commercial_or_Personal_Use__c, Authorization__c, Authorization_Content__c, 
                              Authorization_Group_String__c, Authorization_Text__c, Available_Capabilities__c, Bill_of_Lading__c, 
                              Biological_Material_present_in_Article__c, Brand__c, Breed__c, Breed_Description__c, Breed_Name__c, Canada_CFIA_Certificate_Available__c, 
                              Capability_Name__c, Category_Type__c, CBI_Justification__c, Cell_Cultures__c, CEM_Affected_Region_Last_12_Months__c, Certification__c, 
                              CITES_Needed__c, Clerk_Comments__c, Cloned_Line_Item__c, Color__c, Commercial_or_Non_Commercial_Use__c, Commercial_Process_List__c, 
                              Commercial_Sub_Option__c, Commercial_Type__c, Commodity_Information_CBI__c, Common_Name__c, Communication_Letter__c, Company_Name__c, 
                              Completed_CEM_testing_in_Canada__c, Completion_date__c, Component__c, Construct_Status__c, Container__c, Containers_Condition__c, 
                              Containers_Seal__c, Contains_Animal_Proteins__c, Contains_Applicant_Attachments__c, Contains_Meat__c, Contains_Sugar__c, 
                              Contains_Viral_Agent__c, Conveyance_Into_the_U_S__c, Conveyance_Out_of_the_U_S__c, Conveyance_While_in_the_U_S__c, Country__c, 
                              Country_and_Locality_Information__c, Country_Group__c, Country_of_destination__c, Country_of_Export__c, Country_Of_Origin__c,
                              Country_Of_Origin_Checkbox_CBI__c, Country_of_Origin_Text__c, Country_of_Transit__c, County_Region__c, Courier__c, Courtesy_Justification__c, 
                              CreatedById, CreatedDate, Culture_Designation__c, Culture_Designation_CBI__c, CVB_Application_Type__c, Date_of_Birth__c, 
                              Date_of_intended_destruction__c, Delivery_Recipient_Country_CBI__c, Delivery_Recipient_Country_Name__c, Delivery_Recipient_Email_CBI__c, 
                              Delivery_Recipient_Fax_CBI__c, Delivery_Recipient_First_Name_CBI__c, Delivery_Recipient_Information_CBI__c, Delivery_Recipient_Last_Name_CBI__c, 
                              Delivery_Recipient_Mailing_City_CBI__c, Delivery_Recipient_Mailing_Street_CBI__c, Delivery_Recipient_Organization__c, 
                              Delivery_Recipient_Organization_CBI__c, Delivery_Recipient_Phone_CBI__c, Delivery_Recipient_Postal_Code_CBI__c, 
                              Delivery_Recipient_State_Name__c, Delivery_Recipient_State_ProvinceLU__c, DeliveryRec_USDA_License_Expiration_Date__c, 
                              DeliveryRec_USDA_Regist_Expiration_Date__c, DeliveryRecipient_Email__c, DeliveryRecipient_Fax__c, DeliveryRecipient_First_Name__c,
                              DeliveryRecipient_Last_Name__c, DeliveryRecipient_Mailing_City__c, DeliveryRecipient_Mailing_Country__c, 
                              DeliveryRecipient_Mailing_CountryLU__c, DeliveryRecipient_Mailing_State__c, DeliveryRecipient_Mailing_Street__c, 
                              DeliveryRecipient_Mailing_Zip__c, DeliveryRecipient_Phone__c, DeliveryRecipient_USDA_License_Number__c, 
                              DeliveryRecipient_USDA_Registration_Num__c, Deny_Resubmit__c, Department__c, Departure_Date_and_Time__c, Departure_Time__c,
                              Derivative_Source__c, Describe_Method_of_Disposition__c, describe_processing_and_purification__c, 
                              Describe_the_escape_prevention_method__c, Describe_the_material_to_be_imported__c, Destination_Port__c, Destroy_RA_after_growing__c, 
                              Details_on_the_destruction_method__c, Disease_Pest__c, Do_you_want_plant_grow_or_propagate__c, 
                              Do_you_want_to_use_a_previously_approved__c, Does_This_Application_Contain_CBI__c, Dog_Out_of_Hawaii_for_Resale__c, Domain__c, 
                              Dry_Milk_Category__c, Ear_Tag__c, EFL_CVB_Product_Name__c, EFLAvailableCapabilites__c, EFLCarriers__c, EFLCurrent_Permit_Number__c, 
                              EFLCustoms_Entry_Number__c, EFLDeliveryRecipientFullName__c, EFLExporterFullName__c, EFLNonrequiredDocumentTypes__c, EFLOrganisms__c, 
                              EFLOrganisms_CBI__c, EFLPathway_Name__c, EFLPending_Authorization_Number__c, EFLPermittedMaterialDescription__c, 
                              EFLProductWithMultiIngr__c, EFLScheduledArrivalDate__c, EFLScheduledDepartureDate__c, EFLTypeofSoilLikeMaterial__c, 
                              EFLTypeofSoilLikeMaterial_CBI__c, Enforcement_Officer_Comments__c, Enter_the_date_of_intended_destruction__c, Escape_Prevention_Method__c, 
                              Estimated_Storage_Time__c, Exporter_Email__c, Exporter_Email_CBI__c, Exporter_Fax__c, Exporter_Fax_CBI__c, Exporter_First_Name__c, 
                              Exporter_First_Name_CBI__c, Exporter_Information_CBI__c, Exporter_Last_Name__c, Exporter_Last_Name_CBI__c, Exporter_Mailing_City__c,
                              Exporter_Mailing_City_CBI__c, Exporter_Mailing_Country__c, Exporter_Mailing_Country_CBI__c, Exporter_Mailing_CountryLU__c, 
                              Exporter_Mailing_State__c, Exporter_Mailing_State_ProvinceLU__c, Exporter_Mailing_Street__c, Exporter_Mailing_Street_CBI__c, 
                              Exporter_Mailing_Zip_Postal_CBI__c, Exporter_Mailing_Zip_Postal_Code__c, Exporter_Organization__c, Exporter_Organization_CBI__c, 
                              Exporter_Phone__c, Exporter_Phone_CBI__c, Exporter_State_Name__c, Expouser__c, ExternalID__c, F_Importer_First_Name__c, 
                              Facility_Address_1__c, Facility_Address_2__c, Facility_Building_Type__c, Facility_City__c, Facility_Contact_Email_Address__c, 
                              Facility_Contact_Fax__c, Facility_Contact_First_Name__c, Facility_Contact_Last_Name__c, Facility_Contact_Phone__c, Facility_Country__c, 
                              Facility_Description__c, Facility_Disposition__c, Facility_IATA_Code__c, Facility_Id__c, Facility_Info_Address__c, 
                              Facility_Info_City__c, Facility_Info_Country__c, Facility_info_State__c, Facility_Info_Zip__c, Facility_Inspection_Expiry_Date__c,
                              Facility_Inspection_Validity_Period__c, Facility_ISO_Code__c, Facility_License_Number__c, Facility_Name__c, Facility_Number__c,
                              Facility_Room_Number__c, Facility_State__c, Facility_Type__c, Facility_Zip__c, Final_destination_Hawaii__c, Final_Disposition__c, 
                              Fish_or_Fish_Eggs__c, Flock_Registered__c, Food_or_Feed__c, Frequency__c, Gelding_or_Intact_Horse__c, Gender__c, GPS_of_work_location__c,
                              GPS_of_work_location__Latitude__s, GPS_of_work_location__Longitude__s, Group__c, Growing_Location_Address__c, 
                              Growing_Location_City__c, Growing_Location_County__c, Growing_Location_Description__c, Growing_Location_GPS_Coordinates__c, 
                              Growing_Location_GPS_Coordinates__Latitude__s, Growing_Location_GPS_Coordinates__Longitude__s, Growing_Location_Lattitude__c,
                              Growing_Location_Longitude__c, Growing_Location_State__c, Growing_Location_Zip__c, Hand_Carrier_Email__c, Hand_Carrier_Fax__c,
                              Hand_Carrier_First_Name__c, Hand_Carrier_Information_CBI__c, Hand_Carrier_Last_Name__c, Hand_Carrier_License_Expiration_Date__c, 
                              Hand_Carrier_License_Number__c, Hand_Carrier_Mailing_City__c, Hand_Carrier_Mailing_CountryLU__c, Hand_Carrier_Mailing_State_Province__c,
                              Hand_Carrier_Mailing_State_Province_Txt__c, Hand_Carrier_Mailing_Street__c, Hand_Carrier_Mailing_ZIP_code__c, Hand_Carrier_Phone__c,
                              Hand_Carrier_Registration_Exp_Date__c, Hand_Carrier_USDA_Registration_Number__c, Hand_Carry__c, Hand_Carry_CBI__c, 
                              Has_it_been_approved__c, Has_it_been_approved_for_commercial_use__c, Has_the_seed_been_genetically_modified__c, 
                              Health_Certificate_Attached__c, Height_Weight__c, how_material_is_treated__c, How_will_variance_be_used__c, Human_Specimen__c,
                              If_yes_Please_Describe__c, If_yes_please_explain__c, Importation_Date__c, Importation_Frequency__c, Importer_Email__c, 
                              Importer_Email_CBI__c, Importer_Fax__c, Importer_Fax_CBI__c, Importer_First_Name__c, Importer_First_Name_CBI__c, 
                              Importer_Information_CBI__c, Importer_Last_Name__c, Importer_Last_Name_CBI__c, Importer_Mailing_City__c, Importer_Mailing_City_CBI__c,
                              Importer_Mailing_Country__c, Importer_Mailing_Country_CBI__c, Importer_Mailing_CountryLU__c, Importer_Mailing_State__c, 
                              Importer_Mailing_State_ProvinceLU__c, Importer_Mailing_Street__c, Importer_Mailing_Street_CBI__c, Importer_Mailing_ZIP_CBI__c, 
                              Importer_Mailing_ZIP_code__c, Importer_Organization__c, Importer_Organization_CBI__c, Importer_Phone__c, Importer_Phone_CBI__c, 
                              Importer_Position_Title__c, Importer_USDA_License_Expiration_Date__c, Importer_USDA_License_Number__c,
                              Importer_USDA_Registration_Exp_Date__c, Importer_USDA_Registration_Number__c, In_Canada_for_60_days_or_more__c, Infected__c,
                              Information_you_want_us_to_know__c, Ingredient__c, Insect_Proof_Packaging_Materials__c, Intend_to_release_the_RA__c, 
                              Intended_Use__c, Intended_Use_CBI__c, Intended_Use_Desc_CBI__c, Intended_Use_for_Seeds__c, Intended_Use_Name__c,
                              Introduction_Type__c, Irradiation_Facilities__c, Is_a_Release__c, Is_Age_in_month__c, Is_Alaska_your_destination_state__c,
                              Is_bamboo_split_or_cut__c, is_Commerical_Process__c, Is_it_for_animal_use__c, Is_it_for_Commercial_Use__c, 
                              Is_the_animal_older_than_6_months__c, Is_the_Article_Established_in_US__c, Is_the_Destination__c, Is_the_material_treated_or_processed__c, 
                              Is_this_bird_US_originated__c, Is_violator__c, Is_your_regulated_article_a_plant__c, IsDeleted, Item_Fee__c, Item_Type__c, Item_Types__c, 
                              Laboratory_Isolate__c, Laboratory_Room_Numbers_facility_locatio__c, LastActivityDate, LastModifiedById, LastModifiedDate, 
                              LastViewedDate, Life_Stage__c, Life_Stage_CBI__c, Line_Item_Type_Hidden_Flag__c, Line_Number__c, Live_Dogs_Intended_Use__c, 
                              Lived_in_Location__c, Location_Status__c, Locked__c, Major_Host_CBI__c, Make__c, Make_of_escape_prevention_equipment__c, 
                              Material_Description__c, Means_of_Importation__c, Means_of_Movement__c, Method_of_destruction__c, Method_of_Packaging__c, 
                              Method_of_Preparation__c, Method_of_Shipment__c, Method_of_Shipment_CBI__c, Method_of_Transport__c, Microchip_Number__c, 
                              mode_or_method__c, Model__c, Model_of_escape_Prevention_Equipment__c, Move_out_Hawaii__c, Movement_Type__c, New_line_item_wizard__c, 
                              Number_of_Labels__c, Number_of_Labels_CBI__c, Number_of_New_Design_Protocols__c, Number_of_Release_Sites__c, 
                              Number_of_specimens_or_units_to_import__c, Number_of_Units_CBI__c, Organism__c, Organism_State__c, Organisms__c, Originally_Collected__c, 
                              Originally_collected_CBI__c, Other__c, Other_identifying_information__c, Other_Intended_Use_CBI__c, Other_Method_of_Sterilization__c,
                              Other_Port__c, Outcome__c, OwnerId, Packaging_Type__c, Packaging_Units__c, Parent_Facility_for_New_Facility__c, 
                              Parent_Line_Item__c, Passport_Number__c, Percent_Cooked_Meat__c, Percent_Raw_Meat__c, Permit_Expiration_Date__c, 
                              Picture__c, Plant_for_Preclearance__c, Please_Explain__c, Please_Specify__c, Please_Specify_Material_Description__c,
                              Port_of_Embarkation__c, Port_of_Embarkation_Text__c, Port_of_Entry__c, Port_of_entry_hidden__c, Port_of_Entry_State__c,
                              Port_of_Entry_Text__c, Port_of_entry_transit__c, Port_of_exit_hidden__c, Port_of_exit_transit__c, Precautions__c, Previously_Bred__c,
                              Principal_Partners__c, Processed_anyway_that_change_its_nature__c, processing_and_exporting_countries__c, Producer_Name__c, 
                              Product__c, Product_Name__c, Product_Type__c, Production_Method__c, Program_Line_Item_Pathway__c, Proof_of_Doctor_s_Approval__c, 
                              Proof_of_Origin_Type__c, Proposed_date_of_arrival__c, Proposed_End_Date__c, Proposed_Method_of_Devitalization_after__c, 
                              Proposed_Method_of_Sterilization__c, Proposed_Method_of_Sterilization_before__c, Proposed_plan_of_evaluation__c, Proposed_Route__c, 
                              Proposed_Shipping_Date__c, Proposed_Start_Date__c, Proposed_Use_Of_Material_And_Derivatives__c, Protected_Plant_Permit_number__c, 
                              Provide_Description_of_Organism__c, Provide_Detail_Information__c, Purpose_of_Permit__c, Purpose_of_the_Importation__c, 
                              Quantitiy_Per_Shipment__c, Quantity__c, Quantity_of_each_Shipment__c, Quantity_Type__c, Quantity_UOMs__c, Quarantine_Facility__c,
                              Quarantine_Type__c, Question_Responses__c, RA_be_planted_or_grown_and_or_propagated__c, /*RA_Comfected_or_Contaminated__c,*/ 
                              RA_Scientific_Name__c, Rabies_Vaccination_Certificate_Attached__c, Read_Only__c, Reason_to_keep_the_material_and_location__c,
                              Recommendation__c, Recommended_Biosafety_Level__c, Record_Type_Name__c, RecordTypeId, Registration_Number__c, Regular_Permit_Needed__c, 
                              Regulated_Article__c, Regulated_Article_Formula__c, Regulated_Article_Genetically_Engineered__c, Regulated_Article_Name__c, 
                              Regulated_Article_PackageId__c, Regulated_Article_Status__c, Rejected_Date__c, Release_End_Date__c, Release_Site_Address__c,
                              Release_Start_Date__c, Renew_Authorization__c, Research_Name__c, Reviewer_Comments__c, Scientific_Name__c, Scientific_Name_CBI__c, 
                              Scientific_Name_Text__c, Sealed_Vehicle__c, Secondary_Email_Address__c, Seed_be_planted_or_propagated__c, Seed_Characteristic__c, 
                              Seed_Condition__c, Seed_Exterior__c, Seed_Interior__c, Seed_Maturity__c, Select_Agent__c, Sex__c, Shelf_Stable__c, 
                              Ship_to_another_country__c, Shipment_frequency_CBI__c, Shipment_frequency_of_Regulated_Article__c, Shipment_Quantity__c,
                              Shipping_Container_Types__c, Show_Multi_Permittee_Button__c, Signature__c, Soil_Movement_Without_Sterilization__c, 
                              Soil_Treatment__c, SOP_Status__c, Special_Notes__c, Species__c, Specific_Port_of_Entry__c, Standard_Permit_Types__c, 
                              State_Approval_Numbers__c, State_Province_of_Origin__c, State_Territory_of_Destination__c, Status__c, Storage_Authorized__c,
                              Storage_facilitiy_Name__c, Storage_Locations__c, Store_the_material_for_future__c, Street_Address__c, SystemModstamp, 
                              Tattoo_Number__c, Temperature_this_has_been_processed_to__c, Temperature_UOMs__c, Test_Field__c, Thumbprint__c, Time_Zone_of_Arriving__c,
                              Time_Zone_of_Arriving_Place__c, Time_Zone_of_Departing__c, Time_Zone_of_Departing_Place__c, Tissue__c, Tissue_of_Origin__c, 
                              Total_Fees__c, Total_number_of_shipments__c, Tracking_Enabled__c, Tracking_Number__c, Transfer_Transshipment_Authorized__c, 
                              Transfer_Transshipment_Locations__c, Transit_entire_shipment_APHIS_precleared__c, Transit_Time__c, Transloading_Authorized__c, 
                              Transloading_Locations__c, Transport_Mode__c, Transporter_City__c, Transporter_Email_Address__c, Transporter_Fax__c, 
                              Transporter_Information_CBI__c, Transporter_Last_Name__c, Transporter_Name__c, Transporter_Phone__c, Transporter_State_Province__c, 
                              Transporter_Street_Address__c, Transporter_Type__c, Transporter_Zip_Postal_code__c, Treatment_Already_Received__c, 
                              Treatment_available_in_Country_of_Origin__c, Treatment_prior_to_importation__c, Type_Of_Application__c, Type_Of_Organization__c, 
                              Type_of_Permit__c, Type_of_Research__c, Unit_Of_Measure_CBI__c, Unit_of_Measurement__c, Unit_Of_Measures__c, USDA_Expiration_Date__c, 
                              USDA_License_Expiration_Date__c, USDA_License_Number__c, USDA_License_Number_Format__c, USDA_Registration_Number__c, 
                              USDA_Registration_Number_Format__c, Vessel__c, Vet_Agreement_Attached__c, View_Line_Item_Details__c, Violator__c, 
                              Was_the_country_previous_to_Canada_CEM_A__c, Was_this_horse_in_Canada_for_60_days_or__c, What_is_the_Major_Host__c, 
                              What_is_the_Major_Host_CBI__c, What_is_the_previously_reviewed_variance__c, Where_will_the_RA_be_grown__c, 
                              Will_the_Seed_be_processed_in_any_way_th__c, Will_the_seed_s_be_planted__c, Will_the_seed_s_be_processed__c, 
                              Will_the_soil_be_used_for__c, Wizard_Questions_Map__c, Zip_Postal_Code__c,Sharing_Account__c, 
                              Program_Line_Item_Pathway__r.Program__r.Name,Program_Line_Item_Pathway__r.Show_Required_Documents_Button__c
                                FROM AC__c 
                               WHERE application_number__c =:applicationID]; 
                 return lineItemlist;
            }catch(exception e){
                          EFLErrorLog.createErrorLog('EFLLineItemRepository.selectbyApplicationID()',e);                
            }     
        }
        return null;
     } 
    
    //Get line item details for an line item ID
    public static AC__c selectAllFieldsAndRelatedFieldsbyID(id lineItemID){
        AC__c lineItemRecord;
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('AC__c');
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet()); 
            string soql = 'select ' + String.join(fields, ', ') + ',Program_Line_Item_Pathway__r.Name, Program_Line_Item_Pathway__r.Is_Country_of_Export_Required__c,program_line_item_pathway__r.Allow_Various_Shippers_for__c,program_line_item_pathway__r.Add_More_Shippers__c, program_line_item_pathway__r.EFLAddMoreStops__c,Importer_Mailing_State_ProvinceLU__r.Name,Exporter_Mailing_State_ProvinceLU__r.Name,Approved_Facility__r.Name,Application_Number__r.Applicant_Alternate_Email_Id__c,Application_Number__r.Applicant_Email__c,Application_Number__r.Applicant_Fax__c,Application_Number__r.Applicant_Phone__c,Application_Number__r.Applicant_Address__c,Application_Number__r.Organization__c,Application_Number__r.Applicant_Name__r.Name,Program_Line_Item_Pathway__r.Program__r.Name, State_Territory_of_Destination__r.Name,Country_Of_Origin__r.Name, Scientific_Name__r.Name,Program_Line_Item_Pathway__r.All_Parent_Pathways__c,Program_Line_Item_Pathway__r.Contains_CBI__c,Country_of_Export__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Ra_Scientific_Name__r.name,Scientific_Name__r.Scientific_Name__c, Component__r.Name ,DeliveryRecipient_Last_Name__r.Name, Delivery_Recipient_State_ProvinceLU__r.Name,DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Hand_carrier_Last_Name__r.Name, Hand_carrier_Mailing_CountryLU__r.Name, Importer_Last_Name__r.Name, Importer_Mailing_CountryLU__r.Name, Application_Number__r.Application_Status__c, County_Region__r.Name, State_Province_of_Origin__r.Name, Country_of_destination__r.name,Authorization__r.Status__c, Port_of_Embarkation__r.Name, Domain__r.Name, Application_Number__r.Application_Type__c, Application_Number__r.Application_Submitted__c from AC__c where id = :lineItemID limit 1';   
            lineItemRecord = Database.query(soql); 
            return lineItemRecord;
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLineItemRepository.selectAllFieldsAndRelatedFieldsbyID()',e);                
        }     
        
        return null;
    } 
    
    
}