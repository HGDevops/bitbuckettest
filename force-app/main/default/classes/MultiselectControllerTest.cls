/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class MultiselectControllerTest{ 

     @IsTest(SeeAllData=false) public static void MultiselectControllerTest() {
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
        
         
          
        // Instantiate a new controller with all parameters in the page
        MultiselectController controller = new MultiselectController();
        controller.leftOptions = new List<SelectOption>();
        controller.leftOptions.add(new SelectOption('one','one'));
        controller.leftOptions.add(new SelectOption('two','two'));
        controller.leftOptions.add(new SelectOption('three','three'));                

        controller.rightOptions = new List<SelectOption>();
        controller.rightOptions.add(new SelectOption('one','one'));
        controller.rightOptions.add(new SelectOption('two','two'));
        controller.rightOptions.add(new SelectOption('three','three'));                

        //controller.setOptions(leftOptions,'one');
        controller.leftOptionsHidden = 'Test&Test';
        controller.rightOptionsHidden = 'Test&Test';        
        String le = controller.leftOptionsHidden;
        String re = controller.rightOptionsHidden;        

        System.assert(controller != null); 
        
    }
    
}