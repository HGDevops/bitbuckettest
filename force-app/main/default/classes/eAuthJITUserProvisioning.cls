//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global inherited sharing class eAuthJITUserProvisioning implements Auth.SamlJitHandler {
    
    public string callingCommunityID = null;
    
    private class JitException extends Exception{}
    
    private static final string EFL_JIT_NOT_LEVEL_2_Error = 'EFLJITNotLevel2Error';
    
    @TestVisible
    private void handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) {
        
        if(create) {
            string lnameStr = attributes.get('usdalastname');
            string lnameTrim  = lnameStr.replaceAll(' ',''); //remove spaces from the last name
            u.Username = attributes.get('usdaemail') + '.' +lnameTrim;
            if(attributes.containsKey('usdaeauthid')) {
                u.FederationIdentifier = attributes.get('usdaeauthid');
            }
        }
        
        if(attributes.containsKey('usdahomephone')) {
            u.Phone = attributes.get('usdahomephone');
        }
        if(attributes.containsKey('usdaemail')) {
            u.Email = attributes.get('usdaemail');
        }
        if(attributes.containsKey('usdafirstname')) {
            u.FirstName = attributes.get('usdafirstname');
        }
        if(attributes.containsKey('usdalastname')) {
            u.LastName = attributes.get('usdalastname');
        }
        if(attributes.containsKey('usdastreetaddress')) {
            u.Street = attributes.get('usdastreetaddress');
        }
        if(attributes.containsKey('usdastate')) {
            Level_1_Region__c userState =  [select id, Level_1_Name__c,State_Name__c, Name from Level_1_Region__c where Level_1_Region_Code__c =: attributes.get('usdastate') limit 1];
            u.State = userState != null ? userState.State_Name__c : '';              
        }
        if(attributes.containsKey('usdacity')) {
            u.City = attributes.get('usdacity');
            
        }
        if(attributes.containsKey('usdazip')) {
            u.PostalCode = attributes.get('usdazip');
            
        }        
        if(attributes.containsKey('usdaagencycode')) {
            u.Division = attributes.get('usdaagencycode');
        }
        
        String uid = UserInfo.getUserId();
        User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
        if(create) {
            u.LocaleSidKey = currentUser.LocaleSidKey;
            u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
            u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
            u.EmailEncodingKey = currentUser.EmailEncodingKey;
            String alias = '';
            alias = u.LastName;
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
        }         
        u.CommunityNickname = u.FirstName+u.LastName+u.FederationIdentifier.substring(0,4);
        
        
        if (create){
            communityJITHelper__mdt JITHelper = [SELECT Profile_Name__c FROM communityJITHelper__mdt WHERE Community_ID__c = :callingCommunityID ];
            
            Profile p = [SELECT Id FROM Profile WHERE Name= :JITHelper.Profile_Name__c];
            u.ProfileId = p.Id;
            u.SpringCMEos__SpringCM_User__c = true; 
            u.SpringCMEos__Portal_Only__c = true; 
            u.SpringCMEos__SpringCM_Persona__c = 'eFile P - External Users';
            u.SpringCMEos__SpringCM_Role__c = 'Guest';
            if(!Test.isRunningTest()){
                insert u;
            }            
            
            /* //FA (8/14/19) commenting out. Doesn't appear to be currently used for anything
string name = u.FirstName + ' ' + u.LastName;
Id contactId = [select id from Contact where Name =: name LIMIT 1].Id;
if(attributes.containsKey('usdaemployeestatus') && attributes.get('usdaemployeestatus') != null) {
string agencyCode = 'agency code ' + attributes.get('usdaemployeestatus');
agencyCode = agencyCode.substring(0, 1);
if(agencyCode == 'E'){
}

}*/
        }
        
    }
    
    @TestVisible
    private void handleContact(boolean create, String accountId, User u, Map<String, String> attributes) {
        Contact c;
        boolean newContact = false;
        communityJITHelper__mdt JITHelper = [SELECT Profile_Name__c,AccountRecordTypeName__c,ContactRecordTypeName__c FROM communityJITHelper__mdt WHERE Community_ID__c = :callingCommunityID ];
        if(create) {            
            c = new Contact();
            newContact = true;
            c.EFLFirst_Time_Logged_In__c = TRUE;            
        }
        
        if(attributes.containsKey('usdaemail')) {
            c.Email = attributes.get('usdaemail');
        }
        if(attributes.containsKey('usdafirstname')) {
            c.FirstName = attributes.get('usdafirstname');
        }
        if(attributes.containsKey('usdalastname')) {
            c.LastName = attributes.get('usdalastname');
        }
        if(attributes.containsKey('usdahomephone')) {
            c.Phone = attributes.get('usdahomephone');
        }
        if(attributes.containsKey('usdastreetaddress')) {
            c.MailingStreet = attributes.get('usdastreetaddress');
            c.Mailing_Street_LR__c = c.MailingStreet;
        }
        if(attributes.containsKey('usdacity')) {
            c.MailingCity = attributes.get('usdacity');
            c.Mailing_City_LR__c = c.MailingCity;
        }
        if(attributes.containsKey('usdastate')) {
            Level_1_Region__c contactState =  [select id, Level_1_Name__c,State_Name__c, Name from Level_1_Region__c where Level_1_Region_Code__c =: attributes.get('usdastate') limit 1];            
            c.MailingState = contactState != null ? contactState.State_Name__c : '';            
        }        
        if(attributes.containsKey('usdazip')) {
            c.MailingPostalCode = attributes.get('usdazip');
            c.Mailing_Zip_Postalcode_LR__c = c.MailingPostalCode;
        }        
        if(attributes.containsKey('usdahomephone')) {
            c.HomePhone = attributes.get('usdahomephone');
        }        
        
        c.JITUserProvisioning__c = true;
        if(newContact) {
            c.AccountId = accountId;
            c.recordTypeId = EFLGenericUtility.getrecordtypeid(recordtypelabel(JITHelper.contactRecordtypeName__c,'Contact'));
            c.EFLJIT_Contact__c = true;
            insert(c);
            if(!Test.isRunningTest()){
                 u.ContactId = c.Id;
            }
            
        } 
    }
    
    @TestVisible
    private String handleAccount(boolean create, User u, Map<String, String> attributes) {
        Account a;
        boolean newAccount = false;
        communityJITHelper__mdt JITHelper = [SELECT Profile_Name__c,AccountRecordTypeName__c,ContactRecordTypeName__c FROM communityJITHelper__mdt WHERE Community_ID__c = :callingCommunityID ];
        if(create) {
            if(attributes.containsKey('User.Account')) {
                String account = attributes.get('User.Account');
                a = [SELECT Id FROM Account WHERE Id=:account];
            } else {                
                a = new Account();
                newAccount = true;                
            }
        } 
        if (newAccount == true){            
            a.Name = attributes.get('usdafirstname') + ' ' + attributes.get('usdalastname') + ' Account';
            
            if(attributes.containsKey('usdastreetaddress')) {
                a.BillingStreet = attributes.get('usdastreetaddress');
                a.ShippingStreet = attributes.get('usdastreetaddress');
            }
            if(attributes.containsKey('usdacity')) {
                a.BillingCity = attributes.get('usdacity');
                a.ShippingCity = attributes.get('usdacity');
            }            
            if(attributes.containsKey('usdastate')) {
                Level_1_Region__c userState =  [select id, Level_1_Name__c,State_Name__c, Name from Level_1_Region__c where Level_1_Region_Code__c =: attributes.get('usdastate') limit 1];
                a.BillingState = userState != null ? userState.State_Name__c : '';  
                a.ShippingState =  userState != null ? userState.State_Name__c : '';
            }            
            if(attributes.containsKey('usdazip')) {
                a.BillingPostalCode = attributes.get('usdazip');
                a.ShippingPostalCode = attributes.get('usdazip');
            }    
            if(attributes.containsKey('usdaworkphone')) {
                a.Phone = attributes.get('usdaworkphone');
            }
            a.Description = 'Community User Account';
            a.Type = 'Applicant'; 
            
            a.recordTypeId = EFLGenericUtility.getrecordtypeid(recordtypelabel(JITHelper.accountRecordtypeName__c,'Account'));
            insert(a);
            return a.Id;
        }
        return null;
    }
    
    public void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
                          String federationIdentifier, Map<String, String> attributes, String assertion) {
                              if(communityId != null || portalId != null) {
                                  callingCommunityID = communityID;
                                  String account = handleAccount(create, u, attributes);
                                  handleContact(create, account, u, attributes);
                                  handleUser(create, u, attributes, federationIdentifier, false);
                              } else {
                                  handleUser(create, u, attributes, federationIdentifier, true);
                              }
                          }
    
    private string recordtypelabel(string recordtypeName,string objectName){
        string rtLabel;        
        EFLRecordType__mdt  recordtypelabel = [Select label FROM EFLRecordType__mdt WHERE Record_Type_Name__c =: recordtypeName AND Object_API_Name__c = :objectName limit 1];
        rtLabel = recordtypelabel.label ;
        
        return rtLabel ;
    }                     
    
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
                           String federationIdentifier, Map<String, String> attributes, String assertion) 
    {
        
        if(!isLevel2Assurance(attributes))
        {
           // throw new JitException(EFLGenericUtility.getMessage(EFL_JIT_NOT_LEVEL_2_Error));
        }
        
        User u = new User();
        handleJit(true, u, samlSsoProviderId, communityId, portalId,
                  federationIdentifier, attributes, assertion);
        return u;
    }
    
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,  
                           String federationIdentifier, Map<String, String> attributes, String assertion) 
    {
        
        //Check and exception if not level 2 assurance
        if(!isLevel2Assurance(attributes))
        {
            //throw new JitException(EFLGenericUtility.getMessage(EFL_JIT_NOT_LEVEL_2_Error));
        }
    }
    
    //usdaassurancelevel saml attribute check for level 2 assurance
    global boolean isLevel2Assurance(Map<String, String> attributes)
    {
        boolean isLevel2Assurance = false;
        string usdaAssuranceLevel = '';
        if(attributes.containsKey('usdaassurancelevel')) 
        {
            usdaAssuranceLevel = attributes.get('usdaassurancelevel');
        }
        if(usdaAssuranceLevel=='2')
        {
            isLevel2Assurance = true;
        }
        else
        {
            isLevel2Assurance = false;
        }
        return isLevel2Assurance;
    }
    
}