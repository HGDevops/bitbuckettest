public with sharing class CARPOL_InspectionLetterPDF_controller {

      public INSPECTION__C INSPECTION { get; set; }
    
    public CARPOL_InspectionLetterPDF_controller(ApexPages.StandardController controller) {
    inspection = [select id, name, Compliance_letter_content__c, Inspector_Email__c, Communication_Manager_Template__c  FROM Inspection__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
    }

}