@isTest

private class TLPortofEntryLookupTest 
{

    public static testMethod void TLPortofEntryLookup() 
    {
        //pageRef to VF page
        PageReference pageRef = Page.TLPortofEntryLookup;
        
        //search string
        pageRef.getParameters().put('lksrch', 'something');
        
        //regulated_article__c
        pageRef.getParameters().put('regartid', '123');
        
        //porttype to portofexit later     
        pageRef.getParameters().put('txt', 'Portofentry');
        
        //form
        pageRef.getParameters().put('frm', 'anFrm');
        
        Test.setCurrentPage(pageRef);

        //construct the controller class.   
        TLPortofEntryLookup controller = new TLPortofEntryLookup();
        controller.search();
        controller.getFormTag();
        controller.getTextBox();
        
        ApexPages.currentPage().getParameters().put('txt', 'Portofexit');
        TLPortofEntryLookup controller2 = new TLPortofEntryLookup();

   
     }
}