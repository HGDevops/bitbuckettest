public with sharing class EFLInspectionActivityHandler implements EFLIActivityInterface
{
    
    public void populateActivityRecords(sObject parentRecord,list<sObject> activityList ) 
    { 
        
                    Inspection__c inspRecord = (Inspection__c)parentRecord;
                    EFLIInspectionEngine engine = EFLInspectionEngineFactory.getEngine(inspRecord);  
        			system.debug('engine '+engine);
                    engine.loadActivities(inspRecord, activityList);  
        			system.debug('authRecord '+inspRecord);
       				 system.debug('activityList '+activityList);
        			
        
      
    }
    
   public class activityException extends Exception {}
   
}