@isTest(seeAllData=false)
public class EFLAnnualReportSummaryApex_Test{
    
    @isTest
    public static void EFLAnnualReportSummaryApex_TestMethod1(){
        
        
        
        String accRecordTypeId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('AC R&L Account').getRecordTypeId();
        Account testAcc = new Account();
        testAcc.RecordTypeId = accRecordTypeId;
        testAcc.Name = 'Test Account';
        Database.insert(testAcc);
        
        Contact ACISCon = new Contact();
        ACISCon.LastName = 'Test Contact';
        ACISCon.FirstName = 'ACIS';
        ACISCon.EFL_Role__c= 'CEO';
        ACISCon.AccountID = testAcc.id;
        Database.insert(ACISCon); 
        
        Contact testCon = new Contact();
        testCon.LastName = 'Test Contact';
        testCon.FirstName = 'Name';
        testCon.EFL_Role__c= 'CEO';
        testCon.AccountID = testAcc.id;
        testCon.EFL_ACIS_Contact__c = ACISCon.id;
        Database.insert(testCon);      
        
        
        User usershare = new User();
        usershare.ProfileID = [Select Id From Profile Where Name='Report Preparer'].id;
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.LanguageLocaleKey = 'en_US';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.FirstName = 'first';
        usershare.LastName = 'last';
        usershare.Username = 'testUsername1@test.com';   
        usershare.CommunityNickname = 'testUser123';
        usershare.Alias = 't1';
        usershare.Email = 'no@email.com';
        usershare.IsActive = true;
        usershare.ContactId = testCon.id;
        Database.insert(usershare);   
        
        EFLRegistration__c testReg = new EFLRegistration__c();
        testReg.EFLAccount__c =testAcc.id; 
        Database.insert(testReg);
        
        EFLAnnual_Report__c annRep = new EFLAnnual_Report__c();
        annRep.EFLRegistration__c = testReg.id;
        annRep.EFLSites__c = 'Site1,Site2,Site3';
        annRep.EFLAccount__c =testAcc.id; 
        Database.insert(annRep);
        
        EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
        regAnimal.EFLAnnual_Report__c = annRep.id;
        regAnimal.EFLSelectedForReport__c = true;
        regAnimal.EFLAnimal__c = 'Other Animals';
        regAnimal.ELFColumnEPainNotMinimized__c = 100;
        Database.insert(regAnimal); 
        integer countAnmls = [select count() from EFLRegistered_Animal__c];
        system.runas (usershare){ 
            Test.startTest();
            system.assertEquals(EFLAnnualReportSummaryApex.fetchFacilities(annRep.id).id,annRep.id);
            system.assertEquals(EFLAnnualReportSummaryApex.fetchAnimalUsage(annRep.id).size(),1);
            EFLAnnualReportSummaryApex.updateARStatus(annRep.id);
            string newstatus = [select status__c from eflAnnual_report__c where id=:annRep.id].status__c;
            system.assertEquals(newstatus, 'Ready for CEO/IO Signature');
            EFLAnnualReportSummaryApex.updateARContact(annRep.id);
            Id updConId = [select EFLcontact__c from eflAnnual_report__c where id=:annRep.id].EFLcontact__c;
            system.assertEquals(updConId, ACISCon.Id);
            EFLAnnualReportSummaryApex.updateSingerAgreed(annRep.id,true);
            boolean currSignAgreed = [select EFLSigner_Agreed__c from eflAnnual_report__c where id=:annrep.Id].EFLSigner_Agreed__c;
            system.assertEquals(currSignAgreed,true);
            system.assertEquals(EFLAnnualReportSummaryApex.getConType(), 'CEO');        
            system.assertEquals(EFLAnnualReportSummaryApex.getParentIds(annrep.id).size(),2);
            
            EFLAnnual_Report__c arRes = EFLAnnualReportSummaryApex.getReportForView(annRep.Id);
            system.assertEquals(true, arRes.EFL_Locked__c);
            Test.stopTest();
        }
    }
}