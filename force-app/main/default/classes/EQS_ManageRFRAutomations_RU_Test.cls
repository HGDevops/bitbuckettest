@isTest(seealldata=false)
private class EQS_ManageRFRAutomations_RU_Test
{
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test() 
    {
      
      
      Test.startTest();
      EQSPrepareTestData2();
      String jobId = System.schedule('ManageRFRAutomationsTest2',CRON_EXP, new EQS_ManageRFRAutomations_RU());

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
      System.assertEquals(CRON_EXP, ct.CronExpression);

      System.assertEquals(0, ct.TimesTriggered);

      System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
      
      Test.stopTest();
      
      
      // Now that the scheduled job has executed after Test.stopTest(),
      Contact[] tmpResponder = [Select Id,EQS_Responder_Status__c,EQS_Unavailable_Reason__c,EQS_Unavailable_End_Date__c From Contact Where FirstName = 'Responder2'];
      System.assertEquals(tmpResponder.size(), 1);
      System.assertEquals(tmpResponder[0].EQS_Responder_Status__c+'','Available');
      //System.assertEquals(tmpResponder[0].EQS_Unavailable_Reason__c+'','');
      //System.assertEquals(tmpResponder[0].EQS_Unavailable_End_Date__c,null);
      

   }
    
   static testMethod void EQSPrepareTestData2() 
   {
    
      String EQSAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('APHIS EQS Organization').getRecordTypeId();
      String EQSIncidentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('APHIS EQS Incident').getRecordTypeId();
      String EQSResponderRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('APHIS EQS Responder').getRecordTypeId();
      String EQSRFRRecordTypeId = Schema.SObjectType.EQS_Resource_Request__c.getRecordTypeInfosByName().get('Resource Fill Request - Dispatcher Initiated').getRecordTypeId();
      
      Case EQSIncident=new Case(RecordTypeId=EQSIncidentRecordTypeId,Subject='HPAI INDIANA 01 2016-Test');
      EQSIncident.EQS_Category__c='ANIMAL HEALTH';
      EQSIncident.Priority='Medium';
      EQSIncident.Status='Open';
      insert EQSIncident;
      Account Org1=new Account(RecordTypeId=EQSAccountRecordTypeId,Name='ADMIN, FAC & FLEET MGMT',EQS_Org_Abbreviation__c='AF&FM',EQS_Org_Code__c='05');
      insert Org1;
      
      //date MobDate=date.newInstance(2016, 11, 14);
      //date DeMobDate=date.newInstance(2016, 11, 30);
      date MobDate=date.today();
      //MobDate=MobDate.addDays(-4);
      //date DeMobDate=MobDate.addDays(15);
      
      Contact Responder1=new Contact(RecordTypeId=EQSResponderRecordTypeId);
      Responder1.FirstName = 'Responder2';
      Responder1.LastName = 'Test2';
      Responder1.Email = 'Responder2.Test2@gmail.com';
      Responder1.EQS_Responder_Status__c='Unavailable';
      Responder1.EQS_Unavailable_Reason__c='Jury or Court Obligations';
      Responder1.EQS_Unavailable_Start_Date__c=MobDate.addDays(-7);
      Responder1.EQS_Unavailable_End_Date__c=MobDate.addDays(-1);        
      Responder1.AccountId = Org1.Id;
      insert Responder1;
      
      
    }      
}