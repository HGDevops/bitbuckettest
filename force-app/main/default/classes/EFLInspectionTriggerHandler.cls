/**
* Trigger Handler for the Inspection SObject. This class implements the EFLITrigger
* interface to help ensure the trigger code is bulkified and all in one place.
*/ 
public with sharing class EFLInspectionTriggerHandler 
implements EFLITrigger 
{     
    public static boolean inspectorSwitchEmailReOccurrenceCheck {get; set;}
    public static boolean inspectorCancelComplaintEmailReOccurrenceCheck {get; set;}
    list<sObject> activityList = new list<sObject>();
    list<sObject> finalActivityList = new list<sObject>();
    list<Inspection__c> inspectionsToUpdate = new list<Inspection__c>();
    List<sObject> questionList= new List<sObject>();
    List<sObject> finallstInsQuestioneries = new List<sObject>();
    list<Inspection__c> inspListtoUpdate = new list<Inspection__c>();
    //List<Sobject> inspSobjsToUpdateLocations = new List<Sobject>();
    //Set<ID> inspectionIds = (new Map<Id,Inspection__c>((List<Inspection__c>)Trigger.New)).keySet();
    static integer firstVar = 0;
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
* Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        if (EFLGenericUtility.isTriggerDisabled('EFLInspectionTrigger'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    /*
* Constructor
*/ 
    public EFLInspectionTriggerHandler()
    {
        
    }
    
    /**
* bulkBefore
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore() {
        Map<Id,Id> locationStateIdMap = new Map<Id,Id>();
        Map<Id,String> locationStateNameMap = new Map<Id,String>();
        //W-026861: Update sharing account on inspection through location record
        for (SObject inspSobj : Trigger.New){
            System.debug('location value changed...'+ EFLGenericUtility.isFieldValueChanged(Trigger.OldMap, inspSobj, 'Location__c'));
            if (EFLGenericUtility.isFieldValueChanged(Trigger.OldMap, inspSobj, 'Location__c') && 
                (inspSobj.get('Location__c') == null || inspSobj.get('State__c') == null)){ //location removed or state name does not exist (likely a state outside of the US)
                inspSobj.put('Sharing_Account__c', null);
            } else if (EFLGenericUtility.isFieldValueChanged(Trigger.OldMap, inspSobj, 'Location__c')){                 
                Inspection__c insp = (Inspection__c)inspSobj;       
                locationStateNameMap.put(insp.Location__c,insp.State__c.replace('[','').replace(']',''));//Strip out cbi tags from state field, if present   
            }            
        }
        //if location changed, get account ID for state on location record. replace state id with account id
        for (Account acct : [Select Id, BillingState from Account where Type = 'State' and BillingState in :locationStateNameMap.values()]){
            for (Id locId : locationStateNameMap.keySet()){
                if (locationStateNameMap.get(locId) == acct.BillingState){
                    locationStateIdMap.put(locId,acct.Id);
                }
            }            
        }
        //update inspection sharing account with state id
        if (!locationStateIdMap.isEmpty()){
            for (SObject inspSobj : Trigger.New){
                Inspection__c insp = (Inspection__c)inspSobj;
                inspSobj.put('Sharing_Account__c', locationStateIdMap.get(insp.Location__c));               
            }
        }        
        
    }
    
    /**
* bulkAfter
* This method is called prior to execution of an AFTER trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkAfter()
    {
        Set<Id> InspecTempId = new Set<Id>();
        for(Inspection__c ins : (List<Inspection__c>)trigger.new){
            if(ins.EFL_Inspection_Questionnaire_Template__c != null)InspecTempId.add(ins.EFL_Inspection_Questionnaire_Template__c);
        }
        //call engine method to get the question record
        if(InspecTempId.size() > 0){
            //EFLBRSInspectionEngine engine = new EFLBRSInspectionEngine();
            EFLBRSInspectionEngine.getQuestion(InspecTempId);
        }        
        
    }    
    
    /**
* beforeInsert
* This method is called iteratively for each record to be inserted during a BEFORE
* trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
*/
    public void beforeInsert(SObject so)
    {    
        
    }
    
    /**
* beforeUpdate
* This method is called iteratively for each record to be updated during a BEFORE
* trigger.
*/
    public void beforeUpdate(SObject oldSo, SObject so)
    {
        string statCompl = 'Completed';
        Inspection__c oldInspectionRecord = (Inspection__c)oldSo;
        Inspection__c newInspectionRecord = (Inspection__c)so;
        
        EFLIInspectionEngine engine = EFLInspectionEngineFactory.getEngine(newInspectionRecord);
        
        //validate task creation and Load all activities along with authorizations
        if(EFLInspectionUtility.needsTaskCreation(oldInspectionRecord,newInspectionRecord)){
            newInspectionRecord.Activity_Sequence__c = 0;
            engine.loadActivities(newInspectionRecord, activityList);
            finalActivityList.addAll(activityList);
            inspectionsToUpdate.add(newInspectionRecord);
        }
        if((oldInspectionRecord.Stage__c != newInspectionRecord.Stage__c)){
            newInspectionRecord.Ready_for_next_stage__c =  false;
        }
        
        // W-032241 Rajesh Potla added  
        Id BRS_RecType = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();   
        
        if((!oldInspectionRecord.certified__c && newInspectionRecord.certified__c) && newInspectionRecord.RecordTypeId == BRS_RecType){
            List<EFL_Inspection_Questionnaire_Questions__c> InsQUesQ = new List<EFL_Inspection_Questionnaire_Questions__c> ();
            InsQUesQ  = [Select ID, Name, Response__c, Inspection__c, Inspection__r.Stage__c from EFL_Inspection_Questionnaire_Questions__c where Inspection__c  =: newInspectionRecord.Id AND Response__c ='' AND Inspection__r.Stage__c = 'Inspection Report'];
            if(InsQUesQ.size()>0){
                if(Trigger.newMap.containsKEy(InsQUesQ[0].Inspection__c))
                    Trigger.newMap.get(InsQUesQ[0].Inspection__c).addError('All inspection question responses must be populated before certification.');
            }
        }
    } 
    
    /**
* beforeDelete
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete(SObject so)
    {
        
    }
    
    /**
* afterInsert
* This method is called iteratively for each record inserted during an AFTER
* trigger. Always put field validation in the 'After' methods in case another trigger
* has modified any values. The record is 'read only' by this point.
*/ 
    public void afterInsert(SObject so)
    {
        //Inspection__c oldInspectionRecord = (Inspection__c)oldSo;
        Inspection__c newInspectionRecord = (Inspection__c)so;
        //Rajesh Potla - W-034151
        if(newInspectionRecord.EFL_Inspection_Questionnaire_Template__c != null /* && newInspectionRecord.Program__c=='BRS'*/){
            EFLBRSInspectionEngine  engine = new EFLBRSInspectionEngine ();
            engine.loadQuestion(newInspectionRecord ,questionList);
            finallstInsQuestioneries.addall(questionList);
        }
        // VV added for W-027342 send email to the inspector that was assigned
        if(newInspectionRecord.EFL_Inspector__c != null ||
           newInspectionRecord.EFL_Responsible_SPRO__c!= null){
               Inspection__c newInspectionRecordWithInspector = new Inspection__c();
               newInspectionRecordWithInspector = [SELECT Id,Name,EFL_Inspector__r.EFL_Email_Address__c,EFL_Inspector__r.Name, 
                                                   EFL_Responsible_SPRO__r.Email,EFL_Responsible_SPRO__r.Name FROM Inspection__c WHERE Id=:newInspectionRecord.Id];
               string InspEmailAddress = '';
               string InspName = '';
               //for internal/SPHD assignments
               if(newInspectionRecord.EFL_Inspector__c != null){
                   InspEmailAddress  = newInspectionRecordWithInspector.EFL_Inspector__r.EFL_Email_Address__c;
                   InspName = newInspectionRecordWithInspector.EFL_Inspector__r.Name;
               }else if(newInspectionRecord.EFL_Responsible_SPRO__c != null){
                   //for external/SPRO assignments
                   InspEmailAddress  = newInspectionRecordWithInspector.EFL_Responsible_SPRO__r.Email;
                   InspName = newInspectionRecordWithInspector.EFL_Responsible_SPRO__r.Name;
               }
               if(InspEmailAddress != '' && InspName != ''){
                   Messaging.reserveSingleEmailCapacity(2);
                   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                   String[] toAddresses = new String[] {InspEmailAddress}; 
                       // String[] ccAddresses = new String[] {'vijay.vellaturi@accenturefederal.com'};                
                       mail.setToAddresses(toAddresses);
                   //mail.setCcAddresses(ccAddresses);        
                   //mail.setReplyTo('vijay.vellaturi@accenturefederal.com');        
                   mail.setSenderDisplayName('eFile Support');        
                   mail.setSubject('APHIS eFile: Complete Inspection Record ');        
                   mail.setBccSender(false);        
                   mail.setUseSignature(false);
                   String recordLink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newInspectionRecordWithInspector.Id;
                   //system.debug('####  recordLink = '+recordLink);        
                   String emailBody = 'Dear '+InspName;
                   emailBody += '<br/>';
                   emailBody += 'You have been assigned Inspection Record <a href='+recordLink+'>'+newInspectionRecordWithInspector.Name+'</a>. Please sign in to complete all tasks on this inspection.';
                   emailBody += '<br/>';
                   emailBody += 'Thank you,';
                   emailBody += '<br/>';
                   emailBody += 'USDA APHIS eFile Team';
                   mail.setHtmlBody(emailBody);        
                   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
               }
           }
    }
    
    /**
* afterUpdate
* This method is called iteratively for each record updated during an AFTER
* trigger.
*/
    public void afterUpdate(SObject oldSo, SObject so)
    { 
        Inspection__c oldInspectionRecord = (Inspection__c)oldSo;
        Inspection__c newInspectionRecord = (Inspection__c)so;
        //W-032244 VV added code for task creation when the existing task is completed and certified is set to true and
        //inspection stage is Inspection Report
        Program_Activity__mdt mdt = [select Activity_Type__c, Assigned_To__c, Priority__c, Subject__c, Program_Stage__c from Program_Activity__mdt where developerName = 'PINS_Inspection_Report_2'];
        string mbrRole = mdt.Assigned_To__c;//ROP Reviewer (Regulatory Biotechnologist 2)
        string taskSubj = mdt.Subject__c;//Review and Accept Inspection Report
        string inspStage = [select Stage__c from Program_Stages__mdt where developerName = 'Inspection_Report'].Stage__c;//Inspection Report
        string normal = mdt.Priority__c;//low
        string statusCmpl = System.Label.EFLCompleted; //Completed       
        string notStarted =  System.Label.EFL_Not_Started; //Not Started      
        Boolean createTask = false;
        //if inspection.certified flag is being set to true and in Inspection Report stage
        if(newInspectionRecord.Stage__c == inspStage && newInspectionRecord.Certified__c) {    
            //if inspection has a team member is present w/role ROP Reviewer (Regulatory Biotechnologist 2)
            if([select count() from Team__c where Inspection__c = :newInspectionRecord.Id and Member_Role__c = :mbrRole] > 0) {
                // if inspection has a task of subject Review and Accept Inspection Report and was completed
                if([select count() from Task where WhatId = :newInspectionRecord.Id and Subject = :taskSubj and Status = :statusCmpl] >0) {
                    // then set the create task flag to true
                    createTask = true;
                }
                if([select count() from Task where WhatId = :newInspectionRecord.Id and Subject = :taskSubj and Status != :statusCmpl] >0) {
                    createTask = false;
                }                
            }
        }
        if(createTask) {
            Task newTaskRef = new Task();
            newTaskRef.WhatId = newInspectionRecord.Id;
            newTaskRef.Subject  = taskSubj;
            //get the user from Team object in the role ROP Reviewer (Regulatory Biotechnologist 2)
            newTaskRef.OwnerId = [select Member__c from Team__c where Inspection__c = :newInspectionRecord.Id and Member_Role__c = :mbrRole limit 1].Member__c;
            newTaskRef.Priority   = normal;
            newTaskRef.Status   = notStarted;
            newTaskRef.stage__c = newInspectionRecord.Stage__c;
            newTaskRef.RecordTypeId = EFLGenericUtility.getRecordTypeId('TASK Standard');
            insert newTaskRef;            
        }
        //END W-032244 VV updates
        // Rajesh Potla - W-034151
        if(newInspectionRecord.EFL_Inspection_Questionnaire_Template__c != null && newInspectionRecord.EFL_Inspection_Questionnaire_Template__c !=oldInspectionRecord.EFL_Inspection_Questionnaire_Template__c  /*&& newInspectionRecord.Program__c=='BRS'*/){
            EFLBRSInspectionEngine  engine = new EFLBRSInspectionEngine ();
            engine.loadQuestion(newInspectionRecord ,questionList);
            finallstInsQuestioneries.addall(questionList);
        }
        // VV added for W-027342 send email to the inspector that was assigned
        if(EFLInspectionTriggerHandler.inspectorSwitchEmailReOccurrenceCheck!=true && (newInspectionRecord.EFL_Inspector__c != oldInspectionRecord.EFL_Inspector__c ||
           newInspectionRecord.EFL_Responsible_SPRO__c!= oldInspectionRecord.EFL_Responsible_SPRO__c)){
               Inspection__c newInspectionRecordWithInspector = new Inspection__c();
               newInspectionRecordWithInspector = [SELECT Id,Name,EFL_Inspector__r.EFL_Email_Address__c,EFL_Inspector__r.Name, 
                                                   EFL_Responsible_SPRO__r.Email,EFL_Responsible_SPRO__r.Name FROM Inspection__c WHERE Id=:newInspectionRecord.Id];
               string InspEmailAddress = '';
               string InspName = '';
               Messaging.reserveSingleEmailCapacity(2);
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               mail.setBccSender(false);        
               mail.setUseSignature(false);
               //for internal/SPHD assignments
               if((newInspectionRecord.EFL_Inspector__c != oldInspectionRecord.EFL_Inspector__c) && newInspectionRecord.EFL_Inspector__c != NULL){
                   InspEmailAddress  = newInspectionRecordWithInspector.EFL_Inspector__r.EFL_Email_Address__c;
                   InspName = newInspectionRecordWithInspector.EFL_Inspector__r.Name;
                   List<string> toAddressList = new List<string>();
                   toAddressList.add(InspEmailAddress);
                   mail.setToAddresses(toAddressList);
                   mail.settargetObjectId(EFLContactRepository.contactIdForsettargetObjectId());
        		   mail.setTreatTargetObjectAsRecipient(false);
               }
               if((newInspectionRecord.EFL_Responsible_SPRO__c != oldInspectionRecord.EFL_Responsible_SPRO__c) && newInspectionRecord.EFL_Responsible_SPRO__c != NULL){
                   //for external/SPRO assignments
                   InspEmailAddress  = newInspectionRecordWithInspector.EFL_Responsible_SPRO__r.Email;
                   InspName = newInspectionRecordWithInspector.EFL_Responsible_SPRO__r.Name;
               	   mail.setTargetObjectId(newInspectionRecordWithInspector.EFL_Responsible_SPRO__c);
               }
               
               //mail.setTargetObjectId(newInspectionRecordWithInspector.EFL_Responsible_SPRO__c);
               EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Complete_Inspection_Record_Email_HTML'];
               mail.setTemplateId(template.Id);
               mail.setWhatId(newInspectionRecordWithInspector.Id);
               /*String recordLink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newInspectionRecordWithInspector.Id;
               String emailBody = 'Dear '+InspName;
               emailBody += '<br/><br/>';
               emailBody += 'You have been assigned Inspection Record <a href='+recordLink+'>'+newInspectionRecordWithInspector.Name+'</a>. Please sign in to complete all tasks on this inspection.';
               mail.setHtmlBody(emailBody);  */    
               EFLInspectionTriggerHandler.inspectorSwitchEmailReOccurrenceCheck = true;
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
           }
        //If status is canceled then send email to creater and biotechnologist
        if(EFLInspectionTriggerHandler.inspectorCancelComplaintEmailReOccurrenceCheck!=true && newInspectionRecord.status__c != oldInspectionRecord.status__c && (newInspectionRecord.status__c == 'Cancelled'|| newInspectionRecord.status__c == 'Compliant')){
            Set<Id> setUserId = new Set<Id>();
            Team__c objeTm= [select id,name,Authorization__r.name,Member__c,Member__r.Email ,Member_Role__c from Team__c where Authorization__c =:newInspectionRecord.Authorization__c and Member_Role__c = 'BRS Biotechnologist' limit 1]; 
            setUserId.add(objeTm.Member__c);
            setUserId.add(newInspectionRecord.CreatedById);
            List<User> objuser = [select id,Email,FirstName,LastName from user where id In:setUserId];
            //system.debug('####  objuser = '+objuser);    
            //system.debug('####  objeTm = '+objeTm);
            if(objuser.size() > 0){
                for(User u : objuser){
                    List<String> toAddresses = new List<String>();
                    toAddresses.add(u.Email);
                    Messaging.reserveSingleEmailCapacity(2);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                  
                    mail.setToAddresses(toAddresses);
                    mail.setBccSender(false);        
                    mail.setUseSignature(false);
                    //mail.setTargetObjectId(newInspectionRecord.EFL_Responsible_SPRO__c);
                    mail.settargetObjectId(EFLContactRepository.contactIdForsettargetObjectId());
        		    mail.setTreatTargetObjectAsRecipient(false);
                    EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Cancelled_Inspection_Record_Email_HTML'];
                    mail.setTemplateId(template.Id);
                    mail.setWhatId(newInspectionRecord.Id);
                    mail.setSaveAsActivity(false);
                    /*String recordLink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newInspectionRecord.Id;
                    //system.debug('####  recordLink = '+recordLink);        
                    String emailBody = 'Hi '+u.FirstName +' '+ u.LastName;
                    emailBody += '<br/><br/>';
                    emailBody += 'This email is to inform you that Inspection <a href='+recordLink+'>'+newInspectionRecord.Name+'</a> has been completed for authorization <a href='+recordLink+'>'+objeTm.Authorization__r.name+'</a> Please review the inspection and take the necessary steps on the authorization.';
                    mail.setHtmlBody(emailBody);*/    
                    EFLInspectionTriggerHandler.inspectorCancelComplaintEmailReOccurrenceCheck = true;
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
                }
            }
        }
        
        //Added by Conor for W-032208
        if (newInspectionRecord.Certified__c == true){
            inspListToUpdate.add(newInspectionRecord);
        }
        
    } 
    
    /**
* afterDelete
* This method is called iteratively for each record deleted during an AFTER
* trigger.
*/ 
    public void afterDelete(SObject so)
    {
    }
    
    /**
* andFinally
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    
    public void andFinally()
    {
        //Inserting Activities
        if(finalActivityList != null){
            //system.debug('finalActivityList'+ finalActivityList);
            Database.SaveResult[] results = Database.insert(finalActivityList,true); 
            
        }
        if(finallstInsQuestioneries.size()>0){
            Database.SaveResult[] results = Database.insert(finallstInsQuestioneries,true); 
        }
        
        if(inspListToUpdate.size()>0){
            firstVar++;
            //system.debug('@@@@@@@@@@@@@@@@@' + firstVar);
            if (firstVar == 1){
                SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), inspListToUpdate.get(0).getSObjectType().getDescribe().getName(), inspListToUpdate, 'Inspection Report Versioning');   
            }
        }
    } 
    
}