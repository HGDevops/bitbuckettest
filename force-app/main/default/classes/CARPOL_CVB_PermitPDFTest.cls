@isTest(SeeAllData=true)
private class CARPOL_CVB_PermitPDFTest{
    static testMethod void testCARPOL_CVB_PermitPDF() {
        
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp);      
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
        Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Attachment attach = testData.newattachment(ac.Id);  
        
        test.startTest();
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.Authorization_Type__c = 'Permit';
        update objauth;
        ac.Authorization__c = objauth.Id;
        update ac;
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        PageReference pageRef = Page.CARPOL_CVB_PermitPDF;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        CARPOL_CVB_PermitPDF objCarpol = new CARPOL_CVB_PermitPDF(sc);
        List<AC__c> listAC=objCarpol.getACRecords();
        List<Authorization_Junction__c> listAJ=objCarpol.getConditions();
        test.stopTest();
        
    }
}