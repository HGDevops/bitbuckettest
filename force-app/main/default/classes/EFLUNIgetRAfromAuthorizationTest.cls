/**
* Author : Kishore Kumar
* Created Date : 5/1/2017
* Purpose : This is used for code coverage of class EFLUNIgetRAfromAuthorization 
* Related Class: EFLUNIgetRAfromAuthorization
*/
@isTest(seealldata=true)
private class EFLUNIgetRAfromAuthorizationTest{
    @IsTest
    static void EFLUNIgetRAfromAuthorizationTest(){
    	string AccountRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
        //string AccountRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('APHIS Efile Standard Account').getRecordTypeId();
/**
* Prepare Account, Contact, Pathway, Application, Authorization ,line item, Applicant Attachment records
**/

        CARPOL_AC_TestDataManager testData=new  CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Applicant_Contact__c imp = testData.newappcontact(); 
        Applicant_Contact__c exp = testData.newappcontact(); 
        Applicant_Contact__c dd = testData.newappcontact(); 
        List<Applicant_Contact__c> AssoccontactList = new List<Applicant_Contact__c>();
        AssoccontactList.add(exp);
        Account objacct=testData.newAccount(AccountRecordTypeId);
        Contact objcont=testData.newcontact();
        Contact objstatecon=testData.newStateContact();

        Program_Line_Item_Pathway__c objpathway=testData.newCaninePathway();
        Application__c objapp=testData.newapplication();
        Application__c objapp1=testData.newapplication();
        List<Application__c> AppList = new List<Application__c>();
        AppList.add(objapp);
        List<Application__c> AppList1 = new List<Application__c>();
        AppList1.add(objapp1);
        Authorizations__c objauth=testData.newAuth(objapp.id);
        Reviewer__c testreviewer = testdata.reviewer(objauth.id, objstatecon.id);
        Date dtExpiry=Date.newInstance(2019,9,7);
        dtExpiry=dtExpiry.addDays(365);
        objauth.Expiry_Date__c=dtExpiry;
        objauth.Application__c=objapp.Id;
        objauth.Authorized_User__c = objcont.Id;
        Update objauth;
    
        AC__c objac=testData.newLineItem('Personal Use',objapp);
        objac.Authorization__c=objauth.id;
        objac.Application_Number__c=objapp.Id;
        Update objac;
    
        Attachment objattach=testData.newAttachment(objauth.id);
        Applicant_Attachments__c objappattach=testData.newAttach(objac.id);
        objappattach.Authorization__c=objauth.id;
        Update objappattach;
        List<Authorizations__c> auth = new List<Authorizations__c>();
        auth.add(objauth); 
        List<id> authidlist = new List<id>();
        authidlist.add(objauth.id); 

/**
*  Executing test scenarios
**/ 
           
        Test.startTest();
         EFLUNIgetRAfromAuthorization.getRegulatedArticles(authidlist);
        Test.stopTest();

    }
}