public with sharing class EFLSelfReportingHelper {
     
    public Map<id, Authorizations__c> authMapFieldTestContext {get; set;}
    // Pupulate the authmap to retrieve the expiration date to handle Field validations that dependant on Authorization
    // // Add additional fields to the SOQL as needed when new fields are required.
    public EFLSelfReportingHelper(set<id> authIds)
    {
        authMapFieldTestContext = new Map<id, Authorizations__c>([select id,Expiration_Date__c from Authorizations__c where id in:authIds ]);
        
    }
    
    public EFLSelfReportingHelper(){}
 
    //Common submission method which updates report summary, related self reporting/report summary history 
    public void submitReport(Report_Summary__c rs)
    {
        try
        {
            EFLEnforceAccessUtility.checkObjectUpdateAccess('Report_Summary__c');
            EFLEnforceAccessUtility.checkObjectUpdateAccess('self_reporting__c');
            EFLEnforceAccessUtility.checkObjectUpdateAccess('Report_Summary_History__c');
            if(rs != null && rs.id != null) {
                rs.status__c = 'Submitted';
                rs.Submitted_Date__c = system.now();
                rs.Certify_and_Submit__c = true;
                rs.Certify_on_Behalf_and_Submit__c = false;
                rs.Certified_By__c = UserInfo.getUserId();
                update rs;
                
                List<Self_Reporting__c> selfReportList = new List<Self_Reporting__c>();
                selfReportList = [Select id,Is_Submitted__c from Self_Reporting__c where Report_Summary__c = :rs.id];
                if(!selfReportList.isEmpty())
                {
                    for(Self_Reporting__c sr:selfReportList)
                    {
                        sr.Is_Submitted__c = true;
                    }
                    update selfReportList;
                }
                List<Report_Summary_History__c> rshList = new List<Report_Summary_History__c>();
                rshList = [Select Id, Name,Report_Summary__c, Status__c 
                           from Report_Summary_History__c where Report_Summary__c = :rs.Id and Status__c = 'Saved'];
                if(!rshList.isEmpty()){
                    Report_Summary_History__c rsh = new Report_Summary_History__c();
                    rsh = rshList[0];
                    rsh.status__c = 'Submitted';
                    rsh.Submitted_Date__c = system.now();
                    upsert rsh;
                }
            }
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingHelper.submitReport()',e);   
            throw e;
        } 
    }    
 
    //Feild Test Report submission method which updates report summary and related self reporting 
    public void submitFeildTestReport(Report_Summary__c rs)
    {
        try
        {
            EFLEnforceAccessUtility.checkObjectUpdateAccess('Report_Summary__c');
            EFLEnforceAccessUtility.checkObjectUpdateAccess('self_reporting__c');
            if(rs != null && rs.id != null) {
                rs.status__c = 'Submitted';
                rs.Submitted_Date__c = system.now();
                rs.Certify_and_Submit__c = true;
                rs.Certify_on_Behalf_and_Submit__c = false;
                rs.Certified_By__c = UserInfo.getUserId();
                update rs;
                List<Self_Reporting__c> selfReportList = new List<Self_Reporting__c>();
                selfReportList = [Select id,Is_Submitted__c from Self_Reporting__c where Report_Summary__c = :rs.id];
                if(!selfReportList.isEmpty())
                {
                    for(Self_Reporting__c sr:selfReportList)
                    {
                        sr.Is_Submitted__c = true;
                    }
                    update selfReportList;
                }
            }
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingHelper.submitFeildTestReport()',e);   
            throw e;
        } 
    }    
       
    //Feild Test Report Validations
    public Map<string, string> validateFieldTestReport(boolean isTrigger, Self_Reporting__c selfReporting, Map<string, string> pageErrorMap, Set<Date> plantingDates)
    {
        try
        {
            if(string.isBlank(selfReporting.Field_Test_Report_Type__c)){
                string errorMessage = EFLGenericUtility.getMessage('FieldTestReportTypeRequired');
                if(isTrigger)
                {
                    selfReporting.Field_Test_Report_Type__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Field_Test_Report_Type__c', errorMessage);
                }
            }
            
            if(string.isBlank(selfReporting.Any_Planting_Material_Harvested__c)){
                string errorMessage = EFLGenericUtility.getMessage('PlantedMaterialHarvestedRequired');
                if(isTrigger)
                {
                    selfReporting.Any_Planting_Material_Harvested__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Any_Planting_Material_Harvested__c', errorMessage);
                }
            }
            else
            {
                if(selfReporting.Any_Planting_Material_Harvested__c == 'Yes' && selfReporting.Anticipated_Harvest_Destruct_Date__c == null){
                    string errorMessage = EFLGenericUtility.getMessage('AnticipatedHarvestDestructDateRequired');
                    if(isTrigger)
                    {
                        selfReporting.Anticipated_Harvest_Destruct_Date__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('Anticipated_Harvest_Destruct_Date__c', errorMessage);
                    }        
                }
                
                if(selfReporting.Any_Planting_Material_Harvested__c == 'Yes' && (string.isBlank(selfReporting.How_was_it_terminated__c)))
                {
                    string errorMessage = EFLGenericUtility.getMessage('OneBothInFieldOffFieldTerminationPresent');
                    if(isTrigger)
                    {
                        selfReporting.How_was_it_terminated__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('How_was_it_terminated__c', errorMessage);
                    }
                }
                
            }
            
            if(selfReporting.Any_Planting_Material_Harvested__c == 'Yes' && (selfReporting.How_was_it_terminated__c == 'All terminated in-field' || selfReporting.How_was_it_terminated__c == 'Both'))
            {
                if(selfReporting.In_field_Termination_Date__c == null)
                {
                    string errorMessage = EFLGenericUtility.getMessage('InFieldTerminationDateIsRequired');
                    if(isTrigger)
                    {
                        selfReporting.In_field_Termination_Date__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('In_field_Termination_Date__c', errorMessage);
                    }                        
                }
                if(selfReporting.In_Field_Description__c == null)
                {
                    string errorMessage = EFLGenericUtility.getMessage('InFieldTerminationDescriptionIsRequired');
                    if(isTrigger)
                    {
                        selfReporting.In_Field_Description__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('In_Field_Description__c', errorMessage);
                    }
                }
            }   
            
            if(selfReporting.How_was_it_terminated__c == 'All terminated off-field' || selfReporting.How_was_it_terminated__c == 'Both')
            {
                checkOffFieldTermValidations(selfReporting, isTrigger, pageErrorMap);
            }
            
            if(selfReporting.Planted_Material_Destroyed_Before_Harves__c == 'Yes')
            {
                checkPreHarvestValidations(selfReporting, isTrigger, pageErrorMap);
            }   
            
            if(selfReporting.Planting_Material_Still_Growing__c == 'Yes')
            {
                checkStillGrowingValidations(selfReporting, isTrigger, pageErrorMap);
            }
            
            if(PlantingDates!=null && !PlantingDates.isEmpty())
            {
                for(Date srDate : PlantingDates){
                    string formatedDateString = srDate.month()+'/'+srDate.day()+'/'+srDate.year();
                    
                    if(selfReporting.Any_Planting_Material_Harvested__c == 'Yes' && selfReporting.Anticipated_Harvest_Destruct_Date__c != null && selfReporting.Anticipated_Harvest_Destruct_Date__c < srDate){
                        string errorMessage = EFLGenericUtility.getMessage('HarvestDateMustBeAfterPlantingDate');
                        if(isTrigger)
                        {
                            selfReporting.Anticipated_Harvest_Destruct_Date__c.addError(errorMessage + ' ' + formatedDateString);
                        }
                        else
                        {
                            pageErrorMap.put('Anticipated_Harvest_Destruct_Date__c', errorMessage + ' ' + formatedDateString);
                        }
                    }
                    
                    if((selfReporting.How_was_it_terminated__c == 'All terminated in-field'|| selfReporting.How_was_it_terminated__c == 'Both') && selfReporting.In_field_Termination_Date__c != null && selfReporting.In_field_Termination_Date__c < srDate)
                    {
                        string errorMessage = EFLGenericUtility.getMessage('InFieldTerminationDateAfterPlantingDate');
                        if(isTrigger)
                        {
                            selfReporting.In_field_Termination_Date__c.addError(errorMessage + ' ' + formatedDateString);
                        }
                        else
                        {
                            pageErrorMap.put('In_field_Termination_Date__c', errorMessage + ' ' + formatedDateString);
                        }
                    }
                    
                    if((selfReporting.How_was_it_terminated__c == 'All terminated off-field' || selfReporting.How_was_it_terminated__c == 'Both')&&(selfReporting.How_was_material_disposed__c  == 'All destroyed'|| selfReporting.How_was_material_disposed__c  == 'Both (some of each)') && selfReporting.Off_Field_Destruction_Date__c != null && selfReporting.Off_Field_Destruction_Date__c < srDate)
                    {
                        string errorMessage = EFLGenericUtility.getMessage('OffFieldDestructionDateAfterPlantingDate');
                        if(isTrigger)
                        {
                            selfReporting.Off_Field_Destruction_Date__c.addError(errorMessage + ' ' + formatedDateString);
                        }
                        else
                        {
                            pageErrorMap.put('Off_Field_Destruction_Date__c', errorMessage + ' ' + formatedDateString);
                        }                        
                    }
                    
                }
            }
            
            if(string.isBlank(selfReporting.Planted_Material_Destroyed_Before_Harves__c)){
                string errorMessage = EFLGenericUtility.getMessage('PlantedMaterialDestroyedBeforeHarvesRequ');
                if(isTrigger)
                {
                    selfReporting.Planted_Material_Destroyed_Before_Harves__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Planted_Material_Destroyed_Before_Harves__c', errorMessage);
                }    
            }
            
            if(string.isBlank(selfReporting.Planting_Material_Still_Growing__c)){
                string errorMessage = EFLGenericUtility.getMessage('PlantingMaterialStillGrowingRequried');
                if(isTrigger)
                {
                    selfReporting.Planting_Material_Still_Growing__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Planting_Material_Still_Growing__c', errorMessage);
                }        
            }
            
            //if((selfReporting.Explanation__c == null || selfReporting.Explanation__c =='') && (selfReporting.Any_Planting_Material_Harvested__c != null || selfReporting.Any_Planting_Material_Harvested__c != '') &&(selfReporting.Planting_Material_Still_Growing__c == '' || selfReporting.Planting_Material_Still_Growing__c == null) && (selfReporting.Planting_Material_Still_Growing__c == ''|| selfReporting.Planting_Material_Still_Growing__c == null))
            if((string.isBlank(selfReporting.Explanation__c)) && (selfReporting.Any_Planting_Material_Harvested__c == 'No'&& selfReporting.Planting_Material_Still_Growing__c == 'No' && selfReporting.Planted_Material_Destroyed_Before_Harves__c == 'No'))
            {
                string errorMessage = EFLGenericUtility.getMessage('ExplanationIsRequired');
                if(isTrigger)
                {
                    selfReporting.Explanation__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Explanation__c', errorMessage);
                } 
            }
            
            if(string.isBlank(selfReporting.Unexpected_Effects_Picklist__c))
            {
                string errorMessage = EFLGenericUtility.getMessage('UnexpectedEffectsIsRequired');
                if(isTrigger)
                {
                    selfReporting.Unexpected_Effects_Picklist__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Unexpected_Effects__c', errorMessage);
                } 
            }
            
            if(string.isBlank(selfReporting.Deleterious_Effects_Picklist__c))
            {
                string errorMessage = EFLGenericUtility.getMessage('DeleteriousEffectsIsRequired');
                if(isTrigger)
                {
                    selfReporting.Deleterious_Effects_Picklist__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Deleterious_Effects__c', errorMessage);
                }
            }  
            
            //if((selfReporting.Unexpected_Effects_Picklist__c=='Yes' || selfReporting.Deleterious_Effects_Picklist__c=='Yes') && string.isBlank(selfReporting.Explanation__c))
            if((selfReporting.Unexpected_Effects_Picklist__c=='Yes') && string.isBlank(selfReporting.Explanation__c))
            {
                string errorMessage = EFLGenericUtility.getMessage('ExplanationIsRequired');
                if(isTrigger) 
                {
                    selfReporting.Explanation__c.addError(errorMessage);
                } 
                else
                {
                    pageErrorMap.put('Explanation__c', errorMessage);
                }  
            } 
            
            if(selfReporting.Deleterious_Effects_Picklist__c=='Yes' && string.isBlank(selfReporting.Deleterious_Effects_Data__c))
            {
                string errorMessage = EFLGenericUtility.getMessage('DeleteriousEffectsDataIsRequired');
                if(isTrigger)
                {
                    selfReporting.Deleterious_Effects_Data__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Deleterious_Effects_Data__c', errorMessage);
                }
            } 
            
            /*if(string.isBlank(selfReporting.Crop_Observations__c))
            {
                string errorMessage = EFLGenericUtility.getMessage('CropObservationsIsRequired');
                if(isTrigger)
                {
                    selfReporting.Crop_Observations__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Crop_Observations__c', errorMessage);
                }
            }*/
            
            if((selfReporting.How_was_it_terminated__c == 'All terminated off-field' || selfReporting.How_was_it_terminated__c == 'Both') && (string.isBlank(selfReporting.How_was_material_disposed__c)))
            {
                string errorMessage = EFLGenericUtility.getMessage('EitherStoredOrOffFieldDestructionSelect');
                if(isTrigger)
                {
                    selfReporting.How_was_material_disposed__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('How_was_material_disposed__c', errorMessage);
                }
            }
            
            //Annual dependency field validations
            if(string.isNotBlank(selfReporting.Field_Test_Report_Type__c) && selfReporting.Field_Test_Report_Type__c == 'Annual (only applicable for multi-year permits)'){
                
                if(string.isBlank(selfReporting.Is_monitoring_volunteers_required_FT__c)){
                    string errorMessage = EFLGenericUtility.getMessage('MonitoringVolunteersFieldTestRequired');
                    if(isTrigger)
                    {
                        selfReporting.Is_monitoring_volunteers_required_FT__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('Is_monitoring_volunteers_required_FT__c', errorMessage);
                    }
                }
                else if(selfReporting.Is_monitoring_volunteers_required_FT__c=='Yes')
                {
                    
                    if(selfReporting.Monitoring_Period_Start__c==null){
                        string errorMessage = EFLGenericUtility.getMessage('MonitorStartDateRequired');
                        if(isTrigger)
                        {
                            selfReporting.Monitoring_Period_Start__c.addError(errorMessage);
                        }
                        else
                        {
                            pageErrorMap.put('Monitoring_Period_Start__c', errorMessage);
                        }
                    }

                    if(selfReporting.Monitoring_Period_End__c==null){
                        string errorMessage = EFLGenericUtility.getMessage('MonitorEndDateRequired');
                        if(isTrigger)
                        {
                            selfReporting.Monitoring_Period_End__c.addError(errorMessage);
                        }
                        else
                        {
                            pageErrorMap.put('Monitoring_Period_End__c', errorMessage);
                        }
                    }

                }

                if(string.isBlank(selfReporting.Is_monitoring_flowering_FT_site__c)){
                    string errorMessage = EFLGenericUtility.getMessage('MonitoringFloweringFieldTestSiteRequired');
                    if(isTrigger)
                    {
                        selfReporting.Is_monitoring_flowering_FT_site__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('Is_monitoring_flowering_FT_site__c', errorMessage);
                    }
                }
                else if(selfReporting.Is_monitoring_flowering_FT_site__c=='Yes')
                {
                    
                    if(string.isBlank(selfReporting.Did_Flowering_Occur__c)){
                        string errorMessage = EFLGenericUtility.getMessage('DidFloweringOccurRequired');
                        if(isTrigger)
                        {
                            selfReporting.Did_Flowering_Occur__c.addError(errorMessage);
                        }
                        else
                        {
                            pageErrorMap.put('Did_Flowering_Occur__c', errorMessage);
                        }
                    }
                    else if(selfReporting.Did_Flowering_Occur__c=='Yes'){

                        if(string.isBlank(selfReporting.Is_flowering_authorized__c)){
                            string errorMessage = EFLGenericUtility.getMessage('FloweringAuthorizedRequired');
                            if(isTrigger)
                            {
                                selfReporting.Is_flowering_authorized__c.addError(errorMessage);
                            }
                            else
                            {
                                pageErrorMap.put('Is_flowering_authorized__c', errorMessage);
                            }
                        }
                        else if(selfReporting.Is_flowering_authorized__c=='Yes'){

                            if(string.isBlank(selfReporting.Required_to_submit_flowering_report__c)){
                                string errorMessage = EFLGenericUtility.getMessage('SubmitFloweringReportRequired');
                                if(isTrigger)
                                {
                                    selfReporting.Required_to_submit_flowering_report__c.addError(errorMessage);
                                }
                                else
                                {
                                    pageErrorMap.put('Required_to_submit_flowering_report__c', errorMessage);
                                }
                            }

                        }

                    }
                    
                }

            }
            
          // W-026141 Added to handle validation logic for Harvest completion date and In-field Termination Completion Date
          if(authMapFieldTestContext!=null)
          {
              if(selfReporting.Anticipated_Harvest_Destruct_Date__c!=null && selfReporting.Anticipated_Harvest_Destruct_Date__c>authMapFieldTestContext.get(selfReporting.Authorization__c).Expiration_Date__c)
                {
                    string errorMessage = EFLGenericUtility.getMessage('HarvestCompletionDate');
                    if(isTrigger)
                    {
                        selfReporting.Anticipated_Harvest_Destruct_Date__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('Anticipated_Harvest_Destruct_Date__c', errorMessage);
                    }
                }  
              if(selfReporting.In_field_Termination_Date__c!=null && selfReporting.In_field_Termination_Date__c > authMapFieldTestContext.get(selfReporting.Authorization__c).Expiration_Date__c )
                {
                    string errorMessage = EFLGenericUtility.getMessage('InfieldTerminationCompletionDate');
                    if(isTrigger)
                    {
                        selfReporting.In_field_Termination_Date__c.addError(errorMessage);
                    }
                    else
                    {
                        pageErrorMap.put('In_field_Termination_Date__c', errorMessage);
                    }
                }  
              
          }
            
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingHelper.validateFieldTestReport()',e);   
            throw e;
        }  
        return pageErrorMap;
    } 
    
    public void checkOffFieldTermValidations(Self_Reporting__c selfReporting, boolean isTrigger, Map<string, string> pageErrorMap) {
        
        if(selfReporting.How_was_material_disposed__c  == 'All stored or contained')
        {
            checkstoredValidations(selfReporting, isTrigger, pageErrorMap);
        } 
        if(selfReporting.How_was_material_disposed__c  == 'All destroyed')
        {
            checkDestroyedValidations(selfReporting, isTrigger, pageErrorMap);
        }
        if(selfReporting.How_was_material_disposed__c  == 'Both (some of each)')
        {
            checkstoredValidations(selfReporting, isTrigger, pageErrorMap);
            checkDestroyedValidations(selfReporting, isTrigger, pageErrorMap);
        }
        
    }
    
    public void checkstoredValidations(Self_Reporting__c selfReporting, boolean isTrigger, Map<string, string> pageErrorMap) {
        if(selfReporting.Stored_Quantity__c==null){ 
            string errorMessage = EFLGenericUtility.getMessage('StoredQuantityIsRequired');
            if(isTrigger)
            {
                selfReporting.Stored_Quantity__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Stored_Quantity__c', errorMessage);
            }
        }
        else
        {
            if(selfReporting.Stored_Quantity__c < 0){ 
                string errorMessage = EFLGenericUtility.getMessage('StoredQuantityGreaterThanZero');
                if(isTrigger)
                {
                    selfReporting.Stored_Quantity__c.addError(errorMessage);
                }
                else
                {
                    pageErrorMap.put('Stored_Quantity__c', errorMessage);
                }
            }
            
        }
        if(selfReporting.Units__c == null)
        {
            string errorMessage = EFLGenericUtility.getMessage('StoredUnitsIsRequired');
            if(isTrigger)
            {
                selfReporting.Units__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Units__c', errorMessage);
            }
        }
        if(selfReporting.Stored_Material_Type__c == null)
        { 
            string errorMessage = EFLGenericUtility.getMessage('StoredMaterialTypeIsRequired');
            if(isTrigger)
            {
                selfReporting.Stored_Material_Type__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Stored_Material_Type__c', errorMessage);
            }
        }
        if(selfReporting.Stored_Description__c == null)
        {
            string errorMessage = EFLGenericUtility.getMessage('StoredDescriptionIsRequired');
            if(isTrigger)
            {
                selfReporting.Stored_Description__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Stored_Description__c', errorMessage);
            }
        }
    }
    
    public void checkDestroyedValidations(Self_Reporting__c selfReporting, boolean isTrigger, Map<string, string> pageErrorMap) {
        if(selfReporting.Off_Field_Destruction_Date__c == null)
        { 
            string errorMessage = EFLGenericUtility.getMessage('OffFieldDestructionDateIsRequired');
            if(isTrigger)
            {
                selfReporting.Off_Field_Destruction_Date__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Off_Field_Destruction_Date__c', errorMessage);
            }
        }
        if(selfReporting.Off_Field_Description__c == null)
        {
            string errorMessage = EFLGenericUtility.getMessage('OffFieldDescriptionIsRequired');
            if(isTrigger)
            {
                selfReporting.Off_Field_Description__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Off_Field_Description__c', errorMessage); 
            }
        }
    }
    
    public void checkPreHarvestValidations(Self_Reporting__c selfReporting, boolean isTrigger, Map<string, string> pageErrorMap) {
       if(selfReporting.Before_Harvest_Destruction_Date__c == null)
       { 
            string errorMessage = EFLGenericUtility.getMessage('BeforeHarvestDestructionDateRequired');
            if(isTrigger)
            {
                selfReporting.Before_Harvest_Destruction_Date__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Before_Harvest_Destruction_Date__c', errorMessage);
            }  
       }
       if(selfReporting.Before_Harvest_Description__c == null)
       {
            string errorMessage = EFLGenericUtility.getMessage('BeforeHarvestDescriptionRequired');
            if(isTrigger)
            {
                selfReporting.Before_Harvest_Description__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Before_Harvest_Description__c', errorMessage);
            }
       }
    }
    
    public void checkStillGrowingValidations(Self_Reporting__c selfReporting, boolean isTrigger, Map<string, string> pageErrorMap) {
       if(selfReporting.Still_Growing_Quantity__c == null)
       { 
            string errorMessage = EFLGenericUtility.getMessage('StillGrowingQuantityRequired');
            if(isTrigger)
            {
                selfReporting.Still_Growing_Quantity__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Still_Growing_Quantity__c', errorMessage);
            }    
       }
       if(selfReporting.Still_Growing_Description__c == null)
       {
           string errorMessage = EFLGenericUtility.getMessage('StillGrowingDescriptionRequired');
            if(isTrigger)
            {
                selfReporting.Still_Growing_Description__c.addError(errorMessage);
            }
            else
            {
                pageErrorMap.put('Still_Growing_Description__c', errorMessage);
            } 
       }
    }
    
    //Get planting start dates for field test self report by locations
    public map<Id, set<Date>> locationIdplantingDatesMap(set<id> authorizationIds, Set<Id> locationIds)
    {
        map<Id, set<Date>> locationIdPlantingDatesMap = new map<Id, set<Date>>(); 
        try
        {
            list <Report_Summary__c> plantingReportSummaryList = new list <Report_Summary__c>();
            plantingReportSummaryList = [select id,(select id,Release_Record_ID__c,Planting_ID__c, start_date__C from self_reporting__r where RecordType.Name = 'Planting/Release Reports' and is_no_planting__C = false) from Report_Summary__c where Authorization__c in:authorizationIds and report_type__c = 'Planting/Release Reports'];
            for (Report_Summary__c rs : plantingReportSummaryList) {
                for (self_reporting__c sr : rs.self_reporting__r) {
                    //if(!String.isEmpty(selfReporting.Release_Record_ID__c) && sr.Release_Record_ID__c == selfReporting.Release_Record_ID__c) {  
                    if(sr.Release_Record_ID__c!=null && locationIds.contains(sr.Release_Record_ID__c)) {  
                        set<Date> tempSet = new set<Date>();
                        if(locationIdplantingDatesMap.get(sr.Release_Record_ID__c)!=null)
                        {
                            tempSet = locationIdplantingDatesMap.get(sr.Release_Record_ID__c);
                        }
                        tempSet.add(date.valueOf(sr.start_date__c));
                        locationIdplantingDatesMap.put(sr.Release_Record_ID__c, tempSet);
                    }
                }
            }
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLSelfReportingHelper.srIdplantingDatesMap()',e);   
            throw e;
        }    
        return locationIdPlantingDatesMap;
    }
    
}