@isTest
public class IncidentAddLocationsControllerTest {
    
    @testSetup 
    static void setupTestData(){
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        testDataBRS.insertcustomsettings();
        CARPOL_UNI_DisableTrigger__c dt = new  CARPOL_UNI_DisableTrigger__c(Name = 'CARPOL_BRS_LocationsTrigger', Disable__c = true);
        Insert dt;
        Application__c app = testDataBRS.newapplication();
        Authorizations__c Auth = testDataBRS.newAuth(app.id);
        AC__c lineItemz = testDataBRS.newLineItem('Release',app);
        lineItemz.Authorization__c = auth.id;
        update lineItemz;
        
        Country__c testCountry = testDataBRS.newcountrywithassoc();
        Level_1_Region__c testLevelRegion1 = testDataBRS.newlevel1region(testCountry.Id);
        Level_2_Region__c testLevelRegion2 = testDataBRS.newlevel2region(testLevelRegion1.Id);
        /*
        Facility__c fclity = testDataAC.newfacility('Domestic Port');
        fclity.Name = 'Testing Facility';
        update fclity;
        */
        String InsRecordTypeId = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Test.startTest();
        List<Location__c> locs = new List<Location__c>();
        Location__c orgloc = createLocation('Origin Location');
        orgloc.Country__c = testCountry.id;
        orgloc.Line_Item__c = lineItemz.id;
        orgloc.State__c =  testLevelRegion1.id;
        orgloc.Level_2_Region__c = testLevelRegion2.id; 
        locs.add(orgloc);
        insert locs;
        
        Inspection__c Ins = new Inspection__c();
        Ins.RecordTypeId = InsRecordTypeId;
        //Ins.Facility__c = fclity.id;
        Ins.Program__c = 'BRS';
        Ins.Authorization__c = auth.id;
        insert Ins;
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Authorization__c = auth.id;
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Incident_Date__c = date.today(); 
        objInc.Incident_Reported_Date__c = date.today(); 
        objInc.Incident_Discovery_Date__c = Date.today();
        insert objInc;
    }
    
    static Location__c createLocation(String recordType) {
        Location__c orgloc = new Location__c(
            Name='testloc',
            RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId(),
            Contact_Name1__c = 'Test',Primary_Contact_Last_Name__c = 'LName', Day_Phone__c = '555-555-5555',Status__c='Review Completed',
            Applicant_Instructions__c='Test Instructions'
            );
        return orgloc;
    }
    
    @isTest
    static void executeScenario(){
        Profile p = EFLTestDataFactoryBulk.getProfileByName('BRS ROP Reviewer');
        User brsReviewer = EFLTestDataFactoryBulk.getUser(p);
        Incident__c oIncident = [Select Id,Authorization__c From Incident__c limit 1];
        List<Location__c> recordSetVar = [Select Id From Location__c];
        Test.startTest();
        system.runAs(brsReviewer){
            Test.setCurrentPage(Page.Incident_Add_Locations);
            ApexPages.currentPage().getParameters().put('id',oIncident.id);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(recordSetVar);
            IncidentAddLocationsController con = new IncidentAddLocationsController(stdSetController);       
            
            //getAvailable Locations
            List<IncidentAddLocationsController.LocationWrapper> availableList=con.getAvailableLocations();
            System.assertEquals(1, availableList.size());
            con.saveLocations();
            //select a location
            con.selectedLocationId = availableList.get(0).location.Id;
            con.selectLocation();
            List<IncidentAddLocationsController.LocationWrapper> selectedList = con.getSelectedLocations();
            System.assertEquals(1, selectedList.size());
            //remove selected locations
            con.selectedLocationId = selectedList.get(0).location.Id;
            con.removeSelectedLocation(); 
            //select a location
            con.selectedLocationId = availableList.get(0).location.Id;
            con.selectLocation();
            selectedList = con.getSelectedLocations();            
            //Save Selected location to Incident
            con.saveLocations();
            
        }
    }
}