public inherited sharing class EFLAdditionalContactDetailsRepository {

    // Obtain Location records using line Item ID used for ***CLONING Additional Contacts ONLY***   
    public static List<Additional_Contact_Detail__c> selectByLineItemIDforClone(Id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Additional_Contact_Detail__c');
            return [SELECT Account__c, Contact__c, 
                           Type_Of_Address__c, Last_Name__c, 
                           Type_of_Associated_Contact__c
                      FROM Additional_Contact_Detail__c 
                     WHERE Line_Item__c=:lineItemID ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLAdditionalContactDetailsRepository.selectByLineItemIDforClone()',e);                
           } 
         return null;
    }  
            
        
}