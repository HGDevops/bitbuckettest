public class EFLGenericWizardActivityProcessor implements EFLIWizardActivityProcessor {
    
    public List<SelectOption> loadOptions(EFLWizardActivity wizardActivity){
        
        return EFLWizardActivityUtility.getOptions(wizardActivity);
        
    }
   
    public void evaluateResult(EFLWizardActivity wizardActivity){
        try
        {
            EFLWizardActivityUtility.setSelectedOptionByValue(wizardActivity);
        }
        catch(Exception ex)
        {
            
        }
    }
    
    

}