public without sharing class CARPOL_Application_ManagedSharing {
    
    public static Boolean afterApplicationInsert(Id userId, List<Application__c> applications){
       
        List<sObject> shares = new List<sObject>();
        
        try{
            
            for(Application__c app: applications){
                sObject dynObject= Schema.getGlobalDescribe().get('Application__Share').newSObject();
                dynObject.put(Schema.Application__Share.ParentId, app.id);
                dynObject.put(Schema.Application__Share.UserOrGroupId, userId);
                dynObject.put(Schema.Application__Share.AccessLevel, 'Edit');
                dynObject.put(Schema.Application__Share.RowCause, Schema.Application__Share.RowCause.PartnerContact__c);
                shares.add(dynObject);
            }   
        }
        catch(Exception e){
            System.debug('Applications >>> ' + e.getMessage());
        }
        
        if(shares.size() == 0)
            return false;
        else{
            insert shares;
            return true;
        }
        
    }
    
    public static Boolean afterLineItemInsert(Id userId, List<AC__c>Lines){
       
        List<sObject> Lineshares = new List<sObject>();
        
        try{
           
           system.debug('Inside line apex sharing');
               for(AC__c Line: Lines){
                        sObject LinedynObject= Schema.getGlobalDescribe().get('AC__Share').newSObject();
                        LinedynObject.put(Schema.AC__Share.ParentId, Line.id);
                        LinedynObject.put(Schema.AC__Share.UserOrGroupId, userId);
                        LinedynObject.put(Schema.AC__Share.AccessLevel, 'Edit');
                        LinedynObject.put(Schema.AC__Share.RowCause, Schema.AC__Share.RowCause.PartnerContact__c);
                        Lineshares.add(LinedynObject);
                    }
            
        }
        catch(Exception e){
            System.debug('Lines >>> ' + e.getMessage());
        }
        
        if(Lineshares.size() == 0)
            return false;
        else{
            insert Lineshares;
            return true;
        }
    }
    
   public static Boolean afterAuthInsert(Id userId, List<Authorizations__c> Authorizations){
       
        List<sObject> shares = new List<sObject>();
        
        try{
            
            for(Authorizations__c auth: Authorizations){
                sObject dynObject= Schema.getGlobalDescribe().get('Authorizations__Share').newSObject();
                dynObject.put(Schema.Authorizations__Share.ParentId, auth.id);
                dynObject.put(Schema.Authorizations__Share.UserOrGroupId, userId);
                dynObject.put(Schema.Authorizations__Share.AccessLevel, 'Edit');
                dynObject.put(Schema.Authorizations__Share.RowCause, Schema.Authorizations__Share.RowCause.PartnerContact__c);
                shares.add(dynObject);
            }
            
        }
        catch(Exception e){
            System.debug('Applications >>> ' + e.getMessage());
        }
        
        if(shares.size() == 0)
            return false;
        else{
            insert shares;
            return true;
        }
        
    }
    
    //share with the group SPRO/SPHD
    public static Boolean afterRegulationInsert(List<Regulation__c> regulations){
        
        Id groupId = returnGroupId(UserInfo.getUserId());
        
        if(groupId == null)
            return false;
        
        List<sObject> shares = new List<sObject>();
        
        try{
            
            for(sObject reg: regulations){
                sObject dynObject= Schema.getGlobalDescribe().get('Regulation__share').newSObject();
                dynObject.put(Schema.Regulation__share.ParentId, reg.id);
                dynObject.put(Schema.Regulation__share.UserOrGroupId, groupId);
                dynObject.put(Schema.Regulation__share.AccessLevel, 'Edit');
                dynObject.put(Schema.Regulation__share.RowCause, Schema.Regulation__share.RowCause.Manual);
                shares.add(dynObject);
            }
            
        }
        catch(Exception e){
            System.debug('Applications >>> ' + e.getMessage());
        }
        
        if(shares.size() == 0)
            return false;
        else{
            insert shares;
            return true;
        }
    }
    
    public static Id returnGroupId(Id userId){
        
        User u = [SELECT Id, ContactID, Name, UserType, Profile.UserLicense.Name FROM User WHERE ID  = : UserID LIMIT 1];
        if(u.UserType != 'PowerPartner')
            return null;
        Contact c = [select id, name, recordTypeID, SPRO_Public_Group__c, SPHD_Public_Group__c from Contact where id =: u.ContactId limit 1];
        Id sproTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('State SPRO').getRecordTypeId();
        Id sphdTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('State SPHD').getRecordTypeId();
        
        Id groupId;
        if(c.recordTypeID == sproTypeId)
            return [select id from Group where name =: c.SPRO_Public_Group__c].id;
        else if(c.recordTypeID == sphdTypeId)
            return [select id from Group where name =: c.SPHD_Public_Group__c].id;
        
        else
            return null;
        
    }
    

    
    

}