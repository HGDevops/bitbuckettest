@isTest
public class InspectionStageProgressBarControllerTest {
    private static testMethod void test_inspection_Ctrl() {
        Domain__c objDom = new Domain__c();
        objDom.Name = 'BRS';
        insert objDom;
        
        Program_Prefix__c	objPre = new Program_Prefix__c();
        objPre.Name= 'BRS Inspection';
        objPre.Inspection_Related__c = true;
        objPre.Program__c = objDom.id;
        insert objPre;
        
        Inspection__c Ins = new Inspection__c();
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Ins.RecordTypeId = IncRecTypeID;
        Ins.Stage__c = 'Inspection Report';
        Ins.status__c='Cancelled';
        Ins.Reason_for_Cancellation__c = 'Test Cancellation';    
        //system.debug('val'+Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId());
        insert Ins;
        
        ApexPages.CurrentPage().getparameters().put('id', Ins.id);              
        Apexpages.StandardController sc = new Apexpages.StandardController(Ins);
        InspectionStageProgressBarController ext = new InspectionStageProgressBarController(sc); 
        ext.moveToFirstStage();
        ext.moveToNextStage();
        ext.updateStage('Inspection Report');
        
        Ins.status__c = 'Responses Submitted';
        update Ins; 
        ext = new InspectionStageProgressBarController(sc); 
        
        ext.moveToNextStage();
        
        //Error Path
        try
        {
            ext.currentStageIndex = 100;
            ext.moveToNextStage();
        }catch(exception ex){} 
        
    }
    private static testMethod void test_incident_Ctr2() {
        Domain__c objDom = new Domain__c();
        objDom.Name = 'BRS';
        insert objDom;
        
        Program_Prefix__c	objPre = new Program_Prefix__c();
        objPre.Name= 'BRS Inspection';
        objPre.Inspection_Related__c = true;
        objPre.Program__c = objDom.id;
        insert objPre;
        Inspection__c Ins=new Inspection__c();
        Ins.status__c='Cancelled';
        Ins.Reason_for_Cancellation__c = 'Test Cancellation';    
        Ins.Stage__c = 'Inspection Assignment';
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Ins.RecordTypeId = IncRecTypeID;
        Ins.Workflow_Number__c = null;
        insert Ins;
        
        ApexPages.CurrentPage().getparameters().put('id', Ins.id);             
        Apexpages.StandardController sc = new Apexpages.StandardController(Ins);
        InspectionStageProgressBarController ext = new InspectionStageProgressBarController(sc); 
        
        ext.moveToFirstStage();
        ext.moveToNextStage();
        ext.updateStage('Inspection Assignment');
        
    }
}