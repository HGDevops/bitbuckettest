public with sharing class EFL_NewRegulationController {
    
    public User currentUser {get; set;}
    public Contact currentContact {get;set;}
    public Regulation__c regulation {get;set;}
    public final String REGULATION_TYPE = 'State Conditions';
    
    public Regulation__c existingRegulation {get;set;}
    
    public EFL_NewRegulationController() {
        
        currentUser = [SELECT Id, ContactID, Name, Profile.UserLicense.Name FROM User WHERE ID  = : UserInfo.getUserID() LIMIT 1];
        currentContact = [select Id, name, State__c from Contact where id =: currentUser.contactId limit 1];
        regulation = new Regulation__c();
        regulation.type__c = REGULATION_TYPE;
        regulation.State__c = currentContact.State__c;
        regulation.recordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('State Regulations').getRecordTypeId();
        
    }
    
    public EFL_NewRegulationController(ApexPages.StandardController controller){
        
        existingRegulation = (Regulation__c)controller.getRecord();
        
        existingRegulation = [select id, name, Short_Name__c, Title__c, Custom_Name__c, Type__c, Regulation_Description__c
                              from Regulation__c where id =: existingRegulation.Id];
        
    }
    
    public PageReference save(){
        
        insert regulation;
        return returnToTemplatePage();
    }
    
    public PageReference updateRegulation(){
        
        update existingRegulation;
        return returnToTemplatePage();
    }
    
    public PageReference returnToTemplatePage(){
        
        PageReference ref = new PageReference('/RequirementsTemplate');
        ref.setRedirect(true);
        return ref;
    }
    
    
}