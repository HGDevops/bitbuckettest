/**
 * Inspection Engine Interface
 */
public interface EFLIInspectionEngine { 

   void loadActivities(inspection__c inspectionRecord,list<sObject> activityList);
    
}