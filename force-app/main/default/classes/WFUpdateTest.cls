@isTest
private with sharing class WFUpdateTest{
    
    public class HttpCalloutMockImpl implements HttpCalloutMock{
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"test":"me"}');
            res.setStatusCode(200);
            return res;
        }
    }
    
    
    private static testMethod void test() {
         
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Datetime inPast = Datetime.now().addDays(-20);
          
          
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c li = testData.newlineitem('Personal Use', objapp);
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'Reviewer__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!Reviewer__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,Reviewer__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        Authorizations__c objauth = new Authorizations__c();
        objauth.Application__c = objapp.id;
        objauth.RecordTypeID = ACauthRecordTypeId;
        objauth.Status__c = 'Submitted';
        objauth.Date_Issued__c = date.today();
        objauth.Applicant_Alternate_Email__c = 'test@test.com';
        objauth.UNI_Zip__c = '32092';
        objauth.UNI_Country__c = 'United States';
        objauth.UNI_County_Province__c = 'Duval';
        objauth.Effective_Date__c = date.today()+30;
        objauth.BRS_Proposed_Start_Date__c = date.today()+30;
        objauth.BRS_Proposed_End_Date__c = date.today()+40;
        objauth.CBI__c = 'CBI Text';
        objauth.Application_CBI__c = 'No';
        objauth.Applicant_Alternate_Email__c = 'email2@test.com';
        objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
        objauth.AC_Applicant_Email__c = 'email2@test.com';
        objauth.AC_Applicant_Fax__c = '(904) 123-2345';
        objauth.AC_Applicant_Phone__c = '(904) 123-2345';
        objauth.AC_Organization__c = 'ABC Corp';
        objauth.Means_of_Movement__c = 'Hand Carry';
        objauth.Biological_Material_present_in_Article__c = 'No';
        objauth.If_Yes_Please_Describe__c = 'Text';
        objauth.Applicant_Instructions__c = 'Make corrections';
        objauth.BRS_Number_of_Labels__c = 10;
        objauth.BRS_Purpose_of_Permit__c='Traditional';
        objauth.Authorization_Type__c='Permit';
        objauth.BRS_Introduction_Type__c='Import';
        objauth.Documents_Sent_to_Applicant__c = false;
        objauth.Status__c = 'Approved';
        insert objauth;          

        li.Authorization__c = objauth.id;
        update li;
          
        Reviewer__c objRev = new Reviewer__c();
        objRev.Authorization__c = objauth.id;
        objRev.Notes_to_State__c = 'test Notes';
        insert objRev;
          
        Workflow_Task__c objWflow = new Workflow_Task__c();
        objWflow.Authorization__c = objAuth.id;
        objWflow.Is_violator__c = 'No';
        objWflow.Status__c = 'Not Started';
        objWflow.Program__c = 'AC';
        insert objWflow;
          
        Test.setCreatedDate(objRev.id, inPast);
         
        Test.startTest();                                
            Test.setMock(HttpCalloutMock.class, new HttpCalloutMockImpl());
            WFUpdate wf = new WFUpdate();
            Database.executeBatch(wf,200);
        Test.stopTest();    
                                                                 
      }
}