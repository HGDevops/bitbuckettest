/*
 * Purpose: Central place to access Construct/Genotypes/Phenotypes object from database with different set of filter parameters.
*/
public with sharing class EFLLinkRegulatedArticleRepository {

   // Obtain Link Regulated Article records using line Item ID for ***CLONING ONLY*** 
   // Note: Do not add any reference fields in this Query   
    public static List<Link_Regulated_Articles__c> selectByLineItemID(id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Link_Regulated_Articles__c');
           return [SELECT Name,Regulated_Article_CBI__c,
                           Regulated_Article__r.name,Regulated_Article__r.Category_Group_Ref__r.name,
                           Regulated_Article__r.Additional_Scientific_Name__c,Scientific_Name_Text__c,
                           Regulated_Article__r.Additional_Common_Name__c,Common_Name__c,Common_Name_Text__c,
                           Scientific_Name__c,Cultivar_and_or_Breeding_Line__c,
                           Corrections_Required__c, status__C ,Regulated_Article_Text__c
                      FROM Link_Regulated_Articles__c 
                     WHERE Line_Item__c =:lineItemID];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLLinkRegulatedArticleRepository.selectByLineItemID()',e);                
        } 
         return null;
    }

   // Obtain Link Regulated Article records using line Item ID     
    public static List<Link_Regulated_Articles__c> selectLRAByLineItemID(id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Link_Regulated_Articles__c');
           return [SELECT id,Name,Regulated_Article_CBI__c,Link_Regulated_Articles__c.Line_Item__r.status__c,
                           Link_Regulated_Articles__c.Line_Item__r.OwnerId, Regulated_Article__r.name,Regulated_Article__r.Category_Group_Ref__r.name, 
                           Regulated_Article__r.Additional_Scientific_Name__c, CreatedById,
                           Regulated_Article__r.Additional_Common_Name__c,Common_Name__c,Common_Name_Text__c,Regulated_Article_Text__c,
                           Scientific_Name__c,Scientific_Name_Text__c,Cultivar_and_or_Breeding_Line__c,Status_Graphical__c,status__c,EnableRAUpdateAction__c,EnableRADeleteAction__c,
                           Corrections_Required__c 
                      FROM Link_Regulated_Articles__c 
                     WHERE Line_Item__c =:lineItemID];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLLinkRegulatedArticleRepository.selectLRAByLineItemID()',e);                
        } 
         return null;
    }

   // Obtain Link Regulated Article records using line Item ID     
    public static  Link_Regulated_Articles__c selectByID(id linkRAID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Link_Regulated_Articles__c');
           return [SELECT id,Name,Regulated_Article_CBI__c,Link_Regulated_Articles__c.Line_Item__r.status__c,Link_Regulated_Articles__c.Line_Item__r.Does_This_Application_Contain_CBI__c,
                           Regulated_Article__r.name,Regulated_Article__r.Category_Group_Ref__r.name,
                           Regulated_Article__r.Additional_Scientific_Name__c,
                           Regulated_Article__r.Additional_Common_Name__c,Common_Name__c,Common_Name_Text__c,
                           Scientific_Name__c,Scientific_Name_Text__c,Cultivar_and_or_Breeding_Line__c,EnableRAUpdateAction__c,Regulated_Article_Text__c,
                           Corrections_Required__c 
                      FROM Link_Regulated_Articles__c 
                     WHERE ID =:linkRAID];
        }catch(exception e){
               EFLErrorLog.createErrorLog('EFLLinkRegulatedArticleRepository.selectLRAByLineItemID()',e);                
        } 
         return null;
    }
     
}