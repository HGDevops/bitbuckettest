@isTest(seealldata=true)
private class CARPOL_Application_ManagedSharing_Test {

	private static testMethod void unitTest() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
       
        Boolean result;
        Id userId = UserInfo.getUserId();
        Application__c objapp = new Application__c();
        List<Application__c> lstApp = new List<Application__c>();
        objapp = testData.newapplication();
        lstApp.add(objapp);
        AC__c objLineItem = new AC__c();
        List<AC__c> lstLineItem = new List<AC__c>();
        objLineItem = testData.newLineItem('Resale/Adoption', objapp);
        lstLineItem.add(objLineItem);
        Authorizations__c objauth = new Authorizations__c();
        List<Authorizations__c> lstAuth = new List<Authorizations__c>();
        objauth = testData.newAuth(objapp.id);
        lstAuth.add(objauth);
        
        Test.startTest();
        
        result = CARPOL_Application_ManagedSharing.afterApplicationInsert(userId, lstApp);
        result = CARPOL_Application_ManagedSharing.afterLineItemInsert(userId, lstLineItem);
        result = CARPOL_Application_ManagedSharing.afterAuthInsert(userId, lstAuth);
        
        Profile p = [SELECT Id FROM Profile WHERE Name IN ('State Reviewer','eFile APHIS Collaborator') limit 1];
	    User u = [select id from User where ProfileId =: p.Id and isActive = true limit 1];
        System.runAs(u){
            Regulation__c reg = new Regulation__c(Short_Name__c = 'test 123', 
                Regulation_Description__c = 'testing 1234', title__c = 'coolest', Custom_Name__c = 'craziest');
            insert reg;
            //Regulation__share regShare = [select id, rowCause from Regulation__share where parentId =: reg.Id limit 1];
            System.assert(reg != null);
        }
        
        p = [SELECT Id FROM Profile WHERE Name='State Plant Health Director'];
	    //u = [select id from User where ProfileId =: p.Id and isActive = true limit 1];
        System.runAs(u){
            Regulation__c reg = new Regulation__c(Short_Name__c = 'test 123', 
                Regulation_Description__c = 'testing 1234', title__c = 'coolest', Custom_Name__c = 'craziest');
            insert reg;
            //Regulation__share regShare = [select id, rowCause from Regulation__share where parentId =: reg.Id limit 1];
            System.assert(reg != null);
        }
        Test.stopTest();
        
	}

}