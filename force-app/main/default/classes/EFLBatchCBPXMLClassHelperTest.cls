@isTest
public class EFLBatchCBPXMLClassHelperTest {

    @isTest 
    public static void getDocumentNull(){
        String docId = '11111';
        Blob document = EFLBatchCBPXMLClassHelper.getDocument(docId, new SpringCMService(null));
        System.assertEquals(null, document);
    }
    
    @isTest
    public static void createAttachmentBlob(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        EFLIntegrationLog__c log = new EFLIntegrationLog__c();
        insert log;
        Blob attachmentBlob = Blob.valueOf('testblobdata');
        String attachmentName = 'testAttachment';
        Attachment attachment = EFLBatchCBPXMLClassHelper.createAttachment(attachmentName, log.Id, attachmentBlob, null);
        System.assertEquals(log.Id, attachment.ParentId);
        System.assertEquals(attachmentBlob, attachment.Body);
        System.assertEquals(attachmentName, attachment.Name);
    }
    
    @isTest
    public static void createAttachmentXml(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        EFLIntegrationLog__c log = new EFLIntegrationLog__c();
        insert log;
        String testXml = 'testxml';
        String attachmentName = 'testAttachment';
        Attachment attachment = EFLBatchCBPXMLClassHelper.createAttachment(attachmentName, log.Id, null, testXml);
        System.assertEquals(log.Id, attachment.ParentId);
        System.assertEquals(Blob.valueOf(testXml), attachment.Body);
        System.assertEquals(attachmentName, attachment.Name);
    }
    
    @isTest
    public static void createHeaderSuccess(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        EFLIntegrationLog__c log = new EFLIntegrationLog__c();
        insert log;
        String dateString = Datetime.now().format('mm-dd-yyyy');
        XmlStreamWriter xmlStream = EFLBatchCBPXMLClassHelper.createHeader(log.Id, dateString);
        System.assertNotEquals(null, xmlStream);
        System.assertEquals(true, xmlStream.getXmlString().contains('UTF-8'));
        System.assertEquals(true, xmlStream.getXmlString().contains(log.Id));
        System.assertEquals(true, xmlStream.getXmlString().contains(dateString));
    }

    @isTest
    public static void createAttachmentListSuccess(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        EFLIntegrationLog__c log = new EFLIntegrationLog__c();
        insert log;
        
        List<Authorizations__c> auths = new List<Authorizations__c>();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='555';
        Insert objPrefix;
        
        Map<String, Blob> blobToAuth = new Map<String, Blob>();
        auth = [SELECT Name FROM Authorizations__c WHERE Id = :auth.Id];
        auth.Application_Type__c = 'Renewal';
        auth.Expiration_Date__c = Date.today().addDays(10);
        auth.Effective_Date__c = Date.today();
        auth.Status__c = 'Cancelled';
        auth.Permit_Number__c = '555-A1';
        update auth;
        blobToAuth.put(auth.Name, Blob.valueOf('test'));
        auths = EFLIntegrationDocReferenceRepository.selectByAuth(blobToAuth);
        
        List<Attachment> attachments = EFLBatchCBPXMLClassHelper.createAttachmentList(auths, log.Id, blobToAuth, 1);
        System.assertEquals(3, attachments.size());
        
    }
    
    @isTest
    public static void createAttachmentListMultipleAuthsSuccess(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        EFLIntegrationLog__c log = new EFLIntegrationLog__c();
        insert log;
        
        List<Authorizations__c> auths = new List<Authorizations__c>();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        Authorizations__c auth2 = testData.newAuth(app.id);
        Authorizations__c auth3 = testData.newAuth(app.id);
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='150';
        Insert objPrefix;
                
        Map<String, Blob> blobToAuth = new Map<String, Blob>();
        auth = [SELECT Name FROM Authorizations__c WHERE Id = :auth.Id];
        auth2 = [SELECT Name FROM Authorizations__c WHERE Id = :auth2.Id];
        auth3 = [SELECT Name FROM Authorizations__c WHERE Id = :auth3.Id];
        auth.Application_Type__c = 'Renewal';
        auth.Expiration_Date__c = Date.today().addDays(10);
        auth.Effective_Date__c = Date.today();
        auth.Status__c = 'Cancelled';
        auth.Permit_Number__c = '150-Z1BMRP5-A1';
        auth2.Application_Type__c = 'Renewal';
        auth2.Expiration_Date__c = Date.today().addDays(10);
        auth2.Effective_Date__c = Date.today();
        auth2.Status__c = 'Suspended';
        auth2.Permit_Number__c = '150-Z1BMRP6';
        auth3.Application_Type__c = 'Renewal';
        auth3.Expiration_Date__c = Date.today().addDays(10);
        auth3.Effective_Date__c = Date.today();
        auth3.Status__c = 'Superceded';
        auth3.Permit_Number__c = '150-Z1BMRP7';
        update auth;
        update auth2;
        update auth3;
        blobToAuth.put(auth.Name, Blob.valueOf('test'));
        blobToAuth.put(auth2.Name, Blob.valueOf('test'));
        blobToAuth.put(auth3.Name, Blob.valueOf('test'));
        auths = EFLIntegrationDocReferenceRepository.selectByAuth(blobToAuth);
        
        List<Attachment> attachments = EFLBatchCBPXMLClassHelper.createAttachmentList(auths, log.Id, blobToAuth, 1);
        System.assertEquals(5, attachments.size());
    }
    
    @isTest
    public static void formatSingleDigitString(){
        String str = EFLBatchCBPXMLClassHelper.formatSingleDigitString(1);
        System.assertEquals('01', str);
    }
    
    @isTest
    public static void formatSingleDigitStringDoubleDigit(){
        String str = EFLBatchCBPXMLClassHelper.formatSingleDigitString(10);
        System.assertEquals('10', str);
    }

	@isTest
    public static void createMarkerFile(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        EFLIntegrationLog__c log = new EFLIntegrationLog__c();
        insert log;
        
    	Attachment att =  EFLBatchCBPXMLClassHelper.createMarkerFile(log.Id, true, 10);
        System.assertNotEquals(null, att);
        System.assertEquals(log.Id, att.ParentId);
    }
    
    @isTest 
    public static void validateStatusFalse(){
        Authorizations__c auth = new Authorizations__c();
        auth.Status__c = 'Submitted';
        boolean isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(false, isValid);
        
        auth.Status__c = null;
        isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(false, isValid);
    }
    
    @isTest 
    public static void validateStatusTrue(){
        Authorizations__c auth = new Authorizations__c();
        auth.Status__c = 'Issued';
        boolean isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(true, isValid);        
        
        auth.Status__c = 'Revoked';
        isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(true, isValid);
        
        auth.Status__c = 'Cancelled';
        isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(true, isValid);
        
        auth.Status__c = 'Superceded';
        isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(true, isValid);
        
        auth.Status__c = 'Suspended';
        isValid = EFLBatchCBPXMLClassHelper.validateStatus(auth);
        System.assertEquals(true, isValid);
    }
}