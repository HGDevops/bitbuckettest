public with sharing class Portal_Associated_Contact_Create{    
    public String accountName{get;set;}
    //public String emailAddress{get;set;}
    //public String firstname{get;set;}
    public Applicant_Contact__c appCon{get;set;}
    public User u{get;set;}
    public Boolean showMessage {get;set;}
    //public Account v{get;set;}
    public string recordTypeName{get;set;}
    public string indOrgCon {get; set;}
    public Boolean showFirstName{get;set;}
    public Portal_Associated_Contact_Create(ApexPages.StandardController stdController){
        showMessage = false;
        try {
            showFirstName = true;
            indOrgCon = apexpages.currentpage().getparameters().get('indOrgCon'); // VV added for 17403
            if(indOrgCon == null)
                indOrgCon = 'Con';
            //u=[SELECT id,contactId,accountId,account.Name FROM USER where id=:userInfo.getUserId() limit 1] ;
            u=[SELECT id, contactId, accountId FROM USER where id=:userInfo.getUserId() limit 1] ;
            List<account> v = [Select Id,Name from Account where OwnerId=:u.Id];
            if(v.size() > 0 )
                accountName = v[0].Name;
            //accountName =u.account.Name ;
            this.appCon =(Applicant_Contact__c)stdController.getRecord();
            //firstname = appCon.First_Name__c;
            //emailAddress =appCon.Email_Address__c;
            string recordtypeid = ApexPages.currentPage().getParameters().get('recordtype');
            if(recordtypeid !=null && recordtypeid !=''){
                recordTypeName = [Select Id,Name From RecordType Where Id=:recordtypeid].Name;
            } 
            showMessage = true;
            
        } catch(DmlException e) {
            //system.debug('The following exception has occurred: ' + e.getMessage());
            ApexPages.addMessages(e);
        }
        
    }
    
    public PageReference matchValues(){  // W-017403 VV added to sync last name w/busname for orgs
        if(appCon.EFL_Business_Name__c!=appCon.Name && indOrgCon=='Org')
            appCon.Name = appCon.EFL_Business_Name__c;
        return null;
    } 
    
    public PageReference cancel()
    {
        PageReference dirpage= new PageReference('/apex/associated_contacts');
        dirpage.setRedirect(true);
        return dirpage;
    }
    
    
    public PageReference editAssocContact() {
        PageReference pgRef = new PageReference('/apex/Portal_Associated_Contact_Create'); 
        pgRef.getParameters().put('id',appCon.id);  // pass param when coming from portal_associate_contact_detail page
        if (appcon.first_name__c!='' && appcon.first_name__c!=null)
            pgRef.getParameters().put('indOrgCon','Con');
        else
            pgRef.getParameters().put('indOrgCon','Org');
        pgRef.setRedirect(true);
        return pgRef;            
    }
    
    
    //KA:W-025564:: Starts:::: 
    public pageReference save(){
      try{
        upsert appCon;
        PageReference pg =new PageReference('/apex/Portal_Associate_Contact_Detail?id='+appCon.Id);
        pg.setRedirect(true);
        return pg;
          }catch(DMLException e) { 
          ApexPages.addMessages(e); //display the error message to the User
           return null; //return null
          }
     }

    //KA:W-025564::Ends:::: 
}