@isTest
private class EFLSelfReportControllerTest {
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    static Application__c app;
    static Authorizations__c auth;
    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static AC__c li;
    static Applicant_Attachments__c objaa;
    static Attachment objattach;
    static Id RtId;
    static Location__c loc;
    static Construct__c construct; 
    static Report_Summary__c rs;
    static Report_Summary_History__c rsh;
    static Self_Reporting__c sr;
    static EFL_Related_Record__c eflRelatedRec;
    static GPS_Coordinate__c eflRelatedRecGps;
    public static String VAR_RELATED_CONSTRUCT = 'relatedConstruct';
    public static String VAR_RELATED_GPS = 'relatedGps';
    public static String VAR_RELATED_MODE_EDIT = 'edit';
    public static String VAR_RELATED_MODE_LIST = 'list';
    public static String VAR_RELATED_MODE_CREATE = 'create';

    
    @IsTest
    public static void init(){
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
      
        app = new Application__c();
        app = testDataBRS.newapplication();
        auth = new Authorizations__c();
        auth = testDataBRS.newAuth(app.id);
        country = testDataBRS.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        li = new AC__c();
        li = testDataBRS.newLineItem('Resale/Adoption', app);

          
        RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId(); 
      	Id vrrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Volunteer Monitoring Report').getRecordTypeId();
        
        loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
          
        construct = testDataBRS.newconstruct(li.id);
        construct.Authorization__c = auth.id;
        update construct;
               
        rs = testDataBRS.newReportSummayByTypeStatus(auth.id, CARPOL_Constants.VOLUNTEER_MONITORING_REPORT, 'UnSubmitted');   
        
      	rsh = new Report_Summary_History__c();
        rsh.Authorization__c = auth.id;
        rsh.Submitted_Date__c = date.today();
        rsh.Report_Summary__c = rs.Id;
        rsh.Status__c = 'Saved';
        insert rsh; 
        
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()-1;
        sr.Monitoring_Period_End__c = date.today();
        sr.Observation_Date__c = date.today();
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Authorization__c = auth.id;
        sr.Final_Volunteer_Monitoring_Report__c = 'No';
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.RecordTypeId = vrrectypeid;
        sr.Comments__c = 'Test description';
        sr.Quantity_Acres__c = 1;
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
   		sr.Explanation__c = 'test';
      	sr.Is_No_Planting__c = true;
        sr.No_Monitoring_Report__c = true;
        sr.Is_Submitted__c = true;
        sr.Start_Date__c= date.today();
    
        insert sr;
     
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = loc.id;
        eflRelatedRec.Construct__c  = construct.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;    
       
        
      }
      
     @isTest
      static void testPlantingReport()
      {
       testData.insertcustomsettingsWithBRSTriggerDisabled();
            
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);

        Id noticerectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get(CARPOL_Constants.PLANTING_REPORT).getRecordTypeId();
        test.startTest();
        
        system.runAs(userShare)
        {
            
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            auth = new Authorizations__c();
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            construct = testData.newconstruct(LineItem.id);
            construct.Authorization__c = auth.id;
            update construct;
            
            Location__c loc= new Location__c();
        /*    loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
         */
            loc = testdata.newlocationByLineItem(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'));
            loc = testdata.newlocationByLineItem(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'));
            loc = testdata.newlocationByLineItem(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'));
            loc = testdata.newlocationByLineItem(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'));
            loc = testdata.newlocationByLineItem(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'));
            rs = testdata.newReportSummayByTypeStatus(auth.id, CARPOL_Constants.PLANTING_REPORT, 'UnSubmitted');         
            sr=  testdata.newPlantingSelfReporting(auth.id, rs.Id, loc.Id);
        
            eflRelatedRec = new EFL_Related_Record__c();
            eflRelatedRec.Authorization__c = auth.id;
            eflRelatedRec.Self_Reporting__c = sr.id;
            eflRelatedRec.Location__c = loc.id;
            eflRelatedRec.Construct__c  = construct.id;
            eflRelatedRec.Description__c = 'Test';
            insert eflRelatedRec;
            
            eflRelatedRecGps = new GPS_Coordinate__c();
            eflRelatedRecGps.Self_Reporting__c = sr.id;
            eflRelatedRecGps.GPS_Coordinates__Latitude__s = 23;
            eflRelatedRecGps.GPS_Coordinates__Longitude__s =11;
            eflRelatedRecGps.Location__c = loc.id;
            insert eflRelatedRecGps; 
            
            PageReference pageRef = Page.EFLSelfReport;
            pageRef.getParameters().put('authId', String.valueOf(auth.Id));
            pageRef.getParameters().put('reportType', 'planting_report');
            pageRef.getParameters().put('rsId', rs.Id);
            Test.setCurrentPage(pageRef);  
            
            ApexPages.StandardController sc = new ApexPages.StandardController(new Self_Reporting__c());
            EFLSelfReportController plantreport = new EFLSelfReportController(sc);
            plantreport.addform();
            plantreport.selfReportId = sr.Id;
            try{plantreport.save();}catch(exception ex){}
            plantreport.selfReportId = sr.id;
      
            try{plantreport.editForm();}catch(exception ex){}
            plantreport.cancelSRInfo();
			string applicantName = plantreport.applicantName;
            Authorizations__c authObj = plantreport.authObj;
            string doesAppContainCBI = plantreport.doesAppContainCBI;
            Id reportSumId = plantreport.reportSumId;
            String repSummName = plantreport.repSummName;
            Report_Summary__c rs = plantreport.rs;
            Boolean rsIsSubmitted = plantreport.rsIsSubmitted;
         //   plantreport.fireDocLauncher();
       }
        
        
        
        test.stopTest();
          
      }
      
       @IsTest 
      static void testVolunteerReport() {
      
          init();
          
          ApexPages.StandardController sc = new ApexPages.StandardController(new Self_Reporting__c());
          EFLSelfReportController cont = new EFLSelfReportController(sc);
          List<Location__c> PlantingLocationsAndReports = cont.getPlantingLocationsAndReports();
          cont.renderDetail = false;
          cont.isGPSCBI=true;
          cont.disabledReleaseLocation = 'test';
          cont.disabledStartDate=date.today();
          cont.selfReporting = new Self_Reporting__c();
          cont.rs = new Report_Summary__c();
          cont.eflRelatedRecListObservation = new List<Observation__c>();
          cont.erRelatedObjObservations = new Observation__c();
          cont.erRelatedObj = new EFL_Related_Record__c();
          cont.erRelatedObjGPS = new GPS_Coordinate__c();
          cont.eflRelatedRecListGPS = new List<GPS_Coordinate__c>();
          cont.repSummName = '';
          cont.authorizationID = auth.id;
          cont.action='create';
          cont.sobj='relatedGps';
        // cont.reportType = 'Volunteer Monitoring Report';
      
          cont.plantingListview = false;
          cont.plantingEditview = false;
          cont.NoPlantingview = false;
          cont.volunteerListview = false;
          cont.volunteerEditview = false;
          cont.NoMonitoringview= false;
          cont.isGPSCBI=false;
  	      cont.pageCount = 0;
          cont.submitBool = false;
          cont.authObj = new authorizations__c();
          cont.constructKeyPrefix = '';
          cont.locationKeyPrefix = '';
          
          cont.rsIsSubmitted = false;
    
          test.startTest();
       
          PageReference pageRef = Page.EFLSelfReport;
          Test.setCurrentPage(pageRef);
          pageRef.getParameters().put('authId', String.valueOf(auth.Id));           
          pageRef.getParameters().put('reportSumID', String.valueOf(rs.Id));  
          pageRef.getParameters().put('reportType', 'volunteer_monitoring_report');
          cont.authorizationID = auth.id;
          cont.authObj=auth;
          cont.doesAppContainCBI=auth.Application_CBI__c;
          cont.selfReporting = sr;
          cont.reportSumId = rs.Id;
          cont.reportTypeName = CARPOL_Constants.VOLUNTEER_MONITORING_REPORT;
          
          cont.submitBool = false;
      //    cont.callToggleCheckBox();
          cont.submitBool = true;
      //    cont.callToggleCheckBox();
          cont.rs = rs;
          
          Self_Reporting__c objSelfReportCopy = new Self_Reporting__c();
          objSelfReportCopy = sr.clone(false, true, false, false);
          objSelfReportCopy.Quantity_Acres__c = 2;
          insert objSelfReportCopy;
        
          List<EFL_Related_Record__c> lstEFLRelatedRec = new List<EFL_Related_Record__c>();
          lstEFLRelatedRec.add(eflRelatedRec);
        //  cont.reportType = 'Planting/Release Reports';
  
          cont.locationIdParam = loc.id;
          cont.addPlanting();
          cont.addNoPlanting();
          cont.addMonitoring();
          cont.addNoMonitoring();
          cont.selfReportId = sr.id;
          cont.selfReporting.id = sr.id;
          cont.editform();
          cont.cancelSRInfo();
          Integer noOfRecords = cont.noOfRecords;
          cont.refreshPageSize();
          
          cont.selfReporting = objSelfReportCopy;
     
          cont.getRelatedConstructs();
          
          cont.getRelatedObservations();
          
          GPS_Coordinate__c gps = new GPS_Coordinate__c();
          gps.Self_Reporting__c = sr.id;
          gps.GPS_Coordinates__Latitude__s = 23;
          gps.GPS_Coordinates__Longitude__s =11;
          gps.GPS_Coordinates_CBI__c = true;
          gps.Location__c = loc.id;
          insert gps; 
          cont.delrecid=gps.id;    
          cont.action='create';
          cont.sobj='relatedGps';
          cont.action();
          cont.editgps();
          cont.getRelatedGpsCoordinates();   
          cont.delrecid=gps.id;      
          cont.action='Save';
          cont.sobj='relatedGps';
          cont.action();
          
          EFL_Related_Record__c  relRec = new EFL_Related_Record__c();
          relRec.Authorization__c = auth.id;
          relRec.Self_Reporting__c = sr.id;
          relRec.Location__c = loc.id;
          relRec.Construct__c  = construct.id;
          relRec.Description__c = 'Test';
          insert relRec;
          
          cont.delrecid=relRec.id;    
          cont.action='create';
          cont.sobj='relatedConstruct';
          cont.action();
          cont.editConstruct();
          
          cont.delrecid=relRec.id;      
          cont.action='Save';
          cont.sobj='relatedConstruct';
          cont.action();
          
          Observation__c obs = new Observation__c();
          obs.Self_Reporting__c = sr.id;
          obs.Number_of_Volunteers__c = 0;
          obs.Observation_Date__c = date.today();
          insert obs; 
            
          cont.delrecid=obs.id;    
          cont.action='create';
          cont.sobj='relatedObs';
          cont.action();
          
          
          cont.delrecid=obs.id;      
          cont.action='Save';
          cont.sobj='relatedObs';
          cont.action();
          cont.editObs();
          
          cont.deleteRec();
          objSelfReportCopy.Quantity_Acres__c = 33;
          cont.selfReporting = objSelfReportCopy;
          pageRef = cont.Save();
          upsert objSelfReportCopy;
          cont.checkReadyToSubmit(rs,CARPOL_Constants.VOLUNTEER_MONITORING_REPORT);
          cont.doSubmit();
          cont.deleteSelfRepRec();
   		  Authorizations__c authObj = cont.authObj;
          string doesAppContainCBI = cont.doesAppContainCBI;
          
          test.stopTest();  
    }
    
    @isTest
    public static void test1(){
        
        init();
        
         ApexPages.StandardController sc = new ApexPages.StandardController(sr);
        EFLSelfReportController cont = new EFLSelfReportController(sc);
  		
        
        
    }

}