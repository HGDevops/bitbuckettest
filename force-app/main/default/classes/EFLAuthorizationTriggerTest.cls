/*
 * Notes by j.benkert@accenturefederal.com 1/30/2019
 *  Notes: The following is a list of things that need to be fixed within this test class as of 1/30/2019:
 *   - remove seeAllData = true from class declaration
 *   - There is NO bulk data testing happening in the legacy codebase.
 *   - There are no assertions that validate any business logic is achieved
 *   - most tests do not use runAs and only run in the current user's context
 */

@isTest(seealldata=true)
public class EFLAuthorizationTriggerTest {
    public static Authorizations__c auth;
    @isTest
    static void testdata(){
       string ACauthRecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='555';
        Insert objPrefix;
        Signature__c objTP=new  Signature__c();
        objTP.Name='Test AC TP Jialin';
        objTP.Recordtypeid=Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c=objPrefix.Id;
        Insert objTP;
      /*   auth = new Authorizations__c();
        auth.RecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId(); 
        auth.BRS_Introduction_Type__c='Interstate Movement';
        auth.thumbprint__c = objTP.id;
        auth.effective_date__c=system.today();
        auth.Expiry_Date__c = system.today().adddays(250);
        auth.BRS_Proposed_Start_Date__c=system.today();
        auth.BRS_Proposed_End_Date__c = system.today().adddays(250);
        auth.Application__c=app.id;
        auth.status__c = 'Submitted';
        auth.stage__c='Permit Package';
        auth.workflow_Number__c='150';
        auth.Authorization_Type__c='Permit';
        auth.BRS_Purpose_of_Permit__c='Importation';
        auth.Expiration_Timeframe_Override__c='1 Month';
        insert auth; */
       
          
    }
  /*  @isTest
    static void insertTestMethod(){
       // testdata();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
       Profile p = [SELECT Id FROM Profile WHERE Name='BRS Analyst'];
      //UserRole r = [SELECT Id, FROM UserRole WHERE Name IN ('BRS Program Specialist,BRS Biotechnologist') Limit 1];
       
        User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Manager', LanguageLocaleKey='en_US',FirstName='ROP CEEB', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
        insert u1;
        User u2 = new User(Alias = 'stand', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Biotechnologist', LanguageLocaleKey='en_US', FirstName='efile BRS',
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow1@testorg.com');
        insert u2;
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        system.runas(sysAdm)
        {
         team__c team = new team__c();
         // team.member_role__c='BRS Program Specialist';
           team.member_role__c= 'ROP CEEB Manager';
          team.member__c = u1.id;
            team.Authorization__c=auth.id;
            team.RecordTypeId=Schema.SObjectType.team__c.getRecordTypeInfosByName().get('BRS Team').getRecordTypeId();
            insert team;
         /*   team__c team1 = new team__c();
           // team1.member_role__c='BRS Biotechnologist';
           team.member_role__c= 'ROP CEEB Manager'
            team1.member__c = u2.id;
            insert team1;*/
      /*  }
        auth.status__c = 'Waiting on Customer';
        auth.stage__c='Application Review';
        system.test.starttest();
        update auth;
        EFLBRSAuthorizationEngine brsEngine = new EFLBRSAuthorizationEngine();
        brsEngine.validateRoles(auth);
        system.test.stoptest();
    } */
    @isTest
    static void ApprovedMethod(){
       // testdata();
       CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        auth.status__c = 'Approved';
        update auth;
    }
    @isTest
    static void issuedMethod(){
       // testdata();
       CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        
       
    }
    
   
    
    @isTest
    static void deleteTestMethod(){
       CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        delete auth;
    }
    
    /* 
     * New Test Method by j.benkert@accenturefederal.com 1/30/2019
     * @description when an auth record status changes to superceded, all 'active' labels under that auth should be voided
     */
    @isTest
    static void testSupercededAuthGetsVoidedLabels(){
        // when an Auth goes from any status to Superceded, all active labels should be changed to voided.
        Profile p = EFLTestDataFactoryBulk.getProfileByName('BRS Analyst');
        User analyst = EFLTestDataFactoryBulk.getUser(p);
        EFLTestDataFactoryBulk.setupTestDataSuite(10);
        list<Authorizations__c> auths = [SELECT Id FROM Authorizations__c WHERE Id IN :EFLTestDataFactoryBulk.authIds];
        system.assertEquals(10, auths.size());
        
        list<Label__c> labels = [SELECT Id, Status__c FROM Label__c WHERE Authorization__c IN :EFLTestDataFactoryBulk.authIds];
        system.assertEquals(10, labels.size());
        string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
        for(Label__c l:labels){
            system.assertEquals(active, l.Status__c);
        }
        
        string superceded = EFLGlobalConstants.getConstantValue('AUTH_STATUS_SUPERCEDED');
        
        for(Authorizations__c a:auths){
            a.Status__c = superceded;
        }
        
        test.startTest();
        //system.runAs(analyst){
            //AvoidRecursion.firstRunBulkAfter = true;
            update auths;
        //}
        test.stopTest();
        
        string LABEL_STATUS_VOIDED = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
        
        labels = [SELECT Id, Status__c FROM Label__c WHERE Authorization__c IN :EFLTestDataFactoryBulk.authIds];
        system.assertEquals(10, labels.size());
        for(Label__c l:labels){
            system.assertEquals(LABEL_STATUS_VOIDED, l.Status__c);
        }
    }
    
    /* This test changes the Sharing_Account__c field on Application__c object.
     * The result should be all related objects under the EFLAccountSharingSetting__mdt should also be updated 
     * to the new account.
     */
    @isTest
    static void testChangeSharingAccountOnAuth_ReportSummary(){
        // making data specific to this test due to "SeeAllData" declaration.
        CARPOL_BRS_TestDataManager mgr = new CARPOL_BRS_TestDataManager();
        mgr.insertcustomsettings();
        
        Application__c app = mgr.newapplication();
        
        list<Report_Summary__c> rss = new list<Report_Summary__c>();
        list<Id> authIds = new list<Id>();
        
        Authorizations__c auth = mgr.newAuth(app.Id);
        authIds.add(auth.Id);
        
        Report_Summary__c rs = new Report_Summary__c();
        rs.Authorization__c = auth.Id;
        rss.add(rs);
        
        Authorizations__c auth2 = mgr.newAuth(app.Id);
        authIds.add(auth2.Id);
        
        Report_Summary__c rs2 = new Report_Summary__c();
        rs2.Authorization__c = auth.Id;
        rss.add(rs2);
        
        insert rss;
        
        list<Report_Summary__c> reportsOriginal = [SELECT Id, Sharing_Account__c FROM Report_Summary__c WHERE Authorization__c IN :authIds];
        system.assertEquals(2, reportsOriginal.size());
        for(Report_Summary__c r :reportsOriginal ){
            system.assertEquals(null, r.Sharing_Account__c);
        }
        
        list<Authorizations__c> auths = [SELECT Id, Sharing_Account__c FROM Authorizations__c WHERE Application__c = :app.Id];
        system.assertEquals(2, auths.size());
        for(Authorizations__c au:auths){
            system.assertEquals(null, au.Sharing_Account__c);
        }
        
        Account newAcct = mgr.newAccount(mgr.AccountRecordTypeId);
        
        test.startTest();
        for(Authorizations__c au:auths){
            au.Sharing_Account__c = newAcct.Id;
        }
        //AvoidRecursion.firstRunBulkAfter = true;
        update auths;
        test.stopTest();
        
        list<Report_Summary__c> reports = [SELECT Id, Sharing_Account__c FROM Report_Summary__c WHERE Authorization__c IN :authIds];
        system.assertEquals(2, reports.size());
        
        // verify the Auths were updated.
        /*for(Report_Summary__c r: reports){
            system.assertEquals(newAcct.Id, r.Sharing_Account__c);
        }*/
    }
    
    @isTest
    static void testChangeSharingAccountOnAuth_ReportSummary_SetToNull(){
        // making data specific to this test due to "SeeAllData" declaration.
        CARPOL_BRS_TestDataManager mgr = new CARPOL_BRS_TestDataManager();
        mgr.insertcustomsettings();
        
        Account newAcct = mgr.newAccount(mgr.AccountRecordTypeId);
        Account newAcct2 = mgr.newAccount(mgr.AccountRecordTypeId);
        
        Application__c app = mgr.newapplication();
        
        list<Authorizations__c> authorizations = new list<Authorizations__c>();
        list<Report_Summary__c> rss = new list<Report_Summary__c>();
        list<Id> authIds = new list<Id>();
        
        Authorizations__c auth = mgr.newAuth(app.Id);
        auth.Sharing_Account__c = newAcct.id;
        authorizations.add(auth);
        authIds.add(auth.Id);
        
        Report_Summary__c rs = new Report_Summary__c();
        rs.Authorization__c = auth.Id;
        rs.Sharing_Account__c = newAcct.Id;
        rss.add(rs);
        
        Authorizations__c auth2 = mgr.newAuth(app.Id);
        auth2.Sharing_Account__c = newAcct.id;
        authorizations.add(auth2);
        authIds.add(auth2.Id);
        
        update authorizations;
        
        Report_Summary__c rs2 = new Report_Summary__c();
        rs2.Authorization__c = auth2.Id;
        rs2.Sharing_Account__c = newAcct.Id;
        rss.add(rs2);
        
        insert rss;
        
        list<Report_Summary__c> reportsOriginal = [SELECT Id, Sharing_Account__c FROM Report_Summary__c WHERE Authorization__c IN :authIds];
        system.assertEquals(2, reportsOriginal.size());
        for(Report_Summary__c r :reportsOriginal ){
            system.assertEquals(newAcct.Id, r.Sharing_Account__c);
        }
        
        list<Authorizations__c> auths = [SELECT Id, Sharing_Account__c FROM Authorizations__c WHERE Application__c = :app.Id];
        system.assertEquals(2, auths.size());
        for(Authorizations__c au:auths){
            system.assertEquals(newAcct.Id, au.Sharing_Account__c);
        }
        
        test.startTest();
        for(Authorizations__c au:auths){
            au.Sharing_Account__c = null;
        }
        //AvoidRecursion.firstRunBulkAfter = true;
        update auths;
        test.stopTest();
        
        list<Report_Summary__c> reports = [SELECT Id, Sharing_Account__c FROM Report_Summary__c WHERE Authorization__c IN :authIds];
        system.assertEquals(2, reports.size());
        
        // verify the Auths were updated.
   //     for(Report_Summary__c r: reports){
     //       system.assertEquals(null, r.Sharing_Account__c);
   //     }
    }
}