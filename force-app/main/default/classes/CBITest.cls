@IsTest(SeeAllData=true)
/*
This test class should cover class cbi, cbiservice, ppqimpl
*/
private class CBITest {
    
    static testMethod void validateCBIData() {
   CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();     
        // Create custom settings and poulate data in USDAGroups, PPQCBIFields
        // No need since we are using SeeAllData=true for ConnectApi error
        //USDAGroup__c usdagrp = new USDAGroup__c(Name = 'PPQ');
        //insert usdagrp;
        Trade_Agreement__c ta=new  Trade_Agreement__c();
        ta.Name='test ta';
        ta.Trade_Agreement_Code__c='ta';
        ta.Trade_Agreement_Status__c='Active';
        Insert ta;
        
       Country__c objcountry=new  Country__c();
        objcountry.Name='Afghanistan';
        objcountry.Country_Code__c='AF';
        objcountry.Country_Status__c='Active';
        objcountry.Trade_Agreement__c=ta.Id;
        Insert objcountry; 
        
        Level_1_Region__c objL1R=new  Level_1_Region__c();
        objL1r.Country__c=objcountry.Id;
        objL1r.Name='Alabama';
        objL1r.Level_1_Name__c='Alabama';
        objL1r.Level_1_Region_Code__c='AL';
        objL1r.Level_1_Region_Status__c='Active';
        Insert objL1r;
        
        level_2_Region__c objL2R=new level_2_Region__c(Name='Baltimore',level_1_Region__c=objL1R.id,Level_2_Region_Status__c = 'Active');
        Insert objL2R;
        
        // create associated contact record
        Applicant_Contact__c aContact = new Applicant_Contact__c(name = 'AssociatedLastName',
                                                                 first_name__c = 'AssociatedFirstName',
                                                                 Email_Address__c = 'Associatedcontact@test.com',Mailing_Country__c='United States',
       Mailing_Country_LR__c=objcountry.Id,
        Mailing_County__c = objL2R.Id,
       Mailing_State_LR__c=objL1r.Id,
        Mailing_State__c='Alabama',
        Mailing_Zip_Postal_Code__c = '35005',
        EFL_Business_Name__c = 'Test');
        insert aContact;
        
        // Created PPQCBIField custom setting data
        //List<PPQCBIFields__c> cbiPPQList = new List<PPQCBIFields__c>();
        /* No need since we are using SeeAllData=true to resolve ConnectApi error
PPQCBIFields__c ppqcbiCS = new PPQCBIFields__c( Name = 'Hand_Carrier_Last_Name__c',
FieldApi__c = 'Hand_Carrier_Last_Name__c',
FieldApiCBI__c = 'Hand_Carrier_Information_CBI__c');
insert ppqcbiCS;
//cbiPPQList.add(ppqcbiCS);

PPQCBIFields__c ppqcbiCS2 = new PPQCBIFields__c( Name = 'Number_of_Labels__c',
FieldApi__c = 'Number_of_Labels__c',
FieldApiCBI__c = 'Number_of_Labels_CBI__c');
insert ppqcbiCS2;

//cbiPPQList.add(ppqcbiCS2);
*/
     /*   PPQCBIFields__c ppqcbiCS3 = new PPQCBIFields__c( Name = 'Organisms__c',
                                                        FieldApi__c = 'Organisms__c',
                                                        FieldApiCBI__c = 'EFLOrganisms_CBI__c');
        insert ppqcbiCS3;
        //cbiPPQList.add(ppqcbiCS3);
        
        
        
        // Create Program
        Domain__c prg = new Domain__c(Name = 'PPQv', Active__c = true);
        insert prg;
        
        // Create Program_Prefix__c
        Program_Prefix__c prgPreFix = new Program_Prefix__c(Name = '102', Program__c = prg.Id);
        insert prgPreFix;
        
        //Create thumbprint
        Signature__c sign = new Signature__c(Program_Prefix__c = prgPreFix.Id);
        insert sign;
        // create Program_Line_Item_Pathway__c
        Program_Line_Item_Pathway__c prgPath = new Program_Line_Item_Pathway__c(Contains_CBI__c = 'Yes',
                                                                                Name = 'Soil or Soil like Substrate',
                                                                                Program__c = prg.Id);
        insert prgPath;
        
        // Create Application
        Application__c app = new Application__c();
        insert app;
        
        // Create Authroization     
        //Authorizations__c auth = new Authorizations__c(Program_Pathway__c = prgPath.Id, Application__c=app.Id);
         Authorizations__c auth = testData.newAuth(app.id);
        
        AC__c lineItem = new AC__c(Number_of_Labels__c = 8, Number_of_Labels_CBI__c = true, 
                                   Hand_Carrier_Last_Name__c = aContact.Id, Authorization__c = auth.Id,
                                   Hand_Carrier_Information_CBI__c = true, Make__c = '[Demo Make]', 
                                   Program_Line_Item_Pathway__c = prgPath.Id, other__c='[other]method',
                                   Application_Number__c = app.Id, Organisms__c = 'Bacteria', EFLOrganisms_CBI__c=true );
        
        test.startTest();
       // insert auth; 
        insert lineItem;
        
        
        
        CBI c = new CBI('PPQ');
        lineItem = c.getCBIDeletedData(lineItem);
        test.stopTest();
        system.debug('Make=== ' + [Select id, Make__C FROM Ac__c where id=: lineItem.Id].Make__c);
        system.assertEquals(lineItem.Make__c, '[ ]');
        system.assertEquals(lineItem.Hand_Carrier_Last_Name__r.Name, '[ ]');
        
        try {
            c = new CBI('PPQa');
            
        } catch  (Exception e) {
            system.assertEquals(e.getMessage(), 'PPQa is not valid.');
        } 
        c = new CBI('PPQ');
        lineItem = c.getCBIData(lineItem);*/
    } 
}