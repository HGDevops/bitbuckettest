@isTest
private class ProcessPaymentPayGov_Test {

	private static testMethod void testCompleteOnlineCollection()
	{
	    CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c lineItem1 = testData.newLineItem('Resale',objapp);
        AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
        
        DateTime dt = DateTime.now();
        String strToken = dt.format('yyyyMMddhhss');
        String transactionId = objapp.id;
        CARPOL_ProcessPaymentPayGov payObj = new CARPOL_ProcessPaymentPayGov();
        payObj.completeOnlineCollection(strToken, transactionId);
        
	}
	
	private static testMethod void teststartOnlineCollection()
	{
	    CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c lineItem1 = testData.newLineItem('Resale',objapp);
        AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
        
        String transAmount = '300'; //random test amount
        String transactionId = objapp.id;
        CARPOL_ProcessPaymentPayGov payObj = new CARPOL_ProcessPaymentPayGov();
        payObj.startOnlineCollection(transAmount, transactionId);
        
	}

}