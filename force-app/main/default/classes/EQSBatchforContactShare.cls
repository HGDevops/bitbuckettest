global class EQSBatchforContactShare implements Database.batchable<sobject> {
    global EQSBatchforContactShare()
    {

    }

    global String query = '';

    global database.querylocator start(database.batchablecontext bc) {
        String recID = [SELECT Id FROM RecordType WHERE DeveloperName = 'APHIS_EQS_Responder'].Id;
        //Id conid = '003t0000006EghF';
        String entryID = recID.substring(0, recID.length() - 3);
        List<User> ul = [select id from user where name = 'EQS Data Integrator'];
        id eqsuserid = ul[0].id;
        if (ul.size() > 0 && Test.isRunningTest()) {
            //Querying the test user in case of test class execution.
            User testUser = [Select id from user where name = 'Test EQS Data Integrator' Limit 1];
            eqsuserid = testUser.id;
        }

        // Query Relationship Data       
        //vishnu: added EQS_Employee_ID__c to use as a reference instead of Email.
        query = 'Select Id, Name, ReportsTo.Email, Email, EQS_Employee_ID__c, ReportsTo.EQS_Employee_ID__c, User_id_used_for_sharing__c, Ownerid, ReportsToId,';
        query += '(select Id, OwnerId from EQS_Responder_Position_Certifications__r),';
        query += '(select Id, Name, OwnerId from Responder_Fit_Test_Data__r),';
        query += '(select Id, Name, OwnerId from Resource_Fill_Requests__r),';
        query += '(select Id, Name, OwnerId from EQS_Responder_Medical_Clearances__r),';
        query += '(select Id, Name, OwnerId from Responder_PPEs__r),';
        query += '(Select id, Name, OwnerId from Responder_Training__r),';
        query += '(Select id, name from EQS_Resp_Prof_Certs__r)from Contact where recordTypeId =:recID ';
        //Swami: commented the below line so as to include contact owned by anyone.
        //query +='(Select id, name from EQS_Resp_Prof_Certs__r)from Contact where OwnerId = :eqsuserid';
        //query +='(Select Id, Name, OwnerId from EQS_Resp_Prof_Certs__r)from Contact where id = :conid';

        system.debug('=====' + query);
        return database.getquerylocator(query);
    }


    Global void execute(Database.batchablecontext bc, List<Contact> conRelData) {

        system.debug('conreldata ' + conRelData);

        if (conRelData.size() > 0) {
            //Create a User Map, only return Standard Users with one of EQS Permission Sets assigned
            List<User> userList = new List<User> ();
            if (Test.isRunningTest()) {
                userList = [Select Id, Email, EQS_Employee_ID__c from User where usertype = 'Standard' AND isActive = true];
            }
            else {
                userList = [Select Id, Email, EQS_Employee_ID__c from User where usertype = 'Standard' AND isActive = true and Id IN(
                                                                                                                                     SELECT AssigneeId FROM PermissionSetAssignment
                                                                                                                                     WHERE PermissionSet.Name like '%APHIS_EQS%'
                )];
            }
            Map<String, Id> usrMap = new Map<String, Id> ();

            for (User i : userList) {
                //Added by Swami: 08/11/2017: Select only those users who have Edit Access on all the related Objects.
                usrMap.put(i.EQS_Employee_ID__c, i.Id);
            }

            //Users used for sharing for rule deletion on Reports to Change, takes the old value
            List<String> uShared = new List<String> ();

            //Initialize a ContactShare List
            List<ContactShare> csList = new List<ContactShare> ();
            List<ContactShare> csListx = new List<ContactShare> ();

            //Initialize a EQS_Responder_Position_Certification__share List
            List<EQS_Responder_Position_Certification__share> esList = new List<EQS_Responder_Position_Certification__share> ();
            List<EQS_Responder_Position_Certification__share> esListx = new List<EQS_Responder_Position_Certification__share> ();
            List<EQS_Responder_Position_Certification__c> esOwnerUpdate = new List<EQS_Responder_Position_Certification__c> ();

            //Initialize a EQS_Responder_Respirators_Fit_Test__share List
            List<EQS_Responder_Respirators_Fit_Test__share> erfList = new List<EQS_Responder_Respirators_Fit_Test__share> ();
            List<EQS_Responder_Respirators_Fit_Test__share> erfListx = new List<EQS_Responder_Respirators_Fit_Test__share> ();
            List<EQS_Responder_Respirators_Fit_Test__c> erfOwnerUpdate = new List<EQS_Responder_Respirators_Fit_Test__c> ();

            //Initialize a EQS_Responder_Training__share List
            List<EQS_Responder_Training__share> rtsList = new List<EQS_Responder_Training__share> ();
            List<EQS_Responder_Training__share> rtsListx = new List<EQS_Responder_Training__share> ();
            List<EQS_Responder_Training__c> rtsOwnerUpdate = new List<EQS_Responder_Training__c> ();

            //Initialize a EQS_Resource_Fill__share List
            List<EQS_Resource_Request__share> rfrList = new List<EQS_Resource_Request__share> ();
            List<EQS_Resource_Request__share> rfrListx = new List<EQS_Resource_Request__share> ();
            List<EQS_Resource_Request__c> rfrOwnerUpdate = new List<EQS_Resource_Request__c> ();

            //Initialize a EQS_Responder_PPE__share List
            List<EQS_Responder_PPE__share> rpeList = new List<EQS_Responder_PPE__share> ();
            List<EQS_Responder_PPE__share> rpeListx = new List<EQS_Responder_PPE__share> ();
            List<EQS_Responder_PPE__c> rpeOwnerUpdate = new List<EQS_Responder_PPE__c> ();

            //Initialize a EQS_Responder_Medical_Clearance__share List
            List<EQS_Responder_Medical_Clearance__share> rmcList = new List<EQS_Responder_Medical_Clearance__share> ();
            List<EQS_Responder_Medical_Clearance__share> rmcListx = new List<EQS_Responder_Medical_Clearance__share> ();
            List<EQS_Responder_Medical_Clearance__c> rmcOwnerUpdate = new List<EQS_Responder_Medical_Clearance__c> ();

            //Initialize a EQS_Resp_Prof_Cert__share List
            List<EQS_Resp_Prof_Cert__share> rpcList = new List<EQS_Resp_Prof_Cert__share> ();
            List<EQS_Resp_Prof_Cert__share> rpcListx = new List<EQS_Resp_Prof_Cert__share> ();
            List<EQS_Resp_Prof_Cert__c> rpcOwnerUpdate = new List<EQS_Resp_Prof_Cert__c> ();

            //Prepare to build a ContactShare deletion list
            Map<ID, String> csDelMap = new Map<ID, String> ();
            Map<ID, String> csDelMap2 = new Map<ID, String> ();
            List<ContactShare> conSharesToDelete = new List<ContactShare> ();
            List<ContactShare> conSharesToDelete2 = new List<ContactShare> ();
            List<ContactShare> conSharesToDelete3 = new List<ContactShare> ();

            //Initialize a Contact update list
            List<Contact> conToUpdate = new List<Contact> ();
            List<Contact> conupdate = new list<Contact> ();
            // boolean con_update = false; 

            //Initialize String to hold email for control flow
            String UserToShareWith;
            String OwnerToShareWith;
            //Boolean emailMatchCheck =  false;

            //Flags to remove old share records
            Boolean RemoveOldUserShares = false;
            Boolean RemoveOldOwnerShares = false;

            //Populate the ContactShare List
            for (Contact con : conRelData) {
                //Case when there is a reports to Manager contact present.

                system.debug('contact rel data passed to the execute method ' + conRelData);

                if (con.ReportsToId != null) {
                    //If the reports to manager's user id is there in User_id_used_for_sharing__c
                    if (con.User_id_used_for_sharing__c != null)
                    {
                        //Populate the DeleteMap csDelMap2
                        csDelMap2.put(con.id, con.User_id_used_for_sharing__c);
                        //uShared.add(con.User_id_used_for_sharing__c);
                        //RemoveOldUserShares = true;

                        system.debug('condelmap ' + csDelMap2);
                    }
                    //Try and see if there is a user record for ReportsTo is present
                    UserToShareWith = usrMap.get(con.ReportsTo.EQS_Employee_ID__c);
                    System.debug('%%%%%%%%%%%%%%%% ' + UserToShareWith + ' %%%%%%%%%%%%%%%%%%%%');
                    //if so create a share record
                    if (UserToShareWith != null) {
                        //Initialize the ContactShare record
                        ContactShare cs = new ContactShare();
                        cs.ContactId = con.Id;
                        cs.ContactAccessLevel = 'Edit';
                        cs.RowCause = 'Manual';
                        cs.UserOrGroupId = UserToShareWith;
                        //Populate the DeleteMap
                        csDelMap.put(cs.ContactId, cs.UserOrGroupId);
                        //uShared.add(con.User_id_used_for_sharing__c);
                        //RemoveOldUserShares = true;
                        //Update custom field with new user id value
                        con.User_id_used_for_sharing__c = cs.UserOrGroupId;
                        //Add contactshare to the insert list
                        csList.add(cs);
                        //Add contact to the update list, only to save the new user id value
                        conToUpdate.add(con);

                        system.debug('contact share list ' + csList);

                        system.debug('contact update list ' + conToUpdate);
                    }
                }

                if (con.reportsToId == null) {
                    if (con.User_id_used_for_sharing__c != null)
                    {
                        //Populate the DeleteMap
                        csDelMap.put(con.id, con.User_id_used_for_sharing__c);
                        uShared.add(con.User_id_used_for_sharing__c);
                        con.User_id_used_for_sharing__c = null;
                        //Flag to remove old share records
                        RemoveOldUserShares = true;
                        conToUpdate.add(con);
                    }
                }

                if (con.EQS_Employee_ID__c != null) {
                    OwnerToShareWith = usrMap.get(con.EQS_Employee_ID__c);
                    System.debug('%%%%%%%%%%%%%%%% ' + OwnerToShareWith + ' %%%%%%%%%%%%%%%%%%%%');
                    //Added by Swami- 08/11/2017: Added condition to check if OwnerToShareWith is not null
                    if (OwnerToShareWith != null && con.Ownerid != OwnerToShareWith) {
                        con.OwnerId = OwnerToShareWith;
                        con.Owner_id_used_for_sharing__c = OwnerToShareWith;
                        conUpdate.add(con);
                        System.debug('@@@@' + conUpdate);
                    }
                }

                //Added by Swami- 08/11/2017: Check if the UserToShareWith and OwnerToShareWith have Read/Update Permissions on all objects before setting them as record owners.
                Schema.DescribeSObjectResult eqsResponderDescribe = EQS_Responder_Position_Certification__c.sObjectType.getDescribe();
                Schema.DescribeSObjectResult eqsResponderRespDescribe = EQS_Responder_Respirators_Fit_Test__c.sObjectType.getDescribe();
                Schema.DescribeSObjectResult eqsResponderTrainDescribe = EQS_Responder_Training__c.sObjectType.getDescribe();
                Schema.DescribeSObjectResult eqsResponderProfDescribe = EQS_Resp_Prof_Cert__c.sObjectType.getDescribe();

                //Boolean userObjectAccess = eqsResponderDescribe.
                System.debug('eqsResponderDescribe accessible:' + eqsResponderDescribe.accessible);

                //Swami: added && UserToShareWith !=OwnerToShareWith condition when reportsTo and contact are the same.
                if (UserToShareWith != null && UserToShareWith != OwnerToShareWith) {
                    ///////////////////////
                    if (con.EQS_Responder_Position_Certifications__r != null) {
                        for (EQS_Responder_Position_Certification__c rpc : con.EQS_Responder_Position_Certifications__r) {
                            EQS_Responder_Position_Certification__share ers = new EQS_Responder_Position_Certification__share();
                            ers.ParentId = rpc.Id;
                            ers.UserOrGroupId = UserToShareWith;
                            ers.AccessLevel = 'Edit';
                            ers.RowCause = 'Manual';
                            esList.add(ers);

                            //rpc.OwnerId = OwnerToShareWith;
                            //esOwnerUpdate.add(rpc); 

                            system.debug('esList ' + esList);
                        }
                    }

                    ///////////////////////
                    if (con.Responder_Fit_Test_Data__r != null) {
                        for (EQS_Responder_Respirators_Fit_Test__c ftd : con.Responder_Fit_Test_Data__r) {
                            EQS_Responder_Respirators_Fit_Test__share erf = new EQS_Responder_Respirators_Fit_Test__share();
                            erf.ParentId = ftd.Id;
                            erf.UserOrGroupId = UserToShareWith;
                            erf.AccessLevel = 'Edit';
                            erf.RowCause = 'Manual';
                            erfList.add(erf);

                            //ftd.OwnerId = OwnerToShareWith;
                            //erfOwnerUpdate.add(ftd);

                            system.debug('erfList ' + erfList);
                        }
                    }

                    ///////////////////////
                    if (con.Responder_Training__r != null) {
                        for (EQS_Responder_Training__c rtc : con.Responder_Training__r) {
                            EQS_Responder_Training__share rts = new EQS_Responder_Training__share();
                            rts.ParentId = rtc.Id;
                            rts.UserOrGroupId = UserToShareWith;
                            rts.AccessLevel = 'Edit';
                            rts.RowCause = 'Manual';
                            rtsList.add(rts);

                            //rtc.OwnerId = OwnerToShareWith;
                            //rtsOwnerUpdate.add(rtc);

                            system.debug('rtsList ' + rtsList);

                        }
                    }
                    ////////////////////
                    if (con.Resource_Fill_Requests__r != null) {
                        for (EQS_Resource_Request__c rfrc : con.Resource_Fill_Requests__r) {
                            EQS_Resource_Request__share rfr = new EQS_Resource_Request__share();
                            rfr.ParentId = rfrc.Id;
                            rfr.UserOrGroupId = UserToShareWith;
                            rfr.AccessLevel = 'Edit';
                            rfr.RowCause = 'Manual';
                            rfrList.add(rfr);

                            //rtc.OwnerId = OwnerToShareWith;
                            //rtsOwnerUpdate.add(rtc);

                            system.debug('rfrList ' + rfrList);
                        }
                    }
                    ///////////////////////////
                    if (con.Responder_PPEs__r != null) {
                        for (EQS_Responder_PPE__c rpec : con.Responder_PPEs__r) {
                            EQS_Responder_PPE__share rpe = new EQS_Responder_PPE__share();
                            rpe.ParentId = rpec.Id;
                            rpe.UserOrGroupId = UserToShareWith;
                            rpe.AccessLevel = 'Edit';
                            rpe.RowCause = 'Manual';
                            rpeList.add(rpe);

                            //rtc.OwnerId = OwnerToShareWith;
                            //rtsOwnerUpdate.add(rtc);
                            //
                            system.debug('rpeList ' + rpeList);
                        }
                    }
                    /////////////////////////
                    if (con.EQS_Responder_Medical_Clearances__r != null) {
                        for (EQS_Responder_Medical_Clearance__c rmcc : con.EQS_Responder_Medical_Clearances__r) {
                            EQS_Responder_Medical_Clearance__share rmc = new EQS_Responder_Medical_Clearance__share();
                            rmc.ParentId = rmcc.Id;
                            rmc.UserOrGroupId = UserToShareWith;
                            rmc.AccessLevel = 'Edit';
                            rmc.RowCause = 'Manual';
                            rmcList.add(rmc);

                            //rtc.OwnerId = OwnerToShareWith;
                            //rtsOwnerUpdate.add(rtc);
                            //
                            system.debug('rmcList ' + rmcList);
                        }
                    }

                    ///////////////////////
                    if (con.EQS_Resp_Prof_Certs__r != null) {
                        for (EQS_Resp_Prof_Cert__c erp : con.EQS_Resp_Prof_Certs__r) {
                            EQS_Resp_Prof_Cert__share rpc = new EQS_Resp_Prof_Cert__share();
                            rpc.ParentId = erp.Id;
                            rpc.UserOrGroupId = UserToShareWith;
                            rpc.AccessLevel = 'Edit';
                            rpc.RowCause = 'Manual';
                            rpcList.add(rpc);

                            // erp.OwnerId = OwnerToShareWith;
                            // rpcOwnerUpdate.add(erp);
                            // 
                            system.debug('rpcList ' + rpcList);
                        }
                    }
                }
                // Added if Ownertosharewith is Not Null 
                if (OwnerToShareWith != null) {
                    ///////////////////////
                    if (con.EQS_Responder_Position_Certifications__r != null) {
                        for (EQS_Responder_Position_Certification__c rpc : con.EQS_Responder_Position_Certifications__r) {
                            EQS_Responder_Position_Certification__share ers = new EQS_Responder_Position_Certification__share();
                            /*   ers.ParentId=rpc.Id; 
                              ers.UserOrGroupId = UserToShareWith;
                              ers.AccessLevel = 'Edit';
                              ers.RowCause = 'Manual';
                              esList.add(ers); */

                            rpc.OwnerId = OwnerToShareWith;
                            esOwnerUpdate.add(rpc);

                            system.debug('esOwnerUpdate ' + esOwnerUpdate);
                        }
                    }

                    ///////////////////////
                    if (con.Responder_Fit_Test_Data__r != null) {
                        for (EQS_Responder_Respirators_Fit_Test__c ftd : con.Responder_Fit_Test_Data__r) {
                            EQS_Responder_Respirators_Fit_Test__share erf = new EQS_Responder_Respirators_Fit_Test__share();
                            /*    erf.ParentId=ftd.Id; 
                              erf.UserOrGroupId = UserToShareWith;
                              erf.AccessLevel = 'Edit';
                              erf.RowCause = 'Manual';
                              erfList.add(erf); */

                            ftd.OwnerId = OwnerToShareWith;
                            erfOwnerUpdate.add(ftd);

                            system.debug('erfOwnerUpdate ' + erfOwnerUpdate);
                        }
                    }

                    ///////////////////////
                    if (con.Responder_Training__r != null) {
                        for (EQS_Responder_Training__c rtc : con.Responder_Training__r) {
                            EQS_Responder_Training__share rts = new EQS_Responder_Training__share();
                            /*     rts.ParentId=rtc.Id; 
                              rts.UserOrGroupId = UserToShareWith;
                              rts.AccessLevel = 'Edit';
                              rts.RowCause = 'Manual';
                              rtsList.add(rts); */
                            rtc.OwnerId = OwnerToShareWith;
                            rtsOwnerUpdate.add(rtc);

                            system.debug('rtsOwnerUpdate ' + rtsOwnerUpdate);
                        }
                    }
                    if (con.Resource_Fill_Requests__r != null) {
                        for (EQS_Resource_Request__c rfrc : con.Resource_Fill_Requests__r) {
                            EQS_Resource_Request__share rfr = new EQS_Resource_Request__share();
                            /*     rfr.ParentId=rfrc.Id; 
                              rfr.UserOrGroupId = UserToShareWith;
                              rfr.AccessLevel = 'Edit';
                              rfr.RowCause = 'Manual';
                              rfrList.add(rfr); */

                            rfrc.OwnerId = OwnerToShareWith;
                            rfrOwnerUpdate.add(rfrc);

                            system.debug('rfrOwnerUpdate ' + rfrOwnerUpdate);
                        }
                    }
                    ///////////////////////////
                    if (con.Responder_PPEs__r != null) {
                        for (EQS_Responder_PPE__c rpec : con.Responder_PPEs__r) {
                            EQS_Responder_PPE__share rpe = new EQS_Responder_PPE__share();
                            /*     rpe.ParentId=rpec.Id; 
                              rpe.UserOrGroupId = UserToShareWith;
                              rpe.AccessLevel = 'Edit';
                              rpe.RowCause = 'Manual';
                              rpeList.add(rpe);  */

                            rpec.OwnerId = OwnerToShareWith;
                            rpeOwnerUpdate.add(rpec);

                            system.debug('rpeOwnerUpdate ' + rpeOwnerUpdate);
                        }
                    }
                    /////////////////////////
                    if (con.EQS_Responder_Medical_Clearances__r != null) {
                        for (EQS_Responder_Medical_Clearance__c rmcc : con.EQS_Responder_Medical_Clearances__r) {
                            EQS_Responder_Medical_Clearance__share rmc = new EQS_Responder_Medical_Clearance__share();
                            /*       rmc.ParentId=rmcc.Id; 
                              rmc.UserOrGroupId = UserToShareWith;
                              rmc.AccessLevel = 'Edit';
                              rmc.RowCause = 'Manual';
                              rmcList.add(rmc);  */

                            rmcc.OwnerId = OwnerToShareWith;
                            rmcOwnerUpdate.add(rmcc);

                            system.debug('rmcOwnerUpdate ' + rmcOwnerUpdate);
                        }
                    }

                    ///////////////////////
                    if (con.EQS_Resp_Prof_Certs__r != null) {
                        for (EQS_Resp_Prof_Cert__c erp : con.EQS_Resp_Prof_Certs__r) {
                            EQS_Resp_Prof_Cert__share rpc = new EQS_Resp_Prof_Cert__share();
                            /*      rpc.ParentId=erp.Id; 
                              rpc.UserOrGroupId = UserToShareWith;
                              rpc.AccessLevel = 'Edit';
                              rpc.RowCause = 'Manual';
                              rpcList.add(rpc); */

                            erp.OwnerId = OwnerToShareWith;
                            rpcOwnerUpdate.add(erp);

                            system.debug('rpcOwnerUpdate ' + rpcOwnerUpdate);
                        }
                    }
                }
                //  Added if Ownertosharewith is Not Null ends here

            }




            /***********************
             *  DML to finish job 
             * 
             ************************/
            //////////////////
            //On reports to change: delete old shares on related objects before the insertion of new ones
            if (RemoveOldUserShares = true) {
                //Swami: add AND RowCause = 'Manual' condition to only pick manually created sharing records.
                esListx = [Select id from EQS_Responder_Position_Certification__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
                erfListx = [Select id from EQS_Responder_Respirators_Fit_Test__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
                rtsListx = [Select id from EQS_Responder_Training__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
                rfrListx = [Select id from EQS_Resource_Request__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
                rpeListx = [Select id from EQS_Responder_PPE__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
                rmcListx = [Select id from EQS_Responder_Medical_Clearance__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
                rpcListx = [Select id from EQS_Resp_Prof_Cert__share where UserOrGroupId IN :uShared AND RowCause = 'Manual'];
            }

            //Update the contact records for ownership change 
            if (conUpdate.size() > 0) {
                try { update conUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }

            //Query records based off of the delete map
            if (csDelMap.size() > 0) {
                conSharesToDelete = [Select Id, ContactAccessLevel, ContactId from ContactShare where(ContactId IN :csDelMap.keySet() AND UserOrGroupId in :csDelMap.values() AND RowCause = 'Manual')];
                system.debug(conSharesToDelete);
            }

            //Delete the old ContactShare rules
            if (conSharesToDelete.size() > 0) {
                try { //system.debug('inside delete');
                    delete conSharesToDelete; }
                catch(System.CalloutException e) { system.debug('#####' + e + '#####'); }
            }
            if (csdelmap2.size() > 0) {
                conSharesToDelete2 = [Select Id from ContactShare where(ContactId IN :csDelMap2.keySet() AND UserOrGroupId in :csDelMap2.values() AND RowCause = 'Manual')];
                system.debug(conSharesToDelete2);
            }

            if (conSharesToDelete2.size() > 0) {
                try {
                    delete conSharesToDelete2; }
                catch(System.CalloutException e) { system.debug('#####' + e + '#####'); }
            }

            //Insert the new ContactShare rules
            if (csList.size() > 0 ) {
                try { insert csList; }
                catch(System.CalloutException e) { system.debug('<<<<<' + e + '>>>>>'); }
            }
            //Insert the new ContactShare rules
            if (csListx.size() > 0) {
                try { insert csListx; }
                catch(System.CalloutException e) { system.debug('#####' + e + '#####'); }
            }

            //Update the contact records
            if (conToUpdate.size() > 0) {
                try { update conToUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }

            //Insert other share records
            if (esList.size() > 0) {
                try {
                    //insert esList;
                    Database.SaveResult[] lsr = Database.insert(esList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }
            if (erfList.size() > 0) {
                try {
                    //insert erfList;
                    Database.SaveResult[] lsr = Database.insert(erfList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }
            if (rtsList.size() > 0) {
                try {
                    //insert rtsList;
                    Database.SaveResult[] lsr = Database.insert(rtsList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }
            if (rpcList.size() > 0) {
                System.debug('<<<rpcList>>>');
                try {
                    // insert rpcList;
                    Database.SaveResult[] lsr = Database.insert(rpcList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (rfrList.size() > 0) {
                try {
                    //insert rfrList;
                    Database.SaveResult[] lsr = Database.insert(rfrList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }
            if (rpeList.size() > 0) {
                try {
                    //insert rpeList;
                    Database.SaveResult[] lsr = Database.insert(rpeList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }

            }
            if (rmcList.size() > 0) {
                try {
                    //insert rmcList;
                    Database.SaveResult[] lsr = Database.insert(rmcList, false);
                }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }

            /////Update records with new owners
            if (esOwnerUpdate.size() > 0) {
                try { update esOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (erfOwnerUpdate.size() > 0) {
                try { update erfOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (rtsOwnerUpdate.size() > 0) {
                try { update rtsOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (rpcOwnerUpdate.size() > 0) {
                try { update rpcOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (rfrOwnerUpdate.size() > 0) {
                try { update rfrOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (rpeOwnerUpdate.size() > 0) {
                try { update rpeOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
            if (rmcOwnerUpdate.size() > 0) {
                try { update rmcOwnerUpdate; }
                catch(System.CalloutException e) { system.debug('@@@@@' + e + '@@@@@'); }
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
    }
}