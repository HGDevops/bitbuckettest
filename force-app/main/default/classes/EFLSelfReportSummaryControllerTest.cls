@isTest(seealldata=false)
private class EFLSelfReportSummaryControllerTest{
    
   
    static Application__c app;
    static Authorizations__c auth;
    static Application__c app1;
    static Authorizations__c auth1;
    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static AC__c li;
    static Applicant_Attachments__c objaa;
    static Report_Summary__c rs;
    static Attachment objattach;
    static Id RtId;
    static Location__c loc;
    static GPS_Coordinate__c eflRelatedRecGps;
    public static String VAR_RELATED_CONSTRUCT = 'relatedConstruct';
    public static String VAR_RELATED_GPS = 'relatedGps';
    public static String VAR_RELATED_MODE_EDIT = 'edit';
    public static String VAR_RELATED_MODE_LIST = 'list';
    public static String VAR_RELATED_MODE_CREATE = 'create';
    static Construct__c construct; 
    static Report_Summary_History__c rsh;
    static Self_Reporting__c sr;   
    static EFL_Related_Record__c eflRelatedRec;
    
    @IsTest
    public static void init(){
   CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
      
        app = new Application__c();
        app = testData.newapplication();
        auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
        
        app1 = new Application__c();
        app1 = testData.newapplication();
        auth1 = new Authorizations__c();
        auth1 = testData.newAuth(app1.id);
        auth1.Parent_Authorization__c =auth.Id;
        auth1.Application_Type__c='Amendment';
        update auth1;
        
        country = testDataBRS.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        li = new AC__c();
        li = testData.newLineItem('Resale/Adoption', app, auth);

          
        RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId(); 
        Id vrrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Volunteer Monitoring Report').getRecordTypeId();
        
        loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
    //    loc.Observation_Date__c = date.today()-1;
   //     update loc;
        construct = testDataBRS.newconstruct(li.id);
        construct.Authorization__c = auth.id;
        update construct;
               
        rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Volunteer Monitoring Report';
        rs.Status__c = 'UnSubmitted';
        Report_Summary__c rs1 = new Report_Summary__c();
        rs1.Authorization__c = auth1.id;
        rs1.Certify_and_Submit__c = true;
        rs1.Description__c = 'Test';
        rs1.Due_Date__c = date.today() + 60;
        rs1.Equipment__c = 'Facility';
        rs1.Report_Type__c = 'Volunteer Monitoring Report';
        rs1.Status__c = 'UnSubmitted';
        insert rs1;        
        insert rs;        
       
        
        rsh = new Report_Summary_History__c();
        rsh.Authorization__c = auth.id;
        rsh.Submitted_Date__c = date.today();
        rsh.Report_Summary__c = rs.Id;
        rsh.Status__c = 'Saved';
        insert rsh; 
        
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()-3;
        sr.Monitoring_Period_End__c = date.today();
        sr.Observation_Date__c = date.today()-1;
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Authorization__c = auth.id;
        sr.Final_Volunteer_Monitoring_Report__c = 'No';
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.RecordTypeId = vrrectypeid;
        sr.Comments__c = 'Test description';
        sr.Quantity_Acres__c = 1;
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
        sr.Explanation__c = 'test';
        sr.Is_No_Planting__c = true;
        sr.No_Monitoring_Report__c = true;
        sr.Is_Submitted__c = true;
        sr.Start_Date__c= date.today();
    
        insert sr;
          
     
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = loc.id;
        eflRelatedRec.Construct__c  = construct.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;    
       
      }
      
      @isTest
      static void ReportSummary_TestMethod1()
      {
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
      
        app = new Application__c();
        app = testData.newapplication();
        auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
        country = testDataBRS.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        li = new AC__c();
        li = testData.newLineItem('Resale/Adoption', app, auth);

          
        RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId(); 
      
        loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
          
        construct = testDataBRS.newconstruct(li.id);
        construct.Authorization__c = auth.id;
        update construct;
         
         
        rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
       // rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Planting/Release Reports';
        rs.Status__c = 'UnSubmitted';
        insert rs;        
        
        Id prrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today();
        sr.Monitoring_Period_End__c = date.today() + 60;
        sr.Observation_Date__c = date.today() + 50;
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Authorization__c = auth.id;
        sr.Action_Taken__c = 'Test description';
        sr.Comments__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Any_Planting_Material_Harvested__c = 'No';
        sr.In_field_Termination_Date__c = date.today() + 60;
        sr.In_Field_Description__c = 'Test description';
        sr.How_was_material_disposed__c = 'Both';
        sr.Stored_Quantity__c = 5;
        sr.RecordTypeId = prrectypeid;
        sr.Quantity_Acres__c = 1;
        sr.Final_Volunteer_Monitoring_Report__c = 'No';
        //sr.No_Monitoring_Report__c = False;
        //sr.Units__c = 'Acres';
        sr.Stored_Material_Type__c = 'test';
        sr.Stored_Description__c = 'Test';
        sr.Off_Field_Destruction_Date__c = date.today() + 61;
        sr.Off_Field_Description__c = 'test';
        sr.Before_Harvest_Destruction_Date__c = date.today() + 61;
        sr.Before_Harvest_Description__c = 'test';
        sr.Still_Growing_Quantity__c = 4;
        sr.Still_Growing_Description__c = 'test';
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
        sr.Planted_Material_Destroyed_Before_Harves__c = 'Yes';
        sr.Planting_Material_Still_Growing__c  = 'Yes';
        sr.Explanation__c = 'test';
        sr.Unexpected_Effects__c = 'test';
        sr.Deleterious_Effects__c = 'test';
        sr.Deleterious_Effects_Data__c = 'test';
        sr.Crop_Observations__c = 'test';
        sr.Start_Date__c= date.today()-100;
        sr.Anticipated_Harvest_Destruct_Date__c = date.today()-99;  
     //   sr.Monitoring_Period_End__c = date.today()-99;
        insert sr;
          
         
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = loc.id;
        eflRelatedRec.Construct__c  = construct.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;
        
        eflRelatedRecGps = new GPS_Coordinate__c();
        eflRelatedRecGps.Self_Reporting__c = sr.id;
        eflRelatedRecGps.GPS_Coordinates__Latitude__s = 23;
        eflRelatedRecGps.GPS_Coordinates__Longitude__s =11;
        eflRelatedRecGps.Location__c = loc.id;
        insert eflRelatedRecGps;
        
        EFLSelfReportSummaryController cont = new EFLSelfReportSummaryController();
          
      }
      
      @IsTest 
      static void testForceGetSetReportSummary() {
      
          init();
          EFLSelfReportSummaryController cont = new EFLSelfReportSummaryController();
       
          cont.renderSection1 = false;
          cont.renderSection2 = false;
          cont.renderSection3 = false;
          cont.selfReporting = new Self_Reporting__c();
          cont.selfReportingList = new List<Self_Reporting__c>();
     //     cont.selfReportingCummList = new List<EFLSelfReportSummaryController.CummulativeWrapClass>();
          cont.rs = rs;//new Report_Summary__c();
       //   cont.eflRelatedRecList = new List<EFLSelfReportSummaryController.EFLRelatedRecordWrapper>();
      //    cont.eflRelatedRecListObservation = new List<EFL_Related_Record__c>();
     //     cont.eflRelatedRecListObservation = new List<Observation__c>();
     //     cont.erRelatedObj = new EFL_Related_Record__c();
      //    cont.erRelatedObjObservations = new Observation__c();
          cont.repSummName = '';
          cont.authorizationID = auth.id;
         // cont.reportSumID = '';
 //         cont.alertType = '';
 //         cont.gps1 = false;
//          cont.gps2 = false;
//          cont.gps3 = false;
//          cont.gps4 = false;
//          cont.gps5 = false;
//          cont.gps6 = false;
//          cont.errmsg = '';
//          cont.gpsno = '';
//          cont.currentPage = 0;
//          cont.headerStrList = new List<String>();
       //   cont.appObj = new Applicant_Attachments__c();
          cont.reportType = 'Volunteer Monitoring Report';
          cont.fileName = '';
          cont.fileName2 = '';
          cont.fileName3 = '';
          cont.selectedplantType = '';
          cont.obj = new Applicant_Attachments__c();
          cont.appAttachments = new List<Applicant_Attachments__c>();
          cont.appAttachments = new List<Applicant_Attachments__c>();
          cont.returnurl = '';
          cont.selfrepo = false;
          cont.ReportSummary = new list<Report_Summary__c>();
          cont.PrePlantingReportSummary = new list<Report_Summary__c>();
          cont.PlantingReportSummary = new list<Report_Summary__c>();
          cont.VolunteerReportSummary = new list<Report_Summary__c>();
          cont.FieldReportSummary = new list<Report_Summary__c>();
          cont.PreFloweringReportSummary = new list<Report_Summary__c>();
          cont.PreHarvestReportSummary = new list<Report_Summary__c>();
          cont.CleaningReportSummary = new list<Report_Summary__c>();
          cont.StorageReportSummary = new list<Report_Summary__c>();
          cont.FinalReportSummary = new list<Report_Summary__c>();
          cont.isCumulative = false;
          cont.PrePlantingReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.PlantingReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.VolunteerReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.FieldReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
         cont.PreFloweringReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.PreHarvestReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.CleaningReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.StorageReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.FinalReportSummaryWrapper = new List<EFLSelfReportSummaryController.wrapDataTables>();
          cont.cbiDisplayPdf = false;
 //         cont.PrePlantingview = false;
        //  cont.Plantingview = false;
//          cont.NoPlantingview = false;
        //  cont.Volunteerview = false;
 //         cont.FieldReportview = false;
//          cont.PreFloweringview = false;
//          cont.PreHarvestview = false;
//          cont.Cleaningview = false;
//          cont.Storageview = false;
//          cont.Finalview = false;
//          cont.panelview = false;
//          cont.locationsecview = false;
//          cont.attachdocview = false;
//          cont.attachdocview = false;
//          cont.certifyview = false;
//          cont.coordinateView = false;
//          cont.isValid = false; 
          //cont.selectedReporttype = '';
          cont.reportType = 'Planting/Release Reports';
      //    cont.selectedReporttype = 'Planting/Release Reports';
          cont.authObj = new authorizations__c();
          cont.reportRenderingObj = new Self_Report_Renderings__mdt();
          //cont.selfReportingMap = new Map<id, Self_Reporting__c>();
          cont.attachBlob = null;
          cont.attachBlob2 = null;
          cont.attachBlob3 = null;
          cont.isViewMode = false;
//          cont.constructKeyPrefix = '';
//          cont.locationKeyPrefix = '';
          cont.hasPlantingReport = false;
          cont.hasUnsubmittedPlantingReport = false;
          cont.hasUnsubmittedVMReport = false;
 //         cont.cbiPdfMap = new map<string,boolean>();
          cont.cummApplicantObj = new Applicant_Attachments__c();
          cont.cummApplicantListPlanting = new List<Applicant_Attachments__c>();
          cont.cummApplicantListVolun = new List<Applicant_Attachments__c>();
          ApexPages.StandardController sc = new ApexPages.StandardController(sr);
          EFLSelfReportSummaryController scont = new EFLSelfReportSummaryController(sc);
                
        test.startTest();
          PageReference pageRef = Page.EFLReportSummary;
           cont.authorizationID = auth.id;
         ApexPages.currentPage().getParameters().put('authId', auth.id); 
          Test.setCurrentPage(pageRef);
        cont.lineitems = new List<AC__c>();
        cont.lineitems.add(li);
      cont.authorizationID = auth.id;
      ApexPages.currentPage().getParameters().put('authId', auth.id); 
    //  ApexPages.standardController controller = new ApexPages.standardController(new <Applicant_Attachments__c> );
      
      cont.ReportSummaryAll();
        cont.checkIsSaved();
        cont.gotoReportPlanting();
        cont.gotoReportVolunteer();
        String idVal1 = System.currentPageReference().getParameters().get('idVal1');
        String idVal2 = System.currentPageReference().getParameters().get('idVal2');
        
 
          ApexPages.currentPage().getParameters().put('idVal1', rsh.id);
          ApexPages.currentPage().getParameters().put('idVal2', rs.id);
      //  ApexPages.currentPage().getParameters().put('idVal1', objaa.id);
                
        
      cont.authorizationID = auth.id;
      ApexPages.currentPage().getParameters().put('authId', auth.id); 
      String repType = 'Planting/Release Reports';
   //   List<Applicant_Attachments__c> lstAppAttach =  cont.handlecummulativeApplicants(repType);
  //    List<EFLSelfReportingWizardController.ReportSummaryWrapper> lstRSWrapper = cont.getCummulativereports(repType);
      repType = 'Volunteer Monitoring Report';
   //   lstAppAttach =  cont.handlecummulativeApplicants(repType);
   //   lstRSWrapper = cont.getCummulativereports(repType);  
      cont.reportType = 'Volunteer Monitoring Report';
    //  Report_Summary__c repSum = cont.createReportSummary(rs.id);
   //   ApexPages.currentPage().getParameters().put('idVal2', repSum.id);
  //    repSum = cont.getRepSummary();
 
      pageReference pgRef;
 
      Self_Reporting__c objSelfReportCopy = new Self_Reporting__c();
      objSelfReportCopy = sr.clone(false, true, false, false);
      objSelfReportCopy.How_was_material_disposed__c = 'Both (some of each)';
      objSelfReportCopy.Quantity_Acres__c = -2;
      insert objSelfReportCopy;
      ApexPages.currentPage().getParameters().put('idVal', rs.id);
      List<EFL_Related_Record__c> lstEFLRelatedRec = new List<EFL_Related_Record__c>();
      lstEFLRelatedRec.add(eflRelatedRec);
      cont.reportType = 'Planting/Release Reports';
      List<Report_Summary__c> lstRS = new List<Report_Summary__c>();
      lstRS.add(rs);
      cont.plantingReportSummaryList = lstRS;
      cont.reportType = 'Field Test Reports (Annual or Final)'; 
      List<Report_Summary__c> lstRepSum = new List<Report_Summary__c>();
  //    lstRepSum = cont.getReportSummary();
      cont.isCBI = true;
      ApexPages.currentPage().getParameters().put('pageCountVal', '3');
      cont.selfReporting = objSelfReportCopy;
      ApexPages.currentPage().getParameters().put('rowValConst', '3');
      pgRef = cont.deleteSummary();
  //    cont.deleteSelfRepRec();
  //    cont.deleteSelfRepRec();
        cont.deleteRshSummary();
        cont.deleteSummary();
      cont.deleteAppSummary();
      test.stopTest();  
    }
    
    @isTest
    public static void test1(){
        
        init();
        EFLSelfReportSummaryController cont = new EFLSelfReportSummaryController();
		EFLSelfReportingHelper selfhelper  = new EFLSelfReportingHelper();
        selfhelper.submitReport(rs);
        
        
    }

}