public class EFLPSQQuestion {
    
    public List<string> Options {get; set;}
    public String SelectedOption {get;set;}
    //Used incase of dynamic option to set the application request Answer
    public String SelectedOptionText {get;set;}
    public String oldOption {get;set;}
    public Wizard_Questionnaire__c psquestion{get;set;} //prescreen question
    public List<Wizard_Selections__c> psqOptions {get; set;}
    public Wizard_Selections__c psqSelectedOption {get;set;}
    
    public EFLPSQQuestion(){ 
        
    }
    
    public EFLPSQQuestion( Wizard_Questionnaire__c question, List<Wizard_Selections__c> options )
    {
        this.psquestion = question;
        this.psqOptions = options;
    }
        
}