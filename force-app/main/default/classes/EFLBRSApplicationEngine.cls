/*
* Purpose: Perform BRS specific Application operations
*/ 
public with sharing class EFLBRSApplicationEngine 
implements EFLIApplicationEngine{
    
    
    //Load Application wrapper using PSQ inputs                   
    public EFLApplicationWrapper createApplication(EFLPreScreeningWrapper psw){  
        
        return EFLApplicationEngineUtility.createApplication(psw); 
        
    }
    
    public void shareApplicationToAccount(list<Application__c> apps){
        EFLAccountSharingUtility.shareChildrenFromParent(apps, 'BRS');
    }
}