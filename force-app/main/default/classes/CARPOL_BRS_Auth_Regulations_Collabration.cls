public with sharing class CARPOL_BRS_Auth_Regulations_Collabration {

     public Id authID{get;set;}
     public Id Id{get;set;}
     
     public  String AuthJuncRecordTypeId {get;set;}
     
     public List<Authorization_Junction__c> AssociatedRegulations;
     public List<Authorization_Junction__c> Regulations;
     public List<Authorization_Junction__c> AddInformation;
     public List<Authorization_Junction__c> ApprovedSuppConditions;
     public List<Authorization_Junction__c> OriginalRegulations = new List<Authorization_Junction__c>();
     public List<Authorization_Junction__c> OriginalAddInformation = new List<Authorization_Junction__c>();
     public List<Authorization_Junction__c> OriginalApprovedSuppConditions = new List<Authorization_Junction__c>();

    public CARPOL_BRS_Auth_Regulations_Collabration(ApexPages.StandardController controller) {
    
         authID=ApexPages.currentPage().getParameters().get('id');
         Regulations = new List<Authorization_Junction__c>();
         AddInformation = new List<Authorization_Junction__c>();
         ApprovedSuppConditions = new List<Authorization_Junction__c>();
        
         AuthJuncRecordTypeId = Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('Regulation Junction').getRecordTypeId();

    }
 
     
    public List<Authorization_Junction__c> getRegulations(){
        
        
    
         Regulations = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                    BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Standard'
                                                        and Authorization__c =: authID order by Order_Number__c];
         OriginalRegulations =  Regulations.deepClone();
         return Regulations;
       }
          
    public List<Authorization_Junction__c> getAddInformation(){
        
        
    
         AddInformation = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                    BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ((Needed_ROP_Collaboration__c =: 'Yes' or Needed_ROP_Collaboration__c =: null )
                                                         or (Needed_BRS_Manager_Collaboration__c =: 'Yes'  or Needed_BRS_Manager_Collaboration__c =: null))
                                                        and Authorization__c =: authID order by Order_Number__c];
         OriginalAddInformation  =  AddInformation.deepClone();
         
               return AddInformation;
          }          
          
    public List<Authorization_Junction__c> getApprovedSuppConditions(){
        
        
    
         ApprovedSuppConditions = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                    BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c         
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and (Needed_ROP_Collaboration__c =: 'No' or Needed_BRS_Manager_Collaboration__c =: 'No' )
                                                        and Authorization__c =: authID order by Order_Number__c];
                                                        
               OriginalApprovedSuppConditions = ApprovedSuppConditions.deepClone();
               return ApprovedSuppConditions;
          }           
  
    

      public PageReference tosave() { 
 
          
            AssociatedRegulations = new List<Authorization_Junction__c>();
            
            for(integer i=0;i<Regulations.size();i++){
            
              if( OriginalRegulations[i].Applicant_Agree__c != Regulations[i].Applicant_Agree__c ||
                  OriginalRegulations[i].Applicant_Comments__c != Regulations[i].Applicant_Comments__c ){
                   Authorization_Junction__c aj = new Authorization_Junction__c();
                    aj.Id = Regulations[i].Id;
                    aj.Applicant_Agree__c = Regulations[i].Applicant_Agree__c;
                    aj.Applicant_Comments__c = Regulations[i].Applicant_Comments__c;
                    aj.BRS_Manager_Agree__c = Regulations[i].BRS_Manager_Agree__c;
                    aj.BRS_Manager_Comments__c = Regulations[i].BRS_Manager_Comments__c;
                    aj.Needed_BRS_Manager_Collaboration__c = Regulations[i].Needed_BRS_Manager_Collaboration__c;
                    aj.ROP_Agree__c = Regulations[i].ROP_Agree__c;  
                    aj.ROP_Comments__c = Regulations[i].ROP_Comments__c;
                    aj.Needed_ROP_Collaboration__c = Regulations[i].Needed_ROP_Collaboration__c;   
                    AssociatedRegulations.add(aj);
                  }
            
              }
              
              for(integer i=0;i<AddInformation.size();i++){
            
           //   if( OriginalAddInformation  [i].Applicant_Agree__c != AddInformation[i].Applicant_Agree__c ||
           //       OriginalAddInformation  [i].Applicant_Comments__c != AddInformation[i].Applicant_Comments__c ){
               if( OriginalAddInformation[i].Needed_ROP_Collaboration__c != AddInformation[i].Needed_ROP_Collaboration__c ||
                   OriginalAddInformation[i].Needed_BRS_Manager_Collaboration__c != AddInformation[i].Needed_BRS_Manager_Collaboration__c ){            
                   Authorization_Junction__c aj = new Authorization_Junction__c();
                    aj.Id = AddInformation[i].Id;
                    aj.Applicant_Agree__c = AddInformation[i].Applicant_Agree__c;
                    aj.Applicant_Comments__c = AddInformation[i].Applicant_Comments__c;
                    aj.BRS_Manager_Agree__c = AddInformation[i].BRS_Manager_Agree__c;
                    aj.BRS_Manager_Comments__c = AddInformation[i].BRS_Manager_Comments__c;
                    aj.Needed_BRS_Manager_Collaboration__c = AddInformation[i].Needed_BRS_Manager_Collaboration__c;
                    aj.ROP_Agree__c = AddInformation[i].ROP_Agree__c;  
                    aj.ROP_Comments__c = AddInformation[i].ROP_Comments__c;
                    aj.Needed_ROP_Collaboration__c = AddInformation[i].Needed_ROP_Collaboration__c;   
                    AssociatedRegulations.add(aj);
                  }
            
              }
              
              
             for(integer i=0;i<ApprovedSuppConditions.size();i++){
            
             // if( OriginalApprovedSuppConditions[i].Applicant_Agree__c != ApprovedSuppConditions[i].Applicant_Agree__c 
              //   OriginalApprovedSuppConditions[i].Applicant_Comments__c != ApprovedSuppConditions[i].Applicant_Comments__c ){
               if( OriginalApprovedSuppConditions[i].Needed_ROP_Collaboration__c != ApprovedSuppConditions[i].Needed_ROP_Collaboration__c ||
                   OriginalApprovedSuppConditions[i].Needed_BRS_Manager_Collaboration__c != ApprovedSuppConditions[i].Needed_BRS_Manager_Collaboration__c ){              
                   Authorization_Junction__c aj = new Authorization_Junction__c();
                    aj.Id = ApprovedSuppConditions[i].Id;
                    aj.Applicant_Agree__c = ApprovedSuppConditions[i].Applicant_Agree__c;
                    aj.Applicant_Comments__c = ApprovedSuppConditions[i].Applicant_Comments__c;
                    aj.BRS_Manager_Agree__c = ApprovedSuppConditions[i].BRS_Manager_Agree__c;
                    aj.BRS_Manager_Comments__c = ApprovedSuppConditions[i].BRS_Manager_Comments__c;
                    aj.Needed_BRS_Manager_Collaboration__c = ApprovedSuppConditions[i].Needed_BRS_Manager_Collaboration__c;
                    aj.ROP_Agree__c = ApprovedSuppConditions[i].ROP_Agree__c;  
                    aj.ROP_Comments__c = ApprovedSuppConditions[i].ROP_Comments__c;
                    aj.Needed_ROP_Collaboration__c = ApprovedSuppConditions[i].Needed_ROP_Collaboration__c;                      
                    
                    AssociatedRegulations.add(aj);
                  }
            
              }
            
            
            
             system.debug('##### AssociatedRegulations ### '+ AssociatedRegulations); 
             if(AssociatedRegulations.size()>0){
             update AssociatedRegulations;
             }
             PageReference RowEditing=new PageReference('/apex/Portal_Auth_Regulations_Detail?id='+authID);
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Saved Successfully!'));
             RowEditing.setRedirect(true); // 5-24-16 VV Uncommented
            return RowEditing;
 
       
       }
       
        public PageReference submit() { 
 
            list<Authorizations__c> authToUpdate = [Select Id,Status__c from Authorizations__c where id=:authID limit 1];
            
            if(authToUpdate.size() >0){
             authToUpdate[0].Status__c  = 'In Review';
             update authToUpdate;
            }
                        
            PageReference RowEditing=new PageReference('/apex/Portal_Auth_Regulations_Detail?id='+authID);
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully!'));
            RowEditing.setRedirect(true); // 5-24-16 VV Uncommented
            return RowEditing;
 
       
       } 
 
}