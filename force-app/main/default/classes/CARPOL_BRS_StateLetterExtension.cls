public with sharing class CARPOL_BRS_StateLetterExtension {
    public ID revID;
    public ID authID;
    public ID usrID;
    public ID appID;
    public ID lineitemID;
    public string commonName {get; set;}
    public string userEmail {get; set;}
    public string userFirstName {get; set;}
    public string userLastName {get; set;}
    public string userPhone {get; set;}
    public string appNum {get; set;}
    public List<Authorizations__c> authList {get; set;}
    public List<Reviewer__c> revList {get; set;}
    public List<User> usrList {get; set;}
    public List<AC__c> lineitemList {get; set;}
    public List<Application__c> appList{get;set;}
    public List<Link_Regulated_Articles__c> lraList {get; set;}
    public CARPOL_BRS_StateLetterExtension(ApexPages.StandardController stdController){
        revID = ApexPages.currentPage().getParameters().get('ID');
       //System.Debug('Reviewer ID : ' + revID);
        List<Reviewer__c> revList = new List<Reviewer__c>();
        revList = [SELECT Authorization__c FROM Reviewer__c WHERE ID = :revID];
        //System.debug('Reviewer List : '+ revList);
        for(Reviewer__c r : revList){
            authID = r.Authorization__c;
           //System.Debug('Authorization ID : '+ authID);
        }
        List<Authorizations__c> authList = [SELECT Name, Application__c, Bio_Tech_Reviewer__c FROM Authorizations__c WHERE ID = :authID];
        for(Authorizations__c a : authList){
            usrID = a.Bio_Tech_Reviewer__c;
            appID = a.Application__c;
        }
        List<User> usrList = [SELECT FirstName, LastName, Phone, Email FROM User WHERE ID = :usrID];
        for(User u : usrList){
            userEmail = u.Email;
            userFirstName  = u.FirstName;
            userLastName = u.LastName;
            userPhone = u.Phone;
        }
        List<Application__c> appList = [SELECT Name FROM Application__c WHERE ID = :appID];
        for(Application__c app : appList){
            appNum = app.Name;
        }
        List<AC__c> lineitemList = [SELECT ID FROM AC__c WHERE Authorization__c = :authID];
        for(AC__c ac: lineitemList){
            lineitemID = ac.ID;
        }
        List<Link_Regulated_Articles__c> lraList = [SELECT ID, Common_Name__c FROM Link_Regulated_Articles__c WHERE Line_Item__c = :lineitemID];
        for(Link_Regulated_Articles__c lr : lraList){
            commonName = lr.Common_Name__c;
        }
    }

}