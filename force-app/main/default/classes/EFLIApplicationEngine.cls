public interface EFLIApplicationEngine {

    EFLApplicationWrapper createApplication(EFLPreScreeningWrapper psw);
    
}