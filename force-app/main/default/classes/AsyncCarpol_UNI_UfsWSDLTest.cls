@IsTest
private with sharing class AsyncCarpol_UNI_UfsWSDLTest
{
    private static testMethod void testAll()
    {
        new AsyncCarpol_UNI_UfsWSDL.receiveTransactionResponse_elementFuture();
        new AsyncCarpol_UNI_UfsWSDL.AsyncReceiveUFSTransactionHttpSoap11Endpoint();
    }
    
    
    private class WebServiceMockImpl implements WebServiceMock
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
            {
                if(request instanceof Carpol_UNI_UfsWSDL.Message_element)
                response.put('response_x', new AsyncCarpol_UNI_UfsWSDL.receiveTransactionResponse_elementFuture());
                return;               
            }
    }       
       private static testMethod void testAsyncCompleteStartOnlineCollection() 
    {
        Test.startTest();
            Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
           
           
           
            try{
                System.Continuation cont = new System.Continuation(120);
                Carpol_UNI_UfsWSDL.Receipt_element receipt = new Carpol_UNI_UfsWSDL.Receipt_element();   
                   
                AsyncCarpol_UNI_UfsWSDL.AsyncReceiveUFSTransactionHttpSoap11Endpoint req = new AsyncCarpol_UNI_UfsWSDL.AsyncReceiveUFSTransactionHttpSoap11Endpoint();
                
                req.beginReceiveTransaction(cont,receipt,null,null);
            } catch(Exception e){
            }            

        Test.stopTest();
    }
}