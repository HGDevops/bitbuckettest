/**********************************************************************  
* @Purpose : This Utility class handles the logic to retrieve 
*            major attributes of any user in efile. 
* @User Story Number :  
************************************************************************/
public with sharing class EFLUserUtility {
    
  /* @purpose: returns User Details for both Partner and External Users.               
  */    
   public static User userDetails{
        set;
        get{if(userDetails == null){
 
               userDetails = [SELECT Id, ContactID, Name, Profile.UserLicense.Name, UserRole.Name, 
                                     Contact.AccountId, Contact.Account.Name, Profile.Name,AccountID,
                                     Contact.Account_Admin__c,Contact.State__r.Name
                                FROM User
                               WHERE ID  = : UserInfo.getUserID() LIMIT 1];
            }
                        system.debug('userDetail return--->'+userDetails );
            return userDetails; }
    }
    
 /* @purpose: returns Contact details for any specific Community Users.               
  */
    public static Contact contactDetails {
        set;
        get {
            if (contactDetails == null) {
                contactDetails = [SELECT ID, FirstName, LastName, Account_Admin__c, Account.Name, AccountId,Account.ParentId, 
                                         State__r.name, Phone, HomePhone, MobilePhone, Email, Title 
                                   FROM Contact 
                                  WHERE ID =: userDetails.ContactID 
                                  LIMIT 1];
                            system.debug('contactDetail--->'+contactDetails);
            }
                        system.debug('contactDetail return--->'+contactDetails);
                        
            
            return contactDetails;
        
    }}
    
    
 /* @purpose: returns if the community user is an Account Admin.               
  */  
    public static boolean isAccountAdmin(){
       return userDetails.Contact.Account_Admin__c;
    }
    
    


}