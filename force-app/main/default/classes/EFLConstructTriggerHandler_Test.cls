@IsTest
public class EFLConstructTriggerHandler_Test {
    public static  AC__c ac1;
    public static Account objacct;
    public static Contact objcont;
    public static Regulated_Article__c regArt;
   static void setupData(){
     CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
       String AccountRecordTypeId = testData.AccountRecordTypeId;
        objacct = testData.newAccount(AccountRecordTypeId); 
        objcont = testData.newcontact();
        Application__c objApp = testData.newapplication();
        ac1=testData.newLineItemBRS('Personal Use',objApp);
       // ac1.Construct_Status__c = 'Ready to Submit';
        // update ac1;
        GenotypeType__c genoTypeType = new GenotypeType__c();
        
        //Commented this code as this below newAuth method is failing with "EFLAuthorizationEngineFactory.EFLAuthorizationEngineException: No Authorization Engine found for Authorization: null"
        //Authorizations__c authObj = testData.newAuth(objApp.id);
        Domain__c objProg = new Domain__c();
        objProg.Name = 'AC';
        objProg.Active__c = true;
        insert objProg;
        
        Program_Prefix__c prefix = new Program_Prefix__c();
        prefix.Program__c = objProg.Id;
        prefix.Name = '555';
        insert prefix; 
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP';
        objTP.Recordtypeid = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c = prefix.Id;
        insert objTP;
        //appObj = testData.newapplication();
        
       test.startTest();
        Authorizations__c authObj = testData.newAuth(objApp.Id);
                       
        regArt = testData.newRegulatedArticle(authObj.Program_Pathway__c);
        link_Regulated_articles__c lra = new link_Regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(ac1.Id, regArt.id, 'Draft');
        Construct__c testConstruct = testData.newconstruct(ac1.Id, regArt);
        PhenoType__c phenoTypeExpected = testData.newphenotype(testConstruct.Id);
        genoTypeType.Construct__c = testConstruct.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;
        GenoType__c objGenoType = testData.newgenotype(testConstruct.id, genoTypeType);
        
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        Applicant_Attachments__c appAttObj = testDataAC.newAttach(ac1.id);
        Construct_Application_Junction__c objCAJ = new Construct_Application_Junction__c();
        objCAJ.Construct__c = testConstruct.Id;
        objCAJ.Application__c = objApp.Id;
        objCAJ.Authorization__c = authObj.Id;
        objCAJ.Line_Item__c = ac1.Id;
        objCAJ.Design_Protocol_Record_SOP__c = appAttObj.Id;
        insert objCAJ;
       test.stopTest();
    }
    public static testmethod void testConstruct(){
     setupData();
     List<Construct__c> Constructs = new  List<Construct__c>();
     Construct__c objcaj = new  Construct__c();
       objcaj.Construct_s__c='Construct';
        objcaj.Line_Item__c=ac1.Id;
        objcaj.Status__c='Ready to Submit';
        objcaj.Link_Regulated_Article__c = regArt.id;
        objcaj.Mode_of_Transformation__c='Direct Injection';
        objcaj.Corrections_Required__c ='';
        objcaj.Construct_is_CBI__c = true;
        Insert objcaj;
         //objcaj.Corrections_Required__c ='';
       // objcaj.Construct_is_CBI__c = true;
        ac1.Status__c='Saved';
        update ac1;
        objcaj.Status__c='Review Complete';
        update objcaj;
        Constructs.add(objcaj);
        
      EFLConstructTriggerHandler.ValidateConstructs(Constructs);
      EFLConstructTriggerHandler.ValidateConstructs(Constructs,Constructs);
      EFLConstructTriggerHandler.processConstructReadiness(Constructs);
      EFLConstructTriggerHandler.processConstructReadinessForUpdate(Constructs);
      EFLConstructTriggerHandler.resetConstructReadiness(Constructs);
      EFLConstructTriggerHandler.ValidateRequiredFields(Constructs);
      delete Constructs;
     }  
    //Construct with same name under same organization for different regulated article ERRORS   
   // @IsTest
    static void DuplicateConstructThrowsError(){
        
    }
    
    //Construct with same name under same organization for same regulated article does not error
   // @IsTest
    public static void NegativeTest1(){
        
    }
    
    //Construct with different name under same organization for same regulated article does not error
   // @IsTest    
    public static void NegativeTest2(){
        
    }
    
    //Construct with different name under same organization for different regulated article does not error
   // @IsTest    
    public static void NegativeTest3(){
        
    }
    
    //Construct with same name under different organization for same regulated article does not error
   // @IsTest    
    public static void NegativeTest4(){
        
    }
    //Construct with different name under different organization for same regulated article does not error
   // @IsTest    
    public static void NegativeTest5(){
        
    }
    //Construct with different name under different organization for different regulated article does not error
   // @IsTest
    public static void NegativeTest6(){
        
    }
}