/**
* Trigger Handler for the  Report Summary SObject. This class implements the EFLITrigger
* interface to help ensure the trigger code is bulkified and all in one place.
*/
public with sharing class EFLReportSummaryTriggerHandler implements EFLITrigger {
    /*
    * Attributes
    */
    list<Report_Summary__c> reportSummaryList = new list<Report_Summary__c>();
    final String WorkflowSelfReportingBackEnd = 'Self Reporting Back End';
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;

    /*
    * Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        if (EFLGenericUtility.isTriggerDisabled('EFLReportSummaryTrigger')) {
            return true;
        } else {
            return TriggerDisabled;
        }
    }

    /*
    * Constructor
    */
    public EFLReportSummaryTriggerHandler() {}

    /**
    * bulkBefore
    * This method is called prior to execution of a BEFORE trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    */
    public void bulkBefore() {
    }

    /**
    * bulkAfter
    * This method is called prior to execution of an AFTER trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    */
    public void bulkAfter() {
    }

    /**
    * beforeInsert
    * This method is called iteratively for each record to be inserted during a BEFORE
    * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
    */
    public void beforeInsert(SObject so) {
         Report_Summary__c reportSummaryRecord = (Report_Summary__c)so;
		 setDefaultValues(reportSummaryRecord);     
    }

    /**                                     
    * beforeUpdate
    * This method is called iteratively for each record to be updated during a BEFORE
    * trigger.
    */
    public void beforeUpdate(SObject oldSo, SObject so) {
    }

    /**
    * beforeDelete
    * This method is called iteratively for each record to be deleted during a BEFORE
    * trigger.
    */
    public void beforeDelete(SObject so) {}

    /**
    * afterInsert
    * This method is called iteratively for each record inserted during an AFTER
    * trigger. Always put field validation in the 'After' methods in case another trigger
    * has modified any values. The record is 'read only' by this point.
    */
    public void afterInsert(SObject so) {
    }

    /**
    * afterUpdate
    * This method is called iteratively for each record updated during an AFTER
    * trigger.
    */
    public void afterUpdate(SObject oldSo, SObject so) {
        Report_Summary__c reportSummaryNew = (Report_Summary__c)so;
        Report_Summary__c reportSummaryOld = (Report_Summary__c)oldSo;
        List<Report_Summary__c> listOfRs = new List<Report_Summary__c>();
		if(reportSummaryNew.Status__c == 'Submitted' && reportSummaryOld.Status__c != 'Submitted'){
			reportSummaryList.Add(reportSummaryNew);
		}
        
        if(reportSummaryNew.Status__c == 'Submitted'){
            listOfRs.Add(reportSummaryNew);
        }
        
        if(listOfRs.size() > 0){
            SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), listOfRs.get(0).getSObjectType().getDescribe().getName(), listOfRs, 'External Users View Only - Report Summary');
        }
    }
    
    /**
* afterDelete
* This method is called iteratively for each record deleted during an AFTER
* trigger.
*/ 
    public void afterDelete(SObject so)
    {
    }
    
    /**
* andFinally
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    public void andFinally()
    {

        if(reportSummaryList.size()>0){
            //Run SpringCM workflow
            SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), reportSummaryList.get(0).getSObjectType().getDescribe().getName(), reportSummaryList, WorkflowSelfReportingBackEnd);
        }
    }
   
   /*
    * Set Default values when inserting a record
    */ 
    private void setDefaultValues(Report_Summary__c reportSummaryRecord){
        reportSummaryRecord.Parent_Authorization__c = reportSummaryRecord.Authorization__c; 
    }
}