public Interface EFLIWizardActivityProcessor {
    
    List<SelectOption> loadOptions(EFLWizardActivity wizardActivity);
    void evaluateResult(EFLWizardActivity wizardActivity);

}