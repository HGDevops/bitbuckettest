public without sharing class EFLFindUserInfo {  
    @AuraEnabled 
    public static user fetchUser(){
        User u = [select id,firstName, lastname from User where id =: userInfo.getUserId()];
        return u;
    }    
    @AuraEnabled
    //method to check the entries and return the FA Access Code used flag 
    //this method is called when there is no ACIS contact or ACIS contact doesn't have email address
    public static EFLRegistration__c findInfo(String regNum, String faAccessCode){
        String regId = '';
        EFLRegistration__c reg;
        contact ACIScon;
        contact newACIScon;
        // check if account code and regn are correct and connected
        if(faAccessCode != '' && regNum!= ''){
            reg = [Select Id,EFLAccount__r.EFL_FA_Code_Used__c FROM EFLRegistration__c WHERE EFL_Account_FA_Access_Code__c=:faAccessCode AND EFLRegistrationNumber__c=:regNum limit 1];
        }
        if(!reg.EFLAccount__r.EFL_FA_Code_Used__c){
            //query logged in user information (this is the community user)
            User loggedInUser = [Select Id, firstname,lastname, ContactId,email FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
            //get JIT contact associated w/this user
            contact JITcon = [select Id, firstname,lastname, email,accountid from contact where Id=:loggedInUser.ContactId];  
            //check if there is NO ACIS contact with JIT contact's email, insert and set as FA (since FA is not used yet)
            if([select count() 
                from contact 
                where account.EFLAC_Access_Code__c =:faAccessCode 
                and firstname like :'%'+loggedInUser.firstName+'%'
                and lastname like :'%'+loggedInUser.lastName+'%' 
                and EFL_Role__c != null and eflJIT_Contact__c=false] ==0) { //recordtype.developername = 'AC_R_L_Contact'] ==0) {
                    newACIScon = new contact(
                        firstname=loggedInUser.firstname, 
                        lastname=loggedInUser.lastname, 
                        accountid=reg.EFLAccount__c,
                        EFL_Role__c='Institutional Official', 
                        email=loggedInUser.email,
                        EFL_Facility_Admin__c = true,
                        recordtypeid = [select id from recordtype where SObjectType = 'Contact' and developername='AC_R_L_Contact' limit 1].id);
                    try{insert newACIScon;}
                    catch(dmlexception e){system.debug('#### Exception on ACIS Contact insert: ' + e.getMessage());}
                }else{
                    //check if there is ACIS contact with JIT contact's email, update and set as FA, since FA is not used yet
                    ACIScon = [select Id, firstname, email 
                               from contact 
                               where 
                               account.EFLAC_Access_Code__c =:faAccessCode 
                               and firstname like :'%'+loggedInUser.firstName+'%' 
                               and lastname like :'%'+loggedInUser.lastName+'%' 
                               and EFL_Role__c != null
                               and EFLJIT_contact__c = false];//recordtype.developername = 'AC_R_L_Contact']; 
                    if(ACIScon!=null){
                    	ACIScon.Email = loggedInUser.Email; 
                    	ACIScon.firstname=loggedInUser.firstname;
                    	ACIScon.lastname=loggedInUser.lastname;                
                    	ACIScon.EFL_Facility_Admin__c = true;
                    	update ACIScon;
                    }
                }    
            // set the account level FA Access code used flag = true
            account acisAccount = [select id, EFL_FA_Code_Used__c from account where id=:reg.EFLAccount__c];
            acisAccount.EFL_FA_Code_Used__c = true;
            update acisAccount;
            // update JIT contact's account and type to ACIS Contact since this is the go forward contact in use
            // Save off JIT Account Id to delete that account later
            string jitAccountId = JITcon.accountid;
            if(JITcon.accountid != reg.eflAccount__c){
                JITcon.accountid = reg.eflAccount__c;
                JITcon.EFL_Role__c = 'Institutional Official';
                jitcon.EFL_Facility_Admin__c = true;
                jitcon.recordtypeid = [select id from recordtype where SObjectType = 'Contact' 
                                       and developername='AC_R_L_Contact' limit 1].id;
                //Update the JIT contact to have the ID of the ACIS contact so this is checked for subsequent logins
                string acisconId = '';
                if (ACIScon != null) acisconId = ACIScon.id;
                if (newACIScon != null) acisconId = newACIScon.id;
                If(acisconId != null) jitcon.EFL_ACIS_Contact__c = acisconId;    
                try{update JITcon;}
                catch(Exception e){
                    System.debug('#### Exception on JIT Contact update: ' + e.getMessage());
                }
            } 
            //Delete the JIT Account 
            /*account jitAc = [select id from account where id =:jitAccountId];

try{delete jitAc;}
catch(Exception E){

}*/
        } 
        return reg;  
    }
    
    @AuraEnabled
    //check the contact information whether to show the entry screen for the regn # and Account code
    public static sobject checkContactAccess(){        
        User loggedInUser = [Select Id, firstname,ContactId,email FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
        //get JIT contact associated w/this user
        contact JITcon = [select Id, firstname,lastname, email, AccountId, EFL_Facility_Admin__c,EFL_ACIS_Contact__c,
                          EFL_ACIS_Contact__r.EFL_Facility_Admin__c from contact where Id=:loggedInUser.ContactId];
        //check if there is an ACIS contact with JIT contact's email
        if ( [select COUNT() from contact where email=:JITcon.email and EFL_Role__c!=null and efljit_contact__c = false]==0 ){//recordtype.developername = 'AC_R_L_Contact'] ==0){        
            //if this JIT contact doesn't have an ACIS contact tied then show the FTUL screen.
            //if ( JITcon.EFL_ACIS_Contact__c == null ){
            return null; 
        }else{ // found ACIS contact so update name if there is any difference from JIT contact
            contact aciscon = [select id,email,LastName,AccountId,EFL_Role__c from contact where email=:JITcon.email and EFL_Role__c!=null and id != :JITcon.id];
            //contact aciscon = [select id,email,LastName,EFL_ACIS_Contact__c from contact where id=:JITcon.EFL_ACIS_Contact__c];
            ACIScon.firstname=JITcon.firstname;
            ACIScon.lastname=JITcon.lastname; 
            ACIScon.email = JITCon.email;
            try{
                update acisCon;
            }
            catch(dmlexception e){
                system.debug('#### Exception on ACIS Contact update: ' + e.getMessage());
            }
            if (ACIScon != null) {
                jitcon.EFL_ACIS_Contact__c = aciscon.Id;  
                jitcon.Accountid = AcisCon.AccountId;
                JITCon.EFL_Role__c = ACIScon.EFL_Role__c;
                try{update JITcon;}
                catch(Exception e){
                    System.debug('#### Exception on JIT Contact update: ' + e.getMessage());
                } 
            }
            contact JITconUpd = [select Id, firstname,lastname, email, AccountId, EFL_Facility_Admin__c,EFL_ACIS_Contact__c,
                                 EFL_ACIS_Contact__r.EFL_Facility_Admin__c from contact where Id=:loggedInUser.ContactId];      
            //system.debug('#### JITconUpd = '+JITconUpd);
            return JITconUpd; 
        }        
    }
    @future
    public static void regUserUpdate(Id regConId,Id updateUserId){
        User userToUpdate = [Select Id, ContactId FROM User WHERE Id=:updateUserId LIMIT 1];
        userToUpdate.ContactId = regConId;
        update userToUpdate;    
    }
    /*@invocableMethod
public static void deleteJITAccountACISContact (list<string> eflacisId){
EFL_ACIS_Contact__c eflacis  = [select Id,EFLContact__c,JITAccount__c from EFL_ACIS_Contact__c where Id=:eflacisId[0]];
account acctToDelete = [select Id from account where Id=:eflacis.JITAccount__c];
delete acctToDelete;
contact ConToDelete = [select Id  from contact where Id =:eflacis.EFLContact__c];
delete ConToDelete;
}*/
}