@isTest
public class EFLBatchCBPXMLClassTest {

    @isTest
    public static void createAttachmentListMultipleAuthsSuccess(){    
        CARPOL_UNI_DisableTrigger__c setting = new CARPOL_UNI_DisableTrigger__c();
        setting.Name = 'CARPOL_UNI_MasterApplicationTrigger';
        setting.disable__c = false;
        insert setting;
        
        CARPOL_UNI_DisableTrigger__c setting2 = new CARPOL_UNI_DisableTrigger__c();
        setting2.Name = 'EFLAuthorizationTrigger';
        setting2.disable__c = false;
        insert setting2;
        
        List<Authorizations__c> auths = new List<Authorizations__c>();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.id);
        Authorizations__c auth2 = testData.newAuth(app.id);
        Authorizations__c auth3 = testData.newAuth(app.id);
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='150';
        Insert objPrefix;
                
        Map<String, Blob> blobToAuth = new Map<String, Blob>();
        auth = [SELECT Name FROM Authorizations__c WHERE Id = :auth.Id];
        auth2 = [SELECT Name FROM Authorizations__c WHERE Id = :auth2.Id];
        auth3 = [SELECT Name FROM Authorizations__c WHERE Id = :auth3.Id];
        auth.Application_Type__c = 'Renewal';
        auth.Expiration_Date__c = Date.today().addDays(10);
        auth.Effective_Date__c = Date.today();
        auth.Status__c = 'Cancelled';
        auth.Permit_Number__c = '150-Z1BMRP5-A1';
        auth2.Application_Type__c = 'Renewal';
        auth2.Expiration_Date__c = Date.today().addDays(10);
        auth2.Effective_Date__c = Date.today();
        auth2.Status__c = 'Suspended';
        auth2.Permit_Number__c = '150-Z1BMRP6';
        auth3.Application_Type__c = 'Renewal';
        auth3.Expiration_Date__c = Date.today().addDays(10);
        auth3.Effective_Date__c = Date.today();
        auth3.Status__c = 'Superceded';
        auth3.Permit_Number__c = '150-Z1BMRP7';
        update auth;
        update auth2;
        update auth3;
        blobToAuth.put(auth.Name, Blob.valueOf('test'));
        blobToAuth.put(auth2.Name, Blob.valueOf('test'));
        blobToAuth.put(auth3.Name, Blob.valueOf('test'));
        auths = EFLIntegrationDocReferenceRepository.selectByAuth(blobToAuth);
        
        List<EFLIntegrationDocReference__c> docRefs = new List<EFLIntegrationDocReference__c>();
        for (Authorizations__c refAuth : auths){
            EFLIntegrationDocReference__c ref = new EFLIntegrationDocReference__c();
            ref.Object_Type__c = 'Authorizations__c';
            ref.Object_Name__c = refAuth.Name;
            ref.Document_ID__c = 'TestId';
            ref.Name = 'TestName' + refAuth.Name;
            docRefs.add(ref);
        }        
        insert docRefs;
        
        Test.startTest();
        EFLBatchCBPXMLClass.getXML();
        Test.stopTest();
        
        List<EFLIntegrationLog__c> integrationLog = [SELECT Id, Name, EFLIntegrationJobType__c, EFLJobNotes__c, EFLJobRunDate__c, Status__c FROM EFLIntegrationLog__c LIMIT 1];
        System.assertEquals(0, integrationLog.size());
    }
}