public class GenericPreScreeningProcessor implements EFLIPreScreeningProcessor 
{
    public ProcessSwitchOptions pso {get; set;}
    
    public GenericPreScreeningProcessor ()
    {
        pso = new ProcessSwitchOptions();
    }
    
    public void getNextQuestion(EFLPreScreeningWrapper psw)
    {
        EFLPSQQuestion psqq;
        String activityResult;
        if(psw.QuestionList.size() > 0 )
        {
            //Is the current question last of the questions already answered
            if(psw.currentQuestion.psquestion.id == psw.QuestionList[psw.QuestionList.size()-1].psquestion.id) 
            {   
                //Does the question associated with complext apex logic to derive next question
                if(setSelectedOption(psw) )
                {
                    // does the question have value based logic that derives nextscreen 
                    ID nextQuestionID = psw.currentQuestion.psqSelectedOption.Wizard_Next_Question__c;
                    Wizard_Questionnaire__c nextQuestion = EFLPSQUtility.getNextQuestionByValue(nextQuestionID);
                    if(nextQuestion != null) {
                        List<Wizard_Selections__c> questionOptions = EFLPSQUtility.getQuestionOptions(nextQuestion.ID,pso.MovementType, pso.ProgramPathway);
                        psqq = new EFLPSQQuestion(nextQuestion,questionOptions);
                    } 
                    if(psqq != null){
                        psw.QuestionList.add(psqq);
                        psw.currentQuestionIndex += 1;
                    }
                }
                
            }
            
            else
            {
                //Current question is not the last question the user answered
                if(selectedOptionChanged(psw))
                {
                    ResetQuestionList(psw);
                    retrieveNexQuestion(psw);
                    psqq = psw.QuestionList[psw.currentQuestionIndex]; 
                    psw.currentQuestionIndex += 1;
                }
                else
                {
                    psqq = psw.QuestionList[psw.currentQuestionIndex];
                    psw.currentQuestionIndex += 1;
                }
            }
        }
        else 
        {
            psw.currentQuestionIndex = 0;
            Wizard_Questionnaire__c startQuestion = EFLPSQUtility.getStartQuestion();            
            List<Wizard_Selections__c> questionOptions = EFLPSQUtility.getQuestionOptions(startQuestion.ID,'', null );
            psqq = new EFLPSQQuestion(startQuestion,questionOptions);
            
            if(psqq != null){
                psw.QuestionList.add(psqq);
                psw.currentQuestionIndex += 1;
            }
            
        }
        psw.currentQuestion = psqq;

    }
    
    public void getPrevQuestion(EFLPreScreeningWrapper psw)
    {
        if(psw != null){
            if(psw.QuestionList.size() > 1 && psw.currentQuestionIndex > 1 ) {
                psw.currentQuestionIndex -= 1;
                
                psw.currentQuestion = psw.QuestionList[psw.currentQuestionIndex-1];
                psw.currentQuestion.oldOption = psw.currentQuestion.SelectedOption;
                
            }
        }
        
    }
    
    public void updateMovementAndPathway (string movementType, string pathway)
    {
        pso.MovementType = movementType ;
        pso.ProgramPathway = pathway;
    }
    
    public List<SelectOption> getQuestionOptions (EFLPreScreeningWrapper psw)
    {
        List<SelectOption> options = new List<SelectOption>();
        if(  isActivityPrecessorDependent(psw) )
        {
            //string activityName = psw.QuestionList[psw.QuestionList.size()-1].psquestion.PSQActivity_Processor__c;
            string activityName = psw.currentQuestion.psquestion.PSQActivity_Processor__c;
            EFLIActivityResultProcessor ActivityResultProcessor= EFLActivityResultFactory.getActivityResultProcessor(pso.MovementType,pso.ProgramPathway, activityName);
            //TODO: Activity Processor is not configured for movement and Pathway
            options  = ActivityResultProcessor.loadOptions(psw);
            if(options.size()>0)
                return options;
        }
        
        for(Wizard_Selections__c opt : psw.currentQuestion.psqoptions)
        {
            options.add(new SelectOption(opt.Id,opt.Value__c));
        }
        return options;
    }
    
    public boolean setSelectedOption (EFLPreScreeningWrapper psw)
    {
        if(  isActivityPrecessorDependent(psw) )
        {
            String activityResult;
            string activityName = psw.QuestionList[psw.QuestionList.size()-1].psquestion.PSQActivity_Processor__c;
            EFLIActivityResultProcessor ActivityResultProcessor= EFLActivityResultFactory.getActivityResultProcessor(pso.MovementType,pso.ProgramPathway, activityName);
            //TODO: Activity Processor is not configured for movement and Pathway
            //processResult method on the Interface has been changed from string to void
            //All of the processing happens within that method to set the selectedoption 
            return ActivityResultProcessor.processResult(psw);
        }
        else
        {
            return setSelectedOptionByValue(psw);
            
        }
    }
    
    public boolean setSelectedOptionByActivityResult(EFLPreScreeningWrapper psw,string activityResult)
    {
        if(string.isNotBlank(activityResult))
        {
            for(Wizard_Selections__c opt : psw.currentQuestion.psqOptions)
            {
                if(opt.Activity_Processor_Result__c == activityResult)
                {
                    opt.Name = psw.currentQuestion.SelectedOption;
                    opt.Value__c = psw.currentQuestion.SelectedOption;
                    psw.currentQuestion.psqSelectedOption = opt;
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean setSelectedOptionByValue(EFLPreScreeningWrapper psw)
    {
        if(string.isNotBlank( psw.currentQuestion.SelectedOption))
        {
            for(Wizard_Selections__c opt : psw.currentQuestion.psqOptions)
            {
                if(opt.id == psw.currentQuestion.SelectedOption)
                {
                    psw.currentQuestion.psqSelectedOption = opt;
                    return true;
                }
            }
        }
        return false;
    }
    private void retrieveNexQuestion(EFLPreScreeningWrapper psw)
    {
        EFLPSQQuestion psqq;
        //Does the question associated with complext apex logic to derive next question
        if(setSelectedOption(psw) )
        { 
            // does the question have value based logic that derives nextscreen 
            ID nextQuestionID = psw.currentQuestion.psqSelectedOption.Wizard_Next_Question__c;
            Wizard_Questionnaire__c nextQuestion = EFLPSQUtility.getNextQuestionByValue(nextQuestionID);
           
            if(nextQuestion != null) {
                List<Wizard_Selections__c> questionOptions = EFLPSQUtility.getQuestionOptions(nextQuestion.ID,pso.MovementType, pso.ProgramPathway);
                psqq = new EFLPSQQuestion(nextQuestion,questionOptions);
            } 
            if(psqq != null){
                psw.QuestionList.add(psqq);
                psw.currentQuestionIndex += 1;
            }
        }

    }
    private boolean selectedOptionChanged(EFLPreScreeningWrapper psw)
    {
        if(psw.currentQuestion.SelectedOption != psw.currentQuestion.oldOption)
        {
            return true;
        }
        return false;
    }
    private void ResetQuestionList(EFLPreScreeningWrapper psw)
    {
       
        for(integer i = psw.QuestionList.size()-1;i>=psw.currentQuestionIndex; i--)
        {
            EFLPSQQuestion psqq = psw.QuestionList.Remove(i);
        }
        psw.currentQuestionIndex = psw.currentQuestionIndex-1;

    }
    
    public boolean isActivityPrecessorDependent(EFLPreScreeningWrapper psw)
    {
        for(Wizard_Selections__c opt : psw.currentQuestion.psqOptions)
        {
            if(string.isNotBlank(opt.Activity_Processor_Result__c))
            {
                return true;
            }
        }
        return false;
    }
    
    public class ProcessSwitchOptions
    {
        public string MovementType {get;set;}
        public id ProgramPathway {get;set;}
    }
}