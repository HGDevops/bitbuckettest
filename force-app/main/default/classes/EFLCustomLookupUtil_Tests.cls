@isTest
public class EFLCustomLookupUtil_Tests {
    
    @TestSetup
    static void makeData(){
        
    }
    
    @isTest
    static void testFetchLookUpValues(){
        
        User admin = EFLACTestServices.getAdminUser();
        UserRole r = [SELECT Id FROM UserRole LIMIT 1];
        admin.UserRoleId = r.Id;
        system.runAs(admin){
            Account a = EFLACTestServices.getAccount();
            insert a;
        }
        
        User u = getARGuestUser();
        system.runAs(u){
            test.startTest();
            EFLCustomLookupUtil util = new EFLCustomLookupUtil();
            util.sObjectToSearch = 'Account';
            
            //String searchKeyWord, String ObjectName, string pageData
            list<EFLCustomLookUpController.LookupItem> items = util.queryData('Test');
            system.assertEquals(1, items.size());
            test.stopTest();
        }
    }
    
    @isTest
    static void testFetchLookUpValues_Negative(){
        
        User admin = EFLACTestServices.getAdminUser();
        UserRole r = [SELECT Id FROM UserRole LIMIT 1];
        admin.UserRoleId = r.Id;
        system.runAs(admin){
            Account a = EFLACTestServices.getAccount();
            insert a;
        }
        
        User u = getARGuestUser();
        system.runAs(u){
            test.startTest();
            EFLCustomLookupUtil util = new EFLCustomLookupUtil();
            util.sObjectToSearch = 'Account';
            util.whereFields = new set<string>();
            util.whereFields.add('Name');
            
            //String searchKeyWord, String ObjectName, string pageData
            list<EFLCustomLookUpController.LookupItem> items = util.queryData('Other Value');
            system.assertEquals(0, items.size());
            test.stopTest();
        }
    }
    
    private static User getARGuestUser(){
        return EFLACTestServices.getCommunityUser('ACIS_Community');
    }
}