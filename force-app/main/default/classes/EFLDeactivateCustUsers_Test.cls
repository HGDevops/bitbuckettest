@isTest
public class EFLDeactivateCustUsers_Test
{
    public static testMethod void EFLDeactivateCustUsers_schedule()
    {
    Test.StartTest();    
    CARPOL_AC_TestDataManager objInst = new CARPOL_AC_TestDataManager();
    String AccountRecordTypeId = objInst.AccountRecordTypeId; 
    //Id RTId = [select id From recordtype where name = 'Accounts'].Id;  
   // Account acc = objInst.newAccount(RTId); 
    Account acc = objInst.newAccount(AccountRecordTypeId);   
    Contact portCon = objInst.newContact();
    contact CustCon = new contact(lastname='Cust', firstname='Con', accountid=acc.id);    
    insert custcon;
    
    
    Profile prof = [Select Id from Profile WHERE Name IN ('APHIS Applicant','eFile Applicant')Limit 1];
    User custusr = new User(Alias='stndt',email='testUser@gmail.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = prof.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',ContactID=CustCon.Id);
    insert custusr;


    User_invite__c ui = new User_invite__c();
    ui.Partner_Account__c = acc.Id;
    ui.Partner_Admin__c = portcon.id;
    ui.Invitee_List__c = '[{"userName":"treysmith0789@gmail.com","UserInviteId":null,"status":"Accepted","oldUserId":"005r0000001KWfpAAG","newConId":"003r0000006dBtyAAE","lastName":"Smith","firstName":"Trey","FedId":null,"email":"treysmith0789@gmail.com"}]';
    insert ui;
        
    EFLDeactivateCustUsers EFLInstance = new EFLDeactivateCustUsers();
    String schTime = '0 0 12 * * ?';
    System.schedule('Test_EFLDeactivateCustUsers',schTime,EFLInstance);
    Test.StopTest();
    }
}