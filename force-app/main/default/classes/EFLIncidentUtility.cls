public inherited sharing class EFLIncidentUtility {
    
    public static boolean needsTaskCreation(Incident__c oldIncidentRecord, Incident__c incidentRecord){
        
        if(oldIncidentRecord.Stage__c != incidentRecord.stage__c){ 
            return true;
        } 
        return false;
    }
 
}