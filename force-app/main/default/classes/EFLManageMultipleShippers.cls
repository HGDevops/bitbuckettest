public with sharing class EFLManageMultipleShippers 
{
    public String idProgpathway{ get; set; }
    public String idProgpathway2{ get; set; }
    public String applicantContactPrefix { get; set; }
    public String selectedpathwayName{ get; set; }
    public String portPrefix{get;set;}
    public String idScientificName{get;set;}
    public Additional_Contact_Detail__c  newAC{get;set;} 
    public Additional_Contact_Detail__c  brandnewACD{get;set;} 
    public applicant_contact__c brandnewAC{get;set;} 
    // first show the existing shippers list
    public string appid {get;set;}
    public string ACtype; //type of associated contact. get this from URL
    public string AssosContype {get;set;}
    public string lineitemid{get;set;}
    public list<Applicant_Contact__c> asconlist{get;set;}
    public ID shipperToDelete{get;set;}
    public Decimal shipperLimit; //Stores imit for the total number of shippers allowed for current Line Item
    
    public EFLManageMultipleShippers()
    {
        String pageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        //system.debug('>>>pageName = '+pageName);
        applicantContactPrefix = Applicant_Contact__c.SObjectType.getDescribe().getKeyPrefix();        
        newAC = new Additional_Contact_Detail__c (); 
        brandnewACD = new Additional_Contact_Detail__c (); 
        brandnewAC = new applicant_Contact__c ();  
        portPrefix = Facility__c.SObjectType.getDescribe().getKeyPrefix();        
        idScientificName = apexpages.currentpage().getparameters().get('Scientific_Name__c');         
        idProgpathway2 = idProgpathway = apexpages.currentpage().getparameters().get('strPathway'); 
        ACtype = EFLGenericUtility.sanitizeString( apexpages.currentpage().getparameters().get('type'));
        program_line_item_pathway__c plip = [select name, Maximum_number_of_shippers_allowed__c from program_line_item_pathway__c where id =:idProgpathway2];
        if (plip.Maximum_number_of_shippers_allowed__c!=null)
            shipperLimit = plip.Maximum_number_of_shippers_allowed__c - 1;
        selectedpathwayName = plip.name;
        //    applicantContactPrefix = Applicant_Contact__c.SObjectType.getDescribe().getKeyPrefix();
        appid  = apexpages.currentpage().getparameters().get('appId');
        lineitemid = EFLGenericUtility.sanitizeString( apexpages.currentpage().getparameters().get('lineitemId') );
        list<Additional_Contact_Detail__c> addlConlist = [select id,Line_Item__c, Last_Name__c from Additional_Contact_Detail__c where Line_Item__c =:lineitemid and Type_of_Associated_Contact__c=:ACtype];
        set<string> AssocConIds = new Set<string>();
        for(Additional_Contact_Detail__c ascon: addlConlist )
        {
            AssocConIds.add(ascon.Last_Name__c);
        }
        if (ACtype == 'Shipper')
            AssosContype = 'Shippers';
        if (ACtype == 'Permittee')  
            AssosContype = 'Permittees';
        asconlist = [select First_Name__c,name,Email_Address__c,Mailing_City__c,Mailing_Country__c,Mailing_State_LR__c,Mailing_State_LR__r.name,Mailing_Country_LR__c,
                     Mailing_Country_LR__r.name,Mailing_Street__c,Mailing_Zip_Postal_Code__c,Phone__c from Applicant_Contact__c where id in :AssocConIds];
    }
    
    public pagereference createAdd()
    {
        insert brandnewAC;        // inserting Associated contact  
        //system.debug('newAssocCon>>>'+brandnewAC);                                                   
        brandnewACD.last_name__c = brandnewAC.id;
        brandnewACD.line_item__c = lineitemid;
        brandnewACD.Type_of_Associated_Contact__c = ACtype;
        insert  brandnewACD; // Inserting JN record
        if (brandnewACD.Type_of_Associated_Contact__c == 'Shipper') // call method only for shipper        
            updateAuthGrpRec(brandnewACD.last_name__c);
        pagereference currpgref1 = new pagereference(apexpages.currentpage().geturl());
        currpgref1.setredirect(true);
        return currpgref1;                                                                                                                                                 
    }
    
    public PageReference populateLastNameLookup()
    {
        String RTId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        return null;
    }
    
    public pagereference saveACList()       // for lookup AC and association
    {    
        if(newAC.Last_Name__c == null){
            Apexpages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + 'Please enter a Last Name before saving'));
            return null;
        }
        Boolean contains = false;
        for(Applicant_Contact__c acc : asconlist)
        {
            if(acc.Name=='Shippers' && acc.First_Name__c=='Various')
            {
                contains=true;
            }
        }
        //If Shippers exceeded the limit, show warning message
        //system.debug('newAc='+newAC.Last_Name__c+newAc.Last_Name__r.First_Name__c);
        Applicant_Contact__c appCon= [Select Name,First_Name__c From Applicant_Contact__c Where id=:newAc.Last_Name__c];
        if(shipperLimit!= null && asconlist.size() >= shipperLimit && appCon.Name!='Shippers' && appCon.First_Name__c!='Various')
        {
            if(contains)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, +' You have exceeded the limit for adding shippers.'));
            else    
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, +' You have exceeded the limit for adding shippers. Add '+'"'+'Various Shippers'+'"'+' to continue'));
            return null;
        }
        else if(appCon.Name=='Shippers' && appCon.First_Name__c=='Various' && contains)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, +' You have already added '+'"'+'Various Shippers'+'"'+'.'));
            return null;
        }
        //Else save the record and redirect 
        else
        {
            //system.debug('newAC>>>'+newAC); 
            newAC.line_item__c = lineitemid;
            newAC.Type_of_Associated_Contact__c = ACtype;
            insert  newAC ; //Inserting JN record
            if (newAC.Type_of_Associated_Contact__c == 'Shipper') // call method only for shipper
                updateAuthGrpRec(newAC.last_name__c);
        }
        Pagereference currpgref2 = new pagereference(apexpages.currentpage().geturl());
        currpgref2.setredirect(true);
        return currpgref2;
    }
    
    public PageReference returntoLine() 
    {
        pagereference linepg = new pagereference('/apex/carpol_uni_lineitem?Id='+LineItemid+'&strPathway='+idProgpathway2+'&appId='+appId);
        return linepg ;
    } 
    
    public void updateAuthGrpRec(string AssocConId)
    {
        ac__c lineitem = [select Exporter_last_name__c, Authorization_Group_String__c from ac__c where id =:lineitemid];
        if (!LineItem.Authorization_Group_String__c.contains(lineitem.exporter_last_name__c)){                   
            LineItem.Authorization_Group_String__c += '_'+lineitem.exporter_last_name__c;   
        }
        if (!LineItem.Authorization_Group_String__c.contains(AssocConId)){                   
            LineItem.Authorization_Group_String__c += '_'+AssocConId;   
        }
        LineItem.Authorization_Group_String__c = LineItem.Authorization_Group_String__c.replace('UniqueAuthGroupId:_','');
        List<String> splitList = LineItem.Authorization_Group_String__c.split('_');               
        String finalGrpStr = 'UniqueAuthGroupId:';        
        splitList.sort();
        for(String s : splitList)
        {
            finalGrpStr+='_'+s;
        }
        LineItem.Authorization_Group_String__c = finalGrpStr;             
        update LineItem;
    } 
    
    public PageReference deleteShipper()
    {
        //system.debug('>>>>shipperToDelete'+shipperToDelete);
        Additional_Contact_Detail__c  addCon = new Additional_Contact_Detail__c();
        addCon = [select id,Line_Item__c, Last_Name__c from Additional_Contact_Detail__c where Last_Name__c=:shipperToDelete AND Line_Item__c =:lineitemid Limit 1];
        delete addCon;
        // remove AssocCon from auth group string on line item
        ac__c lineitem = [select Exporter_last_name__c, Authorization_Group_String__c from ac__c where id =:lineitemid]; 
        string strToRemove = '_'+shipperToDelete;
        //system.debug('>>>>strToRemove = '+strToRemove);
        if (lineitem.Authorization_Group_String__c.contains(shipperToDelete)){   
            //system.debug('>>> before = '+lineitem.Authorization_Group_String__c);       
            lineitem.Authorization_Group_String__c.replace(strToRemove,'');
            //system.debug('>>> after = '+lineitem.Authorization_Group_String__c);      
            update lineitem;
        }
        PageReference pg = new Pagereference(apexpages.currentpage().geturl());
        pg.setredirect(true);
        return pg;
    }
    
}