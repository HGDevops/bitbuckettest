@isTest//(seealldata=true)
private class CARPOL_VS_AddPorts_Test {
      @IsTest static void CARPOL_VS_AddPorts_Test() {
          //CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId; 
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
 
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);     
          // FIXME: commenting out multiple record creation due to TOO MANY SOQL errors.
          //AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          //AC__c ac3 = testData.newLineItem('Personal Use',objapp); 

          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          ac1.Authorization__c = objauth.id;
          update ac1;  
          
          Test.startTest();
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          //Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          //Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          //Group__c objgroup = testData.newgroup();
          
          //Test.startTest();
          PageReference pageRef = Page.CARPOL_AddPortsToGroup;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
          ApexPages.currentPage().getParameters().put('Id',ac1.id);
          CARPOL_VS_AddPorts extclass = new CARPOL_VS_AddPorts(sc);
          extclass.totalPorts = 10;
          extclass.disableRow = false;
          extclass.addRow();
          extclass.savePorts();
          extclass.removeRow();
          delete ac1;
          extclass.removeRow();
          system.assert(extclass != null);          
          Test.stopTest();     
      }
}