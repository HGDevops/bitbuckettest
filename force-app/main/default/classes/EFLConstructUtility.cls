public inherited sharing class EFLConstructUtility {
    
    /**** Get Previously Reviewed Constructs ****/
    public static List<Construct__c> getPreviouslyReviewedConstructs(Id lineItemId) {
        List<Link_Regulated_Articles__c> lstRA = new List<Link_Regulated_Articles__c>();
        lstRA = EFLBRSLineItemUtility.getRA(lineItemId);
        Set<Id> raids = new Set<Id>();
        for (Link_Regulated_Articles__c ras : lstRA ){
            raids.add(ras.Regulated_Article__c);
        }
        
        List<String> permitTypes = new List<String>();
        List<String> cbiTypes = new List<String>();
        String organization = '';
        AC__c lineItem = EFLLineItemRepository.selectbyID(lineItemId);
        if (String.isNotBlank(lineItem.Type_of_Permit__c)) {
            if (lineItem.Type_of_Permit__c == 'Notification') {
                permitTypes.add('Notification%');
            } else {
                permitTypes.add('Notification%');
                permitTypes.add('%Permit%');
            }
        }
        if (String.isNotBlank(lineItem.Does_This_Application_Contain_CBI__c)) {
            if (lineItem.Does_This_Application_Contain_CBI__c == 'No') {
                cbiTypes.add('No');
            } else {
                cbiTypes.add('No');
                cbiTypes.add('Yes');
            }
        }
        
        Application__c app = EFLApplicationRepository.selectApplicationById(lineItem.Application_Number__c);
        List<Construct__c> constructList = EFLConstructRepository.selectConstructs(
            'Review Complete', 'Authorized', raids, false, permitTypes, cbiTypes, app.Applicant_Name__r.AccountId);
        
        return constructList;
    }
    
    /* Move Up/Down - Update the Moved Construct Component and Consecutive Construct Component (move order differed by Operator) */
    public static void updateContructComponentSortOrder(id contructComponentId, id genotypeTypeId, string moveQueryOperator)
    {
        try
        {
            List<Genotype__c> genotypesToUpdate = new List<Genotype__c>(); 
            List<Genotype__c> genotypes = new List<Genotype__c>(); 
            genotypes = EFLConstructRepository.selectConstructComponentsForReOrdering(contructComponentId, genotypeTypeId, moveQueryOperator);	
            if(genotypes!=null && !genotypes.isEmpty() && genotypes.size()==2)
            {
                integer movedRecordSortOrder = (integer)genotypes[0].Construct_Component_Sort_Order__c;
                integer sequentialRecordSortOrder = (integer)genotypes[1].Construct_Component_Sort_Order__c;
                genotypesToUpdate.add(new Genotype__c(id=genotypes[0].id, Construct_Component_Sort_Order__c=sequentialRecordSortOrder));
                genotypesToUpdate.add(new Genotype__c(id=genotypes[1].id, Construct_Component_Sort_Order__c=movedRecordSortOrder));
            }
            if(!genotypesToUpdate.isEmpty())
            {
                update genotypesToUpdate;
            }
        }
        catch(exception e)
        {
            EFLErrorLog.createErrorLog('EFLConstructUtility.updateContructComponentSortOrder()',e);                
        } 
    }
    
}