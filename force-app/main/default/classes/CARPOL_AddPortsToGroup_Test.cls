@isTest(seealldata=false)
private class CARPOL_AddPortsToGroup_Test {
    //CM 6/2
    static testmethod void addPortsToGroup1(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        String PortsRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        Account testAcct = testData.newAccount(AccountRecordTypeId);
        Application__c testApp = testData.newapplication();
        AC__c testLine = testData.newLineItem('Personal Use', testApp);
        Group__c testGroup = testData.newgroup();
        Facility__c testFac1 = testData.newfacility('Domestic Port');
        Facility__c testFac2 = testData.newfacility('Foreign Port');
        testFac1.Description__c = 'TestTestTest';
        testFac1.RecordTypeId = PortsRecordTypeId;
        update testFac1;
        testLine.Port_of_Embarkation__c = testFac2.Id;
        testLine.Port_of_Entry__c = testFac1.Id;
        update testLine;
        System.debug('<<Group ID>>' + testGroup.Id);
            
        Test.startTest();
        PageReference pageRef = Page.CARPOL_AddPortsToGroup;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(testApp);
        ApexPages.currentPage().getParameters().put('Id',testApp.id);
        CARPOL_AddPortsToGroup extclass = new CARPOL_AddPortsToGroup(sc);
        extclass.selectedPorts = new List<SelectOption>();
        extclass.selectedPorts.add(new SelectOption(testFac1.Id, testFac1.Name));
        extclass.selectedPorts.add(new SelectOption(testFac2.Id, testFac2.Name));
        extclass.getPorts();
        extclass.updatePortGroup();
        extclass.getPorts();
        system.assert(extclass != null);
        Test.stopTest();
    }
}