public with sharing class CARPOL_CVB_PermitPDF {
    
    public static string currentPDF = 'CARPOL_CVB_PermitPDF';
    public boolean pn {get;set;}
    public ID appId {get; set;}
    public ID authId {get; set;}
    public string ApplAddress {get;set;}
    public Authorizations__c auth{get;set;}
    List<AC__c> aclst {get; set;}
    public AC__c lineitem{get;set;}
    public Contact con{get;set;}
    public Application__c appln {get;set;}
    public List<Authorization_Junction__c> scj {get;set;}  
    public boolean shipperMatchProducer {get;set;}
    public String ApplicationType {get;set;}
    public boolean specificPortFlag {get;set;}
    
    public CARPOL_CVB_PermitPDF(ApexPages.StandardController controller)
    {
        
        authId = ApexPages.currentPage().getParameters().get('id');
        auth= [SELECT Permit_Number__c,Date_Issued__c,Authorization_Letter_Content__c,Application__c, Application__r.Applicant_Name__r.name,Application__r.Applicant_Name__r.title,Application__r.Applicant_Name__c,Application__r.Applicant_Address__c, Issuer_Name__c,  
               Issuer_Title__c,Expiration_Date__c    FROM Authorizations__c WHERE id=:authId and Authorization_Type__c='Permit' LIMIT 1];
        appId = auth.Application__c;
        getACRecords();
        con = [SELECT id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry FROM contact WHERE id = :auth.Application__r.Applicant_Name__c];
        //Jon Sadler added 12/19/2017 - check for parameter to render permit number on pdf, and not on draft versions
        //The parameter is non-descript on purpose, to prevent unintentional url hacking
        //The URL is also not displayed to the enduser directly, but part of a larger esignLive redirect 
        string pnString = (ApexPages.currentPage().getParameters().get('pn'));
        if(string.isBlank(pnString)){pn=false;}
        else{pn=true;}
        
        
    }
    
    public List<AC__c> getACRecords(){
        if(auth.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ', Delivery_Recipient_State_ProvinceLU__r.Name, RA_Scientific_Name__r.name, Importer_Last_Name__r.Name,Exporter_Mailing_State_ProvinceLU__r.Name, Country_Of_Origin__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Scientific_Name__r.Name,Group__r.Name,Component__r.Name ,Country_of_Export__r.Name,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Port_of_Embarkation__r.Name,AC__c.Importer_Mailing_State_ProvinceLU__r.Name,Importer_Mailing_CountryLU__r.Name, Country_of_destination__r.Name  from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            lineitem = aclst[0];
            
            //Port Of Entry speicificty
            if (lineitem.Specific_Port_of_Entry__c == 'No, Allow entry at any US port') //Keerthi W-017162 01-19-2018
            {
                specificPortFlag = false;
            }
            else{ 
                specificPortFlag = true;
            } 
            
            //To render the Application Type on the PDF  
            
            if(lineitem.CVB_Application_Type__c=='RESEARCH AND EVALUATION' && lineitem.Movement_Type__c=='Import'){
                ApplicationType = 'RESEARCH AND EVALUATION';
            }else if(lineitem.CVB_Application_Type__c=='Yes' && lineitem.Movement_Type__c=='Transit'){
                ApplicationType = 'TRANSIT SHIPMENT ONLY';   
            }
            
            
            //NOTE :  Explore the option of moving Application Type to Custom Metatada Type
            
            //Comparison check to check if Producer address (Org, City, State, Country) is same as Shipper address (Org, City, State, Country)
            if((lineitem.Exporter_Mailing_CountryLU__c == lineitem.DeliveryRecipient_Mailing_CountryLU__c) &&
               (lineitem.Exporter_Mailing_City__c == lineitem.DeliveryRecipient_Mailing_City__c) &&
               (lineitem.Exporter_Mailing_State_ProvinceLU__c == lineitem.Delivery_Recipient_State_ProvinceLU__c)&&
               (lineitem.Exporter_Organization__c == lineitem.Delivery_Recipient_Organization__c)
              )
            {
                shipperMatchProducer = true;
                
            }
            else{
                shipperMatchProducer = false;
                
            }
        }
        
        return aclst;
    }
    
    
    public List<Authorization_Junction__c> getConditions(){
        try
        {
            
            scj= new List<Authorization_Junction__c>();
            scj = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :authId ORDER BY Order_Number__c ASC];
            
        }
        catch (Exception e){
            System.debug('ERROR:' + e);
        }
        return scj;
        
    }
    //End of Tharun changes --04/27/2016  
}