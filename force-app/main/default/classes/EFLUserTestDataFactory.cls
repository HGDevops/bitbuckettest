@isTest
public class EFLUserTestDataFactory {
  public static String testemail = 'puser000@test.com';  
  static final string emailencodingkey = 'UTF-8';
  static final string localesidkey = 'en_US';
  static final string languagelocalekey = 'en_US';
  static final string timezonesidkey = 'America/Los_Angeles';
  static final string alias='cspu';
   
 public static User getUser(String userCMDLabel) {
     id userroleId;
     id contactId;
     User testUser;
     EFLUserLibrary__mdt userCMD = [SELECT label,First_Name__c,role__c,isAdmin__c,
                                           Last_Name__c,Profile__c,email__c,
                                           usertype__c
                                      FROM EFLUserLibrary__mdt 
                                     WHERE label =:userCMDLabel
                                     LIMIT 1]; 

     if(userCMD.Role__c!=null && userCMD.Role__c!=''){
          userroleId = getUserRole(userCMD.Role__c).id;
       }

     if(userCMD.usertype__c != 'Standard'){
          contactId = getContact(userCMD.isAdmin__c).id;
       }
         testUser = new User(profileId = getProfile(userCMD.Profile__c).id, 
                             username  = getUserNamePrefix()+userCMD.email__c, 
                             email     = userCMD.email__c,
                             emailencodingkey = emailencodingkey, 
                             localesidkey     = localesidkey,
                             userroleid       =  userroleId,
                             languagelocalekey = languagelocalekey, 
                             timezonesidkey    = timezonesidkey,
                             alias      = alias, 
                             lastname   = userCMD.Last_Name__c, 
                             contactId  = contactId);
            Database.insert(testUser);  

        return testUser;
    } 
    
    public static String getUserNamePrefix(){
        return UserInfo.getOrganizationId() + System.now().millisecond();
    }
    
    public static account getAccount(){
       Account testAccount = new Account(name = randomString());
       Database.insert(testAccount);
       return testAccount;
    }
    
    public static contact getContact(boolean isadmin){
       Contact testContact = new Contact(AccountId = getAccount().id, 
                                         lastname = randomString(),
                                         Account_Admin__c=isadmin,
                                         FirstName = randomString() );
       Database.insert(testContact);
       return testContact;
    } 

    public static profile getProfile(String ProfileName){
        try{
            Profile userProfile = [select id
                                   from profile
                                   where name = :ProfileName
                                   limit 1];  
           return userProfile;
        }Catch(Exception e){
            system.debug('User Profile Name is invalid, please correct');
        } 
        return null;
    }   
    
    public static userRole getUserRole(String userRoleName){
        try{
         UserRole userRole = [select id
                             from UserRole
                             where name = :userRoleName
                             limit 1]; 
            return userRole;
        }catch(exception e){
           system.debug('User Role Name is invalid, please correct'); 
        } 
        return null;
    }  
    
    public static String randomString() {
       return EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0, 5);
     }    
   
}