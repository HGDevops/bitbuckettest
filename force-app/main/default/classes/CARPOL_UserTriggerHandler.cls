public inherited sharing class CARPOL_UserTriggerHandler {
    
    private static final String PARTNER_TYPE_NAME = 'Accounts';
    private static final String CONTACT_TYPE = 'Contact';
    
    public static void afterInsertHandler(List<User> userList){
        try{
            List<Id> contactIds = new List<Id>();
            List<Id> userIds = new List<Id>();
            for(User u: userList){
                contactIds.add(u.ContactId);
                userIds.add(u.id);
            }
            List<User> users = [select id, name, ContactId, Profile.UserLicense.Name from User where id in: userIds];
            Map<String, Group> pubGroupMap = new Map<String, Group>(); //to save public groups
            for(Group g: [select id, name from Group])
                pubGroupMap.put(g.Name, g);
            Id partnerRecTypeId = [select id from RecordType where developerName =: PARTNER_TYPE_NAME limit 1].id; //record type of Partner Account
            Id contactRecTypeId = [select id, name, DeveloperName, SobjectType from RecordType where name =: CONTACT_TYPE limit 1].id;
            Map<Id, Contact> contactsMap = new Map<Id, Contact>([select id, Name, Account.id, Account.recordTypeID, Account.Name 
                                                                 from Contact where id in: contactIds and recordTypeID =: contactRecTypeId and
                                                                 Account.recordTypeID =: partnerRecTypeId]);
            List<GroupMember> groupMembers = new List<GroupMember>();
            for(User u: users){
                if(u.Profile.UserLicense.Name == 'Partner Community Login'){
                    Contact tempCon = contactsMap.get(u.ContactId);
                    if(tempCon != null){
                        GroupMember mem = new GroupMember();
                        mem.GroupId = pubGroupMap.get(tempCon.Account.Name).id;
                        mem.UserOrGroupId = u.Id;
                        groupMembers.add(mem);
                    }
                    
                }
            }
            if(groupMembers.size() > 0)
                insert groupMembers;
        }
        catch(Exception e){
            System.debug('There was an exception >>>> ' + e.getMessage());
        }
    }
    
}