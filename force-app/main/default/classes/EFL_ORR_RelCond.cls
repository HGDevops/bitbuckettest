public with sharing class EFL_ORR_RelCond{
    private final Reviewer__c orrRec;
    public string orrId;
    public string authId;
    //Get  the review record and Auth id
    public EFL_ORR_RelCond(ApexPages.StandardController controller) {
        this.orrRec = (Reviewer__c)controller.getRecord();
        orrId= orrRec.Id;
        authId = [select Authorization__c from Reviewer__c where id=:orrId].Authorization__c;
    }
//This method is called when page loaded. It passes the ids and calls the page. If no Auth then throw error
public PageReference gotoRelatedCondPage() {
   if(authId == null){
     ApexPages.Message errMsg= new ApexPages.Message(ApexPages.Severity.ERROR,'No Authorization associated. Cannot add regulations.');
     ApexPages.addMessage(errMsg);
     return null;
        }else{
    PageReference pr = new PageReference ('/apex/EFL_Ltng_StateRegulations?id='+authId+'&orr='+orrId);   
    return pr;
    }
   }
 //Method for navigating back to ORR record.  
  public pagereference backToRR(){
    PageReference pr = new PageReference (URL.getSalesforceBaseUrl().toExternalForm()+'/Collaborator/s/detail/'+orrId); 
    return pr;
  }
}