@isTest
public class EFLLabelListControllerTest {
    
    @TestSetup
    static void makeData(){
        string voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
        string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
        
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        
        Application__c app = testData.newapplication();
        
        Authorizations__c auth = testData.newAuth(app.Id);
        auth.Expiration_Date__c = Date.today().addDays(10);
        auth.BRS_Introduction_Type__c = 'Import';
        update auth;
        
        Label__c l = new Label__c();
        l.Authorization__c = auth.Id;
        l.Status__c = voided;
        insert l;
    }
    
    @isTest
    static void testGetVoidedLabels(){
        Label__c label = [SELECT Name, Authorization__c FROM Label__c LIMIT 1];
        Profile p = EFLTestDataFactoryBulk.getProfileByName('BRS Analyst');
        User analyst = EFLTestDataFactoryBulk.getUser(p);
        //system.runAs(analyst){
            test.startTest();
            EFLLabelListController e = new EFLLabelListController();
            Authorizations__c auth = [SELECT Id FROM Authorizations__c  WHERE Id = :label.Authorization__c LIMIT 1];
            e.authId = auth.Id;
            system.assertEquals(1, e.labelNumbers.size());
            system.assertEquals(label.Name, e.labelNumbers[0]);
            test.stopTest();
        //}
    }
}