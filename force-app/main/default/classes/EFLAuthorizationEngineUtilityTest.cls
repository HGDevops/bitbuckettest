@isTest(SeeAllData=true)
public class EFLAuthorizationEngineUtilityTest {
    public static Authorizations__c auth;
    public static Application__c app;
    public static  AC__c lineItem;
    public static Program_Line_Item_Pathway__c plip;
    public static Program_Prefix__c objPrefix;
    public static Signature__c objTP;
    public static void testdata(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
         app = testData.newapplication();
        auth = testData.newAuth(app.id);
        plip = testData.newBRSPathway();
        objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Permit_PDF_Template__c='CARPOL_PPQ_PEQ_PermitPDF';
        objPrefix.Name='204';
        Insert objPrefix;
        objTP=new  Signature__c();
        objTP.Name='Test AC TP Jialin';
        objTP.Recordtypeid=Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c=objPrefix.Id;
        Insert objTP;
        
        /* auth = new Authorizations__c();
        auth.RecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId(); 
        auth.BRS_Introduction_Type__c='Interstate Movement';
        auth.thumbprint__c = objTP.id;
        auth.effective_date__c=system.today();
        auth.Expiry_Date__c = system.today().adddays(250);
        auth.BRS_Proposed_Start_Date__c=system.today();
        auth.BRS_Proposed_End_Date__c = system.today().adddays(250);
        auth.Application__c=app.id;
        auth.status__c = 'Submitted';
        auth.stage__c='Permit Package';
        auth.workflow_Number__c='150';
        auth.Authorization_Type__c='Permit';
        auth.Application_Type__c = 'New';
        auth.Expiration_Timeframe_Override__c='1 Month';
        insert auth; */
        lineItem = testData.newLineItemBRS('test',app);
        LineItem.Facility_Building_Type__c='Building';
        LineItem.Authorization__c= auth.id;
        update LineItem;
    }
    
    @IsTest
    static void TransferRecordsForAmendsTest(){
        testdata();
        CARPOL_BRS_TestDataManager testData1 = new CARPOL_BRS_TestDataManager();
        Authorizations__c auth2 = testData1.newAuth(app.id);
        auth.status__c = 'Draft';
        auth.Stage__c = 'Prepare Permit';
        update auth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = auth2.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        Incident__c incident = new Incident__c();
        incident.Incident_Date__c = Date.today();
        incident.Incident_Discovery_Date__c = Date.today();
        incident.Incident_Reported_Date__c = Date.today();
        incident.Authorization__c = auth.Id;
        insert incident;
  
        //create a location 
        
        Id locRT = Id.valueOf('012t000000005qlAAA');
        Location__c newLocation = testData1.newlocationByLineItem(lineItem.id,locRT);
        Incident_Location__c incLocation = new Incident_Location__c();
        incLocation.Incident__c = incident.Id;
        incLocation.Location__c = newLocation.Id;
        insert incLocation;
        
        Report_Summary__c reportSummary = new Report_Summary__c();
        reportSummary.Authorization__c = auth.Id;
        insert reportSummary;
        
        
        
        system.test.starttest();
        EFLAuthorizationEngineUtility.TransferRecordsForAmends(new Set<Id>{auth2.Id}, new List<Authorizations__c>{auth2});
        system.test.stoptest();
        
    }
    
    @IsTest
    static void supercedeParentAuthorizationTest(){
        testdata();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Authorizations__c auth1 = testData.newAuth(app.id);
        auth1.Parent_Authorization__c = auth.Id;
        update auth1;
        
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = auth1.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        
        test.startTest();
        EFLAuthorizationEngineUtility.supercedeParentAuthorization(auth1);
        test.stopTest();
    }
    
    static testmethod void getPermitNumberNewAppType(){
        // testdata();
         CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        
        Authorizations__c childAuth = testData.newAuth(app.id);
        childAuth.Parent_Authorization__c = auth.Id;
        childAuth.Application_Type__c = 'New';
        update childAuth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = childAuth.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        system.test.starttest();
        EFLAuthorizationEngineUtility.getPermitNumber(childAuth);
        system.test.stoptest();
    }
    
    
    //456-9876-A3
    static testmethod void getPermitNumberAmendmentPermitNumberAppType(){
        // testdata();
         CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        auth.Permit_Number__c = '456-9876-A3';
        update auth;
        Authorizations__c childAuth = testData.newAuth(app.id);
        childAuth.Parent_Authorization__c = auth.Id;
        childAuth.Application_Type__c = 'Amendment';
        update childAuth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = childAuth.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        system.test.starttest();
        EFLAuthorizationEngineUtility.getPermitNumber(childAuth);
        system.test.stoptest();
    }
    
    static testmethod void getPermitNumberAmendmentAppType(){
        // testdata();
         CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        auth.Permit_Number__c = '1';
        update auth;
        Authorizations__c childAuth = testData.newAuth(app.id);
        childAuth.Parent_Authorization__c = auth.Id;
        childAuth.Application_Type__c = 'Amendment';
        update childAuth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = childAuth.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        system.test.starttest();
        EFLAuthorizationEngineUtility.getPermitNumber(childAuth);
        system.test.stoptest();
    }
    
    //124-2QLSAE9-R1
    static testmethod void getPermitNumberRenewalPermitNumberAppType(){
        // testdata();
         CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        Authorizations__c childAuth = testData.newAuth(app.id);
        auth.Permit_Number__c = '124-2QLSAE9-R1';
        update auth;
        childAuth.Parent_Authorization__c = auth.Id;
        childAuth.Application_Type__c = 'Renewal';
        update childAuth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = childAuth.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        system.test.starttest();
        EFLAuthorizationEngineUtility.getPermitNumber(childAuth);
        system.test.stoptest();
    }
    
    static testmethod void getPermitNumberRenewalAppType(){
        // testdata();
         CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        Authorizations__c childAuth = testData.newAuth(app.id);
        auth.Permit_Number__c = '1';
        update auth;
        childAuth.Parent_Authorization__c = auth.Id;
        childAuth.Application_Type__c = 'Renewal';
        update childAuth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = childAuth.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        system.test.starttest();
        EFLAuthorizationEngineUtility.getPermitNumber(childAuth);
        system.test.stoptest();
    }
    
    
    
    @IsTest
    static void TransferRecordsForRenewalsTest(){
        testdata();
        CARPOL_BRS_TestDataManager testData1 = new CARPOL_BRS_TestDataManager();
        Authorizations__c auth2 = testData1.newAuth(app.id);
        auth.status__c = 'Draft';
        auth.Stage__c = 'Prepare Permit';
        update auth;
        Amendment_Renewal__c amendRenewal = new Amendment_Renewal__c();
        amendRenewal.Parent_Authorization__c = auth.Id;
        amendRenewal.Child_Authorization__c = auth2.Id;
        amendRenewal.RecordTypeId = '012t0000000GsAOAA0';
        insert amendRenewal;
        Incident__c incident = new Incident__c();
        incident.Incident_Date__c = Date.today();
        incident.Incident_Discovery_Date__c = Date.today();
        incident.Incident_Reported_Date__c = Date.today();
        incident.Authorization__c = auth.Id;
        insert incident;
        
        AC__c lineItem1 = testData1.newLineItemBRS('test',app);
        LineItem1.Facility_Building_Type__c='Building';
        LineItem1.Authorization__c= auth.id;
        update LineItem1;
        
        //create a location 
        Id locRT = Id.valueOf('012t000000005qlAAA');
        Location__c newLocation = testData1.newlocationByLineItem(lineItem1.id,locRT);
        Incident_Location__c incLocation = new Incident_Location__c();
        incLocation.Incident__c = incident.Id;
        incLocation.Location__c = newLocation.Id;
        insert incLocation;
        
        Inspection__c inspection = new Inspection__c();
        inspection.Authorization__c = auth.Id;
        inspection.Location__c = newLocation.Id;
        insert inspection;
        
        //Reviewer__c reviewer = new Reviewer__c();
        //reviewer.Authorization__c = auth.Id;
       // insert reviewer;
        
        system.test.starttest();
        EFLAuthorizationEngineUtility.TransferRecordsForRenewals(new Set<Id>{auth2.Id});
        system.test.stoptest();
        
    }
    
    @IsTest
    static void getLineItemTest(){
        testdata();
        string ACBRSLIRecordTypeId=Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
        system.test.starttest();
        EFLAuthorizationEngineUtility.getLineItem(auth, ACBRSLIRecordTypeId );
        system.test.stoptest();
    }
    
    @IsTest
    static void managedSharingTest(){
        testdata();
        system.test.starttest();
        EFLAuthorizationEngineUtility.managedSharing(UserInfo.getUserId(), auth);
        system.test.stoptest();
    }
    
    @IsTest
    static void getWorkflowNumberTest(){
        testdata();
        auth.BRS_Introduction_Type__c = 'Import';
        auth.BRS_Purpose_of_Permit__c = 'Traditional';
        update auth;
        system.test.starttest();
        EFLAuthorizationEngineUtility.getWorkflowNumber(auth);
        system.test.stoptest();
    }
    static testmethod void updateAuthorizationTest(){
         //testdata();
       CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        update auth;
        system.test.starttest();
        EFLAuthorizationEngineUtility.updateAuthorization(auth,auth);
        system.test.stoptest();
    }
    
    static testmethod void updateAuthorizationTest2(){
       testdata();
       CARPOL_BRS_TestDataManager testData1 = new CARPOL_BRS_TestDataManager();
        //Application__c app = testData.newapplication();
        // Authorizations__c auth = testData.newAuth(app.id);
        Authorizations__c auth2 = testData1.newAuth(app.id);
        objPrefix.Process_type__c = 'Test';
        update objPrefix;
        auth2.Authorization_Type__c = EFLGlobalConstants.getConstantValue('AUTHORIZATION DECISION TYPE PERMIT');
        auth2.Expiration_Timeframe_Override__c = 'Pathway';
        auth2.Expiration_Timeframe__c = '10';
        update auth2;
        system.test.starttest();
        EFLAuthorizationEngineUtility.updateAuthorization(auth,auth2);
        system.test.stoptest();
    }
    
    static testmethod void generateFacilityTest(){
        test.starttest();
        testdata();
        //CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
       // Application__c app = testData.newapplication();
        // Authorizations__c auth = testData.newAuth(app.id);
         Workflow_Task__c wft = new Workflow_Task__c();
        wft.Name = 'Issue Protected Plant Permit';
        wft.Status__c = 'Complete';
        wft.Authorization__c =auth.id;
        wft.Program__c='BRS';
        insert wft;
         CARPOL_BRS_TestDataManager testData1 = new CARPOL_BRS_TestDataManager();
        facility__c fac = testData1.newfacility('Inspection Station	');
        update auth;
       // String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Live Dogs').getRecordTypeId();      
        string RecordTypeId =Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
        //system.test.starttest();
        EFLAuthorizationEngineUtility.generateFacility(auth,lineItem);
         EFLAuthorizationEngineUtility.generateFacilityCapabilities(fac,lineItem);
         //EFLAuthorizationEngineUtility.getLineItem(auth,RecordTypeId);
        EFLAuthorizationEngineUtility.emailAuthorizationDocument(UserInfo.getSessionId(),app.id,auth.id,'Permit');
        
         EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Permit');
         EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Letter of Denial');
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Letter of No Permit Required');
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Letter of No Jurisdiction');
        system.test.stoptest();
    }
    
    
    static testmethod void FastTrackAttachTestMethod(){
       system.test.starttest();
        testdata();
       //CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
       // Application__c app = testData.newapplication();
       //  Authorizations__c auth = testData.newAuth(app.id);
         objPrefix.Permit_PDF_Template__c=null;
        update objPrefix;
        plip.Permit_PDF_Template__c='CARPOL_PPQ_PermitPDF';
        update plip;
        auth.Program_Pathway__c = plip.id;
        auth.CITES_Needed__c = false;
        update auth;
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Permit');
       system.test.stoptest();
    }
    
    static testmethod void FastTrackAttachPermitPDFNotNullTestMethod(){
       
        testdata();
       //CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        //Application__c app = testData.newapplication();
        // Authorizations__c auth = testData.newAuth(app.id);
         objPrefix.Permit_PDF_Template__c='null';
        update objPrefix;
        plip.Permit_PDF_Template__c='CARPOL_PPQ_PermitPDF';
        update plip;
        auth.Thumbprint__c = objTP.Id;
        auth.Program_Pathway__c = plip.id;
        auth.CITES_Needed__c = false;
        update auth;
        system.test.starttest();
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Permit');
       system.test.stoptest();
    }
    
    static testmethod void FastTrackAttachPermitPDFNotNullIssuedTestMethod(){
       
        testdata();
       //CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
       // Application__c app = testData.newapplication();
//         Authorizations__c auth = testData.newAuth(app.id);
         objPrefix.Permit_PDF_Template__c='null';
        update objPrefix;
        plip.Permit_PDF_Template__c='CARPOL_PPQ_PermitPDF';
        update plip;
        auth.Thumbprint__c = objTP.Id;
        auth.Program_Pathway__c = plip.id;
        auth.CITES_Needed__c = false;
        auth.Status__c = 'Issued';
        update auth;
        system.test.starttest();
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Permit');
       system.test.stoptest();
    }
    
    
    
    static testmethod void FastTrackAttachPermitPDFNullTestMethod(){
       
        testdata();
       //CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
       // Application__c app = testData.newapplication();
       //  Authorizations__c auth = testData.newAuth(app.id);
         objPrefix.Permit_PDF_Template__c=null;
        update objPrefix;
        plip.Permit_PDF_Template__c=null;
        update plip;
        auth.Thumbprint__c = objTP.Id;
        auth.Program_Pathway__c = plip.id;
        auth.CITES_Needed__c = false;
        auth.Status__c = 'Issued';
        update auth;
        system.test.starttest();
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Permit');
       system.test.stoptest();
    }
        
    static testmethod void FastTrackAttachTestMethodOne(){
       system.test.starttest();
        testdata();
      // CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
//    //    Application__c app = testData.newapplication();
       //  Authorizations__c auth = testData.newAuth(app.id);
        objPrefix.Name='211';
        update objPrefix; 
        plip.Permit_PDF_Template__c='CARPOL_PPQ_PermitPDF';
        update plip;
        auth.Program_Pathway__c = plip.id;
        auth.CITES_Needed__c = true;
        update auth;
        EFLAuthorizationEngineUtility.FastTrackAttach(UserInfo.getSessionId(),app.id,auth.id,'Permit');
       system.test.stoptest();
    }
    
    @IsTest
    static void checkValidStageTest(){
        Test.startTest();
        boolean retVlaue = EFLAuthorizationEngineUtility.checkValidStage('152', 'Application Review');
        Test.stopTest();
        System.assertEquals(false, retVlaue);
        
    }
    
    @IsTest
    static void validateActionAmendmentTest(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        Test.startTest();
        	boolean retVal = EFLAuthorizationEngineUtility.validateAction(auth, 'Amendment');
        Test.stopTest();
    }
    
      @IsTest
    static void validateActionRenewalTest(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        Test.startTest();
        	boolean retVal = EFLAuthorizationEngineUtility.validateAction(auth, 'Renewal');
        Test.stopTest();
    }
    
    @IsTest
    static void validateActionIssuedTest(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        auth.Status__c = 'Issued';
        update auth;
        Test.startTest();
        	boolean retVal = EFLAuthorizationEngineUtility.validateAction(auth, 'Renewal');
        Test.stopTest();
    }
    
   /* static testmethod void sendEmailTest(){ 
        // testdata();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Application__c app = testData.newapplication();
         Authorizations__c auth = testData.newAuth(app.id);
        EFLAuth_AddlRecpnt__c addRec = new EFLAuth_AddlRecpnt__c();
        addRec.EFLEmail__c = 'EFLTestEmail@devshe.com';
        addRec.EFLAuthorization__c = auth.id;
        insert addRec;
         system.test.starttest();
        EFLAuthorizationEngineUtility.sendEmail(auth);
        auth.Authorization_Type__c='Letter of Denial';
        update auth;
        EFLAuthorizationEngineUtility.sendEmail(auth);
        auth.Authorization_Type__c='Letter of No Permit Required';
        update auth;
        EFLAuthorizationEngineUtility.sendEmail(auth);
        auth.Authorization_Type__c='Letter of No Jurisdiction';
        update auth;
        EFLAuthorizationEngineUtility.sendEmail(auth);
        EFLAuthorizationEngineUtility.getAdditionalRecipients(auth);
        system.test.stoptest();  
    } */
}