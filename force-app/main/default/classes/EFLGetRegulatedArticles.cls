/**
*Author : Kishore Kumar
*Date Created : 2/13/2016
*Purpose : Public Class to Obtain Regulator Articles and its Categories 
*          used for both External and Internal Wizards  
**/
public inherited sharing class EFLGetRegulatedArticles {
    
    static final string ACTIVE = 'Active';
    
    @RemoteAction
    @readOnly 
    public static map<Id,String> getCategories(string pathway){
        map<Id,String> categoryMap = new map<Id,String>();
        for (AggregateResult results : [Select Regulated_Article__r.Category_Group_Ref__r.Name grpName,Regulated_Article__r.Category_Group_Ref__c grpId from RA_Scientific_Name_Junction__c 
                                        where (Program_Pathway__c=:pathway or Program_Pathway__r.Name =:pathway ) and Status__c =: 'Active'
                                        group by Regulated_Article__r.Category_Group_Ref__r.Name,Regulated_Article__r.Category_Group_Ref__c order by Regulated_Article__r.Category_Group_Ref__r.Name ASC]){
                                            if((string)results.get('grpName') !=null && (string)results.get('grpName') != '')
                                                categoryMap.put((ID)results.get('grpId'),(string)results.get('grpName'));
                                        }
        return categoryMap;
    }
    
    @RemoteAction
    @readOnly 
    public static list<Id> getRegulatedArticles(string pathway){
        TRANSIENT list<Id> RAIdList = new list<Id>();
        string active = 'Active';
        //restricted at 100 records to overcome governor limits
        //TODO: Restrict this function to what's reasonable for default display in the interface
        string queryString = 'Select Regulated_Article__c from RA_Scientific_Name_Junction__c where (Program_Pathway__c=:pathway or Program_Pathway__r.Name =:pathway ) and Status__c =: active LIMIT 100'   ;
        list<RA_Scientific_Name_Junction__c> listRAPW=Database.query(queryString);           
        for(RA_Scientific_Name_Junction__c r:listRAPW ){
            RAIdList.add(r.Regulated_Article__c);
        }
        return RAIdList;
    }
    
    
    @RemoteAction
    @readOnly 
    public static list<Regulated_Article__c> getRegulatedArticles(string pathway,string SelectedCategory){
        TRANSIENT list<Regulated_Article__c> RAList = new list<Regulated_Article__c>();        
        //TODO: Restrict this function to what's reasonable for default display in the interface             
        list<Regulated_Article__c> listRA=[select id,Name,Scientific_Name__c,Common_Name__c ,
                                           Additional_Common_Name__c,Additional_Scientific_Name__c,
                                           Category_Group_Ref__r.Name 
                                           from Regulated_Article__c 
                                           where Category_Group_Ref__c =: SelectedCategory 
                                           and Status__c =: 'Active' 
                                           limit 100];
        for(Regulated_Article__c r:listRA ){
            RAList.add(r);
        }
        return RAList;
    } 
    
    public static list<Id> getRegulatedArticleIdsbyPathway(id programPathwayId){
        
        //string active = 'Active';
        list<Id> RAIdList = new list<Id>();
        string queryString = 'Select Regulated_Article__c from RA_Scientific_Name_Junction__c where (Program_Pathway__c=:programPathwayId or Program_Pathway__r.Name =:programPathwayId) and Status__c =: ACTIVE';
        
        list<RA_Scientific_Name_Junction__c> listRAPW=Database.query(queryString);           
        
        for(RA_Scientific_Name_Junction__c r:listRAPW ){
            RAIdList.add(r.Regulated_Article__c);
        }
        return RAIdList;
    }  
    
    public static List<Regulated_Article__c> getRegulatedArticlesbyPathway(id programPathwayId,set<id> categoryIds){
        
        list<Regulated_Article__c> listRA=[select id,Name,Scientific_Name__c,Common_Name__c ,
                                           Additional_Common_Name__c,Additional_Scientific_Name__c,
                                           Program_Pathway__r.program__r.name 
                                           from Regulated_Article__c 
                                           where id IN: getRegulatedArticleIdsbyPathway(programPathwayId) //Category_Group_Ref__c =: SelectedCategory 
                                           and Category_Group_Ref__c IN: categoryIds
                                           and Status__c =: ACTIVE ];
        
        return listRA;        
    }    
    @RemoteAction
    @readOnly    
    public static List<Regulated_Article__c > searchRegulatedArticles(List<Regulated_Article__c > listRSA, string sSrhArticle, string sSelCat,string SelectedPathwayId){
        List<RA_Scientific_Name_Junction__c> listRAPW=new List<RA_Scientific_Name_Junction__c>();
        List<RA_Scientific_Name_Junction__c> testlist=new List<RA_Scientific_Name_Junction__c>();          
        String status_active ='Active'; 
        list<Id> RAIdList = new list<Id>();  
        if(sSrhArticle!='' && sSrhArticle!=null)
        {
            //             listRAPW=[Select Regulated_Article__c,Primary_Scientific_Name__c from RA_Scientific_Name_Junction__c where Program_Pathway__c=:SelectedPathwayId];
            //listRAPW=[Select Regulated_Article__c,Primary_Scientific_Name__c from RA_Scientific_Name_Junction__c where Program_Pathway__c=:SelectedPathwayId Limit 40000];             
            if(sSelCat!=null && sSelCat!='')            
            {
                sSelCat += '%' + sSelCat + '%';
                ssrharticle = '%' + ssrharticle + '%';
                listRAPW = database.query('Select Regulated_Article__c,Regulated_Article__r.Category_Group_Ref__r.Name, Primary_Scientific_Name__c from RA_Scientific_Name_Junction__c where Program_Pathway__c=:SelectedPathwayId and Regulated_Article__r.Category_Group_Ref__c=:sSelCat and Regulated_Article__r.status__c=\''+status_active+'\' and (Regulated_Article__r.Name like :sSrhArticle OR Regulated_Article__r.Additional_Common_Name__c like :sSrhArticle OR Regulated_Article__r.Family__c like :sSrhArticle OR Regulated_Article__r.Scientific_Name__c like :sSrhArticle OR Regulated_Article__r.Additional_Scientific_Name__c like :sSrhArticle OR Regulated_Article__r.Common_Disease_Name__c like :sSrhArticle OR Regulated_Article__r.Common_Name__c like :sSrhArticle)');                             
            }
            else
            {
                ssrharticle = '%' + ssrharticle + '%';
                listRAPW = database.query('Select Regulated_Article__c,Regulated_Article__r.Category_Group_Ref__r.Name, Primary_Scientific_Name__c from RA_Scientific_Name_Junction__c where Program_Pathway__c=:SelectedPathwayId and Regulated_Article__r.status__c=\''+status_active+'\' and (Regulated_Article__r.Name like  :sSrhArticle OR Regulated_Article__r.Additional_Common_Name__c like  :sSrhArticle OR Regulated_Article__r.Family__c like  :sSrhArticle OR Regulated_Article__r.Scientific_Name__c like  :sSrhArticle OR Regulated_Article__r.Additional_Scientific_Name__c like  :sSrhArticle OR Regulated_Article__r.Common_Disease_Name__c like  :sSrhArticle OR Regulated_Article__r.Common_Name__c like  :sSrhArticle)');             
            }
            
            for(RA_Scientific_Name_Junction__c r:listRAPW ){
                RAIdList.add(r.Regulated_Article__c);
            } 
            
            if(sSelCat!=null && sSelCat!='')
            {
                sSelCat += '%' + sSelCat + '%';
                ssrharticle = '%' + ssrharticle + '%';
                listRSA=database.query('select Name,Program_Pathway__r.program__r.name, Scientific_Name__c,Category_Group_Ref__r.Name,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c from Regulated_Article__c where ID in:RAIdList and Category_Group_Ref__c=:sSelCat and status__c=\''+status_active+'\' and (Name like  :sSrhArticle OR Additional_Common_Name__c like  :sSrhArticle OR Family__c like  :sSrhArticle OR Scientific_Name__c like  :sSrhArticle OR Additional_Scientific_Name__c like  :sSrhArticle OR Common_Disease_Name__c like  :sSrhArticle OR Common_Name__c like  :sSrhArticle)'+' order by Name');                
            }
            else
            {
                ssrharticle = '%' + ssrharticle + '%';
                listRSA=database.query('select Name,Program_Pathway__r.program__r.name, Scientific_Name__c,Category_Group_Ref__r.Name,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c from Regulated_Article__c  where ID in:RAIdList and status__c=\''+status_active+'\' and (Name like  :sSrhArticle OR Additional_Common_Name__c like  :sSrhArticle OR Family__c like  :sSrhArticle OR Scientific_Name__c like  :sSrhArticle OR Additional_Scientific_Name__c like  :sSrhArticle OR Common_Disease_Name__c like  :sSrhArticle OR Common_Name__c like  :sSrhArticle)'+' order by Name');
            }
        }
        // VV added for W-017570
        /* system.debug ('#### prgmNm  = '+prgmNm );
for (Regulated_Article__c updlistRSA: listRSA){
if(prgmNm != 'PPQ')
updlistRSA.Common_Name__c =  updlistRSA.Category_Group_Ref__r.Name + ' - '+updlistRSA.Common_Name__c;
}*/        
        listRSA.sort();       
        return listRSA;
    }       
    
    
}