/*
* ClassName: EFLCreateApplFromTemplate_Test
* CreatedBy: Vijay Vellaturi
* LastModifiedBy: Vijay Vellaturi
* LastModifiedOn: 24 Jan 2017
* Description: This is to Cover the code for the class: EFLCreateApplFromTemplate
* Revision History

*/
@IsTest(seeAlldata=false)
public class EFLCreateApplFromTemplate_Test{
    static testMethod void testEFLCreateApplFromTemplate(){
            String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
            CARPOL_AC_TestDataManager dataObj = new CARPOL_AC_TestDataManager ();
            dataObj.insertcustomsettings();
            Application__c  newApp =dataObj.newapplication();
            AC__c  newLine = dataObj.newLineItem('Personal Use', newApp);
            newLine.Proposed_date_of_arrival__c = date.today().addDays(30);
            newLine.Proposed_Start_Date__c = date.today().addDays(30);
            newLine.Proposed_End_Date__c = date.today().addYears(1);
            newLine.Departure_Time__c = date.today().addDays(1);
            update newLine;
            test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.EFLCreateApplFromTemplate'));
            EFLCreateApplFromTemplate obj = new EFLCreateApplFromTemplate();
            obj.selectedapplId = newApp.id;
            obj.deepClonefromAppl();
            system.assert(obj != null);
            test.stopTest();
    }         
}