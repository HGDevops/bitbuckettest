@isTest
 private class EQSScheduledTest{
     static testmethod void  testschedule(){
      Id recID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('APHIS EQS Responder').getRecordTypeId();
        //String entryID = recID.substring(0, recID.length()-3);
        Id accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('APHIS EQS Organization').getRecordTypeId();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User u0  = new User(
            email= 'con1@testing123.com', 
            profileid = p.Id,
            UserName= 'con22@testing.com', 
            Alias = 'usr0',
            TimeZoneSidKey='America/New_York',
            EmailEncodingKey='ISO-8859-1',
            LocaleSidKey='en_US', 
            LanguageLocaleKey='en_US',
            FirstName = 'Test EQS Data',
            LastName = 'Integrator'
        );
           insert u0;
       Test.startTest();
       List<User> ul = [select id from user where name = 'Test EQS Data Integrator'];
       id eqsuserid = ul[0].id;
        if(ul.size() > 0 && Test.isRunningTest()){
            //Querying the test user in case of test class execution.
            User testUser = [Select id from user where name = 'Test EQS Data Integrator' Limit 1];
            eqsuserid = testUser.id;
        }
       EQSContactShareScheduled sh1 = new EQSContactShareScheduled();
       String sch = '0 0 2 * * ?'; 
       system.schedule('Test Territory Check', sch, sh1); 
       Test.stopTest();
     }
  }