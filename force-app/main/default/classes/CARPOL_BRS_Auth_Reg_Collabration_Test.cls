/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class CARPOL_BRS_Auth_Reg_Collabration_Test {

    static testMethod void test_CARPOL_BRS_Auth_Reg_Collabration() {

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          Regulation__c objReg = testData.newRegulation('Standard','');
          Regulation__c objReg1 = testData.newRegulation('Supplemental Conditions','');
          Regulation__c objReg2 = testData.newRegulation('Supplemental Conditions','');
          Authorization_Junction__c objAJ = testData.newAuthorizationJunction(objauth.Id,objReg.Id);
          objAJ.Applicant_Agree__c = 'Agree';
          objAJ.Applicant_Comments__c = 'Test1';
          update objAJ;
          Authorization_Junction__c objAJ1 = testData.newAuthorizationJunction(objauth.Id,objReg1.Id);
          objAJ1.Applicant_Agree__c = 'Disagree';
          objAJ1.Applicant_Comments__c = 'Test2';          
          update objAJ1;          
          Authorization_Junction__c objAJ2 = testData.newAuthorizationJunction(objauth.Id,objReg2.Id);
          objAJ2.Applicant_Agree__c = 'Agree';
          objAJ2.Applicant_Comments__c = 'Test3';
          objAJ2.Needed_ROP_Collaboration__c = 'No';
          objAJ2.Needed_BRS_Manager_Collaboration__c = 'No';
          update objAJ2;          
          
          Test.startTest(); 
          PageReference pageRef = Page.CARPOL_BRS_Auth_Reg_Collaboration;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('Id',objauth.id);                     
          CARPOL_BRS_Auth_Regulations_Collabration extclass = new CARPOL_BRS_Auth_Regulations_Collabration(sc);
          List<Authorization_Junction__c> lstAJ = extclass.getRegulations();
          List<Authorization_Junction__c> lstAJ1 = extclass.getAddInformation();
          List<Authorization_Junction__c> lstAJ2 = extclass.getApprovedSuppConditions();
          
          List<Authorization_Junction__c> updAJ = new List<Authorization_Junction__c>();
          for(Integer i=0;i<lstAJ.size();i++){
          		Authorization_Junction__c objUpdAj = lstAJ[i];
          		objUpdAj.Applicant_Comments__c = 'Test2131'+i;
          		objUpdAj.Needed_BRS_Manager_Collaboration__c = 'No';
          		objUpdAj.Needed_ROP_Collaboration__c = 'No';          		
          		updAJ.add(objUpdAj);
          }
          update updAJ;
          
          List<Authorization_Junction__c> updAJ1 = new List<Authorization_Junction__c>();
          for(Integer i=0;i<lstAJ1.size();i++){
          		Authorization_Junction__c objUpdAj1 = lstAJ1[i];
          		//objUpdAj1.Applicant_Comments__c = 'Test2131'+i;
          		objUpdAj1.Needed_BRS_Manager_Collaboration__c = 'No';
          		objUpdAj1.Needed_ROP_Collaboration__c = 'No';          		
          		updAJ1.add(objUpdAj1);
          }
          update updAJ1;
          
          List<Authorization_Junction__c> updAJ2 = new List<Authorization_Junction__c>();
          for(Integer i=0;i<lstAJ2.size();i++){
          		Authorization_Junction__c objUpdAj2 = lstAJ2[i];
          		//objUpdAj1.Applicant_Comments__c = 'Test2131'+i;
          		objUpdAj2.Needed_BRS_Manager_Collaboration__c = 'Yes';
          		objUpdAj2.Needed_ROP_Collaboration__c = 'Yes';          		
          		updAJ2.add(objUpdAj2);
          }
          update updAJ2;
          
          extclass.tosave();
          extclass.submit();          
          system.assert(extclass != null);
          Test.stopTest();    

        
    }
}