@isTest
private class EFLSelfReportingWizardController_Test {    
     
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    static Application__c app;
    static Authorizations__c auth;
    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static AC__c li;
    static Applicant_Attachments__c objaa;
    static Attachment objattach;
    static Id RtId;
    static Location__c loc;
    static Construct__c construct; 
    static Report_Summary__c rs;
    static Self_Reporting__c sr;
    
    @IsTest
    public static void init(){
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
      
        app = new Application__c();
        app = testData.newapplication();
        auth = new Authorizations__c();
        auth = testDataBRS.newAuth(app.id);
        country = testDataBRS.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        li = new AC__c();
        li = testData.newLineItem('Resale/Adoption', app, auth);

          
        RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId(); 
      	Id preplantrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Pre-Planting/Pre-Release Notices').getRecordTypeId();
        
        loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
  
        rs = testDataBRS.newReportSummayByTypeStatus(auth.id, CARPOL_Constants.PRE_PLANTING_NOTICE, 'UnSubmitted');      
 		           
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()-1;
        sr.Monitoring_Period_End__c = date.today();
        sr.Observation_Date__c = date.today();
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Authorization__c = auth.id;
        sr.Final_Volunteer_Monitoring_Report__c = 'No';
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.RecordTypeId = preplantrectypeid;
        sr.Comments__c = 'Test description';
        sr.Quantity_Acres__c = 1;
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
   		sr.Explanation__c = 'test';
      	sr.Is_No_Planting__c = true;
        sr.No_Monitoring_Report__c = true;
        sr.Is_Submitted__c = true;
        sr.Start_Date__c= date.today();
    
        insert sr;
        
      }
      
    static testMethod void testCleaning() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
            
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        Id noticerectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get(CARPOL_Constants.CLEANING_NOTICE).getRecordTypeId();
        test.startTest();
        
        system.runAs(userShare)
        {
            
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            auth = new Authorizations__c();
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            Location__c loc= new Location__c();
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            loc = testdata.newlocationByLineItemAndStatus(lineItem.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Waiting on Customer');
            
            Report_Summary__c preplantingRS = testData.newReportSummayByTypeStatus(auth.id, CARPOL_Constants.CLEANING_NOTICE, 'UnSubmitted');
    		sr= new Self_Reporting__c();
            sr.Monitoring_Period_Start__c = date.today()-1;
            sr.Monitoring_Period_End__c = date.today();
            sr.Observation_Date__c = date.today();
            sr.Number_of_Volunteers__c = 5;
            sr.Release_Record_ID__c = loc.id;
            sr.Planting_ID__c=loc.id;
            sr.Authorization__c = auth.id;
            sr.Final_Volunteer_Monitoring_Report__c = 'No';
            sr.Action_Taken__c = 'Test description';
            sr.report_summary__c = preplantingRS.id;
            sr.Release_Record_ID__c = loc.id;
            sr.Planting_ID__c=loc.id;
            sr.RecordTypeId = noticerectypeid;
            sr.Description__c = 'Test description';
            sr.Equipment__c = 'Both';
            sr.Comments__c = 'Test description';
            sr.Quantity_Acres__c = 1;
            sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
            sr.Explanation__c = 'test';
            sr.Is_No_Planting__c = true;
            sr.No_Monitoring_Report__c = true;
            sr.Is_Submitted__c = true;
            sr.Start_Date__c= date.today();
    
        	insert sr;     
            
            PageReference pageRef = Page.EFLSelfReportingWizard;
            pageRef.getParameters().put('authId', String.valueOf(auth.Id));
            pageRef.getParameters().put('reportType', 'cleaning_notice');
            Test.setCurrentPage(pageRef);  
            
            ApexPages.StandardController sc = new ApexPages.StandardController(new Self_Reporting__c());
            EFLSelfReportingWizardController notice = new EFLSelfReportingWizardController(sc);
            notice.addform();
            notice.selfReportId = sr.Id;
            try{notice.save();}catch(exception ex){}
            notice.selfReportId = sr.id;
      
            try{notice.editForm();}catch(exception ex){}
            notice.cancel();
            notice.fireDocLauncher();
			string applicantName = notice.applicantName;
            Authorizations__c authObj = notice.authObj;
            string doesAppContainCBI = notice.doesAppContainCBI;
        }
        
        
        
        test.stopTest();
        
    }
      
       @IsTest 
      static void testPrePlanting() {
      
          init();
          ApexPages.StandardController sc = new ApexPages.StandardController(sr);
          EFLSelfReportingWizardController cont = new EFLSelfReportingWizardController(sc);
          
          cont.searchString = '';
          cont.selfReporting = new Self_Reporting__c();
          cont.rs = new Report_Summary__c();
          cont.repSummName = '';
          cont.authorizationID = auth.id;
          cont.PreHarvestview=false;
          cont.Cleaningview =false;
          cont.PrePlantingview=true;
          cont.reportType = 'Pre-Planting/Pre-Release Notices';
          cont.submitBool = false;
          cont.locationKeyPrefix = '';
          cont.rsIsSubmitted = false;
      
          test.startTest();
          ApexPages.StandardSetController scSet = cont.con;
     	
          PageReference pageRef = Page.EFLSelfReportingWizard;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('authId', auth.id);         
          cont.authorizationID = auth.id;
          cont.selfReporting = sr;
          cont.authObj = auth;
          Report_Summary__c repSum = cont.createReportSummary();
          ApexPages.currentPage().getParameters().put('rsId', rs.id);
          ApexPages.currentPage().getParameters().put('reportType', 'pre_planting_notice');
          List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
          srList.add(sr);
          cont.selfReportingList=srList;
          cont.searchLocations();
          cont.searchString='test';
          cont.searchLocations();
          List<Self_Reporting__c> srList1 = cont.getselfRepList();
          cont.rs = rs;
          cont.submitBool = false;
          cont.callToggleCheckBox();
          cont.submitBool = true;
          cont.callToggleCheckBox();
          cont.selfReportId = sr.Id;
          
          Self_Reporting__c objSelfReportCopy = new Self_Reporting__c();
          objSelfReportCopy = sr.clone(false, true, false, false);
          insert objSelfReportCopy;
     	  srList.add(objSelfReportCopy);
          cont.selfReportingList=srList;
          cont.reportType = 'Pre-Planting/Pre-Release Notices';
          List<Report_Summary__c> lstRS = new List<Report_Summary__c>();
          lstRS.add(rs);
          cont.addform();
          cont.editform();
          pageRef = cont.Save();
          
          cont.cancel();
          string applicantName = cont.applicantName;
          Authorizations__c authObj = cont.authObj;
          string doesAppContainCBI = cont.doesAppContainCBI;
          boolean rsIsSubmitted = cont.rsIsSubmitted;
          string repSummName= cont.repSummName;
          pageRef = cont.refreshPageSize();          
          pageRef = cont.doSubmit();  
          cont.rsIsSubmitted = true;
          cont.selfReporting = objSelfReportCopy;
          cont.deleterSsR();
          
     //	 cont.fireDocLauncher();
          
          test.stopTest();  
    }
    
    @isTest
    public static void test1(){
        
        init();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(sr);
        EFLSelfReportingWizardController cont = new EFLSelfReportingWizardController(sc);
  		
    }

}