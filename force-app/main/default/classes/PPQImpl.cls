/**
* Purpose : This is used to display CBI data and CBI deleted data Permit PDF 
Related Class :- CARPOL_UNI_CreateReviewer_Email
* 
*/
public class PPQImpl implements CBIService {
    

    public AC__c getCBIData(AC__c lineItemObj) {
        
        Map<String, String> customSettingfieldMap = new Map<String, String>();        
        Map<String, PPQCBIFields__c> csPPQMap = PPQCBIFields__c.getAll();
        Map<String, PPQCBIFields__c> csPPQ = new Map<String, PPQCBIFields__c>();
       
        for (String s: csPPQMap.keySet()) {
            csPPQ.put(csPPQMap.get(s).FieldApi__c, csPPQMap.get(s));
        }
        
        //Commented out Patter Matcher for Enclosed CBI data in [] 
        /* Pattern p=Pattern.compile('\\['+'[^\\[]*'+'\\]');
            Matcher pm=p.matcher(JSON.serialize(lineItemObj));
            String MatchTxt=pm.replaceAll('[ ]');
            lineItemObj=(AC__c)JSON.deserialize(MatchTxt,AC__c.class);*/
       
        Schema.SObjectType lineItemSchema = Schema.getGlobalDescribe().get('AC__c');
        Map<String, Schema.SObjectField> lineItemFieldMap = lineItemSchema.getDescribe().fields.getMap();
        
        String RelationshipName = null;
                     
        for (Schema.SObjectField fieldName: lineItemFieldMap.values()) {
            
            Schema.DescribeFieldResult DescField = fieldName.getDescribe();
            
            if (csPPQ.get(DescField.getName()) == null)
                continue;
            if (csPPQ.get(DescField.getName()).FieldApiCBI__c != null &&
                csPPQ.get(DescField.getName()).FieldApiCBI__c != '' &&
                lineItemObj.get(csPPQ.get(DescField.getName()).FieldApiCBI__c) == true) {                    
                            
                if (String.ValueOf(fieldName.getDescribe().getType()) == 'DOUBLE') {
                    lineItemObj.put(csPPQ.get(DescField.getName()).FieldApi__c, null);  
                    
                    }
                else if (String.ValueOf(fieldName.getDescribe().getType()) == 'REFERENCE'){
                         
                    if (lineItemObj.get(DescField.getName()) != null) {
                        
                       RelationshipName = null; 
                       for(Schema.SObjectType reference : DescField.getReferenceTo()) {                           
                           RelationshipName = reference.getDescribe().getName();                           
                       }
                        
                       // Create Type of Relation object and Assign as Sobject
                       
                       Type customType = Type.forName(RelationShipName);                       
                       Sobject newSobject = (Sobject)customType.newInstance();                        
                       newSobject.put('Name', '['+lineItemObj.get(csPPQ.get(DescField.getName()).FieldApi__c)+']'); // Change Name to "[ ]"
                                               
                       lineItemObj.putSObject(DescField.getRelationshipName(), newSobject); 

                    }
                    
                } else if (String.ValueOf(fieldName.getDescribe().getType()) == 'DATE') {
                    lineItemObj.put(csPPQ.get(DescField.getName()).FieldApi__c, null);
                } else {
                    
                    lineItemObj.put(csPPQ.get(DescField.getName()).FieldApi__c, '['+lineItemObj.get(csPPQ.get(DescField.getName()).FieldApi__c)+']' );                   
                                    
                }             
            }
        }
        return lineItemObj;                   
    } 
 
    public AC__c getCBIDeletedData(AC__c lineItemObj) {
        
        Map<String, String> customSettingfieldMap = new Map<String, String>();        
        Map<String, PPQCBIFields__c> csPPQMap = PPQCBIFields__c.getAll();
        Map<String, PPQCBIFields__c> csPPQ = new Map<String, PPQCBIFields__c>();
        
        for (String s: csPPQMap.keySet()) {
            
            csPPQ.put(csPPQMap.get(s).FieldApi__c, csPPQMap.get(s));
        }
            Pattern p=Pattern.compile('\\['+'[^\\[]*'+'\\]');
            Matcher pm=p.matcher(JSON.serialize(lineItemObj));
            String MatchTxt=pm.replaceAll('[ ]');
            lineItemObj=(AC__c)JSON.deserialize(MatchTxt,AC__c.class);
        
            Schema.SObjectType lineItemSchema = Schema.getGlobalDescribe().get('AC__c');
            Map<String, Schema.SObjectField> lineItemFieldMap = lineItemSchema.getDescribe().fields.getMap();
        
            String RelationshipName = null;
                      
        for (Schema.SObjectField fieldName: lineItemFieldMap.values()) {
            
            Schema.DescribeFieldResult DescField = fieldName.getDescribe();
            
            if (csPPQ.get(DescField.getName()) == null)
                continue;
            
            if (csPPQ.get(DescField.getName()).FieldApiCBI__c != null &&
                csPPQ.get(DescField.getName()).FieldApiCBI__c != '' &&
                lineItemObj.get(csPPQ.get(DescField.getName()).FieldApiCBI__c) == true) 
            {
               if (String.ValueOf(fieldName.getDescribe().getType()) == 'DOUBLE') 
               {
                 lineItemObj.put(csPPQ.get(DescField.getName()).FieldApi__c, null);  
                    
                } else if (String.ValueOf(fieldName.getDescribe().getType()) == 'REFERENCE') 
                     {
                       if (lineItemObj.get(DescField.getName()) != null)
                       {
                            RelationshipName = null; 
                            for(Schema.SObjectType reference : DescField.getReferenceTo()) {                           
                            RelationshipName = reference.getDescribe().getName();                           
                       }
                           
                       // Create Type of Relation object and Assign as Sobject
                       
                       Type customType = Type.forName(RelationShipName);                       
                       Sobject newSobject = (Sobject)customType.newInstance();
                       newSobject.put('Name', '[ ]');   // Change Name to "[ ]"
                           
                       lineItemObj.putSObject(DescField.getRelationshipName(), newSobject); 

                    }
                    
                } 
                else if (String.ValueOf(fieldName.getDescribe().getType()) == 'DATE') 
                {
                    lineItemObj.put(csPPQ.get(DescField.getName()).FieldApi__c, null);
                } else 
                {
                    lineItemObj.put(csPPQ.get(DescField.getName()).FieldApi__c, '[ ]');                   
                }             
            }
        }
        return lineItemObj;                   
    }
}