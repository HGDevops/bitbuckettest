/**
* Author :  
* Created Date :  
* Purpose : This is used to review state review package
* Related Pages: CARPOL_BRS_StateReviewEditRecords
*/
public with sharing class CARPOL_BRS_StateReviewEditRecords{
    
/**
*  Properties
**/    
	public Id reviewerId{get;set;}
	public Reviewer__c Reviewer{get;set;}
	public Reviewer__c obj{get;set;}
	public boolean messageVisible{get;set;}
	public Id AuthId {get;set;}
	public id stateletterrecid{get;set;}
	public string redirect{get;set;}
	public List<Applicant_Attachments__c> lststaterev{get;set;}
	public List<Applicant_Attachments__c> lstappattachment{get;set;}
	public ApexPages.StandardController stdController;
	public string delrecid{get;set;}
	public Map<string,string> sobjectkeys{get;set;}
    public boolean showFSIS{get;set;}
	public Id LineItemId{get;set;}

/**
* Standard Controller Constructor  
**/	
	public CARPOL_BRS_StateReviewEditRecords(ApexPages.StandardController controller){
		reviewerId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
		obj=(Reviewer__c)controller.getRecord();
		stateletterrecid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
		lststaterev=new  List<Applicant_Attachments__c>();
		lstappattachment=new  List<Applicant_Attachments__c>();
		lststaterev=appattach(stateletterrecid);
		  sobjectkeys=new  Map<string,string>();
	 	stdController=controller;
		messageVisible=false;
        String FSISrecordtypeid = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('FSIS Review Record').getRecordTypeId();
        showFSIS = false;		
		Reviewer=[SELECT Id,Name,State_Regulatory_Official__c,
		        BRS_State_Reviewer_Email__c,State__c,FSIS_Reviewer__c,recordtypeid,Authorization__r.name,Authorization__c,Status__c,State_Comments__c,state_has_no_comments__c,Requirement_Desc__c,Review_Complete__c,State_has_comments__c,No_Requirements__c,State_not_Responded__c,Notes_to_State__c,CreatedByID,LastModifiedDate,CreatedDate,LastModifiedByID FROM Reviewer__c WHERE ID=:reviewerId];
		AuthId = Reviewer.Authorization__c;
       if(Reviewer.recordtypeid == FSISrecordtypeid) {
         showFSIS = true; 
      }  
 	}
 	 public List<Applicant_Attachments__c> appattach(id rectypeid){
        lstappattachment=[SELECT ID,Name,Animal_Care_AC__c,Attachment_Type__c,RecordType.Name,Document_Types__c,File_Name__c,Status__c,Official_Review_Record__r.Name,Status_Graphical__c,Total_Files__c,CBI_Attachment__c,Applicant_Instructions__c,(SELECT ID,Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Official_Review_Record__c=:reviewerId AND recordTypeId=:rectypeid];
        return lstappattachment;
 	 }
/**
* save method 
**/	 	
	public PageReference saveAndCongrat(){
           if(obj.No_Requirements__c== true && (obj.Requirement_Desc__c!= '' && obj.Requirement_Desc__c!= null)){
                  ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'State should not have any Requirements entered, Please empty State Requirements.');
                  ApexPages.addMessage(message); 
                   return null; 
                   }
           if(obj.State_has_no_comments__c== true && (obj.State_Comments__c!= '' && obj.State_Comments__c!= null)){
                  ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'State should not have any comments entered, Please empty State Comments.');
                  ApexPages.addMessage(message); 
                   return null; 
                   }                   
 			reviewerId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
			obj.id=reviewerId;
 			Update obj;
 			PageReference congratsPage=Page.CARPOL_BRS_StateReviewRecords;
			congratsPage.getParameters().put('id',obj.id);
			congratsPage.setRedirect(true);
			messageVisible=false;
			return congratsPage;
 	}
 	 public PageReference deleteRecord(){
        //System.debug('***delRecId:' + delrecid);
        string sobjkey=delrecid.substring(0,3);
        string sobjname=sobjectkeys.get(sobjkey);
        //System.debug('***sobjname:' + sobjname);
        string strqurey='select id from '+sobjname+' where id=: delrecid';
        list<sobject> lst=database.query(strqurey);
        Delete lst;
        PageReference dirpage=new  PageReference('/apex/CARPOL_BRS_StateReviewEditRecords?Id='+reviewerId);
        dirpage.setRedirect(true);
        return dirpage;
  }
  
/** 
 * Authorization Related Federal Conditions
**/     
      public List<Authorization_Junction__c> getRelatedConditions(){
         List<Authorization_Junction__c> authRelatedConditions = new List<Authorization_Junction__c>();
         if(authId !=null){
            authRelatedConditions= EFLGetConditions.getAuthorizationRelatedConditions(AuthId,'Federal Conditions',null);
            return authRelatedConditions;
          }
        else{
           return null; 
        }  
    } 
 	
/** 
 * Authorization Related State Requirements
**/     
      public List<Authorization_Junction__c> getRelatedStateRequirements(){
         List<Authorization_Junction__c> authRelatedConditions = new List<Authorization_Junction__c>();
         if(authId !=null){
            authRelatedConditions= EFLGetConditions.getAuthorizationRelatedConditions(AuthId,'State Conditions',null);
            return authRelatedConditions;
          }
        else{
           return null; 
        }  
    }  
}