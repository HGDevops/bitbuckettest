public with sharing class EFLCustomLookUpController {
    
    @AuraEnabled public static pageData initController(string usageKey){
        // get the search fields & where fields & config details from custom MDT's.
        EFL_Custom_Lookup_Instance__mdt instance = [SELECT Id, Service_Class_Extension__c, Number_of_Results_to_Display__c, sObject_to_Search__c,
                                                    Order_By__c, Query_Filter__c, 
                                                    (SELECT Field_API_Name__c, Include_Field_As__c 
                                                     FROM EFL_Custom_Lookup_Fields__r)
                                                    FROM EFL_Custom_Lookup_Instance__mdt
                                                    WHERE Usage_Key__c = :usageKey][0];
        return new PageData(instance);
    }
    
    @AuraEnabled
    public static List<LookupItem> fetchLookUpValues(String searchKeyWord, String ObjectName, string pageData) {
        string serviceProvider = 'EFLCustomLookupUtil'; // this is base class as fallback
        PageData pd;
        if(string.isNotBlank(pageData)){
            pd = (PageData)JSON.deserialize(pageData,PageData.class);
            if(string.isNotBlank(pd.utilClassName)){
                serviceProvider = pd.utilClassName;
            }
        }
        Type utilType = Type.forName(serviceProvider);
        EFLCustomLookupUtil util = (EFLCustomLookupUtil)utilType.newInstance();
        util.setParams(pd);
        return util.queryData(searchKeyword);
    }
    
    // LookupItem is an instance of a search result
    public class LookupItem{
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string value{get;set;}
        @AuraEnabled public string helpText{get;set;}
        public lookupItem(string labl, string val, string help){
            this.value = val; // typically the Id
            this.label = labl; // typically the name
            this.helpText = help; // concatenated elsewhere.
        }
    }
    
    // PageData class is used to supply the configuration to the page.  It's passed back
    // later so that it only needs to be compiled once on page load.
    public class PageData{
        @AuraEnabled public string utilClassName{get;set;}
        @AuraEnabled public integer resultListSize{get;set;}
        @AuraEnabled public set<string> whereFields{get;set;}
        @AuraEnabled public string sObjectName{get;set;}
        @AuraEnabled public string sortOrder{get;set;}
        @AuraEnabled public string queryFilter{get;set;}
        public pageData(EFL_Custom_Lookup_Instance__mdt cli){
            this.sObjectName = cli.sObject_to_Search__c;
            this.utilClassName = cli.Service_Class_Extension__c;
            this.resultListSize = integer.valueOf(cli.Number_of_Results_to_Display__c);
            this.whereFields = new set<string>();
            this.sortOrder = cli.Order_By__c;
            this.queryFilter = cli.Query_Filter__c;
            for(EFL_Custom_Lookup_Field__mdt f : cli.EFL_Custom_Lookup_Fields__r){
                this.whereFields.add(f.Field_API_Name__c);
            }
        }
    }
}