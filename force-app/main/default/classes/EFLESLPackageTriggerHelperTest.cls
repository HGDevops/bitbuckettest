@isTest(SeeAllData = True)
public class EFLESLPackageTriggerHelperTest {
    static testMethod void validateIncident(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Application__c objapp = testData.newapplication();
        AC__c li = TestData.newlineitem('Personal Use', objapp);
        Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        Authorizations__c objauth = testData.newAuth(objapp.id);
        li.Authorization__c = objauth.id;
        update li;
        
        update objauth;
        Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
        
        
        //Create Facility
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Facility__c fac = new Facility__c();
        fac.RecordTypeId = PortsFacRecordTypeId;
        fac.Name = 'entryport';
        fac.Type__c = 'Laboratory';  
        insert fac;
        
        
        //Incident__c
        Incident__c inc = new Incident__c();
        inc.Requester_Name__c='testReqName';
        inc.EFL_Template__c=objcm.id;
        inc.Facility__c =fac.id;
        inc.Authorization__c= objauth.id;
        inc.EFL_Issued_Date__c = Date.today();
        inc.Related_Program__c = 'AC';
        inc.Requester_email__c='testReqName@test.com';
        inc.Incident_Date__c = Date.today();
        inc.Incident_Discovery_Date__c = Date.today();
        inc.Incident_Reported_Date__c = Date.today();
        //insert incident;
        //Incident__c inc = new Incident__c();
        insert inc;
        
        system.debug('inc === ' + inc);
        ESL__Package__c eslPackage = new ESL__Package__c(Incident_Signature__c = inc.Id, ESL__Parent_Id__c = inc.Id); 
        eslPackage.ESL__Status__c = 'Draft';
        insert eslPackage;  

        List<ESL__Package_Signer__c> eslSigner = [select id, ESL__Number_of_signature_completed__c, ESL__Number_of_signature_required__c 
        from ESL__Package_Signer__c where ESL__Package__c =: eslPackage.Id];
        system.debug('eslSigner1 === ' + eslSigner[0].ESL__Number_of_signature_completed__c);
        system.debug('eslSigner2 === ' + eslSigner[0].ESL__Number_of_signature_required__c);
        
        
        ESL__Package_Document__c pkgDoc = new ESL__Package_Document__c (esl__package__c = eslPackage.Id);
        insert pkgDoc;
        
        Attachment attach=new Attachment();     
       
        attach.Name= '_esigned.pdf';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=pkgDoc.Id;        
        
        List<ESL__Package_Signer__c> eslSignerUpdateList = new List<ESL__Package_Signer__c>();
        for(ESL__Package_Signer__c eslp: eslSigner){
            //eslp.ESL__Number_of_signature_completed__c = eslp.ESL__Number_of_signature_required__c;
            eslp.ESL__Number_of_signature_completed__c = 1;
            eslp.ESL__Number_of_signature_required__c = 1;
            eslSignerUpdateList.add(eslp);
        }
        Test.startTest();
        update eslSignerUpdateList;
        
        eslPackage.ESL__Status__c= 'All signers have signed';
        insert attach;
        pkgDoc.esl__sourceId__c= attach.Id;
        update pkgDoc;
        
        update eslPackage;
        Test.stopTest();

        ESL__Package__c eslPackage1 = [select ESL__Signer_Status__c, Name from ESL__Package__c where Id =: eslPackage.Id];
        
        Attachment att = [SELECT Id, parentId FROM Attachment WHERE parentId =: inc.Id Limit 1];
        
        system.assert(att.parentId == inc.Id);
                      
    }
    
    static testMethod void validateAttAssociationTOAuth(){
        
        Test.startTest();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        Application__c appl = new Application__c();
        insert appl;
        Authorizations__c authHandler = new Authorizations__c(Application__c = appl.Id);
        authHandler.AC_Applicant_Email__c = 'testapplicant@aphis.gov';
        authHandler.RecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
        authHandler.permit_number__c = 'APermit123';
        authHandler.BRS_Proposed_Start_Date__c = date.today()+30;
        authHandler.BRS_Proposed_End_Date__c = date.today()+40;
        authHandler.Effective_Date__c= date.today();
        authHandler.Status__c='Submitted';
        authHandler.Date_Issued__c=date.today();
        authHandler.UNI_Zip__c='32092';
        authHandler.UNI_Country__c='United States';
        authHandler.UNI_State__c = 'TEST';
        authHandler.UNI_County_Province__c='Duval';
        authHandler.CBI__c='CBI Text';
        authHandler.Application_CBI__c='No';
        authHandler.Applicant_Alternate_Email__c='email2@test.com';
        authHandler.UNI_Alternate_Phone__c='(904) 123-2345';
        authHandler.AC_Applicant_Email__c='email2@test.com';
        authHandler.AC_Applicant_Fax__c='(904) 123-2345';
        authHandler.AC_Applicant_Phone__c='(904) 123-2345';
        authHandler.AC_Organization__c='ABC Corp';
        authHandler.Means_of_Movement__c='Hand Carry';
        authHandler.Biological_Material_present_in_Article__c='No';
        authHandler.If_Yes_Please_Describe__c='Text';
        authHandler.Applicant_Instructions__c='Make corrections';
        authHandler.BRS_Number_of_Labels__c=10;
        authHandler.BRS_Purpose_of_Permit__c='Importation';
        authHandler.Documents_Sent_to_Applicant__c=false;
        authHandler.Expiration_Timeframe_Override__c = 'Manual';
        authHandler.Thumbprint__c = testData.newThumbprint().id;
        authHandler.stage__c='Permit Package';
        authHandler.workflow_Number__c='150';
        authHandler.Authorization_Type__c='Permit';
        insert authHandler;
        
        //ESL__Package__c eslPackage = [select Id, ESL__Parent_Id__c from ESL__Package__c where ESL__Signer_Status__c LIKE '%not yet signed.' AND ESL__Sent__c = true LIMIT 1];
        ESL__Package__c eslPackage = new ESL__Package__c(eFile_Signature__c = authHandler.Id, ESL__Parent_Id__c = authHandler.Id); 
        eslPackage.ESL__Status__c = 'Draft';
        insert eslPackage;
        
        
        List<ESL__Package_Signer__c> eslSigner = [select id, ESL__Number_of_signature_completed__c, ESL__Number_of_signature_required__c 
        from ESL__Package_Signer__c where ESL__Package__c =: eslPackage.Id];
        
        
        ESL__Package_Document__c pkgDoc = new ESL__Package_Document__c (esl__package__c = eslPackage.Id);
        insert pkgDoc;
        
        Attachment attach=new Attachment();     
       
        attach.Name= '_esigned.pdf';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=pkgDoc.Id;        
        
        List<ESL__Package_Signer__c> eslSignerUpdateList = new List<ESL__Package_Signer__c>();
        for(ESL__Package_Signer__c eslp: eslSigner){
            eslp.ESL__Number_of_signature_completed__c = eslp.ESL__Number_of_signature_required__c;
            eslSignerUpdateList.add(eslp);
        }
        update eslSignerUpdateList;
        
        eslPackage.ESL__Status__c= 'All signers have signed';
        insert attach;
        pkgDoc.esl__sourceId__c= attach.Id;
        update pkgDoc;
        
        update eslPackage;
        
        ESL__Package__c eslPackage1 = [select ESL__Signer_Status__c from ESL__Package__c where Id =: eslPackage.Id];
        Attachment att = [SELECT Id, parentId FROM Attachment WHERE Name like '%AUTH - %' AND parentId =: authHandler.Id Limit 1];
        system.assert(att.parentId == authHandler.Id);
        Test.stopTest();
    }
    
}