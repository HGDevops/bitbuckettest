public with sharing class EFLBarcodeDataController {
    
    public string barcodeText{get;set;}
    public string barcodeHTML;
    public string barcodeType{get;set;}
    
    
    public String getBarcodeHTML()
    {        
        barcodeHTML = EFLBarcodeGeneratorFactory.getBarcode(barcodeText, barcodeType );
        return barcodeHTML;
    }
    
    
}