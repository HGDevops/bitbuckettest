@isTest
public class IncidentStageProgressBarControllerTest {
    private static testMethod void test_incident_Ctrl() {
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today();
        insert objInc;
  
        ApexPages.CurrentPage().getparameters().put('id', objInc.id);             
        Apexpages.StandardController sc = new Apexpages.StandardController(objInc);
        IncidentStageProgressBarController ext = new IncidentStageProgressBarController(sc); 
         
        ext.moveToFirstStage();
        ext.moveToNextStage();
        ext.updateStage('Incident Review');
            
        }
    private static testMethod void test_incident_Ctr2() {
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Workflow_Number__c = null;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today();
        insert objInc;
  
        ApexPages.CurrentPage().getparameters().put('id', objInc.id);             
        Apexpages.StandardController sc = new Apexpages.StandardController(objInc);
        IncidentStageProgressBarController ext = new IncidentStageProgressBarController(sc); 
         
        ext.moveToFirstStage();
        ext.moveToNextStage();
        ext.updateStage('Incident Review');
            
        }
}