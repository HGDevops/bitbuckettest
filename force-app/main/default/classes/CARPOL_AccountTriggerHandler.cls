public inherited sharing class CARPOL_AccountTriggerHandler {
    
    public static void afterUpdateHandler(List<Account> accounts){
        
        List<Group> groups = new List<Group>();
        
        // adding logic to get SOQL out of for-loop.
        // Changes by JB
        set<string> names = new set<string>();
        for(Account a:accounts){
            names.add(a.Name);
        }
        
        set<string> groupNames = new set<string>();
        for(Group g : [SELECT Id, Name FROM Group WHERE Name IN :names]){
            groupNames.add(g.Name);
        }
        
        list<Group> newGroups = new list<Group>();
        for(Account a :accounts){
            if(!groupNames.contains(a.Name)){
                newGroups.add(
                    new Group(
                      Name = a.Name
                    )
                );
            }
        }
        
        if(!newGroups.isEmpty()){
            try{
                insert newGroups;
            }catch(Exception e){
               // EFLErrorLog.createErrorLog('CARPOL_AccountTriggerHandler', e);
            }
        }
        
        // Commenting out previous for-loop for historical preservation purposes.
        // Changes by JB
        /*for(Account a: accounts){
            if(a.IsPartner){
                //create a public group with the same account name
                Group publicGroup;
                try{
                    publicGroup = [select Id, Name from Group where name =: a.Name limit 1];
                }
                catch(Exception e){}
                if(publicGroup == null){
                    publicGroup = new Group(Name = a.Name);
                    groups.add(publicGroup);
                }
            }
        }
        if (groups != null && groups.size() > 0)
            insert groups;
        */
    }
}