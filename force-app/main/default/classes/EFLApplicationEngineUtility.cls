/*
* Purpose: Application Engine Specific Utility Methods
*/ 

public with sharing class EFLApplicationEngineUtility {
    
    //Constants
    static final string MULTIPLE='Multiple';
    static final string NONE='None';    
    
    /*
* Purpose: Load application wrapper
*/ 
    public static EFLApplicationWrapper createApplication(EFLPreScreeningWrapper psw){ 
        //Attributes
        string EXTERNALID =  EFLGenericUtility.randomString();
        list<sObject> applicationPSQDetails = new list<sObject>();
        EFLApplicationWrapper apw = new EFLApplicationWrapper();
        list<sObject> applWrap = new list<sObject>();
        
        //load application, line item and application request records and add to application wrapper
        apw.application = EFLApplicationEngineUtility.prepareApplication(EXTERNALID,psw);  
        apw.lineItem = EFLApplicationEngineUtility.prepareLineItem(EXTERNALID,psw); 
        apw.applicationRequestList.addAll(EFLApplicationEngineUtility.prepareApplicationRequest(EXTERNALID,psw,apw.lineItem)); 
        return apw;   
        
    }                
    /*
* Purpose: Load application 
*/     
    public static application__c prepareApplication(string EXTERNALID,EFLPreScreeningWrapper psw){ 
        Application__c application = new Application__c();
        application = prepareGenericApplication(application,psw.application.Applicant_Account__c);
        application.ExternalID__c = EXTERNALID;
        return application;
    } 
    
    /*
* Purpose: Generic application creation method that can be used from multiple sources 
*/     
    public static application__c prepareGenericApplication(Application__c application,Id applAcctId){ 
        application.RecordTypeID = EFLGenericUtility.getRecordTypeId('Application Standard Application');
        if (application.Applicant_Name__c == null){
            application.Applicant_Name__c = getApplicant(applAcctId);
        }        
        application.Authorized_User__c = application.Applicant_Name__c;
        application.Applicant_Account__c = applAcctId; 
        application.sharing_account__c = getSharingAccount(applAcctId);
        application.Application_Status__c = 'Open';
        return application;
    } 
    
    /*
* Get Applicant's Contact ID
*/     
    private static ID getApplicant(ID accountID){
        ID applicantContactID;
        if(userinfo.getUserType() == 'Standard'){ //internal preparer:- assumption is they can only select applicant personal accounts
            applicantContactID = EFLContactUtility.getPersonalContactId(accountID);
        } else { //external user:- Get their own contact info
            applicantContactID = EFLUserUtility.userDetails.Contact.Id;
        }
        return applicantContactID;
        
    } 
    
    /*
* get Sharing Account
*/
    public static ID getSharingAccount(Id accountID){
        Id personalAccountId; 
        if(userinfo.getUserType() != 'Standard'){
            //For all Org Applicants or Org Admins or Users who deal with Business Accounts
            for (AccountContactRelation acr: EFLContactUtility.selectContactAccountRelationsByContactId(EFLUserUtility.userDetails.Contact.Id).AccountContactRelations){
                if (acr.Personal__c){//find ACR flagged as personal
                    personalAccountId = acr.AccountId;
                    break;   
                }
            }
        } 
        //For all Normal Applicants and Standard User(Internal User) will be populated with selected account 
        if(personalAccountId ==NULL){
            personalAccountId = accountID;// Selected account on PSQ
        }
        return personalAccountId;
    }
    
    /*
* get Primary Contact
*/
    private static ID getPrimaryContact(ID AccountID){
        accountcontactrelation acr;
        try{
            acr = [select contactid 
                   from accountcontactrelation 
                   where accountId = :AccountID 
                   and IsDirect = TRUE
                   limit 1];
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));  
        }
        
        return acr.ContactId;
    }      
    
    
    /*
* Purpose: Load line Item 
*/    
    public static AC__c prepareLineItem(string EXTERNALID, EFLPreScreeningWrapper psw){
        
        application__c applicationReference = new application__c(ExternalID__c=EXTERNALID);
        integer noOfQuestions = psw.QuestionList.size();
        AC__c lineItem = new AC__c();
        lineItem.Application_Number__r = applicationReference;
        lineItem.recordTypeID = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get(psw.QuestionList[noOfQuestions - 1].psquestion.Signature__r.Line_Record_Type__c).getRecordTypeId();
        lineItem.Locked__c ='No'; 
        lineItem.Signature__c = psw.QuestionList[noOfQuestions - 1].psquestion.Signature__c;
        lineItem.Thumbprint__c = psw.QuestionList[noOfQuestions - 1].psquestion.Signature__c;
        lineItem.Type_of_Permit__c = psw.QuestionList[noOfQuestions - 1].psquestion.Type_of_Permit__c;
        lineItem.Program_Line_Item_Pathway__c = psw.QuestionList[noOfQuestions - 1].psquestion.Program_Line_Item_Pathway__c;
        lineItem.ExternalID__c = EXTERNALID;
        lineitem.sharing_account__c = getSharingAccount(psw.application.Applicant_Account__c);
        return lineItem;
        
    }
    
    /*
* Purpose: Load Application Request
*/     
    public static list<Application_Request__c> prepareApplicationRequest(string EXTERNALID,EFLPreScreeningWrapper psw,AC__c lineItem){
        
        list<Application_Request__c> applicationRequestList = new list<Application_Request__c>();
        AC__c lineItemReference = new AC__c(ExternalID__c=EXTERNALID);
        integer i = 0;
        for(EFLPSQQuestion psqq : psw.QuestionList){
            Application_Request__c ar = new Application_Request__c();
            i = i + 1;
            ar.Question__c = psqq.psquestion.Question__c; 
            ar.Answer__c = psqq.psqSelectedOption.Value__c;
            if(psqq.psquestion.PSQActivity_Processor__c!=null)
            {
                ar.Answer__c = psqq.psqSelectedOption.Name;    
            }
            ar.Sequence_Number__c = i; 
            ar.Line_Item__r = lineItemReference;
            IF( psqq.psquestion.Application_Mapping_Index__c != null ){
                lineItem.put( getFieldAPIName(psqq.psquestion.Application_Mapping_Index__c),psqq.psqSelectedOption.Value__c);  
            }
            applicationRequestList.add(ar); 
        }
        
        return applicationRequestList;
    } 
    /*
* Purpose: Provide PSQ application mapping Field API Name
*/     
    public static string getFieldAPIName(string MasterLabel){
        return[select MasterLabel,
               Field_API_Name__c
               from EFL_PSQ_Application_Mapping__mdt
               where MasterLabel =: MasterLabel 
               limit 1].Field_API_Name__c;
    }
    
    
    /*
* Purpose: Update status for Withdraw action
*/ 
    public static void WithdrawStatusUpdate(set<ID> appids){
        try{
            if(!appIds.isEmpty()){
                list <authorizations__c> lstAuth = new list <authorizations__c>();
                list <AC__c > lstAC = new list <AC__c >();
                for( authorizations__c auth: [select id, status__c,Application__c  from Authorizations__c where Application__c in :appids])
                {
                    auth.status__c = 'Withdrawn';
                    lstAuth.add(auth);
                    //auths.add(auth.id);
                }
                
                // system.debug('---auths--- '+auths);
                for ( AC__c LineItem: [select id, status__c,Application_Number__c from AC__c where Application_Number__c in :appids])
                {
                    LineItem.status__c = 'Withdrawn';
                    lstAC.add(LineItem);
                }    
                if (!lstAC.isempty())
                {
                    update lstAC;
                }
                if (!lstAuth.isempty()){
                    update lstAuth;
                }   
            }         
        }catch(exception e){
            system.debug('Excetion occured in EFLApplicationEngineUtility'+e);
            
        }
    }
    
    /*
* Purpose: prepare the Regulated Article per line item 
*/ 
    public static map<id,string> getRegulatedArticlesPerLineItem(list<ac__c> lineItemList){
        map<id,string> lineRegulatedArticleMap = new  map<id,string>();
        //prepare the lineitem list with link regulated article details
        for(ac__c line: lineItemList){
            
            if(line.Program_Line_Item_Pathway__r.Program__r.Name == EFLGlobalConstants.getConstantValue('PROGRAM BRS')){
                if(line.Link_Regulated_Articles__r.size()==1){
                    lineRegulatedArticleMap.put(line.id,line.Link_Regulated_Articles__r[0].Regulated_Article__r.name);
                }else if(line.Link_Regulated_Articles__r.size()==0){
                    lineRegulatedArticleMap.put(line.id,NONE);
                }else if(line.Link_Regulated_Articles__r.size()>1){
                    lineRegulatedArticleMap.put(line.id,MULTIPLE);
                }
                // Default to Dogs for Animal Care program
            }else if(line.Program_Line_Item_Pathway__r.Program__r.Name == EFLGlobalConstants.getConstantValue('PROGRAM AC')){  
                lineRegulatedArticleMap.put(line.id,EFLGlobalConstants.getConstantValue('REGULATED ARTICLE DOGS'));
            }else{
                //for VS and PPQ programs regulated article is from line item object   
                lineRegulatedArticleMap.put(line.id,line.Scientific_Name_Text__c);
            }             
        }  
        return lineRegulatedArticleMap;
    }    
    
}