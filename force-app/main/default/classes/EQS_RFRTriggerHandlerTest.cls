/* ***************************************************************************************
-- Begin Default  ---
** Class Name     : EQS_TriggerHandlerTest 
** Description   : Test Class  
** Technical Info : Test class for the Helper EQS_RFRTriggerHandler 
** Author(s)     : Harish Gudipudi (HG)
**        Revision History:-
** Version  Date        Author  Description of Action
** 1.0      08/24/2018 HG      Initiated Script  
 
**************************************************************************************** **/


@isTest 
private class EQS_RFRTriggerHandlerTest {
    static testMethod void validateEQSRFRUpdate() {  
      
    Contact c = new Contact(FirstName='EQS Contact', LastName='Test');
    insert c;
    
    Case cs = new Case(ContactId=c.Id);
    insert cs;
    
    EQS_Incident_Position_Certification__c eos = new EQS_Incident_Position_Certification__c(EQS_Incident__c = cs.Id);
    insert eos;
    
    EQS_Resource_Request__c er = new EQS_Resource_Request__c (EQS_Responder__c=c.Id, EQS_Position__c =eos.Id, EQS_Rotation_Begin_Date__c=System.TODAY(), EQS_Type__c = 'Requested', EQS_Status__c = 'Final Approval');
    insert er;
    
    EQS_Resource_Request__c er1 = new EQS_Resource_Request__c (EQS_Responder__c=c.Id, EQS_Position__c =eos.id, EQS_Rotation_Begin_Date__c=System.TODAY(), EQS_Type__c = 'Volunteer', EQS_Status__c = 'Rejected');
    insert er1;
    
    EQS_Resource_Request__c er2 = new EQS_Resource_Request__c (EQS_Responder__c=c.Id, EQS_Position__c =eos.Id, EQS_Rotation_Begin_Date__c=System.TODAY(), EQS_Type__c = 'Requested', EQS_Status__c = 'Released by Dispatcher');
    insert er2;
    
    EQS_Resource_Request__c er3 = new EQS_Resource_Request__c (EQS_Responder__c=c.Id, EQS_Position__c =eos.id, EQS_Rotation_Begin_Date__c=System.TODAY(), EQS_Type__c = 'VEERC', EQS_Status__c = 'Pending - Volunteer Application Submission');
    insert er3;
        
    EQS_Incident_Position_Certification__c  epc = [SELECT Id, EQS_Approved__c, EQS_Rejected__c, EQS_Total_Evaluated__c, EQS_Requested__c, EQS_Volunteer__c, 
                                                  EQS_In_Progress__c, EQS_Released_By_Dispatcher__c FROM EQS_Incident_Position_Certification__c WHERE Id = : eos.id];
    
    // Assert Statements on Insert
    system.assertequals(3, epc.EQS_Total_Evaluated__c);
    system.assertequals(2, epc.EQS_Requested__c);
    system.assertequals(1, epc.EQS_Volunteer__c);
    system.assertequals(1, epc.EQS_Approved__c);
    
    er3.EQS_Type__c = 'Volunteer';
    er3.EQS_Status__c = 'Pending - Under Dispatcher Consideration';
    update er3;    
    delete er3;
    }
}