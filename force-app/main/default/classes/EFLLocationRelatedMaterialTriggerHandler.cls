public class EFLLocationRelatedMaterialTriggerHandler {
    
    //W-031437 - Required Field Validation Fix
    public static void validateFieldFormatAndRequired(List<Material__c> materials){     
        //System.debug('Inside the validate field format for EFLLocationRelatedMaterialTriggerHandler');        
        
        string requiredMessage = 'You must enter a value';
        
        for (Material__c mat : materials){
            
            //Required Field Validations
            if (mat.Quantity__c == null){
               mat.Quantity__c.addError(requiredMessage);
            }
            if (mat.Unit_of_Measure__c == null){
               mat.Unit_of_Measure__c.addError(requiredMessage);
            }
            if (mat.Material__c == null){
               mat.Material__c.addError(requiredMessage);
            }
            if (mat.Material__c == 'Other' && mat.Other_Material__c == null){
               mat.Other_Material__c.addError('Describe your Material Type');
            }
            
            //Format Validations
            if (mat.quantity__c != null  && mat.quantity__c <= 0){
                mat.quantity__c.addError('Enter a value greater than or equal to .001');
            }
            
            if (mat.Other_Material__c != null && (mat.Other_Material__c.indexOfChar(91) != -1 || mat.Other_Material__c.indexOfChar(93) != -1)){ //check for opening/closing bracket
                //System.debug('material other text contains CBI brackets');
                mat.Other_Material__c.addError('To claim materials as CBI, use checkbox labeled "Claim as CBI." Brackets are not allowed');
            }
                
         }
    }
}