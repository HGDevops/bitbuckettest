@isTest
private class EFLGenotypeTriggerHandler_Test {
    static testMethod void testEFLGenotypeTriggerHandler(){
        List<Genotype__c> genotypes = new List<Genotype__c>();
        set<ID> constructList = new set<ID>();
        GenotypeType__c genoTypeType = new GenotypeType__c();
        Genotype__c genoType;
        Genotype__c gt;
        
        //EFLConstructController conControl = new EFLConstructController();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c currentApp = testData.newapplication();
        Regulated_Article__c regArt = new  Regulated_Article__c();
        regArt.Name='Test article';
        regArt.RecordTypeID=Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        regArt.Status__c='Active';
        regArt.Category_Group_Ref__c = testData.newgroup().Id;
        insert regArt;
        RA_Scientific_Name_Junction__c raJunc = new  RA_Scientific_Name_Junction__c();
        raJunc.Regulated_Article__c = regArt.id;
        
        Authorizations__c auth = testData.newAuth(currentApp.Id);
        AC__c ac1 = testData.newLineItemBRS('Personal Use', currentApp);
        ac1.Type_of_Permit__c = 'Standard Permit';
        update ac1;
        Regulated_Article__c regArt2 = testData.newRegulatedArticle(auth.Program_Pathway__c);
       /* Link_Regulated_Articles__c linkRA = testData.newlinkRegArticle(ac1.id, currentApp.Id, auth.id);
        linkRA.Line_Item__c = ac1.id;
        linkRA.Regulated_Article__c = regArt2.id;
        upsert linkRA;
        system.debug('LinkRA set to: ' + linkRA);*/
        link_regulated_articles__c lra = new link_regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(ac1.id, regArt2.id, 'Draft');
        Construct__c cons1 = testData.newconstruct(ac1.id, regArt2);
        Construct__c cons2 = testData.newconstruct(ac1.id, regArt2);
        Phenotype__c phenoType = testData.newphenotype(cons1.id);
        genoTypeType.Construct__c = cons1.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;
        genoType = testData.newgenotype(cons1.id, genoTypeType);
        genotypes.add(genoType);
        
        constructList.add(cons1.id);
        constructList.add(cons2.id);
        
        Test.startTest();
           
        	EFLGenotypeTriggerHandler.ValidateRequiredFields(genotypes);
        	EFLGenotypeTriggerHandler.SortOrderEvaluationOnInsert(genotypes);
        	EFLGenotypeTriggerHandler.processGenotypeReadiness(genotypes);
        	EFLGenotypeTriggerHandler.resetGenotypeReadiness(genotypes);
        	EFLGenotypeTriggerHandler.updateConstructCBIflag(genotypes);
        	genoType = testData.newgenotype(cons1.id, genoTypeType);
        	genotypes.add(genoType);
        	
        	genoType = testData.newgenotype(cons1.id, genoTypeType);
        	try{delete genoType;}catch(exception ex){}
        	
        	EFLGenotypeTriggerHandler.DeleteParentAndChildOrder(genotypes);
			genoType = new Genotype__c();
       		try{insert genoType;}catch(exception ex){}
        	

        
        Test.stopTest();
    }
}