public with sharing class EFLConditionDescUpdateCtrl {
    
    public Authorization_Junction__c authjucObj  {get;set;}
    public String wfid    {get;set;}
    
    public EFLConditionDescUpdateCtrl(ApexPages.StandardController controller) {
        authjucObj = (Authorization_Junction__c)controller.getRecord();
        wfid = ApexPages.currentPage().getParameters().get('wfid');
    }
    
    public void saveDescription() {
        if(authjucObj.id != null)
            update authjucObj;
    }
    
}