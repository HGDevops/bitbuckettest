public inherited sharing class CARPOL_UNI_Auth_Junction_WF {
    
    public void createAuthorizationJunctionRecord(Map<Authorizations__c, List<AC__c>> authList) //n number of auths in authlist
    {
        if(authlist!=null && authlist.size()>0)
        {
            Set<Id> currentAuthorizationIdSet = new Set<Id>();
            List<Id> childApplicationIdList = new List<Id>();
            Map<Id,Authorizations__c> authCloneMap = new Map<Id,Authorizations__c>();
            Authorizations__c originalAuthorization = new Authorizations__c();
            Map<Id,AC__c> lineItemMap = new Map<Id,AC__c>();
            Map<Id, AC__c> lineItemAuthorizationMap = new Map<Id, AC__c>();
            List<Amendment_Renewal__c> ARParentLineItemList = new List<Amendment_Renewal__c>();
            Map<id,id> acAmendmentRenewalMap = new Map<id,id>();
            List<Amendment_Renewal__c> parentAuthorizationsList = new List<Amendment_Renewal__c>();
            List<Id> parentAuthorizationsIdList = new List<Id>();
            list < Workflow_Task__c > WFtasksinsert = new list < Workflow_Task__c > ();
            list < AC__c > lineItemList = new list < AC__c > ();
            list <AC__c> newLineItemList = new list <AC__c>();
            List < Authorizations__c > ProductAuths = new List < Authorizations__c > ();
            Map < String, Authorizations__c > tpauthmap = new Map < String, Authorizations__c > ();
            Set < ID > TPids = new Set < ID > ();
            Map < ID, Authorizations__c > originalAuthMap = new Map < ID, Authorizations__c > ();
            Map < ID, Authorizations__c > authidauth = new Map < ID, Authorizations__c > ();
            List < Authorizations__c > FacilityAuths = new List < Authorizations__c > ();
            List<Authorization_Junction__c> amendmentConditionsList = new List<Authorization_Junction__c>();
            List<Authorization_Junction__c> renewalConditionsList = new List<Authorization_Junction__c>();
            List<Authorization_Junction__c> newConditionsList = new List<Authorization_Junction__c>();
            String ResearchLocation ;
            Map < ID,string> pathways = new map<ID,string>();
            for (program_line_item_pathway__c ppty:[select id, name from  program_line_item_pathway__c]){
                pathways.put(ppty.id, ppty.name);                         
            }
            for(Authorizations__c auth1: authList.keyset()){
                currentAuthorizationIdSet.add(auth1.Id);
                authCloneMap.put(auth1.id, auth1);
                if(auth1.Application_Type__c == 'Amendment' || auth1.Application_Type__c == 'Renewal'){
                    childApplicationIdList.add(auth1.Application__c);
                }
            }
            if(childApplicationIdList.size() > 0){
                // //system.debug('Child Application ID List:  ' + childApplicationIdList);
                parentAuthorizationsList = [SELECT Id, Parent_Authorization__c FROM Amendment_Renewal__c 
                                            WHERE Child_Application__c in:childApplicationIdList];
                
                //system.debug('Child Auth ID List:  ' + currentAuthorizationIdSet);
                if (parentAuthorizationsList.size() > 0){
                    for(Amendment_Renewal__c auth : parentAuthorizationsList){
                        parentAuthorizationsIdList.add(auth.Parent_Authorization__c);
                    }
                    
                    ////system.debug('Parent Auth ID Set: ' + parentAuthorizationsIdList);
                    
                    amendmentConditionsList = [SELECT id, Applicant_Agree__c, Applicant_Comments__c, Authorization__c, BRS_Manager_Agree__c, BRS_Manager_Comments__c, Regulation__c,
                                               Regulation_Description__c, Regulation_Short_Name__c, Decision_Matrix__c, Is_Active__c, Method_of_Transportation__c, Mode_of_Transportation__c,
                                               Needed_BRS_Manager_Collaboration__c, Needed_ROP_Collaboration__c, Official_Review_Record__c, Order_Number__c, Port__c, Port_Name__c,
                                               Port_Name_text__c, ROP_Agree__c, ROP_Comments__c, Signature_Regulation_Junction_Name__c, Thumbprint__c, Title__c
                                               FROM Authorization_Junction__c 
                                               WHERE Authorization__c in: parentAuthorizationsIdList];
                    
                    renewalConditionsList = [SELECT id, Applicant_Comments__c, Authorization__c, BRS_Manager_Comments__c, Regulation__c,
                                             Regulation_Description__c, Regulation_Short_Name__c, Decision_Matrix__c, Is_Active__c, Method_of_Transportation__c, Mode_of_Transportation__c,
                                             Needed_BRS_Manager_Collaboration__c, Needed_ROP_Collaboration__c, Official_Review_Record__c, Order_Number__c, Port__c, Port_Name__c,
                                             Port_Name_text__c, ROP_Comments__c, Signature_Regulation_Junction_Name__c, Thumbprint__c, Title__c
                                             FROM Authorization_Junction__c 
                                             WHERE Authorization__c in: parentAuthorizationsIdList];
                    
                    originalAuthorization = [SELECT ID,Name,Permit_number__c,Expiration_Date__c,Program__c 
                                             FROM Authorizations__c 
                                             WHERE ID in: parentAuthorizationsIdList];
                    ////system.debug('Original Authorization: ' + originalAuthorization);
                    
                    ////system.debug('Conditions List: ' + conditionsList);
                }
                
                lineItemList = [SELECT ID,Name,Authorization__c,Research_Name__c FROM AC__c 
                                WHERE Authorization__c in: currentAuthorizationIdSet
                                OR Authorization__c in: parentAuthorizationsIdList];
                
                for(AC__c item : lineItemList){
                    lineItemMap.put(item.Authorization__c, item);
                    lineItemAuthorizationMap.put(item.id, item);
                }
                
                ARParentLineItemList = [SELECT Id, Parent_Line_Item__c, Child_Line_Item__c FROM Amendment_Renewal__c 
                                        WHERE Child_Line_Item__c in:lineItemList];
                
                for(Amendment_Renewal__c currentLineItem :ARParentLineItemList ){
                    //Map<Child_Line_Item__c, Amendment_Renewal__c.Parent_Line_Item__c>
                    acAmendmentRenewalMap.put(currentLineItem.Child_Line_Item__c, currentLineItem.Parent_Line_Item__c);
                }
                ////system.debug('Amendment Renewal Map: ' + acAmendmentRenewalMap);
            }
            list<Amendment_Renewal__c> newARs = new list<Amendment_Renewal__c>();
            for(Authorizations__c auth1: authList.keyset()) //3 
            {
                //system.debug('Application Type: ' + auth1.Application_Type__c);
                ////system.debug('Program Type: ' + auth1.Program__c);
                if(auth1.Application_Type__c != 'Amendment' && auth1.Application_Type__c != 'Renewal'){
                    ////system.debug('Inside Amendment Condtional!!!!!!');
                    if (auth1.Auth_Type_Hidden_Flag__c != 'product' && authlist.get(auth1) != null) {
                        newLineItemList.addall(authlist.get(auth1));
                    }
                    if ( pathways.get(auth1.program_pathway__c) == 'Veterinary Biological Products' && authlist.get(auth1) != null) { // VV added on 2-6-18 for W-017459 to resolve missing conditions
                        newLineItemList.addall(authlist.get(auth1));
                    }
                }
                
                if ((auth1.Auth_Type_Hidden_Flag__c == 'product' || auth1.Auth_Type_Hidden_Flag__c == 'ingredient')
                    && pathways.get(auth1.program_pathway__c) != 'Veterinary Biological Products') { // VV added on 2-6-18 for W-017459 to resolve missing conditions
                        ProductAuths.add(auth1);
                    }
                //check for nulls to avoid referencing null instead of string on field
                if (auth1.Auth_Type_Hidden_Flag__c != null) {
                    if (auth1.Auth_Type_Hidden_Flag__c.contains('Facility')) {
                        FacilityAuths.add(auth1);
                    }
                }
                
                
                if(auth1.Application_Type__c != null && auth1.Application_Type__c != '') {
                    if(auth1.Application_Type__c.contains('Renewal') || auth1.Application_Type__c.contains('Amendment')){
                        
                        //Commented out by Terry Hale 1/30/2019 W-025837 Query inside For Loop
                        //AC__c lineitem = new AC__c();
                        //lineitem = [SELECT ID,Name FROM AC__c WHERE Authorization__c=:auth1.id limit 1];
                        
                        
                        /*****************************************************************************************************************start=========Amendment and Renewal*********************************************************/
                        
                        //if(lineitem!=null){  
                        if(lineItemMap.get(auth1.id)!=null) {
                            AC__c currentLineItem = lineItemMap.get(auth1.id);
                            //Commented out by Terry Hale 1/30/2019 W-025837 Query inside For Loop
                            //ID ARParentLineItem = [SELECT ID,Name,Parent_Line_Item__c,Child_Line_Item__c,Child_Authorization__c,Parent_Authorization__c 
                            //                       FROM Amendment_Renewal__c 
                            //                      WHERE Child_Line_Item__c=:lineitem.id limit 1].Parent_Line_Item__c;
                            //ID originalauthID = [SELECT ID,Name,Authorization__c 
                            //FROM AC__c WHERE ID=:ARParentLineItem limit 1].Authorization__c;
                            ////system.debug('Current Line Item: ' + currentLineItem);
                            ////system.debug('Amendment Renewal Map: ' + acAmendmentRenewalMap);
                            ID parentLineItemId = acAmendmentRenewalMap.get(currentLineItem.id);
                            ////system.debug('Parent Line Item ID: ' + parentLineItemId);
                            ////system.debug(' Line Item Map: ' + lineItemAuthorizationMap);
                            AC__c originalauthObject   =   lineItemAuthorizationMap.get(parentLineItemId);
                            ////system.debug('Original Auth Object: ' + originalauthObject);
                            ID originalauthid = originalauthObject.Authorization__c;
                            //system.debug('Original Auth ID: ' + originalauthID);
                            Amendment_Renewal__c newAR = new Amendment_Renewal__c();
                            newAR.Child_Authorization__c = auth1.id;
                            newAR.Parent_Authorization__c = originalauthid;
                            String orgAuthStatusUpdate;
                            //Authorizations__c originalauth = [SELECT ID,Name,Permit_number__c,Expiration_Date__c,Program__c FROM Authorizations__c WHERE id=:originalauthID];
                            if(auth1.Application_Type__c.contains('Renewal')){
                                newAR.RecordTypeID = Schema.SObjectType.Amendment_Renewal__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
                                orgAuthStatusUpdate = 'Renewal In Review';
                                if(originalAuthorization.Program__c.contains('BRS')){
                                    for(Authorization_Junction__c oldConditions : renewalConditionsList){
                                        Authorization_Junction__c newConditions = oldConditions.clone(FALSE, TRUE);
                                        newConditions.Authorization__c = auth1.id;
                                        newConditionsList.add(newConditions);
                                    }
                                    // commenting out DML in loop. JB 5/8/2019
                                    //insert newConditionsList;
                                }
                            }
                            
                            if(auth1.Application_Type__c.contains('Amendment')){
                                newAR.RecordTypeID  = Schema.SObjectType.Amendment_Renewal__c.getRecordTypeInfosByName().get('Amendment').getRecordTypeId();
                                orgAuthStatusUpdate = 'Amendment In Review';
                                //Terry Hale 1/17/19 W-025837  Clones related conditions of original authorization
                                if(originalAuthorization.Program__c.contains('BRS')){
                                    for(Authorization_Junction__c oldConditions : amendmentConditionsList){
                                        Authorization_Junction__c newConditions = oldConditions.clone(FALSE, TRUE);
                                        newConditions.Authorization__c = auth1.id;
                                        newConditionsList.add(newConditions);
                                    }
                                    // commenting out DML in loop. JB 5/8/2019
                                    //if(!newConditionsList.isEmpty())  //KA
                                    //insert newConditionsList;
                                }
                            }
                            // commenting out DML inside for-loop. JB 5/8/2019
                            newARs.add(newAR);
                            //insert newAR;
                            if(auth1.Permit_number__c==null || auth1.Permit_number__c==''){
                                //Terry Hale 1/17/19 W-026036
                                if(originalAuthorization.Program__c.contains('AC') || originalAuthorization.Program__c.contains('VS') || originalAuthorization.Program__c.contains('PPQ')){
                                    originalAuthorization.Status__c =orgAuthStatusUpdate;
                                    auth1.Permit_number__c = originalAuthorization.Permit_number__c;
                                }else if(originalAuthorization.Program__c.contains('BRS')){
                                    originalAuthorization.Status__c = 'Issued';
                                    auth1.Permit_number__c = '';
                                }
                                if(auth1.Application_Type__c.contains('Amendment')){ 
                                    auth1.Expiration_Date__c = originalAuthorization.Expiration_Date__c;
                                    //auth1.Effective_Date__c = date.TODAY();  //Ravee Racharla W-026210  
                                }
                                if(auth1.Application_Type__c.contains('Renewal')){ 
                                    //auth1.Expiration_Date__c = originalAuthorization.Expiration_Date__c.addYears(1);
                                    // auth1.Effective_Date__c = originalAuthorization.Expiration_Date__c+1;
                                    // W-036582  RR 5/24/2019 No need to populate issue date
                                   // auth1.Date_Issued__c = originalAuthorization.Expiration_Date__c.adddays(1);
                                } 
                                
                                
                            }
                            
                        }   
                        /******************************************************************************end=========Amendment and Renewal******************************************************************************************/
                        
                    }
                }    
                TPids.add(auth1.Thumbprint__c);
                tpauthmap.put(auth1.prefix2__c, auth1);
                
                authidauth.put(auth1.id, auth1);
                
            }
            
            // JB 5/8/2019: Moving DML outside of for-loop.
            if(!newARs.isEmpty()){
                insert newARs;
            }
            
            if(originalAuthorization.Id != null){
                update originalAuthorization;
            }
            list<Authorizations__c> authListToUpdate1 = new list<Authorizations__c>();
            authListToUpdate1.addAll(authList.keyset());
            if(!authListToUpdate1.isEmpty()){
                update authListToUpdate1;
            }
            if(!newConditionsList.isEmpty()){
                insert newConditionsList;
            }
            // END OF NEW CODE BLOCK. JB
            
            //system.debug('newLineItemList>>>>>>'+newLineItemList);         
            //************************************************************Authorization Junction Creation - Start ************************************************************
            if(!newLineItemList.isEmpty()) 
            {
                Set<Related_Line_Decisions__c> relatedLineDecisionsList =new Set<Related_Line_Decisions__c>([SELECT ID,Line_Item__c, Decision_Matrix__c FROM Related_Line_Decisions__c WHERE Line_Item__c IN: newLineItemList ]); //#####################################################Q2
                set<id> DMIDs = new Set <id>();
                list<Authorization_Junction__c> AuthJunctionList = new list<Authorization_Junction__c>();
                Map<ID, ID> lineAuthMap = new  Map<ID, ID>();
                Map<ID, Authorization_Junction__c> insertingAJs = new Map<ID, Authorization_Junction__c>();
                List<Authorization_Junction__c> Final_AuthJunction_list = new List<Authorization_Junction__c>();
                Map<id,Authorization_Junction__c> authjunctionMap = new map<id,Authorization_Junction__c> () ; 
                //system.debug('relatedLineDecisionsList >>>>>>'+relatedLineDecisionsList );    
                for(Related_Line_Decisions__c RLD : relatedLineDecisionsList)
                { 
                    DMIDs.add(RLD.Decision_Matrix__c);
                    
                }
                
                
                for(AC__c a: newLineItemList){
                    lineAuthMap.put(a.ID,a.Authorization__c);
                    if(a.Research_Name__c!=null || a.Research_Name__c!=''){
                        ResearchLocation = a.Research_Name__c;
                    }
                }
                //system.debug('r>>>>>>>>>>>>>>>>>>>>>>>>'+DMIDs);
                
                for(Related_Line_Decisions__c Rel : relatedLineDecisionsList)
                {
                    for(Signature_Regulation_Junction__c SRJ: [SELECT ID,order_number__c, Decision_Matrix__c, Decision_Matrix__r.Thumbprint__c, Regulation__c, Regulation__r.Id, Regulation__r.Regulation_Description__c, Signature__c FROM Signature_Regulation_Junction__c WHERE Decision_Matrix__c IN:DMIDs] )
                    {
                        if(Rel.Decision_Matrix__c == SRJ.Decision_Matrix__c)
                        {
                            ID lineid = Rel.line_item__c;
                            
                            Authorization_Junction__c Authorization_Junction = new Authorization_Junction__c();
                            Authorization_Junction.is_Active__c = 'Yes';
                            Authorization_Junction.Authorization__c=lineAuthMap.get(lineid);
                            Authorization_Junction.Signature_Regulation_Junction_Name__c=SRJ.ID;
                            Authorization_Junction.Regulation__c = SRJ.Regulation__c;
                            Authorization_Junction.Decision_Matrix__c = SRJ.Decision_Matrix__c;
                            
                            Authorization_Junction.Regulation_Description__c= SRJ.Regulation__r.Regulation_Description__c;
                            
                            Authorization_Junction.order_number__c = SRJ.order_number__c;
                            AuthJunctionList.add(Authorization_Junction);
                            
                        }
                    }
                }
                
                //system.debug('AuthJunctionList >>>>>>>>>>>>>>'+AuthJunctionList );
                for(Authorization_Junction__c aj: AuthJunctionList )
                {
                    String changedstring;
                    if(aj.Regulation_Description__c!=null && aj.Regulation_Description__c.contains('{!LineItemLocation}') && (ResearchLocation!=null && ResearchLocation!='')){
                        changedstring = aj.Regulation_Description__c.replace('{!LineItemLocation}',ResearchLocation);
                        aj.Regulation_Description__c = changedstring;
                        
                    }
                    authjunctionMap.put(aj.Regulation__c, aj);       
                }       
                
                Final_AuthJunction_list.addAll(authjunctionMap.values());
                
                //system.debug('Final_AuthJunction_list>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.'+Final_AuthJunction_list);
                try
                {
                    upsert Final_AuthJunction_list; //---------------------inserting Auth Junctions 
                    // Teh L 8/24: commenting out this 
                    /*******
for(Authorization_Junction__c aj: Final_AuthJunction_list){
//system.debug('aj.Id>>>>.'+aj.id);
//system.debug('aj.Name>>>>>'+aj.Name);
//system.debug('aj Reg Desc>>>>'+aj.Regulation_Description__c);

} ****/
                }
                catch(Exception e)
                {
                    //system.debug(e);   
                }
            }
            
            
            //************************************************************Authorization Junction Creation - End ************************************************************
            
            
            
            Set<String>  PrefixIDs = new Set<String>();
            Map<ID,Signature__c> ThumbprintMap = new Map<ID,Signature__c>();
            
            if(!TPids.isEmpty()) 
            {
                for(Signature__c sign : [select ID,Program_Prefix__c,Name,Response_Type__c, Program_Prefix__r.Name,Program_Prefix__r.Process_Type__c From Signature__c where ID IN: TPids])
                {
                    PrefixIDs.add(sign.Program_Prefix__r.Name);
                    ThumbprintMap.put(sign.ID, sign);
                }
            }   
            
            
            // Commented Out by KK as we are deleting Old Workflow Design 
            //  WFtasksinsert = CARPOL_UNI_WFTaskCreator.createWFTasks(PrefixIDs);
            
            Id recTypeID= Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
            
            
            Map<Authorizations__c,Id> childauthparentidmap = new   Map<Authorizations__c,ID>(); 
            Map<String,List<Workflow_Task__c>> prefixwflistmap = new  Map<String,List<Workflow_Task__c>>();
            Set<ID> wfauth = new Set<ID>();
            Integer IntA = 0;
            String ResponseType;
            String appId;
            String authId;
            String ResponseId;
            String ProcessType;
            String Hiddenflag='';
            String prefix;
            String violator;
            string authstatus;
            Boolean neeedBothPermits=false;
            List<Authorizations__c> AuthListToUpdate = new List<Authorizations__c>();
            List<Workflow_Task__c> deletelist = new  List<Workflow_Task__c>();
            /* Commented Out by KK as we are deleting Old Workflow Design
* To Be: Clean Out this class which are referring to Old Workflow Design
for(Workflow_Task__c wf : WFtasksinsert)
{       
Authorizations__c auth = tpauthmap.get(wf.Program_Prefix__c);

wf.WF_Auth_Hidden_flag__c = auth.Auth_Type_Hidden_Flag__c;
if(wf.WF_Auth_Hidden_flag__c=='ingredient' && WF.name.contains('Violator'))
{
deletelist.add(wf);
}

if(wf.WF_Auth_Hidden_flag__c=='ingredient' && WF.name.contains('Issue'))
{
deletelist.add(wf);
}
if(auth.BRS_Introduction_Type__c!='Import' && wf.Program_Prefix__c=='100' && WF.Name== 'Assign Inspection Station and Generate, Send Labels')
{
wf.Status__c = 'Deferred';
}
////system.debug('wf.Program_Prefix__c>>>>>>>>>'+wf.Program_Prefix__c);
////system.debug('tpauthmap.get(wf.Program_Prefix__c).ID>>>>>>>>>>>>>>>>'+ tpauthmap.get(wf.Program_Prefix__c).ID);
if(wf.Program_Prefix__c==tpauthmap.get(wf.Program_Prefix__c).prefix2__c)
{   
wf.authorization__c = tpauthmap.get(wf.Program_Prefix__c).ID;
WF.Is_Violator__c = tpauthmap.get(wf.Program_Prefix__c).Is_Violator__c;                             

CARPOL_UNI_Auth_Junction_WF createAuthjunction = new CARPOL_UNI_Auth_Junction_WF();
createAuthjunction.modifyWF(WF,auth,hiddenflag,neeedBothPermits,recTypeID,ThumbprintMap,AuthListToUpdate,childauthparentidmap);

if(prefixwflistmap.containsKey(wf.Program_Prefix__c))
{
List<Workflow_Task__c> MapwfList = prefixwflistmap.get(wf.Program_Prefix__c);
MapwfList.add(wf);
prefixwflistmap.remove(wf.Program_Prefix__c);
prefixwflistmap.put(wf.Program_Prefix__c,MapwfList);
} else {
List<Workflow_Task__c> MapwfList = new List<Workflow_Task__c>();
MapwfList.add(wf);
prefixwflistmap.put(wf.Program_Prefix__c,MapwfList);                                        
}


}
ResponseType = ThumbprintMap.get(auth.Thumbprint__c).Response_Type__c;
appId = string.valueof(auth.Application__c);
authId = string.valueof(auth.id);
ResponseId = string.valueof(ResponseType);
ProcessType = ThumbprintMap.get(auth.Thumbprint__c).Program_Prefix__r.Process_Type__c;
prefix = auth.Thumbprint__r.Program_Prefix__r.Name;
violator = auth.Is_violator__c;
authstatus = auth.status__c;

wfauth.add(wf.authorization__c);
}  */
            
            ////system.debug('wfauth>>>'+wfauth);
            List<Authorizations__c> leftoverauths = new List<Authorizations__c>();
            if(authList.size()!=wfauth.size() || test.isRunningTest())
            {
                for(Authorizations__c auth : authList.keyset())
                {
                    if(!wfauth.contains(auth.id) || test.isRunningTest())
                        leftoverauths.add(auth);
                }
            }
            // //system.debug('<!!! leftoverauths'+leftoverauths);
            
            //*****************************************************************************************************************************leftover auths************************
            /* Commented Out by KK as we are deleting Old Workflow Design
if(!leftoverauths.isEmpty()){
for(Authorizations__c auth: leftoverauths)
{
List<Workflow_Task__c> leftoverwfs =prefixwflistmap.get(auth.prefix2__c);

if(leftoverwfs!=null)
{
List<Workflow_Task__c> programworkflows = new List<Workflow_Task__c>();
for(Workflow_Task__c wf : leftoverwfs)
{
Workflow_Task__c WFTask = new Workflow_Task__c();
WFTask = wf.clone(false,true);
WFTask.authorization__c = auth.id;
programworkflows.add(WFTask);
}
for(Workflow_Task__c wf : programworkflows)
{   
//===========================================================================start========================Leftover===========Fast Track======================                              

CARPOL_UNI_Auth_Junction_WF createAuthjunction = new CARPOL_UNI_Auth_Junction_WF();
createAuthjunction.modifyWF(WF,auth,hiddenflag,neeedBothPermits,recTypeID,ThumbprintMap,AuthListToUpdate,childauthparentidmap);
}
WFtasksinsert.addAll(programworkflows);
}

}
}
*/
            /*list <authorizations__c> authToUpdWFTcountlist = new list <authorizations__c>(); // VV w-017405
for (authorizations__c authToUpdWFTcount: authList.keyset()){
authToUpdWFTcount.EFLNumberofWFtasks__c = WFTasksinsert.size(); 
authToUpdWFTcountlist.add(authToUpdWFTcount);
}
if (!test.isrunningtest())
update authToUpdWFTcountlist; */
            //***********************************************************************************************************************************leftover auths*************************************************************************************
            
            ////system.debug('WFTasksinsert>>>>>>>>'+WFTasksinsert);
            // Teh L 8/24: Commenting out this
            
            //     for(Workflow_Task__c wf:WFtasksinsert){
            //       //system.debug('auth is???????????'+wf.authorization__c);
            //       //system.debug('WF owner is???????????'+wf.owner); 
            //   }
            //Commented out by KK 
            // Database.SaveResult[] savelst = Database.insert(WFtasksinsert); //********************inserting WF tasks *********************
            
            Set<Authorizations__c> Dedupeset = new Set<Authorizations__c>();
            List<Authorizations__c> DedupeAuths = new  List<Authorizations__c>();
            if(ProductAuths.size()!=null)//.isEmpty()
            {
                AuthListToUpdate.addall(ProductAuths);
                Dedupeset.addall(AuthListToUpdate);
                DedupeAuths.addall(Dedupeset);
                for(Authorizations__c auth :DedupeAuths){
                    if(auth.Auth_Type_Hidden_Flag__c=='ingredient' && (auth.Status__c=='Issued' || auth.Status__c=='Approved'))
                    {   
                        if(authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c==null){
                            Integer i=1;
                            authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c = string.valueOf(i);
                        }
                        else{
                            Integer j;
                            j=integer.valueOf(authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c);
                            j++;
                            authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c =string.valueOf(j);
                        }
                    }   
                }
                try
                {
                    update DedupeAuths; //---------------------updating Product +Fasttrack auths 
                }
                catch(Exception e)
                {
                    //system.debug(e);   
                }
            }
            
            else{
                try
                {
                    update AuthListToUpdate; //---------------------updating just Fasttrack auths 
                }
                catch(Exception e)
                {
                    //system.debug(e);   
                }
            }
            if(FacilityAuths.size() > 0){
                try
                {
                    //updating in bulk
                    update FacilityAuths; //---------------------updating facility auths 
                }
                catch(Exception e)
                {
                    //system.debug(e);   
                }
            }
            /*KK Commented on 6/17/2019 # To remove the FutureAttachmentCreator reference.
            //system.debug('Before future : ' + 'Process Type is '+ProcessType + ' '+' hidden flag is ' + hiddenflag + ' ' +' Response type is'+ ResponseType);
            if(ProcessType == 'Fast Track' && 
               (hiddenflag==null || hiddenflag=='' || hiddenflag=='product')  &&
               (ResponseType =='Permit' ||
                ResponseType == 'Letter of Denial' ||
                ResponseType == 'Letter of No Permit Required' ||
                ResponseType == 'Letter of No Jurisdiction'))
            {
                //system.debug('entering fast track loop ');
                IntA++;
                
                String Productcore = 'No';
                
                FutureAttachmentCreator.FastTrackAttach(string.valueOf(userinfo.getSessionId()), appId, authId, ResponseId);
                // Teh Liu:  This is esginlive Code: START
                if (ProcessType == 'Fast Track' && ResponseType == 'Permit' && System.Label.EFLeSignLive == 'Yes') {
                    Map<String, String> callouts = new Map<String, String>();
                    callouts.put('AuthId', authId);
                    //ESignLive.createPackageWithDocument(authId, true, 'package description', null, 'email message');                           
                    //EFLESignLive.createPackageWithDocument(authId, true, 'package description', null, 'email message');                           
                }
                // Teh Liu:  This is esginlive Code: END
                //system.debug('This is after future atttachm');
            }*/
            
        }
    }
    /*// KK Commented - Unused Code.
    public void modifyWF(Workflow_Task__c WF, Authorizations__c auth, String hiddenflag, Boolean neeedBothPermits,ID recTypeID, Map<ID,Signature__c> ThumbprintMap, List<Authorizations__c>AuthListToUpdate, Map<Authorizations__c,Id> childauthparentidmap){
        
        if(wf.Process_Type__c=='Fast Track')                                                           //===========================================================================start===========Fast Track====================== 
        { 
            WF.Complete_from_trigger__c = TRUE;
            wf.Locked__c = 'Yes';
            
            if( WF.name.contains('Check Violator')) //---------------------WF 1 - Fast Track
            {  
                WF.Status__c = 'Complete';
            }
            
            if(WF.Name.contains('Review Potential Violator')) //---------------------WF 2 - Fast Track
            {  
                WF.Status__c = 'Deferred';
            }
            
            if(WF.Name.contains('Process Authorization'))  //---------------------WF 3 - Fast Track
            { 
                WF.Status__c = 'Complete';
            }
            
            if(WF.Name.contains('Issue Authorization') || wf.Name.contains('Issue')) //---------------------WF 4 - Fast Track
            {  
                WF.Status__c = 'Complete';
                
                auth.Status__c = 'Issued';
                auth.Date_Issued__c = System.Today();
                
                Date AuthDate;
                Date expAuthDate;
                Integer ExpirationTimeframe;
                ExpirationTimeframe = integer.valueOf(auth.Expiration_Timeframe__c);
                AuthDate =  Date.newInstance(auth.Date_Issued__c.Year(),auth.Date_Issued__c.month(), auth.Date_Issued__c.day());
                auth.Expiration_Date__c = AuthDate.addDays(ExpirationTimeframe);
                
                auth.Authorization_Type__c = ThumbprintMap.get(auth.Thumbprint__c).Response_Type__c;
                hiddenflag = auth.Auth_Type_Hidden_Flag__c;
                
                AuthListToUpdate.add(auth);
                childauthparentidmap.put(auth,auth.Parent_Authorization__c);
            }
            
            //CITES LOGIC
            if(!auth.CITES_Needed__c || !auth.Regular_Permit_Needed__c) {
                if(WF.Name.contains('Issue Protected Plant Permit'))
                {
                    if(!auth.CITES_Needed__c){
                        WF.Status__c = 'Deferred';
                    }
                }
                if(WF.Name == 'Issue Standard Permit'){
                    if(auth.CITES_Needed__c && !auth.Regular_Permit_Needed__c)
                        WF.Status__c = 'Deferred';  
                }
            }
            if(auth.CITES_Needed__c && auth.Regular_Permit_Needed__c)
            {
                neeedBothPermits =true;
            }
            
        }                                                                                                              //===========================================================================end===========Fast Track====================== 
        else                                                                                                               //=======================================================================start=========Manual====================== 
        {
            if (auth.Is_Violator__c == 'No' && WF.name.contains('Check Violator')) //---------------------WF 1 - Manual 
            {
                if (auth.select_agent__c){ //VV added 9/8/16 or select agent
                    WF.Status__c = 'Not Started';
                }else{
                    WF.Status__c = 'Complete';
                }
                WF.Complete_from_trigger__c = TRUE;
                wf.Locked__c = 'Yes';
            }
            
            if (auth.Is_Violator__c == 'No' /*&& auth.Auth_Type_Hidden_Flag__c !='ingredient'*/ /*&& WF.Name.contains('Review Potential Violator'))//---------------------WF 2 - Manual
            { 
                WF.Status__c = 'Deferred';
                WF.Complete_from_trigger__c = TRUE;
                wf.Locked__c = 'Yes';
            }
            
            if (auth.Is_Violator__c == 'No'  && wf.Name.contains('Process Authorization') && auth.RecordTypeID != recTypeID ) //---------------------WF 3 - Manual-------keerthi changes added
            {
                WF.Status__c = 'Not Started';
            }
            
            if (auth.Is_Violator__c == 'No'  && (wf.Name.contains('Perform Evaluation') ||wf.Name.contains('Review Application'))) //---------------------WF 3 - Manual
            {
                WF.Status__c = 'Not Started'; //-----------------Added by keerthi
            }
            
        }
        
    }
   */
}