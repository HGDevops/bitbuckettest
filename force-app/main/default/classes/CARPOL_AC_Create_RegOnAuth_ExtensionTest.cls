@isTest(seealldata=true)
private class CARPOL_AC_Create_RegOnAuth_ExtensionTest {
    static testMethod void testCARPOL_AC_Create_RegOnAuth_Extension() {
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        objauth.thumbprint__c = [select id from Signature__c where name like '%Dogs%Permit' limit 1].Id;
        update objauth;        
        AC__c ac = testData.newLineItem('Personal Use',objapp,objauth);        
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp,objauth);
        Attachment attach = testData.newattachment(ac.Id);           
        
        PageReference pageRef = Page.CARPOL_AC_Create_RegulationOnAuth;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        CARPOL_AC_Create_RegOnAuth_Extension acepreg = new CARPOL_AC_Create_RegOnAuth_Extension(sc);
        acepreg.currentAuthorization.Id = objauth.Id;
        acepreg.newRegulation.Title__c = 'Test 20150727';
        acepreg.newRegulation.Custom_Name__c = 'Test 20150727 JialinDing';
        acepreg.newRegulation.Short_Name__c = 'Test 20150727 JialinDing';
        acepreg.newRegulation.Type__c = 'Import Requirements';
        acepreg.newRegulation.Sub_Type__c = 'Import Permit Requirements';
        acepreg.newRegulation.Regulation_Description__c = 'Test Description 20150727 JialinDing';
        acepreg.returnEditPermit();
        acepreg.RegRecordtypeName = 'Animal Care (AC)';
        acepreg.RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        acepreg.createReg();
        System.assert(acepreg != null); 
    }
}