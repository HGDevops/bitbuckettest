public with sharing class HelpComponentController {
    
    
    //Subclass : Wrapper Class 
    public class HelpListWrapper {
        //Static Variables 
        public string option;
        public string description;
        
        //Wrapper  Class Controller
        HelpListWrapper() {
            
        }
        
    }
    public List<HelpListWrapper> hWrapList {get;set;}
    public static String getHelpContent(Boolean secondScreen, Boolean thirdScreen){
        
        List<HelpListWrapper> hWrapList= new List<HelpListWrapper>();
        List<SelectOption> options = new List<SelectOption>();
        for(CARPOL_External_Landing__c recCEL: [Select Name,Description__c from CARPOL_External_Landing__c where Is_Active__c=:true Order By Sequence__c]){
            HelpListWrapper hwrap = new HelpListWrapper();
            hwrap.option = recCEL.Name;
            hwrap.description = recCEL.Description__c;
            hWrapList.add(hwrap);
        }
        return Json.serialize(hwrapList);
    }
    
    
}