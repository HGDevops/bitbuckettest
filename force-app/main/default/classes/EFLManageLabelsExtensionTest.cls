@isTest(SeeAllData=True)
public class EFLManageLabelsExtensionTest {
    @isTest
    public static void testMyController() {
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        testDataBRS.insertcustomsettings();
        
        Domain__c prg = new Domain__c();
        prg = testDataBRS.newprogram('BRS');
        
        Program_Prefix__c pprex = testDataBRS.newPrefix();
        
        Signature__c sign = new Signature__c();
        sign = testDataBRS.newThumbprint();
        
        Program_Line_Item_Pathway__c plipath = new Program_Line_Item_Pathway__c();
        plipath = testDataBRS.newBRSPathway();
        
        Application__c app = new Application__c();
        app = testDataBRS.newapplication();
        
        Authorizations__c auth = new Authorizations__c();
        auth = testDataBRS.newauth(app.id);
        auth.Authorization_Type__c = 'Letter of No Permit Required';
        auth.BRS_Create_Labels__c = false;
        auth.Program_Pathway__c = plipath.id;
        //auth.Status__c = 'Issued';
        auth.Expiration_Date__c = Date.Today();
        auth.BRS_Number_of_Labels__c = 20;
        update auth;
        
        Label__c lab = new Label__c();
        lab.Name = 'Testlab';
        lab.Authorization__c = auth.id;
        insert lab;
        
        Label__c lab1 = new Label__c();
        lab1.Name = 'Testlab1';
        lab1.Authorization__c = auth.id;
        insert lab1;
        
        Test.startTest();
        EFLManageLabelsExtension obj = new EFLManageLabelsExtension(new ApexPages.StandardController(auth));
        obj.AdditionalLabels = 10;
        obj.viewDraftPDF();
        obj.attachPDF();
        obj.Cancel();
        obj.Createlabels();
        //obj.sendlabels();
        obj.Redirect();
        obj.deleteRec();
        obj.voidlabels();
        //add labels and create them
        obj.AdditionalLabels = 2;
        obj.labelcolor = 'Blue and White';
        obj.btntype='All';
        obj.Createlabels();
        obj.authorization = new Authorizations__c();
        obj.sobjectkeys = new Map<string, string>();        
        
        Test.stopTest();
    }
    
    // new unit test added 2/4/2019 by j.benkert@accenturefederal.com
    // @description when new labels are created, the Plant_Inspection_Station_lkup__c should copy from the auth record.
    @isTest
    static void testPISCopiedToLabels(){
        Profile p = EFLTestDataFactoryBulk.getProfileByName('BRS Analyst');
        User analyst = EFLTestDataFactoryBulk.getUser(p);
        
        system.runAs(analyst){
            EFLTestDataFactoryBulk.setupTestDataSuite(1);
            Id rtId = [SELECT Id FROM REcordType WHERE Name = 'Inspection Station' AND SObjectType = 'Facility__c'][0].Id;
            
            Facility__c fac = new Facility__c();
            fac.RecordTypeId = rtId;
            insert fac;
            
            Authorizations__c auth = [SELECT Id FROm Authorizations__c WHERE Id IN :EFLTEstDataFactoryBulk.authIds][0];
            auth.Authorization_Type__c = 'Letter of No Permit Required';
            auth.BRS_Create_Labels__c = false;
            auth.BRS_Number_of_Labels__c = 20;
            auth.Plant_Inspection_Station__c = fac.Id;
            auth.Status__c = 'Approved';
            update auth;
            
            Delete [SELECT Id FROM Label__c WHERE Authorization__c = :auth.Id]; // this removes labels from auth without PIS.
            
            ApexPages.StandardController sc  = new ApexPages.StandardController(auth);
            EFLManageLabelsExtension ext = new EFLManageLabelsExtension(sc);
            test.startTest();
            ext.createLabels();
            
            test.stopTest();
            
            Label__c[] labels = [SELECT Id, Plant_Inspection_Station_lkup__c FROM Label__c WHERE Authorization__c = :auth.Id];
            system.assertEquals(auth.BRS_Number_of_Labels__c, labels.size());
            for(Label__c l : labels){
                system.assertEquals(fac.Id, l.Plant_Inspection_Station_lkup__c);
            }
        }
    }
}