@isTest(seealldata=true)
private class EFLConstructViewController_Test {
   
   
   Public static EFLConstructController conCntrl;
   Public static GenotypeType__c genoTypeType;
   Public static CARPOL_BRS_TestDataManager testData;
   Public static Application__c currentApp;
   Public static AC__c ac1;
   Public static Authorizations__c auth;
   Public static Program_Line_Item_Pathway__c progPath;
   Public static Regulated_Article__c regArt;
   Public static RA_Scientific_Name_Junction__c raJunc;
   Public static Link_Regulated_Articles__c linkRA; 
   Public static List<Link_Regulated_Articles__c> linkRAList;  
   Public Static Construct__c cons1;
   Public Static Phenotype__c phenoType;
   Public Static Genotype__c genoType;
   Public static Regulated_Article__c regArt2;
      
   Public static void initdata()
    {
        
        
        
        
        testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        currentApp = testData.newapplication();
        
        regArt = new  Regulated_Article__c();
        regArt.Name='Test article';
        regArt.RecordTypeID=Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        regArt.Status__c='Active';
        regArt.Category_Group_Ref__c = testData.newgroup().Id;
        insert regArt;
        
        raJunc = new  RA_Scientific_Name_Junction__c();
        raJunc.Regulated_Article__c = regArt.id;
        
        auth = testData.newAuth(currentApp.Id);
        ac1 = testData.newLineItemBRS('Personal Use', currentApp);
        
        Regulated_Article__c regArt2 = testData.newRegulatedArticle(auth.Program_Pathway__c);
        linkRA = testData.newlinkRegArticle(ac1.id, currentApp.Id, auth.id);
        linkRAList =  new list<Link_Regulated_Articles__c>();
        linkRAList.add(linkRA);
       /* linkRA.Line_Item__c = ac1.id;
        linkRA.Regulated_Article__c = regArt2.id;
        upsert linkRA;
        system.debug('LinkRA set to: ' + linkRA); */
        
        cons1 = testData.newconstruct(ac1.id, regArt2);        
        phenoType = testData.newphenotype(cons1.id);
        
        genoTypeType = new GenotypeType__c();
        genoTypeType.Construct__c = cons1.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;
        
        genoType = testData.newgenotype(cons1.id, genoTypeType);
       
        
        
           
       } 
        public static testMethod void csstest_control_Test()
        {
        initData();
        Test.startTest();
         EFLConstructViewController ConViewCntlr = new EFLConstructViewController();
         ConViewCntlr.lineItemId = ac1.id;
         ConViewCntlr.constructID = cons1.id;
         ConViewCntlr.constructRecord = cons1;
         ConViewCntlr.phenoTypeRecord = phenoType;
         ConViewCntlr.genoTypeRecord = genoType;
         ConViewCntlr.linkregart = linkRAList;
         ConViewCntlr.selectedRA = cons1.Link_Regulated_Article__c;
         ConViewCntlr.phenoTypeID= phenoType.id;
         ConViewCntlr.genoTypeID= genoType.id;
         ConViewCntlr.selectedGenoCategory = genoType.recordtypeid;
        Test.stopTest(); 
        }
    //-------------------------------------------------------------------------------------------------
}