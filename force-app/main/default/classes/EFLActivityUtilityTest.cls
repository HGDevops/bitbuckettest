@isTest
public class EFLActivityUtilityTest {
    
    static testMethod void testPopulateTask(){
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today(); 
        insert objInc;
        system.debug('Incident from Test Class@@@@ : ' + objInc);
        system.debug('Incident WF Number from Test Class@@@@ : ' + objInc.Workflow_Number__c);
        integer taskSequence = objInc.Activity_Sequence__c==null?1:objInc.Activity_Sequence__c.intvalue();
        Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(objInc.stage__c, objInc.Workflow_Number__c,taskSequence + 1);
        system.debug('ActivityCMD from Test Class#### : ' + currentActivityCMD);
        
        EFLActivityUtility.populateTask(objInc, currentActivityCMD, objInc.Owner.id);
    }
    
    static testMethod void testgetTeamMembers(){
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today();   
        insert objInc;
        
        EFLActivityUtility.getTeamMembers(objInc.Id);
    }
    
    static testMethod void testvalidateRoles(){
        EFLActivityUtility activityUtility = new EFLActivityUtility();
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today(); 
        insert objInc;
        List<String> currentRequiredRoles = new List<String>();
        list<team__c> teamMembersList = new list<team__c>();
        currentRequiredRoles = EFLProgramPrefixUtility.getRequiredRoles(objInc.Workflow_Number__c);
        teamMembersList = EFLTeamRepository.selectByIncidentID(objInc.ID);
        
        activityUtility.validateRoles(objInc, teamMembersList, currentRequiredRoles);
    }
    
    static testMethod void testBooleans(){
        Incident__c objInc=new Incident__c();
        objInc.status__c='ANALYSIS IN PROGRESS';
        objInc.Stage__c = 'Incident Review';
        objInc.Workflow_Number__c = 'BRS Incident';
        String IncRecTypeID = Schema.SObjectType.Incident__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        objInc.RecordTypeId = IncRecTypeID;
        objInc.Incident_Date__c = Date.today();
        objInc.Incident_Discovery_Date__c = Date.today();
        objInc.Incident_Reported_Date__c = Date.today(); 
        insert objInc;
        system.debug('Incident from Test Class@@@@ : ' + objInc);
        system.debug('Incident WF Number from Test Class@@@@ : ' + objInc.Workflow_Number__c);
        integer taskSequence = objInc.Activity_Sequence__c==null?1:objInc.Activity_Sequence__c.intvalue();
        Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(objInc.stage__c, objInc.Workflow_Number__c,taskSequence + 1);
        system.debug('ActivityCMD from Test Class#### : ' + currentActivityCMD);
        
        EFLActivityUtility.validateTeam();
        EFLActivityUtility.validateActivityTask();
        EFLActivityUtility.isLastActivityForStage(currentActivityCMD);
    }

}