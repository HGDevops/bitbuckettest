public with sharing class EFLExtContactAcceptance {
    
    //Class for external user invitation page to join as a partner user
    // if user accepted, get params from URL, Create a new contact, update the User Info record with Old user id, new userid, fed id 
    // status and UI id. This update triggers a helper class that creates partner user by reading UI values set.
    // If user denied, update status in UI record.
    public String lastName{get;set;}
    public String firstName{get;set;}
    public string uiStatus{get;set;} 
    public String emailaddress;
    public String currUserName; 
    public String accId;
    public Id contactQueriedId;
    public Id userInviteId;
    public string accountName {get;set;}
    public string accountAdminName {get;set;}
    public Contact currentContact;
    public user currUser;
    public boolean matchFound{get;set;}
    public boolean matchNotFound{get;set;}
    public User_Invite__c ui = new User_Invite__c();
    public List<CARPOL_UserInviteController.InvitedContact> inviteeList = new List<CARPOL_UserInviteController.InvitedContact>();
    public EFLExtContactAcceptance() {
        accId = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('accId') );
        lastName = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('lastname') );
        firstName = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('firstname') );
        emailaddress = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('emailaddress') );
        currUsername = userInfo.getusername();
        userInviteId = EFLGenericUtility.sanitizeString( apexpages.currentpage().getparameters().get('userInviteId') );
        List<contact> accAdmn = [select id, name, Account_Admin__c, account.name from contact where AccountId =:accId and Account_Admin__c =true]; //Get Account admin details
        currUser = [SELECT contactId, contact.Phone FROM user WHERE username =:currUsername];
        if(!accAdmn.isempty()){
            //accountAdminName = accAdmn[0].name; 
            accountName = accAdmn[0].account.name;  
            contactQueriedId = accAdmn[0].id;
        }
        String inviteQuery = 'SELECT id, Invitee_List__c,Partner_Admin__r.firstname,Partner_Admin__r.lastname FROM User_Invite__c WHERE id=:userInviteId';
        ui = database.query(inviteQuery); 
        accountAdminName = ui.Partner_Admin__r.firstname+' '+ui.Partner_Admin__r.lastname;
        // parse json string for this contact
        inviteeList = (List<CARPOL_UserInviteController.InvitedContact>) Json.deserialize(ui.Invitee_List__c, List<CARPOL_UserInviteController.InvitedContact>.class);
        matchFound = false;
        matchNotFound = false;  
        for (CARPOL_UserInviteController.InvitedContact ic: inviteeList){
            if (ic.email == emailAddress && ic.status == 'Sent'){        
                matchFound = true;
            }else{
                if ( ic.status == 'Accepted')
                    uiStatus = 'accepted';
                if ( ic.status == 'Denied' || ic.Status=='Declined')
                    uiStatus = 'declined';                     
                matchNotFound = true;
            }
        }
        
    }
    
    public PageReference accept() {
        try {
            
            if(String.isNotBlank(emailAddress) && String.isNotBlank(lastName) && String.isNotBlank(firstName) && String.isNotBlank(accId) && String.isNotBlank(currUserName)) {
                List<Account> accList = [SELECT Id, Name,IsCustomerPortal,IsPartner FROM Account WHERE Id=:accId LIMIT 1]; 
                String profName = '';
                if(accList[0].IsCustomerPortal)
                    profName = 'APHIS Applicant';
                else if(accList[0].IsPartner)
                    profname = 'Org Admin';
                List<Profile> profList = [SELECT Id FROM Profile WHERE Name = :profname LIMIT 1];              
                Database.saveResult conResult;
                if(accList != Null && !accList.isEmpty() && profList != Null && !profList.isEmpty()) {                    
                    //Creating new Contact
                    Contact newCon = new Contact();
                    newCon.AccountId = accList[0].Id;
                    newCon.FirstName = firstname;
                    newCon.LastName = lastname;
                    newCon.Email = emailaddress;
                    newCon.Phone = currUser.contact.Phone;
                    newCon.RecordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();                  
                    conResult = Database.insert(newCon);
                    //get user details of community contact
                    User u =[SELECT Id, FederationIdentifier, ContactId, UserName FROM User WHERE username = :currUsername limit 1];
                    if (inviteeList != null){
                        //set values so they can be used in user creation  in trigger
                        for (CARPOL_UserInviteController.InvitedContact ic: inviteeList){
                            if (ic.email == emailAddress && ic.status == 'Sent'){
                                ic.status = 'Accepted';//
                                ic.oldUserId = u.Id;
                                ic.newConId = conResult.Id;
                                ic.FedId = u.FederationIdentifier;                               
                            }   
                        }                       
                    }
                    ui.Invitee_List__c = JSON.serialize(inviteeList);
                    if(!ui.From_Accept__c){
                        ui.From_Accept__c = TRUE;
                    }
                    update ui; //Update the user invite record
                    PageReference newPgr = new PageReference('/EFLPartnerUserCreateCompletion');
                    return newPgr;                     
                }   
            }           
        } catch(Exception e) {
            System.debug('Exception : '+e.getStackTraceString());
        }
        return null;
    }
    //updating the status field to Denied when they click Deny.
    public PageReference deny() {  
        if (inviteeList != null){
            for (CARPOL_UserInviteController.InvitedContact ic: inviteeList){
                if (ic.email == emailAddress && ic.status == 'Sent'){
                    ic.status = 'Declined';                    
                }   
            }                       
        }
        ui.Invitee_List__c = JSON.serialize(inviteeList); 
        update ui; 
        pagereference pgrf = new pagereference('/portal_application');          
        return pgrf;        
    }
    public PageReference gotoLndgPg() {
        pagereference pgrf = new pagereference('/portal_application');          
        return pgrf;  
    }    
}