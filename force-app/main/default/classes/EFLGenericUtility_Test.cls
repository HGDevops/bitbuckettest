@isTest
private class EFLGenericUtility_Test{

    static testMethod void TestrandomString() {
        system.assert(EFLGenericUtility.randomString() != null);
    }
    
    static testMethod void TestgetRecordTypeIdAndDelete() {
        
        id olocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin');
        Id olocId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        
		system.assert(olocrectypeid == olocId);

        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objApp = testData.newapplication();
        AC__c ac1=testData.newLineItemBRS('Personal Use',objApp);
        
        Test.startTest();		     
        EFLGenericUtility.deleteRecord(ac1.Id); 
        EFLGenericUtility.deleteRecord(null); 
        Test.stopTest();
        
        List<AC__c> ac = [SELECT Id FROM AC__c WHERE Id=: ac1.Id];
       
        system.assert(ac.size() == 0);        
    }
    
    static testMethod void TesthideCBIContent() {
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objApp = testData.newapplication();
        AC__c ac1=testData.newLineItemBRS('Personal Use',objApp);
        ac1.Air_Transporter_Flight_Number__c = '[PHL12345]';
        update ac1;
        List<SObject> results = new List<SObject>();
        results.add(ac1);
        //system.debug('ac1=== ' + EFLGenericUtility.hideCBIContent(results));
        results =  EFLGenericUtility.hideCBIContent(results);
        for (AC__c lineItem: (List<AC__c>) results) {
            system.assert(lineItem.Air_Transporter_Flight_Number__c == '[  ]');            
        }
        
     
    }
    
    static testMethod void TestisValidSalesforceId() {
        Account acc1 = new Account(Name = 'TestAccountName');
        Insert acc1;
        
        Contact con = new Contact(LastName = 'TestContactLName');
        insert con;        
        Type t = Type.forName('Account');
        Test.startTest();
        Boolean isValidaSFId = EFLGenericUtility.isValidSalesforceId(acc1.Id, t);
        system.assert(isValidaSFId == true);

        system.assert(EFLGenericUtility.isValidSalesforceId(con.Id, t) == false);
        Test.stopTest();
    }
}