public inherited sharing class EFLConstructTriggerHandler {
    
    public static void ValidateConstructs(List<Construct__c> constructs)
    {
        Map<string,set<Construct__c>> constructsByOrg = new Map<string ,set<Construct__c>>();
        for(Construct__c con : constructs)
        { 
            //W-032118 ::  Redesign: PRC Filtering | Account Restriction
            if(string.isNotBlank(con.AccountID__c)){
                con.Account__c = con.AccountID__c;
            }
            
            //Adding Construct Uniquenesss logic based on formula text field vs old logic with text field which had a limitation of 255
            if(con.Construct_s__c  !='' || con.Construct_s__c  !=NULL){
                if(constructsByOrg.get(con.Uniqueness__c) == null)
                {
                    constructsByOrg.put(con.Uniqueness__c, new set<Construct__c>{con});
                }
                else
                {
                    constructsByOrg.get(con.Uniqueness__c).add(con);
                }
            }
        }
        if(constructsByOrg.size()>0)
        {
            set<string> uniqueCons = constructsByOrg.keySet();
            for (Construct__c o : [select Regulated_Article__c,Uniqueness__c from Construct__c where Uniqueness__c in :uniqueCons]) 
            {
                for(Construct__c con : constructsByOrg.get(o.Uniqueness__c))
                {
                    if(con.Regulated_Article__c != null || con.Regulated_Article__c != ' '){
                        if(con.Regulated_Article__c == o.Regulated_Article__c)
                            con.Construct_s__c.adderror('Duplicate Construct Uniqueness name found. Construct already exist with same Name. Please change the Name.');
                    }
                }
            }
            
        }
        
    }
    public static void ValidateConstructs(List<Construct__c> oldConstructs, List<Construct__c> newConstructs )
    {
        List<Construct__c> constructs = new List<Construct__c>();
        
        for(integer i =0; i<oldConstructs.size() ; i++)
        {
            if(oldConstructs[i].Construct_s__c != newConstructs[i].Construct_s__c)
            {
                constructs.add(newConstructs[i]);
            }
        }
        if(constructs.size()>0)
        {
            ValidateConstructs(constructs);
        }
        
    }
    
    public static void ValidateRequiredFields(List<Construct__c> constructs)    
    {
        for(Construct__c conRec:constructs)
        {
            if(conRec.Link_Regulated_Article__c  ==NULL)
            {conRec.Link_Regulated_Article__c.adderror('Regulated Article is required.');} 
            if(conRec.Construct_s__c  =='' || conRec.Construct_s__c  ==NULL) {
                conRec.Construct_s__c.adderror('You must enter a value.');
            }
            if(conRec.Mode_of_Transformation__c == '' || conRec.Mode_of_Transformation__c  ==NULL)
            {conRec.Mode_of_Transformation__c.adderror('You must select a value.');}              
            
            if(string.isNotBlank(conRec.Construct_s__c)) {
                String strConstruct = conRec.Construct_s__c; 
                if( strConstruct.indexOf('[') >  -1 && strConstruct.indexOf(']') > 0 ){
                    
                    conRec.Construct_is_CBI__c = True; 
                }
            }
            if(conRec.Mode_of_Transformation_CBI__c == true){
                conRec.Construct_is_CBI__c = True; 
            }
            if(string.isNotBlank(conRec.Identifying_Line_s__c)){
                String strTrEvents = conRec.Identifying_Line_s__c; 
                if( strTrEvents.indexOf('[') >  -1 && strTrEvents.indexOf(']') > 0 ){
                    
                    conRec.Construct_is_CBI__c = True; 
                }
            }
        }
        
    }
    //Sets the sharing account on new construct records to ensure proper bulkification
    public static void setConstructSharing(List<Construct__c> constructs){
        Set<Id> lineItemIds = new Set<Id>();
        for (Construct__c con :constructs ){
            if (con.Source__c == 'XML'){
                lineItemIds.add(con.Line_Item__c);
            }            
        }
        if (!lineItemIds.isEmpty()){
            for (AC__c li : [Select Id,Sharing_Account__c from AC__c where id in :lineItemIds]){
                for (Construct__c con :constructs ){
                    if (con.Line_Item__c == li.Id){
                        con.Sharing_Account__c = li.Sharing_Account__c;
                    }
                }
            }
        }
    }
    
    /*public static list<AC__c> processConstructs(List<Construct__c> constructList){
list<ID> BRSlineitemlst = new list<ID>();
list<AC__c> listlineitemToUpdate = new list<AC__c>();
//system.debug('constructList:'+ constructList);
for(Construct__c con:constructList)
{
BRSlineitemlst.add(con.Line_Item__c); 
}
AC__c a = [SELECT ID,RecordType.Name,Construct_Status__c,(select id,Ready_to_Submit__c,Status__c,Corrections_Required__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst];
if(a.Constructs__r.size() > 0){
for(construct__c cons :a.constructs__r ){
if(cons.Ready_to_Submit__c == false){
a.Construct_Status__c = 'Yet to Add';
listlineitemToUpdate.add(a);

}
}
}

return listlineitemToUpdate;

}
//Set "Ready to Submit" flag for Constructs when Phenotype and Genotype records are created
public static void processConstructReadiness(List<Construct__c> constructList){
list<AC__c> lstlineitemToUpdate = new list<AC__c>(); 
lstlineitemToUpdate = processConstructs(constructList);
if(lstlineitemToUpdate.size()>0){
update lstlineitemToUpdate;
} 
}*/
    //Set "Ready to Submit" flag for Constructs when Phenotype and Genotype records are created
    public static void processConstructReadiness(List<Construct__c> constructList){
        list<ID> BRSlineitemlst = new list<ID>();
        list<AC__c> listlineitemToUpdate = new list<AC__c>();
        List<Construct__c> constructUpdate = new List<Construct__c>();
        //system.debug('constructList:'+ constructList);
        for(Construct__c con:constructList)
        {
            //system.debug('Construct - In insert/updaaaate is Line item null ?>> '+ con.Line_Item__c);
            BRSlineitemlst.add(con.Line_Item__c); 
        }
        //system.debug('***Construct - In insert/updaaaate all Line item ids:' + BRSlineitemlst);
        
        //AC__c a : [SELECT ID,RecordType.Name,Construct_Status__c,(select id,Ready_to_Submit__c,Status__c,Corrections_Required__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst];
        //Commenting the above line and adding below list to bulkify this scenario
        for(AC__c a : [SELECT ID,RecordType.Name,Construct_Status__c,(select id,Ready_to_Submit__c,Status__c,Corrections_Required__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst])
        {
            integer constructCount = 0;
            for(construct__c c: a.Constructs__r){
                constructCount ++; 
            }
            
            // if(a.Constructs__r.size() > 0){ // Commented to hanlde error related to W-037205
            if(constructCount > 0){ 
                Integer i=0;
                for(construct__c cons :a.constructs__r ){
                    //system.debug('Construct  record>>' + cons);
                    if(cons.Ready_to_Submit__c == false){
                        if(i==0){
                            a.Construct_Status__c = CARPOL_Constants.YET_TO_ADD;
                            //a.status__C = 'Saved'; // KK Commented as Line Item trigger will take care of line item status based on related record statuses
                            listlineitemToUpdate.add(a);
                        }
                        i++;
                        
                    }
                    if(cons.Status__c == 'Review Complete' && cons.Corrections_Required__c != null){
                        cons.Corrections_Required__c='';
                        constructUpdate.add(cons);
                        
                    }
                }
                if(i==0){a.Construct_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
                         listlineitemToUpdate.add(a);}
            }
        }
        if(listlineitemToUpdate.size()>0){
            update listlineitemToUpdate;
        }     
        if (!constructUpdate.isEmpty()){
            update constructUpdate;
        }
        
    }
    
    //Not seeing this method being used/called from anywhere
    //This Method may need to removed
    public static void processConstructReadinessForUpdate(List<Construct__c> constructList){
        list<ID> BRSlineitemlst = new list<ID>();
        list<AC__c> listlineitemToUpdate = new list<AC__c>();
        list<ID> Constructslist = new list<ID>();
        List<Construct__c> constructUpdate = new List<Construct__c>();
        for(Construct__c con:constructList)
        {
            BRSlineitemlst.add(con.Line_Item__c); 
            Constructslist.add(con.id);
        }
        //AC__c a = [SELECT ID,RecordType.Name,Construct_Status__c,Status__c,(select id,Total_No_of_PhenoTypes__c,Total_No_of_GenoTypes__c,Status__c,Corrections_Required__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst FOR UPDATE];
        for(AC__c a : [SELECT ID,RecordType.Name,Construct_Status__c,Status__c,(select id,Total_No_of_PhenoTypes__c,Total_No_of_Genotypes__c,Status__c,Corrections_Required__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst FOR UPDATE])     
        {
            Integer i=0;
            integer constructCount = 0;
            for(construct__c c: a.Constructs__r){
                constructCount ++; 
            }
            // if(a.Constructs__r.size() > 0){ // Commented to hanlde error related to W-037205
            if(constructCount > 0){ 
                for(construct__c cons :a.constructs__r ){
                    if(cons.Status__c == 'Review Complete' && cons.Corrections_Required__c != null){
                        cons.Corrections_Required__c='';
                        constructUpdate.add(cons);
                        
                    }
                    if(cons.Total_No_of_PhenoTypes__c>0 && cons.Total_No_of_Genotypes__c>0)
                    {  
                        i++;
                    }
                    
                }
                
                //  if(a.constructs__r.size()==i){ // Commented to hanlde error related to W-037205
                if(constructCount > 0){ 
                    if( (a.Status__c =='Saved' || a.Status__c =='Waiting on Customer') && a.status__c !='Submitted') {
                        a.Construct_Status__c = 'Ready to Submit';
                        listlineitemToUpdate.add(a);
                    }
                    
                }
                if(i==0)
                {  
                    a.Construct_Status__c = 'Yet to Add';
                    //a.Status__c ='Saved'; // KK Commented as Line Item trigger will take care of line item status based on related record statuses
                    listlineitemToUpdate.add(a);
                }
            }
        }
        if(listlineitemToUpdate.size()>0){
            update listlineitemToUpdate;
        }     
        if (!constructUpdate.isEmpty()){
            update constructUpdate;
        }
    }
    
    public static void resetConstructReadiness(List<Construct__c> constructList){ 
        list<string> lstappids = new list<string>();
        list<AC__c> listlineitemToUpdate = new list<AC__c>();
        Set<id> conIds = new Set<id>();
        Set<id> lineitemIds = new Set<id>();        
        for(Construct__c l:constructList){
          if(l.Line_Item__c!=null && !conids.Contains(l.id)){ //KA:W-037698
                lstappids.add(l.Line_Item__c); //x
                conIds.add(l.id);//Y  con, sop, line item
            }
        }
        
        List<AC__c> LineItemList = [select id,Construct_Status__c, (select id,Ready_to_Submit__c from Constructs__r), 
                                    (select id,construct__c from Link_Construct_Design_Protocol_Records__r
                                     where construct__c != null) 
                                    from AC__c 
                                    where id in : lstappids];
            
        for (AC__c a : LineItemList) {
            if(a.Constructs__r.size() > 0){
                Integer i=0;
                for(construct__c cons :a.constructs__r ){
                    if(cons.Ready_to_Submit__c == false){
                        i++;
                        a.Construct_Status__c = CARPOL_Constants.YET_TO_ADD;
                        //KA:W-037698
                        if(!lineitemIds.Contains(a.id)){
                        listlineitemToUpdate.add(a);
                        lineitemIds.add(a.id); 
                        }
                    }
                }
                if(i==0){a.Construct_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
                         listlineitemToUpdate.add(a);}
            }
            else if(a.Constructs__r.size() == 0){ 
                if(a.Link_Construct_Design_Protocol_Records__r.size() == 0){
                    a.Construct_Status__c = CARPOL_Constants.YET_TO_ADD;
                }else{
                    a.Construct_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
                }
                listlineitemToUpdate.add(a);
            }
        }
        
        if(listlineitemToUpdate.size()>0){
           update listlineitemToUpdate;
        }  
    }
    
}