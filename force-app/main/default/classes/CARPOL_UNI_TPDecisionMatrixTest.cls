@isTest(seealldata=false)
private class CARPOL_UNI_TPDecisionMatrixTest {
    static testMethod void testCARPOL_UNI_TPDecisionMatrix() {
    
          
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Country__c cntryaf = testData.newcountryaf();
        Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
        Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
        Group__c grp = testData.newgroup();      
        objdm.Country__c = cntryaf.id;
        objdm.Program_Line_Item_Pathway__c = plip.id;
        objdm.Regulated_Article__c = regart.id;
        objdm.Criteria_Group__c = grp.id;
        insert objdm;

        
        Signature__c newsig = testData.newThumbprint();
        Regulation__c newreg1 = testData.newRegulation('Import Requirements','Test');
        Regulation__c newreg2 = testData.newRegulation('Additional Information','Test');        
        Signature_Regulation_Junction__c srj1 = testData.newSRJ(newsig.id,newreg1.id);
        Signature_Regulation_Junction__c srj2 = testData.newSRJ(newsig.id,newreg2.id);        
        
        CARPOL_UNI_TPDecisionMatrix extclass = new CARPOL_UNI_TPDecisionMatrix();
        extclass.checkForDecisionMatrix(cntryaf.id,regArt.Id,plip.id,'Afghanistan');
        extclass.checkForDecisionMatrixWithGroup(cntryaf.id,regArt.Id,plip.id,grp.id,'Afghanistan');
        system.assert(extclass != null);
    }
   }