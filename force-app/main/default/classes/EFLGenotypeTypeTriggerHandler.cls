public with sharing class EFLGenotypeTypeTriggerHandler {
    // Validate Required Fields 
    public static void ValidateRequiredFields(List<GenotypeType__c> genotypeTypes){
        
       for(GenotypeType__c genoRec:genotypeTypes)
        {
 
            if(genoRec.Genotype_Category__c  =='' || genoRec.Genotype_Category__c  ==NULL) {
                genoRec.Genotype_Category__c.adderror('You must enter a value.');
            }
            
         }         
        
    }
   
    //Set "Ready to Submit" flag for Constructs when a new Genotype and Construct Component is created
    public static void processConstructReadiness(List<GenotypeType__c> genotypeList){
         list<Construct__c> constructList = new list<Construct__c>();
         set<ID> constructSetIDs =  prepareConstructSetIds(genotypeList);
         list<Construct__c> inCompleteconstructList = getIncompleteConstructs(constructSetIDs);
        system.debug('inCompleteconstructList@@'+ inCompleteconstructList);
        for(Construct__c cons:inCompleteconstructList){
            if(cons.GenotypeType__r.size() > 0 && cons.PhenoTypes__r.size()>0){
                
            //if(cons.GenotypeType__r.size() == 0 && cons.PhenoTypes__r.size()>0 && cons.Ready_to_Submit__c != true){
                system.debug('Inside inCompleteconstructList');
                cons.Ready_to_Submit__c= true;
                constructList.add(cons);  
            //} else {
            } else if (cons.Ready_to_Submit__c != false) {
                cons.Ready_to_Submit__c= false;
                constructList.add(cons); 
            }
        }
         if(constructList!=NULL){
             system.debug('constructList@@'+ constructList);
             update constructList;  }                
      }
    
    //Reset "Ready to Submit" flag for Constructs when a Genotype and Construct Components are deleted
     public static void resetConstructReadiness(List<GenotypeType__c> genotypeList){ 
         list<Construct__c> constructList = new list<Construct__c>();
         set<ID> constructSetIDs =  prepareConstructSetIds(genotypeList);
         list<Construct__c> relatedconstructList = getRelatedConstructs(constructSetIDs);
         system.debug('relatedconstructList@@'+ relatedconstructList);
        for(Construct__c cons:relatedconstructList){
            //if(cons.GenotypeType__r.size() == 0){
            if(cons.GenotypeType__r.size() == 0 && cons.Ready_to_Submit__c != false){
                cons.Ready_to_Submit__c= false;
                constructList.add(cons); 
            //}else{
            }else if(cons.Ready_to_Submit__c != true){
                cons.Ready_to_Submit__c= true;
                constructList.add(cons); 
            }
        }
         if(constructList!=NULL){
             system.debug('constructListGenoTypeType@@'+ constructList);
            Update constructList;  }                
      }
   
    //Prepare incomplete constructs to be processed for flag update
    private static list<Construct__c> getIncompleteConstructs(set<ID> constructSetIDs){
        return[select Id, 
                      Ready_to_Submit__c,
               (select id 
                  from GenotypeType__r
                //where Ready_to_Submit__c =: false
               ),
               (select id 
                  from PhenoTypes__r)
               from Construct__c
                where Id in :constructSetIDs FOR UPDATE]; 
    }
    
    //Prepare related constructs to be processed for flag update
    private static list<Construct__c> getRelatedConstructs(set<ID> constructSetIDs){
        return[select Id, 
                      Ready_to_Submit__c, 
               (select id 
                 from GenotypeType__r)
                from Construct__c
               where Id in :constructSetIDs FOR UPDATE]; 
    }    
    
    //Prepare Construct set Ids from genotype List
    private static set<ID> prepareConstructSetIds (List<GenotypeType__c> genotypeList){
         set<ID> constructSetIDs = new set<ID>();
         for(GenotypeType__c cc : genotypeList){
            constructSetIDs.add(cc.Construct__c); 
         }        
        return constructSetIDs;
    }
      
}