public without sharing class EFLLabelExpirySchedule implements Schedulable{

    public void execute(SchedulableContext sc){
        //system.debug('EXECUTING LABEL BATCH');
        EFLLabelExpiryBatch b = new EFLLabelExpiryBatch();
        Database.executeBatch(b);
    }
}