/*
Admins to Add Applicant User to thier Account
*/

public without sharing class EFLAddUser {
    
    //Variables
    public list<userWrapper> userWrappers {get; set;}
    public list<string> usernames {get; set;}
    public List<Contact> ContactsUsersAccountRelationsByUserNames {get; set;} 
    public map<string, Contact> usernameContactMap {get; set;}
    public map<string, string> validatedUserNames {get; set;}
    private Id parentAccountId = eflUserUtility.contactDetails.AccountId;
    public Id myAccountId{
        get
        {
            string paramAccountId = EFLGenericUtility.sanitizeString(apexpages.currentpage().getparameters().get('accId'));
            if( String.isEmpty(paramAccountId))
                return parentAccountId;
            else
                return paramAccountId;
        }
        set;}
    
    //Returns if the current account is a parent account or a child
    //If the current account is same as the logged in user's(admin) account 
    //it is considered as parent account.
    private boolean isChildAccount{
        get{
            if( String.isEmpty(myAccountId))
                return false;
            else if (myAccountId == parentAccountId)
                return false;
            else
                return true;
        }
    }
    //public list<contact> validUsernameContacts {get; set;}
    public list<AccountContactRelation> validACRs {get; set;}
    
    //constructor         
    public EFLAddUser()
    {
        initiateUserWrapper();
    }
    
    //init UI data wrapper         
    public void initiateUserWrapper()
    {
        userWrappers = new list<userWrapper>();
        userWrappers.add(new userWrapper('',''));
    }
    
    //Add username element on UI
    public void add()
    {
        if(userWrappers!=null && userWrappers.size()<100)
        {
            userWrappers.add(new userWrapper('',''));
        }
    }
    
    //Remove the last username element on UI
    public void remove()
    {
        if(userWrappers!=null && userWrappers.size()>1)
        {
            userWrappers.remove(userWrappers.size()-1);
        }
    }
    
    //Add User to Admin Account
    public pagereference addUser()
    {
        System.debug(LoggingLevel.INFO,'Inside the addUser()');
        try
        {  
            //Getting Usernames
            usernames = new list<string>();
            for(userWrapper uw:userWrappers)
            {
                usernames.add(uw.usernameLowerCase);       
            }
            
            //Getting Contact-User-AccountContactRelation List for usernames
            ContactsUsersAccountRelationsByUserNames = new List<Contact>();
            ContactsUsersAccountRelationsByUserNames = EFLContactRepository.selectContactsUsersAccountRelationByUserNames(usernames);
            usernameContactMap = new Map<string, contact>();
            for(contact c: ContactsUsersAccountRelationsByUserNames)
            {
                if(c.users!=null)
                {
                    usernameContactMap.put(c.users[0].username.toLowercase(), c);
                }    
            }
            
            //Validate username wrapper list data from UI
            integer errorCounter = 0;
            validatedUserNames = new Map<string, string>();
            //validUsernameContacts = new list<contact>(); 
            validACRs = new list<AccountContactRelation>();
            for(userWrapper uw:userWrappers)
            {
                System.debug(LoggingLevel.INFO,'uw: '+uw+ ':::valid: '+validateEmail(uw.usernameLowerCase));
                if(validateEmail(uw.usernameLowerCase)!='')
                {
                    errorCounter += 1;
                    uw.error = validateEmail(uw.usernameLowerCase);
                }
                else
                {
                    uw.error = ''; 
                    //W-037177 - Instead of adding org employee as related contact, flip relationship to set employee as direct contact to org account and 
                    //create ACR relationship for the users' personal contact/account
                    Contact c = usernameContactMap.get(uw.usernameLowerCase);
                    if (!isChildAccount){ //assumption: personal contact is only directly related to personal account
                        //reparent contact to parent account                         
                        validACRs.add(new AccountContactRelation(AccountId = c.AccountId, ContactId = c.id,Personal__c=true));
                        usernameContactMap.get(uw.usernameLowerCase).AccountId = myAccountId;
                    } else {
                        validACRs.add(new AccountContactRelation(AccountId = myAccountId, ContactId = c.id));
                    }
                }
                if(uw.usernameLowerCase!=null)
                {
                    validatedUserNames.put(uw.usernameLowerCase, uw.usernameLowerCase);
                }    
            }
            //No error - to go through and add success elements
            if(errorCounter==0)
            {
                //if(!validUsernameContacts.isempty())
                if(!validACRs.isEmpty())
                {
                    update usernameContactMap.values();
                    insert validACRs;                    
                    
                    //reset wrapper bound to UI
                    initiateUserWrapper();
                    
                    // Redirect to Account's edit page if it came from there
                    string isEditAccount = EFLGenericUtility.sanitizeString(apexpages.currentpage().getparameters().get('accId'));
                    if ( isEditAccount != null && string.isNotEmpty(isEditAccount) ){
                        PageReference redirectPage = new PageReference('/EFLEditAccount?accId=' + isEditAccount );
                        return redirectPage;
                    }
                    else{
                        //add success message
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, EFLGenericUtility.getMessage('EFLAddUserSuccessMessage')));
                        return null;
                    }
                }
            }
        } 
        catch(system.exception ex)
        {
            EFLErrorLog.createErrorLog('EFLAddUser.addUser()',ex);
        }  
        return null;
    }
    
    //Validate username
    //Validated in the order of required, repeated username entry, user does not exist, user already exist on Admin Account 
    public string validateEmail(string usernameLowerCase)
    {
        string result = '';
        
        //Required validation
        if(usernameLowerCase=='')
        {
            result = EFLGenericUtility.getMessage('EFLRequiredField');
        } 
        else
        {
            //repeated username entry
            if(validatedUserNames.get(usernameLowerCase)!=null)
            {
                result = EFLGenericUtility.getMessage('EFLAddUserDuplicateUserName'); 
            }
            else
            {
                //user does not exist
                if(usernameContactMap.get(usernameLowerCase)==null)
                {
                    result = EFLGenericUtility.getMessage('EFLAddUserUsernameNotExist');
                }
                else
                {
                    //User already exist on Admin Account
                    List<AccountContactRelation> acrList = new List<AccountContactRelation>();
                    if(usernameContactMap.get(usernameLowerCase).AccountContactRelations!=null)
                    {
                        acrList = usernameContactMap.get(usernameLowerCase).AccountContactRelations;
                    }
                    boolean accountRelationExist = false;
                    boolean isContactAssociatedWithParent = false;
                    for(AccountContactRelation acr:acrList)
                    {
                        if(myAccountId == acr.AccountId)
                        {
                            accountRelationExist = true; 
                        }
                        if(parentAccountId == acr.AccountId){
                            isContactAssociatedWithParent = true;
                        }
                            
                    }
                    if(accountRelationExist)
                    {
                        result = EFLGenericUtility.getMessage('EFLAddUserUserExistOnAccount');
                    }
                    else if (isChildAccount && !isContactAssociatedWithParent){
                        result = EFLGenericUtility.getMessage('EFLAddUserUserNotAssociatedWithAccount1');
                        result = result + eflUserUtility.contactDetails.Account.Name;
                        result = result + EFLGenericUtility.getMessage('EFLAddUserUserNotAssociatedWithAccount2');
                    }
                }
                
            }
        }
        
        return result;
    }
    
    public pagereference cancel()
    {
        // Redirect to Account's edit page if it came from there
        string isEditAccount = EFLGenericUtility.sanitizeString(apexpages.currentpage().getparameters().get('accId'));
        if ( isEditAccount != null && string.isNotEmpty(isEditAccount) ){
            PageReference redirectPage = new PageReference('/EFLEditAccount?accId=' + isEditAccount );
            return redirectPage;
        }
        else{
            //add success message
            pagereference contactPage = page.contacts;
            return contactPage;
        }
        
    }
    
    //Wrapper class for binding it to UI - Username and corresponding Error components
    public class userWrapper
    {
        //Variables
        public string username {get; set;}
        public string usernameLowerCase {
            get
            {
                if(username != null)
                {
                    return username.toLowerCase();
                }
                else
                {
                    return username;
                }
            } 
            set;}
        public string error {get; set;}
        /*public user userRecord {get; set;}
public contact contactRecord {get; set;}
public List<AccountContactRelation> AccountContactRelationRecords {get; set;}*/
        
        //Constructor
        public userWrapper(string usernameStr, string errorStr)
        {
            username =  usernameStr;
            error =  errorStr;   
        }
    }
    
}