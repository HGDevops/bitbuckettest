public class EFLGetAccountName {
	@AuraEnabled
    public static String getAccountName(){
     Id conId = [select id,contactId from user where Id=:Userinfo.getUserId()].contactId; 
     if(!Test.isRunningTest())
     	return  [select account.name from contact where Id=:conId].account.name;  
     else
        return null;
    }
}