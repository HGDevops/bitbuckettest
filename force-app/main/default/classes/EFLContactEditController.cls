public without sharing class EFLContactEditController {

    @AuraEnabled public static PageData getPageData(Id contactId){
        PageData pd = new PageData();
        if(contactId != null){
            Contact c = [SELECT AccountId FROM Contact WHERE Id = :contactId LIMIT 1];
            pd.accountId = c.AccountId;
        }else{
            User myUser = [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            pd.accountId = myUser.Contact.AccountId;
        }
        pd.adminDetails = getCanChangeAdmin(contactId, pd.accountId);
        pd.roleOptions = EFLUtils.getPicklistOptions('Contact','EFL_Role__c');
        if(contactId != null){
            pd.contactRecord = [SELECT Id, FirstName, LastName, Email, Phone, EFL_Facility_Admin__c, EFL_Role__c, EFL_Send_Welcome_Email__c 
                                FROM Contact 
                                WHERE Id = :contactId];
        }else{
            pd.contactRecord = new Contact(AccountId = pd.accountId);
        }
        return pd;
    }
    
    @AuraEnabled public static void saveContact(Contact contactRecord){
        system.debug('contactRecord: ' + contactRecord);
        if(contactRecord.Id == null){
            Id rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'AC_R_L_Contact' LIMIT 1].Id;
            contactRecord.RecordTypeId = rtId;
        }
        upsert contactRecord;
    }
    
    @AuraEnabled public static AdminDetails getCanChangeAdmin(Id contactId, Id accountId){
        User myUser = [SELECT ContactId, Contact.EFL_ACIS_Contact__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        AdminDetails ad = new AdminDetails();
        ad.numAdmins = getAdminsForAccount(accountId);
        if(contactId == null){
            ad.isOwnContact = false;
        }else{
        	ad.isOwnContact = contactId == myUser.Contact.EFL_ACIS_Contact__c;
        }
        return ad;
    }
    
    @TestVisible
    private static integer getAdminsForAccount(Id accountId){
        return [SELECT Count() FROM Contact WHERE AccountId = : accountId AND EFL_Facility_Admin__c = TRUE AND EFL_ACIS_Contact__c = null];
    }
    
    public class AdminDetails{
        @AuraEnabled public integer numAdmins{get;set;}
        @AuraEnabled public boolean isOwnContact{get;set;}
        public adminDetails(){}
    }
    
    public class PageData{
        @AuraEnabled public AdminDetails adminDetails{get;set;}
        @AuraEnabled public list<EFLUtils.LightningPicklistOption> roleOptions{get;set;}
        @AuraEnabled public Id accountId{get;set;}
        @AuraEnabled public Contact contactRecord{get;set;}
    }
}