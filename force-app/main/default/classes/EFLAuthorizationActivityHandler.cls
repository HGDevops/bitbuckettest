public with sharing class EFLAuthorizationActivityHandler 
                    implements EFLIActivityInterface
{
    
    public void populateActivityRecords(sObject parentRecord,list<sObject> activityList ) 
    { 
        
                    authorizations__c authRecord = (authorizations__c)parentRecord;
                    EFLIAuthorizationEngine engine = EFLAuthorizationEngineFactory.getEngine(authRecord);
        			system.debug('engine '+engine);
                    engine.loadActivities(authRecord, activityList);  
        			system.debug('authRecord '+authRecord);
       				 system.debug('activityList '+activityList);
        			
        
      /*  list<team__c> teamMembersList = new list<team__c>();
        authorizations__c authrecord = (authorizations__c)parentRecord;
 
        teamMembersList = EFLTeamRepository.selectByAuthorizationID(parentRecord.ID); 
        ID ownerID;

        if(!teamMembersList.isEmpty()){
         
            Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(authrecord.stage__c, authrecord.Workflow_Number__c,1);
            for(team__c t : teamMembersList){
                if(currentActivityCMD.Assigned_To__c == t.member_role__c){
                   ownerID = t.Member__c;
                }
            } 
            if(ownerID == null){ 
                throw new activityException('Please assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
            }
            activityList.add(EFLActivityUtility.populateTask( parentRecord, currentActivityCMD, ownerID ));
             
        }else{
            throw new activityException('Please assign Team Members.');
        }
        
        return null; */
    }
    
   public class activityException extends Exception {}

  

}