public inherited sharing class EFLregarticleinformationTriggerHandler {
    
  /*  public static void validateFieldFormatAndRequired(List<RegulatedArticle_Information__c> RAINFList){           
      
        string requiredMessage = 'You must enter a value';
        
        for (RegulatedArticle_Information__c info: RAINFList){
            
            //Required Field Validations
            if (info.Date_of_Birth__c == null){
               info.Date_of_Birth__c.addError(requiredMessage);
            }
            if (info.Color__c== null){
               info.Color__c.addError(requiredMessage);
            }
            if (info.Breed__c== null){
               info.Breed__c.addError(requiredMessage);
            }
            if (info.Sex__c== null){
               info.Sex__c.addError(requiredMessage);
            }
                   
            
        }
    } */
    
    public static void ACRegulatedArticleInformationProcessReadiness(List<RegulatedArticle_Information__c> raiList)
    {
    	set<Id> lineItemIds = new set<Id>();
        for(RegulatedArticle_Information__c rai : raiList)
        {
                lineItemIds.add(rai.Line_Item__c);
        }
        
        if(!lineItemIds.isEmpty())
        {
            List<AC__c> LineItemsToUpdate = new List<AC__c>();
            List<AC__c> LineItemsToDocumentStatusUpdate = new List<AC__c>();
            List<AC__c> LineItemsWithChildren = new List<AC__c>();
            LineItemsWithChildren = [select id, Regulated_Article_Status__c, Document_Status__c,
                                     (select id, Name, Document_Status__c from RegulatedArticle_Information__r) 
                                     from AC__c where Id in :lineItemIds];
            for(AC__c lineItem : LineItemsWithChildren)
            {
                boolean hasRAI = false;
                boolean hasDocument = true;
                if(!lineItem.RegulatedArticle_Information__r.isEmpty())
                {
                    hasRAI = true;
                    for(RegulatedArticle_Information__c rai : lineItem.RegulatedArticle_Information__r)
                    {
                        if(rai.Document_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
                        {
                            hasDocument = false;
                        }
                    }
                }
                else
                {
                    hasDocument = false;
                }
                
                if(hasRAI==true)
                {
                    if(lineItem.Regulated_Article_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
                    {
                    	LineItemsToUpdate.add(new AC__c(id=lineItem.Id, Regulated_Article_Status__c=CARPOL_Constants.READY_TO_SUBMIT));    
                    }
                }
                else
                {
                 	if(lineItem.Regulated_Article_Status__c!=CARPOL_Constants.YET_TO_ADD)
                    {
                        LineItemsToUpdate.add(new AC__c(id=lineItem.Id, Regulated_Article_Status__c=CARPOL_Constants.YET_TO_ADD)); 
                    }
                }
                if(hasDocument==true)
                {
                    if(lineItem.Document_Status__c!=CARPOL_Constants.READY_TO_SUBMIT)
                    {
                    	LineItemsToDocumentStatusUpdate.add(new AC__c(id=lineItem.Id, Document_Status__c=CARPOL_Constants.READY_TO_SUBMIT));    
                    }
                }
                else
                {
                    if(lineItem.Document_Status__c!=CARPOL_Constants.YET_TO_ADD)
                    {
                    	LineItemsToDocumentStatusUpdate.add(new AC__c(id=lineItem.Id, Document_Status__c=CARPOL_Constants.YET_TO_ADD));    
                    }
                }
            }
            if(!LineItemsToUpdate.isEmpty())
            {
                update LineItemsToUpdate;
            }
            if(!LineItemsToDocumentStatusUpdate.isEmpty())
            {
                update LineItemsToDocumentStatusUpdate;
            }
        }    
    }
    
}