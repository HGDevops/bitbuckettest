// Test Class for InspectionPlantingInformationCtrl class
// Created By - Rajesh Potla 
// Date - 5/7/2019
@isTest
private class InspectionPlantingInformationCtrl_Test {
	
    @testsetup
    static void setupData(){
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        
        String InsRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
    	Inspection__c oInspection = new Inspection__c(RecordTypeId = InsRecTypeID);
		insert oInspection;
        
        Application__c oApplication = testData.newapplication();
        Authorizations__c oAuth = testData.newAuth(oApplication.Id); 
        AC__c oLineItem = testData.newLineItem('Personal Use',oApplication);
        oLineItem.Release_Start_Date__c=Date.today();
        oLineItem.Release_end_Date__c=date.today()+30;
        update oLineItem;
        Country__c USCountry = testData.newcountryus();
        Level_1_Region__c USState = testData.newlevel1region(USCountry.id);
        Level_2_Region__c USCounty= testData.newlevel2region(USState.id);
        Location__c oLocation=testData.newlocation(USCountry.Id,USState.Id,USCounty.Id,oLineItem.id,Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId());
        
        Report_Summary__c oSummary = new Report_Summary__c(Authorization__c=oAuth.Id,Due_Date__c = date.today() + 60,Report_Type__c = 'Volunteer Monitoring Report');
        insert oSummary;
      
        Self_Reporting__c oSelfReport= new Self_Reporting__c(Final_Volunteer_Monitoring_Report__c = 'No',report_summary__c = oSummary.id,Release_Record_ID__c = oLocation.id,Authorization__c = oAuth.id,Monitoring_Period_Start__c = date.today()-1,Start_Date__c = date.today(),Monitoring_Period_End__c = date.today(),Observation_Date__c = date.today(),Number_of_Volunteers__c = 5,Anticipated_Harvest_Destruct_Date__c=Date.today()+90);
        insert oSelfReport;
          
        EFL_Inspection_Questionnaire__c Questionnaire = new EFL_Inspection_Questionnaire__c (Inspection__c = oInspection.Id,RecordTypeId =  Schema.SObjectType.EFL_Inspection_Questionnaire__c.getRecordTypeInfosByName().get('Compliance officer/staff').getRecordTypeId());
        Questionnaire.Location__c = oLocation.ID;
        Questionnaire.Planting_ID__c = oSelfReport.Id;
        insert Questionnaire;
       
    }
    
    @isTest
    static void runTest(){
        Inspection__c oInspection = [Select ID From Inspection__c limit 1];
        Test.startTest();
            ApexPages.currentPage().getParameters().put('Id',oInspection.Id);
            ApexPages.StandardController stdCon = new ApexPages.StandardController(oInspection);
            InspectionPlantingInformationCtrl oInspectionPlantingInformationCtrl = new InspectionPlantingInformationCtrl(stdCon);
        	Date expectedHarvestDate = Date.today()+90;
        	System.assertEquals(expectedHarvestDate.format(), oInspectionPlantingInformationCtrl.harvestdate);
        Test.stopTest();
    }
}