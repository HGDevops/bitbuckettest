@IsTest(seealldata=false)
private class EFLWizardFlagHistoryWrapper_Test{

    static testmethod void unittest(){
    
    //create all the flags we pass in to the methods
    Test.startTest();      
         Boolean bCompSelection = false;
         Boolean bRegArticle = false;
         Boolean bRegScreenCmpltd = false;
         Boolean bSearchClicked = false;
         Boolean bShowACWizard = false;
         Boolean bShowArticle = false;
         Boolean bShowCountry = false;
         Boolean bShowCountryDestn = false;
         Boolean bShowCountryExport = false;
         Boolean bShowCountrytransit = false;
         Boolean bshowNoheader = false;
         Boolean bShowPermit = false;
         Boolean bShowPermitCR = false;
         Boolean bShowPermitLD = false;
         Boolean bShowPermitLNJ = false;
         Boolean bShowPermitNPR = false;
         Boolean bShowRegion = false;
         Boolean bShowRegion2 = false;
         Boolean bShowStateDestination = false;
         Boolean firstQuestion = false;
         Boolean isChild = false;
         Boolean isPickList = false;
         Boolean isRadio = false;
         Boolean isScientfic = false;
         Boolean isSealedProduct = false;
         Boolean isShowIntendedUse = false;
         Boolean isWizardQuestion = false;
         Boolean showAdditionalInformation = false;
         Boolean showImportRequirements = false;
         Boolean ShowInfo = false;
         Boolean showLOD = false;
         Boolean showNext = false;
        
         Boolean ShowNextWizardQuestionsOne = false;
         Boolean showNJLetter = false;
         Boolean showNPRLetter = false;
         Boolean showProductType = false;
         Boolean showRegulations = false;
         Integer questionNo = 1;
         String currentQuestStr = '';  
         String optionType = '';
         String parentpathwayId = '';
         String programPathwayID = '';
         String questionId = '';
         String questionType = '';
         String selectedAnswerId = '';
         String sSelArticle = '';
         String sSelCat = '';
         String sSelComponent = '';
         String sSelCountry = '';
         String sSelCountryExport = '';
         String sSelDestState = '';
         String sSelProduct = '';
         String sSelProductType = '';
         String sSelReg1 = '';
         String sSelScient = '';
         String sSelectedLandOpt = '';
         String sSrhArticle = '';
         String selectedPortOptionsEntry = '';
         String selectedPortOptionsExit = '';        
         String sSelTransitCountry = '';
         String thumbPrintResponseID = '';
         String thumbPrintResponseType = '';
         String wizardQuestion = '';
         String wqTPId = ''; 
       
         Map<String, String> allAnswersTraceMap = new Map<String, String>();
         Map<String, String> fieldAnswerMap = new Map<String, String>();        
         Map<String, String> selectedAnswerIdMap = new Map<String, String>();
         Map<String, String> allAnswersMap = new Map<String, String>();
         List<regulated_article__c> listRSA = new List<Regulated_article__c>();
         List<Program_Line_Item_Pathway__c> progLineItemPathwayRecords = new List<Program_Line_Item_Pathway__c>();
         List<Program_Line_Item_Pathway__c> ProgramLineItem = new List<Program_Line_Item_Pathway__c>();
         List<Id> RAIdList = new List<Id>();
         List<SelectOption> selValSelectOptions = new List<SelectOption>();
         List<SelectOption> ScientificList = new List<SelectOption>();
         Set<String> setAnsTrace = new Set<String>();
         Wizard_Questionnaire__c objwizardquestion = new Wizard_Questionnaire__c();
         Wizard_Questionnaire__c wizardQARecord = new Wizard_Questionnaire__c();
         Boolean isAnswerTrace = false;
         String SelectedPathwayId = '';
         Set<Id> FinalRAGroupIds = new Set<Id>();
         Set<Id> RAGroupIds = new Set<Id>(); 
         Set<Id> CountryGroupIds = new Set<Id>();   
         

             EFLWizardFlagHistoryWrapper flagHistoryWrapper;
             flagHistoryWrapper = new EFLWizardFlagHistoryWrapper();
             flagHistoryWrapper.setWizardFlagHistory1(bCompSelection, bRegArticle, bRegScreenCmpltd, bSearchClicked, bShowACWizard, 
                    bShowArticle, bShowCountry, bShowCountryDestn, bShowCountryExport, bShowCountrytransit, 
                    bshowNoheader, bShowPermit, bShowPermitCR, bShowPermitLD, bShowPermitLNJ, bShowPermitNPR, 
                    bShowRegion, bShowRegion2, bShowStateDestination, firstQuestion, isChild, isPickList, isRadio, 
                    isScientfic, isSealedProduct, isShowIntendedUse, isWizardQuestion, showAdditionalInformation, 
                    showImportRequirements, ShowInfo, showLOD, showNext);
                    
             flagHistoryWrapper.setWizardFlagHistory2(ShowNextWizardQuestionsOne, showNJLetter, showNPRLetter, showProductType, showRegulations, questionNo, 
                    currentQuestStr,  optionType, parentpathwayId, programPathwayID, questionId, questionType, selectedAnswerId, 
                    sSelArticle, sSelCat, sSelCountry, sSelCountryExport, sSelDestState, sSelProduct, sSelProductType, 
                    sSelReg1, sSelScient, sSrhArticle, thumbPrintResponseID, thumbPrintResponseType, wizardQuestion, wqTPId,
                    sSelectedLandOpt, sSelComponent, selectedPortOptionsEntry, selectedPortOptionsExit, sSelTransitCountry);
                    
             flagHistoryWrapper.setWizardFlagHistory3(allAnswersTraceMap, fieldAnswerMap, selectedAnswerIdMap, allAnswersMap, listRSA, progLineItemPathwayRecords, ProgramLineItem, RAIdList, 
                    selValSelectOptions, ScientificList, setAnsTrace, objwizardquestion, wizardQARecord, isAnswerTrace, SelectedPathwayId, FinalRAGroupIds, RAGroupIds, CountryGroupIds);           
                    
             system.assert(flagHistoryWrapper.WizardFlagHistoryList1.size() == 1);
                    
             flagHistoryWrapper.setWizardFlagHistory1(bCompSelection, bRegArticle, bRegScreenCmpltd, bSearchClicked, bShowACWizard, 
                    bShowArticle, bShowCountry, bShowCountryDestn, bShowCountryExport, bShowCountrytransit, 
                    bshowNoheader, bShowPermit, bShowPermitCR, bShowPermitLD, bShowPermitLNJ, bShowPermitNPR, 
                    bShowRegion, bShowRegion2, bShowStateDestination, firstQuestion, isChild, isPickList, isRadio, 
                    isScientfic, isSealedProduct, isShowIntendedUse, isWizardQuestion, showAdditionalInformation, 
                    showImportRequirements, ShowInfo, showLOD, showNext);
                    
             flagHistoryWrapper.setWizardFlagHistory2(ShowNextWizardQuestionsOne, showNJLetter, showNPRLetter, showProductType, showRegulations, questionNo, 
                    currentQuestStr,  optionType, parentpathwayId, programPathwayID, questionId, questionType, selectedAnswerId, 
                    sSelArticle, sSelCat, sSelCountry, sSelCountryExport, sSelDestState, sSelProduct, sSelProductType, 
                    sSelReg1, sSelScient, sSrhArticle, thumbPrintResponseID, thumbPrintResponseType, wizardQuestion, wqTPId,
                    sSelectedLandOpt, sSelComponent, selectedPortOptionsEntry, selectedPortOptionsExit, sSelTransitCountry);
                    
             flagHistoryWrapper.setWizardFlagHistory3(allAnswersTraceMap, fieldAnswerMap, selectedAnswerIdMap, allAnswersMap, listRSA, progLineItemPathwayRecords, ProgramLineItem, RAIdList, 
                    selValSelectOptions, ScientificList, setAnsTrace, objwizardquestion, wizardQARecord, isAnswerTrace, SelectedPathwayId, FinalRAGroupIds, RAGroupIds, CountryGroupIds);                               
                    
             system.assert(flagHistoryWrapper.WizardFlagHistoryList1.size() == 2);
                                 
             flagHistoryWrapper.lastWizardHistory1 = flagHistoryWrapper.getWizardFlagHistory1();
             flagHistoryWrapper.lastWizardHistory2 = flagHistoryWrapper.getWizardFlagHistory2();
             flagHistoryWrapper.lastWizardHistory3 = flagHistoryWrapper.getWizardFlagHistory3();
             
             system.assert(flagHistoryWrapper.WizardFlagHistoryList1.size() == 1);             

         Test.stopTest();
             
    
    
    }

}