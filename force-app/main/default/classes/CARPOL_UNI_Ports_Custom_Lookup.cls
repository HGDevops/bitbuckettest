/* This class provides support for a custom lookup that displays */
/* available ports filtered by their capability mapping between  */
/* facility and regulated article (pathway or category)          */
/* Originally created 12/13/16 by VV                             */
/*Last modified: 1/4/2017                                       */
// VV 9-27-17 added capabilities logic
public with sharing class CARPOL_UNI_Ports_Custom_Lookup{
    public List<Facility__c> results{get;set;}// search results
    public Map<Integer , Facility__c> FacMap {get;set;}
    public ID RecordTypeId;
    public string type1 = 'Domestic Port';
    public string type2 = 'Foreign Port';
    public string type3 = 'Laboratory';
    public string type4 = 'Inspection Station';
    public string type5 = 'Quarantine Facility';  
    public string countryUS = 'United States of America';
    public Boolean changeHeading {get;set;}
    public string strActive='Active';
    public string strApproved='Yes';
    public string regartid{get;set;}
    public string pathwayid{get;set;}
    public CARPOL_UNI_Ports_Custom_Lookup(){        
        pathwayid = system.currentpagereference().getparameters().get('pathwayid');
        regartid =  system.currentpagereference().getparameters().get('regartid');
        string allReqCaps = getAllReqdCaps(pathwayid,regartid); //get all reqd capabilities listed on pathway, RA
        //system.debug('allReqCaps >>>>>>>>>>>'+allReqCaps );
        RecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        FacMap = new Map<Integer , Facility__c>();
        List<Facility__c> FacList = new List<Facility__c>(); 
        
        if (allReqCaps == null){ // run regular queries
            if(System.CurrentPageReference().getParameters().get('txt').contains('testidport_AppFacility')){
                changeHeading = true;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND Country__r.name = :countryUS and (type__c=:type3 OR type__c=:type4 OR type__c=:type5)  Order By Name]; //where (RecordTypeId=:RecordTypeId AND Port_Type__c!='') 
            }
            else if(System.CurrentPageReference().getParameters().get('txt').contains('testidportEntry')){
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='' AND Country__r.name = :countryUS)  Order By Name]; 
            }
            else if(System.CurrentPageReference().getParameters().get('txt').contains('testidportEmbkartion')){
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='' AND Country__r.name != :countryUS)  Order By Name]; 
            }            
            else if(System.CurrentPageReference().getParameters().get('txt').contains('therepeat')){ // VV added for domestic and foriegn ports per 11964
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='') and EFLAvailableCapabilties__c includes(:allReqCaps) Order By Name]; 
            }                        
            else{
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c where Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='' )  Order By Name]; //AND Country__r.name != :countryUS
            }
        }else{  // run queries with capabilities filter on
            if(System.CurrentPageReference().getParameters().get('txt').contains('testidport_AppFacility')){
                changeHeading = true;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND Country__r.name = :countryUS and (type__c=:type3 OR type__c=:type4 OR type__c=:type5) and EFLAvailableCapabilties__c includes(:allReqCaps) Order By Name]; //where (RecordTypeId=:RecordTypeId AND Port_Type__c!='') 
            }
            else if(System.CurrentPageReference().getParameters().get('txt').contains('testidportEntry')){
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='' AND Country__r.name = :countryUS) and EFLAvailableCapabilties__c includes(:allReqCaps) Order By Name]; 
            }
            else if(System.CurrentPageReference().getParameters().get('txt').contains('testidportEmbkartion')){
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c WHERE Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='' AND Country__r.name != :countryUS) and EFLAvailableCapabilties__c includes(:allReqCaps) Order By Name]; 
            }                       
            else{
                changeHeading = false;
                FacList = [SELECT Id, Name, Type__c ,IATA_Code__c,Port_Type__c,Country__r.Name,State_Code__c, State__c, EFLAvailableCapabilties__c FROM Facility__c where Status__c=:strActive and Approved__c =:strApproved AND (RecordTypeId=:RecordTypeId AND Port_Type__c!='') and EFLAvailableCapabilties__c includes(:allReqCaps) Order By Name]; 
            }       
        }  
        for(Integer index = 0 ; index < FacList.size() ; index++) {
            FacMap.put(index, FacList[index]);
        }
    }
    
    public string getFormTag(){
        return System.currentPageReference().getParameters().get('frm');
    }
    
    public String getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    public string getAllReqdCaps(string selpathwayid, string selregartid){
        string reqcapabilities = '';
        string finalReqcapabilities = '';     
        //get all pathways' reqd cap
        program_line_item_pathway__c currPthwy = [select id, All_Parent_Pathways__c from program_line_item_pathway__c where id =:selpathwayid ];
        List<string> pthwylist = currPthwy.All_parent_pathways__c.split(',');
        if (pthwylist.size()>0){
            for(program_line_item_pathway__c p:[select id, EFLRequiredCapabilities__c from program_line_item_pathway__c where id in:pthwylist]){
                if(p.EFLRequiredCapabilities__c!=null){
                    list<string> PthwyCaps = p.EFLRequiredCapabilities__c.split(';');
                    for (string t:PthwyCaps){
                        reqcapabilities += t+',';            
                    }
                    reqcapabilities.removeEnd(',');
                }
            }
        }
        //system.debug('>>>> reqcapabilities after pthwy = '+reqcapabilities );
        //get all RA reqd cap
        if(selregartid != ''){
            regulated_article__c ra = [select id, EFLRequiredCapabilties__c from regulated_article__c where id =:selregartid];
            if (ra != null && ra.EFLRequiredCapabilties__c != null){          
                list<string> raCaps = ra.EFLRequiredCapabilties__c.split(';');
                if (reqcapabilities!=null)
                    reqcapabilities = reqcapabilities+',';
                for (string r: raCaps){
                    reqcapabilities += r+',';
                }
                reqcapabilities.removeEnd(',');
            }
        }
        //system.debug('>>>> reqcapabilities after ra = '+reqcapabilities );
        //add them all and sort them   
        if(reqcapabilities!=''){
            list<string> listCaps = reqcapabilities.split(',');
            listCaps.sort(); 
            for(string t: listCaps){
                if(!finalreqcapabilities.contains(t))
                    finalreqcapabilities += t+ '; ';
            }        
            finalreqcapabilities = finalreqcapabilities.substring(0,finalreqcapabilities.length()-2);
            //system.debug('>>>> finalreqcapabilities = '+finalreqcapabilities );              
            return finalreqcapabilities;         
        }else{
            return null;
        }
        
    }
}