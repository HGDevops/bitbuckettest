@isTest
public class EFLLineItemUtilityTest {
    
    static CARPOL_AC_TestDataManager testACData = new CARPOL_AC_TestDataManager();
     
    static testMethod void testMoreACHappyPath() {
        testACData.insertcustomsettingsWithBRSTriggerDisabled();
        
        Application__c app = testACData.newapplicationByStatus('Submitted');
     	Authorizations__c auth = testACData.newAuthByStatus(app.id, 'Submitted');
        AC__c lineitem = testACData.newLineItemDogs('Personal Use',app, 'Submitted', auth.id);  
        
        app.Application_Submitted__c = false;
        app.Application_Status__c = 'Waiting on Customer';
        update app;
        
        EFLLineItemUtility.ACAnimalTransportationProcessReadiness(lineitem);
        EFLLineItemUtility.ACLiveDogLineItemProcessReadiness(lineitem);
        
        List<AC__c> lineItemList =  new List<AC__c>();
        lineItemList.add(lineitem);
        map<string,list<ac__c>> getMovementTypeofLineItemList = EFLLineItemUtility.getMovementTypeofLineItemList(lineItemList);
        list<ac__c> getlineItembyMovementType = EFLLineItemUtility.getlineItembyMovementType(lineItemList, 'Import');
		
        lineItem.Status__c='Saved';
		lineItem.Application_Details_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
		lineItem.Related_Contact_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
		lineItem.Regulated_Article_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
		lineItem.Document_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
        EFLLineItemUtility.ACLiveDogLineItemProcessReadiness(lineitem);
        
        lineItem.Status__c=CARPOL_Constants.READY_TO_SUBMIT;
        lineItem.Application_Details_Status__c = CARPOL_Constants.YET_TO_ADD;
		lineItem.Related_Contact_Status__c = CARPOL_Constants.YET_TO_ADD;
		lineItem.Regulated_Article_Status__c = CARPOL_Constants.YET_TO_ADD;
		lineItem.Document_Status__c = CARPOL_Constants.YET_TO_ADD;
        EFLLineItemUtility.ACLiveDogLineItemProcessReadiness(lineitem);
        
        lineItem.Transporter_Type__c=null;
        lineItem.Port_of_Entry__c=null;
        lineItem.Proposed_date_of_arrival__c=null;
        lineItem.Departure_Time__c=null;
        EFLLineItemUtility.ACAnimalTransportationProcessReadiness(lineitem);
    }
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    static testMethod void testAppLineItemLRAGenoPhenoChanges() {
        testData.insertcustomsettings();
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLChangeHistoryTrigger'; 
        chDt.Disable__c = true; 
        insert chDt; 
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticle(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticle(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
        Phenotype__c phenotype = new Phenotype__c();
        Genotype__c genotype = new Genotype__c(); 
        
        test.startTest();
        
        system.runAs(userShare)
        {
            
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            CARPOL_UNI_DisableTrigger__c LRAdt = new CARPOL_UNI_DisableTrigger__c(); 
            LRAdt.Name = 'CARPOL_BRS_Link_RegulatedTrigger'; 
            LRAdt.Disable__c = true; 
            insert LRAdt;
            LRA = new Link_Regulated_Articles__c();
            LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Draft');   
            
            CARPOL_UNI_DisableTrigger__c ConDt = new CARPOL_UNI_DisableTrigger__c(); 
            ConDt.Name = 'CARPOL_BRS_ConstructTrigger'; 
            ConDt.Disable__c = true; 
            insert ConDt;
            con = new Construct__c();
            con = testdata.newconstructByLIIdLRAId(LineItem.id, RA.Id, 'Waiting on Customer');
            
            phenotype = new Phenotype__c();
            phenotype = testdata.newphenotype(con.id);
            
            CARPOL_UNI_DisableTrigger__c gtDt = new CARPOL_UNI_DisableTrigger__c(); 
            gtDt.Name = 'CARPOL_BRS_GenotypeTrigger'; 
            gtDt.Disable__c = false; 
            insert gtDt;  
            GenotypeType__c genotypeType = new GenotypeType__c(Genotype_Category__c='Empty Transformation Vector', Construct__c=con.id);
            insert genotypeType;
            genotype = testdata.newgenotype(con.id, genotypeType);
            set<ID> constructSetIDs = new set<ID>();
            constructSetIDs.add(con.id);
            list<Construct__c> getIncompleteConstructs = EFLLineItemUtility.getIncompleteConstructs(constructSetIDs);
            list<Construct__c> processIncompleteConstructs = EFLLineItemUtility.processIncompleteConstructs(constructSetIDs);
            
        }
        
        test.stopTest();
        
    }

}