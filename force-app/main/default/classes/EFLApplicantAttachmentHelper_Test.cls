@isTest
public class EFLApplicantAttachmentHelper_Test {
     static ID SOPRecordtypeID = EFLGenericUtility.getRecordTypeId('Applicant Attachment SOP');
     static CARPOL_AC_TestDataManager dataHandler = new CARPOL_AC_TestDataManager();
    //validate file name sync functionality
    static testMethod void validateFileNames() {
            dataHandler.insertcustomsettings();
            Application__c app = dataHandler.newapplication();
            AC__c lineItem = dataHandler.newLineItem('Adoption', app);
            Applicant_Contact__c appcont = dataHandler.newappcontact();
            lineItem.Importer_Last_Name__c = appcont.Id;
            update lineItem;

           List<Applicant_Attachments__c> applicantAttachmentList = new List <Applicant_Attachments__c>();
           applicantAttachmentList.add(prepareApplicantAttachments(lineItem.ID));
           Map <id,Applicant_Attachments__c> appAttachOldMap = new Map <id,Applicant_Attachments__c>();
           appAttachOldMap.put(applicantAttachmentList[0].id,prepareApplicantAttachments(lineItem.ID));
           EFLApplicantAttachmentHelper.syncFileNames(applicantAttachmentList,appAttachOldMap);
        
        }
    //Prepare applicant attachment records with attachment 
    private static Applicant_Attachments__c prepareApplicantAttachments(ID lineItemID){
         
           Applicant_Attachments__c appAttachment = dataHandler.newAttach(lineItemID); 
           appAttachment.RecordTypeId=SOPRecordtypeID;
           appAttachment.File_Name__c='CBI File';
           appAttachment.CBI_Deleted_File_Name__c='CBI Deleted File';
           update appAttachment;
        
           attachment Attachment1 = dataHandler.newAttachment(appAttachment.ID,'.pdf');
           Attachment1.Name = appAttachment.File_Name__c;
           update Attachment1;
           attachment Attachment2 = dataHandler.newAttachment(appAttachment.ID,'.pdf');
           Attachment2.Name = appAttachment.CBI_Deleted_File_Name__c;
           update Attachment2;        
           return appAttachment;        
           
        
    }
        

}