@isTest
public class EFLContactEditController_Test {
    
    @TestSetup
    static void makeData(){
        Account account = new Account();
        account.Name = 'Test Account';
        insert account;
        
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.AccountId = account.Id;
        contact.EFL_Facility_Admin__c = true;
        insert contact;
    }

    @isTest
    public static void getCanChangeAdminForNonCommunityUser_DataReturned(){
        User communityUser = getARGuestUser();
        
        EFLContactEditController.AdminDetails ad = EFLContactEditController.getCanChangeAdmin(null, communityUser.AccountId);
        
        System.assertEquals(0, ad.numAdmins);
        System.assertEquals(false, ad.isOwnContact);
    }

    @isTest
    public static void getCanChangeAdminForOwnContact_DataReturned(){
        Contact testContact = [Select Id, AccountId From Contact Limit 1];
        User communityUser = getARGuestUser();
        
        // Set EFL_ACIS_Contact__c for the user's contact
        Contact contact = [Select Id, EFL_ACIS_Contact__c From Contact Where Id = :communityUser.ContactId];
        User currentUser = [Select Id From User Where Id = :UserInfo.getUserId() Limit 1];
        System.runAs(currentUser) {
            contact.EFL_ACIS_Contact__c = testContact.Id;
            update contact;
        }
        
        EFLContactEditController.AdminDetails ad = null;
        System.runAs(communityUser) {
			ad = EFLContactEditController.getCanChangeAdmin(testContact.Id, testContact.AccountId);
        }
        
        System.assertEquals(1, ad.numAdmins);
        System.assertEquals(true, ad.isOwnContact);
    }

    @isTest
    public static void getCanChangeAdminForOtherContact_DataReturned(){
        User communityUser = getARGuestUser();
        
        EFLContactEditController.AdminDetails ad = null;
        System.runAs(communityUser) {
			ad = EFLContactEditController.getCanChangeAdmin(communityUser.Id, communityUser.AccountId);
        }
        
        System.assertEquals(0, ad.numAdmins);
        System.assertEquals(false, ad.isOwnContact);
    }

    @isTest
    public static void getPageDataForFacilityAdmin_DataReturned(){
        Contact contact = [Select Id, AccountId From Contact Limit 1];

        EFLContactEditController.PageData pd = EFLContactEditController.getPageData(contact.Id);
        
        System.assertEquals(contact.AccountId, pd.accountId);
        System.assertEquals(1, pd.adminDetails.numAdmins);
        System.assertEquals(false, pd.adminDetails.isOwnContact);
        System.assertEquals(true, pd.contactRecord.EFL_Facility_Admin__c);
        System.assertNotEquals(0, pd.roleOptions.size());
    }
    
    @isTest
    public static void getPageDataForCommunityUser_DataReturned(){
        User communityUser = getARGuestUser();
        
        EFLContactEditController.PageData pd = null;
        System.runAs(communityUser) {
			pd = EFLContactEditController.getPageData(null);
        }
        
        System.assertNotEquals(null, pd.accountId);
        System.assertEquals(0, pd.adminDetails.numAdmins);
        System.assertEquals(false, pd.adminDetails.isOwnContact);
        System.assertEquals(false, pd.contactRecord.EFL_Facility_Admin__c);
        System.assertNotEquals(0, pd.roleOptions.size());
    }

    @isTest
    public static void saveContact_contactUpdated(){
        Contact contact = [Select Id, AccountId, EFL_Facility_Admin__c From Contact Limit 1];
        System.assertEquals(true, contact.EFL_Facility_Admin__c);
        
        contact.EFL_Facility_Admin__c = false;
        EFLContactEditController.saveContact(contact);
        
        contact = [Select Id, AccountId, EFL_Facility_Admin__c From Contact Where Id = :contact.Id Limit 1];
        System.assertEquals(false, contact.EFL_Facility_Admin__c);
    }
    
    private static User getARGuestUser(){
        return EFLACTestServices.getCommunityUser('ACIS_Community');
    }    
}