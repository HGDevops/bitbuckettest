@isTest

private class  EFLCloneApplicationController_test {
   private static testMethod void testEFLCloneApplicationController() {
          Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
         User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True AND UserRole.Name = 'ADMIN' LIMIT 1];
        system.runAs(u){
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId; 
            String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
           Domain__c pgm = testData.newProgram('BRS');
          Program_Line_Item_Pathway__c objParentPathway=new  Program_Line_Item_Pathway__c();
        objParentPathway.Program__c=pgm.Id;
        objParentPathway.Name='Live Animals';
        Insert objParentPathway;
          AC__c objac = testData.newlineitem('Resale/Adoption',objapp);
            objac.Program_Line_Item_Pathway__c = objParentPathway.id;
            update objac;
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name IN ('eFile Applicant') Limit 1].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          
          Application__c objapp2 = testData.newapplication();
            
            
          insert usershare;    
            Test.startTest(); 
           	  PageReference pageRef = Page.EFLCloneApplication ;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('Id',objapp.id);
              EFLCloneApplicationController extclass = new EFLCloneApplicationController(sc);
              extclass.cloneApplication();
              extclass.goBackApplication();
              PageReference pageRef2 = Page.EFLCloneApplication ;
              Test.setCurrentPage(pageRef2);
              ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objapp2);
              ApexPages.currentPage().getParameters().put('Id','0');
              EFLCloneApplicationController extclass2 = new EFLCloneApplicationController(sc2);
              extclass2.cloneApplication();
            Test.stopTest(); 
            
        }
    }
    
    public static testmethod void method1(){
        Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
         User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True AND UserRole.Name = 'ADMIN' LIMIT 1];
        system.runAs(u){
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newlineitem('Resale/Adoption',objapp);
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name IN ('eFile Applicant') Limit 1].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          
          Application__c objapp2 = testData.newapplication();
            
            
          insert usershare;    
            Test.startTest(); 
           	  PageReference pageRef = Page.EFLCloneApplication ;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('Id',objapp.id);
              EFLCloneApplicationController extclass = new EFLCloneApplicationController(sc);
              extclass.cloneApplication();
              extclass.goBackApplication();
              PageReference pageRef2 = Page.EFLCloneApplication ;
              Test.setCurrentPage(pageRef2);
              ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objapp2);
              ApexPages.currentPage().getParameters().put('Id','0');
              EFLCloneApplicationController extclass2 = new EFLCloneApplicationController(sc2);
              extclass2.cloneApplication();
            Test.stopTest();
    }
    }
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    static testMethod void testHappyPath() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
            
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        test.startTest();
        
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Open', 1);
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            auth = new Authorizations__c();
            auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            Application_Request__c ar = new Application_Request__c(question__c='Test Question',answer__c='Test Answer',Sequence_Number__c=1, line_Item__c=LineItem.id);
        	insert ar;    
        try{EFLCloneApplicationHandler.throwerror('errorMessage');}catch(exception ex){}
        EFLCloneApplicationHandler CloneApplicationHandler = new EFLCloneApplicationHandler();
        CloneApplicationHandler.CloneApplication(app.id, auth, 'Manual');
        test.stopTest();
        
    }
    
}