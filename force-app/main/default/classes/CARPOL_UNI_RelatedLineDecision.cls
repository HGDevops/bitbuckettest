// updated 5/8/2019 by JB.  Removing SOQL inside for-loop
public inherited sharing class CARPOL_UNI_RelatedLineDecision { 
    public static List<AC__C> insertRLD(List <AC__c> LineList) {
        List<AC__c> returnList = new List<AC__c>();
        AC__c lineitem = linelist[0];
        system.debug('enter insertRLD' + lineitem.Id);
        AC__c testlinelist = new AC__c();
        testlinelist = [SELECT ID, Program_Line_Item_Pathway__c, Program_Line_Item_Pathway__r.Name, 
                        Program_Line_Item_Pathway__r.Program__r.Name, Program_Line_Item_Pathway__r.All_Parent_Pathways__c, Authorization_Group_String__c, AC_Unique_Id__c,Arrival_Time__c,
                        Status__c, Breed__c, Country_Of_Origin__c, Country_of_Export__c, Date_of_Birth__c, DeliveryRecipient_First_Name__c, DeliveryRecipient_Last_Name__c,
                        Departure_Time__c, Dog_Out_of_Hawaii_for_Resale__c, Exporter_First_Name__c, Exporter_Last_Name__c, Final_destination_Hawaii__c, Hand_Carry__c, Will_the_Seed_be_processed_in_any_way_th__c,
                        Has_the_seed_been_genetically_modified__c, Importer_First_Name__c, Importer_Last_Name__c, Intended_Use__c, Intended_Use_for_Seeds__c, Introduction_Type__c,
                        Is_this_bird_US_originated__c, Live_Dogs_Intended_Use__c, Means_of_Importation__c,Method_of_Transport__c,Scientific_Name__c, Regulated_Article_PackageId__c, 
                        Movement_Type__c, Number_of_Labels__c, Number_of_New_Design_Protocols__c, Number_of_Release_Sites__c,
                        Parent_Facility_for_New_Facility__c,Regulated_Article_Status__c,SOP_Status__c,Construct_Status__c,Location_Status__c,Attachment_Status__c,
                        Port_of_Embarkation__c, Port_of_Entry__c, Port_of_Entry_State__c, Processed_anyway_that_change_its_nature__c,
                        Proposed_date_of_arrival__c, Proposed_End_Date__c, Proposed_Start_Date__c, Purpose_of_the_Importation__c,
                        //Quarantine_Facility__c, RA_Component_Junction__c, Move_out_Hawaii__c, Seed_be_planted_or_propagated__c,
                        Quarantine_Facility__c, Move_out_Hawaii__c, Seed_be_planted_or_propagated__c,
                        Thumbprint__c, State_Territory_of_Destination__c, Time_Zone_of_Arriving_Place__c, Time_Zone_of_Arriving__c,
                        Time_Zone_of_Departing_Place__c, Time_Zone_of_Departing__c,Will_the_seed_s_be_planted__c, Will_the_seed_s_be_processed__c,
                        Cloned_Line_Item__c,Authorization__c,Related_Contact_Status__c,Application_Number__r.Application_Type__c
                        FROM AC__c 
                        WHERE  Id =: lineitem.Id ];
        //system.debug('28 line in related line ++++++++'+testlinelist.status__c);
        returnList.add(testlinelist);                   
        Authorizations__c authrec = new Authorizations__c(); 
        if(testlinelist.Authorization__c!= null){ //Get related auth to get all line items under that auth
            authrec=[select Id,Name,Status__c,(select Id,Name,Status__c from Animal_Care_AC_Authorization__r) from Authorizations__c where Id=:testlinelist.Authorization__c];
        }
        
        if(testlinelist.status__c != 'Submitted')
        {
            String res = testlinelist.Program_Line_Item_Pathway__r.All_Parent_Pathways__c;
            string excp = testlinelist.Program_Line_Item_Pathway__r.Name;
            List<AC__C> LineItemsToReturn = new List<AC__C>();
            //W-034895 - Rajesh Potla - To prevent errors when Program_Line_Item_Pathway__r is blank.
            String[] results= new String[]{};
                if(String.isNotBlank(res)){
                    results = res.split(',');
                }
            List<Country_Junction__c> CountryJunctions = new  List<Country_Junction__c>();
            if(lineItem.Country_Of_Origin__c!=null || lineItem.Country_Of_Origin__c!=''){
                CountryJunctions = [SELECT Name, Country__c, Group__c FROM Country_Junction__c WHERE Country__c=: lineItem.Country_Of_Origin__c and RecordTypeId =: CARPOL_UNI_RecordType.getObjectRecordTypeId(Country_Junction__c.SObjectType, 'Group and Country') ];
            }
            Set <ID> CountryGroupIds = new Set<ID>();
            for(Country_Junction__c country: CountryJunctions)
            {
                CountryGroupIds.add(country.Group__c);
            }
            
            List<Related_Line_Decisions__c>  newRLDList = new List<Related_Line_Decisions__c>();
            List<Regulations_Association_Matrix__c> DecisionMatrixList = new List<Regulations_Association_Matrix__c>();
            List <Regulations_Association_Matrix__c>  dmQuery = new List <Regulations_Association_Matrix__c>();
            
            // JB Commenting out original code block & replacing w/better approach.
            /*
for(String pathway: results) {
dmQuery = [SELECT Id,
Number_of_Req_Docs__c, 
Excluded_City_Township__c,
Excluded_City_Township__r.Name ,
Excluded_Countries__c,
Excluded_State_Province__c
From Regulations_Association_Matrix__c 
where Program_Line_Item_Pathway__c = :pathway
AND (Movement_Type__c includes (:lineItem.Movement_Type__c) OR Movement_Type__c = '') 
AND (Country__c =: lineItem.Country_Of_Origin__c OR Country__c = '')
AND (State_Territory_of_Destination__c =: lineItem.State_Territory_of_Destination__c OR State_Territory_of_Destination__c = '')
AND (Export_City_Township__c =:lineitem.Exporter_Mailing_City__c OR Export_City_Township__c ='')
AND  (Export_Country__c =:lineItem.Exporter_Mailing_CountryLU__c OR Export_Country__c = '')
AND  (Export_State_Province__c =:lineItem.Exporter_Mailing_State_ProvinceLU__c OR Export_State_Province__c = '')
AND (Country_of_Export__c =:lineItem.Country_of_Export__c OR Country_of_Export__c = '')
AND  (Export_State_Province__c =:lineItem.Exporter_Mailing_State_ProvinceLU__c OR Export_State_Province__c = '')
AND  (Level_2_Region__r.Name =:lineItem.Exporter_Mailing_City__c OR Level_2_Region__r.Name = '')
AND  (Method_of_Transportation__c =:lineItem.Transporter_Type__c OR Method_of_Transportation__c = '') 
AND  (Country_Group__c IN: CountryGroupIds OR Country_Group__c='')
AND  (Regulated_Article__c=:lineItem.Scientific_Name__c OR Regulated_Article__c = '') 
AND  (Breed__c =:lineItem.Breed__c OR Breed__c = '')
AND  (Thumbprint__c =:lineItem.Thumbprint__c OR Thumbprint__c = '')
AND  (Intended_Use__c =:lineItem.Intended_Use__c OR Intended_Use__c = '')
];


DecisionMatrixList.addall(dmQuery);

}
*/
            
            // below is refactored code block to avoid SOQL in for-loop
            if(!results.isEmpty()){
                DecisionMatrixList = [SELECT Id,
                                      Number_of_Req_Docs__c, 
                                      Excluded_City_Township__c,
                                      Excluded_City_Township__r.Name ,
                                      Excluded_Countries__c,
                                      Excluded_State_Province__c
                                      From Regulations_Association_Matrix__c 
                                      where Program_Line_Item_Pathway__c IN :results
                                      AND (Movement_Type__c includes (:lineItem.Movement_Type__c) OR Movement_Type__c = '') 
                                      AND (Country__c =: lineItem.Country_Of_Origin__c OR Country__c = '')
                                      AND (State_Territory_of_Destination__c =: lineItem.State_Territory_of_Destination__c OR State_Territory_of_Destination__c = '')
                                      AND (Export_City_Township__c =:lineitem.Exporter_Mailing_City__c OR Export_City_Township__c ='')
                                      AND  (Export_Country__c =:lineItem.Exporter_Mailing_CountryLU__c OR Export_Country__c = '')
                                      AND  (Export_State_Province__c =:lineItem.Exporter_Mailing_State_ProvinceLU__c OR Export_State_Province__c = '')
                                      AND (Country_of_Export__c =:lineItem.Country_of_Export__c OR Country_of_Export__c = '')
                                      AND  (Export_State_Province__c =:lineItem.Exporter_Mailing_State_ProvinceLU__c OR Export_State_Province__c = '')
                                      AND  (Level_2_Region__r.Name =:lineItem.Exporter_Mailing_City__c OR Level_2_Region__r.Name = '')
                                      AND  (Method_of_Transportation__c =:lineItem.Transporter_Type__c OR Method_of_Transportation__c = '') 
                                      AND  (Country_Group__c IN: CountryGroupIds OR Country_Group__c='')
                                      AND  (Regulated_Article__c=:lineItem.Scientific_Name__c OR Regulated_Article__c = '') 
                                      AND  (Breed__c =:lineItem.Breed__c OR Breed__c = '')
                                      AND  (Thumbprint__c =:lineItem.Thumbprint__c OR Thumbprint__c = '')
                                      AND  (Intended_Use__c =:lineItem.Intended_Use__c OR Intended_Use__c = '')
                                     ];
            }
            // end of new code block.
            
            Set<id> DMIDs = new Set <id>();
            integer j=0;
            
            for(Regulations_Association_Matrix__c r : DecisionMatrixList) {
                DMIDs.add(r.Id);
                if((r.Excluded_City_Township__r.Name!=null) && (r.Excluded_City_Township__r.Name == lineItem.Exporter_Mailing_City__c))
                {
                    DMIDs.remove(r.Id);
                }
                
                
                if((r.Excluded_State_Province__c!=null) && (r.Excluded_State_Province__c == lineItem.State_Territory_of_Destination__c))
                {
                    DMIDs.remove(r.Id);
                }
                
            }
            
            for(Regulations_Association_Matrix__c r : DecisionMatrixList) {
                if(r.Number_of_Req_Docs__c!=0)
                {
                    j= j+r.Number_of_Req_Docs__c.intValue();
                }
                
            }

            //*******************************************************BRS CASE *********************************
            
            
            if(testlinelist.Program_Line_Item_Pathway__r.Program__r.Name == 'BRS' && testlinelist.status__c != 'Waiting on Customer')
            { 
                if( testlinelist.Construct_Status__c == 'Ready to Submit' && testlinelist.SOP_Status__c== 'Ready to Submit' && 
                   testlinelist.Regulated_Article_Status__c == 'Ready to Submit' && testlinelist.Location_Status__c== 'Ready to Submit' && testlinelist.Attachment_Status__c == 'Ready to Submit' && (testlinelist.Related_Contact_Status__c == 'Ready to Submit' || testlinelist.Related_Contact_Status__c  == NULL))
                    
                {
                    system.debug('testlinelist.Related_Contact_Status__c++' +testlinelist.Related_Contact_Status__c);
                    if (!(testlinelist.Status__c == 'Voided' || testlinelist.Status__c == 'No jurisdiction' || testlinelist.Status__c == 'Permit not required'  || testlinelist.Status__c == 'Draft' || testlinelist.status__c == 'Waiting on Customer' || testlinelist.status__c == 'Cancelled'|| testlinelist.status__c == 'Disapproved' || testlinelist.status__c == 'Withdrawn'|| (testlinelist.Application_Number__r.Application_Type__c == 'Amendment' && String.isBlank(testlinelist.Amendment_Description__c)))){ //12/18/17 VV Updated to add Withdrawn per US-17053 //9/5/2019 KA:W-038160    
                        if (linelist[0].isClone() == false) {system.debug('Ready to Submit++');
                            testlinelist.Status__c = 'Ready to Submit';
                        }
                    }
                
                }
                else 
                {
                    if(testlinelist.Status__c!='Draft') // VV added for 11321
                        testlinelist.Status__c = 'Saved';  
                    
                } 
            }
            
            
            for (ID dm: DMIDs)
            {
                newRLDList.add(new Related_Line_Decisions__c(Line_Item__c = lineItem.Id, Decision_Matrix__c = dm));
            }
            
            LineItemsToReturn.add(testlinelist);
            upsert newRLDList;
            // If Auth of current line item has only this line item, sync auth status with line item status - when PS changes line status to one of these 3 statuses                     
            if(authrec.Animal_Care_AC_Authorization__r.size() == 1){              
                if(authrec.Animal_Care_AC_Authorization__r[0].Status__c == 'Voided' || authrec.Animal_Care_AC_Authorization__r[0].Status__c == 'No jurisdiction' ||authrec.Animal_Care_AC_Authorization__r[0].Status__c == 'Permit not required' || testlinelist.status__c == 'Withdrawn'){  //12/18/17 VV Updated to add Withdrawn per US-17053    
                    authrec.Status__c = authrec.Animal_Care_AC_Authorization__r[0].Status__c; 
                    update authrec;
                }
            } 
            
            return LineItemsToReturn;
        }
        else{
            return returnList;
        }
    } 
}