public class EFLUserInvitetriggerclass{   
 
//@future(Callout = false)    
    public static void updateOldUser(set<Id> usersIdsToDeactivate, Map<Id, String> MapForFedPopulation) {
        string conid;
        user usersToDeactivate = [SELECT Id, username,isActive,contactid FROM User WHERE Id IN:usersIdsToDeactivate limit 1];
        /*for (User usr:usersToDeactivate){                  
                        usr.username = 'old'+usr.username;
                        usr.isActive = FALSE;
        }*/
        Integer len = 5; 
        String str = string.valueof(Math.abs(Crypto.getRandomLong()));
        String randomNumber = str.substring(0, len);
        Profile profList = [SELECT Id FROM Profile WHERE Name = 'Org Admin' LIMIT 1]; 
            List<User> newUsersTobeInserted  = new List<User>();
                    for (Id feds:mapForFedPopulation.keySet()){
                     // Creating new User
                    User newUser = new User();
                    CARPOL_UserInviteController.InvitedContact invite = (CARPOL_UserInviteController.InvitedContact)JSON.deserialize(MapForFedPopulation.get(feds),CARPOL_UserInviteController.InvitedContact.class);
                    conid = invite.newConId; 
                    Contact con = [select id, account.name from contact where id =:conid];
                    string account = con.account.name;                    
                    system.debug('--invite--'+invite);
                    newUser.Email = invite.email;
                    newUser.UserName = invite.firstName+'.'+invite.lastname+'@'+account+'.com';
                    newUser.UserName =newUser.UserName.replaceAll( '\\s+', '');
                    system.debug('>>>>UserName =' +newUser.UserName);
                    newUser.FirstName = invite.firstName;
                    newUser.LastName = invite.lastname;
                    newUser.isActive = TRUE;
                    newUser.ProfileId = profList.Id;
                    newUser.contactId = invite.newConId;
                    newUser.communityNickname = invite.lastname+randomNumber;
                    newUser.alias = string.valueof(invite.FirstName.substring(0,1) + invite.LastName.substring(0,1));
                    newUser.emailencodingkey = 'UTF-8';
                    newUser.languagelocalekey = 'en_US';
                    newUser.localesidkey = 'en_US';
                    newUser.timezonesidkey = 'America/Los_Angeles';
                    //newUser.FederationIdentifier = feds;
                    newUsersTobeInserted.add(newUser); 
                    } 
        //system.debug('--newUsersTobeInserted--'+newUsersTobeInserted);
        insert newUsersTobeInserted;

        //Add sharing to these users to all records        
        //first get the account name
        string accountname = [select account.name from contact where id=:ConId].account.name;
        //get the user public group
        Group gr = [SELECT Id, Name FROM Group WHERE Name =:accountname  LIMIT 1];
        //call method to add sharing for user and group
       /*for (user u:newUsersTobeInserted){
           CARPOL_ApexManagedSharing.addSharingForUserAndGroup(usersToDeactivate.id, newUsersTobeInserted[0].id, gr.id, conId);
       } */
        //call method to change contact
        updateApplication(usersToDeactivate.contactId, conId);
        // Have system send password reset email 
            try{
                for (User uC: newUsersTobeInserted){
                    System.resetPassword(uC.Id,true);
                }
            }catch(Exception e){
                System.Debug(e.getMessage());
            }    
        }        
        @future (callout=false)
        public static void updateApplication(string commconid, string partConId){
        //update contact Id from Comm contact to partner contact                        
                List<Application__c> applicationList = [SELECT Id FROM Application__c WHERE Applicant_Name__c =:commconid];
                List<Application__c> updapplicationList = new List<Application__c>() ;
                List<Authorizations__c> authorizationList = [SELECT Id FROM Authorizations__c WHERE Authorized_User__c = :commconid];
                List<Authorizations__c> updauthorizationList = new List<Authorizations__c>();
                
                for (Application__c app: applicationList ){
                    app.Authorized_User__c = partConId;
                    app.Applicant_name__c = partConId;
                    updapplicationList.add(app);        
                }
                for (Authorizations__c auth:authorizationList ){
                     auth.Authorized_User__c = partConId;
                     updauthorizationList.add(auth);  
                }
                if (updapplicationList!=null && !updapplicationList.isempty()){
                    update updapplicationList;
                }
                if (updauthorizationList!=null && !updauthorizationList.isempty()){
                    update updauthorizationList;
                }                
               }      
                
}