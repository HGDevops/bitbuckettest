public with sharing class CARPOL_VS_AddPorts {
    public Authorization_Junction__c PortJunction { get; set; }
    public List<Authorization_Junction__c> allPortsjunction { get; set; }
    public String selectedRowIndex { get; set; }
    public Integer totalPorts { get; set; }
    public Map<String, Integer> PortsMap = new Map<String, Integer>();
    public String mapKeyValue = '';
    public ID VSLineItemID;
    public Integer NumberOfPorts = 0;
    public Boolean disableRow {get; set;}
    
    public CARPOL_VS_AddPorts(ApexPages.StandardController controller) {
        
        //System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        VSLineItemID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
        allPortsjunction = new List<Authorization_Junction__c>();
        selectedRowIndex = '0';
        allPortsjunction = [SELECT ID, Port_Name__c, Method_of_Transportation__c, Mode_of_Transportation__c FROM Authorization_Junction__c WHERE VS_Line_Item__c = :VSLineItemID];
        for(Authorization_Junction__c paj : allPortsjunction)
        {
            mapKeyValue = paj.Mode_of_Transportation__c + '_' + paj.Method_of_Transportation__c + '_' + paj.Port_Name__c;
            PortsMap.put(mapKeyValue, NumberOfPorts);
            NumberOfPorts += 1;
        }
        PortJunction = new Authorization_Junction__c();
        allPortsjunction.add(PortJunction);
        NumberOfPorts = allPortsjunction.size();
        //System.Debug('<<<<<<< Total Ports : ' + NumberOfPorts + ' >>>>>>>');
    }
    
    public PageReference savePorts()
    {
        try
        {
            //System.Debug('<<<<<<< Total Ports : ' + allPortsjunction.size() + ' >>>>>>>');
            Boolean showSuccess = true;
            
            for(Authorization_Junction__c paj  : allPortsjunction)
            {
                paj.VS_Line_Item__c = VSLineItemID;
                mapKeyValue = paj.Mode_of_Transportation__c + '_' + paj.Method_of_Transportation__c + '_' + paj.Port_Name__c;
                if(PortsMap.get(mapKeyValue) == null)
                {
                    PortsMap.put(mapKeyValue, NumberOfPorts);
                    NumberOfPorts += 1;
                }
            }
            
            //System.Debug('<<<<<<< Map Size : ' + PortsMap.size() + ' >>>>>>>');
            
            //Checking if List size and Map size are same else deleting the unwanted entries
            if(PortsMap.size() > allPortsjunction.size())
            {
                PortsMap.remove('null_null_null');
            }
            
            Integer index; 
            for(String key : PortsMap.keySet())
            {
                //System.Debug('<<<<<<< Key : ' + key + ' >>>>>>>');
                if(!key.contains('null'))
                {
                    /*index = ingredientsMap.get(key);
                    //System.Debug('<<<<<<< Index : ' + index + ' >>>>>>>');
                     if(index != null)
                    {
                        allPRAjunction[index].VS_Line_Item__c = VSLineItemID;
                        upsert allPRAjunction[index];
                    }*/
                    
                }
                else
                {
                    showSuccess = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Port, Mode of Transportation, and Method of Transportation to save.'));
                }
            }
            if(showSuccess)
            {
                upsert allPortsjunction;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Information Saved Successfuly.'));
            }
        }
        catch(Exception e)
        {
            //System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administraor.'));
        }
        return null;
    }
    
    public PageReference addRow()
    {
        //System.Debug('<<<<<<< Total Ports at start : ' + allPortsjunction.size() + ' >>>>>>>');
        NumberOfPorts = allPortsjunction.size();
        if(NumberOfPorts < 75)
        {
            Authorization_Junction__c pajunction = new Authorization_Junction__c(VS_Line_Item__c = VSLineItemID);
            //pajunction.VS_Line_Item__c = VSLineItemID;
            allPortsjunction.add(pajunction);
            NumberOfPorts = allPortsjunction.size();
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can only add 75 ingredients at a time. Please save the current ingredients and repeat the process.'));
        }
        //System.Debug('<<<<<<< Total Ports : ' + NumberOfPorts + ' >>>>>>>');
        return null;
    }
    
    public PageReference removeRow()
    {
        //System.Debug('<<<<<<< Selected Row Index : ' + selectedRowIndex + ' >>>>>>>');
        try
        {
            Integer index = allPortsjunction.size();
            if(index > 1)
            {
                List<Authorization_Junction__c> delPorts = [SELECT ID, Port_Name__c, Mode_of_Transportation__c, Method_of_Transportation__c FROM Authorization_Junction__c WHERE VS_Line_Item__c = :VSLineItemID AND Port_Name__c = :allPortsjunction[Integer.valueOf(selectedRowIndex)].Port_Name__c AND Method_of_Transportation__c = :allPortsjunction[Integer.valueOf(selectedRowIndex)].Method_of_Transportation__c AND Mode_of_Transportation__c = :allPortsjunction[Integer.valueOf(selectedRowIndex)].Mode_of_Transportation__c LIMIT 1];
                if(delPorts.size() > 0)
                {
                    mapKeyValue = delPorts[0].Mode_of_Transportation__c + '_' + delPorts[0].Method_of_Transportation__c + '_' + delPorts[0].Port_Name__c;
                    PortsMap.remove(mapKeyValue);
                    delete(delPorts[0]);
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Information was removed successfuly.'));
                }
                else
                {
                    PortsMap.remove('null_null_null');
                }  
                allPortsjunction.remove(Integer.valueOf(selectedRowIndex));
                NumberOfPorts = allPortsjunction.size();
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot remove the last row.'));
            }
        }
        catch(Exception e)
        {
            //System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administrator.'));
        }
        return null;
    }

}