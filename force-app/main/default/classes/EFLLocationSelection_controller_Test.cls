@isTest
private class EFLLocationSelection_controller_Test {
    
    @testSetup
    static void initData() {
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        testDataAC.insertcustomsettingsWithBRSTriggerDisabled();
        CARPOL_UNI_DisableTrigger__c dt = new  CARPOL_UNI_DisableTrigger__c(Name = 'CARPOL_BRS_LocationsTrigger', Disable__c = false);
        Insert dt;
        Contact testContact = testDataBRS.newContact();
        Country__c testCountry = testDataBRS.newcountryus();
        Level_1_Region__c testLevelRegion1 = testDataBRS.newlevel1region(testCountry.Id);
        Level_2_Region__c testLevelRegion2 = testDataBRS.newlevel2region(testLevelRegion1.Id); 
        
        Application__c application = testDataAC.newApplication();
        AC__c lineItem = testDataAC.newLineItem('Resale',application);
    }
    
    static Location__c createLocation(String recordType) {
        Country__c testCountry = [Select Id from Country__c LIMIT 1];
        Level_1_Region__c testLevelRegion1 = [Select Id from Level_1_Region__c LIMIT 1];
        Level_2_Region__c testLevelRegion2 = [Select Id from Level_2_Region__c LIMIT 1];
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        Location__c orgloc = new Location__c(
            Name='testloc', Country__c=testCountry.Id, state__C= testLevelRegion1.Id,
            Level_2_Region__c = testLevelRegion2.Id, 
            RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId(),
            GPS_1__c = 'GPS_1__c',
            GPS_2__c = 'GPS_2__c',
            GPS_3__c = 'GPS_3__c',
            GPS_4__c = 'GPS_4__c',
            GPS_5__c = 'GPS_5__c',
            GPS_6__c = 'GPS_6__c',
            Contact_Name1__c = 'Test',
            Primary_Contact_Last_Name__c = 'LName', 
            Day_Phone__c = '555-555-1212',
            Status__c='Review Completed',
            Applicant_Instructions__c='Test Instructions',
            Unit_of_Measure_CBI__c=true,
            Material_Type_CBI__c=true,
            Primary_Country_CBI__c=true,
            Primary_State_CBI__c=true,
            Secondary_Country_CBI__c=true,
            Secondary_County_CBI__c = true,
            Secondary_State_CBI__c=true,
            Line_Item__c= lineItem.Id
        );
        return orgloc;
    }
    
    @IsTest
    static void locationSelectionTest () {
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        Application__c app = testDataBRS.newApplication();
        Authorizations__c Auth = testDataBRS.newAuth(app.id);
        AC__c lineItemz = testDataBRS.newLineItem('Release',app);
        lineItemz.Authorization__c = auth.id;
        update lineItemz;
        
        Country__c testCountry = testDataBRS.newcountrywithassoc();
        Level_1_Region__c testLevelRegion1 = testDataBRS.newlevel1region(testCountry.Id);
        Level_2_Region__c testLevelRegion2 = testDataBRS.newlevel2region(testLevelRegion1.Id);
        
        Facility__c fclity = testDataAC.newfacility('Domestic Port');
        fclity.Name = 'Testing Facility';
        update fclity;
        Inspection__c Ins = new Inspection__c();
        String InsRecordTypeId = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Test.startTest();
        List<Location__c> locs = new List<Location__c>();
        Location__c orgloc = createLocation('Origin Location');
        
        orgloc.Country__c = testCountry.id;
        orgloc.Line_Item__c = lineItemz.id;
        orgloc.State__c =  testLevelRegion1.id;
        orgloc.Level_2_Region__c = testLevelRegion2.id; 
        insert orgloc;
        locs.add(orgloc);
        Ins.RecordTypeId = InsRecordTypeId;
        Ins.Facility__c = fclity.id;
        Ins.Program__c = 'AC';
        Ins.Authorization__c = auth.id;
        insert Ins;
        EFL_Related_Record__c RR = new EFL_Related_Record__c();
        RR.Inspection__c = Ins.id;
        RR.Location__c = orgloc.id;
        insert RR;
        ApexPages.currentPage().getParameters().put('Id',Ins.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(Ins);
        EFLLocationSelection_controller cont1 = new EFLLocationSelection_controller(stdCon);       
        cont1.processSelected();
        
        ApexPages.currentPage().getParameters().put('Id',Ins.Id);
        ApexPages.StandardController stdCon2 = new ApexPages.StandardController(Ins);
        EFLLocationSelection_controller cont3 = new EFLLocationSelection_controller(stdCon2); 
        EFLLocationSelection_controller.cLocation clocvar=new EFLLocationSelection_controller.cLocation(orgloc ); 
             
        cont3.processSelected();
        InsRecordTypeId = null;
        Ins.Authorization__c = null;
        update Ins;
        ApexPages.currentPage().getParameters().put('Id',InsRecordTypeId);
        try{
            ApexPages.StandardController stdCon1 = new ApexPages.StandardController(Ins);
            EFLLocationSelection_controller cont2 = new EFLLocationSelection_controller(stdCon1);
            cont2.processSelected();
        }catch (exception e){
            System.AssertEquals(e.getMessage(), 'No Authorization associated');
        }
        Test.stopTest();
    }                    
}