public inherited sharing class EFLPreScreeningFactory {
    
    public static EFLIPreScreeningProcessor getPrescreeningProcessor(string movmentType, string pathway)
    {
        EFLIPreScreeningProcessor psp = null;
        String implementation;
        
        try{
            //Query CMD to get the name of the implementation for movement type and Program Pathway
            
            implementation = getImplementation (movmentType, pathway) ;
            if(String.isBlank(implementation)) 
            {
                implementation = getImplementation(movmentType); 
            }
            
            
            if(String.isNotBlank(implementation))
            {
                //system.debug('implementation: ' + implementation);
                Type myType=Type.forName(implementation);
                if (myType!= null)
                {
                    //system.debug('myType: ' + myType);
                    psp = (EFLIPreScreeningProcessor) myType.newInstance();
                    
                }
            }
            else
            {
                
                psp =new GenericPreScreeningProcessor();
                //system.debug('psp:' + psp);
            }
            
        }
        catch (Exception ex)
        {
            
        }
        
        return psp;
    }
    
    
    
    
    
    public static String getImplementation(string MovementType, string ProgramPathway)
    {
        String implementation;
        if(String.isNotBlank(MovementType) && String.isNotBlank(ProgramPathway))
        {
            EFLPSQImplementationReference__mdt[] result = [ SELECT Implementation__c FROM EFLPSQImplementationReference__mdt WHERE Movement_Type__c = :MovementType and Program_Pathway__c = :ProgramPathway and Interface__c = 'EFLIPreScreeningProcessor'];
            if(result != null)
            {
                implementation = result[0].Implementation__c;
            }
        }        
        
        return implementation;
    }
    
    public static String getImplementation(string MovementType)
    {
        String implementation;
        
        if(String.isNotBlank(MovementType))
        {
            EFLPSQImplementationReference__mdt[] result = [SELECT Implementation__c FROM EFLPSQImplementationReference__mdt WHERE Movement_Type__c = :MovementType and Interface__c = 'EFLIPreScreeningProcessor'];
            if(result != null)
            {
                implementation = result[0].Implementation__c;
            }
        }    
        
        return implementation;
    }
    
}