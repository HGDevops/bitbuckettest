public with sharing class EFLVSAuthorizationEngine implements EFLIAuthorizationEngine {
	/*
	* Purpose: change authorization record before inserting
	*/
	public void createAuthorization(authorizations__c authRecord){

	}

	/*
	* Purpose: validate Authorization record
	*/
	public boolean validate(authorizations__c authRecordOld, authorizations__c authRecord) {
		return EFLAuthorizationEngineUtility.validate(authRecordOld, authRecord); 
	}

	/*
	* Purpose: update Authorization record
	*/
	public void updateAuthorization(authorizations__c authRecordOld, authorizations__c authRecord) {
		EFLAuthorizationEngineUtility.updateAuthorization(authRecordOld, authRecord);
	}

	/*
	* Purpose: handle email communication
	*/
	public void handleCommunication(authorizations__c authRecord){
		EFLAuthorizationEngineUtility.sendEmail(authRecord);  
	}

	/*
	* Purpose: load Activities 
	*/
	public void loadActivities(authorizations__c authRecord,list<sObject> activityList){
		 EFLActivityFactory.loadActivities(authRecord, activityList);
	}

	public Application__c processAmendment(authorizations__c authRecord, String actionType){
		Application__c newApp = new Application__c();
		return newApp;
	}
}