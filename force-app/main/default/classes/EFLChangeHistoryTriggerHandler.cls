public inherited sharing class EFLChangeHistoryTriggerHandler {
    
    public static void onAfterInsert(Map<Id, Change_History__c> newChangeMap)
    {
        GenericHistoryClass.createChangeHistoryLitesByChangeIdsForInternalEdits(newChangeMap.keySet());
    }

}