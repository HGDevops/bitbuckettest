public with sharing class EFLARFileUploadController {

    @AuraEnabled public static void setupNewFileWithParams(string cds, Id parentId, string fileDetails, string fieldMappingString){
        list<FieldMapping> fieldMappings = (list<FieldMapping>)JSON.deserialize(fieldMappingString, list<FieldMapping>.class);
        
        // after a contentdocument has been created, clone its details into an uploaded_file__c record
        list<EFLFileListController.LightningFileUpload> cdUploads = (list<EFLFileListController.LightningFileUpload>)JSON.deserialize(cds, list<EFLFileListController.LightningFileUpload>.class);
        map<Id, String> cdMap = new map<Id, String>();
        for(EFLFileListController.LightningFileUpload lfu : cdUploads){
            cdMap.put(lfu.documentId, lfu.name);
        }
        list<Uploaded_File__c> ufs = EFLFileListController.makeNewFiles(cdUploads, parentId, fileDetails, cdMap);
        if(!ufs.isEmpty()){
            for(Uploaded_File__c uf : ufs){
                if(fieldMappings != null && !fieldMappings.isEmpty()){
                    for(FieldMapping m : fieldMappings){
                        if(m.fieldValue != null && m.fieldName != null){
                        	uf.put(m.fieldName, m.fieldValue);
                        }
                    }
                }
            }
            insert ufs;
        }
    }
    
    @AuraEnabled public static list<Id> getParentIds(Id arId){
    	list<Id> ids = new list<Id>();
        ids.add(arId);
        for(EFLRegistered_Animal__c s : [SELECT Id FROM EFLRegistered_Animal__c 
                                         WHERE EFLAnnual_Report__c = :arId 
                                         AND EFLSelectedForReport__c = TRUE]){
            ids.add(s.Id);
        }
        return ids; 
    }
    
    @AuraEnabled public static PageData getPageData(Id arId){
        return new PageData(arId);
    }
    
    public class PageData{
        @AuraEnabled public list<Id> parentIds{get;set;}
        @AuraEnabled public string usageKey{get;set;}
        public PageData(Id arId){
            this.parentIds = getParentIds(arId);
            this.usageKey = 'EFL AR Internal File List';
            // TODO check for perm set name here.
            list<PermissionSetAssignment> assignments = [SELECT PermissionSetId 
                                                         FROM PermissionSetAssignment 
                                                         WHERE AssigneeId= :UserInfo.getUserId() 
                                                         AND PermissionSet.Name = 'AC_R_L_Read_Only_Users'];
            if(!assignments.isEmpty()){
                this.usageKey = 'EFL AR Internal Locked';
            }
        }
    }
    
    public class FieldMapping{
        public string fieldName{get;set;}
        public string fieldValue{get;set;}
    }
}