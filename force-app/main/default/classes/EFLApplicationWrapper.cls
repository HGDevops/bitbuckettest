public with sharing class EFLApplicationWrapper {

    public Application__c application {get;set;}
    public AC__c lineItem {get;set;}
    public list<Application_Request__c> applicationRequestList {get;set;}  
    
    public EFLApplicationWrapper(){
        
       applicationRequestList = new list<Application_Request__c>(); 
    }
    
 
    
}