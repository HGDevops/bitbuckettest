/**
 * Purpose: Handles the application cloning functionality which calls the CloneFactory
 */ 
public with sharing class EFLCloneApplicationHandler {

 /**
 * @Return Parameter: Provides cloning related error messages 
 * */
    public Application__c CloneApplication(ID applicationId,authorizations__c authRecord, String ActionType) {
        // NUll or blank value or white spaces check
        if (applicationID!=null) {
            List<AC__C> newLineItemsToInsertList=new  List<AC__c>();
            List<AC__c> processLineItemsList=new List<AC__c>();
            List<id> oldLineItemIDList=new  List<id>();
            Application__c app=new Application__c();

            /** 
            * Clone # Line Items, Applicant Attachments,Attachments
            **/
            Application__c originalApplicationRecord = [SELECT Applicant_Name__c,RecordtypeId, Id,
                                                            Applicant_Email__c,Applicant_Phone__c,Sharing_Account__c,
                                                            Applicant_Fax__c,Organization__c,Applicant_Account__c,Authorized_User__c,
                                                            Application_Status__c,Applicant_Address__c,US_Address__c 
                                                        FROM Application__c 
                                                        WHERE ID=:applicationID];

                string EXTERNALID = applicationID+EFLGenericUtility.randomString();
                //Creating Application with application status as 'Open' and application type as New for same applicant
                app.Applicant_Name__c=originalApplicationRecord.Applicant_Name__c;
                app.Authorized_User__c=originalApplicationRecord.Authorized_User__c;
                app.Application_Status__c='open';
                app.RecordTypeId=originalApplicationRecord.RecordTypeId;
                app.Application_Type__c='New';
                app.ExternalID__c = EXTERNALID;
                app.RelatedApplication__c = originalApplicationRecord.Id;
                app.Applicant_Account__c = originalApplicationRecord.Applicant_Account__c;//KA:T-07663
                app.Sharing_Account__c = originalApplicationRecord.Sharing_Account__c;//KA:T-07663
                if (ActionType=='Clone Button') {
                    app.Application_Type__c='New';
                } else {
                    app.Application_Type__c = ActionType;
                    app.Renewal_Application__c = true;
                    if (authRecord != null) {
                        app.Renewal_Proposed_Start_Date__c = authRecord.Expiration_Date__c+1;
                        app.Renewal_Proposed_End_Date__c = authRecord.Expiration_Date__c.addYears(1);
                    }
                }

                Insert app;

                processLineItemsList = EFLlineItemRepository.selectbyApplicationID(applicationID); 
                list<Application_Request__c> appRequestList = [select id,question__c,answer__c,Sequence_Number__c from Application_Request__c where line_Item__c IN: processLineItemsList]; 
                application__c applicationReference = new application__c(ExternalID__c=EXTERNALID);
                List<sObject> sObjectFinalCloneList = new List<sObject>();

                //Process through each line item and clone and change each line item status and proposed date values and prepare the new line item list to Insert
                for (AC__c oldline:processLineItemsList){
                    //validate line item record
                    // validateBeforeCloning(oldline.Program_Line_Item_Pathway__r.Program__r.Name,oldline); //Ravee Racharla 12/13/2018
                    //Cloning each line item and its related records
                    List<sObject> sObjectCloneList = new List<sObject>();
                    sObjectCloneList = EFlCloneFactory.cloneRecord( oldline.Program_Line_Item_Pathway__r.Program__r.Name, ActionType, oldline, applicationReference, authRecord);
                    //prepare sObject list array
                    //Ownership Update for AC Line items
                    if(oldline.Program_Line_Item_Pathway__r.Program__r.Name=='AC')
                    {
                       SObjectType objToken = Schema.getGlobalDescribe().get('AC__c');
                       for(sObject obj: sObjectCloneList)
                       {
                           if(obj.getSObjectType() == objToken)
                           {
                               obj.put('ownerId',UserInfo.getUserId());
                           }                           
                       }
                    }
                    sObjectFinalCloneList.addAll(sObjectCloneList);
                    system.debug( 'sObjectFinalCloneList@@@@' + sObjectFinalCloneList); 
                }

                if (sObjectFinalCloneList!=null) {
                    Database.SaveResult[] results = Database.insert(sObjectFinalCloneList,true);
                        // Check results.
                        for (Integer i = 0; i < results.size(); i++) {
                            system.debug('results[i]##'+results[i]);
                            if (results[i].isSuccess()) {
                                System.debug('Successfully created ID: '+ results[i].getId());
                            } else {
                                throwerror('Error: could not create any record '+ results[i].getErrors()[0].getMessage() + '\n');
                            }
                        }
                }
            //KA: W-034173 :: Cloning | Cloned Copy does not Show PSQ Answers on App Detail Page :::
            If(appRequestList!=null){
             Application_Request__c cloneApp;
             list<Application_Request__c> cloneAppList = new list<Application_Request__c>();
             for(Application_Request__c a : appRequestList){
               cloneApp = a.clone(false,true);
               cloneApp.line_Item__c = sObjectFinalCloneList[0].id;
               cloneAppList.add(cloneApp); 
             } 
            insert cloneAppList; 
            }
            //KA: W-034173 :: ENDS :::::
            return app; 
        } else {
            return null;
        }
    }

    /* Commenting the unused Private Method - Usage on Clone Application was already existed as commented
      private void validateBeforeCloning(string program, ac__c oldLineItem) {
        if(program == 'BRS'){
            List<authorizations__c> authorizationList = [select status__c,program__c
                                                           from authorizations__c
                                                          where id =: oldLineItem.authorization__c
                                                          limit 1];

            if (authorizationList==NULL || authorizationList.size()==0) {
                    throwerror('This application cannot be cloned, as the related authorization(s) have not been issued. Please ensure the related authorization(s) are issued before attempting to clone this application.');
            } else if ( authorizationList.size()>0 && authorizationList[0].status__c != 'Issued')
                {
                    throwerror('This application cannot be cloned, as the related authorization(s) have not been issued. Please ensure the related authorization(s) are issued before attempting to clone this application.');
                }
        }
    }
	*/

    public class accessException extends Exception{}

    //Log error and Throw error message
    public static void throwerror(string errorMessage){
        throw new accessException(errorMessage);
    }
    
}