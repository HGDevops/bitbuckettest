@isTest 
public class EFLExtContactAcceptance_Test  
{
     static testMethod void acceptingUserInviteTestMethod() 
     {
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        //testAccount.isPartner = TRUE;
        //testAccount.isCustomerPortal = FALSE;
        insert testAccount;

        List<Contact> lstCon = new List<Contact>();
        
        Contact cont = new Contact ();
        cont.FirstName = 'FirstName';
        cont.LastName = 'LastName';
        cont.Email='email@email.com';
        cont.phone='1234567890';
        cont.AccountId = testAccount.id;
        lstCon.add(cont);
        
        Contact cont1 = new Contact ();
        cont1.FirstName = 'FName';
        cont1.LastName = 'LName';
        cont1.Email='em@email.com';
        cont1.Account_Admin__c = TRUE;
        cont1.phone='1267890890';
        cont1.AccountId = testAccount.id;
        lstCon.add(cont1);      
        
        insert lstCon;
        
		User u = new User();
        u = EFLUserTestDataFactory.getUser('eFile Applicant');           
          
    Profile p1 = [Select ID, Name from Profile Where Name = 'Org Admin'];
    
    User usr = new User(    
   username = 'currentuser@curr.com',
   lastname = 'currtest',
   profileId = p1.id,
    alias = 'curr',
    email = 'test' + math.random() + '@test.com',
        CommunityNickName = string.valueOf(math.random()).substring(0,6),        
        TimeZoneSidKey = 'America/New_York', 
        LocaleSidKey = 'en_US', 
        EmailEncodingKey = 'UTF-8', 
        LanguageLocaleKey = 'en_US',
        contactId = cont1.id
       );
       insert usr;
                    
        User_Invite__c ui = new User_Invite__c ();
        ui.Invitee_List__c =  '[{"userName":"a1partnernonadmin2@a1p.com","UserInviteId":null,"status":"test","oldUserId":"005r0000001K4PPAA0","newConId":null,"lastName":"nonadmin2","firstName":"a1partner","FedId":null,"email":"email@email.com"}]';
        ui.Partner_Account__c = testAccount.id;
        ui.Partner_Admin__c = cont1.id;
        insert ui;
        
        
        Test.StartTest(); 

            PageReference pageRef = Page.EFLExtContactAcceptance; // Add your VF page Name here
            pageRef.getParameters().put('accId', testAccount.id);
            pageRef.getParameters().put('firstname', cont.FirstName);
            pageRef.getParameters().put('lastname', cont.lastname);
            pageRef.getParameters().put('emailaddress', cont.Email);
            pageRef.getParameters().put('currUsername', 'currentuser@curr.com');
            pageRef.getParameters().put('userInviteId', ui.id);
            Test.setCurrentPage(pageRef);
            
            EFLExtContactAcceptance exInst = new EFLExtContactAcceptance();
            exInst.accept();
            exInst.deny();            
                
        Test.StopTest();
     }  
}