public class EFLPSQLanding {
    
    public list<SelectOption> pageOptions {get; set;}
    
    public EFLPreScreeningQuestionnaireController pageController {get; set;}
    
    public map<string, Id> pageOptionValueIdMap {get
    {
      pageOptionValueIdMap = new map<string, Id>();
      for(SelectOption opt: pageOptions)  
      {
        pageOptionValueIdMap.put(opt.getLabel().toLowercase(), opt.getValue());  
      }
      return pageOptionValueIdMap;  
    }    
    set;}

}