public inherited sharing class EFLLineItemUtility {
    
    public static string AIR_TRANSPORT = 'Air';
    
    public static map<string,list<ac__c>> getMovementTypeofLineItemList(list<ac__c> lineItemList){
        map<string,list<ac__c>> lineItemMovementMap= new map<string,list<ac__c>>();
        return lineItemMovementMap;
    } 
    
    public static list<ac__c> getlineItembyMovementType(list<ac__c> lineItemList,string movementType){
        map<string,list<ac__c>> lineItemMovementMap= new map<string,list<ac__c>>();
        lineItemMovementMap = getMovementTypeofLineItemList(lineItemList);
        list<ac__c> movementTypeLineItemList = lineItemMovementMap.get(movementType);
        return movementTypeLineItemList;
    }     
    
    //Prepare incomplete constructs to be processed for flag update
    public static list<Construct__c> getIncompleteConstructs(set<ID> constructSetIDs){
        return[select Id, 
               Ready_to_Submit__c, 
               (select id 
                from GenotypeType__r
                // where Ready_to_Submit__c =: false
               ),
               (select id 
                from PhenoTypes__r)
               from Construct__c
               where Id in :constructSetIDs]; 
    }  
    
    //process List of Contructs to be Ready for Submission
    public static list<Construct__c> processIncompleteConstructs(set<ID> constructSetIDs){
        list<Construct__c> processedConstructList = new list<Construct__c>();
        list<Construct__c> inCompleteconstructList =  getIncompleteConstructs(constructSetIDs);
        //system.debug('inCompleteconstructListPheno@@'+ inCompleteconstructList);
        for(Construct__c cons:inCompleteconstructList){
            if(cons.GenotypeType__r.size() > 0 && cons.PhenoTypes__r.size()>0){
                cons.Ready_to_Submit__c= true;
                processedConstructList.add(cons); 
            } else {
                cons.Ready_to_Submit__c= false;
                processedConstructList.add(cons); 
            }
            
        }  
        if(processedConstructList!=NULL){
            return processedConstructList;
        }
        return null;
    }   
    
    public static void ACAnimalTransportationProcessReadiness(AC__c lineItem)
    {
        boolean processReady = true;
        //Transporter_Type__c
        if(lineItem.Transporter_Type__c==null || lineItem.Transporter_Type__c=='')
        {
            processReady = false;
        }
        else
        {
            if(lineItem.Transporter_Type__c==AIR_TRANSPORT && (lineItem.Air_Transporter_Flight_Number__c==null || lineItem.Air_Transporter_Flight_Number__c==''))
            {
                processReady = false;
            }
        }
        //Port_of_Entry__c
        if(lineItem.Port_of_Entry__c==null)
        {
            processReady = false;
        }
        //Proposed_date_of_arrival__c
        if(lineItem.Proposed_date_of_arrival__c==null)
        {
            processReady = false;
        }
        //AC_Arrival_Time__c
        if(lineItem.AC_Arrival_Time__c==null || lineItem.AC_Arrival_Time__c=='')
        {
            processReady = false;
        }
        //Departure_Time__c
        if(lineItem.Departure_Time__c==null)
        {
            processReady = false;
        }
        //AC_Departure_Time__c
        if(lineItem.AC_Departure_Time__c==null || lineItem.AC_Departure_Time__c=='')
        {
            processReady = false;
        }
        if(processReady==true)
        {
            lineItem.Application_Details_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
            if(lineItem.Status__c==EFLGlobalConstants.getConstantValue('LINE ITEM STATUS DRAFT'))
            {
                lineItem.Status__c=EFLGlobalConstants.getConstantValue('LINE ITEM STATUS SAVED');
            }            
        }
        else
        {
            lineItem.Application_Details_Status__c = CARPOL_Constants.YET_TO_ADD;
        }
        
    }
    
    public static void ACLiveDogLineItemProcessReadiness(AC__c lineItem)
    {
        if(lineItem.Status__c== EFLGlobalConstants.getConstantValue('LINE ITEM STATUS SAVED'))
        {
            if(lineItem.Status__c!=CARPOL_Constants.READY_TO_SUBMIT 
               && (lineItem.Application_Details_Status__c==CARPOL_Constants.READY_TO_SUBMIT 
                   && lineItem.Related_Contact_Status__c==CARPOL_Constants.READY_TO_SUBMIT
                   && lineItem.Regulated_Article_Status__c==CARPOL_Constants.READY_TO_SUBMIT
                   && lineItem.Document_Status__c==CARPOL_Constants.READY_TO_SUBMIT)
              )
            {
                lineItem.Status__c=CARPOL_Constants.READY_TO_SUBMIT;  
            }
            else if(lineItem.Status__c==CARPOL_Constants.READY_TO_SUBMIT 
                    && (lineItem.Application_Details_Status__c==CARPOL_Constants.YET_TO_ADD 
                        || lineItem.Related_Contact_Status__c==CARPOL_Constants.YET_TO_ADD
                        || lineItem.Regulated_Article_Status__c==CARPOL_Constants.YET_TO_ADD
                        || lineItem.Document_Status__c==CARPOL_Constants.YET_TO_ADD)
                   )
            {
                lineItem.Status__c=EFLGlobalConstants.getConstantValue('LINE ITEM STATUS SAVED');
            }
        }
        else if(lineItem.Status__c==CARPOL_Constants.READY_TO_SUBMIT 
                    && (lineItem.Application_Details_Status__c==CARPOL_Constants.YET_TO_ADD 
                        || lineItem.Related_Contact_Status__c==CARPOL_Constants.YET_TO_ADD
                        || lineItem.Regulated_Article_Status__c==CARPOL_Constants.YET_TO_ADD
                        || lineItem.Document_Status__c==CARPOL_Constants.YET_TO_ADD)
               )
        {
        	lineItem.Status__c=EFLGlobalConstants.getConstantValue('LINE ITEM STATUS SAVED');    
        }
    }
    
}