public with sharing class EFLLocationSelection_controller {
    
    public Id InspectionId{get;set;}
    public Inspection__c InspRecord = new Inspection__c();
    public List<Location__c> LocationList = new List<Location__c>();
    public String AuthId;
    public List<cLocation> LocationsDisplayList {get;set;}
    public List<Location__c> selectedLocations = new List<Location__c>();
    
    
    public EFLLocationSelection_controller(ApexPages.StandardController controller) {
        InspectionId = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('id') );
        try{
            if(InspectionId!=null || InspectionId!=''){
                InspRecord = [SELECT Id,Name,Authorization__c FROM Inspection__c WHERE Id=:InspectionId LIMIT 1];
            }
            
            AuthId = InspRecord.Authorization__c;
            
            
            if(InspRecord.Authorization__c!=null || InspRecord.Authorization__c!=''){
                AuthId = InspRecord.Authorization__c;
                
                AuthId = AuthId.substring(0,AuthId.length()-3);
                LocationList = [SELECT ID,Name, State__c, Status__c, Status_Graphical__c, Inspection_Status__c FROM Location__c WHERE Authorization_Id__c =:AuthId];
                
                if(LocationList.size()==0){
                    ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Info,+'No Locations associated to this Authorization'));
                }
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Info,+'No Authorization associated'));
        } 
        
    }
    
    
    public void processSelected(){
        selectedLocations = new List<Location__c>();
        for (cLocation cq:getLocs()){
            if(cq.selected == true){
                selectedLocations.add(cq.Loc);
            }
        }
        
        List<EFL_Inspection_Questionnaire__c> inspQList = new List<EFL_Inspection_Questionnaire__c>();
        if(selectedLocations.size()>0){
            for(Location__c q: selectedLocations){
                EFL_Inspection_Questionnaire__c inspq = new EFL_Inspection_Questionnaire__c();
                inspq.Location__c = q.id;
                inspq.Status__c = 'Open';
                inspq.Inspection__c = InspectionId;
                inspQList.add(inspq);
                
                q.Inspection_Status__c = 'Initiated';
                
            }
            
            if(inspQList.size()>0){
                update selectedLocations;
                insert inspQList;
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,+'Successfully created Inspection Questionnaire records'));
                
            }
            //update selectedQuotes;
            
            
            
        }else{
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Error,+'Please select atleast 1 record to update'));
            //return null;
        }
        //QuotesDisplayList=null;
        //return null;
    }
    
    
    
    public List<cLocation> getLocs(){
        if(LocationsDisplayList==null && LocationList!=null){
            LocationsDisplayList = new List<cLocation>();
            for(Location__c q:LocationList){
                LocationsDisplayList.add(new cLocation(q));
            }
        }
        
        return LocationsDisplayList;
    }    
    
    
    public class cLocation {
        public Location__c Loc {get; set;}
        public Boolean selected {get; set;}
        
        public cLocation(Location__c c) {
            Loc = c;
            selected = false;
        }
    }
}