@IsTest
public class ViolatorLookupServiceTest { 
    @IsTest
    public static void integrationTest() {
     Domain__c objProg = new Domain__c();
     objProg.Name = 'VS';
     objProg.Active__c = true;
     insert objProg;

    Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
    objParentPathway.Program__c = objProg.Id;
    objParentPathway.Name = 'Live Animals';
    objParentPathway.Status__c = 'Active';  
    System.assert(objParentPathway != null);     
    insert objParentPathway;
      
    CARPOL_AC_TestDataManager testInst = new CARPOL_AC_TestDataManager();
    testInst.insertcustomsettings();
    Application__c objapp = testInst.newapplication();    
    AC__c ac = testInst.newLineItem('Resale/Adoption',objapp);  
    Applicant_Contact__c appcon = testInst.newappcontact();               
       Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());
       
       //Insert ITEMS record into Custom setting CARPOL URLs
       CARPOL_URLs__c Items = new CARPOL_URLs__c();
       Items.Name = 'ITEMS';
       Items.URL__c = 'https://itemsuat.aphis.edc.usda.gov/itemsApi/rest/subject/search?name='; 
       Items.Security_Key__c = '28992012111608101585641';
       insert Items ;
       
       String response = ViolatorLookupService.initiateCall('Attila', 'Bereczky');
       System.Debug(response);
       response  = null;
       System.assert(ViolatorLookupService.containsResults(response)==false);  
    }
    
    
    @IsTest
    public static void integrationTest_2() {
     Domain__c objProg = new Domain__c();
     objProg.Name = 'VS';
     objProg.Active__c = true;
     insert objProg;

    Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
    objParentPathway.Program__c = objProg.Id;
    objParentPathway.Name = 'Live Animals';
    objParentPathway.Status__c = 'Active';  
    System.assert(objParentPathway != null);     
    insert objParentPathway;
      
    CARPOL_AC_TestDataManager testInst = new CARPOL_AC_TestDataManager();
    testInst.insertcustomsettings();
    Application__c objapp = testInst.newapplication();    
    AC__c ac = testInst.newLineItem('Resale/Adoption',objapp);  
    Applicant_Contact__c appcon = testInst.newappcontact();         
    ac.Importer_Last_Name__c = null; 
    update ac;    
       Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());
       
       //Insert ITEMS record into Custom setting CARPOL URLs
       CARPOL_URLs__c Items = new CARPOL_URLs__c();
       Items.Name = 'ITEMS';
       Items.URL__c = 'https://itemsuat.aphis.edc.usda.gov/itemsApi/rest/subject/search?name=';
       Items.Security_Key__c = '28992012111608101585641';
       insert Items ;
       
       String response = ViolatorLookupService.initiateCall('Attila', 'Bereczky');
       System.Debug(response);
       response  = null;
       System.assert(ViolatorLookupService.containsResults(response)==false);  
    }
    
    @IsTest 
    public static void testParseFoundMatch() {
        String response = '{"subjectDetails":[{"exactMatch":false,"matchCriteria":null,"subjectID":151453,"subjectType":"I","name":"Attila Bereczky","address":{"street1":"99 Canal Center Plz","city":"Alexandria","state":"VA","zip":"22314","country":"USA"},"caution":null,"phoneNumber":null,"emailAddress":null,"ssn":null,"taxId":null,"dunsNumber":null,"passportNumber":null,"birthDate":null,"gender":null}],"message":"Success. Number of records returned are 1"}';
        System.assert(ViolatorLookupService.containsResults(response));  
    }
    
    @IsTest 
    public static void testParseNoMatch() {
        String response = '{"subjectDetails":[],"message":"Success. Number of records returned are 0"}';
        System.assert(!ViolatorLookupService.containsResults(response));  
    }
}