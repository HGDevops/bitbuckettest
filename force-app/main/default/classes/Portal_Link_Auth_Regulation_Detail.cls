public with sharing class Portal_Link_Auth_Regulation_Detail {

    public Id linkauthID{get;set;}
    
     public Application_Condition__c  condition {get;set;}
     public Id Id{get;set;}
     List<ConditionWrapper> conditionsList = new List<ConditionWrapper>();
     List<Application_Condition__c > selectedConditions = new List<Application_Condition__c >();


    public Portal_Link_Auth_Regulation_Detail(ApexPages.StandardController controller) {

     linkauthID=ApexPages.currentPage().getParameters().get('id');
     Application_Condition__c  condition  = new Application_Condition__c();
    }

    /* public List<Application_Condition__c> getSupConditions(){
     return [SELECT ID, Name, Regulation_Title__c, Condition_Number__c, Condition__c, Agree__c, Applicant_Comments__c FROM Application_Condition__c WHERE Link_Authorization_Regulation__c =: linkauthID];
   } */
 
    
      
     public  List<ConditionWrapper> getConditions(){
    
        for(Application_Condition__c  c: [SELECT ID, Name, Regulation_Title__c, Condition_Number__c, Condition__c, Agree__c, Applicant_Comments__c FROM Application_Condition__c WHERE Link_Authorization_Regulation__c =: linkauthID order by Condition_Number__c])
     
              conditionsList.add(new ConditionWrapper(c));
              return conditionsList;
          }

       //all selected Conditions...for edit...       
     public PageReference getSelected() {
    
         selectedConditions.clear();
         for(ConditionWrapper conwrapper: conditionsList) 
            if(conwrapper.selected == true)
       
            selectedConditions .add(conwrapper.con); 
       
            return null;
        }
        
        
     public List<Application_Condition__c> GetSelectedConditions(){
    
        if(selectedConditions.size()>0)
        return selectedConditions;
        else
        return null;
    } 
     
    //saving edited value...
   
      public PageReference tosave() { 
      if(selectedConditions.size()>0){
      
        update selectedConditions;
        PageReference RowEditing=new PageReference('/apex/Portal_Link_Auth_Regulation_Detail?id='+linkauthID);
        RowEditing.setRedirect(true);
        return RowEditing;
        }
        else{
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one Condition.'));
         return null;
        }
       
       }
    
    //wrapper class...
    
    public class ConditionWrapper
    {
        public Application_Condition__c  con{get; set;}
        public Boolean selected {get; set;}
        public ConditionWrapper(Application_Condition__c  c)
        {
            con = c;
            selected = false;
        }
    }

}