@isTest(seealldata=true)
private class csstest_control_Test_old {
    @IsTest static void csstest_control_Test() {
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        csstest_control cssContrlr = new csstest_control();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId; 
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        
        Application__c objapp = testData.newapplication();
        Application__c app = new Application__c();
        app.id =  objapp.id;
        app.Application_Status__c = 'Waiting on Customer';
        app.Share_with_Roles__c = 'BRS';
        update app;
         
        app.Renewal_Application__c = true;
        app.Application_Status__c = 'Withdrawn';
        update app;
        
        Application__c objapp1 = testData.newapplication();
       // delete objapp1;
        
        Authorizations__c objauth = testData.newAuth(objapp.id);
        
        List<Authorizations__c> objauthList = new List<Authorizations__c>();
        objauthList.add(objauth);
        
        Program_Line_Item_Pathway__c progPath = testData.newCaninePathway();
        
        
        
        Regulated_Article__c regArt = testData.newRegulatedArticle(progPath.id);
        User usershare = new User();
        usershare.Username ='aphistestemail@test.com';
        usershare.LastName = 'APHISTestLastName';
        usershare.Email = 'APHISTestEmail@test.com';
        usershare.alias = 'APHItest';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = objcont.id;
        insert usershare;    
        objauth.Authorized_User__c = objcont.id;
        objauth.ownerid = usershare.id;
        update objauth;
        CARPOL_BRS_TestDataManager testdata1 = new CARPOL_BRS_TestDataManager();
        AC__c objac = testdata1.newLineItemBRS('Testing',objapp);
        system.debug('lineitem' +objac);
        objac.Authorization__c = objauth.id;
       // objac.Program_Line_Item_Pathway__c = progPath.id;
         update objac;
        
        Test.startTest(); 
        //run as portal user
        System.runAs(usershare) {
            PageReference pageRef = Page.Portal_Application_Detail;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put('parentlineitemid',objac.id);
            ApexPages.currentPage().getParameters().put('regArticlePackageId',regArt.id);
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
            ApexPages.currentPage().getParameters().put('Id',objapp.id);
            csstest_control extclass = new csstest_control(sc);
            
            csstest_control.wraplnlst objwraplnlst = new csstest_control.wraplnlst(true, objac);
            boolean b = extclass.isCloneBtnAllowed(objauthList);
            boolean b1 = csstest_control.deleteApplication(objapp.id);
                                    
            extclass.CVBflag = false; 
           

           // extclass.lineItemIdToBeWithdrawn = objac.id;
            extclass.parentlineitemid = objac.id;
            extclass.applicationID = objapp.id;
            extclass.regArticlePackageId = regArt.id;
            extclass.selectedAuthid = objauth.id;
            extclass.selectedAuthidother = objauth.id;
            extclass.newlnid.add(objac.id);
            extclass.displayPopup = true;
            extclass.applicationID = objapp.id;
            //csstest_control.wraplnlst objwraplnlst = new csstest_control.wraplnlst(true, li);
            // extclass.wrlnlst = objwraplnlst;
            extclass.getattach();
            extclass.getAC();
            extclass.getAuth();
            extclass.gethistory();
            //extclass.editAssocContact();
            extclass.lineItemIdToBeDeleted = objac.id;
            
            extclass.dir();
            extclass.redirectPage();
            extclass.getProductIngredients();
            extclass.getTrx();
            PageReference p1 = extclass.ActionItems();
            extclass.BRSItemDetails();
            
             extclass.withdrawmethod();
               extclass.withdrawLineItem();
            extclass.showPopup();
            extclass.closePopup();
           
            String helpText = extclass.lineitemhelptext;
            extclass.lineItemIdToBeWithdrawn = objac.id;
        }  
        
        //run as salesforce user
   /*     AC__c li = testData.newlineitem('Resale/Adoption',objapp);
        li.Authorization__c = objauth.id;
        li.Renewal_Application__c = true;
        li.Application_Status__c = 'Withdrawn';
        update li; */
        
        ApexPages.currentPage().getParameters().put('Id',objapp.id);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
        csstest_control extclass = new csstest_control(sc);
        extclass.deleteLineItem();
        
  /*      csstest_control.wraplnlst objwraplnlst = new csstest_control.wraplnlst(true, li);
        PageReference pageRef = Page.Portal_Application_Detail;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('parentlineitemid',objac.id);
        ApexPages.currentPage().getParameters().put('regArticlePackageId',regArt.id);
        ApexPages.currentPage().getParameters().put('Id',objapp.id);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
        csstest_control extclass = new csstest_control(sc);
        
        extclass.CVBflag = false;
        extclass.deleteLineItem();
        extclass.withdrawmethod();
        boolean b = extclass.isCloneBtnAllowed(objauthList);
        boolean b1 = csstest_control.deleteApplication(objapp.id);
        
               
        extclass.lineItemIdToBeWithdrawn = String.valueof(li.id);
        extclass.parentlineitemid = objac.id;
        extclass.regArticlePackageId = regArt.id;
        extclass.selectedAuthid = objauth.id;
        extclass.applicationID = objapp.id;
        extclass.selectedAuthidother = objauth.id;
        extclass.displayPopup = true;
        PageReference p1 = extclass.ActionItems();
        
       
       /* extclass.newlnid.add(objac.id);
        extclass.applicationID = objapp.id;
        extclass.getattach();          
        extclass.getAC();
        extclass.getAuth();
        extclass.gethistory();
        //extclass.editAssocContact();
       
        extclass.dir();
        extclass.redirectPage();
       
        extclass.BRSItemDetails();
        extclass.withdrawLineItem();
        
        extclass.showPopup();
        system.assert(extclass != null);   */
        Test.stopTest(); 
    }
    //-------------------------------------------------------------------------------------------------
}