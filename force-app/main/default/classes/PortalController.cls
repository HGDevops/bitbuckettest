/**
* Created by justinfloyd on 11/10/16.
* TODO:
*  1.make 2 Utility Class(sharing) one for Applicant and one for Partner Users
*    a.Add in different Functions for each call
*  2.Remove usage from Navigation VF Component
*/
public with sharing class PortalController {
    
    
    //Constants
    static final string MULTIPLE='Multiple';
    static final string NONE='None';
    static final string LOWERCASENONE='none';
    static final string BLOCK='block';
    static final string PARTNERUSERLICENSE='Partner Community Login';
    static final string PORTALAPPLICATIONPVT = 'Portal_Application_Private';
    static final string EXTERNAL = 'External';
    static final string LANDINGPAGE = 'Landing_Page';
    static final string GUEST ='Guest';
    static final string NORELATEDACCOUNT = 'Deactivation failed, contact only associated to one account.';
    static final string APPLICANT_TRANSFER = 'Applicant Transfer';
    
    //Variables
    public String privateStatus { get;set;}
    public Id delrecid { get;set;}
    public Map < string, string > sobjectkeys { get;set;}
    public string applicationId { get;set;}
    public string LineItemId { get;set;}
    //public List < ConWrapper > lstConWrapper { get;set;}
    public string indOrgCon { get;set;}
    public string errorMessage {get; set;}
   // public List<Authorizations__c> myAuthorizations { get; set; } //remove after BRS release on 07/22/2019

    public Boolean getAdminAccess() {
        Id UserProfileId = userinfo.getProfileId();
        // //system.debug('UserProfileId+++' +UserProfileId);
        Profile p = [select id FROM Profile WHERE Name = 'eFile Applicant Plus'];
        if(UserProfileId == p.id){
            return True;
        }else{
            return False;
        }
    }
    
    public User userDetails {
        get { return EFLUserUtility.userdetails;}set;
    }    
    public String privateLink { get{
        if (EFLUserUtility.userDetails.Profile.UserLicense.Name == PARTNERUSERLICENSE && privateLink==null) 
            privateLink = BLOCK;
        else
            privateLink = LOWERCASENONE; 
        return privateLink;
    }set;}
    
    public string contactName {
        get {return EFLUserUtility.contactDetails.FirstName + '' + EFLUserUtility.contactDetails.LastName;}set;
        
    }    
    public class ConWrapper {
        public Contact currentCon { get;set;}
        public Boolean active { get; set; }
        public ConWrapper(Contact c, Boolean a) {
            this.currentCon = c;
            this.active = a;
        }
    }    
    
    public Boolean isAdmin {
        get {return EFLUserUtility.contactDetails.Account_Admin__c;} set;
    }
    
    public map<id, Contact> myContacts {
        get{            
            return new map<id, Contact> (EFLContactUtility.selectActiveUserContactsByCurrentUserDirectAccount());
        }
        set;
    }
    
    /*Ravee Racharla 04/05/2019 */
    public list<Account> myAccounts {
        get{
            
            return (EFLAccountRepository.selectActiveUserAccountsByCurrentUserDirectAccount());
        }
        set;
    }
    /*Ravee Racharla 04/05/2019 */
    
    public List < Applicant_Contact__c > myAssocContacts {
        get {
            if (myAssocContacts == null) { 
                myAssocContacts = [SELECT ID, name, First_Name__c, Phone__c,Alternate_Phone__c, Account__c, Account__r.Name, Email_Address__c FROM Applicant_Contact__c where Account__c = :EFLUserUtility.contactDetails.AccountId];
            }
            for (Applicant_Contact__c ac: myAssocContacts){
                if(ac.First_Name__c !=null){
                    ac.Name = ac.First_Name__c+' '+ac.Name;
                    indOrgCon = 'Ind';
                }
            }
            
            
            return myAssocContacts;
        }
        set;
    }
    
    public string privateColumnHelpText {
        get {
            try {
                return EFL_Help_Text__c.getValues(PORTALAPPLICATIONPVT).Help_Text__c;
            } catch (exception e) {
                return '';
            }
        }
        private set {}
    }
    
    public string communityURL {
        get {
            try {
                return baseurl.URL__c;
            } catch (exception e) {
                return '';
            }
        }
        private set {}
    }
    
    CARPOL_URLs__c baseurl = CARPOL_URLs__c.getValues(EXTERNAL);
    
    public List<ConWrapper> getlstConWrapper() {
        List <ConWrapper> lstConWrapper = new List <ConWrapper> ();
        //if (EFLUserUtility.contactDetails.Account_Admin__c) {
        //if (EFLUserUtility.isAccountAdmin()) {
        for (Contact c: myContacts.values()) 
        {
            if (c.users!=null) 
            {
                ConWrapper conWrapInst;
                if (c.Id != EFLUserUtility.contactDetails.id) {
                    conWrapInst = new ConWrapper(c, c.users[0].isActive);
                }else{
                    conWrapInst = new ConWrapper(c, false);
                }
                lstConWrapper.add(conWrapInst);
            }
        }
        /*} else {
ConWrapper conWrapInst = new ConWrapper(EFLUserUtility.contactDetails, false);
lstConWrapper.add(conWrapInst);
}*/
        return lstConWrapper;
    }
    
    
    /* 
*@Purpose:This code is explicity for assigning applications to a Partner user who just joined an org community. 
This code only runs once for each partner user.
TODO: Need to understand why we are firing this on Community Landing page
*/    
    public PageReference forwardToAuthPage() {
        
        if (EFLUserUtility.userDetails.Profile.UserLicense.Name == PARTNERUSERLICENSE && EFLUserUtility.userDetails.ContactId != null) {
            CARPOL_ApexManagedSharing.addSharingForUser(EFLUserUtility.userDetails.id);
        }
        String customHomePage;
        //system.debug('LandingPage###'+CARPOL_URLs__c.getValues(LANDINGPAGE));
        CARPOL_URLs__c LandingPage = CARPOL_URLs__c.getValues(LANDINGPAGE);
        
        customHomePage = LandingPage.URL__c;
        
        if (UserInfo.getUserType().equals(GUEST)) {
            return new PageReference(communityUrl + '/login?startURL=' + EncodingUtil.urlEncode(customHomePage, 'UTF-8'));
        } else {
            return null;
        }
    }
    
    public PageReference editContact() {
        PageReference editContact = new PageReference('/' + EFLUserUtility.contactDetails.ID + '/e');
        editContact.setRedirect(true);
        return editContact;
    }    
    
    
    /* 
*@Purpose:This method is being called from Contacts --> Deactivate link. 
Users applications are transferred to the primary account admin
*/
    public PageReference deactivateUser() {
        Savepoint sp = Database.setSavepoint();
        String deactivatedContactId = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('deactivatedContactId') );
        String currentAccountId = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('currentAccountId') );
        Boolean redirectToContactPage = false;
        PageReference returnPage;
        if(String.isBlank(currentAccountId)){
            currentAccountId = EFLUserUtility.Contactdetails.AccountId;
            redirectToContactPage = true;
        }
        errorMessage ='';    
        try 
        {
            if(String.isNotBlank(deactivatedContactId)) 
            {
                contact deactivatedConUserACR  = new contact();
                deactivatedConUserACR = EFLContactUtility.selectContactAccountRelationsByContactId(deactivatedContactId);
                List<AccountContactRelation> acrsToDelete = new List<AccountContactRelation>();
                //to deactivate contact from account - are we deactivating from parent account or child account?
                //if child account, remove ACR
                //if parent, 1)reparent contact to personal account 2)remove all ACRs from child accounts 3)transfer ownership of records
                Contact userContact = EFLUserUtility.contactDetails;
                if (userContact.AccountId == currentAccountId ){ //deactivate from parent account
                    for (AccountContactRelation acr: deactivatedConUserACR.AccountContactRelations){
                        if (acr.Personal__c){//switch personal account back
                            deactivatedConUserACR.AccountId = acr.AccountId;
                            break;
                        }
                    }
                    //delete all relationships from child accounts
                    acrsToDelete.addAll(EFLContactUtility.contactAccountRelationsByContactAndParentAccount(deactivatedContactId,currentAccountId));  
                    //replacing ownership and Applicant lookup on Applications, Auth, Lineitem and Construct to Admin (Current User)
                    EFLContactUtility.transferOwnership(deactivatedConUserACR.users[0], EFLUserUtility.userDetails, APPLICANT_TRANSFER, null);
                    
                } else { //deactivate from child account
                    for (AccountContactRelation acr: deactivatedConUserACR.AccountContactRelations){
                        if (acr.AccountId == currentAccountId && acr.ContactId == deactivatedContactId){
                            acrsToDelete.add(acr);
                            break;
                        }
                    }
                    Set<Id> applicationIdSet = EFLContactUtility.getUserApplications(deactivatedConUserACR.users[0], currentAccountId);
                    if (applicationIdSet !=null){
                        EFLContactUtility.transferOwnership(deactivatedConUserACR.users[0], EFLUserUtility.userDetails, APPLICANT_TRANSFER, applicationIdSet);
                    }
                }
                //update contact direct account relationship to personal account               
                EFLContactUtility.UpdateContact(deactivatedConUserACR);
                EFLContactUtility.DeleteAccountContactRelationByList(acrsToDelete);
                
                returnPage = redirectToContactPage ? new PageReference('/contacts' ) : new PageReference('/EFLEditAccount?accId=' + currentAccountId );
                
                returnPage.setRedirect(true);
                return returnPage;
                /*
if(deactivatedConUserACR.AccountContactRelations!=null && deactivatedConUserACR.AccountContactRelations.size()>1)
{
//Delink contact from Current User(Admin) Account
contact delinkingContact = new contact();
delinkingContact.id = deactivatedConUserACR.id;
//Delete ACR related to Current User(Admin) Account
AccountContactRelation acrToDelete = new AccountContactRelation();

boolean delink = false;
boolean acrRemove = false;
// RR - W-34930
boolean IsDeactivatingFromParent = false;
// If the current account is same as the logged in user's(admin) account 
//it is considered as parent account.
if(currentAccountId == EFLUserUtility.Contactdetails.AccountId)
IsDeactivatingFromParent = true;

for(AccountContactRelation acr: deactivatedConUserACR.AccountContactRelations)
{
if(acr.AccountId != currentAccountId && delinkingContact.AccountId==null)
{
delinkingContact.AccountId = acr.AccountId;
delink = true;
}
// We delete all ACRs except the one with personal account
if(IsDeactivatingFromParent && acr.IsDirect == false){
acrsToDelete.add(acr);
acrRemove = true;
//system.debug(' Deleting ACR from parent: ' + acr);
}
else if(acr.AccountId == currentAccountId)
{
acrToDelete = acr;
acrRemove = true;
}
} 

if(delinkingContact.AccountId != null && (acrToDelete.id!=null || acrsToDelete.size() > 0))
{
//update contact direct account relationship to personal account
EFLContactUtility.UpdateContact(delinkingContact);
//delete AccountContactRelation on the delinked contact relating to Admin account
if (IsDeactivatingFromParent)
EFLContactUtility.DeleteAccountContactRelationByList(acrsToDelete);
else
EFLContactUtility.DeleteAccountContactRelation(acrToDelete);
// RR -4/19 changes begin
// If the current account is same as the logged in user's(admin) account 
//it is considered as parent account.
if(!IsDeactivatingFromParent) {
Set<Id> applicationIdSet = EFLContactUtility.getUserApplications(deactivatedConUserACR.users[0], currentAccountId);
if (applicationIdSet !=null){
EFLContactUtility.transferOwnership(deactivatedConUserACR.users[0], EFLUserUtility.userDetails, APPLICANT_TRANSFER, applicationIdSet);
}
}
else{
//replacing ownership and Applicant lookup on Applications, Auth, Lineitem and Construct to Admin (Current User)
EFLContactUtility.transferOwnership(deactivatedConUserACR.users[0], EFLUserUtility.userDetails, APPLICANT_TRANSFER, null);
}
// RR -4/19 changes end
//RR 4/18 - begin. refresh entire page
if(redirectToContactPage)
{
returnPage = new PageReference('/contacts' );            

}
else{
returnPage = new PageReference('/EFLEditAccount?accId=' + currentAccountId ); 
}
returnPage.setRedirect(true);
return returnPage;


//RR 4/18 - end
}
}
else
{
Database.rollback(sp); 
errorMessage = NORELATEDACCOUNT;  
return null;
}*/
            }
            //return null;
        } 
        catch (Exception ex) 
        {
            Database.rollback(sp); 
            EFLErrorLog.createErrorLog('PortalController.deactivateUser()',ex); 
        }
        return null;
    }
    
    /* 
*@Purpose:Delete requirements records for state reviewers
*/ 
    public PageReference deleteRecord() {
        EFLGenericUtility.deleteRecord(delrecid);
        PageReference dirpage = new PageReference('/apex/RequirementsTemplate');
        dirpage.setRedirect(true);
        return dirpage;
    }
    /* 
*@Purpose: Returns the Line Item records to both dashboard and Application tab
*/   
    @RemoteAction
    public static list <EFLExternalUsersHandler.lineItemsWrapper> getlineItemRecords(ID appId) {
        
        return EFLExternalUsersHandler.getlineItemRecords(appId);
    }  
    
    /* 
*@Purpose: Returns the official review records for Partner Users
*/
    public list <EFLPartnerUsersHandler.reviewersWrapper> getReviewerRecords() {
        
        return EFLPartnerUsersHandler.getReviewerRecords();
    }
    
    
    /* 
*@Purpose: Returns user specific Authorizations
*/
    public list <Authorizations__c> getmyAuthorizations() {
        
        return EFLExternalUsersHandler.AuthorizationList;
    }    
    
    
    /* 
*@Purpose: Returns user specific requirements/regulations
*/
    public list <Regulation__c> getmyStateRegulations() { 
        
        return EFLPartnerUsersHandler.stateRegulationsList;
    } 
    
    /* 
*@Purpose: Returns user specific Required Attention Authorizations
*/
    public list <Authorizations__c> getRequiredAttentionAuthorizations( ) { 
        
        return EFLExternalUsersHandler.RequiredAttentionauthorizationList;   
    }     
    
    
}