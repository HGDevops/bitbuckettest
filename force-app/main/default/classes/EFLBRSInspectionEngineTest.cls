@isTest
public class EFLBRSInspectionEngineTest {
    
    static testMethod void testLoadActivities(){
        Profile p = [SELECT Id FROM Profile WHERE Name='eFile APHIS Staff'];
        UserRole r = new UserRole(DeveloperName = 'MyBRSPS2', Name = 'ROP Inspection Reviewer');
            Insert r;
        User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
        insert u1;
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        
        system.runas(sysAdm)
        {
            EFLBRSInspectionEngine inspectionEngine = new EFLBRSInspectionEngine();
            Inspection__c Ins = new Inspection__c();
            list<sObject> activityList = new list<sObject>();
            String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
            Ins.RecordTypeId = IncRecTypeID;
            Ins.Stage__c = 'Inspection Assignment';
            Ins.status__c='Cancelled';
            Ins.Activity_Sequence__c=1;
            Ins.Workflow_Number__c ='BRS Inspection';
            Ins.Reason_for_Cancellation__c = 'testing';
            insert Ins;
            
            EFL_Inspection_Questions__c insq = new EFL_Inspection_Questions__c
            (Question__c='test question',Answer_TYpe__c = 'Free Text');
       	 	insert insq;
            
            EFL_Inspection_Questions_Template__c templ = new EFL_Inspection_Questions_Template__c
            (Name='EFL_templ',Inspection__c=Ins.id);
       	 	insert templ;
            
            EFL_INS_Template_Question_Junction__c templJunc = new EFL_INS_Template_Question_Junction__c
            (Inspection_Template_Questions__c=insq.id,EFL_Inspection_Questions_Template__c = templ.Id);
       	 	insert templJunc;
            
            EFL_Inspection_Questionnaire__c q = new EFL_Inspection_Questionnaire__c();
        	insert q;
           	list<EFL_Inspection_Questionnaire_Questions__c> qs = new list<EFL_Inspection_Questionnaire_Questions__c>();
            for(integer i = 0; i < 5; i++){
                EFL_Inspection_Questionnaire_Questions__c qq = new EFL_Inspection_Questionnaire_Questions__c();
                qq.Question__c = 'Test' + i;
                qq.Response__c = 'Yes';
                qq.Comments__c = 'Test';
                qq.EFL_Inspection_Questionnaire__c = q.Id;
                qq.Inspection__c = ins.Id;
                qq.Options__c = 'Yes,No';
                qs.add(qq);
            }
        	String BRSTeamRecordTypeId = Schema.SObjectType.team__c.getRecordTypeInfosByDeveloperName().get('BRS_Team').getRecordTypeId();
        	insert qs;
            team__c team = new team__c();
            team.RecordTypeId = BRSTeamRecordTypeId;
            team.member_role__c='ROP Reviewer (Regulatory Biotechnologist 1)';
            team.member__c = u1.id;
            team.Inspection__c = Ins.id;  
            insert team;
            
                inspectionEngine.loadActivities(Ins, activityList);
                List<EFL_Inspection_Questions_Template__c> templ1 = [Select id from EFL_Inspection_Questions_Template__c limit 1];
                Set<id> InspecTempId = new Set<id>();
                for(EFL_Inspection_Questions_Template__c instempl : templ1){
                    InspecTempId.add(instempl.Id);
                }
                EFLBRSInspectionEngine.getQuestion(InspecTempId);
            try{
                Ins.EFL_Inspection_Questionnaire_Template__c = templ.Id;
                update Ins;
                inspectionEngine.loadQuestion(Ins, qs);
            }catch(exception e){}
            
        }
    }

}