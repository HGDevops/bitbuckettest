public class EFLLockUnlockUtility {
    
    public static boolean lockLineitem(ac__c lineitemRecord)
    {
        if(lineitemRecord.Status__c != 'Submitted' && lineitemRecord.Status__c != 'Withdrawn'  )
            return true;
        else
            return false;
    }

    public static boolean enableApplicantInstructions(ac__c lineitemRecord)
    {
        if(lineitemRecord.Status__c == 'Waiting on Customer')
            return true;
        else
            return false;
    }    
    

}