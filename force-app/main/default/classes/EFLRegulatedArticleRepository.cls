/*
 *Repository Class: Handles the Regulated Article Data Access
*/ 
public class EFLRegulatedArticleRepository {
   //Attributes
   static final string ACTIVE = 'Active';
   
   // Obtain Regulated Articles ID list for the required pathways
   public static list<Id> selectRegulatedArticleIdsByPathwayIds(set<id> programPathwayId){
      try{
        EFLEnforceAccessUtility.checkObjectReadAccess('RA_Scientific_Name_Junction__c');
            list<Id> regulatedArticleIDList = new list<Id>();
            list<RA_Scientific_Name_Junction__c> regulatedArticlesbyPathwayList = new list<RA_Scientific_Name_Junction__c>();
               
              regulatedArticlesbyPathwayList = [Select Regulated_Article__c 
                                                  from RA_Scientific_Name_Junction__c 
                                                 where Program_Pathway__c IN:programPathwayId
                                                   and Status__c =: ACTIVE];            
                for(RA_Scientific_Name_Junction__c r:regulatedArticlesbyPathwayList ){
                    regulatedArticleIDList.add(r.Regulated_Article__c);
                  }
    
              return regulatedArticleIDList;
       }catch(exception e){
                     // EFLErrorLog.createErrorLog('EFLRegulatedArticleRepository.selectRegulatedArticleIdsByPathwayIds()',e);                
        } 
       return null;
     }  
    
    
    // Obtain regulated article's for the selected pathway and category ID's
    public static List<Regulated_Article__c> selectRegulatedArticlesByPathwayIDsandCategoryIDs(set<id> programPathwayIds,set<id> categoryIds){
      try{
        EFLEnforceAccessUtility.checkObjectReadAccess('Regulated_Article__c');
         String soql = 'select id,Name,Scientific_Name__c,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c,Category_Group_Ref__r.Name,Category_Group_Ref__c,Createddate from Regulated_Article__c where status__c = \'' + ACTIVE + '\'';   
          if(categoryIds!=null){
               soql+= ' and Category_Group_Ref__c  in : categoryIds ';
          }
         if(programPathwayIds!=null){
            list<id> regulatedArticleIds = selectRegulatedArticleIdsByPathwayIds(programPathwayIds);
            if(regulatedArticleIds!=null){ 
                soql+= ' and id in : regulatedArticleIds ';}
           }
          soql+= ' Order By Name ';
          list<Regulated_Article__c> listRA = Database.query(soql);  
          return listRA; 
        }catch(exception e){
                     // EFLErrorLog.createErrorLog('EFLRegulatedArticleRepository.selectRegulatedArticlesByPathwayIDsandCategoryIDs()',e);                
        } 
       return null;
    }    
    

}