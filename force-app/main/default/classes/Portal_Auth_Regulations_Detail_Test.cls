@isTest(seealldata=true)
private class Portal_Auth_Regulations_Detail_Test {
    @IsTest static void testPortal_Auth_Regulations_DetailWithAnsweredQ() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings(); 
        
        Application__c objapp = testData.newapplication();
        AC__c objac = testData.newLineItem('Personal Use',objapp);               
        Authorizations__c objauth = testData.newAuth(objapp.id);
        objauth.Status__c = 'In Review';
        update objauth;
        objac.Authorization__c = objauth.id;
        update objac;
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        Workflow_Task__c objWFT = testData.newworkflowtask('Process Authorization', objauth, 'Pending');
        objWFT.Status_Categories__c = 'Customer Feedback';
        update objWFT;
        
        Test.startTest(); 
        
        //run as salesforce user
        PageReference pageRef = Page.Portal_Auth_Regulations_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('id',objauth.id);                                                                                                   
        Portal_Auth_Regulations_Detail extclass = new Portal_Auth_Regulations_Detail(sc);
        objreg3.Type__c = 'Import Requirements';
        update objreg3;
        objreg2.Type__c = 'Additional Information';
        update objreg2;              
        objreg1.Type__c = 'Instruction for CBP Officers';
        update objreg1;                            
        objauthjun3.RecordTypeId = extclass.AuthJuncRecordTypeId;
        objAuthjun3.Applicant_Agree__c = 'Agree';
        update objauthjun3;
        objauthjun2.RecordTypeId = extclass.AuthJuncRecordTypeId;
        objAuthjun2.Applicant_Agree__c = 'Agree';
        update objauthjun2;              
        objauthjun1.RecordTypeId = extclass.AuthJuncRecordTypeId;
        objAuthjun1.Applicant_Agree__c = 'Agree';
        update objauthjun1;
        extclass.saveagreementchanges();
        extclass.getRegulations();  
        extclass.getAddInformation();   
        extclass.getCBPInformation();
        extclass.OriginalRegulations[0].Applicant_Comments__c = 'Test';              
        extclass.OriginalAddInformation[0].Applicant_Comments__c = 'Test';  
        extclass.OriginalCBPInformation[0].Applicant_Comments__c = 'Test';                
        extclass.id = ApexPages.currentPage().getParameters().get('id');                                
        extclass.tosave();
        extclass.submit();
        System.assert(extclass != null);        
        Test.stopTest();   
    }
    
    @IsTest static void testPortal_Auth_Regulations_DetailWithUnansweredQ() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings(); 
        
        Application__c objapp = testData.newapplication();
        AC__c objac = testData.newLineItem('Personal Use',objapp);               
        Authorizations__c objauth = testData.newAuth(objapp.id);
        objauth.Status__c = 'In Review';
        update objauth;
        objac.Authorization__c = objauth.id;
        update objac;
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        Workflow_Task__c objWFT = testData.newworkflowtask('Process Authorization', objauth, 'Pending');
        objWFT.Status_Categories__c = 'Customer Feedback';
        update objWFT;
        
        Test.startTest(); 
        
        //run as salesforce user
        PageReference pageRef = Page.Portal_Auth_Regulations_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('id',objauth.id);                                                                                                   
        Portal_Auth_Regulations_Detail extclass = new Portal_Auth_Regulations_Detail(sc);
        objreg3.Type__c = 'Import Requirements';
        update objreg3;
        objreg2.Type__c = 'Additional Information';
        update objreg2;              
        objreg1.Type__c = 'Instruction for CBP Officers';
        update objreg1;                            
        objauthjun3.RecordTypeId = extclass.AuthJuncRecordTypeId;
        update objauthjun3;
        objauthjun2.RecordTypeId = extclass.AuthJuncRecordTypeId;
        update objauthjun2;              
        objauthjun1.RecordTypeId = extclass.AuthJuncRecordTypeId;
        update objauthjun1;
        extclass.getRegulations();  
        
        extclass.submit();
        System.assert(extclass != null);        
        Test.stopTest();   
    }
}