@isTest
public class EFLACTestServices {
    
    public static integer uniqueNum = 0;
    public static integer getUnique(){
        return getNext();
    }
    public static integer getNext(){
        uniqueNum += 1;
        return uniqueNum;
    }
    public static integer getRepeat(){
        return uniqueNum;
    }
    public static integer getPrevious(){
        uniqueNum -= 1;
        return uniqueNum;
    }

    public static Profile getProfileByName(string name){
        return [SELECT Id FROM Profile WHERE Name = :name];
    }
    
    public static User getUser(Profile p){
        return new User(Alias = 'standt', Email='standarduser' + getUnique() + '@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='eflaclra' + getRepeat() + '@testorg'+getUnique()+'.com');
    }
    
    public static User getCommunityUser(Profile p, Contact c){
        return new User(Alias = 'standt', Email='standarduser' + getUnique() + '@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='eflaclra' + getRepeat() + '@testorg'+getUnique()+'.com', contactId = c.Id);
    }
    
    public static User getAdminUser(){
        Profile pr = EFLACTestServices.getProfileByName('System Administrator');
        User admin = EFLACTestServices.getUser(pr);
        return admin;
    }
    
    public static Wizard_Questionnaire__c getQuestion(string pathway, Id rtId){
        return new Wizard_Questionnaire__c(
        	//Wizard_Question_Rich_Text__c = '<li>First</li> ever <u>Question</u> for    <ul>underlined</ul> text' + getUnique(),
            Wizard_Question_Rich_Text__c = '<p>Of <ul>these</ul> 8 or fewer <u>mammals</u>, are they <i>exclusively</i>:</p><p><br></p><ul><li>Cows, goats, pigs (including mini-pigs), sheep, llamas, alpacas, and their hybrid crosses</li><li><u>Pet animals</u>, <ul>and</ul>/or</li><li><u>Pocket pets</u> </li></ul>' + getUnique(),
            AC_LRA_Pathway__c = pathway,
            Type__c = 'test',
            Type_of_Options_LRA__c = '',
            Question_Number__c = 'Q' + getUnique(),
            RecordTypeId = rtId,
            Status__c = EFLACLRAQAController.STATUS_ACTIVE
        );
    }
    public static void lraTestDataSuite(integer numQuestions){
        // make Wizard questions & Selections.
        // go into 3 levels of questions for each pathway in the mdt
        list<string> pathways = new list<string>();
        for(EFL_AC_LRA_Pathways__mdt m : [SELECT Id, Pathway__c FROM EFL_AC_LRA_Pathways__mdt]){
            pathways.add(m.Pathway__c);
        }
        
        string rtId = [SELECT Id FROM RecordType WHERE DeveloperName = :EFLACLRAQAController.RECORD_TYPE_WIZARD_QUESTION][0].Id;
        
        list<Wizard_Questionnaire__c> questions = new list<Wizard_Questionnaire__c>();
        for(string s:pathways){
            for(integer i = 0; i < numQuestions; i++){
                questions.add(getQuestion(s,rtId));
            }
        }
        insert questions;
    }
    
    public static Account getAccount(){
        Account a = new Account();
        a.Name = 'Test ' + getUnique();
        return a;
    }
    
    public static Contact getContact(){
        Contact c = new Contact();
        c.LastName = 'Contact ' + getUnique();
        return c;
    }
    public static Contact getContactWithAccount(Account a){
        Contact c = getContact();
        c.AccountId = a.Id;
        return c;
    }
    
    public static User getCommunityUser(string communityName){
        User admin = getAdminUser();
        UserRole r = [SELECT Id FROM UserRole LIMIT 1];
        admin.UserRoleId = r.Id;
        Account a;
        Contact c;
        User siteGuest;
        system.runAs(admin){
            a = EFLACTestServices.getAccount();
            insert a;
            c = EFLACTestServices.getContactWithAccount(a);
            insert c;
            Profile p = EFLACTestServices.getProfileByName(getDefaultProfileForCommunity(communityName));
            siteGuest = EFLACTestServices.getCommunityUser(p, c);
        }
        
		return siteGuest;
    }
    
    public static string getDefaultProfileForCommunity(string communityName){
        return [SELECT Profile_Name__c FROM communityJITHelper__mdt WHERE DeveloperName = :communityName][0].Profile_Name__c;
    }
}