@IsTest (SeeAllData=true)
//Auth trigger calls a class (CARPOL_UNI_SendIssuedAttachment) that calls connectedAPI
public class EFLRandomOVInspectionCtrl_test{
  public testmethod static void test1(){
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      String AccountRecordTypeId = testData.AccountRecordTypeId; 
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(testData.AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Group__c objgrp = testdata.newgroup();
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);      
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
      Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Attachment attach = testData.newattachment(ac.Id); 
      domain__c prg = new domain__c(name='VS');
      insert prg;
      Program_Prefix__c pp = new Program_Prefix__c (name ='611',Program__c=prg.id);
      insert pp;
      Signature__c TP = new Signature__c(name='New TP', Program_Prefix__c = pp.id) ;
      insert tp;
      list <Authorizations__c> authLst = new list <Authorizations__c>();         
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      objauth.RecordTypeID = ACauthRecordTypeId;
      objauth.thumbprint__c = TP.id;
      update objauth;
      authLst.add(objauth);
      ac.Authorization__c = objauth.Id;
      update ac;
      test.starttest();
      EFLRandomOVInspectionCtrl ctrl = new EFLRandomOVInspectionCtrl(new ApexPages.StandardController(objauth));
        ctrl.GenerateRandomOVInspection();
        ctrl.goToInspection();
      test.stopTest();
  }
}