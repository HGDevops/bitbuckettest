@isTest
public class XMLDom_Test {
    /*
    public static testmethod void helptest(){
        helptextclass hc = new helpTextClass();      
        helpTextclass.subTabNames tabNames = new helpTextclass.subTabnames();        
        hc.main('http://click-pledge.v-empower.com/web/HelpText/HelpTextXml/HelpTextXml.xml');
        hc.isValid = true;      
         hc.helpText();  
    }
    */
    
    public static testmethod void helptest(){
        
        Facility__c fc = new Facility__c();
        fc.Name = 'Name';
        fc.Address_1__c = '124';
        fc.Address_2__c = '123';
        fc.City__c = 'Burbank';
        fc.State_Code__c = 'CA';
        fc.Zip__c = '91505';
        //fc.Time_Zone__c 
        insert fc;
        
        List<Id> IDset = new List<Id>();
        IDset.add(fc.id);
        CARPOL_AC_GooglePopulateTimeZone.CARPOL_AC_CallGoogleGeoAddress(IDset);
        
        String xmlDoc = '<xml><ToUserName><ToUserName>tUN</ToUserName><FromUserName>fUN</FromUserName><MsgType>mT</MsgType></xml>';
        xmlDoc += '<xml><ToUserName>tUN</ToUserName><FromUserName>fUN</FromUserName><MsgType>mT</MsgType></xml>';
        xmlDoc +=  '<xml><ToUserName>tUN</ToUserName><FromUserName>fUN</FromUserName><MsgType>mT</MsgType></xml>';
        xmlDoc += '<xml><ToUserName>tUN</ToUserName><FromUserName>fUN</FromUserName><MsgType>mT</MsgType></xml>';
        //'<Company><Employee><FirstName>Tanmay</FirstName><LastName>Patil</LastName><ContactNo>1234567890</ContactNo><Email>tanmaypatil@xyz.com</Email><Address><City>Bangalore</City><State>Karnataka</State> <Zip>560212</Zip></Address></Employee></Company>';
        XMLDom xmldm = new XMLDom();
        XMLDom xmldm1 = new XMLDom(xmlDoc);
        
        xmldm.getElementsByTagName('ToUserName');
        xmldm.getElementsByPath('ToUserName');
        xmldm.getElementByPath('ToUserName');
        xmldm.getElementByTagName('ToUserName');
       
        //xmldm.removeChild(xmldm.getElementsByTagName('123')[0]);
        xmldm.ownerDocument();
        xmldm.toXmlString();
        xmldm.getElementByTagName ('ToUserName');
        XMLDom.Element Ele = new XMLDom.Element();
        Ele.getAttribute('ToUserName');
        Ele.getValue('ToUserName'); 
         Ele.nodeName = 'ToUserName';
        Ele.nodeValue = 'tUN';
        Ele.toXmlString();
        Ele.dump();
        Ele.firstChild();
        Ele.textContent();
        Ele.hasChildNodes();
        Ele.nodeName = 'ToUserName';
        Ele.nodeValue = 'tUN';
        Ele.isEqualNode(Ele.getElementByTagName('ToUserName'));
        Ele.cloneNode();
        Ele.hasAttributes();
        Ele.isSameNode(Ele.getElementByTagName('ToUserName'));
        Ele.ownerDocument();
        //Ele.removeChild(Ele.getElementByTagName('ToUserName'));
        
        xmldm.dumpAll();
        /*
        XMLDom.Element eld = XMLDom.Element();
        //xmldm.Element('123');
        //xmldm.Element();
        eld.getElementByTagName('123');
        eld.getValue('123');
        eld.getElementByTagName('123');
        eld.dump('123');
       */
        
        
    }
}