public inherited sharing class EFLLocationRelatedContactTriggerHandler {
    
    //W-031437 - Required Field Validation Fix
    public static void validateFieldFormatAndRequired(List<Related_Contact__c> contacts){     
        
        //System.debug('Inside the validate field format for EFLLocationRelatedContactTriggerHandler');  
        
        String USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;
        string requiredMessage = 'You must enter a value';
        
        for (Related_Contact__c con : contacts){
            
            //Required Field Validations
            if (con.First_Name__c == null){
                con.First_Name__c.addError(requiredMessage);
            }
            if (con.Name == null){
                con.Name.addError(requiredMessage);
            }
            if (con.Phone__c == null){
                con.Phone__c.addError(requiredMessage);
            }
            if (con.Email__c == null){
                con.Email__c.addError(requiredMessage);
            }
            
            //validate zipcode fields || added pattern matches condition as per the ticket 31672
            /*if (con.Country__c == usCountryId && con.Zip__c != null ){
if (!Pattern.matches('[0-9,;-]*',con.Zip__c) || !(con.Zip__c.length() >= 5 && con.Zip__c.length() <= 10)){
con.Zip__c.addError('Zip Code Must Be Numeric and be 5-10 digits');
}
} */
            if (con.Country__c == usCountryId && con.Zip__c != null ){
                if(!EFLGenericUtility.ValidateZip(con.Zip__c)) con.Zip__c.addError(EFLGenericUtility.getMessage('EFLLocationErrorFailZip'));
            }
            
            
            //validate phone fields
            /*if (con.Phone__c != null && !con.Phone__c.isNumeric() && ((!con.Phone__c.startsWith('+') && con.Phone__c.length() < 10) || con.Phone__c.startsWith('+'))){
//check if US number (does not lead with +) or international (leads with +)
if (!con.Phone__c.isNumeric() ){

}
con.Phone__c.addError('For US phone numbers, provide a 10 digit phone number. For Foreign phone numbers, list a "+" at the beginning of the phone number.');
}

//validate phone fields
if (con.Phone__c != null && !con.Phone__c.isNumeric() && ((!con.Phone__c.startsWith('+') && con.Phone__c.length() < 10) || con.Phone__c.startsWith('+'))){
//check if US number (does not lead with +) or international (leads with +)
con.Phone__c.addError('For US phone numbers, provide a 10 digit phone number. For Foreign phone numbers, list a "+" at the beginning of the phone number.');
}

if (con.Alternate_Phone__c != null && !con.Alternate_Phone__c.isNumeric() && ((!con.Alternate_Phone__c.startsWith('+') && con.Alternate_Phone__c.length() < 10) || con.Alternate_Phone__c.startsWith('+'))){
//check if US number (does not lead with +) or international (leads with +)
con.Alternate_Phone__c.addError('For US phone numbers, provide a 10 digit phone number. For Foreign phone numbers, list a "+" at the beginning of the phone number.');
}*/
            
        }
    }
    
}