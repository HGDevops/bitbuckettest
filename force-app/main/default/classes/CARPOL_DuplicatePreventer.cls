public with sharing class CARPOL_DuplicatePreventer {

      Map<Id, AC__c> newLine;
      Set<ID> setLineIds = new Set<ID>();
    
      public CARPOL_DuplicatePreventer(Map<Id, AC__c> newTriggerLines) 
        {
          newLine = newTriggerLines;
        }
     
      public void duplicatePreventer()  
       {
               AC__c newLineItem = new AC__c();
               newLineItem = newLine.values()[0];
               
               String newlineitemRecName = Schema.SObjectType.AC__c.getRecordTypeInfosByID().get(newLineItem.Recordtypeid).getName();
               if (newLineItem.Status__c == 'Saved' && newlineitemRecName != null && newlineitemRecName=='Live Dogs')
                {
                    
                    try{
                           AC__c duplicateline = [SELECT Id
                                                  FROM AC__c 
                                                  WHERE Application_number__c  =: newLineItem.Application_Number__c 
                                                  AND Purpose_of_the_Importation__c = :newLineItem.Purpose_of_the_Importation__c 
                                                  AND Microchip_Number__c=:newLineItem.Microchip_Number__c 
                                                  AND Tattoo_Number__c=:newLineItem.Tattoo_Number__c 
                                                  AND Breed__c=:newLineItem.Breed__c
                                                  AND Breed_Description__c=:newLineItem.Breed_Description__c 
                                                  AND Color__c=:newLineItem.Color__c 
                                                  AND Date_of_Birth__c=:newLineItem.Date_of_Birth__c 
                                                  AND Sex__c=:newLineItem.Sex__c 
                                                  AND Other_identifying_information__c =: newLineItem.Other_identifying_information__c
                                                  AND Animal_Name__c=:newLineItem.Animal_Name__c];
                            if (duplicateline!=null) 
                                {
                                  newLineItem.AddError('A line already exists for this animal. Please update the record for a different animal or click cancel.');
                                }
                        }
                     catch(Exception e)
                        {
                            system.debug('Exception in CARPOL_DuplicatePreventer is '+e);
                        }
                } else if((newLineItem.Status__c == 'Saved' || newLineItem.Status__c == 'Ready to Submit') && newlineitemRecName != null && (newlineitemRecName=='Live Dogs' || newlineitemRecName=='Live Animals - Aquaculture')){
                    //system.debug('@@@entered');
                    try{
                           AC__c duplicateline1 = [SELECT Id
                                                  FROM AC__c 
                                                  WHERE Application_number__c  =: newLineItem.Application_Number__c 
                                                  AND Exporter_Last_Name__c =: newLineItem.Exporter_Last_Name__c
                                                  AND Exporter_First_Name__c =: newLineItem.Exporter_First_Name__c
                                                  AND Importer_First_Name__c =: newLineItem.Importer_First_Name__c
                                                  AND Importer_Last_Name__c =: newLineItem.Importer_Last_Name__c
                                                  AND Port_of_Entry__c =: newLineItem.Port_of_Entry__c
                                                  AND Quantity__c =: newLineItem.Quantity__c
                                                  AND Unit_of_Measurement__c =: newLineItem.Unit_of_Measurement__c
                                                  AND Proposed_date_of_arrival__c =: newLineItem.Proposed_date_of_arrival__c
                                                  Limit 1];
                            if (duplicateline1!=null) 
                                {
                                  newLineItem.AddError('A line already exists for this animal. Please update the record for a different animal or click cancel.');
                                }
                        }
                     catch(Exception e)
                        {
                            system.debug('Exception in CARPOL_DuplicatePreventer is '+e);
                        }
                }
        }

}