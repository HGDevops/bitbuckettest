public inherited sharing class EFLIntegrationDocReferenceRepository {
    public static List<EFLIntegrationDocReference__c> selectByLastModified(String objectType, Date queryDate, Boolean flag){
        try{
            return [SELECT Id, Document_URL__c, Document_ID__c, Object_Name__c,Object_Type__c,CBP_SentFlag__c from EFLIntegrationDocReference__c where 
                    Object_Type__c = :objectType and LastModifiedDate > :queryDate and CBP_SentFlag__c = :flag ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLIntegrationDocReferenceRepository.selectByLastModified()',e);                
        } 
        return null;
    }
    public static List<Authorizations__c> selectByAuth(Map<String, Blob> mapBlobAuth){
        try{
            return [SELECT Application__c, Application__r.Name,Application__r.Id, Application__r.RelatedApplication__c, Application_CBI__c, 
                    Applicant__r.Firstname,Applicant__r.Lastname, Applicant__r.Account.Name,Application_Type__c,Authorization_Type__c,Date_Issued__c,
                    Effective_Date__c,Expiration_Date__c,Permit_Number__c,Status__c, Prefix_CBP__c, Name, prefix__c, Parent_Authorization__c FROM Authorizations__c 
                    WHERE Name in :mapBlobAuth.keyset()];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLIntegrationDocReferenceRepository.selectByAuth()',e);                
        } 
        return null;
    }
}