global inherited sharing class CARPOL_UNI_CBPScheduable implements Schedulable {
 
        global void execute(SchedulableContext sc)
        {
            callSchedule();
        }

        public void callSchedule()
        {
            EFLBatchCBPXMLClass.getXML();
        }
}