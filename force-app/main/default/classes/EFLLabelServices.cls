public inherited sharing class EFLLabelServices {
    
    public static integer EMAILS_INVOKED = 0;
    
    public static void sendLabelExpirationEmails(string authMapString){
        try{
            map<Id, set<decimal>> authMap = (map<Id, set<decimal>>)JSON.deserialize(authMapString, map<Id, set<decimal>>.class);
            list<Authorizations__c> auths = [SELECT Id, Applicant__c, AC_Applicant_Email__c, Applicant__r.Email FROM Authorizations__c WHERE Id IN :authMap.keySet()];
            
            // get the VF email template first.
            EmailTemplate vfTemplate = [SELECT Id, Name, Body FROM EmailTemplate WHERE Name = 'Labels Voided Notification - Applicant' LIMIT 1];
            
            // now, loop through them all and render the emails.
            list<Messaging.SingleEmailMessage> emails = new list<Messaging.SingleEmailMessage>();
            Id orgWideEmailId = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'aphis.efile@aphis.usda.gov'][0].Id;
            for(Authorizations__c auth : auths){
                if(auth.Applicant__r.Email != null){
                    Id contactId = auth.Applicant__c;
                    Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(vfTemplate.Id, contactId, auth.Id);
                    email.setToAddresses(new String[]{auth.AC_Applicant_Email__c});
                    email.setSaveAsActivity(false);
                    email.setOrgWideEmailAddressId(orgWideEmailId);
                    emails.add(email);
                }
            }
            
            // now we have a list of compiled email messages - send them all out.
            if(!emails.isEmpty()){
                Messaging.sendEmail(emails);
            }
        }catch(Exception e){
            EFLErrorLog.createErrorLog('EFLLabelServices',e);
        }
    }
    
    /*
* @description: given a set of Authorizations__c Ids, set all of the related labels that were active to void.
* */
    public static void changeLabelStatus(set<Id> authIds, string oldStatus, string newStatus){
        system.debug('********************changeLabelStatus');
        list<Label__c> labels = [SELECT Id FROM Label__c WHERE Status__c = :oldStatus AND Authorization__c IN :authIds];
        if(!labels.isEmpty()){
            for(Label__c lab : labels){
                lab.Status__c = newStatus;
            }
            try{
                update labels;
            }catch(Exception e){
                system.debug('changeLabelStatus exception : ' + e.getMessage());
                EFLErrorLog.createErrorLog('EFLLabelServices', e);
            }
        }
    }
}