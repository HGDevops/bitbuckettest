/*-----------------------------------------------------------------------  
@Purpose: This Utility class helps in handling build apex Email Content
----------------------------------------------------------------------*/
public inherited sharing class EFLEmailUtility {
    
    public static Messaging.SingleEmailMessage singleEmailMessage;
    public static List<String> toAddresses;
    
    //optional parameters set to default        
    private static String subject = '';
    private static String htmlBody = ''; 
    private static Boolean useSignature = false;
    public static ID TemplateId;
    public static ID targetObjectId;
    public static ID whatId;
    
    //defaults to current user's first name + last name
    private static String senderDisplayName = 'eFile Support';//UserInfo.getFirstName()+' '+UserInfo.getLastName();
    
    //get the current user in context
    User currentUser = [Select email from User where username = :UserInfo.getUserName() limit 1];        
    
    //replyTo defaults to current user's email 
    private static String replyTo =  'support@efile.com';
    private static String plainTextBody = '';
    
    
    //where it all comes together
    //this method is private and is called from sendEmail()
    private static void emailMessage() {
        singleEmailMessage = new Messaging.SingleEmailMessage();
        singleEmailMessage.setToAddresses(toAddresses);
        singleEmailMessage.setSaveAsActivity(true); 
        singleEmailMessage.setSenderDisplayName(senderDisplayName);
        singleEmailMessage.setUseSignature(useSignature);
        singleEmailMessage.setReplyTo(replyTo);
        singleEmailMessage.settargetObjectId(targetObjectId);
        singleEmailMessage.setTreatTargetObjectAsRecipient(false);
        singleEmailMessage.setWhatId(whatId);
        singleEmailMessage.setTemplateId( TemplateId);
    }
    
    //send the email message
    public static void sendEmail() { 
        try {
            emailMessage();
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { singleEmailMessage });
        } catch (Exception e) {
            EFLErrorLog.createErrorLog('EFLEmailUtility.sendEmail()',e);
            throw e;
        }                
    }
    
    /* RecipientEmail Setup Mappping from Custom Metadata - EFLRecipientEmail__mdt */
    public static EFLRecipientEmail__mdt getRecipientSetup(string CMDLabel)
    {
        return [select Id, developername, EmailField__c, Roles__c, ParentFieldApiName__c from EFLRecipientEmail__mdt where Label = :CMDLabel limit 1];
    }
    
    /* Id of EmailTemplate for the DeveloperName */
    public static Id getEmailTemplateId(string developerName)
    {
        return [select Id, developername from EmailTemplate where developername = :developerName limit 1].id;
    }
    
    //custom exception      
    public class emailException extends Exception{
        
    }
    
}