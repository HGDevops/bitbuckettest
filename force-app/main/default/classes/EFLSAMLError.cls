public with sharing class EFLSAMLError {

    public string ErrorDetails {
    get
    {
        ErrorDetails = null;
        if(ApexPages.currentPage().getParameters().get('ErrorDetails')!=null)
        {
           ErrorDetails = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('ErrorDetails') );
           //To remove ApexStandardUserHandler:StandardUserHandler.JitException from the custom exception
           if(ErrorDetails.contains(':'))
           {
               List<string> errorSplit = ErrorDetails.split(':');
               ErrorDetails = errorSplit[errorSplit.size()-1];  
           }   
        }
        return ErrorDetails;
    } 
    set;}
    
}