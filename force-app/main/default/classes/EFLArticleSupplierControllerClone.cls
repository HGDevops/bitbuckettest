/*
 * Author: Ravee Racharla Date:11/8/2018
 * Purpose: Controller Class for Article Supplier/Developer Page.
*/
public with sharing class EFLArticleSupplierControllerClone {
/* COMMENTING OUT ALL LINES SINCE THIS CLASS SHOULD BE DELETED.  JB 4/29/2019
 *
    public Id lineItemId {get;set;}
    public string delrecid {get;set;}
    public Map<string,string> sobjectkeys {get;set;}
    public Article_Supplier_Developer__c asdRecord {get;set;}
    public id asdID {get;set;}
    public Boolean regArtEnableDisable {get;set;}
    public string lraExistError {get;set;}
    public List <AC__c> relatedLineItem {get;set;}
    public Boolean showArticleView {get;set;}
    public boolean renderRTDetail {get;set;}
    public id ApplicationID {get;set;}
    public String subCountryId {get;set;}
    public String subStateId {get;set;}
    public String subCountyId {get;set;}
    public String recordTypename {get;set;}
    public id recordTypeId{get;set;}  
    

    public list<Article_Supplier_Developer__c> ASDList {get;set;}
    public AC__c lineItemRecord {get;set;}
    public Boolean lockLineItem {get;set;}
    public Boolean program{get;set;}
    public Boolean enableApplicantInstructions {get;set;}
    public String instructions {get;set;}
   // public string currentStep {get;set;}
    public String USCountryID; 


    //Rerender Attributes
    public boolean renderList {get;set;}
    public boolean renderDetail {get;set;}
    public integer impcrectype;
    public integer exptrectype;
    public integer dlvreptrectype;

    public boolean isEdit;
    
    Id artsupplrectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Info type');
    Id imprectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Importer');
    Id deliveryrecptrectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Delivery');
    Id exprectypeid = EFLGenericUtility.getRecordTypeId('Article Supplier/Developer Exporter');

    public EFLArticleSupplierControllerClone(ApexPages.StandardController controller) {
        lineItemId = ApexPages.currentPage().getParameters().get('LineItemId'); 
        init();
    }

    public void init() {
        getASDList();
        getLineItemRecord();
        getRelatedLineItem();
        setLockLineItem();
        program();
        getApplicationInstructions();
        getInstructions();
       // getcurrentStep();

        renderDetail = false;
        renderList = true;
        renderRTDetail = false;
        recordtypename = null;
        recordtypeid = null;
       
        USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;
        
        //setRecordtype();
        Map<string,Schema.SobjectType> describe=Schema.getGlobalDescribe();
        sobjectkeys = new Map<string,string>();
        for (string s:describe.keyset()) {
            sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
        } 
    }
     

    public void getInstructions() {
    
        if (instructions == NULL) {
        if(lineitemRecord.Thumbprint__r.REF_Program_Name__c==EFLGlobalConstants.getConstantValue('PROGRAM AC'))
                {
                    instructions =  EFLGenericutility.getInstructions(NULL,NULL,'Importer/Exporter/Recipient');
                }
                else
                {
            instructions =  EFLGenericutility.getInstructions(NULL,NULL,'Article Supplier');
        }
   }     
 }   

    public void getApplicationInstructions() {
        enableApplicantInstructions = EFLLockUnlockUtility.enableApplicantInstructions(lineItemRecord);
        renderRTDetail = true;
    }

    public void setLockLineItem() {
        //Ravee Racharla 11/15/2018 
        //lockLineItem = EFLLockUnlockUtility.lockLineitem(lineItemRecord);
        if (lineitemRecord.Status__c == 'Draft' || lineitemRecord.Status__c == 'Saved' || lineitemRecord.Status__c == 'Ready to Submit') {
            lockLineItem = true;
        } else {
            lockLineItem = false;
        }
    }
    
     public void program() {
      if (lineitemRecord.Thumbprint__r.REF_Program_Name__c== EFLGlobalConstants.getConstantValue('PROGRAM BRS')) {
             program = true;
        } else {
            program = false;
        }
    }
    
    public Boolean getIsFieldRequired(){
        Boolean isRequired = false;
        if(recordtypeid == imprectypeid  || recordtypeid == deliveryrecptrectypeid ){
            isRequired = true;
        } 
        return isRequired;
    } 
    
    public Boolean getIsFieldReadonly(){
        Boolean isReadonly = false;
        if(recordtypeid == imprectypeid  || recordtypeid == deliveryrecptrectypeid ){
            isReadonly = true;
        } 
        return isReadonly;
    }   
       
     
     public string currentStep
    {
     get
     {
        if(lineitemRecord.Thumbprint__r.REF_Program_Name__c==EFLGlobalConstants.getConstantValue('PROGRAM AC'))
      {
         // currentStep = 'Importer/Exporter/Delivery';
            currentStep = 'Importer/Exporter/Recipient';
      }
      else
      {
          currentStep = 'Supplier/Developers';
      }
      return currentStep;
     }
     set;
    }
      
    public void getRelatedLineItem() {
        relatedLineItem = new List<AC__c>();
        relatedLineItem = [SELECT Id, Does_This_Application_Contain_CBI__c FROM AC__c where Id = :lineItemId];
    }

    public void getLineItemRecord() {
        if(lineItemRecord == null && LineItemId != null) {
            lineItemRecord = efllineitemrepository.selectbyID(lineItemID);
            ApplicationID = lineitemrecord.Application_Number__c;
        }
    }

    public void getASDList() {
        if (lineItemID != NULL) {
            ASDList = EFLArticleSupplierRepository.selectASDByLineItemID(lineItemID);
        }
        renderRTDetail = false;
        system.debug('ASDList###'+ASDList);
    }

    public void showArticleDetailView() {
        System.debug('showArticleDetailView');
        System.debug('@K@K@KasdID: '+asdID);
        asdRecord = EFLArticleSupplierRepository.selectByID(asdID);
        system.debug('@K@K@K@KasdRecord ' +asdRecord);
        showArticleView = true;
        // recordtypeid = asdRecord.RecordTypeId;


        //displayDetailsSection();
        renderDetail = false;
        renderList = false; 
        renderRTDetail = false;
    }
    
    public void displayRTSelectionPage(){
        System.debug(LoggingLevel.INFO,'Inside displayRTSelectionPage');
        renderList = false;
        renderRTDetail = true;
        renderDetail = false;
    }   
    
     private void setRecordtype()
    {
         if(lineItemID !=null){    
        list<RecordType> rts = [SELECT ID,name, DeveloperName FROM RecordType WHERE ID = :recordTypeId];
               if(rts.size()>0){
                
                   for(RecordType r : rts){
                          recordTypename = r.name; 
                     asdRecord.RecordTypeId=recordtypeid;                        
                      } 
              }
         }
    }
    
    public void saveRTSelection(){
         impcrectype = 0;
         dlvreptrectype= 0;
         exptrectype= 0;
         If(recordTypeId ==null ){
            System.debug(LoggingLevel.DEBUG,'Record Type Not Selected');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Select Record Type.'));
            return;
        }
        asdRecord = new Article_Supplier_Developer__c(RecordTypeId=recordTypeId,Line_Item__c = lineItemID);
        asdRecord.RecordTypeId=recordtypeid;
        
  
        setRecordtype();
          list<Article_Supplier_Developer__c> listARTSupp = [select id, recordTypeId from Article_Supplier_Developer__c where Line_Item__c =: lineItemId];
         for(Article_Supplier_Developer__c artsup:listARTSupp ){
                    if(artsup.recordtypeid == imprectypeid )
                       impcrectype =  impcrectype+1;
                    if(artsup.recordtypeid == deliveryrecptrectypeid)
                       dlvreptrectype = dlvreptrectype+1;
                    if(artsup.recordtypeid == exprectypeid )
                       exptrectype= exptrectype+1;
                } 
                
                If(impcrectype >= 1 && imprectypeid == recordTypeId ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 Importer'));
                } else if(dlvreptrectype>= 1 && deliveryrecptrectypeid == recordTypeId ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 Delivery Recepient'));
                } else if(exptrectype>= 1 && exprectypeid == recordTypeId ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 Exporter'));
                } 
          else{      
       showDetail();
       }  
      // showDetail();
        }
        
        
    public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        list<RecordType> recTypes = [SELECT ID, name FROM RecordType WHERE SObjectType = 'Article_Supplier_Developer__c' ORDER BY name] ;
        if(lineitemRecord.Thumbprint__r.REF_Program_Name__c==EFLGlobalConstants.getConstantValue('PROGRAM BRS')){                    
               options.add(new SelectOption(artsupplrectypeid, 'Article Supplier/Developer'));
                }
        if(lineitemRecord.Thumbprint__r.REF_Program_Name__c==EFLGlobalConstants.getConstantValue('PROGRAM AC')){                    
               options.add(new SelectOption(imprectypeid, 'Importer'));
               options.add(new SelectOption(deliveryrecptrectypeid, 'Delivery Recipient'));
               options.add(new SelectOption(exprectypeid,  'Exporter'));
                }
        
        options.add(0,new SelectOption('','--Select--'));
        return options;
    
            }

    public void showDetail() {
        lraExistError = null;
        asdRecord = new Article_Supplier_Developer__c();
        system.debug('asdID@@'+ asdID);
        if (asdID!=null) {
            regArtEnableDisable = false;
            asdRecord = EFLArticleSupplierRepository.selectByID(asdID);
        } else {
            regArtEnableDisable = true;
            asdRecord = new Article_Supplier_Developer__c();

        }
         system.debug('USCountryID1'+USCountryID);
                if (recordtypeid == imprectypeid || recordTypeId == deliveryrecptrectypeid ){
                asdRecord.Country__c= USCountryID;
        }
        renderDetail = true;
        renderList = false;
        renderRTDetail = false;

    }

    public void populateApplicantInfo() {
        lraExistError = null;
       // Populateinfo  = true;
        
        if (asdRecord == null) {
            asdRecord = new Article_Supplier_Developer__c();
        }
        //KA
       Contact objAppContact = new Contact();
        objAppContact = EFLArticleSupplierRepository.populateApplicantInfo(ApplicationID); 
        if (objappContact != null ) {
            asdRecord.First_Name__c=objappContact.FirstName;
            asdRecord.name=objappContact.Name ;
            asdRecord.Organization_Name__c=objappContact.Account.id;
            asdRecord.Email__c=objappContact.Email;
            asdRecord.Phone__c=objappContact.Phone;
            //asdRecord.Alternate_Phone__c =objapp. ;
            asdRecord.Street_Address__c =objappContact.Mailing_Street_LR__c;
            asdRecord.City__c=objappContact.Mailing_City_LR__c;
            asdRecord.Postal_Code__c=objappContact.Mailing_Zip_Postalcode_LR__c;
            asdRecord.state__C=objappContact.Mailing_State_Province_LR__c;
            asdRecord.country__C=objappContact.Mailing_Country_LR__c;
        }
        
        
        renderDetail = true;
        renderList = false;

    }

    public void cancel() {
        asdID =null;
        renderDetail = false;
        renderList = true;
       renderRTDetail = false;

    }

    public void updateASD() {
        try {
            if (asdRecord.line_Item__c ==null) {
                asdRecord.line_Item__c = lineItemID;
            }
            system.debug('asdRecord1'+asdRecord);
             asdRecord.RecordTypeId=recordtypeid; 
            upsert asdRecord;
           renderDetail = false;
            renderList = true;
            asdRecord = null;
            asdID = null;
            recordtypeid = null;
                      
            ASDList = EFLArticleSupplierRepository.selectASDByLineItemID(lineItemID);
        } catch( Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
    }

    public void deleteRecord() {
        system.debug('deleteRecord');
        string sobjkey=delrecid.substring(0,3);
        string sobjname=sobjectkeys.get(sobjkey);
        string strqurey='select id from '+sobjname+' where id=:delrecid';
        //cons.deleteConstructRecord();
        list<sobject> lst=database.query(strqurey);
        Delete lst;
    }

    public void processCountrySelection() {
        if (!String.isBlank(subCountryId)) {
            asdRecord.Country__c = subCountryId;
        }

        if (!String.isBlank(subStateId)) {
            asdRecord.State__c = subStateId;
        }
        if (!String.isBlank(subCountyId)) {
            asdRecord.Level_2_Region__c = subCountyId;
        }
    }
*/

}