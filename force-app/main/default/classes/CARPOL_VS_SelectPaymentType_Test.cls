@isTest(seealldata=false)
private class CARPOL_VS_SelectPaymentType_Test {
      @IsTest static void CARPOL_VS_SelectPaymentType_Test() {

          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 

          Test.startTest();
          PageReference pageRef = Page.CARPOL_VS_SelectPaymentType;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
          ApexPages.currentPage().getParameters().put('id',objapp.id);
          CARPOL_VS_SelectPaymentType extclass = new CARPOL_VS_SelectPaymentType(sc);
          extclass.getPaymentTypeOptions();
          extclass.savePaymentType();
          system.assert(extclass != null);          
          Test.stopTest();     
      }
}