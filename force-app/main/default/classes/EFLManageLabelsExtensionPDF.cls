/**
* Author : Kishore Kumar
* Created Date : 05/20/2017
* Purpose : This is used to Generate Labels, Attach and Send Labels to Applicant via email for all pathways
* Releated Pages: EFLManageLabels
*/
public with sharing class EFLManageLabelsExtensionPDF{
/*************************************************************************************** 
 ************************************   attributes ******************************************
 **********************************************************************************************/
    public Authorizations__c au{get;set;}
    public string delrecid{get;set;}
    public Map<string,string> sobjectkeys{get;set;}
    public Authorizations__c authorization{get;set;}
    public Integer AdditionalLabels{get;set;}
    public Boolean BRSProgram{get;set;}
    public Boolean PPQProgram{get;set;} 
    public string labelcolor{get;set;}
    public Boolean btnvisibility{get;set;}
    public list<Label__c> lstlabels=new  list<Label__c>();
    public boolean labelsexist{get;set;}
    public List<Label__c> labelList1;
    public Integer totallabels{get;set;}
    public List<Label__c> additionallabelslist{get;set;}
    public string btntype{get;set;}
    public string Addlabels{get;set;}
    public string Alllabels{get;set;}
    public list<Label__c> labelfinallist{get;set;}
    public list<Label__c> ActiveLabellist{get;set;}
    public string buttontitle {get;set;}
    private final static string INSTRUCTIONS = 'Instructions';
    private final static String PPQ_GY_Label = 'PPQ GY Label';
    private final static String PPQ_RW_Label = 'PPQ RW Label';
    private final static String PPQ_BW_Label = 'PPQ BW Label';
    private final static String BRS_BW_Label = 'BRS BW Label';
    
    
    public string labelGuidanceTemplate{
        get{
            if(labelGuidanceTemplate==null){
                //Pick up the instructions for the correct label color
                String labelcolor;
                if(au != null){
                    if(au.Label_Colors__c == 'Black and White'){
                        labelcolor = PPQ_BW_Label;
                    } 
                    if(au.Label_Colors__c == 'Red and White'){
                        labelcolor = PPQ_RW_Label;
                    } 
                    if(au.Label_Colors__c == 'Green and Yellow'){
                        labelcolor = PPQ_GY_Label;
                    }   
                    if(au.program__c=='BRS'){
                        labelcolor = BRS_BW_Label;
                    }  
                                                        
                }
               labelGuidanceTemplate = [select Template_Name__c from EFLLabelFormat__mdt WHERE Label = :labelcolor limit 1].Template_Name__c; 
            }
            return labelGuidanceTemplate;
        }
        set;
    }
    public string labelGuidanceContent{ 
        get{
            if(labelGuidanceContent == null){
                labelGuidanceContent = [select content__c from communication_manager__c
                                         where name =: labelGuidanceTemplate 
                                           and type__c =: INSTRUCTIONS 
                                         limit 1].content__c;
            }
            return labelGuidanceContent;
       }
        set;
    }
    
/*************************************************************************************** 
 ************************************   Constructor ******************************************
 **********************************************************************************************/
    public EFLManageLabelsExtensionPDF(ApexPages.StandardController controller){
        ActiveLabellist =new  list<label__c>();
        additionallabelslist=new  list<label__c>();
        labelList1=new  list<label__c>();
        Addlabels='Additional';
        Alllabels='All';
        this.au=(Authorizations__c)controller.getRecord();
        if(au.Id!=null){
            au=[SELECT Name,Authorization_Type__c,BRS_Create_Labels__c,Total_No_of_Labels__c,Label_Colors__c,program__c,
                Program_Pathway__r.Permit_PDF_Template__c,Program_Pathway__r.program__r.name,Status__c,Id,Response_Type__c,RecordTypeId,
                RecordType.Name,Application__c,Template__c,Thumbprint__c,Effective_Date__c,BRS_Number_of_Labels__c,Expiry_Date__c,
                pathway_exp_days__c,Expiration_Date__c,Plant_Inspection_Station__c,Prefix__c, (SELECT name FROM Labels__r) 
                FROM Authorizations__c 
                WHERE Id=:au.Id];
            if(au.program__c!=null&&au.program__c=='BRS'){
                BRSProgram=true;
                PPQProgram=false;
            }
            else{
                PPQProgram=true;
                BRSProgram=false;
            }
            btnvisibility=true;
            buttontitle = 'Create Labels';
            if(au.Labels__r.size()>0)btnvisibility=false;
            if(ApexPages.currentPage().getParameters().get('btntype')!=null){
                btntype = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('btntype') );
            }
            getlabelList();
            addlabels();
            labelfinallist1();
      }
    }
    
/*************************************************************************************** 
 ************************************   labels List ******************************************
 **********************************************************************************************/    
    public List<Label__c> getlabelList(){
        for(List<Label__c> lbl:[SELECT id,Name,QRcode_Barcode__c,Label_Sent__c,Notification_Issue_Date__c,Notification_Expiration_Date__c,
                                Status__c,createddate,LastModifiedDate, AutoNumber__c, Plant_Inspection_Station_lkup__r.Address_1__c,
                                Plant_Inspection_Station_lkup__r.Address_2__c, Plant_Inspection_Station_lkup__r.City__c, Plant_Inspection_Station_lkup__r.State_Code__c,
                                Plant_Inspection_Station_lkup__r.Zip__c
                                FROM Label__c 
                                WHERE Authorization__c=:au.Id 
                                ORDER BY AutoNumber__c ASC])
        {   
            labelList1=lbl; 
            
        }
        
        labelsexist=true;
        totallabels=labelList1.size();
        return labelList1;
    } 
    
/*************************************************************************************** 
 ************************************   Additional Labels List ******************************* 
 **********************************************************************************************/    
    public void addlabels(){
        
        if(labelList1.size()>0){
            for(label__c lab:labelList1)
            if(lab.Label_Sent__c==false && lab.status__c != 'Voided-Expired')
              additionallabelslist.add(lab);
        }
    }
    
/*************************************************************************************** 
 ************************************   Final Label list ************************************ 
 **********************************************************************************************/    
    public list<Label__c> labelfinallist1(){
        if(btntype=='Additional')
           labelfinallist=additionallabelslist.clone();
        else if(btntype=='All'){
               //labelfinallist=labelList1.clone();
               for(label__c lbl : labelList1){ 
                   if(lbl.status__c != 'Voided-Expired') 
                      ActiveLabellist.add(lbl);
               }
                labelfinallist=ActiveLabellist.clone(); 
        }
        
        return labelfinallist;
    }

/*************************************************************************************** 
 ************************************   Preview All and Additional Labels ****************** 
 **********************************************************************************************/
    public PageReference viewDraftPDF(){
        PageReference pageRef;
        if(au.program__c=='BRS')
           pageRef=Page.CARPOL_BRS_PDF_Labels;
        else{
            pageRef=Page.EFLPPQLabelPDF;
            pageRef.getParameters().put('color',au.Label_Colors__c);
        }
        pageRef.getParameters().put('Id',au.Id);
        pageRef.getParameters().put('btntype',btntype);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
/*************************************************************************************** 
 **************** Attach Labels and Send Email to Applicant(NOT USED) **********************     
 **********************************************************************************************/    
    public PageReference attachPDF(){
        PageReference pageRef;
        if(labelsexist==true){
            if(au.program__c=='BRS')CARPOL_BRS_Approval.brslabelspdf(au.id);
            else CARPOL_PPQ_sendlabels.brslabelspdf(au.id);
        }
        else{
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please generate the Labels before attaching.'));
            return null;
        }
        return null;
    }
    
/*************************************************************************************** 
 ************************************ Return to Authorization ****************************** 
 **********************************************************************************************/    
    public PageReference cancel(){
        PageReference return2Auth=new  PageReference('/'+au.Id);
        return2Auth.setRedirect(true);
        return return2Auth;
    }
    
/*************************************************************************************** 
 ************************************   Create labels ************************************ 
 **********************************************************************************************/    
    public PageReference Createlabels(){
        string authrt='';
        string authid='';
        string laboriginadd='';
        string authautono='';
        integer nooflabs=0;
        Date authexpdt;
        boolean iscreatelabel=false;
        getlabelList();
        Map<ID,Schema.RecordTypeInfo> locrtMap=Schema.SObjectType.Location__c.getRecordTypeInfosById();
        if(au.Status__c=='Approved'||au.Status__c=='Issued'){
            authid=au.id;
            authautono=au.Name;
            /*
            if(au.Labels__r.size()>0){
                if(additionallabels!=null && additionallabels!=0)
                   nooflabs=additionallabels;
                else{
                  ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a value in Additional Labels field to create more labels.');
                  ApexPages.addMessage(message); 
                  return null;                  
                }
            }
            
            else if(au.BRS_Number_of_Labels__c!=null)
                nooflabs= Integer.valueOf(au.BRS_Number_of_Labels__c);
            */ 
              
            if(additionallabels!=null && additionallabels!=0)
               nooflabs=additionallabels;
            else{
              ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a value in Additional Labels field to create more labels.');
              ApexPages.addMessage(message); 
              return null;                  
            }
            
            if(au.Expiration_Date__c !=null)
               authexpdt=au.Expiration_Date__c;
            if(nooflabs!=null){
                for(Location__c orl:[SELECT id,Name,RecordTypeId FROM Location__c WHERE Authorization__c=:authid]){
                    string orgrtname='';
                    orgrtname=locrtMap.get(orl.RecordTypeId).getName();
                    if(orgrtname=='Origin Location')
                       laboriginadd=orl.id;
                }
                
                if(nooflabs>0&&nooflabs<999){
                    integer labelseries = 0;
                    if(au.Labels__r.size()>0)
                       labelseries = au.Labels__r.size();
                    lstlabels.clear();   
                    for(integer i=labelseries+1;i<labelseries+nooflabs+1;i++){
                        Label__c l=new  Label__c();
                        l.Name=authautono+'-'+i;
                        l.Authorization__c=authid;
                        l.Notification_Expiration_Date__c = authexpdt;
                        l.Notification_Status__c='Active';
                        l.Status__c='Active';
                        if(laboriginadd!=''&&laboriginadd!=null)l.Origin_Address__c=laboriginadd;
                        lstlabels.add(l);
                    }
                }
                
                if(lstlabels.size()>0){
                    Insert lstlabels;
                    lstlabels.sort();
                    if(!au.BRS_Create_Labels__c)
                        au.BRS_Number_of_Labels__c = additionallabels;
                    au.BRS_Create_Labels__c=true;
                    Update au;
                  
                    btnvisibility = false;
                    ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Labels have been created successfully.');
                    ApexPages.addMessage(message); 
                    return null; 
                }
            }
        }
        return null;
    }
    
/*************************************************************************************** 
 ************************************   Attach Labels and Send Email to Applicant  ****** 
 **********************************************************************************************/    
    public PageReference sendlabels(){
        string labelPageName;
        string labelPDFName;
        btntype='Additional';
        getlabelList();
        addlabels();
        labelfinallist1();
        integer labelversion=[SELECT count() FROM Attachment WHERE ParentID=:au.id AND name LIKE'%Label%'];
        labelversion=labelversion+1;
        if(au.program__c=='BRS'){
            labelPageName='CARPOL_BRS_PDF_Labels';
            labelPDFName=au.name+'_Label_V'+labelversion;
        }
        else{
            labelPageName='EFLPPQLabelPDF';
            labelPDFName=au.name+'_Label_V'+labelversion;
        }
        EFLAttachandSendLabels.generateShippingLabelsPDF(au.id,true,labelPageName,labelPDFName,btntype);
        ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Labels have been sent successfully to Applicant.');
        ApexPages.addMessage(message);      
        return null;
    }
    
/*************************************************************************************** 
 ************************************   Redirect to same page ***************************** 
 **********************************************************************************************/    
    public PageReference Redirect(){
        btnvisibility = false;
        PageReference dirpage=new  PageReference('/apex/EFLManageLabels?Id='+au.id);
        dirpage.setRedirect(true);
        return dirpage;
    }
    
/*************************************************************************************** 
 ************************************  Delete any Label record *****************************  
 **********************************************************************************************/    
    public PageReference deleteRec(){
        string strqurey='select id from Label__c where id=:delrecid';
        list<sobject> lst=database.query(strqurey);
        Delete lst;
        PageReference dirpage=new  PageReference('/apex/EFLManageLabels?Id='+au.id);
        dirpage.setRedirect(true);
        return dirpage;
    }
    
/*************************************************************************************** 
 ************************************  Void any Label record *****************************  
 **********************************************************************************************/    
    public PageReference voidlabels(){
        Update labelList1;
        return null;
    }
}