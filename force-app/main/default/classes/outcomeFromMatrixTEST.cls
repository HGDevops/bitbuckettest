@isTest
private class outcomeFromMatrixTEST {
    
    public static CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
  static CARPOL_Outcome_Matrix__c com1;
  static List<CARPOL_Outcome_Matrix__c> comList = new List<CARPOL_Outcome_Matrix__c>();
  
  private static void init() {
        testData.insertcustomsettings();
        com1 = new CARPOL_Outcome_Matrix__c(Combined_Outcome__c='com1',Combined_Outcome_Score__c=10,Outcome_One__c='com1first',Outcome_Two__c ='com1second',name='com1');
        insert com1;  
        comList.add(com1);
      
  }
  
  private static testMethod Void test1(){
      init();
      
      test.startTest();
      outcomeFromMatrix ofm = new outcomeFromMatrix(comList);
      ofm.getOutcome(com1.Outcome_One__c,com1.Outcome_Two__c);
      ofm.getOutcome(com1.Outcome_Two__c,com1.Outcome_One__c);
      ofm.getOutcome(com1.Outcome_Two__c,com1.Outcome_One__c);
      ofm.getOutcome('returnNull','returnNull');
      test.stopTest();
      
  }

}