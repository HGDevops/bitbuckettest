@isTest(seealldata=true)
public class BRS_HistoryData_utility {
    
        string dlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        string locrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        string oanddlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();
        
    public  List<sObject> dataCreation(String Datatype){
        //String OriginDataType = DataType;
        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Account objacct=testData.newAccount(testData.AccountRecordTypeId);
        Contact objcont=testData.newcontact();
        Application__c objapp=testData.newapplication();
        AC__c ac1=testData.newLineItemBRS('Personal Use',objapp);
        Attachment attach=testData.newattachment(ac1.Id);
        Authorizations__c objauth=testData.newAuth(objapp.Id);
        ac1.Authorization__c=objauth.id;
        Update ac1;
        ID plip = ac1.Program_Line_Item_Pathway__c;
        Regulated_Article__c objRA = testData.newRegulatedArticle(plip);
        objRA.Scientific_Name__c = 'TestScientificName';
        update objRA; // TehL: Added this
        Regulation__c  objreg =testData.newRegulation('type','subtype');
        Authorization_Junction__c authjunc=testData.newAuthorizationJunction(objauth.id,objreg.id);
/**
* line item related records like link regulated article,Construct, Geno and Phenotype & locations
**/
        Link_Regulated_Articles__c linkra=testdata.newlinkRegArticle(ac1.id,objapp.Id,objauth.id);
        linkra.Regulated_Article__c = objRA.id;
        linkra.Regulated_Article_CBI__c = true;
        update linkra;
        construct__c objconst=testdata.newconstruct(ac1.id,objRA);
        GenotypeType__c objgenotype = new GenotypeType__c();
        objgenotype.Construct__c = objconst.id;
        objgenotype.Genotype_Category__c = 'Gene Knock-Out';
        insert objgenotype;
        Genotype__c objgeno=testdata.newgenotype(objconst.id,objgenotype);
        
        //objgeno.Name = 'TestGeno';
        //update objgeno;
        Phenotype__C objpheno=testdata.newphenotype(objconst.id); 
        Country__c UScountry=testdata.newcountryus();
        Level_1_Region__c level1reg=testdata.newlevel1region(UScountry.id);
        Level_2_Region__c level2reg=testdata.newlevel2region(level1reg.id);
        location__c orgloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,olocrectypeid);
        location__c desloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,dlocrectypeid);
        location__c relloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,locrectypeid);
        location__c oanddloc = testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,oanddlocrectypeid); 
        Construct_Application_Junction__c objConstAppJunc = new Construct_Application_Junction__c();
        objConstAppJunc.Construct__c = objconst.id;
        objConstAppJunc.Line_Item__c = ac1.id;
        insert objConstAppJunc;
        string soprectypeid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Standard Operating Procedures').getRecordTypeId();
        string Attachrectypeid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Uploaded Attachment').getRecordTypeId(); 
        Applicant_Attachments__c Attach1 = new Applicant_Attachments__c();
        Attach1.RecordTypeID= soprectypeid;
        Attach1.Animal_Care_AC__c = ac1.id;
        Attach1.File_Name__c = 'File1';
        Attach1.CBI_Deleted_File_Name__c = 'CBIDel';
        Attach1.Does_this_contain_CBI__c = 'Yes';
        insert Attach1;
        
          
/**
* line item change history
**/     
       
         List<sObject> inputs = new List<sObject>();
        
        
        if(DataType=='Construct__c'){
           Construct__c objconst2 = testdata.newconstruct(ac1.id);
            inputs.add(objconst);
            inputs.add(objconst2);
            return inputs;
        }//KM:
        else if(DataType=='Authorizations__c'){
           Authorizations__c objauth2=testData.newAuth(objapp.Id);
            inputs.add(objauth);
            inputs.add(objauth2);
            return inputs; //KM
        }else if(DataType=='Location__C'){
             Location__C oanddloc2 = testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,oanddlocrectypeid);
            inputs.add(oanddloc);
            inputs.add(oanddloc2);
            return inputs; 
        }else if(DataType=='Genotype__c'){
            Genotype__c objgeno2=testdata.newgenotype(objconst.id);
            //List<Genotype__c> objGenolist = [SELECT ID,Name FROM Genotype__c WHERE (ID=:objgeno2.id)];
            inputs.add(objgeno);
            inputs.add(objGeno2);
            return inputs;
        }else if(DataType=='Phenotype__c'){
            Phenotype__C objpheno2=testdata.newphenotype(objconst.id);
            inputs.add(objpheno);
            inputs.add(objpheno2);
            return inputs;
        }else if(DataType=='Link_Regulated_Articles__c'){
            Link_Regulated_Articles__c linkra2=testdata.newlinkRegArticle(ac1.id,objapp.Id,objauth.id);
            linkra2.Regulated_Article__c = objRA.id;
            linkra2.Regulated_Article_CBI__c = true;
            update linkra2;
            inputs.add(linkra);
            inputs.add(linkra2);
            return inputs;
        }else if(DataType=='Applicant_Attachments__c'){
            Applicant_Attachments__c Attach2 = new Applicant_Attachments__c();
            Attach2.RecordTypeID= soprectypeid;
            Attach2.Animal_Care_AC__c = ac1.id;
            Attach2.File_Name__c = 'File1';
            Attach2.CBI_Deleted_File_Name__c = 'CBIDel';
            Attach2.Does_this_contain_CBI__c = 'Yes';
            insert Attach2;
            inputs.add(Attach1);
            inputs.add(Attach2);
            return inputs;
        }else if(DataType=='AC__c'){
            AC__c ac2=testData.newLineItemBRS('Personal Use',objapp);
            inputs.add(ac1);
            inputs.add(ac2);
            return inputs;
        }else if(DataType=='Application__c'){
            Application__c objapp2=testData.newapplication();
            inputs.add(objapp);
            inputs.add(objapp2);
            return inputs;
        }else if(DataType=='Construct_Application_Junction__c'){
            Construct_Application_Junction__c objConstAppJunc2 = new Construct_Application_Junction__c();
            objConstAppJunc2.Construct__c = objconst.id;
            objConstAppJunc2.Line_Item__c = ac1.id;
            insert objConstAppJunc2;
            
            inputs.add(objConstAppJunc);
            inputs.add(objConstAppJunc2);
            return inputs;
        }else if(DataType=='Authorization_Junction__c'){
        	Authorization_Junction__c authjunc1=testData.newAuthorizationJunction(objauth.id,objreg.id);
            inputs.add(authjunc);
            inputs.add(authjunc1);
            return inputs;
        }
        else{
            return null;
        }
        
        // else if(DataType=='Location'){
        //      Location__C oanddloc2 = testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,oanddlocrectypeid);
        //     inputs.add(oanddloc);
        //     inputs.add(oanddloc2);
        //     return inputs;
        // }
        /*Change_History__c chghistory=testdata.newchghistory(ac1.id);
        Change_History__c chghistory2=testdata.newchghistory(ac1.id);
        chghistory2.Construct__c = objconst.id;
        chghistory2.Link_Regulated_Articles__c= linkra.id;
        chghistory2.Location__c = oanddloc.id;
        chghistory2.Line_Item__c = ac1.id;
        update chghistory2;
        Map<sObject,Change_History__c> sObjChangeHisMap = new Map<sObject,Change_History__c>();
        sObjChangeHisMap.put(objConst,chghistory2);
        return sObjChangeHisMap.values;*/
        //}
        //else{
          //  return null;
        //}
    }
        
}