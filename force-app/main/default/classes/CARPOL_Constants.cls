public class CARPOL_Constants{
    public CARPOL_Constants(){}
    public static final String YET_TO_ADD = 'Yet to Add';
    public static final String READY_TO_SUBMIT = 'Ready to Submit';
    
    //Report Type (Report Summary)
    public static final String PLANTING_REPORT = 'Planting/Release Reports'; // W-034962 Renamed from "Planting Reports" to "Planting/Release Reports"
    public static final String NO_PLANTING_REPORT = 'No Planting/Release Reports';
    public static final String VOLUNTEER_MONITORING_REPORT = 'Volunteer Monitoring Report';
    public static final String FIELD_TEST_REPORT = 'Field Test Reports (Annual or Final)'; // W-026169 Renamed From "Field Test Report" to "Field Test Reports (Annual or Final)"
    public static final String PRE_PLANTING_NOTICE = 'Pre-Planting/Pre-Release Notices'; // W-027499 changed from 'Pre-Planting Notice' to 'Pre-Planting or Release Notice'
    public static final String PRE_FLOWERING_NOTICE = 'Pre-Flowering Notice';
    public static final String HARVEST_DESTRUCT = 'Pre-Harvest/Pre-Destruct Notices';
    public static final String CLEANING_NOTICE = 'Cleaning (Return to General Use) Notice';
    public static final String STORAGE_REPORT = 'Storage Report';
    public static final String FINAL_DISPOSITION_REPORT = 'Final Disposition Report';
    
    //CBP XML Transfer
    public static final String CBP_DOCREF_OBJECT_TYPE = 'Authorizations__c';
    public static final String PERMIT_PDF_ERRORSTR = ': Permit PDF not found.';
    public static final String PERMIT_PDF_ERROR =    'Error in ';
    public static final String PERMIT_ATTACHMENT_ERROR =    'Attachment creation failed.';
    
}