//Classe Name : ConApp_Jun_TriggerHelper
//Helper Class to Hold Logic for Trigger on Object "Select Previously Approved Construct/SOP" (Construct_Application_Junction__c)
//Created By : Jagadish Konduru
//Created as part of Requirement: W-010921
//Requirement: if there is no construct and Construct_status__c is "Yet to Add" then change status to "Ready to Submit"
//Requirement: if it  Construct_status__c is "Ready to Submit" then don't change anything
//Requirement: if there is no construct and deleting existing Prev. rev'd construct ( and no pre. rev. constructs exist after deleting ) then revert back the Construct_status__c to "Yet to Add"
                

public with sharing class ConApp_Jun_TriggerHelper{

        Set<id> constructLineItemIds =new Set<Id>();
        Set<id> sopLineItemIds =new Set<Id>();
        Map<id, Ac__c> lineItemsToBeUpdated = new Map<id,Ac__c>();
        Map<id, boolean> incompleteConstructs = new Map<id,boolean>();  //line item and ready to submit fields
    
 // start W-035176 Validation logic to check PSC have related RA's when creating new aplications   
    public void validatePSCHasRA(List<Construct_Application_Junction__c> conAppList){
    map<id,id> aaList = new map<id,id>(); // Store retrieved  RA's associated to the PSC from the Construct
    map<id,id> aaListMatch = new map<id,id>();// Store retrieved  RA's on the application (LineItem)
    map<id,id> constructIdList = new map<id,id>(); // Store the PSC's Construct and LineItem 
    for (Construct_Application_Junction__c ci : conAppList){
        constructIdList.put(ci.construct__c,ci.Line_item__c);
    }
    for (construct__c ct : [select Link_Regulated_Article__c from construct__c where id in: constructIdList.keySet()])
    {
 
       aaList.put(ct.Link_Regulated_Article__c,constructIdList.get(ct.id));     
       
    }
    // Find the List of RA's on the LineItem
    list<ac__c> acrList=[select id,name,(select id,name,Regulated_Article__c from Link_Regulated_Articles__r) from ac__c where id in :aaList.values() ];
    for (ac__c ac : acrList){
            for (Link_Regulated_Articles__c ra : ac.Link_Regulated_Articles__r){
            aaListMatch.put(ra.Regulated_Article__c,ac.Id);
            }
    }
// Find the RA's that are in the Line_Item and PSC and remove the valid PSC
    for(id i : aaListMatch.keySet()){  
        if(aaList.containsKey(i) && aaList.get(i)== aaListMatch.get(i)){
           aaList.remove(i);
        }
    
    }
    // Set the error message on each PSC not associated with the same RA as LineItem
    List<id> iderror = aaList.values();
    for(Construct_Application_Junction__c a1 : conAppList){
        if(iderror.contains(a1.Line_Item__c)){
        a1.construct__c.addError('Regulated Article mismatch with application and Previously submitted construct');
        }
    }
} // end W-035176 

    
    public void updateSOP_And_Construct_Status_On_LineItem(List<Construct_Application_Junction__c> conAppList){
System.debug('=========================%%%%%%%%%%+===========');
               //Will filter and load values into constructLineItemIds and sopLineItemIds 
               loadConstructSOPChanges(conAppList);
               getIncompleteConstructs();
               loadConstructLineItemChanges();
               loadSOPLineItemChanges(); 
            update lineItemsToBeUpdated.values();
        
    }
    
    private void loadConstructLineItemChanges(){
        System.debug('@@@@@@@@*XXXXXXXXXXXXXX@@@@@');
       //Handling After Insert for Construct's
        if(Trigger.isAfter & Trigger.isInsert){
            
            for(Ac__c lineItem : [Select id, 
                                  //Total_of_New_Constructs__c, 
                                  Construct_Status__c, SOP_Status__c 
                                  FROM AC__c
                                  Where id in:constructLineItemIds 
                                  //and Total_of_New_Constructs__c = 0 
                                  and Construct_Status__c =:CARPOL_Constants.YET_TO_ADD]){
                       //check if any new constructs with incomplete status
                                  if (incompleteConstructs.get(lineItem.id)  ==null){           
                                      if(lineItemsToBeUpdated.get(lineItem.id) !=null){ 
                                          System.debug('%%%%%%%%%#####%%%');
                                          lineItemsToBeUpdated.get(lineItem.id).Construct_Status__c=CARPOL_Constants.READY_TO_SUBMIT;
                                      }else{
                                          System.debug('+++++++^^^^^+++++++');
                                          lineItem.Construct_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
                                          lineItemsToBeUpdated.put(lineItem.id, lineItem);
                                          System.debug('lineItem: '+ lineItem);
                                          System.debug('+++++++^^^^^+++++++');
                                      }
                                   }
                }
        }else if(Trigger.isDelete & Trigger.isbefore){//Handling Before Delete for Construct's
                for(Ac__c lineItem : [Select id,
                                             //Total_of_New_Constructs__c, 
                                             Construct_Status__c, 
                                             SOP_Status__c,
                                            (select ID, Line_Item__c from Link_Construct_Design_Protocol_Records__r where Construct__c!=null limit 3),
                                      		(select ID, Line_Item__c from Constructs__r where ready_to_submit__c = true limit 2) 
                                       FROM AC__c
                                       Where id in:constructLineItemIds 
                                       //and Total_of_New_Constructs__c = 0 
                                       and Construct_Status__c =:CARPOL_Constants.READY_TO_SUBMIT]){
                                           
                    if(lineItem.Link_Construct_Design_Protocol_Records__r!= null &&  lineItem.Link_Construct_Design_Protocol_Records__r.size()<2)
                    {
                                      if(lineItemsToBeUpdated.get(lineItem.id) !=null){ 
                                          if(lineItem.Constructs__r.size()==0)
                                          {
                                          	lineItemsToBeUpdated.get(lineItem.id).Construct_Status__c=CARPOL_Constants.YET_TO_ADD;
                                          }
                                      }else{
                                          if(lineItem.Constructs__r.size()==0)
                                          {
                                          lineItem.Construct_Status__c = CARPOL_Constants.YET_TO_ADD;
                                          lineItemsToBeUpdated.put(lineItem.id, lineItem);
                                          }
                                      }
                    }
                                           
                                           
                }            
        }//End of is Before Delete 
    } 
 
    private void loadSOPLineItemChanges(){
       //Handling After Insert for SOP's
        if(Trigger.isAfter & Trigger.isInsert){  
        for(Ac__c lineItem : [Select id, Number_of_New_Design_Protocols__c, Construct_Status__c, SOP_Status__c 
                              FROM AC__c
                              Where id in:sopLineItemIds 
                              and Number_of_New_Design_Protocols__c = 0 
                              and SOP_Status__c =:CARPOL_Constants.YET_TO_ADD]){
                                  
                                  if(lineItemsToBeUpdated.get(lineItem.id) !=null){ 
                                      lineItemsToBeUpdated.get(lineItem.id).SOP_Status__c=CARPOL_Constants.READY_TO_SUBMIT;
                                  }else{
                                      lineItem.SOP_Status__c = CARPOL_Constants.READY_TO_SUBMIT;
                                      lineItemsToBeUpdated.put(lineItem.id, lineItem);
                                  }
                 
            }
       }else if(Trigger.isDelete & Trigger.isbefore){ //Handling Before Delete for SOP's
                for(Ac__c lineItem : [Select id,
                                             Number_of_New_Design_Protocols__c, 
                                             Construct_Status__c, 
                                             SOP_Status__c,
                                            (select ID, Line_Item__c from Link_Construct_Design_Protocol_Records__r where Design_Protocol_Record_SOP__c!=null limit 3)
                                       FROM AC__c
                                       Where id in:sopLineItemIds 
                                       and Number_of_New_Design_Protocols__c = 0 
                                       and SOP_Status__c =:CARPOL_Constants.READY_TO_SUBMIT]){
                                           
                    if(lineItem.Link_Construct_Design_Protocol_Records__r!= null &&  lineItem.Link_Construct_Design_Protocol_Records__r.size()<2)
                    {
                                      if(lineItemsToBeUpdated.get(lineItem.id) !=null){ 
                                          lineItemsToBeUpdated.get(lineItem.id).SOP_Status__c=CARPOL_Constants.YET_TO_ADD;
                                      }else{
                                          lineItem.SOP_Status__c = CARPOL_Constants.YET_TO_ADD;
                                          lineItemsToBeUpdated.put(lineItem.id, lineItem);
                                      }
                    }
                                           
                                           
                }            
        }//End of is Before Delete 
    } 
    
    private void loadConstructSOPChanges(List<Construct_Application_Junction__c> conAppList){
       
        for(Construct_Application_Junction__c conAppJun: conAppList){
            
            if(conAppJun.Line_Item__c != null){ 
               if(conAppJun.Construct__c !=null){
                    constructLineItemIds.add(conAppJun.Line_Item__c);
               }else if(conAppJun.Design_Protocol_Record_SOP__c != null){
                  sopLineItemIds.add(conAppJun.Line_Item__c);
                }
            }
        }  
        
    }
    
    private void getIncompleteConstructs(){
       for(construct__C con : [Select id,ready_to_submit__C, Line_Item__c
                              FROM construct__C
                              Where Line_Item__c in:constructLineItemIds
                              and ready_to_submit__C =false]){
                      if(incompleteConstructs.get(con.Line_Item__c) ==null){         
                        incompleteConstructs.put(con.Line_Item__c,con.ready_to_submit__C ) ;       
                            }     
                              }
        
        
    }
    
}