@isTest 
//1-9-17 VV created 
//Test class to cover code of the class --> CARPOL_UNI_Ports_Custom_Lookup
private class CARPOL_UNI_Ports_Custom_Lookup_Test
{

    public static testMethod void PortsLookup() 
    {
        regulated_article__c regart = new regulated_article__c (name='Test RA',EFLRequiredCapabilties__c = 'F&V-Import F&V');
        insert regart;
        Domain__c prgm = new Domain__c(name = 'PPQ');
        insert prgm;
        program_line_item_pathway__c pthwy = new program_line_item_pathway__c (name = 'Test Pathway', program__c = prgm.id, EFLRequiredCapabilities__c='Fish-Import Fish');
        insert pthwy; 
        facility__c fac = new facility__c (name='Test Fac', EFLAvailableCapabilties__c = 'F&V-Import F&V; Fish-Import Fish' );
        insert fac;
        //pageRef to VF page
        PageReference pageRef = Page.CARPOL_UNI_Ports_Custom_Lookup;
        
        //search string
        pageRef.getParameters().put('lksrch', 'something');
        
        //regulated_article__c
        pageRef.getParameters().put('regartid', regart.id);
        
        //porttype to portofexit later     
         pageRef.getParameters().put('pathwayid', pthwy.id);        
         pageRef.getParameters().put('txt', 'testidport_AppFacility');
         Test.setCurrentPage(pageRef);

        //construct the controller class.   
        CARPOL_UNI_Ports_Custom_Lookup controller1 = new CARPOL_UNI_Ports_Custom_Lookup();
        pageRef.getParameters().put('txt', 'testidportEntry');
        regart.EFLRequiredCapabilties__c = null;
        update regart;
        pthwy.EFLRequiredCapabilities__c = null;
        update pthwy;
        fac.EFLAvailableCapabilties__c = null;
        update fac;
        CARPOL_UNI_Ports_Custom_Lookup controller2 = new CARPOL_UNI_Ports_Custom_Lookup();       

   
     }
    public static testMethod void PortsLookup2() 
    {
        regulated_article__c regart = new regulated_article__c (name='Test RA',EFLRequiredCapabilties__c = 'F&V-Import F&V');
        insert regart;
        Domain__c prgm = new Domain__c(name = 'PPQ');
        insert prgm;
        program_line_item_pathway__c pthwy = new program_line_item_pathway__c (name = 'Test Pathway', program__c = prgm.id, EFLRequiredCapabilities__c='Fish-Import Fish');
        insert pthwy; 
        facility__c fac = new facility__c (name='Test Fac', EFLAvailableCapabilties__c = 'F&V-Import F&V; Fish-Import Fish' );
        insert fac;
        //pageRef to VF page
        PageReference pageRef = Page.CARPOL_UNI_Ports_Custom_Lookup;
        
        //search string
        pageRef.getParameters().put('lksrch', 'something');
        
        //regulated_article__c
        pageRef.getParameters().put('regartid', regart.id);
        
        //porttype to portofexit later     
         pageRef.getParameters().put('pathwayid', pthwy.id);        
         pageRef.getParameters().put('txt', 'testidport_AppFacility');
         Test.setCurrentPage(pageRef);

        //construct the controller class.   
        CARPOL_UNI_Ports_Custom_Lookup controller1 = new CARPOL_UNI_Ports_Custom_Lookup();
        pageRef.getParameters().put('txt', 'testidportEmbkartion');
        regart.EFLRequiredCapabilties__c = null;
        update regart;
        pthwy.EFLRequiredCapabilities__c = null;
        update pthwy;
        fac.EFLAvailableCapabilties__c = null;
        update fac;
        CARPOL_UNI_Ports_Custom_Lookup controller2 = new CARPOL_UNI_Ports_Custom_Lookup();       

   
     }    
   public static testMethod void PortsLookup3() 
    {
        regulated_article__c regart = new regulated_article__c (name='Test RA',EFLRequiredCapabilties__c = 'F&V-Import F&V');
        insert regart;
        Domain__c prgm = new Domain__c(name = 'PPQ');
        insert prgm;
        program_line_item_pathway__c pthwy = new program_line_item_pathway__c (name = 'Test Pathway', program__c = prgm.id, EFLRequiredCapabilities__c='Fish-Import Fish');
        insert pthwy; 
        facility__c fac = new facility__c (name='Test Fac', EFLAvailableCapabilties__c = 'F&V-Import F&V; Fish-Import Fish' );
        insert fac;
        //pageRef to VF page
        PageReference pageRef = Page.CARPOL_UNI_Ports_Custom_Lookup;
        
        //search string
        pageRef.getParameters().put('lksrch', 'something');
        
        //regulated_article__c
        pageRef.getParameters().put('regartid', regart.id);
        
        //porttype to portofexit later     
         pageRef.getParameters().put('pathwayid', pthwy.id);        
         pageRef.getParameters().put('txt', 'testidport_AppFacility');
         Test.setCurrentPage(pageRef);

        //construct the controller class.   
        CARPOL_UNI_Ports_Custom_Lookup controller1 = new CARPOL_UNI_Ports_Custom_Lookup();
        pageRef.getParameters().put('txt', 'testidportEntry');
        regart.EFLRequiredCapabilties__c = null;
        update regart;
        pthwy.EFLRequiredCapabilities__c = null;
        update pthwy;
        fac.EFLAvailableCapabilties__c = null;
        update fac;
        CARPOL_UNI_Ports_Custom_Lookup controller2 = new CARPOL_UNI_Ports_Custom_Lookup();       

   
     }   
        public static testMethod void PortsLookup4() 
    {
        regulated_article__c regart = new regulated_article__c (name='Test RA',EFLRequiredCapabilties__c = 'F&V-Import F&V');
        insert regart;
        Domain__c prgm = new Domain__c(name = 'PPQ');
        insert prgm;
        program_line_item_pathway__c pthwy = new program_line_item_pathway__c (name = 'Test Pathway', program__c = prgm.id, EFLRequiredCapabilities__c='Fish-Import Fish');
        insert pthwy; 
        facility__c fac = new facility__c (name='Test Fac', EFLAvailableCapabilties__c = 'F&V-Import F&V; Fish-Import Fish' );
        insert fac;
        //pageRef to VF page
        PageReference pageRef = Page.CARPOL_UNI_Ports_Custom_Lookup;
        
        //search string
        pageRef.getParameters().put('lksrch', 'something');
        
        //regulated_article__c
        pageRef.getParameters().put('regartid', regart.id);
        
        //porttype to portofexit later     
         pageRef.getParameters().put('pathwayid', pthwy.id);        
         pageRef.getParameters().put('txt', 'testidport_AppFacility');
         Test.setCurrentPage(pageRef);

        //construct the controller class.   
        CARPOL_UNI_Ports_Custom_Lookup controller1 = new CARPOL_UNI_Ports_Custom_Lookup();
        pageRef.getParameters().put('txt', 'therepeat');
        regart.EFLRequiredCapabilties__c = null;
        update regart;
        pthwy.EFLRequiredCapabilities__c = null;
        update pthwy;
        fac.EFLAvailableCapabilties__c = null;
        update fac;
        CARPOL_UNI_Ports_Custom_Lookup controller2 = new CARPOL_UNI_Ports_Custom_Lookup();       

   
     }    
   public static testMethod void PortsLookup5() 
    {
        regulated_article__c regart = new regulated_article__c (name='Test RA',EFLRequiredCapabilties__c = 'F&V-Import F&V');
        insert regart;
        Domain__c prgm = new Domain__c(name = 'PPQ');
        insert prgm;
        program_line_item_pathway__c pthwy = new program_line_item_pathway__c (name = 'Test Pathway', program__c = prgm.id, EFLRequiredCapabilities__c='Fish-Import Fish');
        insert pthwy; 
        facility__c fac = new facility__c (name='Test Fac', EFLAvailableCapabilties__c = 'F&V-Import F&V; Fish-Import Fish' );
        insert fac;
        //pageRef to VF page
        PageReference pageRef = Page.CARPOL_UNI_Ports_Custom_Lookup;
        
        //search string
        pageRef.getParameters().put('lksrch', 'something');
        
        //regulated_article__c
        pageRef.getParameters().put('regartid', regart.id);
        
        //porttype to portofexit later     
         pageRef.getParameters().put('pathwayid', pthwy.id);        
         pageRef.getParameters().put('txt', 'testidport_AppFacility');
         Test.setCurrentPage(pageRef);

        //construct the controller class.   
        CARPOL_UNI_Ports_Custom_Lookup controller1 = new CARPOL_UNI_Ports_Custom_Lookup();
        regart.EFLRequiredCapabilties__c = null;
        update regart;
        pthwy.EFLRequiredCapabilities__c = null;
        update pthwy;
        fac.EFLAvailableCapabilties__c = null;
        update fac;
        CARPOL_UNI_Ports_Custom_Lookup controller2 = new CARPOL_UNI_Ports_Custom_Lookup();       

   
     }             
}