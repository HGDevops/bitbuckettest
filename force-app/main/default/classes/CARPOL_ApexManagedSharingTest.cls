@isTest
private class CARPOL_ApexManagedSharingTest {
    
    private Static Id partnerUserId;
    private Static Id publicGroupId;
    private Static Id appId;

    private static testMethod void testSharing() {
        
        //create test data
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser){
            init();
            test.startTest();
            CARPOL_ApexManagedSharing.addSharingForUser(partnerUserId);
            Application__share shareRecord = [select id from Application__share where rowCause = 'PartnerContact__c' and UserOrGroupId =: partnerUserId limit 1];
            System.assert(shareRecord != null);
            String status = CARPOL_ApexManagedSharing.toggleApplicationPrivacy(appId, publicGroupId);
            System.assert(status == 'Public');
            status = CARPOL_ApexManagedSharing.toggleApplicationPrivacy(appId, publicGroupId);
            System.assert(status == 'Private');
            test.stopTest();
        }
        

    }
    
    private static String randomString5() {
        return EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0, 5);
    }
    
    private static void init() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        
        //create account 
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
        Account objacct = new Account();
        objacct.Name = 'Global Account'+ randomString5();
        objacct.RecordTypeId = AccountRecordTypeId;    
        insert objacct;
        
        //create public group
        group publicGroup = new Group(Name = objacct.Name);
        insert publicGroup;
        publicGroupId = publicGroup.Id;
        
        //create contact
        Contact objCont = new Contact();
        objCont.FirstName = 'Global Contact'+randomString5();
        objcont.LastName = 'LastName'+randomString5();
        objcont.Email = randomString5()+'test@email.com';        
        objcont.AccountId = objacct.id;
        objcont.MailingStreet = 'Mailing Street'+randomString5();
        objcont.MailingCity = 'Mailing City'+randomString5();
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '32092';    
        insert objcont; 
        
        //create Partner User
         User usershare = new User();
         usershare.Username ='aphistestemail@test.com';
         usershare.LastName = 'APHISTestLastName';
         usershare.Email = 'APHISTestEmail@test.com';
         usershare.alias = 'APHItest';
         usershare.TimeZoneSidKey = 'America/New_York';
         usershare.LocaleSidKey = 'en_US';
         usershare.EmailEncodingKey = 'ISO-8859-1';
         usershare.ProfileId = [select id from Profile where Name='Org Admin'].Id;
         usershare.LanguageLocaleKey = 'en_US';
         usershare.ContactId = objcont.id;
         insert usershare;
         partnerUserId = userShare.Id;
        
        //create Application
        Application__c objapp = new Application__c();
        objapp.Applicant_Name__c = objcont.Id;
        objapp.Recordtypeid = ACAppRecordTypeId;
        objapp.Application_Status__c = 'Open';
        objapp.Private_Application__c = true;
        insert objapp;
        appId = objapp.id;
        
        AC__c lineItem = testData.newLineItem('Resale', objapp);
        Authorizations__c newAuth = testData.newAuth(appId);
    }

}