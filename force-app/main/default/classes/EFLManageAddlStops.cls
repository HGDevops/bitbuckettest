public with sharing class  EFLManageAddlStops{    
    public boolean tlEditflag {get; set;}
    public List<Transit_Locale__c> lstTransit{get;set;}
    public List<Transit_Locale__c> lstTransittoUpdate;
    public List<Transit_Locale__c> transitToInsert{get;set;}
    public List<transitInnerClass> transitInner{get;set;}
    public String selectedRowIndex{get;set;}  
    public Integer count = 1;
    public string LineItem;
    public string appId;
    public String countryofOrigin; 
    public String ScientificName {get;set;}
    public string linestatus  {get;set;}
    public boolean autoaddTL {get; set;}
    public AC__c objLineItem {get; set;}
    public String idScientificName                      { get; set; } 
    public String idProgpathway                         { get; set; } 
    public transit_locale__c lastStop {get;set;}  // holds last stop  (one prior to the current)
    public string subsqdelTransitid {get;set;}
    public Boolean addClicked{get;set;}
    public Boolean addMore;
    public String user;
    public Boolean fromAddMore ;
    public string portPrefix {get;set;}
    public boolean internaluser{get;set;}
    public static id lnPortofEntry;
    public id lnportofexit;
    public static date lnDateofArr;
    public date lnDateofDep;
    public boolean closewindow {get;set;}
    
    public eflManageAddlStops(){
        internaluser = false;
        closewindow = false;
        addMore = true;
        tlEditflag = false;
        fromAddMore = false;
        lstTransittoUpdate = new List<Transit_Locale__c>(); //new and old AddlStops        
        objLineItem  = new AC__c();
        portPrefix = Facility__c.SObjectType.getDescribe().getKeyPrefix();
        idScientificName = apexpages.currentpage().getparameters().get('ScientificName');  
        idProgpathway = apexpages.currentpage().getparameters().get('strPathway');         
        LineItem = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('lineitemid') );
        countryofOrigin = ApexPages.currentPage().getParameters().get('countryofOrigin'); 
        ScientificName = ApexPages.currentPage().getParameters().get('ScientificName');
        lnPortofEntry =  ApexPages.currentPage().getParameters().get('PortofEntry');
        lnportofexit = ApexPages.currentPage().getParameters().get('portofexit');
        lnDateofDep = Date.valueOf(ApexPages.currentPage().getParameters().get('DateofDep').left(10));
        lnDateofArr = Date.valueOf(ApexPages.currentPage().getParameters().get('DateofArr').left(10));
        user = ApexPages.currentPage().getParameters().get('user');
        objLineItem = [Select id,name,Application_Number__c, status__c, Program_Line_Item_Pathway__c,application_number__r.name,Regulated_Article__c, Scientific_name__r.name, Country_Of_Origin__r.name,Country_of_destination__r.name From AC__c Where id=: LineItem order by createddate asc];
        appId = objLineItem.Application_Number__c;
        linestatus = objLineItem.status__c;        
        transitInner = new List<transitInnerClass>(); //new stops wrapper
        if(user != null && user.contains('internal')){
            internalUser = true;
        }
        getOldTllst();
        if(autoaddTL){
            Add();
        }        
        selectedRowIndex = '0';
        lastStop = new transit_locale__c();
    }
    
    public void Add(){ 
        autoaddTL = true; 
        count = count+1;
        addMore();   // adds a new row   
    } 
    
    public void removeTransit(){    
        //transitInner = new List<transitInnerClass>();
        transitInner.clear();
        
    }
    
    public void addMore(){  
        if(transitInner.size()>0 || lstTransit.size()>0) {
            SaveTransitLocal();
            fromAddMore = true;
        }
        Date arrivalDate;
        Id portOfEntry;
        if(addMore){
            if(lstTransit.size() == 0){
                arrivalDate = lnDateofDep  ;
                portOfEntry = lnPortofExit;
            }         
            if(lstTransit.size() > 0){
                Integer lastIndex = lstTransit.size()-1; 
                arrivalDate = lstTransit.get(lastIndex).EFLArrival_Date__c;
                portOfEntry = lstTransit.get(lastIndex).Port_of_Entry__c;
            }
            if(transitInner.size()==0){ //deal w/1 record at time - current one may have errors
                transitInnerClass objInnerClass = new transitInnerClass(count,countryofOrigin,ScientificName,arrivalDate,portOfEntry);
                transitInner.add(objInnerClass); 
            }
        }   
    }
    
    public string delTransitid {get;set;}
    
    public PageReference deleteTransit() {
        subsqdelTransitid = delTransitid;
        deleteSubSeqTransit();
        list <Transit_Locale__c> delList = new list <Transit_Locale__c >();
        for (Integer i = 0; i < lstTransit.size(); i++) {
            Transit_Locale__c a = lstTransit[i];
            if (a.Id == delTransitid) {
                delList.add(a);                
                lstTransit.remove(i);
                break;
            }
        }
        delete delList; 
        tlEditflag = false;
        return null;
    }
    
    public PageReference SaveTransitLocal(){
        PageReference pgRef; 
        if(tlEditflag){
            lstTransittoUpdate.add(lstTransit[lstTransit.size()-1]);
            tlEditflag = false;
        }
        for(Integer j = 0;j<transitInner.size();j++)
        {
            Transit_Locale__c objTransit = transitInner[j].transit;
            if(lastStop.id != null){
                if (objTransit.Port_of_Exit__c != lastStop.Port_of_Entry__c && lastStop.Port_of_Entry__c!=null){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Port of Exit should be Port of Entry from the previous stop.'));
                    addMore = false;
                    return null;
                }
                if (objTransit.Date_of_Departure__c < lastStop.EFLArrival_Date__c && lastStop.EFLArrival_Date__c!=null){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The Date of Departure should be on or after the Date of Arrival from the previous stop.'));
                    addMore = false;
                    return null;
                }                
            }
            objTransit.Line_Item__c = LineItem;
            lstTransittoUpdate.add(objTransit);
        } 
        Try{
            EFLEnforceAccessUtility.checkObjectUpdateAccess('Transit_Locale__c');
            upsert lstTransittoUpdate;
            transitInner = new List<transitInnerClass>();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Additional Stop was successfully saved'));
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLManageAddlStops.SaveTransitLocal',e);
        }
        
        lstTransittoUpdate.clear();
        autoaddTL = false;
        //transitInner = new List<transitInnerClass>();
        count = 0;
        getOldTllst();
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Additional Stop was successfully saved'));
        addMore = true;        
        return null;
    }
    public PageReference Cancel() {
        PageReference pgRef;
        if(user != null && user.contains('internal'))
            pgRef = new PageReference('/'+lineitem+'?nooverride=1');
        else
            pgRef = new PageReference('/apex/carpol_uni_lineitem?appid='+appid+'&Id='+LineItem+'&strPathway='+objLineItem.Program_Line_Item_Pathway__c);
        pgref.setredirect(true);
        return pgref;
    }    
    public PageReference CancelTransitLocal(){
        PageReference pgRef;        
        if(user != null && user.contains('internal')){
            //pgRef = new PageReference(System.Url.getSalesforceBaseURL().toExternalForm()+'/'+LineItem );
            //pgref.setredirect(true);
        }else{
            pgRef = new PageReference('/apex/carpol_uni_lineitem?appid='+appid+'&Id='+LineItem+'&strPathway='+objLineItem.Program_Line_Item_Pathway__c);
            pgref.setredirect(true);
        }       
        //Added to match last stop details with Line Item
        AC__c parentLI = [Select ID,Port_of_Entry__c,Port_of_Entry__r.Name,Proposed_date_of_arrival__c From AC__c WHERE ID=:LineItem LIMIT 1];
        String formattedLIArrDate = parentLI.Proposed_date_of_arrival__c.format();
        if(lstTransit.size()>0){
            if(lstTransit.get(lstTransit.size()-1).Port_of_Entry__c == parentLI.Port_of_Entry__c && lstTransit.get(lstTransit.size()-1).EFLArrival_Date__c == parentLI.Proposed_date_of_arrival__c){
                return pgRef;
            }
            else{
                if(lstTransit.get(lstTransit.size()-1).Port_of_Entry__c != parentLI.Port_of_Entry__c){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The Port of Entry on the last stop shoud match the Port of Entry on Line Item ['+parentLI.Port_of_Entry__r.Name+'].'));
                }
                if(lstTransit.get(lstTransit.size()-1).EFLArrival_Date__c != parentLI.Proposed_date_of_arrival__c){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The Date of Arrival on the last stop should match the Date of Arrival on Line Item ['+formattedLIArrDate+'].'));
                }
                return null;
            }
        }
        
        return null;
    } 
    public PageReference CancelTransitLocalInt(){
        PageReference pgRef;        
        
        //Added to match last stop details with Line Item
        AC__c parentLI = [Select ID,Port_of_Entry__c,Port_of_Entry__r.Name,Proposed_date_of_arrival__c From AC__c WHERE ID=:LineItem LIMIT 1];
        String formattedLIArrDate = parentLI.Proposed_date_of_arrival__c.format();
        if(lstTransit.size()>0){
            if(lstTransit.get(lstTransit.size()-1).Port_of_Entry__c == parentLI.Port_of_Entry__c && lstTransit.get(lstTransit.size()-1).EFLArrival_Date__c == parentLI.Proposed_date_of_arrival__c){
                closewindow = true; 
                //system.debug('211 #### closewindow = '+closewindow ) ;              
                return pgRef;
            }
            else{
                if(lstTransit.get(lstTransit.size()-1).Port_of_Entry__c != parentLI.Port_of_Entry__c){
                    closewindow = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The Port of Entry on the last stop shoud match the Port of Entry on Line Item ['+parentLI.Port_of_Entry__r.Name+'].'));
                }else{
                    closewindow = true;
                    //system.debug('220 #### closewindow = '+closewindow ) ;                      
                }
                if(lstTransit.get(lstTransit.size()-1).EFLArrival_Date__c != parentLI.Proposed_date_of_arrival__c){
                    closewindow = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The Date of Arrival on the last stop should match the Date of Arrival on Line Item ['+formattedLIArrDate+'].'));
                    //system.debug('225 #### closewindow = '+closewindow ) ; 
                }else{
                    closewindow = true;
                    //system.debug('228 #### closewindow = '+closewindow ) ;                     
                }
                closewindow = false;
                return null;
            }            
        }
        closewindow = true;
        //system.debug('2 #### closewindow = '+closewindow ) ;              
        return null;
    } 
    
    public list<Transit_Locale__c> getOldTllst() {
        lstTransit  = new List<Transit_Locale__c>([select id,Name,Type_of_Conveyance__c,Date_of_Departure__c, Mode_of_Transportion__c,From_Country__c,To_Country__c,Port_of_Entry__c,Port_of_Entry__r.name, Port_of_Exit__c, Port_of_Exit__r.name, Packaging_Material_Category__c, contact__c,EFLArrival_Date__c from Transit_Locale__c where Line_Item__c=:LineItem order by createddate asc]);
        if(lstTransit.size()>0){
            autoaddTL = false;
        }
        else{
            autoaddTL = true;
        }
        for (transit_locale__c tlx : lstTransit ){
            lastStop = tlx;
        }  // final existing record will be the lastStop
        return lstTransit;
    }     
    
    public PageReference deleteSubSeqTransit() {  
        lstTransit.sort(); 
        list<Transit_Locale__c> susqTltoDelete = new list <Transit_Locale__c>();
        integer currRecNum;
        for (Integer i = 0; i < lstTransit.size(); i++) {
            Transit_Locale__c a = lstTransit[i];      
            if (a.Id == subsqdelTransitid) currRecNum = i;
        }
        for (Integer j = 0; j < lstTransit.size(); j++) {
            if (j > currRecNum ) {
                
                Transit_Locale__c a = lstTransit[j];                
                susqTltoDelete.add(a);
            }
        }
        if(!susqTltoDelete.isempty()) 
        {
            Try{
                EFLEnforceAccessUtility.checkObjectUpdateAccess('Transit_Locale__c');
                delete susqTltoDelete;
            }catch(exception e){
                EFLErrorLog.createErrorLog('EFLManageAddlStops.deleteSubSeqTransit',e);
            }            
        }
        getOldTllst();
        tlEditflag = true;
        return null;
    }
    
    public class transitInnerClass{       
        public String recCount{get;set;}
        public Transit_Locale__c transit{get;set;}
        public boolean addTransitBtn{get;set;}
        //public transitInnerClass(Integer intCount){
        
        public transitInnerClass(Integer intCount,String countryOrigin,String ScientificName,Date depDate, Id exitport){
            recCount = String.valueOf(intCount); 
            transit = new Transit_Locale__c();
            transit.Date_of_Departure__c = depDate;
            transit.Port_of_Exit__c = exitport;
            if(Test.isRunningTest())
            {
                transit.EFLArrival_Date__c = EFLManageAddlStops.lnDateofArr;
                transit.Port_of_Entry__c = EFLManageAddlStops.lnPortofEntry;
            }
            if(ScientificName != null && ScientificName != '')
                transit.Regulated_Article__c    =ScientificName;
            if(intCount == 1)
                transit.From_Country__c=countryOrigin;
            addTransitBtn = true;
        }   
    }
}