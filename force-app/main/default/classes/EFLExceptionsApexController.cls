public class EFLExceptionsApexController {
    @AuraEnabled
    public static List<String> getPicklistValues(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = EFLAnnual_Report__c.EFLHasExceptions__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getValue());
        }     
        return pickListValuesList;
    }
    
    
    @AuraEnabled
    public static EFLAnnual_Report__c getARDetails(string arId){      
        System.debug(arId);
        return [select id, EFLHasExceptions__c from EFLAnnual_Report__c where id=:arId];
    }    
    
    @AuraEnabled
    public static void updateReport(String annualReportId, String selectedValue){
        if(annualReportId!=null){
            EFLAnnual_Report__c annRep = [select id, EFLHasExceptions__c from EFLAnnual_Report__c where id=:annualReportId];
            annRep.EFLHasExceptions__c = selectedValue;
            update annRep;
        }
    }
}