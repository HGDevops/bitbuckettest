/**
* Class containing tests for EFLLineItemRepository
*/
@IsTest 
private class EFLLineItemRepository_Test {
    @IsTest 
    static void testSelectbyIDs() {
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testData.newapplication();
        List<AC__c> bulkLineItems = testData.newBulkLineItems(app);
        Set<Id> lineItemIDs = new Set<Id>();
        for (AC__c ac: bulkLineItems) {
            lineItemIDs.add(ac.Id);
        }
        List<AC__c> lineItems = EFLLineItemRepository.selectbyIDs(lineItemIDs);
        System.assertNotEquals(0, lineItems.size());
    }
    
    @IsTest 
    static void testSelectbyID() {
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testData.newapplication();
        AC__c lineItem = testData.newLineItem('Personal Use',app);
        AC__c returnedLineItem = EFLLineItemRepository.selectbyID(lineItem.Id);
        System.assertNotEquals(null, returnedLineItem);
    }
    
    @IsTest 
    static void testSelectbyApplicationID() {
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testData.newapplication();
        List<AC__c> bulkLineItems = testData.newBulkLineItems(app);
        List<AC__c> lineItems = EFLLineItemRepository.selectbyApplicationID(app.Id);
        System.assertNotEquals(0, lineItems.size());
    }
    
    @IsTest 
    static void testSelectbyIDException() {
        AC__c returnedLineItem = EFLLineItemRepository.selectbyID(null);
        System.assertEquals(null, returnedLineItem);
    }
    
    @IsTest 
    static void testSelectbyIDsException() {
        List<AC__c> lineItems = EFLLineItemRepository.selectbyIDs(null);
        System.assertEquals(0, lineItems.size());
    }
    
    @IsTest 
    static void testSelectbyApplicationIDException() {
        List<AC__c> lineItems = EFLLineItemRepository.selectbyApplicationID(null);
        System.assertEquals(0, lineItems.size());
    }

	@IsTest
    static void testselectAllFieldsAndRelatedFieldsbyID(){
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testData.newapplication();
        AC__c lineItem = testData.newLineItem('Personal Use',app);
        AC__c returnedLineItem = EFLLineItemRepository.selectAllFieldsAndRelatedFieldsbyID(lineItem.Id);
        System.assertNotEquals(null, returnedLineItem);
    }  
    
    @IsTest
    static void testselectAllFieldsAndRelatedFieldsbyIDException(){
        CARPOL_BRS_TestDataManager testData=new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = testData.newapplication();
        AC__c lineItem = testData.newLineItem('Personal Use',app);
        AC__c returnedLineItem = EFLLineItemRepository.selectAllFieldsAndRelatedFieldsbyID(null);
        System.assertEquals(null, returnedLineItem);
    }  
}