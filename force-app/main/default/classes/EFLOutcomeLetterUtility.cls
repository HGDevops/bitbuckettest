/*
    @PURPOSE:  This utility is used for the letter button the wizards
               Anonymous users get an informal letter with no address information
               Authenticated users get a formal button with their contact data 
               -putAttributesInMap will set the fieldAnswerMap that we use to produce the parameters section, called from both wizards before getOutcomeLetter is called.
    @Created:  2/6/2018 by Dawn Sangree
    @LastModified  2/9/2018 by Dawn Sangree
*/

public with sharing class EFLOutcomeLetterUtility{

    //TODO:  Move these into Metadata
    private static final String PRIVATE_INFORMAL_PAGE = '/apex/CARPOL_Standard_Informal_Letter';
    private static final String PRIVATE_FORMAL_PAGE = '/apex/CARPOL_AC_Processing';

    //determines if user is anonymous or authenticated, return pagereference
    public static PageReference generateOutcomeLetter(String cookieString, Map<string, string> fieldAnswerMap, String SelectedPathwayId, String wqTPId, String processId ){
        PageReference prOutcomeLetter;
               
        //retrieve conditions
        String conditionslist = EFLGetConditions.getconditions(fieldAnswerMap,SelectedPathwayId);

        //build the body of the prOutComeLetter URL, make sure it is encoded to protect from cross site scripting
        //Begin url, determine if informal or formal
        Boolean authFlag = false;

        String urlString;
        //determine which page to call
        Boolean guestuser = UserInfo.getUserType().toLowerCase().contains('guest');
        if(guestuser){
            urlString = PRIVATE_INFORMAL_PAGE;
        } else {
            urlString = PRIVATE_FORMAL_PAGE;
        }
        
        //Place string in page reference
        prOutcomeLetter = new PageReference(urlString);

        //add processId if authenticated
        if(!guestuser){
            prOutcomeLetter.getparameters().put('processID', processId); // TODO:  get processId from flags
        }
        //add thumbprint to URL
        prOutcomeLetter.getparameters().put('TPId',wqTPId);
        //add pathway to URL
        prOutcomeLetter.getparameters().put('strpathway',SelectedPathwayId);        
        //add conditions to url
        prOutcomeLetter.getparameters().put('conditionslist',conditionslist);        
        //add parameters to url
        if (fieldAnswerMap.size() > 0){
            for(string st: fieldAnswerMap.keySet()){
                prOutcomeLetter.getparameters().put(st,fieldAnswerMap.get(st));   
            }
        }

        //Prepare to return the pagereference
        prOutcomeLetter.setRedirect(true);

        //set cookie with question responses
        Cookie questionResponses = new Cookie('questionResponses',cookieString,null,-1,true);
        prOutcomeLetter.setCookies(new Cookie[]{questionResponses});
        
        return prOutcomeLetter;
    }   
    
}