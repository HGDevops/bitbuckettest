@isTest(seealldata=false)
private class EFLGetRegulatedArticlesTest {
      @IsTest static void EFLGetRegulatedArticlesTest() {
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId; 
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Group__c grp = new Group__c (Name = 'Cat-Grp');
          insert grp;
          Id grpid = grp.id;
          set<id> grpSet = new set<id>();
          grpSet.add(grpid);
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Regulated_Article__c regart1 = testData.newRegulatedArticle(plip.id);
          Regulated_Article__c regart2 = testData.newRegulatedArticle(plip.id);
          List<Regulated_Article__c> regList = new List<Regulated_Article__c>();
          regList.add(regart1);
          regList.add(regart2);
          Map<String,String> answers = new Map<String,String>();
          Test.startTest(); 
            EFLGetRegulatedArticles.getRegulatedArticles(plip.id);
            EFLGetRegulatedArticles.getRegulatedArticles(plip.id,grp.id);
            EFLGetRegulatedArticles.getCategories(plip.id);
            //EFLGetRegulatedArticles.getRegulatedArticleIdsbyPathway(plip.id);
            EFLGetRegulatedArticles.getRegulatedArticlesbyPathway(plip.id,grpSet);
            EFLGetRegulatedArticles.searchRegulatedArticles(regList, regart1.Name, grp.id,plip.id);
          Test.stopTest();   
      }
}