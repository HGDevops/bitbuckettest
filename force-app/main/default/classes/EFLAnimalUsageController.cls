public class EFLAnimalUsageController {
    public static List<EFLRegistered_Animal__c> fetchTableData(String annualReportId){
        List<EFLRegistered_Animal__c> animalsList = [Select Id,EFLAnimal__c,EFLAnimalName__c,EFLAnimalName__r.Name,EFLSequence__c,
                                                     EFLSelectedForReport__c,EFLAnnual_Report__c,
                                                     EFLColumnBHeldNotUsed__c,EFLColumnCUsedPainMinimized__c,
                                                     EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,
                                                     EFLOtherAnimal__c 
                                                     FROM EFLRegistered_Animal__c 
                                                     WHERE EFLAnnual_Report__c =: annualReportId
                                                     ORDER BY EFLSequence__c ASC];
        return animalsList;
    }
    
    @AuraEnabled
    public static Id deleteThisAnimal(EFLRegistered_Animal__c animalToDelete){
        
        Id aId = animalToDelete.Id;
        if(animalToDelete.EFLOtherAnimal__c == false){
            EFLRegistered_Animal__c targetAnimal = [Select ID,EFLSelectedForReport__c, EFLColumnBHeldNotUsed__c,
                                                    EFLColumnCUsedPainMinimized__c, EFLColumnDUsedPainMinimized__c,
                                                    ELFColumnEPainNotMinimized__c FROM EFLRegistered_Animal__c
                                                    WHERE ID=:animalToDelete.id];
            targetAnimal.EFLColumnBHeldNotUsed__c = null;
            targetAnimal.EFLColumnCUsedPainMinimized__c = null;
            targetAnimal.EFLColumnDUsedPainMinimized__c = null;
            targetAnimal.ELFColumnEPainNotMinimized__c = null;
            targetAnimal.EFLSelectedForReport__c = false;
            update targetAnimal;
        }
        else if(animalToDelete.EFLOtherAnimal__c == true){
            delete animalToDelete;
        }
        return aId;
    }
    
    @AuraEnabled public static void upsertAnimals(List<EFLRegistered_Animal__c> animalsToUpsert){
        for(EFLRegistered_Animal__c a :animalsToUpsert){
            if((a.EFLColumnBHeldNotUsed__c != null && a.EFLColumnBHeldNotUsed__c > 0)
               || (a.EFLColumnCUsedPainMinimized__c != null && a.EFLColumnCUsedPainMinimized__c > 0) 
               || (a.EFLColumnDUsedPainMinimized__c != null && a.EFLColumnDUsedPainMinimized__c > 0) 
               || (a.ELFColumnEPainNotMinimized__c != null && a.ELFColumnEPainNotMinimized__c > 0)){
                a.EFLSelectedForReport__c = true;
            }
        }
        upsert animalsToUpsert;
    }
    
    @AuraEnabled public static AnimalDetail addOtherAnimal(String parentId, Id animalId, string animalName){
        EFLRegistered_Animal__c newAnimal = new EFLRegistered_Animal__c();
        newAnimal.EFLAnnual_Report__c = parentId;
        newAnimal.EFLOtherAnimal__c = true;
        newAnimal.EFLSelectedForReport__c = true;
        newAnimal.EFLAnimalName__c = animalId;
        newAnimal.EFLAnimal__c = animalName;
        if(UserInfo.getUserType() == 'Standard'){
            newAnimal.EFLColumnBHeldNotUsed__c = 0;
            newAnimal.EFLColumnCUsedPainMinimized__c = 0;
            newAnimal.EFLColumnDUsedPainMinimized__c = 0;
            newAnimal.ELFColumnEPainNotMinimized__c = 0;
        }
        Integer max = 0;
        list<AggregateResult> ar = [Select MAX(EFLSequence__c)maxseq FROM EFLRegistered_Animal__c WHERE EFLAnnual_Report__c=:parentId];
        if(!ar.isEmpty() && integer.valueOf((decimal)ar[0].get('maxseq')) != null){
            max = integer.valueOf((decimal)ar[0].get('maxseq'));
        }
        newAnimal.EFLSequence__c = max+1;
        insert newAnimal;
        EFLRegistered_Animal__c ret = [SELECT Id,EFLAnimal__c,EFLAnimalName__c,EFLAnimalName__r.Name,EFLSequence__c,
                                       EFLSelectedForReport__c,EFLAnnual_Report__c,
                                       EFLColumnBHeldNotUsed__c,EFLColumnCUsedPainMinimized__c,
                                       EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,
                                       EFLOtherAnimal__c
                                       FROM EFLRegistered_Animal__c
                                       WHERE Id = :newAnimal.Id LIMIT 1];
        return new AnimalDetail(ret);
    }
    
    @AuraEnabled public static PageData getPageData(Id arId){
        PageData pd = new PageData(arId);
        for(EFLRegistered_Animal__c a : fetchTableData(arId)){
            if(a.EFLOtherAnimal__c == true){
                if(a.EFLAnimalName__r.Name == 'Other Animals' || a.EFLAnimal__c == 'Other Animals'){ // have to work around hard-coded string name check for SpringCM to work.
                    pd.makeOtherAnimalSummary(a);
                }else{
                	pd.otherAnimalList.add(new AnimalDetail(a));
                }
            }else if(a.EFLSelectedForReport__c == true || pd.isCommunity == false){
                pd.animalList.add(new AnimalDetail(a));
            }
        }
        return pd;
    }
    
    public class PageData{
        @AuraEnabled public Id annualReportId{get;set;}
        @AuraEnabled public list<AnimalDetail> animalList{get;set;}
        @AuraEnabled public list<AnimalDetail> otherAnimalList{get;set;}
        @AuraEnabled public AnimalDetail otherAnimalSummary{get;set;}
        @AuraEnabled public boolean isInternalUser{get;set;}
        @AuraEnabled public boolean isCommunity{get;set;}
        @AuraEnabled public boolean canEdit{get;set;}
        public PageData(Id arId){
            this.annualReportId = arId;
            this.isInternalUser = userinfo.getusertype() == 'Standard';
            this.animalList = new list<AnimalDetail>();
            this.otherAnimalList = new list<AnimalDetail>();
            this.otherAnimalSummary = new AnimalDetail();
            this.isCommunity = Site.getSiteId() != null;
            this.canEdit = true;
            list<PermissionSetAssignment> assignments = [SELECT PermissionSetId 
                                                         FROM PermissionSetAssignment 
                                                         WHERE AssigneeId= :UserInfo.getUserId() 
                                                         AND PermissionSet.Name = 'AC_R_L_Read_Only_Users'];
            if(!assignments.isEmpty()){
                this.canEdit = false;
            }
        }
        
        public void makeOtherAnimalSummary(EFLRegistered_Animal__c r){
            // first ensure all column values have a non-null value.
            if(r.EFLColumnBHeldNotUsed__c == null){
                r.EFLColumnBHeldNotUsed__c = 0;
            }
            if(r.EFLColumnCUsedPainMinimized__c == null){
                r.EFLColumnCUsedPainMinimized__c = 0;
            }
            if(r.EFLColumnDUsedPainMinimized__c == null){
                r.EFLColumnDUsedPainMinimized__c = 0;
            }
            if(r.ELFColumnEPainNotMinimized__c == null){
                r.ELFColumnEPainNotMinimized__c = 0;
            }
            AnimalDetail ad = new AnimalDetail(r);
            ad.name = 'OTHER ANIMALS';
            ad.helpText = 'Provide common names of regulated species other than dogs, Cats, Guinea Pigs, Hamsters, Rabbits, Non-human Primates, Sheep, or Pigs.';
            ad.canDelete = false;
            this.otherAnimalSummary = ad;
        }
    }
    
    public class AnimalDetail{
        @AuraEnabled public EFLRegistered_Animal__c record{get;set;}
        @AuraEnabled public boolean canDelete{get;set;}
        @AuraEnabled public string name{get;set;}
        @AuraEnabled public string helpText{get;set;}
        @AuraEnabled public string scientificName{get;set;}
        @AuraEnabled public integer sumCDE{get;set;}
        public AnimalDetail(){
            this.sumCDE = 0;
        }
        public AnimalDetail(EFLRegistered_Animal__c r){
            this.record = r;
            this.name = r.EFLAnimalName__c != null ? r.EFLAnimalName__r.Name : r.EFLAnimal__c;
            decimal c = r.EFLColumnCUsedPainMinimized__c != null ? r.EFLColumnCUsedPainMinimized__c : 0;
            decimal d = r.EFLColumnDUsedPainMinimized__c != null ? r.EFLColumnDUsedPainMinimized__c : 0;
            decimal e = r.ELFColumnEPainNotMinimized__c != null ? r.ELFColumnEPainNotMinimized__c : 0;
            this.sumCDE = integer.valueOf(c+d+e);
        }
    }
}