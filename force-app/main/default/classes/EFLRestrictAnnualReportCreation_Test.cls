@isTest (seeAllData = false)
public class EFLRestrictAnnualReportCreation_Test {
   
    @isTest
    public static void test1(){       
        test.startTest();
        EFLRegistration__c testReg = new EFLRegistration__c(EFLRegistrationNumber__c='11-A-1111');
        Database.insert(testReg);      
        EFLAnnual_Report__c annRep = new EFLAnnual_Report__c();
        annRep.EFLRegistration__c = testReg.id;
        annRep.EFLSites__c = 'Site1,Site2,Site3';
        Database.insert(annRep);
        test.stopTest();
        system.assertEquals(annRep.EFLRegistration__c, testReg.id);
    }
}