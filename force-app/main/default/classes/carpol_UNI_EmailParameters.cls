public with sharing class carpol_UNI_EmailParameters {
    
   public  CARPOL_URLs__c baseurl {get;set;}
   public  CARPOL_URLs__c baseurlstate {get;set;}
   public Id authid {get;set;}
   public Id Stateid {get;set;}
   public string URLType {get;set;}
    
   public carpol_UNI_EmailParameters( ) {
       system.debug('URLTYPE!!!!!!'+URLType);
      if(URLType == 'External'){
         baseurl = CARPOL_URLs__c.getValues('External'); 
         baseurlstate = CARPOL_URLs__c.getValues('External_State');
         } 
      Else {     
       baseurl = CARPOL_URLs__c.getValues('Internal'); 
       }
       system.debug('baseurl@@@@@'+baseurl);
      }
}