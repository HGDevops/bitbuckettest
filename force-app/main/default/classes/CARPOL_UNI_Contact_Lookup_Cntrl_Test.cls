@isTest(seeAllData=true)
private class CARPOL_UNI_Contact_Lookup_Cntrl_Test {
    
    @IsTest static void CARPOL_UNI_Contact_Lookup_Controller_Test() {
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          //Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          id p=[select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;  
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
         // usershare.ProfileId = [select id from Profile where Name IN ('APHIS Applicant','eFile Applicant')].Id;
          usershare.ProfileId = p;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;              
       /*   string pathwayId = [select id from program_line_item_pathway__c  where name = 'Veterinary Biological Products'].id;
          Test.startTest(); 
         //run as portal user
          //run as salesforce user
              //PageReference pageRef = Page.Portal_Application_Detail;
              PageReference pageRef = Page.CARPOL_UNI_Contact_Lookup;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('lksrch','Test');
              ApexPages.currentpage().getParameters().put('ACtype','Shippers');
              ApexPages.currentpage().getParameters().put('pathwayId',pathwayId );               
              CARPOL_UNI_Contact_Lookup_Controller extclass = new CARPOL_UNI_Contact_Lookup_Controller();
              extclass.objImpContactNew = new Applicant_Contact__c();
              extclass.selectedcountry = String.valueOf([Select Id, Name from Country__c Where Name='United States of America' Limit 1].Id);
              extclass.searchString = 'Korea';
              extclass.ACtype = 'Shippers';
              extclass.isCVBpthwy = true;
              extclass.search(); 
              extclass.ChangeAndRerenderstate();                      
              extclass.saveAppcont();
              extclass.getFormTag();
              extclass.getTextBox();
              extclass.getjsonmap();
              system.assert(extclass != null);
              
          Test.stopTest();  */ 
      }


     @IsTest static void CARPOL_UNI_Contact_Lookup_Controller_Test2() {
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          //Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
         id p=[select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = p; 
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;              
        /*  string pathwayId = [select id from program_line_item_pathway__c  where name = 'Veterinary Biological Products'].id;
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              //PageReference pageRef = Page.Portal_Application_Detail;
              PageReference pageRef = Page.CARPOL_UNI_Contact_Lookup;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('lksrch','Test');
              ApexPages.currentpage().getParameters().put('ACtype','Shippers');
              ApexPages.currentpage().getParameters().put('pathwayId',pathwayId );               
              CARPOL_UNI_Contact_Lookup_Controller extclass = new CARPOL_UNI_Contact_Lookup_Controller();
              extclass.objImpContactNew = new Applicant_Contact__c();
              extclass.selectedcountry = String.valueOf([Select Id, Name from Country__c Where Name='United States of America' Limit 1].Id);
              extclass.searchString = 'Korea';
              extclass.isCVBpthwy = true;
              extclass.ACtype = 'Permittees';
              extclass.search(); 
              extclass.ChangeAndRerenderstate();                      
              extclass.saveAppcont();
              extclass.getFormTag();
              extclass.getTextBox();
              extclass.getjsonmap();
              system.assert(extclass != null);
              
          Test.stopTest();  */ 
      }   


     @IsTest static void CARPOL_UNI_Contact_Lookup_Controller_Test4() {
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          //Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          id p=[select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = p;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;              
         /* string pathwayId = [select id from program_line_item_pathway__c  where name = 'Veterinary Biological Products'].id;
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              //PageReference pageRef = Page.Portal_Application_Detail;
              PageReference pageRef = Page.CARPOL_UNI_Contact_Lookup;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('lksrch','Test');
              ApexPages.currentpage().getParameters().put('ACtype','Shippers');
              ApexPages.currentpage().getParameters().put('pathwayId',pathwayId );               
              CARPOL_UNI_Contact_Lookup_Controller extclass = new CARPOL_UNI_Contact_Lookup_Controller();
              extclass.objImpContactNew = new Applicant_Contact__c();
              extclass.selectedcountry = String.valueOf([Select Id, Name from Country__c Where Name='United States of America' Limit 1].Id);
              extclass.searchString = 'Korea';
              extclass.isCVBpthwy = false;
              extclass.ACtype = null;
              extclass.search(); 
              extclass.ChangeAndRerenderstate();                      
              extclass.saveAppcont();
              extclass.getFormTag();
              extclass.getTextBox();
              extclass.getjsonmap();
              system.assert(extclass != null);
              
          Test.stopTest();   */
      }    
     @IsTest static void CARPOL_UNI_Contact_Lookup_Controller_Test3() {
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          //Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          id p=[select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = p;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;     
        /*  string pathwayId = [select id from program_line_item_pathway__c  where name = 'Veterinary Biological Products'].id;
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              //PageReference pageRef = Page.Portal_Application_Detail;
              PageReference pageRef = Page.CARPOL_UNI_Contact_Lookup;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('lksrch','Test');
              ApexPages.currentpage().getParameters().put('ACtype','Shippers');
              ApexPages.currentpage().getParameters().put('pathwayId',pathwayId );               
              CARPOL_UNI_Contact_Lookup_Controller extclass = new CARPOL_UNI_Contact_Lookup_Controller();
              extclass.objImpContactNew = new Applicant_Contact__c();
              extclass.selectedcountry = String.valueOf([Select Id, Name from Country__c Where Name='United States of America' Limit 1].Id);
              extclass.searchString = 'Korea';
              extclass.isCVBpthwy = true;
              extclass.ACtype = null;
              extclass.search(); 
              extclass.ChangeAndRerenderstate();                      
              extclass.saveAppcont();
              extclass.getFormTag();
              extclass.getTextBox();
              extclass.getjsonmap();
              system.assert(extclass != null);
              
          Test.stopTest();   */
      }                
}