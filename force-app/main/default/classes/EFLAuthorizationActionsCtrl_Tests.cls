@isTest
public class EFLAuthorizationActionsCtrl_Tests {

    @isTest
    static void codeCoveragePlease(){//...
    	CARPOL_BRS_TestDataManager dm = new CARPOL_BRS_TestDataManager();
        dm.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = dm.newapplication();
        Authorizations__c auth = dm.newAuth(app.Id);
		AC__c LineItem = new AC__c();
        LineItem = dm.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Saved');        
        test.startTest();
        ApexPages.currentPage().getParameters().put('id',auth.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(auth);
        
        // NOTE: ALL OF THES METHOD CALLS BELOW SHOULD BE A SEPARATE UNIT TEST THAT SPECIFICALLY TESTS
        // THAT SINGLE METHOD CALL.  THIS IS BEING PUT TOGETHER IN ORDER TO HAVE AT LEAST SOME CODE COVERAGE ON THE CLASS.
               
        // cover constructor.
        EFLAuthorizationActionsCtrl ctrl = new EFLAuthorizationActionsCtrl(sc);
        
        // cover some other method.
        ctrl.returntoauth();
        
        // cover another random method.
        ctrl.validateSelection();
        
        // and again.
        ctrl.navigateBack();
        
        // and again.
        ctrl.cancelAuth();
        
        // and again.
        ctrl.revokeAuth();
        
        // and again.
        ctrl.withdrawAuth();
        test.stopTest();
    }
}