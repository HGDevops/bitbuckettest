public inherited sharing class EFLRandomInspectionHandler {
    public static void createRandomInspectionRecord(List<EFL_Random_Inspection__c> RandomInspectionsList){
        
        List<String> PrefixList = new List<String>();
        
        List<Inspection__c> inspToInsert = new List<Inspection__c>();
        for(EFL_Random_Inspection__c Record:RandomInspectionsList)
        {
            EFL_Random_Inspection_Prefix__c PrefixConfig = EFL_Random_Inspection_Prefix__c.getInstance(String.valueOf(Record.Prefix_Number__c));
            if(PrefixConfig!=null){
                PrefixList.add(String.valueOf(PrefixConfig.Name));
            }else{
                Record.addError('The system has not been configured to initiate Random Inspections for this Prefix Number.');
            }
            
        }    
        
        if(PrefixList.size()>0)
        {
            
            String ProgramName;
            List<Authorizations__c>PrefixAuths = [SELECT ID,Name,Prefix__c,Program__c FROM Authorizations__c WHERE (Status__c=:'Issued' AND Prefix__c IN: PrefixList) ];
            
            if(PrefixAuths.size()>0)
            {
                Integer authListSize = PrefixAuths.size();
                for(EFL_Random_Inspection__c Record:RandomInspectionsList)
                {  
                    Integer targetNumber = integer.valueOf((Record.Percentage_of_defined_population__c/100)*authListSize);
                    
                    List<String> AuthIDs = new List<String>();
                    for(Authorizations__c Auth : PrefixAuths){
                        AuthIDs.add(auth.Id);
                        ProgramName = Auth.Program__c;
                    }
                    
                    //create a set to hold the returned ids so that we can make sure there are no dupliates
                    Set<String> usedIds = new Set<String>();
                    
                    //Now lets get the random Auth IDs
                    for(Integer i=0; i<targetNumber; i++){
                        String randomId = randomizer.getRandomString(AuthIDs);
                        //check for duplicates
                        while(usedIDs.contains(randomId)){
                            randomId = randomizer.getRandomString(AuthIDs);
                        }
                        usedIDs.add(randomId);
                    }
                    
                    
                    Map<String, Schema.RecordTypeInfo> rtMapByName= Schema.SObjectType.Inspection__c.getRecordTypeInfosByName();
                    
                    for(String AuthID : usedIDs){
                        Inspection__c NewInsp = new Inspection__c();
                        NewInsp.Authorization__c = AuthID;
                        NewInsp.Random_Inspection__c = Record.id;
                        
                        
                        if (ProgramName == 'VS') 
                        { 
                            NewInsp.RecordTypeId = rtMapByName.get('Veterinary Services (VS)').getRecordTypeId();
                        }
                        else if (ProgramName == 'AC') 
                        {
                            NewInsp.RecordTypeID = rtMapByName.get('Animal Care (AC)').getRecordTypeId();
                        }
                        else if (ProgramName == 'PPQ') 
                        {
                            NewInsp.RecordTypeID = rtMapByName.get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
                        }
                        else {
                            NewInsp.RecordTypeID = rtMapByName.get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
                        }
                        
                        
                        inspToInsert.add(NewInsp);
                        
                    }
                    
                    
                }
            }else{
                RandomInspectionsList[0].addError('There are no Authorizations available to initiate a random inspection for the combination you entered.');
            }
            
            
            if(inspToInsert!=null)
                insert inspToInsert;
            
        }
        else{
            
        }
        
    }
}