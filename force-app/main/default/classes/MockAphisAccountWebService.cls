@isTest
global class MockAphisAccountWebService implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {
       string body = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><aph:ValidateAccountResponse xmlns:aph="aphis.vs.ufs"><aph:payload>ineligible</aph:payload></aph:ValidateAccountResponse></soapenv:Body></soapenv:Envelope>';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setHeader('SOAPAction', '""');
        res.setBody(body);
        res.setStatusCode(200);
        return res;
    }
}