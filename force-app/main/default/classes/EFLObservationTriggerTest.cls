//Developer Name: Peter Tran
//Creation Date: 03/19/2019
//File Name: EFLObservationTriggerTest.apxc
//sObject Affected: Observation__c
//Client: United States Department of Agriculture
//Client Project Name: APHIS CARPOL
//Contractor: Accenture Federal Services

// updated by JB 4/23/2019
// - removing the dependency on specific validation rules firing
// - ADDED FIXME COMMENTS

@isTest
private class EFLObservationTriggerTest {    
    //This test method creates several Observation__c dummy records with two records containing the same Observation_Date__c and inserts it into the database
    //A list of Observation__c records are called using a SOQL query to fetch the records from this date range: 2019-03-17 to 2019-03-19
    //Upon the insert of an Observation__c record, static handler method checkObservationStartDate(List<Observation__c triggerNew) is invoked
    //// FIXME: WHAT DOES checkObservationStartDate DO?  WHAT NEEDS TO BE EVALUATED?? - JB
    //Once the test has finished only three records should have been inserted
    @isTest
    static void checkForDuplicateObservationStartDateTest() {
        //Create dummy Observation__c records
        System.debug('EFLObservationTriggerTest: Creating Observation__c dummy records...');
        Observation__c o1 = new Observation__c(Observation_Date__c = Date.valueOf('2019-03-17'), Number_Of_Volunteers__c = 5, Action_Taken__c = 'Test', Self_Reporting__c = 'a0nr0000002dr8D');  
        Observation__c o2 = new Observation__c(Observation_Date__c = Date.valueOf('2019-03-18'), Number_Of_Volunteers__c = 5, Action_Taken__c = 'Test', Self_Reporting__c = 'a0nr0000002dr8D');  
        Observation__c o3 = new Observation__c(Observation_Date__c = Date.valueOf('2019-03-19'), Number_Of_Volunteers__c = 5, Action_Taken__c = 'Test', Self_Reporting__c = 'a0nr0000002dr8D');  //these records have the same Observation_Date__c
        Observation__c o4 = new Observation__c(Observation_Date__c = Date.valueOf('2019-03-19'), Number_Of_Volunteers__c = 5, Action_Taken__c = 'Test', Self_Reporting__c = 'a0nr0000002dr8D');  //these records have the same Observation_Date__c   		

        //Perform the test 
        Test.startTest();
        System.debug('EFLObservationTriggerTest: Test context has started...');
        //Try to insert the list of records
        String errorString = '';
        try {
            System.debug('EFLObservationTriggerTest: Attempting to insert list of dummy records');
            //FIXME: this test should insert a list of records and not one at a time...
            insert o1;
            insert o2;
            insert o3;
            insert o4; //*** This record should throw an error because the Observation_Start__c is the same as o3***
        } catch (Exception e) {
            System.debug('EFLObservationTriggerTest: There was an error: ' + e);
            errorString = e.getMessage();
        }       	
        
        Test.stopTest();
        //FIXME: this test does not assert that 3 were created as stated above in comments.
        System.debug('EFLObservationTriggerTest: Attempting to get list of dummy Observation__c records created above');
        List<Observation__c> insertedObservationRecords = [SELECT Id, 
                                                           Number_of_Volunteers__c, 
                                                           Action_Taken__c, 
                                                           Self_Reporting__c 
                                                           FROM Observation__c 
                                                           WHERE Self_Reporting__c = 'a0nr0000002dr8D' 
                                                           AND (Observation_Date__c >=: Date.valueOf('2019-03-17') AND Observation_Date__c <=: Date.valueOf('2019-03-19'))];
        System.debug('EFLObservationTriggerTest: There was an error: Here is the String: ' + errorString);
        
        //1. Pass case: The inserted Observation__c record failed to commit to the database. The logic here is that duplicate observation start dates should not be commited to the Salesforce cloud database.
        //2. Fail case: The inserted Observation__c record was commited to the database.
        System.assertNotEquals(null, errorString);
        // FIXME: the following assertion fails, and should check against a custom label rather than hard-code text.
        //System.assertEquals('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, There are record(s) where the Observation Date is the same.: [Observation_Date__c]',errorString);
    }
}