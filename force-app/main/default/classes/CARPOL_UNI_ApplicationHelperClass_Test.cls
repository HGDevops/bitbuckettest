@isTest(seealldata=true)
private class CARPOL_UNI_ApplicationHelperClass_Test{ 
public static CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
public static CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
    @IsTest(seealldata=true) static void CARPOL_UNI_ApplicationHelperClass_Test() {
            CARPOL_UNI_ApplicationHelperClass cont = new CARPOL_UNI_ApplicationHelperClass();
            
            testData.insertcustomsettings();
       
            Application__c objapp = testData.newapplication();
            AC__c li = testData.newlineitem('Personal Use', objapp);
            Authorizations__c objauth = testData.newAuth(objapp.id);
            objauth.status__c = 'Withdrawn';
            update objauth;
            
            Workflow_Task__c objWF = testDataBRS.newworkflowtask('TestWF', objauth, 'Deferred');
            set<id> appIds = new set<id>();
            appIds.add(objapp.id);
            Test.startTest();
            //TO-DO: TESTFIX
            //CARPOL_UNI_ApplicationHelperClass.WithdrawAuthWFs(appIds);
            Test.stopTest();
            
      }
}