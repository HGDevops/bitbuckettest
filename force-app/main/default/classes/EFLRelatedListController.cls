/*
* This class is the controller for component: EFLRelatedList.cmp
*/
public with sharing class EFLRelatedListController {
    
    @AuraEnabled public static PageData getPageData(string parentId, string usageKey){
        // use the EFL Related List MDT to get the instance.
        if(string.isBlank(usageKey)){
            return null;
        }
        User me;
        
        // first get a map of all the fields for this object type so we can get important data later.
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        
        // query the mdt for the setup details.
        EFL_Related_List_Instance__mdt instance = [SELECT Title__c, SObject_Type__c, Parent_Lookup_Field__c, Icon_Name__c, Enable_Row_Selection__c, 
                                                   Edit_Screen_Component__c, Enforce_User_Access_Checks__c, Security_Helper_Class__c,
                                                   Service_Class__c, Permanent_Delete_on_Delete__c,Query_Filter__c, Order_By__c,
                                                   Add_New_Screen_Component__c, Object_Common_Name__c, Title_Sub_Text__c, Edit_Screen_CSS_File__c,
                                                   Save_Button_Text__c, Cancel_Button_Text__c, Modal_Header__c,
                                                   (SELECT Field_API_Name__c, Field_Label__c, Sort_Order__c, Help_Text__c FROM EFL_Related_List_Fields__r
                                                    ORDER BY Sort_Order__c ASC)
                                                   FROM EFL_Related_List_Instance__mdt
                                                   WHERE Usage_Key__c = :usageKey And Is_Test_Data__c = :Test.isRunningTest() LIMIT 1];
        system.debug('instance: ' + instance);
        set<string> fields = new set<string>();
        for(EFL_Related_List_Field__mdt f : instance.EFL_Related_List_Fields__r){
            fields.add(f.Field_API_Name__c);
        }
        list<string> fieldList = new list<string>();
        fieldList.addAll(fields);
        
        PageData pd = new PageData(instance);
        map<id,sObject> allObjects = new map<id,sObject>();
        list<Id> idsInOrder = new list<Id>(); // holds the Ids in the right order defined in the MDT.
        
        EFLRelatedListServices serviceUtil;
        if(string.isNotBlank(instance.Service_Class__c)){
            // if a service class is named, then use the service class getRecords method.
            // This is handy when without sharing is needed.
            Type serviceUtilClass = Type.forName(instance.Service_Class__c);
            serviceUtil = (EFLRelatedListServices)serviceUtilClass.newInstance();
        }else{
            // if no service class named, use the base class by default.
            serviceUtil = new EFLRelatedListServices();
        }
        for(sObject o : serviceUtil.getRecords(instance, fields, parentId)){
            allObjects.put((Id)o.get('Id'),o);
            idsInOrder.add((Id)o.get('Id'));
        }
        
        
        EFLRelatedListSecurityUtil_Base util;
        if(instance.Security_Helper_Class__c != null){
        	// figure out who's logged in for security purposes.
            Type securityUtilType = Type.forName(instance.Security_Helper_Class__c);
            util = (EFLRelatedListSecurityUtil_Base)securityUtilType.newInstance();
        	me = util.getMe();
            system.debug('custom security class: ' + me);
        }
        
        map<Id, Record> records = new map<Id, Record>();
        
        for(UserRecordAccess ura : [SELECT RecordId, HasEditAccess, HasDeleteAccess
                                    FROM UserRecordAccess
                                    WHERE RecordId IN :allObjects.keySet()
                                    AND UserId = :UserInfo.getUserId()]){
                                        boolean canEdit = ura.HasEditAccess;
                                        boolean canDelete = ura.HasDeleteAccess;
                                        if(util != null){ // the util may override standard access checks.
                                            canEdit = util.canEdit(allObjects.get(ura.RecordId), me, canEdit);
                                            canDelete = util.canDelete(allObjects.get(ura.RecordId), me, canDelete);
                                        }
                                        records.put(ura.RecordId,new Record(allObjects.get(ura.RecordId), instance.EFL_Related_List_Fields__r, schemaMap, canEdit, canDelete));
                                    }
        for(Id i : idsInOrder){
            pd.records.add(records.get(i));
        }
        return pd;
    }
    /*
    @TestVisible
    private static list<sObject> getRecords(EFL_Related_List_Instance__mdt instance, set<string> fields, Id parentId){
        fields.add('Id');
        fields.add('Name');
        list<string> fieldList = new list<string>();
        fieldList.addAll(fields);
        // make a dynamic query & get all the records.
        string qry = 'SELECT ' + string.join(fieldList,',');
        qry += ' FROM ' + instance.SObject_Type__c;
        qry += ' WHERE ' + instance.Parent_Lookup_Field__c + ' = \'' + parentId + '\'';
        if(instance.Query_Filter__c != null){
            qry += ' AND ' + instance.Query_Filter__c;
        }
        if(instance.Order_By__c != null){
            qry += ' ORDER BY ' + instance.Order_By__c;
        }
        system.debug('query: ' + qry);
        return Database.query(qry);
    }
    */
    @AuraEnabled public static void deleteRecords(list<Id> recordIds, string objectName, string serviceClass, boolean doDelete){
        EFLRelatedListServices serviceUtil;
        if(string.isNotBlank(serviceClass)){
            Type serviceUtilClass = Type.forName(serviceClass);
            serviceUtil = (EFLRelatedListServices)serviceUtilClass.newInstance();
        }else{
            serviceUtil = new EFLRelatedListServices();
        }
        serviceUtil.doDelete(recordIds, objectName, doDelete);
    }
    
    public class PageData{
        // this class contains all data passed to client side.
        @AuraEnabled public string iconName{get;set;}
        @AuraEnabled public string title{get;set;}
        @AuraEnabled public string sObjectType{get;set;}
        @AuraEnabled public boolean rowSelectEnabled{get;set;}
        @AuraEnabled public string editComponent{get;set;}
        @AuraEnabled public string addNewComponent{get;set;}
        @AuraEnabled public string objectCommonName{get;set;}
        @AuraEnabled public string serviceClass{get;set;}
        @AuraEnabled public boolean permaDelete{get;set;}
        @AuraEnabled public list<Record> records{get;set;}
        @AuraEnabled public EFL_Related_List_Instance__mdt config{get;set;}
        public PageData(EFL_Related_List_Instance__mdt instance){
            this.config = instance;
            /*this.iconName = instance.Icon_Name__c;
            this.title = instance.Title__c;
            this.rowSelectEnabled = instance.Enable_Row_Selection__c;
            this.sObjectType = instance.SObject_Type__c;
            this.editComponent = instance.Edit_Screen_Component__c;
            this.serviceClass = instance.Service_Class__c;
            this.permaDelete = instance.Permanent_Delete_on_Delete__c;
            this.addNewComponent = instance.Add_New_Screen_Component__c;
            this.objectCommonName = instance.Object_Common_Name__c;*/
            this.records = new list<Record>();
        }
        
        public void addRecord(sObject obj, list<EFL_Related_List_Field__mdt> fields, Map<String, Schema.SObjectType> schemaMap, boolean canEdit, boolean canDelete){
            this.records.add(new Record(obj, fields, schemaMap,canEdit, canDelete));
        }
    }
    
    public class Record{
        @AuraEnabled public list<Field> fields{get;set;}
        @AuraEnabled public string name{get;set;} // everything has a name.  Names are meant to be clickable when used.
        @AuraEnabled public Id id{get;set;} // everything has an Id.
        @AuraEnabled public string sObjectType{get;set;}
        @AuraEnabled public boolean isSelected{get;set;}
        @AuraEnabled public boolean isSelectable{get;set;}
        @AuraEnabled public boolean canEdit{get;set;}
        @AuraEnabled public boolean canDelete{get;set;}
        public Record(sObject obj, list<EFL_Related_List_Field__mdt> fields, Map<String, Schema.SObjectType> schemaMap, boolean editable, boolean deletable){
            this.name = (string)obj.get('Name');
            this.id = (string)obj.get('Id');
            this.fields = new list<Field>(); // note, order of fields is assumed to be correct here.
            this.isSelected = false;
            this.isSelectable = deletable;
            this.canEdit = editable;
            this.canDelete = deletable;
            this.sObjectType = string.valueOf(obj.getSObjectType());
            if(!fields.isEmpty()){
                Schema.SObjectType objSchema = schemaMap.get(this.sObjectType);
                Map<String, Schema.SObjectField> fieldMap = objSchema.getDescribe().fields.getMap();
                for(EFL_Related_List_Field__mdt s : fields){
                    this.fields.add(new Field(obj,s,fieldMap));
                }
            }
        }
    }
    
    public class Field{
        @AuraEnabled public object value{get;set;}
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string apiName{get;set;}
        @AuraEnabled public string dataType{get;set;}
        public Field(sObject obj, EFL_Related_List_Field__mdt fieldRecord, Map<String, Schema.SObjectField> fieldMap){
            this.apiName = fieldRecord.Field_API_Name__c;
            this.value = EFLUtils.getSObjectFieldObject(obj,this.apiName);
            this.label = fieldRecord.Field_Label__c != null ? fieldRecord.Field_Label__c : fieldMap.get(this.apiName).getDescribe().getLabel();
            this.dataType = string.valueOf(fieldMap.get(this.apiName).getDescribe().getType());
        }
    }
}