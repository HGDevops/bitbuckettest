public class EFLIncidentActivityHandler implements EFLIActivityInterface {
    
    public void populateActivityRecords(sObject parentRecord,list<sObject> activityList ) 
    { 
        
                    Incident__c incRecord = (Incident__c)parentRecord;
                    EFLIIncidentEngine engine = EFLIncidentEngineFactory.getEngine(incRecord);  
        			system.debug('engine '+engine);
                    engine.loadActivities(incRecord, activityList);  
        			system.debug('authRecord '+incRecord);
       				 system.debug('activityList '+activityList);
        			
        
      
    }
    
   public class activityException extends Exception {}

}