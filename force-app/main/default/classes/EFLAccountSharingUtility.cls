/**
 * @author j.benkert@accenturefederal.com
 * @date 4/12/2019
 * @description a utility class for sharing via Account lookup to any child (non-M-D) records
 */
public inherited sharing class EFLAccountSharingUtility {

    /*
     * @description given a list of objects and a program name, update all child objects to match the sharing account
     */
    public static void shareChildrenFromParent(list<sObject> parentObjects, string program){
        sObject baseObject = parentObjects[0];
        string baseType = string.valueOf(baseObject.getSObjectType());
        system.debug('baseType: ' + baseType);
        
        // using custom MDT, get all of the related objects that look up to application 
        // & will need to have the Sharing_Account__c field changed.
        // Line Item + Child records, Authorization, Self Reports, Incidents, Inspections, Conditions
        list<EFLAccountSharingSetting__mdt> types = [SELECT Id,Base_Shared_Object__c,Base_Object_Account_Sharing_Field__c, 
                                                     Related_Object_Account_Field__c, Related_Object_Type_to_Share__c, 
                                                     Related_Object_API_Name__c
                                                    FROM EFLAccountSharingSetting__mdt 
                                                     WHERE Program__c = :program
                                                     AND Base_Shared_Object__c = :baseType];
        
        if(types.isEmpty()){return;}
        
        string baseShareField = types[0].Base_Object_Account_Sharing_Field__c;
        
        // given a list of Authorizations__c records -- if "Sharing Account" is set, then share to all related objects required.
        // Use MDT to fetch the related objects & associated sharing fields.
        map<Id,Id> appToAccount = new map<Id,Id>();
        list<Id> ids = new list<Id>();
        for(sObject app: parentObjects){
            ids.add((Id)app.get('Id'));
            appToAccount.put((Id)app.get('Id'), (Id)app.get(baseShareField));
        }
        string idStr = '(' + string.join(ids,',') + ')';
        
        
        string query = 'SELECT Id, ' + baseShareField + ',';
        list<string> subQueries = new list<string>();
        for(EFLAccountSharingSetting__mdt m:types){
            string q = '(SELECT Id FROM ' + m.Related_Object_Type_to_Share__c + ')';
            subQueries.add(q);
        }
        query += string.join(subQueries, ',');
        query += ' FROM ' + baseType;
        query += ' WHERE Id IN :ids';
        
        list<sObject> toUpdate = new list<sObject>();
        
        list<SObject> allObjects = Database.query(query);
        
        for(SObject a:allObjects){
            for(EFLAccountSharingSetting__mdt m : types){
                if(a.getSObjects(m.Related_Object_Type_to_Share__c) != null){
                    for(sObject o : (list<SObject>)a.getSObjects(m.Related_Object_Type_to_Share__c)){
                        o.put(m.Related_Object_Account_Field__c,a.get(baseShareField));
                        toUpdate.add(o);
                    }
                }
            }
        }
        
        if(!toUpdate.isEmpty()){
            try{
                update toUpdate;
            }catch(Exception e){
                EFLErrorLog.createErrorLog('EFLAccountSharingUtility', e);
            }
        }
    }
}