public class EFLAuthorizationEngineFactory {
	/*
	* Purpose: Get Authorization Engine  
	*/ 
	public static EFLIAuthorizationEngine getEngine(Authorizations__c authRecord) {
		EFLIAuthorizationEngine Engine;
		Engine = getAuthorizationEngine(authRecord);

		if (Engine == null) {
			throw new EFLAuthorizationEngineException('No Authorization Engine found for Authorization: ' + authRecord.Name);
		}

		return Engine;
	}

	/*
	*  Purpose: Get Authorization Engine based on program
	*/
	private static EFLIAuthorizationEngine getAuthorizationEngine(Authorizations__c authRecord){
		system.debug('authRecord.Program__c:&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'+authRecord.Program__c);
		if (authRecord.Program__c == 'BRS') {
			return new EFLBRSAuthorizationEngine();
		} else if (authRecord.Program__c == 'AC') {
			return new EFLACAuthorizationEngine();
		} else if (authRecord.Program__c == 'VS') {
			return new EFLVSAuthorizationEngine();
		} else if (authRecord.Program__c == 'PPQ') {
			return new EFLPPQAuthorizationEngine();
		} else {
			return new EFLBRSAuthorizationEngine();
		}

	}
	
    /*
	* Purpose: Get Authorization Engine  
	*/ 
	public static EFLIAuthorizationEngine getEngineByProgram(string ProgramName) {
		EFLIAuthorizationEngine Engine;
		Engine = getAuthorizationEngineByProgram(ProgramName);

		if (Engine == null) {
			throw new EFLAuthorizationEngineException('No Authorization Engine found for Authorization: ' + ProgramName);
		}

		return Engine;
	}

	/*
	*  Purpose: Get Authorization Engine based on program
	*/
	private static EFLIAuthorizationEngine getAuthorizationEngineByProgram(string ProgramName){
		system.debug('ProgramName:&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'+ProgramName);
		if (ProgramName == 'BRS') {
			return new EFLBRSAuthorizationEngine();
		} else if (ProgramName== 'AC') {
			return new EFLACAuthorizationEngine();
		} else if (ProgramName == 'VS') {
			return new EFLVSAuthorizationEngine();
		} else if (ProgramName == 'PPQ') {
			return new EFLPPQAuthorizationEngine();
		} else {
			return new EFLBRSAuthorizationEngine();
		}

	}
	public class EFLAuthorizationEngineException extends Exception {}

}