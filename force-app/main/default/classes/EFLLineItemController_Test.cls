@isTest(seeAllData=false)
public class EFLLineItemController_Test {
    
    public static ac__c li;
    public static ac__c li1;
    public static ac__c lineItemRecord;
    public Id lineItemID;
    public static Contact con;
    public static Application__c app;
    public static Domain__c prog;
    public static Program_Line_Item_Pathway__c plip;
    public static Program_Prefix__c pp;
    public static Signature__c tp;
    public static Trade_Agreement__c ta;
    public static Country__c Country;
    public static Level_1_Region__c level1;
    public static Level_2_Region__c level2;
    public static Authorizations__c auth; 
    public static Program_Line_Item_Section__c  plis;
    public static Program_Line_Item_Field__c plif;
    public static Program_Line_Item_Field__c plif1;
  //  public static List<dynamicField> lids;
    public static id liRecType;  
    public static void initData(){ 
        liRecType = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Line item').getRecordTypeId();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        con = testData.newContact();
        app = testData.newapplication(); 
        prog = testData.newProgram('test');
        plip = testData.newCaninePathway();
        pp = testData.newPrefix();
        tp = testData.newThumbprint();
        ta = testData.newta();
        // Country = testData.newcountrywithassoc();
        // level1 = testData.newlevel1region(Country.id);
        // level2 = testData.newlevel2region(level1.id);
        auth = testData.newAuth(app.Id);
        li = testData.newLineItem('Adoption', app);
        
        plis = new Program_Line_Item_Section__c();
        plis.Program_Line_Item_Pathway__c = plip.id;
        plis.Name = 'test';
        plis.Type_of_Permit__c = 'Standard Permit';
        plis.Section_Order__c = 123456;
        plis.Include_Applicant_Information__c = true;
        plis.EFLAddMore__c = false;
        plis.section_type__c = 'Exporter Information';
        plis.Destination_Object__c = 'frog';
        plis.Status__c = 'Active';
        plis.RecordTypeID = liRecType;
        plis.Destination_Record_Type__c = 'rest';
        insert plis;
        
        plif = new Program_Line_Item_Field__c();
        plif.Program_Line_Item_Section__c = plis.id;
        plif.Name = 'test';
        plif.isActive__c = 'Yes';
        plif.Movement_Type__c = 'Release';
        plif.Field_Order__c = 10;
        insert plif;
        
         
    }
    
    public static testMethod void AllGetMethod_Test(){
        initData();
        test.startTest();
        Test.setCurrentPage(Page.EFLLineItem);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(li);
        ApexPages.currentPage().getParameters().put('id',li.id);
        EFLLineItemController ELI = new  EFLLineItemController(sc);
        EFLLineItemController.lineitemSection liSection = new EFLLineItemController.lineitemSection();
        EFLLineItemController.dynamicField df = new EFLLineItemController.dynamicField();
               
        boolean testLockLine = ELI.lockLineItem;
        boolean disableCBIField = ELI.disableCBIField;
        string isErr = ELI.isError;
        list<boolean> isReqList = ELI.lstRequired;
        list<Application_Request__c> appReqList = ELI.appResList;
        
        
        string inst = ELI.instructions;
        ELI.chevronOptions = null;
        string chevOpt = ELI.chevronOptions;
        
       
        
        ELI.CBIVal = false;
        ELI.Save();
        
        ELI.CBIVal = true;
        ELI.Save();
        PageReference p1 = ELI.Cancel(); 
        decimal openSection = ELI.openSection;
        ELI.CBIVal = false;
        ELI.getLineItemSectionDetails();
        
        ELI.CBIVal = null;
        liSection.liSection = plis;
        ELI.getLineItemSectionDetails();
        
      
        ELI.getcurrentPanel('test', 10.0);
        ELI.addNewLISection(plis, liSection.liFields);
        
        ELI.lineItemRecord = null;
        ELI.getLineItemRecord();
        
        System.Assert(true);
        test.stopTest();
    }
    
    
}