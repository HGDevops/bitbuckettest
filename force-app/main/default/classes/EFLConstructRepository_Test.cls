@IsTest
public class EFLConstructRepository_Test {
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();

    @testSetup
    static void setupData(){
        testData.insertcustomsettings();
        Application__c objApp = testData.newapplication();
        AC__c ac1=testData.newLineItemBRS('Personal Use',objApp);
        GenotypeType__c genoTypeType = new GenotypeType__c();
        
		//Commented this code as this below newAuth method is failing with "EFLAuthorizationEngineFactory.EFLAuthorizationEngineException: No Authorization Engine found for Authorization: null"
        //Authorizations__c authObj = testData.newAuth(objApp.id);
        Domain__c objProg = new Domain__c();
        objProg.Name = 'AC';
        objProg.Active__c = true;
        insert objProg;
        
        Program_Prefix__c prefix = new Program_Prefix__c();
        prefix.Program__c = objProg.Id;
        prefix.Name = '555';
        insert prefix; 
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP';
        objTP.Recordtypeid = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c = prefix.Id;
        insert objTP;
        //appObj = testData.newapplication();
        
        Authorizations__c authObj=new  Authorizations__c();
        authObj.Application__c=objApp.id;
        authObj.RecordTypeID=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        //authObj.Status__c='Submitted';
        authObj.Date_Issued__c=date.today();
        authObj.Applicant_Alternate_Email__c='test@test.com';
        authObj.UNI_Zip__c='32092';
        authObj.UNI_Country__c='United States';
        authObj.UNI_County_Province__c='Duval';
        authObj.BRS_Proposed_Start_Date__c=date.today()+30;
        authObj.BRS_Proposed_End_Date__c=date.today()+40;
        authObj.CBI__c='CBI Text';
        authObj.Application_CBI__c='No';
        authObj.Applicant_Alternate_Email__c='email2@test.com';
        authObj.UNI_Alternate_Phone__c='(904) 123-2345';
        authObj.AC_Applicant_Email__c='email2@test.com';
        authObj.AC_Applicant_Fax__c='(904) 123-2345';
        authObj.AC_Applicant_Phone__c='(904) 123-2345';
        authObj.AC_Organization__c='ABC Corp';
        authObj.Means_of_Movement__c='Hand Carry';
        authObj.Biological_Material_present_in_Article__c='No';
        authObj.If_Yes_Please_Describe__c='Text';
        authObj.Applicant_Instructions__c='Make corrections';
        authObj.BRS_Number_of_Labels__c=10;
        authObj.BRS_Purpose_of_Permit__c='Importation';
        authObj.Documents_Sent_to_Applicant__c=false;
        authObj.Expiration_Timeframe_Override__c = 'Manual'; 
        authObj.Status__c = 'Approved';
        authObj.Thumbprint__c = objTP.Id;
        Insert authObj;
                
        Regulated_Article__c regArt = testData.newRegulatedArticle(authObj.Program_Pathway__c);
        link_Regulated_articles__c lra = new link_Regulated_articles__c();
        lra = testData.newlinkRegArticleByLIIdRAId(ac1.Id, regArt.id, 'Draft');
        Construct__c testConstruct = testData.newconstruct(ac1.Id, regArt);
        PhenoType__c phenoTypeExpected = testData.newphenotype(testConstruct.Id);
        genoTypeType.Construct__c = testConstruct.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;
        GenoType__c objGenoType = testData.newgenotype(testConstruct.id, genoTypeType);
        
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        Applicant_Attachments__c appAttObj = testDataAC.newAttach(ac1.id);
        Construct_Application_Junction__c objCAJ = new Construct_Application_Junction__c();
        objCAJ.Construct__c = testConstruct.Id;
	    objCAJ.Application__c = objApp.Id;
	    objCAJ.Authorization__c = authObj.Id;
	    objCAJ.Line_Item__c = ac1.Id;
	    objCAJ.Design_Protocol_Record_SOP__c = appAttObj.Id;
        insert objCAJ;
    }
    
    @IsTest
    static void testSelectPhenotypesByConstruct() {
        testData.insertcustomsettings();
        Construct__c testConstruct = [select Id from Construct__c limit 1];
        Test.startTest();
        List<Phenotype__c> phenoTypes = EFLConstructRepository.selectPhenotypesByConstruct(testConstruct.Id);
        Test.stopTest();
        System.assertEquals(1, phenoTypes.size());
    }
    
    @IsTest
    static void testselectPhenotypeByID(){
        testData.insertcustomsettings();
        PhenoType__c phenoTypeExpected = [select Id from PhenoType__c limit 1];
        Test.startTest();
        EFLConstructRepository.selectPhenotypeByID(phenoTypeExpected.Id);
        Test.stopTest(); 
    }
    
    @IsTest
    static void testselectParentGenotypesByConstruct(){
        testData.insertcustomsettings();
        Construct__c testConstruct = [select Id from Construct__c limit 1];
        Test.startTest();
        EFLConstructRepository.selectParentGenotypesByConstruct(testConstruct.Id);
        Test.stopTest();
    }
    
    @IsTest
    static void testSelectGenotypesByConstruct() {
        Construct__c testConstruct = [select Id from Construct__c limit 1];
        Test.startTest();
        List<Genotype__c> genoTypes = EFLConstructRepository.selectGenotypesByConstruct(testConstruct.id);
        Test.stopTest();
        System.assertEquals(1, genoTypes.size());
    }
    
    @IsTest
    static void testSelectGenotypeByID() {
        testData.insertcustomsettings();
        Genotype__c objGenoType = [select Id from Genotype__C limit 1];
        Test.startTest();
        Genotype__c actualGenoType = EFLConstructRepository.selectGenotypeByID(objGenoType.Id);
        Test.stopTest();
        System.assertEquals(objGenoType.Id, actualGenoType.id);
    }
    
    @isTest
    static void testSelectByID() {
        testData.insertcustomsettings();
        Construct__c testConstruct = [select Id from Construct__c limit 1];
        Test.startTest();
        Construct__c actualConstruct = EFLConstructRepository.selectByID(testConstruct.id);
        Test.stopTest();
        System.assertEquals(testConstruct.Id, actualConstruct.id);   
    }
    
    @isTest
    static void testSelectByIDs() {
        testData.insertcustomsettings();
        Construct__c testConstruct = [select Id from Construct__c limit 1];
        Set<Id> constructIds = new Set<Id>();
        constructIds.add(testConstruct.Id);
        Test.startTest();
        List<Construct__c> actualConstructs = EFLConstructRepository.selectByIDs(constructIds);
        Test.stopTest();
        System.assertEquals(1, actualConstructs.size());         
    }
    
    @isTest
    static void testSelectGenotypesbyRecordtype() {
        testData.insertcustomsettings();
      	Construct__c testConstruct = [select Id from Construct__c limit 1];
        Genotype__c objGenoType = [select Id, RecordTypeId from Genotype__C limit 1];
        Test.startTest();
        List<GenoType__c> actualGenoTypes = EFLConstructRepository.selectGenotypesbyRecordtype(testConstruct.Id, objGenoType.RecordTypeId);
        Test.stopTest();
        System.assertEquals(1, actualGenoTypes.size());         
    }
    
	@isTest
    static void testselectByLineItemID() {
        testData.insertcustomsettings();
        AC__c ac1 = [select Id from AC__c limit 1];
        Test.startTest();
        List<Construct__c> actualConstructs = EFLConstructRepository.selectByLineItemID(ac1.id);
        Test.stopTest();
        System.assertEquals(1, actualConstructs.Size());   
    }
    
    @isTest
    static void testselectIDsByLineItemID() { 
        testData.insertcustomsettings();
        AC__c ac1 = [select Id from AC__c limit 1];
        Test.startTest();
        Set<Id> constructIDs = EFLConstructRepository.selectIDsByLineItemID(ac1.id);
        Test.stopTest();
        System.assertEquals(1, constructIds.size());
    }
    
    @isTest
    static void testSelectConstructs() {
        testData.insertcustomsettings();
        Test.startTest();
        Construct__c testConstruct = [select Id, Line_Item__r.Application_Number__r.Applicant_Name__r.AccountId from Construct__c limit 1];
        Program_Line_Item_Pathway__c plip=testData.newCaninePathway();
        Regulated_Article__c regArt=new  Regulated_Article__c();
        regArt.Name='Test article';
        regArt.RecordTypeID=Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        regArt.Status__c='Active';
        regArt.Category_Group_Ref__c=testdata.newgroup().Id;
        regArt.Program_Pathway__c=plip.id;
        Insert regArt; 
        testConstruct.Link_Regulated_Article__c = regArt.id;
        update testConstruct;
        Set<Id> constructIds = new Set<Id>();
        constructIds.add(testConstruct.Id);
        List<Construct__c> actualConstructs = EFLConstructRepository.selectConstructs('Review Complete', 'Authorized', new Set<Id>{regArt.Id}, false, new List<String>{'Notification%', '%Permit%'}, new List<String>{'Yes', 'No'}, testConstruct.Line_Item__r.Application_Number__r.Applicant_Name__r.AccountId);
        List<Construct__c> actualConstructs2 = EFLConstructRepository.selectConstructs(null, null, null, false, null, null, null);
        Test.stopTest();
    }
    
    @IsTest
    static void testselectConstructsByLineItemID(){
        testData.insertcustomsettings();
        AC__c ac1 = [select Id from AC__c limit 1];
        Test.startTest();
        EFLConstructRepository.selectConstructsByLineItemID(ac1.Id);
        Test.stopTest();
        
    }
    
    @IsTest
    static void testAppLineItemLRAGenoPhenoChanges() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        /*user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;*/
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticle(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticle(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
        Phenotype__c phenotype = new Phenotype__c();
        Genotype__c genotype = new Genotype__c(); 
        
        test.startTest();
        
        //system.runAs(userShare)
        //{
            
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = testData.newappcontact();
            
            associatedContact.Account__c = newAccount.id;
            update associatedContact;
            
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Open', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, null, 'Release', 'Saved');
            
            LRA = new Link_Regulated_Articles__c();
            LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Draft');   
            
            con = new Construct__c();
            con = testdata.newconstructByLIIdLRAId(LineItem.id, RA.Id, 'Waiting on Customer');
            
            phenotype = new Phenotype__c();
            phenotype = testdata.newphenotype(con.id);
            
            GenotypeType__c genotypeType = new GenotypeType__c(Genotype_Category__c='Empty Transformation Vector', Construct__c=con.id);
            insert genotypeType;
            genotype = testdata.newgenotype(con.id, genotypeType);
            
            EFLConstructRepository.selectPhenotypesByConstructForClone(con.id);
            EFLConstructRepository.selectgenoTypeforClone(genotypeType.id);
            EFLConstructRepository.selectConstructComponentsForReOrdering(genotype.id, genotypeType.id, '<=');
            
        //}
        test.stopTest();
        
    }
    
    @IsTest
    static void testExceptionPaths() { 
        try{EFLConstructRepository.selectGenotypesbyRecordtype(null, null);}catch(exception ex){}
        try{EFLConstructRepository.selectByLineItemID(null);}catch(exception ex){}
        try{EFLConstructRepository.selectAllConstructs();}catch(exception ex){}
        try{EFLConstructRepository.selectByIDs(null);}catch(exception ex){}
        try{EFLConstructRepository.selectIDsByLineItemID(null);}catch(exception ex){}
        try{EFLConstructRepository.selectConstructs(null, null, null, null, null, null, null);}catch(exception ex){}
        try{EFLConstructRepository.selectConstructsByLineItemID(null);}catch(exception ex){}
        try{EFLConstructRepository.selectConstructComponentsForReOrdering(null, null, null);}catch(exception ex){}        
    }
    
    
}