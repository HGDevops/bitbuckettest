@isTest
public class EFLCloneFactory_Test {
    Static testmethod void method1(){
        Domain__c pgm = new Domain__c();
        pgm.name = 'BRS';
        insert pgm;
        Program_Prefix__c pgmPrefix = new Program_Prefix__c();
        pgmPrefix.program__c = pgm.id;
        insert pgmPrefix;
        
        Signature__c sig = new Signature__c();
        sig.RecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        sig.Program_Prefix__c = pgmPrefix.id;
        insert sig;
        CARPOL_BRS_TestDataManager carBRSTest = new CARPOL_BRS_TestDataManager();
        carBRSTest.insertcustomsettingsWithBRSTriggerDisabled();
        String AccountRecordTypeId = carBRSTest.AccountRecordTypeId; 
        Contact objCont = new Contact();
    objCont.FirstName = 'Global Contact'+carBRSTest.randomString5();
    objcont.LastName = 'LastName'+carBRSTest.randomString5();
    objcont.Email = carBRSTest.randomString5()+'test@email.com';        
    objcont.AccountId = carBRSTest.newAccount(AccountRecordTypeId).id;
    objcont.MailingStreet = 'Mailing Street'+carBRSTest.randomString5();
    objcont.MailingCity = 'Mailing City'+carBRSTest.randomString5();
    objcont.MailingState = 'Ohio';
    objcont.MailingCountry = 'United States';
    objcont.MailingPostalCode = '32092'; 
    insert objcont;
              String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId(); 
        Application__c objapp = new Application__c();
        objapp.Applicant_Name__c = objcont.Id;
        objapp.Recordtypeid = ACAppRecordTypeId;
        objapp.Application_Status__c = 'Open';
        insert objapp; 
        Authorizations__c auth = new Authorizations__c();
        auth.RecordTypeId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId(); 
        auth.BRS_Introduction_Type__c='Interstate Movement';
        auth.thumbprint__c = sig.id;
        auth.effective_date__c=system.today();
        auth.Expiry_Date__c = system.today().adddays(250);
        auth.BRS_Proposed_Start_Date__c=system.today();
        auth.BRS_Proposed_End_Date__c = system.today().adddays(250);
        auth.Application__c=objapp.id;
        auth.BRS_Purpose_of_Permit__c='Traditional';
        auth.Authorization_Type__c='Permit';
        auth.BRS_Introduction_Type__c='Import';
        insert auth;
        Program_Line_Item_Pathway__c plip =carBRSTest.newCaninePathway();
         AC__c ac = new AC__c();
        ac.RecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordTypeId();
        ac.authorization__c = auth.id;
        ac.Departure_Time__c = date.today()+11;
    ac.Arrival_Time__c = date.today()+15;
    ac.Proposed_date_of_arrival__c = date.today()+15;
    ac.Program_Line_Item_Pathway__c=plip.id;
    ac.Transporter_Type__c = 'Ground';
    ac.Date_of_Birth__c = date.today()-400;
    ac.Color__c = 'Brown';
    ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
    ac.Sex__c = 'Male';
    ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
    ac.Status__c = 'Saved';
    ac.application_number__c=objapp.id;
    insert ac;
        system.test.starttest();
        //TO-DO: TESTFIX
        /*EFLCloneFactory.cloneRecord('BRS','Clone',ac,objapp);
        EFLCloneFactory.cloneRecord('VS','Clone',ac,objapp);
        EFLCloneFactory.cloneRecord('AC','Clone',ac,objapp);
        EFLCloneFactory.cloneRecord('PPQ','Clone',ac,objapp);
        EFLCloneFactory.cloneRecord('None','Clone',ac,objapp);*/
        system.test.stoptest();
    }
}