public with sharing class CARPOL_AC_Create_RegOnAuth_Extension {

    public Regulation__c newRegulation { get; set; }
    Public Authorization_Junction__c authJunctionRecord {get; set; }
    public Authorizations__c currentAuthorization;
    public ID RegRecordtypeId {get; set;}
    public string RegRecordtypeName{get; set;}
    public id wfid {get;set;}
    
    //constants
    static final string ACRECORDTYPENAME='Animal Care (AC)';
    static final string BRSRECORDTYPENAME='Biotechnology Regulatory Services (BRS)';
    static final string PPQRECORDTYPENAME='Plant Protection & Quarantine (PPQ)';
    static final string VSRECORDTYPENAME='Veterinary Services (VS)';
    static final string ACTIVE = 'Active';
    static final string YES = 'Yes';
    static final string PERSONAL = 'Personal';
    static final string SHARED = 'Shared';
    
    public CARPOL_AC_Create_RegOnAuth_Extension(ApexPages.StandardController controller) {
        
        newRegulation = new Regulation__c();
        authJunctionRecord = new Authorization_Junction__c();
        this.currentAuthorization= (Authorizations__c)controller.getRecord();
        wfid = ApexPages.currentPage().getParameters().get('wfid');
        
        if (currentAuthorization.Id!=null)
            {
                currentAuthorization = [Select Id, Program_Pathway__r.Name, RecordType.Name,Program__c, Application__c, Thumbprint__c FROM Authorizations__c Where Id =:currentAuthorization.Id];
                /*if(currentAuthorization.Program__c!=null && currentAuthorization.Program__c ==  EFLGlobalConstants.getConstantValue('PROGRAM AC')){
                    RegRecordtypeName = 'Animal Care (AC)';
                    RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
                }else if(currentAuthorization.Program__c!=null && currentAuthorization.Program__c == 'BRS'){
                    RegRecordtypeName = 'Biotechnology Regulatory Services (BRS)';
                    RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
                }else if(currentAuthorization.Program__c!=null && currentAuthorization.Program__c == 'PPQ'){
                    RegRecordtypeName = 'Plant Protection & Quarantine (PPQ)';
                    RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
                }else if(currentAuthorization.Program__c!=null && currentAuthorization.Program__c == 'VS'){
                    RegRecordtypeName = 'Veterinary Services (VS)';
                    RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
                }*/
                
                 //Nitish edit - Start
                 if(currentAuthorization.Program__c!=null)
                     {
                         if(currentAuthorization.Program__c == EFLGlobalConstants.getConstantValue('PROGRAM AC'))
                             {
                                 RegRecordtypeName = 'Animal Care (AC)';
                                 RegRecordtypeID =  EFLGenericUtility.getRecordTypeId('Regulations AC'); 
                                 RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
                             }
                         else if(currentAuthorization.Program__c == 'BRS')
                             {
                                 RegRecordtypeName = 'Biotechnology Regulatory Services (BRS)';
                                 RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
                             }
                         else if(currentAuthorization.Program__c == 'PPQ')
                             {
                                 RegRecordtypeName = 'Plant Protection & Quarantine (PPQ)';
                                 RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
                             }
                         else 
                             {   
                                 if(currentAuthorization.Program_Pathway__r.Name == EFLGlobalConstants.getConstantValue('PROGRAM PATHWAY CVB') )
                                 {
                                     RegRecordtypeName = EFLGlobalConstants.getConstantValue('PROGRAM PATHWAY CVB');
                                     RegRecordtypeId = EFLGenericUtility.getRecordTypeId('Regulations CVB'); 
                                 }else{
                                     RegRecordtypeName = VSRECORDTYPENAME;
                                     RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
                                 }
                             }
                     }
                     //Nitish edit - end
            }
            System.Debug('<<<<<<< RegRecordtypeId >>>>>>>'+RegRecordtypeId);
    }
    
    public PageReference returnEditPermit() { 
        PageReference pageRef;// = Page.CARPOL_AC_EditPermit_Regulations2;
        if(currentAuthorization.Program__c!=null && currentAuthorization.Program__c == 'BRS'){
               pageRef = Page.CARPOL_BRS_EPRegulations;
          }else 
          { 
              // Commented old page and replaced with new permit page
              // pageRef = Page.CARPOL_AC_EditPermit_Regulations2;  
               pageRef = Page.eflpreparepermit; 
          }
        pageRef.getParameters().put('Id',currentAuthorization.Id);
        pageRef.getParameters().put('wfid',EFLGenericUtility.sanitizeString(wfid));
        pageRef.setRedirect(true);
        return pageRef;     
     }
        
    public PageReference createReg(){
        newRegulation.RecordTypeId = RegRecordtypeId;//Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        newRegulation.Status__c = 'Active';
        newRegulation.Shared_Personal__c = 'Personal';
        insert newRegulation ;
        authJunctionRecord.Authorization__c = currentAuthorization.Id;
        authJunctionRecord.Is_Active__c = 'Yes';
        authJunctionRecord.Regulation__c = newRegulation.Id;
        authJunctionRecord.Regulation_Description__c = newRegulation.Regulation_Description__c;
        authJunctionRecord.RecordTypeId = Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('Regulation Junction').getRecordTypeId();
        /* if(sObjectType.getDescribe().isAccessible() && sObjectType.getDescribe().isDeletable()){
                Delete delList;
              }
        */
        insert authJunctionRecord;
        PageReference pageRef;
        if(currentAuthorization.Program__c!=null && currentAuthorization.Program__c == 'BRS'){
               pageRef = Page.CARPOL_BRS_EPRegulations;
          }else 
          { 
              pageRef = Page.eflpreparepermit; 
              
          }
        pageRef.getParameters().put('Id',currentAuthorization.Id);
        pageRef.getParameters().put('wfid',EFLGenericUtility.sanitizeString(wfid));
        pageRef.setRedirect(true);
        return pageRef;
    }
}