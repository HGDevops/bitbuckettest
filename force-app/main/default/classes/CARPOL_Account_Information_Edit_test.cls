@isTest(seeAllData=true)
private class CARPOL_Account_Information_Edit_test{
static testMethod void UnitTest() {    
        User u = new user();
    	u=EFLUserTestDataFactory.getUser('eFile Applicant'); 
        Test.startTest();  
        System.runAs(u){     
            PageReference pRef = Page.CARPOL_Account_Information_Edit;      
            Test.setCurrentPage(pRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(u);
            CARPOL_Account_Information_Edit pc = new CARPOL_Account_Information_Edit(sc);    
            pc.adminUser = null;   
            pc.save();
            pc.cancel();
        }
        Test.stopTest();
    }
}