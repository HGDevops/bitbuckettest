public virtual class EFLCustomLookupUtil {

    public integer searchLimit = 5;
    public string sObjectToSearch;
    public string sortOrder;
    public set<string> whereFields;
    public string queryFilter;
    
    public void setParams(EFLCustomLookUpController.PageData pd){
        this.sObjectToSearch = pd.sObjectName;
        this.searchLimit = pd.resultListSize;
        this.whereFields = pd.whereFields;
        this.queryFilter = pd.queryFilter;
        this.sortOrder = pd.sortOrder;
    }
    
    public virtual list<EFLCustomLookupController.LookupItem> queryData(string searchText){
        string srch = string.escapeSingleQuotes(searchText);
        
        String sQuery =  'SELECT id, Name';
        sQuery += ' FROM ' +sObjectToSearch;
        
        set<string> wheres = new set<string>();
        if(this.whereFields != null){
            wheres = this.whereFields;
        }
        sQuery += getWhereClause(wheres, searchText);
        
        if(this.queryFilter != null){
            sQuery += ' AND ' + this.queryFilter;
        }

        if(this.sortOrder != null){
        	sQuery += ' ORDER BY createdDate DESC';
        }
        
        if(this.searchLimit != null){
        	sQuery += ' LIMIT ' + searchLimit;
        }
        
        list<EFLCustomLookupController.LookupItem> returnList = new list<EFLCustomLookupController.LookupItem>();
        List < sObject > lstOfRecords = Database.query(sQuery);
        for (sObject obj: lstOfRecords) {
            returnList.add(new EFLCustomLookupController.LookupItem((string)obj.get('Name'), obj.Id, null));
        }
        return returnList;
    }
    
    public string getWhereClause(set<string> wheres, string srch){
        wheres.add('Name');
        list<string> whereFlds = new list<string>();
        whereFlds.addAll(wheres);
        list<string> whereClause = new list<string>();
        if(whereFlds != null && !whereFlds.isEmpty()){
            for(string s : whereFlds){
                whereClause.add(' ' +  s + ' LIKE \'%' + srch + '%\'');
            }
        }
        string whereString = ' WHERE (' + string.join(whereClause,' OR ') + ')';
        return whereString;
    }
}