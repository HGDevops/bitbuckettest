@isTest(seealldata=false)
private class portal_assoc_contact_edit_Test {
      @IsTest static void portal_assoc_contact_edit_Test() {

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);
          Applicant_Contact__c objacc = testData.newappcontact();

          User usershare = new User();
          usershare = EFLUserTestDataFactory.getUser('eFile Applicant');       
          
          Test.startTest(); 
        
          //run as salesforce user
              PageReference pageRef = Page.Portal_Application_Edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objacc);
              ApexPages.currentPage().getParameters().put('id',objacc.id);                                                                                                   
              portal_assoc_contact_edit extclass = new portal_assoc_contact_edit(sc);
              extclass.editAssocContact();     
              extclass.dir();
              System.assert(extclass != null);                      
          Test.stopTest();   
      }
}