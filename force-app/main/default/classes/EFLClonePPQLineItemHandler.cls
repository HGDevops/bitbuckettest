/**
 * Purpose: Handles the application with PPQ line item cloning functionality
**/ 
public with sharing class EFLClonePPQLineItemHandler  implements EFLCloneInterface{

    //Cloning Record of line Item and PPQ Related Records
    public list<sObject> cloneRecord(sObject record, sObject applicationReference, string ActionType, authorizations__c authRecord)
    { 
       list<sObject> sObjectList  = new list<sObject>(); 
       ac__c originalLineItem = (ac__c)record;
       list<ac__c> clonedLineItemList = new list<ac__c>();

        //Creates the External ID for each Line Item
       string EXTERNALID = originalLineItem.ID+EFLGenericUtility.randomString();
       ac__c lineItemReference = new ac__c(ExternalID__c= EXTERNALID);

       //Clone Line Item and add to sObjectlist Array 
       clonedLineItemList.add(EFLCloneUtility.cloneLineItem(originalLineItem,(application__c)applicationReference, EXTERNALID,ActionType, authRecord)); 
       sObjectList.addAll(clonedLineItemList); 

       //Clone Applicant Attachment and add to sObjectlist Array
       list<sObject> clonedApplicantAttachmentList =  EFLCloneUtility.cloneApplicantAttachments(originalLineItem,lineItemReference);
       sObjectList.addAll(clonedApplicantAttachmentList);
        
     return sObjectList; 
    }
    
    private void resetRecordFields(){
        
        
    } 
}