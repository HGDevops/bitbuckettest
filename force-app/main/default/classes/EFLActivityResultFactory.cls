public inherited sharing class EFLActivityResultFactory {
    
    public static EFLIActivityResultProcessor getActivityResultProcessor(string movementType, string pathway, string activityName)
    {
        EFLIActivityResultProcessor arp = null;
        String implementation;
        
        try{
            //Query CMD to get the name of the implementation for movement type and Program Pathway
            
                implementation = getImplementation (movementType, pathway, activityName) ;
            
            
            if(String.isNotBlank(implementation))
            {
                Type myType=Type.forName(implementation);
                if (myType!= null)
                {
                    arp = (EFLIActivityResultProcessor) myType.newInstance();
                }
            }
            
        }
        catch (Exception ex)
        {
            
        }
        return arp;
    }
    
  
    
    public static String getImplementation(string MovementType, string ProgramPathway, string PSQActivityName)
    {
        String implementation;
        if(String.isNotBlank(MovementType) || String.isNotBlank(ProgramPathway))
        {
            EFLPSQActivityReference__mdt[] result = [ SELECT PSQActivity_Processor__c FROM EFLPSQActivityReference__mdt WHERE Movement_Type__c = :MovementType and Program_Pathway__c = :ProgramPathway and PSQActivity_Name__c = :PSQActivityName];
            if(result != null)
            {
                implementation = result[0].PSQActivity_Processor__c;
            }
        }        
        
        return implementation;
    }
    

}