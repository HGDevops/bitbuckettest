public with sharing class EFLACLRAExplnController {
    @AuraEnabled
    public static List<EFL_AC_Explanations__c> fetchPopupData(){
        return [Select name,Explanation_Text__c FROM EFL_AC_Explanations__c];
    }    
    
    //This method parses the question (in rich text format) & 
    //convert it in to List<richTextData> which contains hover(clickable) & non hover(non clickable) part of the question
    @AuraEnabled
    public static QuestionData fetchWQ(Wizard_Questionnaire__c questionSelections){
        Wizard_Questionnaire__c wq = new Wizard_Questionnaire__c();
        String Question = '';
        wq = questionSelections;
        //wq = [select ID,Wizard_Question_Rich_Text__c,(SELECT Value__c FROM Wizard_Questionnaire__r ORDER BY Display_Order__c ASC) 
        //									from  Wizard_Questionnaire__c where Id=:'a0vr0000000RErP' limit 1];
        Question = wq.Wizard_Question_Rich_Text__c;  
        List<String> allHoverTexts = new List<String>(); 
        List<String> nonHoverTexts = new List<String>();
        List<String> allLIs = new List<String>();
        //List<answerData> allOptions = new List<answerData>();
        String tagchecker = '<u>'; 		//check for <u> tag
        Integer substrLen = tagchecker.length();
        Integer count = 0;
        Integer index;
        if(Question!=null)
            index = Question.indexOf(tagchecker);
        //Count total occurances of any keyword (identified using occurances of <u> tag)
        while (index >= 0) {
            count++;
            Question = Question.substring(index+substrLen);
            index = Question.indexOf(tagchecker);
        }
        System.debug(count);
        List<richTextData> richTextDataList = new List<richTextData>();
        //Store complete question with tags as a string first
        Question = wq.Wizard_Question_Rich_Text__c;
        //Iterate over the number of occurances of keywords.
        for(Integer i=0;i<count;i++){
            richTextData richTextData = new richTextData();
            //Store keyword in a variable
            String hoverText = Question.substringBetween('<u>','</u>');
            //The question would be split on the basis of the keyword's first occurance
            String subStrToRemove = '<u>'+hoverText+'</u>';
            //Get the first nonHover/static text
            String nonHoverText = Question.split(subStrToRemove)[0];
            //Check if the static text has bullets
            if(nonHoverText.contains('<li>')){
                String newNonHoverText = nonHoverText.substringBeforeLast('<li>');
                //Get the text after the bullet point icon
                String liText = nonHoverText.substringAfterLast('<li>');
                String ulText = '';
                //Store the text between the end of the keyword and end of the bulleted sentence
                String postExpLiText =  Question.split(subStrToRemove)[1].substringBefore('</li>');
                //sub-bullets are part of bullet. check if postExpLiText has sub bullets
                if(postExpLiText.contains('<ul>')){
                    postExpLiText = Question.split(subStrToRemove)[1].substringBefore('<ul>');
                    //Get the sub bullet text
                    ulText= '<ul style="margin-left: 40px;">'+Question.split(subStrToRemove)[1].substringBetween('<ul>','</ul>')+'</ul>';
                    Question = Question.split(subStrToRemove)[1].substringAfter('</ul>'); 
                }
                else{
                    postExpLiText = Question.split(subStrToRemove)[1].substringBefore('</li>');
                    //if no sub bullets, split the question upto the bullet text end and store the second half in the Question variable for next loop
                    Question = Question.split(subStrToRemove)[1].substringAfter('</li>');
                }
                //Since ':' marks the start of bullets and sub-bullets, check if present
                if(newNonHoverText.indexOf(':') > -1){
                    newNonHoverText = newNonHoverText.substring(0,newNonHoverText.indexOf(':')+1) +'<br/>' + newNonHoverText.substring(newNonHoverText.indexOf(':')+1, newNonHoverText.length());
                }
                //Store the static text, bullet texts and sub bullet texts in a instance of wrapper class
                richTextData.nonHoverText = newNonHoverText; 
                richTextData.HoverText = hoverText; 
                richTextData.liTextBefore = liText; //Text before keyword(if exists) on the particular bullet's text 
                richTextData.liTextAfter = postExpLiText; //Text after keyword(if exists) on the particular bullet's text
                richTextData.ulText = ulText;
                richTextData.liTextStatus = true;
            }
            //if the static text does not have bullets
            else{
                //prepare the question text for hte next loop : it is the text after the keyword
                Question = Question.split(subStrToRemove)[1];
                
                if(nonHoverText.indexOf(':') > -1){
                    nonHoverText = nonHoverText.substring(0,nonHoverText.indexOf(':')+1) +'<br/>' + nonHoverText.substring(nonHoverText.indexOf(':')+1, nonHoverText.length());
                }
                richTextData.nonHoverText = nonHoverText; 
                richTextData.HoverText = hoverText; 
                richTextData.liTextBefore = '';
                richTextData.liTextAfter = '';
                richTextData.ulText = '';
                richTextData.liTextStatus = false;
            }
            richTextDataList.add(richTextData);
        }
        
        if(Question.indexOf(':') > -1){
            //Question = '12345678'+Question;
            Question = Question.substring(0,Question.indexOf(':')+1) +'<br/>' + Question.substring(Question.indexOf(':')+1, Question.length());
        }
        //Final static text of the question after the last occurance of keyword
        richTextData richTextDataLast = new richTextData();
        
        richTextDataLast.nonHoverText = Question; 
        richTextDataLast.HoverText = ''; 
        richTextDataLast.liTextBefore = '';
        richTextDataLast.liTextAfter = '';
        richTextDataLast.ulText = '';
        richTextDataList.add(richTextDataLast);			 
        @TestVisible QuestionData returnData = new QuestionData(wq.Id,richTextDataList);
        return returnData;
    }
    @TestVisible
    class QuestionData{
        @AuraEnabled
        public String questionId{get;set;}
        @AuraEnabled
        List<richTextData> richTextData{get;set;}
        @AuraEnabled
        public List<String> allHoverTextList{get;set;}		
        public QuestionData(String questionIdX,List<richTextData> richTextDataX){
            this.questionId = questionIdX;
            this.richTextData = richTextDataX;
        }
    }
    
    public class richTextData{
        @AuraEnabled
        public Boolean liTextStatus{get; set;}
        @AuraEnabled
        public String nonHoverText{get;set;}
        @AuraEnabled
        public String HoverText{get;set;}
        @AuraEnabled
        public String liTextBefore{get;set;}
        @AuraEnabled
        public String liTextAfter{get;set;}
        @AuraEnabled
        public String ulText{get;set;}
    }
}