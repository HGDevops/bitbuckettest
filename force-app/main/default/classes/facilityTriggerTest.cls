@isTest
public with sharing class facilityTriggerTest {

@isTest static void testCallout() {
    
        // Perform some DML to insert test data
        Facility__c testFac = new Facility__c();
        testFac.Name = 'Test Facility';
        testFac.Address_1__c = '2120 West End Avenue';
        
        // Call Test.startTest before performing callout
        // but after setting test data.
        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMockImpl());
        insert testFac;
        List<Id> lstFacIds = new List<Id>();
        
        lstFacIds.add(testFac.Id);
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = new HttpResponse();
        CARPOL_AC_GooglePopulateTimeZone.CARPOL_AC_CallGoogleGeoAddress(lstFacIds);
        
        
        String actualValue = res.getBody();
        String expectedValue = 'Central Daylight Time';
       // System.assertEquals(actualValue, expectedValue);
        //System.assertEquals(200, res.getStatusCode());
        system.assert(testFac != null);                             
        Test.stopTest();
    }
}