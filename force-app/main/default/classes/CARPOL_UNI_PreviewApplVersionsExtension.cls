/**
* Author : Kishore Kumar
* Created Date : 9/13/2016
* Purpose : This is used to provide Application versions
* Releated Pages:CARPOL_UNI_PreviewApplVersion
*/
public with sharing class CARPOL_UNI_PreviewApplVersionsExtension{
    ID AuthorizationID;
    public Authorizations__c auth{get;set;}
    public list<AC__c> lstlnitem{get;set;}
    public String docLauncherFullURL{get;set;}
    public String docLauncherFullURLCBIDeleted{get;set;}
    public String docLauncherFullURLHistory{get;set;}
    public CARPOL_UNI_PreviewApplVersionsExtension(ApexPages.StandardController stdController){
        AuthorizationID=ApexPages.currentPage().getParameters().get('ID');
        auth = new Authorizations__c(Id=AuthorizationID);
        lstlnitem=[SELECT ID FROM AC__c WHERE Authorization__c=:AuthorizationID limit 1];
        auth = [SELECT ID, Name, Authorization_Type__c , Application_CBI__c FROM Authorizations__c WHERE Id=:AuthorizationID limit 1];
        GenerateDocLauncherURLs();
    }
    
    public void GenerateDocLauncherURLs() { 
        String docLauncherURL = 'Doc Launcher URL';
        String scmPath = 'ScmPath Path';
        String aid = 'aid'; 
        
        final String notificationWorkFlowCBI = 'Notification CBI Application';
        final String notificationWorkFlowNoCBI = 'Notification No CBI Application';
        final String notificationWorkFlowCBIDeleted = 'Notification CBI Deleted Application';
        final String notificationWorkFlowHistory = 'Change History Application Notification PDF';
        
        final String permitWorkFlowCBI = 'Permit CBI Application';
        final String permitWorkFlowNoCBI = 'Permit No CBI Application';
        final String permitWorkFlowCBIDeleted = 'Permit CBI Deleted Application';
        final String permitWorkFlowCBIHistory = 'Change History Application Permit PDF';
        
        SpringCM_Components__mdt[] springCMComonents =  [SELECT MasterLabel, Component_Value__c FROM SpringCM_Components__mdt];
        for(SpringCM_Components__mdt scmComp:springCMComonents){
            if(scmComp.MasterLabel == docLauncherURL){
                docLauncherURL = scmComp.Component_Value__c;
            } 
            else if(scmComp.MasterLabel == scmPath){
                scmPath = scmComp.Component_Value__c;
            }
            else if(scmComp.MasterLabel == aid){
                aid = scmComp.Component_Value__c;
            }
        }
        if(auth.Authorization_Type__c == 'Notification'){
            docLauncherFullURLHistory = docLauncherURL + notificationWorkFlowHistory + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            if(auth.Application_CBI__c == 'Yes'){
                docLauncherFullURL = docLauncherURL + notificationWorkFlowCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
                system.debug('docLauncherFullURLCBIDeleted2:'+docLauncherFullURLCBIDeleted);
                docLauncherFullURLCBIDeleted = docLauncherURL + notificationWorkFlowCBIDeleted + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath; 
            }
            else{
                docLauncherFullURL = docLauncherURL + notificationWorkFlowNoCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
        }
        else
        {
            docLauncherFullURLHistory = docLauncherURL + permitWorkFlowCBIHistory + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            if(auth.Application_CBI__c == 'Yes'){
                docLauncherFullURL = docLauncherURL + permitWorkFlowCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
                system.debug('docLauncherFullURLCBIDeleted4:'+docLauncherFullURLCBIDeleted);
                docLauncherFullURLCBIDeleted = docLauncherURL + permitWorkFlowCBIDeleted + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
            else{
                docLauncherFullURL = docLauncherURL + permitWorkFlowNoCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
        }
    }
}