/**
 * Purpose: Handles the application with BRS line item cloning functionality
 */ 
public with sharing class EFLCloneBRSLineItemHandler implements EFLCloneInterface {
	
    //Cloning Record of line Item and BRS Related Records
    public list<sObject> cloneRecord(sObject record, sObject applicationReference, string ActionType, authorizations__c authRecord)
    { 
        system.debug('EFLCloneBRSLineItemHandler.cloneRecord &&&& ' + ActionType);
        list<sObject> sObjectList  = new list<sObject>(); 
        ac__c originalLineItem = (ac__c)record;
        list<ac__c> clonedLineItemList = new list<ac__c>();
        
        //Creates the External ID for each Line Item
        string EXTERNALID = originalLineItem.ID+EFLGenericUtility.randomString();
        ac__c lineItemReference = new ac__c(ExternalID__c= EXTERNALID);
      
       //Clone Line Item and add to sObjectlist Array 
       //clonedLineItemList.add(EFLCloneUtility.cloneLineItem(originalLineItem,(application__c)applicationReference, EXTERNALID,ActionType, authRecord)); 
        ac__c clonedAC= EFLCloneUtility.cloneLineItem(originalLineItem,(application__c)applicationReference, EXTERNALID,ActionType, authRecord); 
        clonedLineItemList.add(clonedAC);
        sObjectList.addAll(clonedLineItemList); 
       system.debug('clonedLineItemList@@@@' + clonedLineItemList);
        
        string Amendrectypeid = Schema.SObjectType.Amendment_Renewal__c.getRecordTypeInfosByName().get('Amendment').getRecordTypeId();
     	string Renewrectypeid = Schema.SObjectType.Amendment_Renewal__c.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
     	string ARrectypeid;
          list<Amendment_Renewal__c> ARtoUpdate = new list<Amendment_Renewal__c>();
          Amendment_Renewal__c lineitemAR = new Amendment_Renewal__c();
        if (actiontype=='Amendment' || actiontype=='Renewal' ){
            system.debug('Amendment_Renewal__c@@@@' + clonedLineItemList);
            //Add Amendment_Renewal__c juction rcords
            	lineitemAR.Parent_Line_Item__c = clonedAC.Parent_Line_Item__c;
               	lineitemAR.Child_Line_Item__r = lineItemReference;
            	lineitemAR.Parent_Application__c = authrecord.Application__c; 
                lineitemAR.Child_Application__r = (application__c)applicationReference;
            	lineitemAR.Parent_Authorization__c = authRecord.id;
                                  
                if(actiontype == 'Amendment'){
                  ARrectypeid = Amendrectypeid;
                }else{
                    ARrectypeid = Renewrectypeid;
                }      
               lineitemAR.RecordTypeId = ARrectypeid;
               ARtoUpdate.add(lineitemAR);
        }
        system.debug('ARtoUpdate@@@@' + ARtoUpdate);
        if (ARtoUpdate.size()>0){
        	sObjectList.addAll(ARtoUpdate);
        }
        //Clone Link Regulated Article and add to sObjectlist Array
  		list<sObject> clonedLinkRegulatedArticleList =  EFLCloneUtility.cloneLinkRegulatedArticle(originalLineItem,lineItemReference, ActionType);
   		sObjectList.addAll(clonedLinkRegulatedArticleList);
    	
        system.debug('clonedLinkRegulatedArticleList@@@@' + clonedLinkRegulatedArticleList);
       
        //Clone Article Supplier Developer Ravee Racharla 12/13/2018 W-026413
        list<sObject> clonedASDList =  EFLCloneUtility.cloneArticleSupplierDeveloper(originalLineItem,lineItemReference);
        sObjectList.addAll(clonedASDList);
        system.debug('clonedASDList@@@@' + clonedASDList); 
       //Clone Article Supplier Developer Ravee Racharla 12/13/2018 W-026413
       
       //Prepare Previously Reviewed Constructs from both Constructs and Prev. Reviewed Constructs from Original Line Item add to sObjectlist Array 
       list<sObject> prevReviewedConstructList =  EFLCloneUtility.cloneConstruct(originalLineItem,lineItemReference,ActionType);
       sObjectList.addAll(prevReviewedConstructList); 
       
       //Clone Location and add to sObjectlist Array
       list<sObject> clonedLocationList =  EFLCloneUtility.cloneLocationAndDetailObjects(originalLineItem,lineItemReference,ActionType);
       sObjectList.addAll(clonedLocationList); 
       system.debug('clonedLocationList@@@@' + clonedLocationList);
        
        /* W-026413  Ravee Racharla 12/13/2018 
       //Prepare Previously Reviewed SOPs from both SOP and Prev. Reviewed SOPs from Original Line Item add to sObjectlist Array 
       list<sObject> prevReviewedSOPList =  EFLCloneUtility.cloneSOP(originalLineItem,lineItemReference);
       sObjectList.addAll(prevReviewedSOPList); 
        */
        //Ravee Racharla  W-026019  Begin
        if (ActionType=='Amendment'){
             system.debug('EFLCloneBRSLineItemHandler.cloneRecord amendments &&&& ' + ActionType);
              list<sObject>  clonedReviewedSOPList =  EFLCloneUtility.cloneSOP(originalLineItem,lineItemReference);
       		 sObjectList.addAll(clonedReviewedSOPList);
           system.debug('clonedReviewedSOPList@@@@' + clonedReviewedSOPList);
            
            //Applicant Attachments
            list<Applicant_Attachments__c> clonedAppAttachments  =EFLCloneUtility.cloneApplicantAttachments(originalLineItem,lineItemReference);
            list<sObject> clonedAA= new list<sObject>(); 
            clonedAA.addAll(clonedAppAttachments);
            sObjectList.addAll(clonedAA);
             system.debug('clonedAA@@@@' + clonedAA);
            
            //Attachments
            list<sObject> clonedAttachments = EFLCloneUtility.cloneAttachments(clonedAppAttachments);
            sObjectList.addAll(clonedAttachments);
            system.debug('clonedAttachments@@@@' + clonedAttachments);
        }
         //Ravee Racharla  W-026019  End
     return sObjectList; 
    }

}