public with sharing class EFLAddAccount {
    /* @Purpose: Add Sub Accounts under a parent account.               
@Author: Ravee Racharla  @Date:4/5/2019	
*/
    
    public string strAccountName{get;set;} 
    public account relatedAccount{get; set;}
    public string strErrorDesc{get; set;}
    @TestVisible
    string paramAccountId {get; set;}
    
    public EFLAddAccount(){
        
    }
    
    // This controller is used as extension controller for Editing accounts. EFLEditAccount.vfp uses custom
    // controller, PortalController. Hence need to have a contructor for that.  
    public EFLAddAccount(PortalController pc){
        paramAccountId = apexpages.currentpage().getparameters().get('accId');
        Account acc1 = [select Id, Name from Account where Id = :paramAccountId ];
        if(acc1 != null){
            strAccountName = acc1.Name;
        }
    }
    /* Contacts list - begin code*/
    public class ConWrapper {
        public Contact currentCon { get;set;}
        public Boolean active { get; set; }
        public ConWrapper(Contact c, Boolean a) {
            this.currentCon = c;
            this.active = a;
        }
    } 
    
    public map<id, Contact> myContacts {
        get{
            //return new map<id, Contact> ([SELECT ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, Email FROM Contact WHERE AccountId =: EFLUserUtility.contactDetails.AccountId ORDER BY CreatedDate DESC]);
            //return new map<id, Contact> (EFLContactRepository.selectActiveUserContactsByCurrentUserDirectAccount());
            try
        {
            EFLEnforceAccessUtility.checkObjectReadAccess('Contact');
            //system.debug('EFLAddAccount.paramAccountId' + paramAccountId);
            //RR 4/18 begin - changing it to Utility class do overcome sharing settings issue
            return new map<id, Contact> (EFLContactUtility.selectActiveUserContactsByCurrentAccount(paramAccountId));
            /*return new map<id, Contact> ([SELECT ID, FirstName, LastName, Account.Name, Title, Phone, HomePhone, MobilePhone, 
                    Email, (select Id, AccountId, ContactId, isDirect from AccountContactRelations ORDER BY CreatedDate ASC), 
                    (select Id, isActive, contactId from users where isActive = true ORDER BY CreatedDate DESC limit 1) FROM Contact 
                    WHERE Id in (select contactId from AccountContactRelation 
                                 where AccountId = :paramAccountId) 
                    AND Id in (select contactId from user where isActive = true)
                   ]);*/
            //RR 4/18 end
        }
        catch(exception e){
            EFLErrorLog.createErrorLog('EFLContactRepository.selectActiveUserContactsByCurrentUserDirectAccount()',e);                
        } 
        return null;
        }
        set;
    }
	
public List<ConWrapper> getlstConWrapperAccount() {
        List <ConWrapper> lstConWrapperAccount = new List <ConWrapper> ();
        //if (EFLUserUtility.contactDetails.Account_Admin__c) {
        //if (EFLUserUtility.isAccountAdmin()) {
           for (Contact c: myContacts.values()) 
           {
            if (c.users!=null) 
            {
             ConWrapper conWrapInst;
             if (c.Id != EFLUserUtility.contactDetails.id) {
                 conWrapInst = new ConWrapper(c, c.users[0].isActive);
             }else{
                 conWrapInst = new ConWrapper(c, false);
             }
             lstConWrapperAccount.add(conWrapInst);
            }
           }
        /*} else {
            ConWrapper conWrapInst = new ConWrapper(EFLUserUtility.contactDetails, false);
            lstConWrapper.add(conWrapInst);
        }*/
        return lstConWrapperAccount;
    }    
    /* Contacts list - end code*/
    
    //Add User to Admin Account
    public void editAccount()
    {
        strErrorDesc = null; 
        string paramAccountId = EFLGenericUtility.sanitizeString(apexpages.currentpage().getparameters().get('accId'));
        
        if (isAccountValid()) {
            try
            {  
                //
                // Update Account Record if there are no dupe
                //- Update record ; Fields : Account Name
                account ac = new account();
                ac.Id = paramAccountId;
                ac.name = strAccountName; 
                update ac; 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, EFLGenericUtility.getMessage('EFLSaveAccountSuccessMessage')));
            } 
            catch(system.exception ex)
            {
                EFLErrorLog.createErrorLog('EFLAddAccount.editAccount()',ex);
                //system.debug('*** EFLAddAccount.editAccount()' + ex);
            }  
        }
    }
    //Add User to Admin Account
    public PageReference addAccount()
    {
        strErrorDesc = null; 
        if (isAccountValid()) {
            try
            {  
                //
                //Create Account Record if there are no dupe
                //- Insert a new record ; Fields : Account Name; Parent Account 
                account ac = new account();
                ac.name = strAccountName; 
                Id parentId = getParentAccountId();
                ac.ParentId = parentId;
                ac.Type = 'Business'; //KA:W-036350
                insert ac; 
                
                //RR 4/17 begin
                List<Contact> currentUserContact = 
                    EFLContactRepository.selectContactsUsersAccountRelationByUserNames(
                        new List<string>{UserInfo.getUserName()});
                //system.debug('UserInfo.getUserName() '+ UserInfo.getUserName());
                
                if(currentUserContact != null && currentUserContact.size()>0){
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId = ac.Id;
                    acr.ContactId = currentUserContact[0].Id;
                    insert acr;
                    //system.debug('Insert acr ' + acr);
                    PageReference editAccount = new PageReference('/EFLEditAccount?accId=' + ac.ID );
                return editAccount;
                }
                else{
                    //system.debug('currentUserContact is null');
                    ApexPages.addmessage(
                        new ApexPages.message(ApexPages.severity.WARNING, 
                                              'Account created but could not add current user as a contact'));
                    return null;
                }
                    
                //RR 4/17 end
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, EFLGenericUtility.getMessage('EFLSaveAccountSuccessMessage')));
                //PageReference editAccount = new PageReference('/EFLEditAccount?accId=' + ac.ID );
                //return editAccount;
            } 
            catch(system.exception ex)
            {
                //system.debug('*** ex' +  ex);
                EFLErrorLog.createErrorLog('EFLAddAccount.addAccount()',ex);
            }  
        }
        return null;
    }
    
    public pagereference cancel()
    {
        pagereference contactPage = page.EFLCommunity_Accounts;
        return contactPage;
    }
    
    private boolean isAccountValid(){
        if (strAccountName == null || String.isEmpty(strAccountName)){
            strErrorDesc = EFLGenericUtility.getMessage('EFLRequiredField');
            return false;
        }
        else{
            //check for dupes
            list<Account> lstAccount = new list<Account>([select id, name from Account where 
                                                          name =:strAccountName]);
            if (lstAccount!= null && lstAccount.size()> 0 ){
                //throw error messgage that this is alreay existing
                strErrorDesc = EFLGenericUtility.getMessage('EFLAccountExists'); 
                return false;
            }  
            return true;
        }
    }
    
    // Returns parent account id  of current user
    // returns null if no parent exists
    private Id getParentAccountId(){
        Contact userContact = EFLUserUtility.contactDetails;
        if(userContact != null){
            return userContact.AccountId;
        }
        else
            return null;
        //Id parentId = [SELECT Id, Name, ParentId FROM Account where Id = :childAccountId LIMIT 1].ParentId;   
    }
}