/***********************************************************************************
* Date      ||      Author      ||      Description
* *********************************************************************************
* 4/2/17    ||      Sreedhar    ||      Label generator for all the pathways
**********************************************************************************/
global with sharing class EFLAttachandSendLabels {
    Webservice static string generateShippingLabelsPDF(id authid, boolean isSendEmail, String NextPage, String AttchName, string btntype){
        try{
            Authorizations__c objauth = [select id,Name,BRS_Send_Labels__c,AC_Applicant_Email__c,Authorized_User__r.Name,Application__c,Application__r.name, RecordType.Name 
                                         from Authorizations__c where id=:authid];
            string appemail = objauth.AC_Applicant_Email__c;
            objauth.BRS_Send_Labels__c = true;
            update objauth;
            attachment attlab = new attachment();
            Blob attbody = null;
            PageReference PDFPage = new PageReference('/apex/'+NextPage+'?id='+authid+'&btntype='+btntype);
            
            if(!test.isRunningTest()) {
                attbody = PDFPage.getContentAsPdf();
            } 
            else {
                attbody = Blob.valueOf('Some Text');
            } 
            attlab.Body = attbody;
            attlab.Name = AttchName+'.pdf';
            attlab.ParentId = authid;
            insert attlab;
            
            if(isSendEmail){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
                String[] toaddress = new string[]{objauth.AC_Applicant_Email__c};
                email.setSubject('USDA APHIS eFile - Shipping Labels for Application '+objauth.Application__r.name);
                email.setToAddresses(toaddress);
                
                //Email Activity History
                email.setSaveAsActivity(true);
                email.setTargetObjectId(objauth.Applicant__c);
                email.setWhatId(objauth.id);
                    
                String appEmailBody = 'Dear ' + objauth.Authorized_User__r.Name;
                List<ConnectApi.Community> communities = ConnectApi.Communities.getCommunities().communities;
                 
                String hostVal = '';
                String URLL;
                
                for(ConnectApi.Community community:communities){
                     
                    if(community.name == Label.Aphis_Community)
                        hostVal = community.siteUrl; 
                }
                
                String urlVal = '/portal_application_detail?id=';
                
                URLL =  hostVal + urlVal;
              
                //appEmailBody += ',<br/>br/>br/>Below you will find a link for the shipping labels you have requested for Authorization' + objauth.name;
                appEmailBody += ',<br/><br/><br/>Attached is the shipping labels you have requested for ' + objauth.name;
                 
                //appEmailBody += '<br/><br/>' + URLL + objauth.Id;
                appEmailBody += '<br/><br/>Please refer to the Label Guidance listed under Permit Conditions. <br/><br/>';
                
                appEmailBody += 'Thank you,<br/>'+objauth.RecordType.Name+' Programs,<br/> USDA, Animal and Plant Health Inspection Service<br/> 4700 River Road <br/> Riverdale, MD 20737  <br/>United States Department of Agriculture<br/>Contact Number: 301-851-3740<br/>';
                
                email.setHtmlBody(appEmailBody);
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(AttchName+'.pdf');
                efa.setBody(attbody);
                
                List<Messaging.EmailFileAttachment> efaList = new List<Messaging.EmailFileAttachment>();
                efaList.add(efa);
                email.setFileAttachments(efaList);

                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email }); 
            }
            
             
            List<Label__c> labelList = [select id,Name,QRcode_Barcode__c,Label_Sent__c,Notification_Issue_Date__c,Notification_Expiration_Date__c,Status__c,createddate,LastModifiedDate from Label__c where Authorization__c =:authid and Label_Sent__c =: false];
            if(labelList.size()>0){
                for(Label__c lbl : labelList){
                    lbl.Label_Sent__c = true;
                 }
                update labelList; 
            }
 
            return 'success';
            
        }
        catch(Exception e)
        {
            return string.valueof(e);
        }
        return null;
    } 
}