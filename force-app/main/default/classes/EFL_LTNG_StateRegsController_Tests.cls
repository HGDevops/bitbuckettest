@isTest
public class EFL_LTNG_StateRegsController_Tests {
	// Rajesh Potla - W035863
    @TestSetup
    static void setupData(){
        CARPOL_BRS_TestDataManager dm = new CARPOL_BRS_TestDataManager();
        dm.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c app = dm.newapplication();
        Authorizations__c auth = dm.newAuth(app.id);
        Regulation__c reg = dm.newRegulation('Standard','');
        dm.newAuthorizationJunction(auth.Id, reg.Id);
    }
    
    // Adding to get coverage on constructor.  no assertions made as this is 
    // part of test cleanup effort.
    @isTest
    static void runConstructorTest(){
        Authorizations__c auth = [SELECT Id FROM Authorizations__c];
        ApexPages.StandardController sc = new ApexPages.StandardController(auth);
        PageReference pr = Page.EFL_StateRegulations;
        pr.getParameters().put('id',auth.Id);
        test.setCurrentPage(pr);
        EFL_LTNG_StateRegulationsController controller = new EFL_LTNG_StateRegulationsController(sc);
    }
    
    @isTest 
    static void runLogicTest(){
        Authorizations__c auth = [SELECT Id FROM Authorizations__c];
        ApexPages.StandardController sc = new ApexPages.StandardController(auth);
        PageReference pr = Page.EFL_StateRegulations;
        pr.getParameters().put('id',auth.Id);
        test.setCurrentPage(pr);
        EFL_LTNG_StateRegulationsController controller = new EFL_LTNG_StateRegulationsController(sc);
        
        controller.regulationsList = new list<EFL_LTNG_StateRegulationsController.wrapperRegulation>();
        Regulation__c r = [SELECT Id, Title__c, Regulation_Description__c FROM Regulation__c LIMIT 1];
        EFL_LTNG_StateRegulationsController.wrapperRegulation w = new EFL_LTNG_StateRegulationsController.wrapperRegulation(r);
        w.selected = true;
        controller.regulationsList.add(w);
        
        controller.addSelected();
        controller.selectedRegulations[0].selected = false;
        
        controller.removeSelected();
        controller.saveEditedRequirements();
        
        controller.addMoreRequirements();
    }
}