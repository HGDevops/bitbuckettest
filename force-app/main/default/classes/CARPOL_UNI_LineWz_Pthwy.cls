/* Class        : CARPOL_UNI_LineWz_Pthwy.cls
 * Description  : US 1595 This controller is used to display the program line item pathways in the line wizard so user can choose other pathway
 * @author : Vijay Vellaturi
 */
public with sharing class CARPOL_UNI_LineWz_Pthwy{
    public String selectedPathwayID    { get; set; }
    public String strAppId             { get; set; }
    public String strTitle             { get; set; }     
    public map<string,string> pageParams{ get; set; }
    public map<string,string> LineItems { get; set; }
    public Boolean pathwayItems        { get; set; }
    List<Program_Line_Item_Pathway__c> progLineItemPathwayRecords = new List<Program_Line_Item_Pathway__c>();
    
    public CARPOL_UNI_LineWz_Pthwy(){
        strTitle = 'What do you want to Import...';
        pathwayItems = true;
        LineItems = new map<string,string>();
        pageParams = ApexPages.currentPage().getParameters();
        if(pageParams.containsKey('AppId') && null != pageParams.get('AppId') && '' != pageParams.get('AppId')){
            strAppId = pageParams.get('AppId'); 
            if(pageParams.containsKey('ipwy') && null != pageParams.get('ipwy') && '' != pageParams.get('ipwy')){
                getPathwayRecord(pageParams.get('ipwy'));
            }else{
                getPathwayRecord(); 
            }     
        }
       
    }
    
    private void getPathwayRecord(){
        progLineItemPathwayRecords = [SELECT ID, Name, Parent_Id__c, Program__c FROM Program_Line_Item_Pathway__c WHERE Parent_Id__r.id = Null order by name];
    }
    
    private List<Program_Line_Item_Pathway__c> getPathwayRecord(String Parent){
        progLineItemPathwayRecords = [SELECT ID, Name, Parent_Id__c,Parent_Id__r.Question__c, Program__c FROM Program_Line_Item_Pathway__c WHERE Parent_Id__r.Name =:Parent OR Parent_Id__r.id =:Parent order by name];
        for(Program_Line_Item_Pathway__c objPathWay : progLineItemPathwayRecords){
            if(objPathWay.Parent_Id__r.Question__c!=null && objPathWay.Parent_Id__r.Question__c!=''){
                strTitle = objPathWay.Parent_Id__r.Question__c;
            }
        }  
        return progLineItemPathwayRecords;
    }
    
    public List<SelectOption> getprogLineItemPathwayItems() {   
        List<SelectOption> options = new List<SelectOption>();    
        if(progLineItemPathwayRecords.size() > 0){
            for(Program_Line_Item_Pathway__c plip : progLineItemPathwayRecords){
                options.add(new SelectOption(plip.Id,plip.Name)); 
                system.debug('####plip = '+plip); 
            }
        }
        return options;
    }
    
    public PageReference getNextChild(){   
  
        PageReference redirect;
        List<Program_Line_Item_Pathway__c> checkForChildLineItem = getPathwayRecord(selectedPathwayID);
        if(checkForChildLineItem != null &&  checkForChildLineItem.size()>0){
               System.debug('&&&&&  progLineItemPathwayRecords = '+progLineItemPathwayRecords );
            redirect = new PageReference('/apex/CARPOL_UNI_LineWz_Pthwy?ipwy='+selectedPathwayID+'&appid='+strAppId); 
        }else{

            redirect = new PageReference('/apex/CARPOL_UniversalLineItem?appid='+strAppId+'&strPathway='+selectedPathwayID+'&Movement_Type__c=import');
        }
        redirect.setRedirect(true);         
        return redirect;
    }



}