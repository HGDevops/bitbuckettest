/**********************************************************************  
* @Class Name: EFLCloneApplicationUtility
* @Author: AFS
* @Purpose : This utility class handles the cloning part of any application.
*            a.Clones all 4 program(AC/BRS/PPQ/VS) related line items from an application.
*            b.It skip the validation across the line items and allow them to validate the cloned line items and make corrections accordingly.
*            c.For BRS it clones all related records along with attachments and in specific to Constructs & SOP's it adds all constructs/SOP's to Previously Reviewed Constructs/SOP's. 
* @Related Classes: EFLCloneApplicationController
* Teh Liu 5/1/2018: Fixed SOP/Construct duplication in method CloneApplication
*                   Added method cloneAddDetails for Additional Other Details clonin
************************************************************************/
// ToDo:
// a.Clone line item related records like Additional Permitties,Additional Shippers, Transit Locales, 
// b.Make sure we have the ability to clone above related records for multiple line items
// c.for BRS if we have multiple BRS line items in one application, we should be able to clone both line items and its related list
public with sharing class EFLCloneApplicationUtility{
  string lineprogram;
 /**
 * @Return Parameter: Provides cloning related error messages 
 * */
    public Application__c CloneApplication(ID applicationId){
        // NUll or blank value or white spaces check
        if(applicationID!=null){
            
            list<AC__C> newLineItemsToInsertList=new  list<AC__c>();
            list<AC__c> processLineItemsList=new list<AC__c>();
            list<id> oldLineItemIDList=new  list<id>();
            Application__c app=new Application__c();
              
/** 
* Clone # Line Items, Applicant Attachments,Attachments
**/
            try{
                List<Application__c> applst=[SELECT Applicant_Name__c,RecordtypeId,
                                                    Applicant_Email__c,Applicant_Phone__c,
                                                    Applicant_Fax__c,Organization__c,
                                                    Application_Status__c,Applicant_Address__c,US_Address__c 
                                               FROM Application__c 
                                               WHERE ID=:applicationID];
               //Creating Application with application status as 'Open' and application type as New for same applicant
                app.Applicant_Name__c=applst[0].Applicant_Name__c;
                app.Application_Status__c='open';
                app.RecordTypeId=applst[0].RecordTypeId;
                app.Application_Type__c='New';
                Insert app;
                
                //Get all attachments from application 
                //ToDo: Do we need to do this as we may not need to copy over any attachments related to application 
                //      as currently all attachments are attached to line Item attachment or applicant attachment object.
                List<Attachment> attachmentList=[SELECT Id,Name,
                                                        ContentType,ParentId,
                                                        Description,Body 
                                                  FROM Attachment 
                                                 WHERE ParentID=:applst[0].Id];
               if(attachmentList!=null && !attachmentList.isEmpty()){
                    list<Attachment> newAttList=new  list<Attachment>();
                    for(Attachment att:attachmentList){
                        Attachment newAtt=new  Attachment();
                        newAtt=att.clone(false,true);
                        newatt.ParentId=app.id;
                        newAttList.add(newAtt);
                    }
                    if(newAttList.size()>0){
                    Insert newAttList;}
                }
                //Clone all Line Items of the application
                DescribeSObjectResult lineItemDescribe=AC__c.SObjectType.getDescribe();
                List<string> fields=new  List<string>(lineItemDescribe.fields.getMap().keySet());
                //Dynamic Query to obtain all line item fields
                string soql='select '+string.join(fields,', ')+',Program_Line_Item_Pathway__r.Program__r.Name, Program_Line_Item_Pathway__r.Show_Required_Documents_Button__c,State_Territory_of_Destination__r.Name,Country_Of_Origin__r.Name, Scientific_Name__r.Name,Program_Line_Item_Pathway__r.All_Parent_Pathways__c,Country_of_Export__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Ra_Scientific_Name__r.name,Scientific_Name__r.Scientific_Name__c, Component__r.Name ,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Hand_carrier_Last_Name__r.Name, Hand_carrier_Mailing_CountryLU__r.Name, Importer_Last_Name__r.Name, Importer_Mailing_CountryLU__r.Name, Application_Number__r.Application_Status__c, County_Region__r.Name, State_Province_of_Origin__r.Name,(Select Id, Account__c, Contact__c, Type_Of_Address__c, Last_Name__c, Type_of_Associated_Contact__c, Line_Item__c From Additional_Address_Other_Details__r) from AC__c where Application_Number__c = \''+applicationID+'\'';
                //obtain all the line item records to process
                processLineItemsList=Database.query(soql);
                //Process through each line item and clone and change each line item status and proposed date values and prepare the new line item list to Insert
                for(AC__c oldline:processLineItemsList){
                    AC__c lineitem=oldline.clone(false,true,false,false);
                    lineitem.Application_Number__c=app.id;
                    lineitem.Parent_Line_Item__c=oldline.id;
                    lineitem.Authorization__c=null;
                    if (lineItem.Program_Line_Item_Pathway__r.Show_Required_Documents_Button__c == true) {
                        lineitem.Status__c='Saved';
                    } else {
                        lineitem.Status__c='Draft';
                    }
                    lineitem.Proposed_Start_Date__c=System.today();
                    lineitem.Proposed_end_date__c=lineitem.Proposed_Start_Date__c.addYears(1);
                    lineitem.skip_validation__c=true;
                    lineitem.Cloned_Line_Item__c=true;
                    lineitem.Locked__c='No';
                    lineitem.EFLPermittedMaterialDescription__c = null;
                    newLineItemsToInsertList.add(lineitem);
                    oldLineItemIDList.add(oldline.id);
                }
                if(newLineItemsToInsertList!=null && !newLineItemsToInsertList.isEmpty()){
                     Insert newLineItemsToInsertList;
                 }
            }
            catch(Exception e){
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'));
            }
            map<id,id> parentIdMap=new  map<Id,Id>();
            //obtain an map which has both old and new line item id's
            if(newLineItemsToInsertList.size()>0){
                for(AC__c newline:newLineItemsToInsertList){
                    parentIdMap.put(newline.Parent_Line_Item__c,newline.Id);}
            }
/**
*line items record Attachments
**/
            if(parentIdMap.size()>0){
                Clonerecordattachments(parentIdMap);
                cloneAddDetails(parentIdMap, processLineItemsList);
            }
/**
*Applicant Attachments along with Attachments 
**/        
            list<Applicant_Attachments__c> AAtoUpdate=new  list<Applicant_Attachments__c>();
            map<id,id> AAIDMap=new  map<Id,Id>();
            if(newLineItemsToInsertList.size()>0&&oldLineItemIDList.size()>0){
                List<Applicant_Attachments__c> AAlist=[SELECT Id,RecordTypeId,Applicant_Instructions__c,File_Name__c,Attachment_Type__c,Confinement_Protocols__c,Destination_Release_Description__c,Document_Types__c,File_Description__c,Final_Disposition_Description__c,Final_Disposition_Method__c,Internal_Only__c,NOTE_Notification__c,NOTE__c,Production_Design__c,Parent_Applicant_Attachment__c,Share_with_Applicant__c,Animal_Care_AC__c,Share_with_State__c,Status__c,Standard_Operating_Procedure__c,Tags__c,Total_Files__c FROM Applicant_Attachments__c WHERE Animal_Care_AC__c in:oldLineItemIDList and RecordType.Name='Uploaded Attachment'];
                if(AAlist.size()>0){
                    for(Applicant_Attachments__c AA:AAlist){
                        Applicant_Attachments__c newAA=new  Applicant_Attachments__c();
                        newAA=AA.clone(false,true);
                        newAA.Animal_Care_AC__c=parentIdMap.get(AA.Animal_Care_AC__c);
                        newAA.Parent_Applicant_Attachment__c=AA.id;
                        AAtoUpdate.add(newAA);
                    }
                }
                if(AAtoUpdate.size()>0){
                    Insert AAtoUpdate;
                    for(Applicant_Attachments__c AA:AAtoUpdate)
                       {AAIDMap.put(AA.Parent_Applicant_Attachment__c,AA.Id);}
                    if(AAIDMap.size()>0)
                      {Clonerecordattachments(AAIDMap);}
                }
            }
/**
 * BRS Specific Related List Records ( SOP's, Constructs , Applicant Attachments, Link Regulated Article) 
 * Constructs are added as Previously reviewed constructs
**/ 
            if(newLineItemsToInsertList.size()>0&&oldLineItemIDList.size()>0){
                //obtain program name to ensure we run this logic only for BRS related line items
                lineprogram=[SELECT id,Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.Program__c,Program_Line_Item_Pathway__r.Program__r.name FROM ac__c WHERE id=:newLineItemsToInsertList[0].id limit 1].Program_Line_Item_Pathway__r.Program__r.name;
                if(lineprogram=='BRS' || Test.isRunningTest()){
                    List<Construct__c> conslst=[SELECT Id,Corrections_Required__c,Link_Regulated_Article__c,
                                                       BRS_Applicant_Response__c,BRS_Applicant_Response_Email__c,
                                                       Construct_s__c,Contact__c,Default_Applicant_Eauth_Id__c,
                                                       Identifying_Line_s__c,Line_Events__c,Mode_of_Transformation__c,
                                                       Needs_Applicant_Attention__c,Line_Item__c,Reason_Exipired_or_Inactive__c,
                                                       Reason_for_disapproval__c,Status__c 
                                                  FROM Construct__c 
                                                 WHERE Line_Item__c in:oldLineItemIDList];
                    List<Applicant_Attachments__c> aalst=[SELECT Id,RecordTypeId,Applicant_Instructions__c,
                                                                 File_Name__c,Attachment_Type__c,Confinement_Protocols__c,
                                                                 Destination_Release_Description__c,Document_Types__c,
                                                                 File_Description__c,Final_Disposition_Description__c,
                                                                 Final_Disposition_Method__c,Internal_Only__c,
                                                                 NOTE_Notification__c,NOTE__c,Production_Design__c,
                                                                 Share_with_Applicant__c,Animal_Care_AC__c,
                                                                 Share_with_State__c,Status__c,Standard_Operating_Procedure__c,
                                                                 Tags__c,Total_Files__c 
                                                            FROM Applicant_Attachments__c 
                                                           WHERE Animal_Care_AC__c in:oldLineItemIDList and RecordType.Name ='Standard Operating Procedures'];
                  List<Location__c> loclst=[SELECT ID,Name,Contact_Name1__c, 
                                                   Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                                                   Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                                                   Day_Phone_2__c,Zip__c,Other_Material_Types__c,
                                                   Proposed_Planting__c,Number_of_Acres_Text__c,
                                                   Primary_Contact_Last_Name__c,Contact_Address1__c,
                                                   Primary_State_Text__c,Primary_Country_Text__c,
                                                   Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                                                   Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                                                   Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                                                   Secondary_Country_Text__c,Secondary_Contact_County__c,
                                                   Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                                                   Line_Item__c,Description__c,Applicant_Instructions__c,
                                                   RecordType.Name,Country__r.Name,Level_2_Region__r.name,
                                                   Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                                                   Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                                                   GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                                                   GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                                                   GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                                                   GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                                                   Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                                                   GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                                                   If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,
                                                   Material_Type1__c, Material_Type2__c, Material_Type3__c, Material_Type4__c, Material_Type5__c, Material_Type6__c, 
                                                   Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                                                   Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                                                   Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c 
                                              FROM Location__c 
                                             WHERE Line_Item__c in:oldLineItemIDList];
                    List<Link_Regulated_Articles__c> lralst=[SELECT Id,Status__c,Status_Graphical__c,Name,Line_Item__c,
                                                                    Regulated_Article__r.name,Regulated_Article__r.Category_Group_Ref__r.name,
                                                                    Regulated_Article__r.Additional_Scientific_Name__c,
                                                                    Regulated_Article__r.Additional_Common_Name__c,Common_Name__c,
                                                                    Scientific_Name__c,Cultivar_and_or_Breeding_Line__c,
                                                                    Corrections_Required__c,Application__c 
                                                               FROM Link_Regulated_Articles__c 
                                                              WHERE Line_Item__c in:oldLineItemIDList];
                    List<Construct_Application_Junction__c> cajlst=[SELECT Id,Name,Line_Item__c,Construct__c,Construct__r.Construct_s__c, Status__c,
                                                                           Status_Graphical__c,Construct_Status__c, Design_Protocol_Record_SOP__c,
                                                                           Status_new_Graphical__c, Applicant_Instructions__c,
                                                                           Construct__r.Mode_of_Transformation__c, 
                                                                           Construct__r.Link_Regulated_Article__c 
                                                                      FROM Construct_Application_Junction__c
                                                                     WHERE Line_Item__c in:oldLineItemIDList];
                   
                    List<Construct_Application_Junction__c> precon=new  List<Construct_Application_Junction__c>();
                    Construct_Application_Junction__c CurCon=new  Construct_Application_Junction__c();
                    List<Construct__c> conlst2=new  List<Construct__c>();
                    list<id> soplstids=new  list<id>();
                   
                    //Clone Construct and add it to previously reviewed constructs
                     if(conslst!=null && !conslst.isEmpty()){    
                        for(Construct__c precons:conslst){
                            Construct_Application_Junction__c newcon=new  Construct_Application_Junction__c();
                            newcon.Line_Item__c=parentIdMap.get(precons.Line_Item__c);
                            newcon.Construct__c=precons.id;
                            precon.add(newcon);
                        }
                    }
                    //Clone SOP and add it to previously reviewed SOP's
                    if(aalst!=null && !aalst.isEmpty()){ 
                        for(Applicant_Attachments__c presop:aalst){
                            Construct_Application_Junction__c newsop=new  Construct_Application_Junction__c();
                            newsop.Line_Item__c=parentIdMap.get(presop.Animal_Care_AC__c);
                            newsop.Design_Protocol_Record_SOP__c=presop.id;
                            precon.add(newsop);
                        }
                    }
                    //Clone previously reviewed constructs
                      if(cajlst!=null && !cajlst.isEmpty()){ 
                        for(integer i=0;i<cajlst.size();i++){
                            Construct_Application_Junction__c caj=new  Construct_Application_Junction__c();
                            caj=cajlst[i].clone(false,true);
                            caj.Line_Item__c=parentIdMap.get(cajlst[i].Line_Item__c);                            
                            precon.add(caj);
                        }
                    }
                    //Insert Constructs, previously reviewed constructs & SOP's at once
                    Insert precon;
                    
                    //Clone line item related locations 
                    List<Location__c> locToBeInsertedList=new  List<Location__c>();
                    if(loclst!=null && !loclst.isEmpty()){     
                         for(integer i=0;i<loclst.size();i++){
                            Location__c loc=loclst[i].clone(false,true);
                            loc.Line_Item__c=parentIdMap.get(loclst[i].Line_Item__c);
                            loc.Application__c=app.id;
                            locToBeInsertedList.add(loc);
                        }
                    }
                    Insert locToBeInsertedList;
                    
                    //clone link regulated articles 
                    List<Link_Regulated_Articles__c> LinkRAToBeInsertedList=new  List<Link_Regulated_Articles__c>();
                     if(lralst!=null && !lralst.isEmpty()){     
                        for(integer i=0;i<lralst.size();i++){
                            Link_Regulated_Articles__c lr=lralst[i].clone(false,true);
                            lr.Line_Item__c=parentIdMap.get(lralst[i].Line_Item__c);
                            lr.Application__c=app.id;
                            LinkRAToBeInsertedList.add(lr);
                        }
                    }
                    Insert LinkRAToBeInsertedList;
                }
            }
            return app;
        }
        else{
            return null;
        }
    }
/**
* @purpose: Method to clone any record attachments 
* @Input Attributes: Parent map of any record with both old and new record Id's
**/  
    public void Clonerecordattachments(map<id,id> parentmap){
        List<Attachment> attachmentList=[SELECT Id,Name,ContentType,ParentId,Description,Body FROM Attachment WHERE ParentID=:parentmap.keySet()];
        if(attachmentList.size()>0){
            list<Attachment> newAttList=new  list<Attachment>();
            for(Attachment att:attachmentList){
                Attachment newAtt=new  Attachment();
                newAtt=att.clone(false,true);
                newatt.ParentId=parentmap.get(att.ParentId);
                newAttList.add(newAtt);
            }
            if(newAttList.size()>0&&!test.isRunningTest()){
               Insert newAttList;}
        }
    }
    /**
    * @purpose: Method to clone Additional Address Other Details to lineItem
    * @Input Attributes: Parent map of any record with both old and new record Id's and List of LineItem
    **/  
    private void cloneAddDetails(map<id,id> parentmap, List<AC__c> lineItemList){
        List<Additional_Contact_Detail__c> addItemList = new List<Additional_Contact_Detail__c>();
        
        for(AC__c lineItem: lineItemList) {
            if(lineItem.Additional_Address_Other_Details__r != null) {
                for(Additional_Contact_Detail__c aaDetails: lineItem.Additional_Address_Other_Details__r){
                    if (parentmap.containsKey(aaDetails.Line_Item__c)) {
                        
                        Additional_Contact_Detail__c aaobj = aaDetails.clone(false,true,false,false);
                        // associate to new/cloned lineItem id
                        aaobj.Line_Item__c = parentmap.get(aaDetails.Line_Item__c);
                        addItemList.add(aaobj);                     
                    }
                }
            }
        }
        if(addItemList.size()>0) {

            insert addItemList;
            
        }
        
    }    
}