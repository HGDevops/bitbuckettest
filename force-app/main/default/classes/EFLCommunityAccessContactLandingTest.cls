@IsTest
public class EFLCommunityAccessContactLandingTest {
    //Primarily to cover portalcontroller class which is used as controller for 
    //Community Landing (Dashboard) page, Contact and Associated Contact
    //Constructs Dashboard
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
     @isTest
    static void CommunityAccessContactTest() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
	    Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        
        Applicant_Contact__c associatedContact = new Applicant_Contact__c();
        associatedContact = testData.newappcontact();
        
        associatedContact.Account__c = newAccount.id;
        update associatedContact;
        
        Account newAccount1 = new Account();
        newAccount1 = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        Contact newContact1 = new Contact();
        newContact1 = testData.newContact();
        newContact1.accountid = newAccount1.id;
        update newContact1;
        
        Contact newContact2 = new Contact();
        newContact2 = testData.newContact();
        newContact2.accountid = newAccount1.id;
        update newContact2;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant Plus' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        user usershare1 = new User();
        usershare1.Username ='orgadmin12202018@test.com';
        usershare1.LastName = 'orgadmin12202018';
        usershare1.Email = 'orgadmin12202018@test.com';
        usershare1.alias = 'org20201';
        usershare1.TimeZoneSidKey = 'America/New_York';
        usershare1.LocaleSidKey = 'en_US';
        usershare1.EmailEncodingKey = 'ISO-8859-1';
        usershare1.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare1.LanguageLocaleKey = 'en_US';
        usershare1.ContactId = newContact1.id;
        insert usershare1;  
        
        User newUser = EFLUserTestDataFactory.getUser('eFile Applicant Plus'); 
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        Account realORG = new Account();
        realORG = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        newContact.accountid = realORG.id;
        update newContact;
        newContact1.accountid = realORG.id;
        update newContact1;
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        /*CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLChangeHistoryTrigger'; 
        chDt.Disable__c = true; 
        insert chDt; */
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
        
        CARPOL_URLs__c Urls = new CARPOL_URLs__c(name='Landing_Page', URL__c='/apex/CARPOL_CommunityLandingPage');
        insert Urls;
        
        test.startTest();
        
        system.runAs(usershare1)
        {
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact1.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount1.id);
            auth = testData.newAuth(app.Id);
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            /*CARPOL_UNI_DisableTrigger__c LRAdt = new CARPOL_UNI_DisableTrigger__c(); 
            LRAdt.Name = 'CARPOL_BRS_Link_RegulatedTrigger'; 
            LRAdt.Disable__c = false; 
            insert LRAdt;*/
            LRA = new Link_Regulated_Articles__c();
            LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Submitted');   
            
            /*CARPOL_UNI_DisableTrigger__c ConDt = new CARPOL_UNI_DisableTrigger__c(); 
            ConDt.Name = 'CARPOL_BRS_ConstructTrigger'; 
            ConDt.Disable__c = true; 
            insert ConDt;*/
            con = new Construct__c();
            con = testdata.newconstructByLIIdLRAId(LineItem.id, RA.Id, 'Waiting on Customer');
        }
    }
    
    @isTest
    static void CommunityAccessContactTest1() {
        
        testData.insertcustomsettingsWithBRSTriggerDisabled();
	    Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        
        Applicant_Contact__c associatedContact = new Applicant_Contact__c();
        associatedContact = testData.newappcontact();
        
        associatedContact.Account__c = newAccount.id;
        update associatedContact;
        
        Account newAccount1 = new Account();
        newAccount1 = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        Contact newContact1 = new Contact();
        newContact1 = testData.newContact();
        newContact1.accountid = newAccount1.id;
        update newContact1;
        
        Contact newContact2 = new Contact();
        newContact2 = testData.newContact();
        newContact2.accountid = newAccount1.id;
        update newContact2;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant Plus' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        user usershare1 = new User();
        usershare1.Username ='orgadmin12202018@test.com';
        usershare1.LastName = 'orgadmin12202018';
        usershare1.Email = 'orgadmin12202018@test.com';
        usershare1.alias = 'org20201';
        usershare1.TimeZoneSidKey = 'America/New_York';
        usershare1.LocaleSidKey = 'en_US';
        usershare1.EmailEncodingKey = 'ISO-8859-1';
        usershare1.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare1.LanguageLocaleKey = 'en_US';
        usershare1.ContactId = newContact1.id;
        insert usershare1;  
        
        User newUser = EFLUserTestDataFactory.getUser('eFile Applicant Plus'); 
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        Account realORG = new Account();
        realORG = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        newContact.accountid = realORG.id;
        update newContact;
        newContact1.accountid = realORG.id;
        update newContact1;
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        /*CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLChangeHistoryTrigger'; 
        chDt.Disable__c = true; 
        insert chDt; */
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
        
        CARPOL_URLs__c Urls = new CARPOL_URLs__c(name='Landing_Page', URL__c='/apex/CARPOL_CommunityLandingPage');
        insert Urls;
        
        test.startTest();
        
        system.runAs(usershare)
        {
            ApexPages.currentPage().getParameters().put('deactivatedContactId',newContact1.Id);
            PortalController pcContext = new PortalController();
            pcContext.deactivateUser();
            pcContext.deactivateUser();
            String privateStatus = pcContext.privateStatus;
            Map < string, string > sobjectkeys = pcContext.sobjectkeys;
            string applicationId = pcContext.applicationId;
            string LineItemId = pcContext.LineItemId;
            String indOrgCon = pcContext.indOrgCon;
            User userDetails = pcContext.userDetails;
            String privateLink = pcContext.privateLink;
            string contactName  = pcContext.contactName;
            Boolean isAdmin = pcContext.isAdmin;
            string privateColumnHelpText = pcContext.privateColumnHelpText;
            string communityURL = pcContext.communityURL;
            
            List < Applicant_Contact__c > myAssocContacts = pcContext.myAssocContacts;
            pcContext.getlstConWrapper();
            pcContext.forwardToAuthPage();
            pcContext.editContact();
            ApexPages.currentPage().getParameters().put('conId',newContact1.Id);
            pcContext.deleteRecord();
            //PortalController.updateOldUser();
            pcContext.getReviewerRecords();
            pcContext.getmyAuthorizations();
            pcContext.getmyStateRegulations();
            
            EFLExternalUsersHandler.lineItemsWrapper lw = new EFLExternalUsersHandler.lineItemsWrapper(Lineitem, '');
            EFLExternalUsersHandler.getlineItemRecords(app.id);
            
            EFLAddUser addUserController = new EFLAddUser();
            addUserController.add();
            addUserController.add();
            addUserController.add();
            addUserController.add();
            addUserController.add();
            addUserController.remove();
            addUserController.userWrappers[1] = new EFLAddUser.userWrapper('Test@test.com', '');
			addUserController.userWrappers[2] = new EFLAddUser.userWrapper('Test@test.com', '');  
            addUserController.userWrappers[3] = new EFLAddUser.userWrapper('orgadmin12202018@test.com', '');            
            addUserController.userWrappers[4] = new EFLAddUser.userWrapper('newuser@test.com', '');
            addUserController.addUser();
            addUserController.cancel();
            addUserController.initiateUserWrapper();
            addUserController.userWrappers[0] = new EFLAddUser.userWrapper('orgadmin12202018@test.com', '');
			addUserController.addUser();
            addUserController.cancel();
            addUserController.initiateUserWrapper();
            addUserController.userWrappers[0] = new EFLAddUser.userWrapper(newUser.Username, '');
            addUserController.addUser();            
            
        }
        /*
        Account realOrg2 = new Account();
        realOrg2 = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        newContact1.accountid = realOrg2.id;
        update newContact1;
        
        system.runAs(usershare)
        {
            ApexPages.currentPage().getParameters().put('deactivatedContactId',newContact1.Id);
            PortalController pcContext = new PortalController();
            //pcContext.deactivateUser();
        }
        */
        test.stopTest();
    
    }
    
    @isTest
    static void ConstructsDashboardTest() {
        
	    Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        
        Applicant_Contact__c associatedContact = new Applicant_Contact__c();
        associatedContact = testData.newappcontact();
        
        associatedContact.Account__c = newAccount.id;
        update associatedContact;
        
        Account newAccount1 = new Account();
        newAccount1 = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        Contact newContact1 = new Contact();
        newContact1 = testData.newContact();
        newContact1.accountid = newAccount1.id;
        update newContact1;
        
        Contact newContact2 = new Contact();
        newContact2 = testData.newContact();
        newContact2.accountid = newAccount1.id;
        update newContact2;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant Plus' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        user usershare1 = new User();
        usershare1.Username ='orgadmin12202018@test.com';
        usershare1.LastName = 'orgadmin12202018';
        usershare1.Email = 'orgadmin12202018@test.com';
        usershare1.alias = 'org20201';
        usershare1.TimeZoneSidKey = 'America/New_York';
        usershare1.LocaleSidKey = 'en_US';
        usershare1.EmailEncodingKey = 'ISO-8859-1';
        usershare1.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare1.LanguageLocaleKey = 'en_US';
        usershare1.ContactId = newContact1.id;
        insert usershare1;  
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        Account realORG = new Account();
        realORG = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        newContact.accountid = realORG.id;
        update newContact;
        newContact1.accountid = realORG.id;
        update newContact1;
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLChangeHistoryTrigger'; 
        chDt.Disable__c = true; 
        insert chDt; 
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
           
        CARPOL_URLs__c Urls = new CARPOL_URLs__c(name='Landing_Page', URL__c='/apex/CARPOL_CommunityLandingPage');
        insert Urls;
        
        test.startTest();
        
        system.runAs(usershare1)
        {
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact1.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
            //auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount1.id);
            auth = testData.newAuth(app.Id);
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            CARPOL_UNI_DisableTrigger__c LRAdt = new CARPOL_UNI_DisableTrigger__c(); 
            LRAdt.Name = 'CARPOL_BRS_Link_RegulatedTrigger'; 
            LRAdt.Disable__c = false; 
            insert LRAdt;
            LRA = new Link_Regulated_Articles__c();
            LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Submitted');   
            
            CARPOL_UNI_DisableTrigger__c ConDt = new CARPOL_UNI_DisableTrigger__c(); 
            ConDt.Name = 'CARPOL_BRS_ConstructTrigger'; 
            ConDt.Disable__c = true; 
            insert ConDt;
            con = new Construct__c();
            con = testdata.newconstructByLIIdLRAId(LineItem.id, RA.Id, 'Waiting on Customer');
            
            EFLConstructs conDashboard = new EFLConstructs();
            ApexPages.currentPage().getParameters().put('id',con.Id);
            //conDashboard.showDetailView();
            conDashboard = new EFLConstructs();
        }
        set<id> appIds = new set<id>();
        appIds.add(app.id);
        EFLContactUtility.transferOwnership(userShare1, userShare, 'Applicant Transfer', appIds);
        EFLContactUtility.getUserApplications(userShare1, realORG.id);
    }
    
    @isTest
    static void CommunityAccessContactExceptionTest() {
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        try{
            
        EFLContactRepository.selectActiveUserContactsByCurrentUserDirectAccount();
        List<string> strList;
        EFLContactRepository.selectContactsUsersAccountRelationByUserNames(strList);
        EFLContactRepository.selectContactAccountRelationsByContactId(newContact.Id);
        EFLContactRepository.contactIdForsettargetObjectId();
        EFLContactUtility.DeleteAccountContactRelation(null);
        EFLContactUtility.transferOwnership(null, null, '', null);
        EFLContactUtility.UpdateContact(null);
        EFLAddUser addUserController = new EFLAddUser();
        addUserController.addUser();
        }
        catch(exception ex)
        {
            
        }
        try{EFLContactUtility.addFilters(null, null, null);}catch(exception ex){}
        try{EFLContactUtility.selectContactAccountRelationsByContactId(null);}catch(exception ex){}
        try{EFLContactUtility.transferOwnership(null, null, '', null);}catch(exception ex){}
        try{EFLContactUtility.DeleteAccountContactRelation(null);}catch(exception ex){}
        try{EFLContactUtility.UpdateContact(null);}catch(exception ex){}
        try{EFLContactUtility.DeleteAccountContactRelationByList(null);}catch(exception ex){}
    }
    

}