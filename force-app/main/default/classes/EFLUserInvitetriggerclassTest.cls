@isTest(seealldata=false)
private class EFLUserInvitetriggerclassTest {
public static CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      @IsTest(seealldata=false) static void EFLUserInvitetriggerclassTest() {
        Set<Id> userIdSet = new Set<Id>();
        Map<Id,String> fedPopMap = new Map<Id,String>();
        Map<String,String> jsonData = new Map<String,String>();
        Account acc=new Account();
        acc.Name='Test ACcount';
        insert acc;
        Contact con= testData.newContact();
        con.AccountId=acc.id;
        update con;        
        user currentUser = EFLUserTestDataFactory.getUser('Partner Account Admin');    

          Group grp = new Group();
          grp.name = acc.Name;
          grp.Type = 'Regular';        
          Insert grp;
          System.debug(grp.Name+'_'+acc.Name);
        
        userIdSet.add(currentUser.id);
        
        jsonData.put('userName','aphistestemailP@test.com');
        jsonData.put('UserInviteId',null);
        jsonData.put('status','Sent');
        jsonData.put('oldUserId',null);
        jsonData.put('newConId',con.id);
        jsonData.put('lastName','APHISTestLastNameP');
        jsonData.put('firstName','Joe');
        jsonData.put('FedId',null);
        jsonData.put('errorMsg','valid');
        jsonData.put('email','APHISTestPEmail@test.com');
        jsonData.put('dateInvitation','11/13/2017');
        
        String mapValue = JSON.serialize(jsonData);
        
        fedPopMap.put(null,mapValue);
        Test.startTest();
        EFLUserInvitetriggerclass cont = new EFLUserInvitetriggerclass();
        EFLUserInvitetriggerclass.updateOldUser(userIdSet,fedPopMap);
        EFLUserInvitetriggerclass.updateApplication(con.id, con.id);
        Test.stopTest();
       
       }
}