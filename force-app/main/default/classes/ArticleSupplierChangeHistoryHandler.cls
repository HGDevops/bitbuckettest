public class ArticleSupplierChangeHistoryHandler  implements IChangeHistoryHandler {
    static final string fieldName = 'Name';
    static final string fieldLabel = 'Last Name';
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        Schema.SObjectType sObjectType = newRecord.getSObjectType();
        
        if (sObjectType != null)
        {
            //record identifier
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
                
                Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {

        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.Old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
        	populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        Article_Supplier_Developer__c asd = (Article_Supplier_Developer__c)record;
        change.Line_Item__c = asd.Line_Item__c;
        if(!change.Delete_Flag__c)change.Article_Supplier_Developer__c = asd.id;        
        
    }

}