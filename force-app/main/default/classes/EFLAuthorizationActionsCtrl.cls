public with sharing class EFLAuthorizationActionsCtrl {

    public static final String INTERNAL_USER = 'Internal';
    public static final String EXTERNAL_USER = 'External';
    public static final String INTERNAL_USER_TYPE = 'Standard';
    public static final String ACTION_TYPE_RENEWAL = 'Renewal';
    public static final String ACTION_TYPE_AMEDNMENT = 'Amendment';
    public static final String ACTION_TYPE_CANCEL = 'Cancel';
    public static final String STATUS_CANCEL = 'Cancelled';
    public static final String ACTION_TYPE_REVOKE = 'Revoke';
    public static final String STATUS_REVOKE = 'Revoked';
    public static final String ACTION_TYPE_WITHDRAW = 'Withdraw';
    public static final String STATUS_WITHDRAW = 'Withdrawn';
        
    public String applicationType {get;set;}
    public List<SelectOption> actionOptions {get;set;}
    public Id authorizationId {get;set;}
    public List<AC__c> lineitemstoshow {get;set;}
    public Authorizations__c objauthItem {get;set;}
    public Id applicationID {get;set;}

    public EFLAuthorizationActionsCtrl(ApexPages.StandardController controller) {
        authorizationId = controller.getId();
        objauthitem = queryAuthorization(authorizationId);
        init();
    }

    public void init() {
        getActions();

        applicationID = objauthitem.Application__c;

        if (applicationID != null) {
            AmendRenewalappllist();
        }
    }

    //Ravee Racharla W-026016 Begin 
    public void getActions() {
        String userType = checkForInternalOrExternalUser();
        actionOptions = new List<SelectOption>();
        List<EFLAuthorizationActions__mdt> AuthActions = [SELECT Action_Type__c, Valid_Statuses__c, Valid_Decision_Types__c, Invalid_Statuses__c 
                                                            FROM EFLAuthorizationActions__mdt 
                                                            WHERE Program__c = 'BRS' 
                                                            AND Active__c = true 
                                                            AND User_Type__c = :userType 
                                                            ORDER BY Action_Type__c];

        for (EFLAuthorizationActions__mdt e : AuthActions) {

            Set<String> statusSet = getStatusSet(e);
            Set<String> invalidStatusSet = getInvalidStatusSet(e);
            Set<String> decisionSet = getDecisionSet(e);

            System.debug('-----------------Auth Status: ' + objauthitem.Status__c);
            System.debug('-----------------Decision Type: ' + objauthitem.Authorization_Type__c);
            System.debug('-----------------statusSet: ' + statusSet);
            System.debug('-----------------invalidStatusSet: ' + invalidStatusSet);
            System.debug('-----------------decisionSet: ' + decisionSet);
            System.debug('---------------------------------------------------------------------------------');

            if (statusSet.size() > 0) {
                if (statusSet.contains(objauthitem.Status__c) && decisionSet.contains(objauthitem.Authorization_Type__c)) {
                    actionOptions.add(new SelectOption(e.action_type__c,e.action_type__c));
                }
            } else if (invalidStatusSet.size() > 0) {
                if (!invalidStatusSet.contains(objauthitem.Status__c) && decisionSet.contains(objauthitem.Authorization_Type__c)) {
                    actionOptions.add(new SelectOption(e.action_type__c,e.action_type__c));
                }
            }

        }
        actionOptions.sort();
    }

    public Set<String> getStatusSet(EFLAuthorizationActions__mdt mdt) {
        if (mdt.Valid_Statuses__c != null) {
            return new Set<String>(mdt.Valid_Statuses__c.split(','));
        }

        return new Set<String>();
    }

    public Set<String> getInvalidStatusSet(EFLAuthorizationActions__mdt mdt) {
        if (mdt.Invalid_Statuses__c != null) {
            return new Set<String>(mdt.Invalid_Statuses__c.split(','));
        }

        return new Set<String>();
    }

    public Set<String> getDecisionSet(EFLAuthorizationActions__mdt mdt) {
        if (mdt.Valid_Decision_Types__c != null) {
            return new Set<String>(mdt.Valid_Decision_Types__c.split(','));
        }

        return new Set<String>();
    }

    // Ravee Racharla W-026016 End
    //
    //---------------------------------------------Return URL--------------------------------------------------------------------------// 
    public PageReference returntoauth() {
        PageReference pg1 = new PageReference('/'+authorizationID);
        return pg1;
    }

    //-------------------------------------- Proceed with Application Button-----------------------------------------------------//
    public PageReference validateSelection() {
        if (applicationType == ACTION_TYPE_CANCEL){
            return cancelAuth();
        }
        if (applicationType == ACTION_TYPE_REVOKE){
            return revokeAuth();
        }
        if (applicationType == ACTION_TYPE_WITHDRAW){
            system.debug('Action@@@ '  + applicationType);
            return withdrawAuth();
        }
        
        if (applicationType != ACTION_TYPE_RENEWAL && applicationType != ACTION_TYPE_AMEDNMENT) {
           system.debug('1++');
            return ProceedwithApplication();
        } else {
            List<EFL_Amendment_and_Renewal_Configuration__mdt> amendmentAndRenewalConfig = EFLAmendmentAndRenewalValidationHelper.queryAmendmentAndRenewalMdt();
            Boolean isValid = EFLAmendmentAndRenewalValidationHelper.doSelectionValidation(amendmentAndRenewalConfig, objauthitem);
            System.debug('------------------------isValid: ' + isValid);
            if (isValid) {
                return ProceedwithApplication();
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Error: ' + applicationType + ' is not allowed on or after a Permit’s expiration date'));
            }
        }

        return null;
    }

    public PageReference ProceedwithApplication() {
        //Use new frame work AuthorizationEngineFactory --> BRSAuthorizationEngine -> EFLauthorizationEngineUtility
        //1. Validations 
        //2. Clone
        //3. Return
        System.debug('Proceed With Application');
        Application__c newApp;

        Boolean bValidate = false;

        if (objauthitem != null){
            EFLIAuthorizationEngine brsEngine = EFLAuthorizationEngineFactory.getEngine(objauthitem);
            bValidate = EFLAuthorizationEngineUtility.validateAction(objauthitem, applicationType);

            System.debug('---------------------------bValidate' + bValidate);

            if (bValidate) {
                System.debug('Process Amendment: ' + applicationType);
                 System.debug('objauthitem: ' + objauthitem);
                newApp =  brsEngine.processAmendment(objauthitem, applicationType);

                if (newApp != null) {
                    return new PageReference( '/'+newapp.id);
                } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'An unexpected error has occurred please contact your system administrator.'));
                }
            }

         }

        return null;
    }

    public void AmendRenewalappllist() {
        List<Amendment_Renewal__c> ARlineitems = new List<Amendment_Renewal__c>();
        List<Id> lineitemids = new List<id>();

        if (applicationID != null) {
            //RR  4/16 - begin
            //Id lineitemid = [SELECT id, Application_Number__c FROM AC__c WHERE Application_Number__c =: applicationID LIMIT 1].Id;

            Id lineitemid;
            List<AC__c> lineItem = [SELECT id, Application_Number__c FROM AC__c WHERE Application_Number__c =: applicationID LIMIT 1];
            if(lineItem != null && lineItem.size() > 0){
                lineitemid= lineItem[0].Id;
            } 
            //RR  4/16 - end
            if (lineitemid != null) {
                ARlineitems = [SELECT ID
                                    , Parent_Line_Item__c
                                    , Parent_line_item__r.Application_Number__c
                                    , child_line_item__c,Parent_Line_Item__r.id
                                    , child_line_item__r.Application_Number__c
                                    , RecordTypeID,RecordType.Name 
                                FROM Amendment_Renewal__c 
                                WHERE (Parent_Line_Item__c = :lineitemid OR child_line_item__c = :lineitemid)];

                if (ARlineitems.size()>0) {
                    for (Amendment_Renewal__c ARLine : ARlineitems) {
                        if (ARLine.Parent_line_item__r.Application_Number__c != applicationID) {
                            lineitemids.add(ARLine.Parent_Line_Item__r.id);
                        }
                        if (ARLine.child_line_item__r.Application_Number__c != applicationID) {
                            lineitemids.add(ARLine.child_line_item__r.id);
                        }
                    }
                }

                if (lineitemids.size()>0) {
                    lineitemstoshow = [SELECT Id
                                            , Name
                                            , Application_Number__c
                                            , Application_Number__r.Application_Status__c
                                            , Application_Number__r.Application_Type__c
                                            , Authorization__c
                                            , Authorization__r.Status__c 
                                        FROM AC__c 
                                        WHERE Id =: lineitemids];
                }
            }
        }
    }

    public String getUserType() {
        return UserInfo.getUserType();
    }

    public String checkForInternalOrExternalUser() {
        if (getUserType() == INTERNAL_USER_TYPE) {
            return INTERNAL_USER;
        } else {
            return EXTERNAL_USER;
        }
    }

    public Authorizations__c queryAuthorization(Id authorizationId) {
        return [SELECT AC_Applicant_Address__c,Associated_Authorization__c,AC_Applicant_Email__c,AC_Applicant_Fax__c,AC_Applicant_Name__c,AC_Applicant_Phone__c,
                    AC_Organization__c,AC_USDA_License_Expiration_Date__c,AC_USDA_License_Number__c,AC_USDA_Registration_Expiration_Date__c,
                    AC_USDA_Registration_Number__c,Applicant_Alternate_Email__c,Applicant_Instructions__c,Applicant_Organization__c,Applicant__c,Application_CBI__c,
                    Application__c,CIP_Catagory__c,Program_Pathway__c,type_of_permit__c,Application_Type__c, Port_of_Entry_For_Importation_Only_del__c,
                    Authorization_Type__c,Authorized_User__c,Biological_Material_present_in_Article__c,Bio_Tech_Reviewer__c,BRAP_Manager1__c,BRAP_Manager2__c,
                    BRS_Create_Labels__c,BRS_Create_State_Review_Records__c,BRS_Hand_Carry_For_Importation_Only__c,BRAP_Manager_3__c,Total_of_New_Constructs__c,
                    BRS_Introduction_Type__c,BRS_Number_of_Labels__c,BRS_Proposed_End_Date__c,BRS_Proposed_Start_Date__c,BRS_Purpose_of_Permit__c,BRS_Send_Labels__c,
                    BRS_State_Review_Notification__c,CBI__c,Country_and_Locality_Information__c,CreatedById,CreatedDate,Date_Issued__c,Documents_Sent_to_Applicant__c,
                    Effective_Date__c,Email_Contents__c,EPA_Review_Required__c,Fee_Amount__c,Id,If_Yes_Please_Describe__c,IsDeleted,Issuer_Name__c,
                    Issuer__c,Is_violator__c,Jurisdiction__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,Share_Permit_Package_with_Applicant__c,
                    LastViewedDate,Locked__c,Mailing_Address__c,Means_of_Movement__c,Name,Number_of_New_Design_Protocols__c,Number_of_Release_Sites__c,
                    Organization_Unique_ID__c,OwnerId,Permit_Number__c,Permit_Specialist__c,Plant_Inspection_Station__c,Ports_of_Arrival__c,
                    Port_of_Entry__c,Position__c,Prefix__c,RecordTypeId,Remaining_Agreement_Needed__c,Status__c,Sub_Status__c,SystemModstamp,Template__c,Thumbprint__c,
                    Thumbprint__r.Program_Prefix__r.Renewal_Start_Period__c,Thumbprint__r.Program_Prefix__r.Renewal_End_Period__c,Total_Conditions__c,
                    Total_Supplemental_Conditions__c,UNI_Alternate_Phone__c,UNI_Country__c,UNI_County_Province__c,UNI_Zip__c,Expiry_Date__c, 
                    Program__c
                    , Program_Pathway__r.Program__r.Name
                    , Program_Pathway__r.Name
                    , Expiration_Date__c 
                    , Withdrawn_Date_Time__c, Withdrawn_By__c
                FROM Authorizations__c 
                WHERE Id = :authorizationId];
    }

    public PageReference navigateBack() {
        PageReference pRef = new PageReference('/' + authorizationId);
        pRef.setRedirect(true);
        return pRef;
    }

    public PageReference cancelAuth(){
        objauthitem.Status__c = STATUS_CANCEL;
        update  objauthitem; 
        
        //Example to go to note  taken from account
        //002/e?parent_id=00135000003zWn9&retURL=%2F00135000003zWn9%3Fnooverride%3D1
        
        //get first 15 chars of authid
        string authId = String.valueOf(authorizationId).substring(0, 15);
        note n = new note();
        n.parentId=authorizationId; 
        n.title= 'Cancellation Reason'; 
        insert n;
        //PageReference redirect = new PageReference('/002/e?parent_id=' + authId + '&retURL=' +authId );
        //redirect.getParameters().put('parent_id', sfObject.Id);
        //redirect.getParameters().put('retURL', this.ReturnUrl);
        PageReference redirect = new PageReference('/' + n.id + '/e?parent_id=' + authId + '&retURL=' +authId );
        redirect.setRedirect(true);
        return redirect;
    }

    public PageReference revokeAuth(){
        objauthitem.Status__c = STATUS_REVOKE;
        update  objauthitem; 
        
        return navigateBack();
    }

    public PageReference withdrawAuth(){
        objauthitem.Status__c = STATUS_WITHDRAW;
        objauthitem.Withdrawn_Date_Time__c = system.now();
        objauthitem.Withdrawn_By__c=UserInfo.getUserId() ; 
        update  objauthitem;
        
        //Set the application (ac__C) status to Withdrawn. The Trigger is updating line item and auth status
        // objauthitem.Application__c
        // 
        if (objauthitem.Application__c !=null || objauthitem.Application__c !=''){
            //system.debug('Action@@@ '  + applicationType);
            list<Application__c>  lstApplication = new list<Application__c>();
            lstApplication  = [select id, Application_Status__c, Locked__c from Application__c where id =:objauthitem.Application__c ];
            list<AC__c>  lstLineItem = new list<AC__c>();
            lstLineItem = [select id, Status__c from AC__c where Authorization__c =:objauthitem.Id];
            
            //This will always have only one record as Auth can have only one parent record application
            //The trigger on application will update status on line item and auth objects; 
            //system.debug('Action@@@ '  + lstApplication);
            if (lstApplication !=null && lstApplication.size()>0){
                //Was Locked
                boolean wasLocked = false;
                if(lstApplication[0].Locked__c=='Yes')
                {
                    wasLocked = true;
                }
                //check if this app has how many auths 
                list<Authorizations__c>  lstAuths = [select id, status__c,Application__c  from Authorizations__c where Application__c =: lstApplication[0].id];
                // if only auth then update application 
                if(lstAuths !=null && lstauths.size()==1 ){
                    
                    if(wasLocked == true)
                    {
                        lstApplication[0].Locked__c = 'No';
                        lstApplication[0].Application_Status__c = STATUS_WITHDRAW; 
                        update lstApplication; 
                    }
                    
                    if(!lstLineItem.isEmpty())
                    {
                        //lstLineItem[0].Locked__c = 'No';
                        lstLineItem[0].Status__c = STATUS_WITHDRAW;
                        update lstLineItem;
                    }
                    
                    /*lstApplication[0].Application_Status__c = STATUS_WITHDRAW; 
                    update lstApplication; */
                    
                    
                    /*if(wasLocked == true)
                    {
                        lstApplication[0].Locked__c = 'Yes';
                        update lstApplication;
                    }*/
                    
                }
            }
            
        }
        return navigateBack();
    }
}