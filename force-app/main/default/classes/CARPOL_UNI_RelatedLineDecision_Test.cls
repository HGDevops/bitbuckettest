/*------
Name : CARPOL_UNI_RelatedLineDecision_Test.cls
Description : Test class for CARPOL_UNI_RelatedLineDecision.cls
Author : Vijay Vellaturi
crated date : 07-Dec-16
-----*/
@isTest //(Seealldata=true)
public class CARPOL_UNI_RelatedLineDecision_Test{
    static testmethod void testRelatedDM(){ 
        Test.startTest();    
        Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];
        system.runAs(u){
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            String AccountRecordTypeId = testData.AccountRecordTypeId;
            testData.insertcustomsettings();
            Account objacct = testData.newAccount(AccountRecordTypeId); 
            Contact objcont = testData.newcontact();
            Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
            plip.Column_1_API_Name__c = 'Scientific_Name__r.Name';
            plip.Column_2_API_Name__c = 'Country_Of_Origin__r.Name';
            plip.Column_3_API_Name__c = 'Component__r.Name';
            plip.Column_4_API_Name__c = 'Where_will_the_RA_be_grown__c';
            plip.Column_5_API_Name__c = 'Intended_Use__r.Name';
            plip.Column_6_API_Name__c = 'Total_number_of_shipments__c';
            update plip;
            Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
            objdm.Program_Line_Item_Pathway__c = plip.id;
            //objdm.Excluded_City_Township__c  = [select id from Level_2_Region__c where name = 'Collin'].id;
            insert objdm;
            Required_Documents__c Reqddocs = new Required_Documents__c(Name='Req Docs Research op',Decision_Matrix__c =objdm.id, Required_Docs__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement');
            insert Reqddocs;
            
            Application__c objapp = testData.newapplication();
            AC__c li = TestData.newlineitem('Personal Use', objapp);
            li.Exporter_Mailing_City__c = 'Collin';
            li.Program_Line_Item_Pathway__c = plip.id; 
            update li;
            Applicant_Attachments__c objAtt = testData.newAttach(li.id);
            
            List <AC__c> LineList = New List <AC__c>();
            LineList.add(li);
            CARPOL_UNI_RelatedLineDecision.insertRLD(LineList);    
            Test.stopTest();   
            
        }
    }
    static testmethod void testRelatedDM2(){ 
        Test.startTest();    
        Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
        User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];         
        system.runAs(u){
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            String AccountRecordTypeId = testData.AccountRecordTypeId;
            testData.insertcustomsettings();
            Account objacct = testData.newAccount(AccountRecordTypeId); 
            Contact objcont = testData.newcontact();
            Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
            plip.Column_1_API_Name__c = 'Scientific_Name__r.Name';
            plip.Column_2_API_Name__c = 'Country_Of_Origin__r.Name';
            plip.Column_3_API_Name__c = 'Component__r.Name';
            plip.Column_4_API_Name__c = 'Where_will_the_RA_be_grown__c';
            plip.Column_5_API_Name__c = 'Intended_Use__r.Name';
            plip.Column_6_API_Name__c = 'Total_number_of_shipments__c';
            update plip;
            Level_1_Region__c objLvl1 = testData.newlevel1regionAL();    
            Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
            objdm.Program_Line_Item_Pathway__c = plip.id;
            objdm.Excluded_City_Township__c  = null;
            objdm.Excluded_State_Province__c = objLvl1.id;     
            insert objdm;
            Required_Documents__c Reqddocs = new Required_Documents__c(Name='Req Docs Research op',Decision_Matrix__c =objdm.id, Required_Docs__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement');
            insert Reqddocs;
            
            
            Application__c objapp = testData.newapplication();
            Authorizations__c objauth = testData.newAuth(objapp.id);    
            AC__c li = TestData.newLineItem('Personal Use', objapp, objauth);
            li.Exporter_Mailing_City__c = 'Collin';
            li.Program_Line_Item_Pathway__c = plip.id; 
            li.Cloned_Line_Item__c = true;
            update li;
            
            Applicant_Attachments__c objAtt = testData.newAttach(li.id);
            
            List <AC__c> LineList = New List <AC__c>();
            LineList.add(li);
            CARPOL_UNI_RelatedLineDecision.insertRLD(LineList);  
            
            AC__c li2 = TestData.newLineItem('Personal Use', objapp, objauth);
            li2.Exporter_Mailing_City__c = 'Collin';
            li2.Program_Line_Item_Pathway__c = plip.id; 
            li2.Cloned_Line_Item__c = false;
            update li2;
            
            
            
            List <AC__c> LineList2 = New List <AC__c>();
            LineList2.add(li2);
            CARPOL_UNI_RelatedLineDecision.insertRLD(LineList2);     
            Test.stopTest();   
            
        }
    }       
}