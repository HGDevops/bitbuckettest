@isTest
public class EFLLabelTriggerHandlerTest {
    
    public static string LABEL_STATUS_ACTIVE = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
    public static string LABEL_STATUS_VOIDED = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
    public static integer NUMBER_OF_RECORDS = 6;
    
    @TestSetup
    static void makeData(){
        EFLTestDataFactoryBulk.setupTestDataSuite(NUMBER_OF_RECORDS);
    }
    
    @isTest
    static void testSendEmailsForVoidedLabels(){
        Profile p = EFLTestDataFactoryBulk.getProfileByName('BRS Analyst');
        User analyst = EFLTestDataFactoryBulk.getUser(p);
        //system.runAs(analyst){
            integer emailsSent =  Limits.getEmailInvocations();
            system.assertEquals(0, emailsSent);
            list<Label__c> labels = [SELECT Id, Status__c FROM Label__c];
            system.assertEquals(NUMBER_OF_RECORDS, labels.size());
            for(Label__c l:labels){
                system.assertEquals(LABEL_STATUS_ACTIVE, l.Status__c);
                l.Status__c = LABEL_STATUS_VOIDED;
            }
            test.startTest();
            UPDATE labels;
            test.stopTest();
            // NOTE: because the emails run in an @Future method, we cannot assert email invocations directly.
            integer emailsSentAfterTrigger =  EFLLabelServices.EMAILS_INVOKED;
            system.assertEquals(NUMBER_OF_RECORDS, emailsSentAfterTrigger);
            list<Error_Logging__c> errors = [SELECT Id, Class__c, Trace__c FROM Error_Logging__c WHERE Class__c = 'EFLLabelService'];
            system.assertEquals(true, errors.isEmpty());
        //}
    }
    
    @isTest
    static void testSendEmailsForVoidedLabels_Negative(){
        Profile p = EFLTestDataFactoryBulk.getProfileByName('BRS Analyst');
        User analyst = EFLTestDataFactoryBulk.getUser(p);
        //system.runAs(analyst){
            integer emailsSent =  Limits.getEmailInvocations();
            system.assertEquals(0, emailsSent);
            integer checkLimit = 3;
            list<Label__c> labels = [SELECT Id, Status__c FROM Label__c LIMIT :checkLimit];
            system.assertEquals(checkLimit, labels.size());
            for(Label__c l:labels){
                system.assertEquals(LABEL_STATUS_ACTIVE, l.Status__c);
                l.Status__c = LABEL_STATUS_VOIDED;
            }
            test.startTest();
            UPDATE labels;
            test.stopTest();
            // NOTE: because the emails run in an @Future method, we cannot assert email invocations directly.
            system.debug('emails invoked: ' + Limits.getEmailInvocations());
            integer emailsSentAfterTrigger =  EFLLabelServices.EMAILS_INVOKED;
            system.assertEquals(checkLimit, emailsSentAfterTrigger);
            
            list<Error_Logging__c> errors = [SELECT Id, Class__c, Trace__c FROM Error_Logging__c WHERE Class__c = 'EFLLabelService'];
            system.assertEquals(true, errors.isEmpty());
        //}
    }
}