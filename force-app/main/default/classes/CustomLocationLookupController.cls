public with sharing class CustomLocationLookupController {
    
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
    public Location__c locObj        {get;set;} // new account to create
    public List<Location__c> results; // search results
    public String searchString           {get;set;} // search keyword 
    public String authId                 {get;set;}
    public Set<Id> lineItemsIdSet;
    
    public CustomLocationLookupController() {
        locObj = new Location__c();
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        authId = System.currentPageReference().getParameters().get('authId');
        size=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        results = (List<Location__c>)setCon.getRecords();
    }    
    
    public ApexPages.StandardSetController setCon {
        get {
            lineItemsIdSet = new Set<Id>();
            if(setCon == null) { 
                if(authId != null && authId != '') {
            lineItemsIdSet = new Map<Id, AC__c>([Select id from AC__c Where Authorization__c =: authId]).keySet();
        }
        
       string locationTypeId = [SELECT id FROM RecordType where SobjectType='Location__c' and  DeveloperName = 'Release_Location'].id;
       // String soql = 'Select id, Contact_Address1__c,name,Primary_Contact__r.Name,Primary_Contact__c,Secondary_Contact__r.Name,Secondary_Contact__c from Location__c Where';
        String soql = 'SELECT ID,Name,Contact_Name1__c, Contact_Name2__c,Street_Add1__c,Street_Add2__c,Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,Day_Phone_2__c,Zip__c,Other_Material_Types__c,Proposed_Planting__c,Number_of_Acres_Text__c,Primary_Contact_Last_Name__c,Contact_Address1__c,Primary_State_Text__c,Primary_Country_Text__c,Contact_Zip__c,Primary_County_Text__c,Contact_City__c,Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_Zip__c,Secondary_Contact_State__c,Secondary_Country_Text__c,Secondary_Contact_County__c,Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, Line_Item__c,Description__c,Applicant_Instructions__c,RecordType.Name,Country__r.Name,Level_2_Region__r.name,Status__c,State__r.Name,Status_Graphical__c,CreatedDate,Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c FROM Location__c WHERE';
        set<string> locationSet = new set<string>();
                
        if(searchString != '' && searchString != null)
                //Safeena start
                system.debug('safeena '+apexpages.currentpage().getparameters().get('reportType'));
                if(apexpages.currentpage().getparameters().get('reportType') == 'field_test_report' || apexpages.currentpage().getparameters().get('reportType') == 'volunteer_monitoring_report'){
                    list<Location__c> tempLoc = [select id,(select id, Is_Submitted__c from Self_Reporting__r where Report_Summary__r.Report_Type__c = 'Planting Report' Order By CreatedDate ASC) from Location__c Where Line_Item__c IN: lineItemsIdSet and RecordTypeId =: locationTypeId];
                    system.debug(tempLoc);
                    for(Location__c locObjTemp : tempLoc){
                        for(Self_Reporting__c sr : locObjTemp.Self_Reporting__r){
                            //rsMap.put(sr.Report_Summary__r.id, locObjTemp.id);
                            if(!sr.Is_Submitted__c){
                                if(locationSet.contains(locObjTemp.id)){
                                   // locationSet.remove(locObjTemp.id);
                                   
                                }
                            }
                            else{
                                locationSet.add(locObjTemp.id); 
                            }
                        }
                    }
                }
                system.debug('safeena '+locationSet);
                
                //Safeena End
                
                //soql = soql +  ' name LIKE \'%' + searchString +'%\' AND';
                if(!string.isBlank(searchString)){
                soql = soql +  ' ( name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                soql = soql +  ' Location_Unique_Id__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                soql = soql +  ' County_Text__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                soql = soql +  ' state__r.Name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' ) AND';
                        }
                else{
                  soql = soql +  ' name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' AND';  
                }
                soql = soql + ' Line_Item__c IN: lineItemsIdSet' ;
                //Added for W-034846 to allow all visibility for all Locations on Cleaning Notice
                if(apexpages.currentpage().getparameters().get('reportType') != 'cleaning_notice'){
               		soql = soql + ' AND RecordTypeId =: locationTypeId' ;
                }//End W-034846
                if(locationSet != null && locationSet.size() > 0){
                    soql = soql + ' AND id IN: locationSet' ;
                }
                soql = soql + ' limit 25';
                system.debug('soql'+ soql );
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }            
            return setCon;
        }
        set;
    }
     
    //Changes the size of pagination
    public PageReference refreshPageSize() {
         setCon.setPageSize(size);
         return null;
    } 
    
    public List<Location__c> getresults() {
         return (List<Location__c>) setCon.getRecords();
    }
   
    // performs the keyword search
    public PageReference search() {
        setCon = null;
        results = (List<Location__c>) setCon.getRecords();
        return null;
    }  
    
    // prepare the query and issue the search command
    public void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = results = (List<Location__c>) setCon.getRecords();               
    } 
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }

    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
}