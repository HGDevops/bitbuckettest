/*

Created By: Anh Phan
Created Date: 07/16/2018
Purpose: This is a helper class for trigger CARPOL_UNI_MasterLineItemTrigger.
------------------------------------------------------------------------------------------------------------------------
Modified By: 
Modified Date: 
Purpose: 

*/

public inherited sharing class CARPOL_LineItemApplicationTriggerHelper {
    
    /*

Method Name: updateLineitemChildRecords
Parameters: Map of line item records
Purpose: This method is used to update parent status to all related child records.
Created By: Anh Phan
-------------------------------------------------------------------------------------

*/
    public static void updateLineitemChildRecords (Map<Id, AC__c > lineItemMap) {
        
        //Constant final string for status
        final string SUBMITTED = 'Submitted';
        
        //Set of LineItem IDs with status submitted
        Set<Id> lineItemIDSet = new Set<Id>();
        for (AC__c li: lineItemMap.values()) {
            //Checking for line item status for submitted
            if (SUBMITTED.equalsIgnoreCase(li.Status__c)) {
                lineItemIDSet.add(li.Id);
            }
        }
        //Collection to hold all the child records from parent line item
        List<sObject> lineItemChildRecordsList = new List<sObject>();
        
        //Iterating and fetching all the child records w.r.t parent line item
        for(AC__c linObj: [select id, Status__c,
                           (select id, Status__c from Link_Regulated_Articles__r where Status__c = 'Draft'),
                           (select id, Status__c from Constructs__r where Status__c = 'Draft'),
                           (select id, Status__c from Link_Construct_Design_Protocol_Records__r where Status__c = 'Draft'),
                           (select id, Status__c from Locations__r where Status__c = 'Draft'),
                           (select id, Status__c from Applicant_Attachments__r where Status__c = 'Draft'),
                           (select id, Status__c from Article_Suppliers__r where Status__c = 'Draft')
                           from AC__c
                           where id IN :lineItemIDSet]) {
                               
                               //Iterating over link regulated articles
                               for(Link_Regulated_Articles__c lraObj: linObj.Link_Regulated_Articles__r) {
                                   
                                   lraObj.Status__c = SUBMITTED;
                                   
                                   lineItemChildRecordsList.add(lraObj);
                                   
                               }//End of for(Link_Regulated_Articles__c lraObj: linObj.Link_Regulated_Articles__r)
                               
                               //Iterating over Construct
                               for(Construct__c consObj: linObj.Constructs__r) {
                                   
                                   consObj.Status__c = SUBMITTED;
                                   
                                   lineItemChildRecordsList.add(consObj);
                                   
                               }//End of for(Constructs__c consObj: linObj.Construct__r)
                               
                               //Iterating over Previously reviewed Constructs/SOPs
                               for(Construct_Application_Junction__c cajObj: linObj.Link_Construct_Design_Protocol_Records__r) {
                                   
                                   cajObj.Status__c = SUBMITTED;
                                   
                                   lineItemChildRecordsList.add(cajObj);
                                   
                               }//End of for(Construct_Application_Junction__c cajObj: linObj.Link_Construct_Design_Protocol_Records__r)
                               
                               //Iterating over Location
                               for(Location__c locObj: linObj.Locations__r) {
                                   
                                   locObj.Status__c = SUBMITTED;
                                   
                                   lineItemChildRecordsList.add(locObj);
                                   
                               }//End of for(Location__c locObj: linObj.Locations__r)
                               
                               //Iterating over Aplicant Attachments
                               for(Applicant_Attachments__c aaObj: linObj.Applicant_Attachments__r) {
                                   
                                   aaObj.Status__c = SUBMITTED;
                                   
                                   lineItemChildRecordsList.add(aaObj);
                                   
                               }//End of for(Applicant_Attachments__c aaObj: linObj.Applicant_Attachments__r) 
                               
                               //Iterating over Article Supplier Developer
                               for(Article_Supplier_Developer__c asdObj: linObj.Article_Suppliers__r) {
                                   
                                   asdObj.Status__c = SUBMITTED;
                                   
                                   lineItemChildRecordsList.add(asdObj);
                                   
                               }//End of for(Article_Supplier_Developer__c asdObj: linObj.Article_Supplier_Developer__r)
                               
                               
                           }//End of for(AC__c linObj:)
        
        // updated by JB 5/8/2019
        // moving this if block outside of for-loop to avoid DML inside a loop.
        
        //Performing null check on collection before DML operation
        if(lineItemChildRecordsList <> NULL && !lineItemChildRecordsList.isEmpty()) {
            
            //Sorting the collection
            lineItemChildRecordsList.sort();
            
            //system.debug('lineItemChildRecordsList : '+ lineItemChildRecordsList);
            
            //Performing DML operation on child records
            update lineItemChildRecordsList;
            
        }//End of if(lineItemChildRecordsList <> NULL && !lineItemChildRecordsList.isEmpty())
        
    }//End of updateLineitemChildRecords
    
}//End of CARPOL_LineItemApplicationTriggerHelper