public class EFLConstructViewController {

    public ID lineItemId {get;set;}
    public ID constructID {get;set;}
    
    public EFLConstructController pageController {get; set;}
    
    
    public construct__c constructRecord{get;set;}
    public string selectedRA{get;set;} 
    public phenoType__c phenoTypeRecord{get;set;}
    public genoType__c genoTypeRecord{get;set;}
    
     public id phenoTypeID{get;set;}
    public id genoTypeID{get;set;}
    public id selectedGenoCategory{get;set;}
    public list<Link_Regulated_Articles__c> linkregart{get;set;}
    public EFLConstructViewController(){
        system.debug('lineItemId:'+ lineItemId);
    }
   /*  public void showDetail(){
        selectedRA = '';
        system.debug('constructID:'+ constructID);
        if(constructID!=null){
            system.debug('constructID:'+ constructID);
             constructRecord = EFLConstructRepository.selectByID(constructID);
             selectedRA = constructRecord.Link_Regulated_Article__c;
             //renderPhenoList = true;
        }else{
             constructRecord = new Construct__c();
             //renderPhenoList = false;
        }
         
     }
    
    public list<Phenotype__c> phenotypeList{get
    {
         if(constructID != NULL){
            phenotypeList = EFLConstructRepository.selectPhenotypesByConstruct(constructID);
          }
        return phenotypeList;   
    }set;} 
    
    public list<Genotype__c> genotypeList{get
    {
      if(constructID != NULL){
            genotypeList = EFLConstructRepository.selectGenotypesByConstruct(constructID);
        }
        return genotypeList;   
    }set;}  
    
     public List<SelectOption> getoptions(){
        list<SelectOption> raoptions=new  list<SelectOption>();
        if(LineItemId==null){
        }
        else{
            linkregart=[SELECT Regulated_Article__c, Regulated_Article__r.name FROM Link_Regulated_Articles__c WHERE Link_Regulated_Articles__c.Line_Item__c=:LineItemId];
        }
        raoptions.add(new  SelectOption(' ','---Select---'));
        if(linkregart.size()>0){
            for(Link_Regulated_Articles__c RA:linkregart){
                raoptions.add(new  SelectOption(RA.Regulated_Article__c, RA.Regulated_Article__r.name));}
        }
        return raoptions;
    }
    
    
    public void raidcheck(){
        if(selectedRA!=' ' && selectedRA != null){
            constructRecord.Link_Regulated_Article__c=selectedRA;
        }
        else 
        {   selectedRA = null;
            constructRecord.Link_Regulated_Article__c=null;
        }
    }
    
    public void showPhenoDetail(){
        
        if(phenoTypeID!=null){
           phenoTypeRecord = EFLConstructRepository.selectPhenotypeByID(phenoTypeID);
        }else{
            system.debug('inside phenodetail');
           phenoTypeRecord = new phenoType__c();
        }

     }

    public void showGenoDetail(){
        if(GenoTypeID!=null){
           GenoTypeRecord = EFLConstructRepository.selectGenotypeByID(GenoTypeID);
           selectedGenoCategory = GenoTypeRecord.recordtypeid;
        }else{
           genoTypeRecord = new genoType__c();
           selectedGenoCategory = null;
        }

     }
    
    
//Genotype wrapper
 public class genotypeWrapper { 
     public string genotypeCategory {get;set;}
     public list<genotype__c> genotypelist {get;set;}
     public genotypeWrapper(){
          genotypelist = new list<genotype__c>();
     }
     public genotypeWrapper(list<genotype__c> genotypelist, string genotypeCategory ) {
        genotypelist = new list<genotype__c>();
        this.genotypeCategory = genotypeCategory; 
     }
 }    
    
    // returns a list of wrapper objects for the sObjects  
    public list<genotypeWrapper> getGenotypeWrapList() {  
        system.debug('Entering getGenotypeWrap');
        getGenoRecordTypes();
        list<genotypeWrapper> genoWrapList = new list<genotypeWrapper>();
        genotypeWrapper genoWrap;
        for(string rectype :genoRecordTypeList) {
            genoWrap = new genotypeWrapper();
            for(genotype__c geno : genotypeList ){
                if(geno.recordtype.name ==  recType){
                   genoWrap.genotypelist.add(geno); 
                }
            }  
            if(!genoWrap.genotypelist.isempty()){ 
                genoWrap.genotypeCategory = recType;
                genoWrapList.add(genoWrap); 
            }
        } 
        system.debug('genoWrapList###'+genoWrapList);
        return genoWrapList;
    }
    
    list<string> genoRecordTypeList;
    public list<SelectOption> getGenoRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        genoRecordTypeList = new list<string>();
        for (list<RecordType> rts : [SELECT ID, name 
                                       FROM RecordType 
                                      WHERE SObjectType = 'Genotype__c' 
                                   ORDER BY name]) {
            for (RecordType rt : rts) {
                options.add(new SelectOption(rt.ID, rt.Name));
                genoRecordTypeList.add(rt.Name);
            }
        }
        options.add(0,new SelectOption('','--Select--'));
        return options;
    } 
    
    */
}