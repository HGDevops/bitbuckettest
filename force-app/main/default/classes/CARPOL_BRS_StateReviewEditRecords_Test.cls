@isTest(seealldata=true)
private class CARPOL_BRS_StateReviewEditRecords_Test {
    @IsTest static void CARPOL_BRS_StateReviewEditRecords_Test() {
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        //Account objacct = testData.newAccount(AccountRecordTypeId); 
        //Contact objcont = testData.newcontact();
        //breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        //Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac1 = testData.newLineItem('Personal Use',objapp);
        //AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
        //AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        ac1.Authorization__c = objauth.id;
        ac1.status__c = 'Waiting on Customer';
        //ac1.RecordTypeId = ACLIRecordTypeId;     
        //FIXME: THE FOLLOWING LINE CAUSES SPRINGCM CLASS ERROR.
        update ac1;
        
        
        Test.startTest();
        
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        //Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        //Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        // FIXME: CANNOT CREATE A REVIEWER DUE TO SPRINGCM ERRORS.
        Reviewer__c objrev = new Reviewer__c();
        objrev.Status__c= 'Open';
        objrev.Authorization__c = objauth.id;
        //objrev.State_Regulatory_Official__c = objcont.id;
        //objrev.BRS_State_Reviewer_Email__c = objcont.Email; 
        objrev.recordtypeid = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('FSIS Review Record').getRecordTypeId();
        insert objrev;
        //   objrev.State_has_no_comments__c = true;
        //  objrev.State_Comments__c ='Test state requirements';
        PageReference pageRef = Page.CARPOL_BRS_StateReviewRecords;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objrev);
        ApexPages.currentPage().getParameters().put('ID',objrev.id);
        
        CARPOL_BRS_StateReviewEditRecords extclass = new CARPOL_BRS_StateReviewEditRecords(sc);
        extclass.saveAndCongrat();
        //extclass.deleteRecord();
        extclass.getRelatedConditions();
        extclass.getRelatedStateRequirements();
        extclass.redirect ='';
        system.assert(extclass != null);     
        Test.stopTest();     
    }
    
    @isTest
    static void testNoReuirementsTrue(){
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        //Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac1 = testData.newLineItem('Personal Use',objapp);
        //AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
        //AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        ac1.Authorization__c = objauth.id;
        ac1.status__c = 'Waiting on Customer';
        //ac1.RecordTypeId = ACLIRecordTypeId;     
        //FIXME: THE FOLLOWING LINE CAUSES SPRINGCM CLASS ERROR.
        //update ac1;
        
        
        Test.startTest();
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        //Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        //Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        // FIXME: CANNOT CREATE A REVIEWER DUE TO SPRINGCM ERRORS.
        Reviewer__c objrev = new Reviewer__c();
        objrev.Status__c= 'Open';
        objrev.Authorization__c = objauth.id;
        objrev.State_Regulatory_Official__c = objcont.id;
        objrev.BRS_State_Reviewer_Email__c = objcont.Email;  
        objrev.No_Requirements__c=true;
        objrev.Requirement_Desc__c='Test Requirements';
        insert objrev;
        PageReference pageRef = Page.CARPOL_BRS_StateReviewRecords;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objrev);
        ApexPages.currentPage().getParameters().put('ID',objrev.id);
        
        CARPOL_BRS_StateReviewEditRecords extclass = new CARPOL_BRS_StateReviewEditRecords(sc);
        extclass.saveAndCongrat();
        //extclass.deleteRecord();
        //  extclass.getRelatedConditions();
        //  extclass.getRelatedStateRequirements();
        system.assert(extclass != null);     
        Test.stopTest(); 
    }
    
    @isTest
    static void testStateReqtTrue(){
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        //Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac1 = testData.newLineItem('Personal Use',objapp);
        //AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
        //AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        ac1.Authorization__c = objauth.id;
        ac1.status__c = 'Waiting on Customer';
        //ac1.RecordTypeId = ACLIRecordTypeId;        
        //FIXME: THE FOLLOWING LINE CAUSES SPRINGCM CLASS ERROR.
        //update ac1;
        
        Test.startTest();
        
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        //Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        //Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        // FIXME: CANNOT CREATE A REVIEWER DUE TO SPRINGCM ERRORS.
        Reviewer__c objrev = new Reviewer__c();
        objrev.Status__c= 'Open';
        objrev.Authorization__c = objauth.id;
        objrev.State_Regulatory_Official__c = objcont.id;
        objrev.BRS_State_Reviewer_Email__c = objcont.Email;  
        objrev.State_has_no_comments__c = true;
        objrev.State_Comments__c ='Test state requirements';
        insert objrev;
        
        PageReference pageRef = Page.CARPOL_BRS_StateReviewRecords;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objrev);
        ApexPages.currentPage().getParameters().put('ID',objrev.id);
        
        CARPOL_BRS_StateReviewEditRecords extclass = new CARPOL_BRS_StateReviewEditRecords(sc);
        extclass.saveAndCongrat();
        //extclass.deleteRecord();
        //   extclass.getRelatedConditions();
        // extclass.getRelatedStateRequirements();
      system.assert(extclass != null);     
        Test.stopTest(); 
    }
    
	@isTest
    static void testdeleteRecord(){
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        //Account objacct = testData.newAccount(AccountRecordTypeId); 
        //Contact objcont = testData.newcontact();
        //breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        //Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac1 = testData.newLineItem('Personal Use',objapp);
        //   AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
        //   AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac1.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        ac1.Authorization__c = objauth.id;
        ac1.status__c = 'Waiting on Customer';
        //ac1.RecordTypeId = ACLIRecordTypeId;
        //FIXME: THE FOLLOWING LINE CAUSES SPRINGCM CLASS ERROR.
        //update ac1;
        
        
        Test.startTest();
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        //Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        //Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        // FIXME: CANNOT CREATE A REVIEWER DUE TO SPRINGCM ERRORS.
        Reviewer__c objrev = new Reviewer__c();
        objrev.Status__c= 'Open';
        objrev.Authorization__c = objauth.id;
        //objrev.State_Regulatory_Official__c = objcont.id;
        //objrev.BRS_State_Reviewer_Email__c = objcont.Email;  
        
        insert objrev;
        System.debug('***Del record id:'+ objrev.id);
        
        PageReference pageRef = Page.CARPOL_BRS_StateReviewRecords;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objrev);
        ApexPages.currentPage().getParameters().put('ID',objrev.id);
        
        CARPOL_BRS_StateReviewEditRecords extclass = new CARPOL_BRS_StateReviewEditRecords(sc);
        extclass.delrecid = ApexPages.currentPage().getParameters().get('ID');
        extclass.saveAndCongrat();
        try{
            extclass.deleteRecord(); 
        }
        catch(Exception e){
            System.debug('*** exception on delete record:'+ e.getMessage());
        }
        //   extclass.getRelatedConditions();
        // extclass.getRelatedStateRequirements();
        system.assert(extclass != null);     
        Test.stopTest(); 
        
    }
}