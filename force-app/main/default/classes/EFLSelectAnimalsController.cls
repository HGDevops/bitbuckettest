public class EFLSelectAnimalsController {
    @auraEnabled
    public static List<String> insertStdAnimals(string annRepId,List<String> selectedAnimalList){
        //system.debug('selectedAnimalList: ' + selectedAnimalList);
        List<String> returnValue = new List<String>();
        list<EFLRegistered_Animal__c> anmlsList = new list<EFLRegistered_Animal__c>();  
        List<EFLRegistered_Animal__c> otherAnimalsToDelete = new List<EFLRegistered_Animal__c>();
        anmlsList = [Select ID,EFLSelectedForReport__c,EFLOtherAnimal__c,EFLSequence__c,EFLAnimal__c FROM EFLRegistered_Animal__c 
                     WHERE EFLAnnual_Report__c=:annRepId];        
        list<eflAnimal__c> stdAnimals = new list<eflAnimal__c>();        
        stdAnimals = [select id, name from eflanimal__c where other_animal__c = false];
        map<string,id> mapStdAnmls = new map<string,id>();
        for (eflAnimal__c sa:stdAnimals){
            mapStdAnmls.put(sa.name,sa.id);
        }
        if(anmlsList.size() == 0){
            string[] animals = new string[] {'Dogs','Cats','Guinea Pigs','Hamsters','Rabbits','Non-Human Primates','Sheep','Pigs','Other Animals'};
                for(integer i=0;i<animals.size();i++){
                    EFLRegistered_Animal__c anml = new EFLRegistered_Animal__c();
                    anml.EFLAnimal__c=animals[i];
                    anml.EFLAnnual_Report__c = annRepId;
                    anml.EFLSequence__c = i+4;
                    if(selectedAnimalList.contains(animals[i])){
                        anml.EFLSelectedForReport__c = true;
                    }
                    if(animals[i] == 'Other Animals'){
                        anml.EFLOtherAnimal__c = true;
                    }
                    anmlsList.add(anml);
                }        
        } else{
            // refactored by JB 7/2019.
            // added check for OtherAnimal = tru as top-level check.
            // added comments into blocks to describe intention.
            for(EFLRegistered_Animal__c savedAnimal : anmlsList){
                if(savedAnimal.EFLOtherAnimal__c == true){
                    // if other animals was previously selected but now is not, 
                    // remove all other animals from list.
                    if(!selectedAnimalList.contains('Other Animals')){
                        savedAnimal.EFLSelectedForReport__c = false;
                        savedAnimal.EFLColumnBHeldNotUsed__c = null;
                        savedAnimal.EFLColumnCUsedPainMinimized__c = null;
                        savedAnimal.EFLColumnDUsedPainMinimized__c = null;
                        savedAnimal.ELFColumnEPainNotMinimized__c = null;
                        if(savedAnimal.EFLAnimal__c != 'Other Animals')
                        {
                            otherAnimalsToDelete.add(savedAnimal);
                        }
                    }else{
                        savedAnimal.EFLSelectedForReport__c = true;
                    }
                }else{
                    if(selectedAnimalList.contains(savedAnimal.EFLAnimal__c)){
                        // if the Animal was previously selected and is now not, then remove it from the list.
                        savedAnimal.EFLSelectedForReport__c = true;
                    }else{
                        system.debug('setting to false because: selectedAnimalList.contains ' + savedAnimal.EFLAnimal__c + ' ' + selectedAnimalList.contains(savedAnimal.EFLAnimal__c));
                        savedAnimal.EFLSelectedForReport__c = false;
                        savedAnimal.EFLColumnBHeldNotUsed__c = null;
                        savedAnimal.EFLColumnCUsedPainMinimized__c = null;
                        savedAnimal.EFLColumnDUsedPainMinimized__c = null;
                        savedAnimal.ELFColumnEPainNotMinimized__c = null;
                    }
                }
            }
        }
        for (EFLRegistered_Animal__c aa: anmlsList){
             if(mapStdAnmls.containsKey(aa.EFLAnimal__c)){                        
                //system.debug('####  mapStdAnmls id = '+mapStdAnmls.get(aa.EFLAnimal__c));
                aa.EFLAnimalName__c = mapStdAnmls.get(aa.EFLAnimal__c);
           }            
        }
        upsert anmlsList;
        delete otherAnimalsToDelete;
        
        Integer counter = 0;
        list<EFLRegistered_Animal__c> savedAnimalList = [Select ID,EFLSelectedForReport__c, EFLAnimal__c, EFLOtherAnimal__c 
                                                   FROM EFLRegistered_Animal__c WHERE EFLAnnual_Report__c=:annRepId AND EFLSelectedForReport__c = TRUE];
        if(savedAnimalList.isEmpty()){
            returnValue.add('showError');
        }
        
        String parentRegId = [Select EFLRegistration__c FROM EFLAnnual_Report__c WHERE ID=:annRepId].EFLRegistration__c;
        returnValue.add(annRepId);
        return returnValue;
    }
    @auraEnabled
    public static existingAnimalWrapper lookForExistingAnimals(string annRepId){
        List<String> selectedAnimalList = new List<String>();
        list<EFLRegistered_Animal__c> anmlsList = new list<EFLRegistered_Animal__c>();  
        anmlsList = [Select ID,EFLSelectedForReport__c,EFLSequence__c,EFLAnimal__c FROM EFLRegistered_Animal__c 
                     WHERE EFLAnnual_Report__c=:annRepId];
        Integer totalAnimals = anmlsList.size();
        for(EFLRegistered_Animal__c existingAnimal : anmlsList){
            if(existingAnimal.EFLSelectedForReport__c){
                if(existingAnimal.EFLAnimal__c == null)
					selectedAnimalList.add('');
                else
                	selectedAnimalList.add(existingAnimal.EFLAnimal__c);
            }
        }
        existingAnimalWrapper returnValue = new existingAnimalWrapper();
        returnValue.selectedAnimals = selectedAnimalList;
        returnValue.totalAnimals = totalAnimals;
        
        return returnValue;
    }
    
    public class existingAnimalWrapper{
        @AuraEnabled
        public List<String> selectedAnimals{get;set;}
        @AuraEnabled
        public Integer totalAnimals{get;set;}
    }
}