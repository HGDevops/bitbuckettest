public with sharing class EFLBRSInspectionEngine 
    implements EFLIInspectionEngine {
    public static Map<id,List<EFL_Inspection_Questionnaire_Questions__c>> mapTempIdToInspecId = new Map<id,List<EFL_Inspection_Questionnaire_Questions__c>>();
            
  
/*
* Purpose: load Activities 
*/
    public void loadActivities(inspection__c inspectionRecord,list<sObject> activityList){
                
        //Populate Next Tasks and increment Sequence of inspection
        
        list<team__c> teamMembersList = new list<team__c>();
        
        teamMembersList = EFLTeamRepository.selectByInspectionID(inspectionRecord.ID); 
        ID ownerID;
        
        system.debug('team member list: ' + teamMembersList);
                
        if(!teamMembersList.isEmpty()){
            system.debug('inspectionRecord '+ inspectionRecord);
            integer taskSequence = inspectionRecord.Activity_Sequence__c==null?1:inspectionRecord.Activity_Sequence__c.intvalue();
            system.debug('taskSequence '+ taskSequence);
           // Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(inspectionRecord.stage__c, inspectionRecord.Workflow_Number__c,taskSequence + 1);
            Program_Activity__mdt currentActivityCMD;
                 //Rajesh Commented
   /*  if(inspectionRecord.stage__c =='Inspection Report' && inspectionRecord.Certified__c == true){
              currentActivityCMD = EFLActivityUtility.getProgramActivity(inspectionRecord.stage__c, inspectionRecord.Workflow_Number__c,2);
              } else {*/
                 currentActivityCMD = EFLActivityUtility.getProgramActivity(inspectionRecord.stage__c, inspectionRecord.Workflow_Number__c,taskSequence + 1);

              // } rajesh uncommented

            system.debug('currentActivityCMD '+ currentActivityCMD);
               
                if(currentActivityCMD != NULL )
                {                
                for(team__c t : teamMembersList){
                        system.debug('EachTeamMember *** ' + t);
                    if(currentActivityCMD.Assigned_To__c == t.member_role__c){
                        system.debug('EachTeamMember Inside Loop *** ' + t);
                        ownerID = t.Member__c;
                        break;
                    }
                } 
                if(ownerID == null){ 
                    
                    trigger.new[0].addError('Please assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
                }
                activityList.add(EFLActivityUtility.populateTask( inspectionRecord, currentActivityCMD, ownerID ));
                system.debug('Before Activity_Sequence__c '+inspectionRecord.Activity_Sequence__c);
                inspectionRecord.Activity_Sequence__c = inspectionRecord.Activity_Sequence__c + 1;
                system.debug('After Activity_Sequence__c '+inspectionRecord.Activity_Sequence__c);                
            }       
        }
        
        else{
               trigger.new[0].addError('Please assign Team Members.');            
        }
    }  
    /*
* Purpose: load Question on inspection object
*/
    public static void getQuestion(Set<id> InspecTempId){
        if(mapTempIdToInspecId.isEmpty() ){
        for(EFL_INS_Template_Question_Junction__c tmp : [Select id,EFL_Inspection_Questions_Template__c,Inspection_Template_Questions__c,Inspection_Template_Questions__r.Options__c,
                                                        Inspection_Template_Questions__r.Question__c,Inspection_Template_Questions__r.id from EFL_INS_Template_Question_Junction__c where EFL_Inspection_Questions_Template__c
                                                        In: InspecTempId]){
            if(mapTempIdToInspecId.containsKey(tmp.EFL_Inspection_Questions_Template__c)){
                mapTempIdToInspecId.get(tmp.EFL_Inspection_Questions_Template__c).add(new EFL_Inspection_Questionnaire_Questions__c(Source_Question__c = tmp.Inspection_Template_Questions__r.id,Question__c = tmp.Inspection_Template_Questions__r.Question__c,
            Options__c = tmp.Inspection_Template_Questions__r.Options__c));
            }
            else{
                mapTempIdToInspecId.put(tmp.EFL_Inspection_Questions_Template__c,new List<EFL_Inspection_Questionnaire_Questions__c>{new EFL_Inspection_Questionnaire_Questions__c(Source_Question__c = tmp.Inspection_Template_Questions__r.id,Question__c = tmp.Inspection_Template_Questions__r.Question__c,
            Options__c = tmp.Inspection_Template_Questions__r.Options__c)});
            
            }
              system.debug('mapTempIdToInspecId++'+mapTempIdToInspecId);     
              system.debug('InspecTempId'+InspecTempId);                                         
            
        }
        }
    }
/*
* Purpose: load Question on inspection object
*/
    public void loadQuestion(inspection__c inspectionRecord,list<sObject> questionList){
        system.debug('inspectionRecord.EFL_Inspection_Questionnaire_Template__c'+inspectionRecord.EFL_Inspection_Questionnaire_Template__c); 
        system.debug('inspectionRecord'+inspectionRecord); 
        system.debug('mapTempIdToInspecId++11'+mapTempIdToInspecId); 
        Integer count = 0;
        if(inspectionRecord.EFL_Inspection_Questionnaire_Template__c != null && mapTempIdToInspecId.containsKey(inspectionRecord.EFL_Inspection_Questionnaire_Template__c)){
            for(EFL_Inspection_Questionnaire_Questions__c insQu : mapTempIdToInspecId.get(inspectionRecord.EFL_Inspection_Questionnaire_Template__c)){
                insQu.Inspection__c = inspectionRecord.id;
                // W-035910 - Rajesh Potla
                if(Trigger.IsExecuting && Trigger.isInsert){
                    insQu.order__c = ++count;
                }
                questionList.add(insQu);
            }
        }
        system.debug('questionList'+questionList); 
        
    }   
}