public class EFLAR_CustomLookupUtil extends EFLCustomLookupUtil {
    
    public override list<EFLCustomLookupController.LookupItem> queryData(string searchText){
        string srch = string.escapeSingleQuotes(searchText);
        
        String sQuery =  'SELECT id, Name, Species_Name__c, Guidance_Text__c from ' +sObjectToSearch;
        
        sQuery += getWhereClause(this.whereFields, searchText);
        
        // this is a unique where statement
        if(this.queryFilter != null){
            sQuery += ' AND ' + this.queryFilter;
        }

        if(this.sortOrder != null){
        	sQuery += ' ORDER BY ' + this.sortOrder;
        }
        if(this.searchLimit != null){
            sQuery += '  LIMIT ' + searchLimit;
        }
        
        system.debug('sQuery: ' + sQuery);
        
        list<EFLCustomLookupController.LookupItem> returnList = new list<EFLCustomLookupController.LookupItem>();
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        for (sObject obj: lstOfRecords) {
            string name = (string)obj.get('Name');
            string helpText;
            if(obj.get('Species_Name__c') != null)
                helpText = (string)obj.get('Species_Name__c');
            if(obj.get('Guidance_Text__c') != null){
                name += ' (' + (string)obj.get('Guidance_Text__c') + ')';
            }
            returnList.add(new EFLCustomLookupController.LookupItem(name, obj.Id, helpText));
        }
        system.debug('return list: ' + returnList);
        return returnList;
    }
}