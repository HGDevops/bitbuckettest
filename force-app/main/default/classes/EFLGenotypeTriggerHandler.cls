public with sharing class EFLGenotypeTriggerHandler {
    
    //W-027020 - Transaction Variable
    Public static Boolean deleteParentAndChildOrderRecurrence = false;
    
    //W-027020 - Validations
    public static void ValidateRequiredFields(List<Genotype__c> genotypes){
        
        for(Genotype__c genoRec:genotypes)
        {
            //Commenting for W-026999
            //if(genoRec.recordtypeid  ==NULL)
            //{genoRec.recordtypeid.adderror('Genotype Category is required.');} 
            
            if(genoRec.GenotypeType__c  ==NULL){
            //if(genoRec.GenotypeType__c  ==NULL && UserInfo.getUserType() != 'CSPLitePortal') {
                genoRec.GenotypeType__c.adderror('You must enter a value.');
            }
            
            if(genoRec.Construct_Component__c  =='' || genoRec.Construct_Component__c  ==NULL) {
                genoRec.Construct_Component__c.adderror('You must enter a value.');
            }
            if(genoRec.Description__c == '' || genoRec.Description__c  ==NULL)
            {genoRec.Description__c.adderror('You must enter a value.');}  
            
            if(genoRec.Donor_List__c  =='' || genoRec.Donor_List__c  ==NULL) {
                genoRec.Donor_List__c.adderror('You must enter a value.');
            }
            if(genoRec.Construct_Component_Name__c == '' || genoRec.Construct_Component_Name__c  ==NULL)
            {genoRec.Construct_Component_Name__c.adderror('You must enter a value.');}  
            
        }  
    }
    
    //W-027020 - SortOrder Evaluation OnInsert
    public static void SortOrderEvaluationOnInsert(List<Genotype__c> genotypes){
        map<id, GenotypeType__c> parentGenoTypes = new map<id,GenotypeType__c>();
        List<id> parentGenoTypeIDs = new List<id>();
        for(Genotype__c genoRec:genotypes)
        {
            if(genoRec.GenotypeType__c!=null)
            {
            	parentGenoTypeIDs.add(genoRec.GenotypeType__c);
            }
        }
        if(!parentGenoTypeIDs.isEmpty())
        {
        Map<Id,AggregateResult> aggregateResultsMap = new Map<id,AggregateResult>([SELECT GenotypeType__c Id, COUNT(Id) FROM Genotype__c WHERE GenotypeType__c in :parentGenoTypeIDs GROUP BY GenotypeType__c]);  
        Map<id, Integer> parentChildRunningCountMap =  new Map<id, Integer>();
        for(Genotype__c genoRec:genotypes)
        {
            if(genoRec.GenotypeType__c!=null)
            {
                AggregateResult recordCountAgg = aggregateResultsMap.get(genoRec.GenotypeType__c);  
                object recordCountObj; 
                if(recordCountAgg!=null)
                {
                 recordCountObj = recordCountAgg.get('expr0');
                }
                if(recordCountObj!=null)
                {
                    integer recordCount = (Integer)recordCountObj;
                    if(parentChildRunningCountMap.get(genoRec.GenotypeType__c)!=null)
                    {
                        parentChildRunningCountMap.put(genoRec.GenotypeType__c, parentChildRunningCountMap.get(genoRec.GenotypeType__c) + 1);
                    }
                    else
                    {
                        parentChildRunningCountMap.put(genoRec.GenotypeType__c, recordCount + 1);   
                    }
                    //genoRec.Construct_Component_Sort_Order__c = recordCount + 1;
                    genoRec.Construct_Component_Sort_Order__c = parentChildRunningCountMap.get(genoRec.GenotypeType__c);
                    //parentGenoTypes.add(new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c=recordCount + 1));
                	//parentGenoTypes.put(genoRec.GenotypeType__c, new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c=recordCount + 1));
                    parentGenoTypes.put(genoRec.GenotypeType__c, new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c=parentChildRunningCountMap.get(genoRec.GenotypeType__c)));
                }
                else
                {
                    if(parentChildRunningCountMap.get(genoRec.GenotypeType__c)!=null)
                    {
                        parentChildRunningCountMap.put(genoRec.GenotypeType__c, parentChildRunningCountMap.get(genoRec.GenotypeType__c) + 1);
                    }
                    else
                    {
                        parentChildRunningCountMap.put(genoRec.GenotypeType__c,  1);   
                    }
                    //genoRec.Construct_Component_Sort_Order__c = 1;   
                    genoRec.Construct_Component_Sort_Order__c = parentChildRunningCountMap.get(genoRec.GenotypeType__c);   
                	//parentGenoTypes.add(new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c= 1));
                    //parentGenoTypes.put(genoRec.GenotypeType__c, new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c= 1));
            	    parentGenoTypes.put(genoRec.GenotypeType__c, new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c= parentChildRunningCountMap.get(genoRec.GenotypeType__c)));
                }
            }
        }
        if(!parentGenoTypes.values().isEmpty())
        {
          update parentGenoTypes.values();  
        }
    }  
    }
    
    /*public static void SortOrderEvaluationOnUpdate(List<Genotype__c> newGenotypes, List<Genotype__c> oldGenotypes){
      if(!SortOrderEvaluationOnUpdateRecurrence)
        {
            Map<id, Map<id, Genotype__c>> affectedParentAffectedChildrenMap = new Map<id, Map<id, Genotype__c>>();
            for(integer i = 0; i<newGenotypes.size(); i++){
                if(newGenotypes[i].Construct_Component_Sort_Order__c!=oldGenotypes[i].Construct_Component_Sort_Order__c)
                {
                    if(affectedParentAffectedChildrenMap.get(newGenotypes[i].GenotypeType__c)==null)  
                    {
                        Map<Id, Genotype__c> childrenMap = new Map<Id, Genotype__c>();
                        childrenMap.put(newGenotypes[i].id, newGenotypes[i]);
                        affectedParentAffectedChildrenMap.put(newGenotypes[i].GenotypeType__c, childrenMap);   
                    }
                    else
                    {
                        Map<Id, Genotype__c> childrenMap = new Map<Id, Genotype__c>();
                        childrenMap = affectedParentAffectedChildrenMap.get(newGenotypes[i].GenotypeType__c);
                        childrenMap.put(newGenotypes[i].id, newGenotypes[i]);
                        affectedParentAffectedChildrenMap.put(newGenotypes[i].GenotypeType__c, childrenMap);
                    }
                }
            }
            Map<Id, Map<Id, Genotype__c>> affectedParentAllChildrenMap = new Map<Id, Map<Id, Genotype__c>>();
			SortOrderEvaluationOnUpdateRecurrence = true;
        }  
        
    }*/
    
    //W-027020 - Delete
    public static void DeleteParentAndChildOrder(List<Genotype__c> genotypes){
        
        if(!deleteParentAndChildOrderRecurrence)
        {
            Map<id, Map<id, Genotype__c>> affectedParentAffectedChildrenMap = new Map<id, Map<id, Genotype__c>>();
            set<id> affectedchildrenIds = new set<id>();
            //Get Affected Geno and Child Comp
            for(integer i = 0; i<genotypes.size(); i++){
                if(affectedParentAffectedChildrenMap.get(genotypes[i].GenotypeType__c)==null)  
                {
                    Map<Id, Genotype__c> childrenMap = new Map<Id, Genotype__c>();
                    Genotype__c gt = new Genotype__c();
                    gt = genotypes[i];
                    childrenMap.put(genotypes[i].id, gt);
                    affectedchildrenIds.add(genotypes[i].id);
                    affectedParentAffectedChildrenMap.put(genotypes[i].GenotypeType__c, childrenMap);   
                }
                else
                {
                    Map<Id, Genotype__c> childrenMap = new Map<Id, Genotype__c>();
                    childrenMap = affectedParentAffectedChildrenMap.get(genotypes[i].GenotypeType__c);
                    Genotype__c gt = new Genotype__c();
                    gt = genotypes[i];
                    childrenMap.put(genotypes[i].id, gt);
                    affectedchildrenIds.add(genotypes[i].id);
                    affectedParentAffectedChildrenMap.put(genotypes[i].GenotypeType__c, childrenMap);
                }
            }
            //Get Affected Geno and All Child Comp
            List<Genotype__c> genoTypeList = new List<Genotype__c>();
            genoTypeList = [select Id, Name, Construct_Component_Sort_Order__c, GenotypeType__c from Genotype__c where GenotypeType__c in :affectedParentAffectedChildrenMap.keySet() and Id not in :affectedchildrenIds order by GenotypeType__c, Construct_Component_Sort_Order__c ASC];
            Map<Id, Map<Id, Genotype__c>> affectedParentAllChildrenMap = new Map<Id, Map<Id, Genotype__c>>();
            for(integer i = 0; i<genoTypeList.size(); i++){
             	if(affectedParentAllChildrenMap.get(genoTypeList[i].GenotypeType__c)==null)  
                {
                    Map<Id, Genotype__c> childrenMap = new Map<Id, Genotype__c>(); 
                    Genotype__c gt = new Genotype__c();
                    gt = genoTypeList[i];
                    childrenMap.put(genoTypeList[i].id, gt);
                    affectedParentAllChildrenMap.put(genoTypeList[i].GenotypeType__c, childrenMap);   
                }
                else
                {
                    Map<Id, Genotype__c> childrenMap = new Map<Id, Genotype__c>();
                    childrenMap = affectedParentAllChildrenMap.get(genoTypeList[i].GenotypeType__c);
                    Genotype__c gt = new Genotype__c();
                    gt = genoTypeList[i];
                    childrenMap.put(genoTypeList[i].id, gt);
                    affectedParentAllChildrenMap.put(genoTypeList[i].GenotypeType__c, childrenMap);
                }
            }
            
            
            List<GenotypeType__c> parentGenoTypeDeleteList = new List<GenotypeType__c>();
            List<Genotype__c> childGenoTypeUpdateList = new List<Genotype__c>();
            List<GenotypeType__c> parentGenoTypes = new List<GenotypeType__c>();
        	//Process Parent Count Field Update
        	//Process Parent Delete on last child delete
        	//Process Child update for re-ordering all siblings
            for(id key: affectedParentAffectedChildrenMap.keySet())
            {
            	if(affectedParentAllChildrenMap.get(key)==null)
                {
                	parentGenoTypeDeleteList.add(new GenotypeType__c(id=key));
                }
                else
                {
                  Map<Id, Genotype__c> affectedChildrenMap = new Map<Id, Genotype__c>();
                  affectedChildrenMap = affectedParentAffectedChildrenMap.get(key);  
                  Map<Id, Genotype__c> allChildrenMap = new Map<Id, Genotype__c>();
                  allChildrenMap = affectedParentAllChildrenMap.get(key);  
                  for(id childKey : affectedChildrenMap.keySet())
                  {
                    double affectedSortOrderNumber = affectedChildrenMap.get(childKey).Construct_Component_Sort_Order__c;
                    for(id allChildKey : allChildrenMap.keySet())
                    {
                     double currentSortOrderNumber = allChildrenMap.get(allChildKey).Construct_Component_Sort_Order__c;
                     if(currentSortOrderNumber>affectedSortOrderNumber)
                     {
                      allChildrenMap.get(allChildKey).Construct_Component_Sort_Order__c = allChildrenMap.get(allChildKey).Construct_Component_Sort_Order__c - 1;   
                     }
                     /*if(allChildKey==childKey) 
                     {
                       allChildrenMap.remove(allChildKey);  
                     }*/
                    }
                  }
                  affectedParentAllChildrenMap.put(key, allChildrenMap); 
                  parentGenoTypes.add(new GenotypeType__c(id=key, Count_of_Construct_Components__c= allChildrenMap.values().size()));  
                }
            }
            
            for(id key:affectedParentAllChildrenMap.keySet())
            {
              List<Genotype__c> gts = new List<Genotype__c>();
              gts = affectedParentAllChildrenMap.get(key).values();
              childGenoTypeUpdateList.addall(gts);  
            }
            
            if(!childGenoTypeUpdateList.isEmpty())
            {
               //system.debug('childGenoTypeUpdateList###'+ childGenoTypeUpdateList); 
               update childGenoTypeUpdateList; 
               //database.update(childGenoTypeUpdateList, false);
            }
            
            if(!parentGenoTypes.isEmpty())
            {
               update parentGenoTypes;
               //database.update(parentGenoTypes, false);
            }
            
            if(!parentGenoTypeDeleteList.isEmpty())
            {
                delete parentGenoTypeDeleteList;
            }
            
            /*//parentGenoTypes.add(new GenotypeType__c(id=genoRec.GenotypeType__c, Count_of_Construct_Components__c= 1));
            if(!parentGenoTypes.isEmpty())
            {
            update parentGenoTypes;  
            }*/
            deleteParentAndChildOrderRecurrence = true;
            
        }  
        
        
    }
    
    //Set "Ready to Submit" flag for Genotypes when a new Construct Component is created
     public static void processGenotypeReadiness(List<Genotype__c> constructComponentList){
         list<GenotypeType__c> genotypeList = new list<GenotypeType__c>();
         set<ID> genotypeSetIDs =  prepareGenotypeSetIds(constructComponentList);
         list<GenotypeType__c> genotypeListToCheck = getGenotypesToBeProcessed(genotypeSetIDs);
         for(GenotypeType__c geno:genotypeListToCheck){
             if(geno.Ready_to_Submit__c != true)
             {
                 geno.Ready_to_Submit__c= true;
                 genotypeList.add(geno);
             }
         }
         if(genotypeList!=NULL){
             update genotypeList;  }                
      }
    
     //Reset "Ready to Submit" flag for Genotypes when a Construct Components are deleted
     public static void resetGenotypeReadiness(List<Genotype__c> constructComponentList){
         list<GenotypeType__c> genotypeList = new list<GenotypeType__c>();
         set<ID> genotypeSetIDs =  prepareGenotypeSetIds(constructComponentList);
         list<GenotypeType__c> genotypeListToCheck = getGenotypesToBeProcessed(genotypeSetIDs);
         for(GenotypeType__c geno:genotypeListToCheck){
            if(geno.Genotypes__r.size() == 0 && geno.Ready_to_Submit__c != false){
                geno.Ready_to_Submit__c= false;
                genotypeList.add(geno); 
            }
        }
         if(genotypeList!=NULL){
            Update genotypeList;  }                 
      }
    
    //Prepare genotypes to be processed for flag update
    private static list<GenotypeType__c> getGenotypesToBeProcessed(set<ID> genotypeSetIDs){
        return[select Id, 
                      Ready_to_Submit__c,
               (select id 
                from Genotypes__r)
               from GenotypeType__c
               where Id in :genotypeSetIDs FOR UPDATE]; 
    } 
    
   //Prepare genotype set Ids from Construct Component List
    private static set<ID> prepareGenotypeSetIds (List<Genotype__c> constructComponentList){
         set<ID> genotypeSetIDs = new set<ID>();
         for(Genotype__c cc : constructComponentList){
            genotypeSetIDs.add(cc.GenotypeType__c); 
         }        
        return genotypeSetIDs;
    }
   
    //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
    public static void updateConstructCBIflag(List<Genotype__c> lstGenotype){
        set<id> idConstruct = new set<id>(); 
           	 for(Genotype__c gg : lstGenotype){
                 if (string.isNotBlank(gg.Description__c)){
                     string strDesc = gg.Description__c;
                     if( strDesc.indexOf('[') >  -1 && strDesc.indexOf(']') > 0 ){
                     	idConstruct.add(gg.Related_Construct_Record_Number__c);
                     }
                 }
                 if (string.isNotBlank(gg.Donor_List__c)){
                     string strDonor = gg.Donor_List__c;
                     if( strDonor.indexOf('[') >  -1 && strDonor.indexOf(']') > 0 ){
                     	idConstruct.add(gg.Related_Construct_Record_Number__c);
                     }
                 }
                 if (string.isNotBlank(gg.Construct_Component_Name__c)){
                     string strName = gg.Construct_Component_Name__c;
                     if( strName.indexOf('[') >  -1 && strName.indexOf(']') > 0 ){
                     	idConstruct.add(gg.Related_Construct_Record_Number__c);
                     }
                 }
                 
         	} 
        	list<Construct__c> lstConstructCBI = new list<Construct__c>();
            if (idConstruct.size()> 0 ){
                 lstConstructCBI = [select id, Construct_is_CBI__c from construct__C where 
                                                     id in :idConstruct];   
            }
            if (lstConstructCBI.size()> 0 )	{
                for (construct__C c :lstConstructCBI){
                    c.Construct_is_CBI__c = True; 
                    system.debug ('GenoType Construct is CBI #### ' +   c.Construct_is_CBI__c);
                }
                
                update lstConstructCBI;
            }
    }
   
    //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
    
    //Reset "Ready to Submit" flag for Constructs when a Genotype and Construct Components are deleted
     public static void resetConstructReadiness(List<Genotype__c> constructComponentList){ 
         list<Construct__c> constructList = new list<Construct__c>();
         set<ID> constructSetIDs =  prepareConstructSetIds(constructComponentList);
         list<Construct__c> relatedconstructList = getIncompleteConstructs(constructSetIDs);
        for(Construct__c cons:relatedconstructList){
            if(cons.GenotypeType__r.size() == 0 && cons.PhenoTypes__r.size()==0){
                cons.Ready_to_Submit__c= false;
                constructList.add(cons); 
            }else{
                cons.Ready_to_Submit__c= true;
                constructList.add(cons); 
            }
        }
         if(constructList!=NULL){
            Update constructList;  }                
      }
   
    //Prepare incomplete constructs to be processed for flag update
    private static list<Construct__c> getIncompleteConstructs(set<ID> constructSetIDs){
        return[select Id, 
                      Ready_to_Submit__c,
               (select id 
                  from GenotypeType__r
                where Ready_to_Submit__c =: false),
               (select id 
                  from PhenoTypes__r)
               from Construct__c
                where Id in :constructSetIDs]; 
    }   
    
    //Prepare Construct set Ids from phenotype List
    private static set<ID> prepareConstructSetIds (List<Genotype__c> constructComponentList){
         set<ID> constructSetIDs = new set<ID>();
         for(Genotype__c cc : constructComponentList){
            constructSetIDs.add(cc.GenotypeType__r.Construct__c); 
         }        
        return constructSetIDs;
    }
}