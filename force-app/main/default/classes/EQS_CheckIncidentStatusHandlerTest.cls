@isTest(seealldata=false)
private class EQS_CheckIncidentStatusHandlerTest
{
    
    /*static testmethod void test() 
    {
      Test.startTest();
        EQSPrepareTestData1();   
        Contact[] tmpResponder = [Select Id,EQS_Responder_Status__c From Contact Where FirstName='Responder1'];
        System.assertEquals(tmpResponder.size(), 1);
        System.debug('-->'+tmpResponder[0].EQS_Responder_Status__c+''); 
               
        
      Test.stopTest();
        

    }*/
   
   static testMethod void test() 
   {
      Test.startTest();
      String EQSAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('APHIS EQS Organization').getRecordTypeId();
      String EQSIncidentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('APHIS EQS Incident').getRecordTypeId();
      String EQSResponderRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('APHIS EQS Responder').getRecordTypeId();
      String EQSRFRRecordTypeId = Schema.SObjectType.EQS_Resource_Request__c.getRecordTypeInfosByName().get('Resource Fill Request - Dispatcher Initiated').getRecordTypeId();
      
      Case EQSIncident=new Case(RecordTypeId=EQSIncidentRecordTypeId,Subject='HPAI INDIANA 01 2017-Test');
      EQSIncident.EQS_Category__c='ANIMAL HEALTH';
      EQSIncident.Priority='Medium';
      EQSIncident.Status='Open';
      insert EQSIncident;
      Account Org1=new Account(RecordTypeId=EQSAccountRecordTypeId,Name='ADMIN, FAC & FLEET MGMT',EQS_Org_Abbreviation__c='AF&FM',EQS_Org_Code__c='05');
      insert Org1;
      Contact Responder1=new Contact(RecordTypeId=EQSResponderRecordTypeId);
      Responder1.FirstName = 'Responder1';
      Responder1.LastName = 'Test1';
      Responder1.Email = 'Responder1.Test1@gmail.com';
      Responder1.EQS_Responder_Status__c='Available';        
      Responder1.AccountId = Org1.Id;
      insert Responder1;
      System.debug('Responder1:>'+Responder1);
      
      EQS_Position_Cert__c PosiCert=new EQS_Position_Cert__c(Name='Liaison Officer (Aghealth)',EQS_Code__c='A060',EQS_Position_Title__c='Liaison Officer (Aghealth)');
      insert PosiCert;
      EQS_Incident_Position_Certification__c IPos=new EQS_Incident_Position_Certification__c(EQS_Incident__c=EQSIncident.Id,EQS_Qty__c=2,EQS_Position__c=PosiCert.Id);
      insert IPos;
      System.debug('IPos:>'+IPos);
      
      //date MobDate=date.newInstance(2016, 11, 14);
      //date DeMobDate=date.newInstance(2016, 11, 30);
      date MobDate=date.today();
      MobDate=MobDate.addDays(-7);
      date DeMobDate=MobDate.addDays(7);
      
      EQS_Resource_Request__c RFR1=new EQS_Resource_Request__c(RecordTypeId=EQSRFRRecordTypeId,EQS_Responder__c=Responder1.Id,EQS_Position__c=IPos.Id,EQS_Type__c='Requested',EQS_Status__c='Final Approval',EQS_Rotation_Begin_Date__c=MobDate,EQS_Rotation_End_Date__c=DeMobDate);
      insert RFR1;
      System.debug('RFR1:>'+RFR1);
      
      Contact tmpResponder = [Select Id,EQS_Responder_Status__c From Contact Where id=:Responder1.id];
      //System.assertEquals(tmpResponder.size(), 1);
      System.assertEquals(tmpResponder.EQS_Responder_Status__c+'','Available');
      
      try
       {
          Case UPDEQSIncident=new Case(id=EQSIncident.id,Status='Closed');
          Update UPDEQSIncident;           
          //Case[] FinalEQSIncident=[Select Id,Status From Case Where id=:UPDEQSIncident.id];
          //System.assertEquals(FinalEQSIncident[0].Status+'','Open');
       }
      catch(Exception e) {
         System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
         //System.Assert(e.getMessage().contains(UPDEQSIncident.id));
         
       }
      
      
      
      Test.stopTest();
      
    }  
   
}