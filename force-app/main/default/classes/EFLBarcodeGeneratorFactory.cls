public class EFLBarcodeGeneratorFactory {
public static string getBarcode(string content, string codeType) {
        string retVal;
        EFLIBarcodeGenerator handler;
        EFLBarcodeData bcData = new EFLBarcodeData();
        bcData.inputData = content;
        
        
        EFLBarcodeType bcType = getBarcodeType(codeType);
        
        if(bcType != null)
        {            
            handler = getHandler(bcType);
            if(handler != null)
            {
                if(handler.validateInput(bcData))
                {
                    if(handler.encodeInput(bcData))
                    {
                        if(handler.generateBarcodeHTML(bcData))
                        {
                            retVal = bcData.barcodeHTML;
                        }
                        else { retVal = '<p> Error generatng barcode HTML</p>';}
                    }
                    else { retVal = '<p> Error encoding the content</p>';}
                }
                else { retVal = '<p> Invalid characters in the content</p>'; }
            }
        }
        else { retVal = '<p> Invalid code type. Please provide supported code type.</p>';        }
        
        return retVal;
    }
    
    private static EFLIBarcodeGenerator getHandler(EFLBarcodeType bcType)
    {
        if(bcType == EFLBarcodeType.CODE128)
        {
            return new EFLBarcodeGenerator_Code128();
        }
        else if(bcType == EFLBarcodeType.CODE39)
        {
            return new EFLBarcodeGenerator_Code39();
        }
        return null;
    }
    
    public static EFLBarcodeType getBarcodeType(string codeType)
    {
        EFLBarcodeType bcType;
        boolean matchFound = false;
        for(EFLBarcodeType bt: EFLBarcodeType.values())
        {
            if(bt.name() == codeType)
            {
                bcType = bt;
                matchFound= true;
                break;
            }
        }
       
        if(matchFound)
        {
            return bcType;
        }
        return null;
    }
}