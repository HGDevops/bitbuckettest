// Test Class for EFLInspQuestionnaireQnsTriggerHandler class
// Created By - Rajesh Potla 
// Date - 5/7/2019
@isTest
public class EFLInspQuestnrQnsTriggerHandler_Test {
   
    @testsetup
    static void setupData(){
        EFL_Inspection_Questions_Template__c inspTmpn = new EFL_Inspection_Questions_Template__c();
         inspTmpn.Name='test';
         insert inspTmpn;
        
        Inspection__c Insn = new Inspection__c();
        String IncRecTypeIDn = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Insn.RecordTypeId = IncRecTypeIDn;
        Insn.Stage__c = 'Inspection Assignment';
        Insn.status__c='Cancelled';
        Insn.Reason_for_Cancellation__c = 'testing';
        Insn.Program__c='BRS';
        Insn.Activity_Sequence__c = 0;
        Insn.EFL_Inspection_Questionnaire_Template__c=inspTmpn.id;
        insert Insn;
    }
    
    @isTest
    static void runTest(){
        Inspection__c oInspection = [Select Id From Inspection__c limit 1];
        EFL_Inspection_Questionnaire_Questions__c recEIQ=new EFL_Inspection_Questionnaire_Questions__c ();
        recEIQ.Question__c='test question1';
        recEIQ.Response__c = 'response1';
        recEIQ.Answer__c='answer';
        recEIQ.Comments__c ='ytest' ;
        recEIQ.Inspection__c=oInspection.id;
        recEIQ.Options__c='Finalize Questionnaire';
        
        List<EFL_Inspection_Questionnaire_Questions__c> toUpdateList = new List<EFL_Inspection_Questionnaire_Questions__c>();
        toUpdateList.add (new EFL_Inspection_Questionnaire_Questions__c(Inspection__c=oInspection.id,Question__c='test question1',Response__c = 'response1',Answer__c='answer',Options__c='Finalize Questionnaire',Comments__c ='c-test'));
        toUpdateList.add(recEIQ);
        
        EFLInspQuestionnaireQnsTriggerHandler oHandler = new EFLInspQuestionnaireQnsTriggerHandler();
        oHandler.isDisabled();
        oHandler.beforeInsert(recEIQ);
        oHandler.beforeUpdate(recEIQ, recEIQ);
        oHandler.beforeDelete(recEIQ);
        oHandler.afterInsert(recEIQ);
        oHandler.afterUpdate(recEIQ, recEIQ);
        oHandler.afterDelete(recEIQ);
        
        insert toUpdateList;
        update toUpdateList;
        delete toUpdateList;
       
    }
   

}