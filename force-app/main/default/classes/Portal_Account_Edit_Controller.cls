/*
*
*  Teh Liu 4/17/2018: Removed BillingAddress, ShippingAddress from SOQL because these are compound fields
*                     and these fields are not referenced anywhere
*/
public with sharing class Portal_Account_Edit_Controller {
    
    public Id accountID{get;set;}
    public Account acct{get;set;}
    public ApexPages.StandardController controller{get;set;}
    public String bAddress{get;set;}
    
    public Portal_Account_Edit_Controller(ApexPages.StandardController controller)
    {
        accountID=ApexPages.currentPage().getParameters().get('id');
        acct=(Account)controller.getRecord();
        controller = controller;
        
        if(accountID!=null){
            acct=(Account)controller.getRecord();
            acct = [Select Id, Name,
                    Recordtypeid, Phone, AnnualRevenue,NumberOfEmployees,Description, Industry, Type,
                    Website, fax, ParentId , DUNS_Number__c,Organization_Unique_ID__c ,RecordType.Name, OwnerId, Account.Owner.Name, Shipping_County__c, Billing_County__c, Billing_Township_Island__c, Shipping_Township_Island__c,
                    Other_Phone__c,  BillingCountryCode, billingStreet, BillingCity,
                    BillingStateCode, BillingPostalCode, ShippingCountryCode, ShippingStreet, ShippingCity,
                    ShippingStateCode, ShippingPostalCode
                    from Account where ID=:accountID];
            controller = controller;
            //bAddress = acct.BillingAddress;
        }
        else
        {
            acct = new Account();
        }
    }
    
    public PageReference Save()
    {
        upsert acct;
        PageReference pg= new PageReference ('/'+acct.Id);
        pg.setRedirect(true);
        return pg;
        
    }
    
    public PageReference getEditPage()
    {
        //String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        // system.debug('+++BaseUrl+++'+BaseUrl);
        //  PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Account_Edit?id='+accountID);
        PageReference dirpage= new PageReference('/apex/Portal_Account_Edit?id='+accountID);
        dirpage.setRedirect(true);
        return dirpage;
    }
    
}