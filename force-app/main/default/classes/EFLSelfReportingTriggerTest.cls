//Developer Name: Peter Tran
//Creation Date: 03/19/2019
//File Name: EFLSelfReportingTriggerTest.apxc
//sObject Affected: Self_Reporting__c
//Client: United States Department of Agriculture
//Client Project Name: APHIS CARPOL
//Contractor: Accenture Federal Services

    /*****************************
     *  Developer Documentation  *
     *****************************
     * _________________________
     * Trigger Test Description
     * 
     * Changelog
     * V1.0: Creation of EFLSelfReportingTriggerTest.apxt
     * - File created in DevShare on 03/19/2019
     *  
     */
 // FIXME: HARD CODING IDS IN UNIT TESTS IS A NO-NO.  JB.
 // FIXME: THIS UNIT TEST DOES NOT INSERT IN BULK CONTEXT
 // FIXME: 
@isTest
private class EFLSelfReportingTriggerTest {
    
    static Application__c app;
    static Authorizations__c auth;
    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static AC__c li;
    static Applicant_Attachments__c objaa;
    static Attachment objattach;
    static Id RtId;
    static Location__c loc;
    static Location__c loc1;
    static List<Id> locations = new List<Id>();
    static Map<Id, Location__c> locationsMap = new Map<Id, Location__c>();
    static Construct__c construct; 
    static Report_Summary__c rs;
    static Report_Summary__c rs1;
    static List<Report_Summary__c> rsList;
    static Self_Reporting__c sr;
    static Self_Reporting__c sr1;
    static Id prrectypeid;
    static EFL_Related_Record__c eflRelatedRec;
    
    @IsTest
    public static void init(){
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
      
        app = new Application__c();
        app = testData.newapplication();
        auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
        country = testDataBRS.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        li = new AC__c();
        li = testData.newLineItem('Resale/Adoption', app, auth);
       // li = testDataBRS.newLineItemByAppAuthPoiStatus(app,auth.id,'Resale/Adoption', 'Saved');

          
        RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId(); 
      
        loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
        loc1 = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
        loc.No_Planting_Flag__c = false;  
        loc1.No_Planting_Flag__c = true;  
        locations.add(loc.Id);
        locations.add(loc1.Id);
        construct = testDataBRS.newconstruct(li.id);
        construct.Authorization__c = auth.id;
        update construct;

        rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Volunteer Monitoring Report';
        rs.Status__c = 'UnSubmitted';
        insert rs;        
        
        prrectypeid = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
	    sr= new Self_Reporting__c();
        sr.Planting_ID__c=loc.id;
        sr.Authorization__c = auth.id;
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.RecordTypeId = prrectypeid;
        sr.Planting_ID__c=loc.id;
        sr.Quantity_Acres__c=1;
        sr.Start_Date__c= date.today();
        sr.Is_No_Planting__c = true;
        insert sr;
        sr.Is_No_Planting__c = false;
        update sr;
        //Insert self reporting with no planting true
        sr1= new Self_Reporting__c();
        sr1.Planting_ID__c=loc1.id;
        sr1.Authorization__c = auth.id;
        sr1.report_summary__c = rs.id;
        sr1.Release_Record_ID__c = loc1.id;
        sr1.RecordTypeId = prrectypeid;
        sr1.Planting_ID__c=loc1.id;
        sr1.Quantity_Acres__c=1;
        sr1.Start_Date__c= date.today();
        sr1.Is_No_Planting__c = false;
        insert sr1;
        sr1.Is_No_Planting__c = true;
        update sr1;

        for(Location__c locs:[select Id, No_Planting_Flag__c from location__c where Id IN:locations]){
            locationsMap.put(locs.Id, locs);
        }
        system.assertEquals(locationsMap.get(loc.Id).No_Planting_Flag__c, false);
        system.assertEquals(locationsMap.get(loc1.Id).No_Planting_Flag__c, true);
   
        objaa = testData.newAttach(li.id);
        objaa.Authorization__c = auth.id;
        objaa.Report_Summary__c = rs.id;
      
        String AppAttRepSummRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Report Summary').getRecordTypeId();
        objaa.recordtypeId = AppAttRepSummRecordTypeId;
        update objaa;
        objattach = testData.newAttachment(objaa.id);          
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = loc.id;
        eflRelatedRec.Construct__c  = construct.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;
      }
    
    //This test method creates several Self_Reporting__c dummy records with two Self_Reporting__c records containing the same custom fields Release_Record_Id__c and Start_Date__c
    @isTest
    static void checkForExistingRecordLocationAndStartDateTest() {
      
        init();
        Self_Reporting__c sr= new Self_Reporting__c();
            sr.Planting_ID__c=loc.id;
            sr.Authorization__c = auth.id;
            sr.report_summary__c = rs.id;
            sr.Release_Record_ID__c = loc.id;
       		sr.RecordTypeId = prrectypeid;
            sr.Planting_ID__c=loc.id;
        	sr.Quantity_Acres__c=1;
            sr.Is_Submitted__c = false;
            sr.Start_Date__c= date.today();
           
        
        //Begin test   
        Test.startTest();
            System.debug('EFLSelfReportingTriggerTest: Test execution context has begun.');
            //Insert the Self_Reporting__c records into the database
            try{
                insert sr;  //Error should be thrown here for the after insert trigger context 
            } catch (Exception e){
                System.debug('EFLSelfReportingTriggerTest: There was an error: ' + e);
            }   
        
            //Fetch the list of inserted Self_Reporting__c dummy records
            //Add WHERE clause in a bit. Taking a break :D
            List<Self_Reporting__c> insertedSelfReportingDummyRecords = [SELECT Id, Start_Date__c, Release_Record_ID__c, Report_Summary__c FROM Self_Reporting__c];
            System.debug('EFLSelfReportingTriggerTest: Test execution context has ended.');
        Test.stopTest();
        
        //Assert results
        //System.assertEquals(expected, actual);
    }//End test method EFLSelfReportingTriggerTest()
    
    
    @isTest 
    static void checkUniquePlantingIdTest() {
         id prRecType = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByName().get('Planting/Release Reports').getRecordTypeId();
        
       Test.startTest();
        System.debug('EFLSelfReportingTriggerTest: Attempting to start the test');
        //Attempt to insert the four Self_Reporting__c dummy records
        try {
            sr.Planting_ID__c=loc.id;
            sr.Authorization__c = auth.id;
            sr.Action_Taken__c = 'Test description';
            sr.report_summary__c = rs.id;
            sr.Release_Record_ID__c = loc.id;
            sr.RecordTypeId = prrectypeid;
            sr.Planting_ID__c=loc.id;
            sr.Quantity_Acres__c=1;
            sr.Is_Submitted__c = false;
            sr.Start_Date__c= date.today()+100;
            insert sr; 
            
        } catch (Exception e) {
            System.debug('EFLSelfReportingTriggerTest: There was an error: ' + e);
        }
        System.debug('EFLSelfReportingTriggerTest: Test has stopped.');
       Test.stopTest();
        
       //System.assertEquals(expected, actual)
    }
} //End class declaration EFLSelfReportingTriggerTest