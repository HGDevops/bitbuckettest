public inherited sharing class EFLBRSCheckStatusUtility{
    
    public static Set<Workflow_Task__c> callingWorkflows = new Set<Workflow_Task__c>();
    public static Set<Workflow_Task__c> workflowsToError = new Set<Workflow_Task__c>();
    public static Map<Id,Workflow_Task__c> workflowLineItemMap = new Map<Id,Workflow_Task__c>();
    public static List<EFLWorkFlowTwoCriteria__mdt> statusFromCMD;
    public static Map<string,list<string>> statusMap = new Map<string,list<string>>();
    public static Map<string,string> errorMap = new Map<string,string>();

    
    public static void checkStatusesFromTrigger(List<Workflow_Task__c> workflowList){
        
        List<Workflow_Task__c> workflowBRSList = new List<Workflow_Task__c>();
        
        for(Workflow_Task__c w : workflowList){
            
            if(w.Program__c.equals('BRS')){
                workflowBRSList.add(w);
            }
        }
        
        if(workflowBRSList.size() < 1){
            return;
        }
        
        statusFromCMD = [SELECT MasterLabel, Statuses__c, Message__c FROM EFLWorkFlowTwoCriteria__mdt];
        
        for(EFLWorkFlowTwoCriteria__mdt m :  statusFromCMD){
            statusMap.put(m.MasterLabel,m.Statuses__c.split('\\,'));
            errorMap.put(m.MasterLabel,m.Message__c);
        }
        
        List<Workflow_Task__c> workflowListToCheck = new list<Workflow_Task__c>();
        for(Workflow_Task__c w : workflowBRSList){
            if(statusMap.containsKey(w.status__c) && w.Workflow_Order__c == 2){
                workflowListToCheck.add(w);    
            }
        }
        if(workflowListToCheck.size()>0){
            checkRelatedAreComplete(workflowListToCheck);
        }
        
    }
    
    public static void checkRelatedAreComplete(List<Workflow_Task__c> workflowList){
        
        callingWorkflows.addall(workflowList);
        
        setupInitialMaps();
        
        //Call each related object to check for errors. After an error is found, that line item is removed from the list.`
        //If no error is found, then no error is thrown
        relatedRegArticleStatus(workflowLineItemMap.keyset());  //{WF.adderror('Oh No');}//messageFromCMD)};
        relatedContstructStatus(workflowLineItemMap.keyset());
        relatedRevConstructStatus(workflowLineItemMap.keyset());
        relatedLocations(workflowLineItemMap.keyset());
        relatedSOPs(workflowLineItemMap.keyset());
        relatedRevSOPs(workflowLineItemMap.keyset());
        relatedAttachments(workflowLineItemMap.keyset());

        if(workflowsToError.size()>0){
            string wfStatus ='';
            for(Workflow_Task__c w : workflowsToError){
                if(w.status__c != null && w.status__c !=''){
                    wfStatus = w.status__c;
                    w.adderror(errorMap.get(wfStatus));
                }    
            }
        }
    }
    
    public static void setupInitialMaps(){
    
        Map <Id,Workflow_Task__c> workflowAuthMap = new Map<Id,Workflow_Task__c>();
        
        for(Workflow_Task__c w : callingWorkflows){
            workflowAuthMap.put(w.Authorization__c,w);
        }
        List<Authorizations__c>authList = [SELECT Id, Application__c FROM Authorizations__c WHERE Id IN :workflowAuthMap.keyset()];
        Map<Id,Workflow_Task__c> workflowAppMap = new Map<Id,Workflow_Task__c>();
        for(Authorizations__c a : authList){
            workflowAppMap.put(a.Application__c,workflowAuthMap.get(a.id));
        }
        List<AC__c> lineItemList = [SELECT Id,Application_Number__c FROM AC__c WHERE Application_Number__c IN :workflowAppMap.keyset()];
        for(AC__c a : lineItemList){
            workflowLineItemMap.put(a.id,workflowAppMap.get(a.Application_Number__c));
        }
    }
    
    private static void relatedRegArticleStatus(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            List<Link_Regulated_Articles__c> relatedRecords = EFLBRSLineItemUtility.getRa(LineItemIds);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void relatedContstructStatus(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            List<Construct__c> relatedRecords = EFLBRSLineItemUtility.getConstruct('Line_item__c',LineItemIds);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void relatedRevConstructStatus(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            List<Construct_Application_Junction__c> relatedRecords = EFLBRSLineItemUtility.getRevCons(LineItemIds);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void relatedLocations(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            List<Location__c> relatedRecords = EFLBRSLineItemUtility.getLocation(LineItemIds);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void relatedSOPs(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            Id soprecid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Standard Operating Procedures').getRecordTypeId();
            List<Applicant_Attachments__c> relatedRecords = EFLBRSLineItemUtility.appattach(LineItemIds,soprecid);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void relatedRevSOPs(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            List<Construct_Application_Junction__c> relatedRecords = EFLBRSLineItemUtility.getRevSOP(LineItemIds);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void relatedAttachments(Set<Id>lineItemIds){
        if(!lineItemIds.isEmpty()){
            Id UploadAttrecid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Uploaded Attachment').getRecordTypeId();
            List<Applicant_Attachments__c> relatedRecords = EFLBRSLineItemUtility.appattach(LineItemIds,UploadAttrecid);
            if(relatedRecords != null){
                Schema.SObjectType sObjectType = relatedRecords.getSObjectType();
                castObjects(relatedRecords,sObjectType);
            }
        }
    }
    
    private static void castObjects(List<sObject> sObjectList,Schema.SObjectType sObjectType){
        
        String listType = 'List<' + sObjectType + '>';
        String sobjectString = string.valueOf(sObjectType);
        List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
        castRecords.addAll(sObjectList);
        
        Map<sObject,string> regArtStatusMap = new Map<sObject,string>();
        Map<Id,Id> regArtLineItemMap = new Map<Id,Id>();
        for (integer i = 0; i<castRecords.size(); i++){
            regArtStatusMap.put(castRecords[i],(string)castRecords[i].get('status__c'));
            if(sObjectString == 'Applicant_Attachments__c'){
                regArtLineItemMap.put((Id)castRecords[i].get('Id'),(Id)castRecords[i].get('Animal_Care_AC__c'));
            }
            else{
                regArtLineItemMap.put((Id)castRecords[i].get('Id'),(Id)castRecords[i].get('Line_Item__c'));
            }
        }
        Set<Id>badStatusList = checkStatus(regArtStatusMap,regArtLineItemMap);
        if(!badStatusList.isEmpty()){
            addErrors(badStatusList,regArtLineItemMap);
        }    
    }
    
    private static set<Id> checkStatus(Map<sObject,string> currentStatuses,Map<Id,Id> lineItemMap){
        Set<Id> sObjectBadStatusList = new Set<Id>();
        for(sObject currentStatus : currentStatuses.keySet()){
            
            string currentLineItem = lineItemMap.get(currentStatus.id);
            List<string> tempList = statusMap.get(workflowLineItemMap.get(currentLineItem).status__c);
            Set<string>toCheck = new Set<string>();
            toCheck.addAll(tempList);
            
            if(!toCheck.contains(currentStatuses.get(currentStatus))){
               sObjectBadStatusList.add(currentStatus.Id);
            }
        }
        return sObjectBadStatusList;
    }
    
    private static void addErrors(Set<Id>badStatusList,Map<Id,Id> sObjectLineItemMap){
        for(Id b : badStatusList){
            string a = sObjectLineItemMap.get(b);
            if(workflowLineItemMap.get(a) != null){
                workflowsToError.add(workflowLineItemMap.get(a));
            }
        }        
    }        
}