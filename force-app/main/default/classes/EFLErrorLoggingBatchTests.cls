@isTest
public class EFLErrorLoggingBatchTests {
    
    @isTest
    static void testSchedule(){
        String CRON_EXP = '0 0 0 15 3 ? *';
        
        // Create your test data
        Account acc = new Account();
        acc.name= 'test';
        insert acc;
        
        Test.startTest();
        
        String jobId = System.schedule('EFLErrorLoggingBatch',  CRON_EXP, new EFLErrorLoggingBatch(Date.today()));
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }
    
    @isTest
    static void testBatch(){
        Error_Logging__c er = new Error_Logging__c();
        insert er;
        test.startTest();
        EFLErrorLoggingBatch b = new EFLErrorLoggingBatch(Date.today().addDays(2));
        Database.executeBatch(b);
        test.stopTest();
        system.assertEquals(0, [SELECT Id FROM Error_Logging__c].size());
    }
}