@isTest
private class GetACLinetItemsTest{
      @IsTest static void GetACLinetItemsTest() {
      
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettingsWithBRSTriggerDisabled();
      Application__c objapp = testData.newapplication();
      AC__c li = testData.newlineitem('Personal Use', objapp);

      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
      
      GetACLinetItems controller = new GetACLinetItems(sc);
      controller.getLineItems();
      system.assert(controller != null);
      }
}