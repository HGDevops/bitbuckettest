public with sharing class IncidentStageProgressBarController {
    private ApexPages.StandardController controller;
    public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
    public Boolean processComplete{get;set;}
    public string opts {get; set;}
    public Id recId{get;set;}
    public List<String> options{get; set;}  
    public list<Program_Stages__mdt> programStageList; 
    public string currentStage{get;set;}
    public string status{get;set;}
    public Incident__c obj;    
    public Boolean navigation{get;set;}  
    public Boolean readyForNextStage{get;set;}
    public integer currentStageIndex{get;set;}
    public Boolean hasError{get;set;}
    public Boolean isActivityCompleted{get;set;}
    
    public IncidentStageProgressBarController(ApexPages.StandardController controller){
        isActivityCompleted = false;
        options = new List<String>();
        recId = ApexPages.currentPage().getParameters().get('id');  
        List<Task> lstTask = [select id from task where status != 'Completed' and whatId =: recId];
        if(lstTask.size() > 0)isActivityCompleted = false;
        else isActivityCompleted = true;
        obj=[select id,Stage__c,status__c, Workflow_Number__c,Ready_for_next_stage__c from Incident__c where id=:recId];
        currentStage=obj.Stage__c;
        status=obj.status__c;
        processComplete = false; 
        readyForNextStage = false;
        readyForNextStage = obj.Ready_for_next_stage__c;
        hasError = false;
        programStageList = [select Workflow_Number__c, Stage__c,Sequence__c from Program_Stages__mdt where object_type__c='Incident__c' Order By Sequence__c];  
        
        for(Program_Stages__mdt ps : programStageList){ 
            options.add(ps.Stage__c); 
        }
        opts = JSON.serialize(options);
        
        if(programStageList.isEmpty()){navigation = false;}
        else{
            navigation = true  ;    
        }
        currentStageIndex = options.indexOf(currentStage);
        if(currentStageIndex == options.size()-1){navigation = false; 
                                                  if(readyForNextStage){processComplete = true;}}
        
    }
    
    public PageReference moveToFirstStage(){
        Incident__c objIns  = new Incident__c(id = recId,Stage__c =options.get(0));
        EFLActivityUtility validateUsers = new EFLActivityUtility();        
        List<Team__c> teamMembersList = new List<Team__c>();
        List<String> currentRequiredRoles = new List<String>();
        hasError = false;
        
        
        teamMembersList = EFLTeamRepository.selectByIncidentID(obj.ID);
       //system.debug('TeamMemberList from Progress Bar Controller @@@@@@ ' + teamMembersList);system.debug('obj.Workflow_Number__c '+obj.Workflow_Number__c);
        if(obj.Workflow_Number__c!=null){
            currentRequiredRoles = EFLProgramPrefixUtility.getRequiredRoles(obj.Workflow_Number__c);
        }
       //system.debug('RequiredRoleList from Progress Bar Controller @@@@@@ ' + currentRequiredRoles);
        String errorMessage = validateUsers.validateRoles(obj, teamMembersList, currentRequiredRoles);
        
        if(String.isBlank(errorMessage)){
           //system.debug('There are no errors*****');
            updateStage(options.get(0));
            //system.debug('options.get(0) '+ options.get(0));
            shouldRedirect = true;
            if(controller!=null){
                redirectUrl = controller.view().getUrl();
                return null;
            }
        }else{
           //system.debug('ErrorMessage#### '+ errorMessage);
            apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,errorMessage));
            if(apexpages.hasMessages()){ 
                hasError = true;
                return null;
            }
        }
        
        //update objIns;
        return null;
    }
    
    
    public pagereference moveToNextStage()
    {
       //system.debug('Move To Next Stage');
        try
        {
           //system.debug('Current Stage: ' + options.get(currentStageIndex + 1));
            List<EFLIncidentStageStatusCriteria__mdt> incidentStageStatusList = [select id, Stage__c, Status__c, Error_Message__c from EFLIncidentStageStatusCriteria__mdt];
            String previousStage = options.get(currentStageIndex);
            Map<String, Set<EFLIncidentStageStatusCriteria__mdt>> incidentStageStatusMap = new Map<String, Set<EFLIncidentStageStatusCriteria__mdt>>();
            for(EFLIncidentStageStatusCriteria__mdt incidentStageStatus : incidentStageStatusList){
                if (!incidentStageStatusMap.containsKey(incidentStageStatus.Stage__c)){
                    Set<EFLIncidentStageStatusCriteria__mdt> newSet = new Set<EFLIncidentStageStatusCriteria__mdt>();
                    newSet.add(incidentStageStatus);
                    incidentStageStatusMap.put(incidentStageStatus.Stage__c, newSet);
                }else {
                    Set<EFLIncidentStageStatusCriteria__mdt> existingSet = incidentStageStatusMap.get(incidentStageStatus.Stage__c);
                    existingSet.add(incidentStageStatus);
                    incidentStageStatusMap.put(incidentStageStatus.Stage__c, existingSet);
                }
            }
           //system.debug('Previous Stage: ' + previousStage);
           //system.debug('Previous Stage Metadata: ' + incidentStageStatusMap.get(previousStage));
            //Check if the metadata contains the stage and if it contains the stage/status combination then return the error
            if(incidentStageStatusMap.containsKey(previousStage)){
                for (EFLIncidentStageStatusCriteria__mdt incidentStageStatus : incidentStageStatusMap.get(previousStage)){
                    if (incidentStageStatus.Status__c == obj.Status__c){                     
                        apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,'Error: ' + incidentStageStatus.Error_Message__c));
                        hasError = true;
                        return null;
                    }
                }
            }
            //The metadata doesn't have this combination so it is ok to move to the next stage
            string currentStage = options.get(currentStageIndex + 1);
            string status = '';
            //If we are going to Incident Review then the status needs to get updated to Analysis in Progress
            //If we are going to Issue Response Letter then the status needs to get updated to Response Pending
            if(currentStage == 'Incident Review'){
                status = 'Analysis in Progress';  
            } else if(currentStage == 'Issue Response Letter'){
                status = 'Response Pending';  
            }
            Incident__c incident = new Incident__c(id = recId, Stage__c = options.get(currentStageIndex+1), Status__c = status);
            update incident;
            return null; 
            
        } catch(exception e){
            String errorMsg = e.getMessage();
            hasError = true;
           //system.debug('errorMsg#### '+ errorMsg);
            if(errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                errorMsg = errorMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', ': ');
               //system.debug('errorMsg#### '+ errorMsg);
                //pagereference p = apexpages.Currentpage();
                apexpages.addmessage(new apexpages.message(apexpages.severity.FATAL,'Error: ' + errorMsg));
                if(apexpages.hasMessages()) { 
                   //system.debug('TestHasMessages');
                    hasError = true;
                    //system.debug('hasError:'+hasError);
                    return null;
                } 
            }
        }
        return null;
    }
    
    public void updateStage(string stageName){  //Incident Review
        //system.debug('stageName '+stageName);
        string status='';
        if(stageName==system.label.Stage_Incident_Review){
            status='Analysis in Progress';
        }
        Incident__c incidentRecord  = new Incident__c(id = recId,Stage__c =stageName,Status__c=status);
        update incidentRecord;
    }
    
    
}