@isTest(SeeAllData=True)
public class EFLCloneApplicationControllerTest {
    public static testMethod void testMyController() {
    Carpol_AC_TestDataManager testData = new Carpol_AC_TestDataManager();
    Application__c app = new Application__c();
    app = testData.newapplication();
    
    Authorizations__c auth = new Authorizations__c();
    auth = testData.newAuth(app.id);
    
    test.startTest();      
         
    ApexPages.StandardController sc = new ApexPages.StandardController(app);
    EFLCloneApplicationController cont = new EFLCloneApplicationController(sc);    
    
    PageReference pageRef = Page.EFLLocationSelection;
    pageRef.getParameters().put('id', app.id);
    Test.setCurrentPage(pageRef);   
    
    cont.CurrentApp = app;
    cont.cloneApplication();
    
    test.stopTest();
        
    }
    public static testMethod void testMyController1() {
    
    Application__c app = new Application__c();
    insert app;
   
    ApexPages.StandardController sc = new ApexPages.StandardController(app);
    EFLCloneApplicationController cont = new EFLCloneApplicationController(sc);     
    //cont.CurrentApp = app;
    cont.cloneApplication();    
   }
    
}