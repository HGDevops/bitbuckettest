public class EFLContactTriggerHandler {
    public static final string AC_RL_ACCOUNT_RECORD_TYPE_NAME = 'AC_R_L_Account';
    
    public static void doBeforeInsert(List<Contact> newContacts){        
        BRSCommunityLogic(newContacts);
        facilityAdminValidation(newContacts, new Map<Id, Contact>());
    }
    
    public static void doBeforeUpdate(List<Contact> newContacts, Map<Id, Contact> oldContacts){
        BRSCommunityLogic(newContacts);
        facilityAdminValidation(newContacts, oldContacts);
    }    
    
    public static void BRSCommunityLogic(List<Contact> newContacts){
        string strprofileid = UserInfo.getProfileId();
        string BRSProfile = [select id,Name from Profile where id=:strprofileid].Name;
        
        Map<ID,RecordType> rt_Map = New Map<ID,RecordType>([Select ID, Name From RecordType Where sObjectType = 'Contact']);
        List<Contact> needAccounts = new List<Contact>();
        
        //Dinesh
        Set<String> Country = new set<String>();
        Set<String> State = new Set<String>();
        
        Map<String,Country__c> countries = new Map<String,Country__c>();
        Map<String,id> countrycode = new Map<String,id>();
        List<Country__c> countriesList = [Select Id, Name, Country_Code__c from Country__c LIMIT 1000];
        if(!countriesList.isEmpty()){
            for(Country__c cl: countriesList){
                countries.put(cl.Name, cl);
                countrycode.put(cl.Country_Code__c, cl.id);    
            }
        }
        
        list<string> stateCodeList = new list<String>();
        map<string,id> stateIdMap = new map<string,id>();
        //if(BRSProfile=='BRS Partner Community User'){
        for (Contact c : newContacts) {
            if(rt_map.get(c.recordTypeID).name.containsIgnoreCase('Individual Owner')){
                if (c.Individual_Owner_Account__c!=true){
                    if (String.isBlank(c.accountid)) {
                        needAccounts.add(c);
                    }
                }
            }
            
            //BY Dinesh to populate LR fields on Contact - 9/22
            
            if(c.MailingCountry != null && c.MailingCountry != ''){
                c.Mailing_City_LR__c = c.MailingCity;
                if(countrycode.containsKey(c.MailingCountryCode)){
                    c.Mailing_Country_LR__c = countrycode.get(c.MailingCountryCode);
                }
                if(c.MailingStateCode!=null){
                    stateCodeList.add(c.MailingStateCode);            
                }
                c.Mailing_Street_LR__c = c.MailingStreet;
                c.Mailing_Zip_Postalcode_LR__c = c.MailingPostalCode;
            } 
        }
        //}
        for(Level_1_Region__c st: [select id,Name,Level_1_Region_Code__c,Country__c from Level_1_Region__c where Level_1_Region_Code__c in:stateCodeList])
        {
            stateIdMap.put(st.Level_1_Region_Code__c,st.id);
        } 
        for (Contact c1 : newContacts) {
            if(stateIdMap.containsKey(c1.MailingStateCode)){
                c1.Mailing_State_Province_LR__c = stateIdMap.get(c1.MailingStateCode);
            }
        }   
        if (needAccounts.size() > 0) {
            List<Account> newAccounts = new List<Account>();
            Map<String,Contact> contactsByNameKeys = new Map<String,Contact>();
            
            //Create account for each contact
            for (Contact c : needAccounts) {
                String accountName = c.firstname + ' ' + c.lastname;
                contactsByNameKeys.put(accountName,c);
                Account a = new Account(name=accountName);
                newAccounts.add(a);
            }
            insert newAccounts;
            
            for (Account a : newAccounts) {
                
                //Put account ids on contacts
                if (contactsByNameKeys.containsKey(a.Name)) {
                    contactsByNameKeys.get(a.Name).accountId = a.Id;
                    contactsByNameKeys.get(a.Name).Individual_Owner_Account__c= true;
                    a.BillingStreet = contactsByNameKeys.get(a.Name).MailingStreet;
                    a.BillingCity= contactsByNameKeys.get(a.Name).MailingCity;
                    a.BillingState= contactsByNameKeys.get(a.Name).MailingState;
                    a.BillingCountry= contactsByNameKeys.get(a.Name).MailingCountry ;
                    a.BillingPostalCode= contactsByNameKeys.get(a.Name).MailingPostalCode;
                    a.ShippingStreet= contactsByNameKeys.get(a.Name).OtherStreet;
                    a.ShippingCity= contactsByNameKeys.get(a.Name).Othercity;
                    a.ShippingState= contactsByNameKeys.get(a.Name).Otherstate;
                    a.ShippingCountry= contactsByNameKeys.get(a.Name).Othercountry ;
                    a.ShippingPostalCode= contactsByNameKeys.get(a.Name).OtherPostalCode;
                    a.Phone= contactsByNameKeys.get(a.Name).Phone;
                    a.Other_Phone__c= contactsByNameKeys.get(a.Name).MobilePhone;
                }
                
            }
            update newAccounts;
        }
    }
    
    
    public static void facilityAdminValidation(List<Contact> newContacts, Map<Id,Contact> oldContacts){
        RecordType acAccountRecordType = [SELECT Id 
                                          FROM RecordType 
                                          WHERE sObjectType = 'Account' 
                                          AND DeveloperName = :AC_RL_ACCOUNT_RECORD_TYPE_NAME];
        
        if(userinfo.getUserType() == 'Standard'){ 
            Set<String> accountIds = new Set<String>();
            for (Contact contact : newContacts){
                accountIds.add(contact.AccountId);
            }
            
            Map<Id, Account> accountRecordTypes = new Map<Id, Account>([SELECT Id, RecordTypeId 
                                                                               FROM Account 
                                                                               WHERE Id IN :accountIds]);
            List<Contact> faList = new List<Contact>();
            Map<String, integer> accountMap = new Map<String, integer>();
            //create a map of the accounts only for contacts that have the FA Access code
            for (Contact contact : newContacts){
                //check record type and EFL_ACIS_Contact__c = null
                if (accountRecordTypes.containsKey(contact.AccountId) &&
                    accountRecordTypes.get(contact.AccountId).RecordTypeId == acAccountRecordType.Id &&
                   	String.isBlank(contact.EFL_ACIS_Contact__c))
                {
                    accountMap.put(contact.AccountId, 0);
                    faList.add(contact);
                }
            }
            
            //get the existing facility admins and update the map
            List<Contact> currentFAs = [SELECT Id, AccountId, EFL_Facility_Admin__c 
                                        FROM Contact 
                                        WHERE AccountId IN :accountMap.keyset()
                                        AND EFL_Facility_Admin__c = true
                                        AND EFL_ACIS_Contact__c = null ];
            for (Contact contact : currentFAs){
                Integer currentFAsOnAccount = accountMap.get(contact.AccountId);
                currentFAsOnAccount++;
                accountMap.put(contact.AccountId, currentFAsOnAccount);
            }
            
            //for each updated contact update the facility admin map
            for (Contact contact: faList) {            
                boolean currentlyFA = false;
                if (oldContacts.containsKey(contact.Id)){
                    currentlyFA = oldContacts.get(contact.Id).EFL_Facility_Admin__c;
                }
                    
                if (contact.EFL_Facility_Admin__c == false && currentlyFA){
                    Integer currentFAsOnAccount = accountMap.get(contact.AccountId);
                    accountMap.put(contact.AccountId, currentFAsOnAccount-1);
                } else if (contact.EFL_Facility_Admin__c == true && !currentlyFA){
                    Integer currentFAsOnAccount = accountMap.get(contact.AccountId);                        
                    accountMap.put(contact.AccountId, currentFAsOnAccount+1);
                }
            }
                        
            //check each account to make sure there is between 1-2 facility admins
            for (String key : accountMap.keySet()){
                if (accountMap.get(key) < 1){
                    for (Contact contact : faList){
                        boolean currentlyFA = false;
                        if (oldContacts.containsKey(contact.Id)){
                            currentlyFA = oldContacts.get(contact.Id).EFL_Facility_Admin__c;
                        }
                        
                        if (contact.AccountId == key && 
                            contact.EFL_Facility_Admin__c == false && 
                            currentlyFA)
                        {
                            contact.addError('Cannot remove the sole Admin on this account.');
                        }
                    }
                }else if (accountMap.get(key) > 2){
                    for (Contact contact : faList){
                        if (contact.AccountId == key){
                            contact.addError('There are already two Admins for this account.');
                        }
                    }                    
                }   
            }
        }
    }
}