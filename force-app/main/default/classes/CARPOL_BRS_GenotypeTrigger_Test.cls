@isTest
private class CARPOL_BRS_GenotypeTrigger_Test {
	@isTest
    private static void test_CARPOL_BRS_GenotypeTrigger(){

      CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
      testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Release',objapp);      
      AC__c ac3 = testData.newLineItem('Import',objapp);            
      Authorizations__c objauth = testData.newAuth(objapp.Id);      
      ac.Authorization__c = objauth.Id;
      update ac;
      
      Construct__c con = new Construct__c();
      con = testData.newconstruct(ac.id);
      
      Genotype__c gen = new Genotype__c();
      gen = testData.newgenotype(con.id);      
      
      Test.startTest();
      gen.Construct_Component__c = 'Other';
      update gen;
      System.assert(gen != null);
      Test.stopTest();
      
      
      
    }
    
}