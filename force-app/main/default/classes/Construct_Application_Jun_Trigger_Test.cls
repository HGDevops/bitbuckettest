@isTest
public class Construct_Application_Jun_Trigger_Test {

    @testSetup
    static void setupData() {
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        
        Application__c objapp = testData.newapplication();
        
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        
        Regulated_Article__c ra = testData.newRegulatedArticle(testData.newBRSPathway().id);
        
        AC__c ac1 = testData.newLineItemBRSByRA('Personal Use',objapp, ra.id); 
        ac1.Authorization__c = objauth.id;
        update ac1;
        
        Link_Regulated_Articles__c lra = testData.newlinkRegArticleByLIIdRAId(ac1.id, ra.Id, 'Draft');
        
        Construct__c objconst = testdata.newconstructByLIAndRegulatedArticle(ac1.id, ra.id);    
        update objconst;
        
        Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
        objcaj.Line_Item__c = ac1.id;
        objcaj.construct__c = objconst.id;
        objcaj.Applicant_Instructions__c = 'Make corrections';
        objcaj.Status__c = 'In Review';
        insert objcaj;        
    }
    
    static testmethod void testConAppJunInsert(){
        Application__c objapp = [select Id from Application__c limit 1];
        Authorizations__c objauth = [select Id from Authorizations__c limit 1];
        AC__c ac1 = [select Id from AC__c limit 1];
        Construct__c objconst = [select Id from Construct__c limit 1];
        Test.startTest();
        Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
        objcaj.Line_Item__c = ac1.id;
        objcaj.construct__c = objconst.id;
        objcaj.Applicant_Instructions__c = 'Make corrections';
        objcaj.Status__c = 'Review Complete';
        insert objcaj;

        System.assert(objcaj.Id != null);
        
        Construct_Application_Junction__c objcaj1 = new Construct_Application_Junction__c();
        objcaj1.Line_Item__c = ac1.id;
        objcaj1.Application__c = objapp.id;
        insert objcaj1;

        System.assert(objcaj1.Id != null);
        Test.stopTest();
    }

    static testmethod void testConAppJunUpdate(){
        Application__c objapp = [select Id from Application__c limit 1];

        Test.startTest();

        Construct_Application_Junction__c objcaj = [select Id from Construct_Application_Junction__c limit 1];
        objcaj.Status__c = 'Review Complete';
        update objcaj;   

        Construct_Application_Junction__c updatedCaj = [select Id, Status__c from Construct_Application_Junction__c where Id = :objcaj.Id];
        System.assert(updatedCaj.Status__c == 'Review Complete');
        Test.stopTest();
    }

    static testmethod void testConAppJunDelete(){
        Application__c objapp = [select Id from Application__c limit 1];
        Authorizations__c objauth = [select Id from Authorizations__c limit 1];
        AC__c ac1 = [select Id from AC__c limit 1];
        Construct__c objconst = [select Id from Construct__c limit 1];

        Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
        objcaj.Line_Item__c = ac1.id;
        objcaj.construct__c = objconst.id;
        objcaj.Applicant_Instructions__c = 'Make corrections';
        objcaj.Status__c = 'Review Complete';
        insert objcaj;
        
        Test.startTest();
        delete objcaj;
        List<Construct_Application_Junction__c> cajList = [select Id from Construct_Application_Junction__c where Id = :objcaj.Id];
        System.assert(cajList.size() == 0);
        
        Test.stopTest();
    }
    
    static testmethod void testConAppJunInsertBulk(){
        Application__c objapp = [select Id from Application__c limit 1];
        Authorizations__c objauth = [select Id from Authorizations__c limit 1];
        AC__c ac1 = [select Id from AC__c limit 1];
        Construct__c objconst = [select Id from Construct__c limit 1];

        List<Construct_Application_Junction__c> cajList = new List<Construct_Application_Junction__c>();
        for (Integer i = 0; i < 200; i++) {
            Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
            objcaj.Line_Item__c = ac1.id;
            objcaj.construct__c = objconst.id;
            objcaj.Applicant_Instructions__c = 'Make corrections';
            objcaj.Status__c = 'Review Complete';
            cajList.add(objcaj);
        }
        
        Test.startTest();
        insert cajList;
        Test.stopTest();
    }    

    static testmethod void testConAppUpdateBulk(){
        Application__c objapp = [select Id from Application__c limit 1];
        Authorizations__c objauth = [select Id from Authorizations__c limit 1];
        AC__c ac1 = [select Id from AC__c limit 1];
        Construct__c objconst = [select Id from Construct__c limit 1];

        List<Construct_Application_Junction__c> cajList = new List<Construct_Application_Junction__c>();
        for (Integer i = 0; i < 200; i++) {
            Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
            objcaj.Line_Item__c = ac1.id;
            objcaj.construct__c = objconst.id;
            objcaj.Applicant_Instructions__c = 'Make corrections';
            objcaj.Status__c = 'In Review';
            cajList.add(objcaj);
        }
        insert cajList;
        
        Test.startTest();
        for (Construct_Application_Junction__c caj : cajList) {
            caj.Status__c = 'Review Complete';
        }       
        update cajList;
        Test.stopTest();
    }       

}