/**********************************************************************  
* @Purpose : This Handler class handles the logic to retrieve 
*            major functionality for APHIS External Users 
*            (Applicants/Account Admin Applicants/Broker/Preparer) in efile. 
* @User Story Number :  
************************************************************************/
public with sharing class EFLExternalUsersHandler {

  //Constants
    static final string MULTIPLE='Multiple';
    static final string NONE='None';    
    
   public static List < Authorizations__c > AuthorizationList {
        get {
            if (AuthorizationList == null) {
                AuthorizationList = [SELECT ID, Application__r.name, Name, CreatedDate, AuthorizationType__c, effective_date__c,BRS_Introduction_Type__c, 
                                           Expiration_Date__c, Status__c, Permit_Number__c, Application__c, RecordType.Name,Regulated_Article__c, Legacy__c 
                                   FROM Authorizations__c 
                                   ORDER BY Name ASC 
                                   LIMIT 999];
            }
            return AuthorizationList;
        }
        set;
    } 

/*
  Get required Authorization records 
 */    
    public static List < Authorizations__c > RequiredAttentionauthorizationList {
        get {
            if (RequiredAttentionauthorizationList == null) {
                RequiredAttentionauthorizationList = [SELECT ID, Application__r.name, Name, CreatedDate, AuthorizationType__c, Date_Issued__c,Applicant__r.Name,Stage__c, 
                                           Expiration_Date__c, Status__c, Permit_Number__c, Application__c, RecordType.Name,Regulated_Article__c,BRS_Introduction_Type__c,LastModifiedDate
                                    FROM Authorizations__c 
                                   WHERE status__c =: EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS WAIT CUST')
                                ORDER BY CreatedDate DESC 
                                   LIMIT 999];
            }
            return RequiredAttentionauthorizationList; 
        }
        set;
    }
    
    
 /**********************************************************************************
  ****Line Item Wrapper********
  *************************************************************************************/
   public class lineItemsWrapper{ 
        public ac__c lineItemsRec {get;set;}
        public string regulatedArticles{get;set;}
       public string proposedEffectiveDate{get;set;}
        public lineItemsWrapper( ac__c lineItemsRec,string regArticles){
            lineItemsRec = lineItemsRec;
            regulatedArticles = regArticles;
         }
     } 
 /**********************************************************************************
  * @purpose: Prepare line Item details along with regulated article 
  *           for the selected application 
  * @Input Attributes: Application ID
  * @Return Parameter: Returns line item wrapper list for the selected application
  *************************************************************************************/  
  public static list<lineItemsWrapper> lineItemWrapperList{get;set;} 
  @RemoteAction
  public static List<lineItemsWrapper> getlineItemRecords(ID appId ){
        string linkRA='';
        List<ac__c> lineItemList = new List<ac__c>();
        //retrieve all line item records 
         lineItemList = [SELECT Id,Name,
                                Status__c,Authorization__r.Name,Record_Type_Name__c,
                                Exporter_First_Name__c,Importer_First_Name__c,AuthorizationType__c,
                                Authorization_Text__c,Application_Number__c,Does_This_Application_Contain_CBI__c,
                                Movement_Type__c,Scientific_Name__c,Authorization__c,Proposed_Start_Date__c,
                                Scientific_Name_Text__c,Port_of_Entry__c,Type_of_Permit__c,
                                Port_of_Entry_Text__c,Port_of_Embarkation__c,Number_of_Items__c,
                                Port_of_Embarkation_Text__c,Proposed_date_of_arrival__c,
                                Program_Line_Item_Pathway__r.Program__r.Name, Legacy__c,
                                (SELECT Regulated_Article__c,
                                        Regulated_Article__r.name,
                                        Scientific_Name__c 
                                   FROM Link_Regulated_Articles__r)
                           FROM ac__c 
                          WHERE application_number__c = :appId
                          ORDER BY CreatedDate DESC 
                          LIMIT 999];
      
         lineItemWrapperList = new List<lineItemsWrapper>();
        
        //prepare the lineitem wrapper list with link regulated article details
            for(ac__c line: lineItemList){
                  lineItemsWrapper li = new lineItemsWrapper(line,linkRA);
                  li.lineItemsRec = line;
                //Check if line is BRS program then obtain regulated article details from Link Regulated Article object
                if(line.Program_Line_Item_Pathway__r.Program__r.Name == EFLGlobalConstants.getConstantValue('PROGRAM BRS')){
                      if(line.Link_Regulated_Articles__r.size()==1){
                         li.regulatedArticles = line.Link_Regulated_Articles__r[0].Regulated_Article__r.name;
                      }else if(line.Link_Regulated_Articles__r.size()==0){
                         li.regulatedArticles = NONE;  
                      }else if(line.Link_Regulated_Articles__r.size()>1){
                         li.regulatedArticles = MULTIPLE; 
                      }
                }else if(line.Program_Line_Item_Pathway__r.Program__r.Name == EFLGlobalConstants.getConstantValue('PROGRAM AC')){  
                         li.regulatedArticles = EFLGlobalConstants.getConstantValue('REGULATED ARTICLE DOGS');
                }else{
                   //for VS and PPQ programs regulated article is from line item object   
                       li.regulatedArticles = line.Scientific_Name_Text__c;  
                }
                if(li.lineItemsRec.Proposed_Start_Date__c == NULL){
                    li.proposedEffectiveDate =  'Not Applicable';}else{
                    li.proposedEffectiveDate =  li.lineItemsRec.Proposed_Start_Date__c.format();  
                    }
                  lineItemWrapperList.add(li);
          }
          // return the wrapper list  
          return lineItemWrapperList;
     }
   
}