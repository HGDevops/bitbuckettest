public without sharing class IncidentAddLocationsController {
    public String incidentId {get;set;}
    private Incident__c rIncident;
    private Map<Id,LocationWrapper> mAvailableLocations;
    private Map<Id,LocationWrapper> mSelectedLocations;
    public Id selectedLocationId {get;set;}
    
    public IncidentAddLocationsController(ApexPages.StandardSetController controller){
         incidentID = ApexPages.currentPage().getParameters().get('id');
         System.debug('inside controller=='+incidentID);
         mAvailableLocations = new Map<id,LocationWrapper>();
         mSelectedLocations = new Map<Id, LocationWrapper>();
         List<AC__c> lsAuthorizations=[select Id,(Select Id,State__c,Name,RecordType.Name,Level_2_Region__c,Country__c,Description__c
                                                  ,Status__c,Status_Graphical__c,Applicant_Instructions__c From Locations__r )
                                       From AC__c Where Authorization__c in (Select Authorization__c from Incident__c WHere Id=:incidentId)];
        
         for(AC__c oAuth:lsAuthorizations){
            for(Location__c loc:oAuth.Locations__r){
                mAvailableLocations.put(loc.Id,new LocationWrapper(loc));
            }
        }
    }	
    
    public List<LocationWrapper> getAvailableLocations(){
        if(mAvailableLocations!=null) return mAvailableLocations.values();
        return null;
    }
    
    public List<LocationWrapper> getSelectedLocations(){
        if(mSelectedLocations.isEmpty()) return null;
        return mSelectedLocations.values();
    }
    
    public PageReference saveLocations(){
        if(mSelectedLocations.isEmpty()){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error: Please select at least one location.'));
            return null;
        }
        List<Location__c> lsLocationsToUpdate = new List<Location__c>();
        for(LocationWrapper lw:mSelectedLocations.values()){
            Location__c loc = lw.location;
            loc.Incident__c = incidentId;
            lsLocationsToUpdate.add(loc);
        }
        if(!lsLocationsToUpdate.isEmpty()){
            update lsLocationsToUpdate;
        }
        PageReference pageRef = new PageReference('/'+incidentId);
        return pageRef;
    }
    
    public void selectLocation(){
        if(selectedLocationId!=null){
            LocationWrapper selectedLocation = mAvailableLocations.get(selectedLocationId);
            mAvailableLocations.remove(selectedLocationId);
            mSelectedLocations.put(selectedLocationId,selectedLocation);
            selectedLocationId=null;
        }
    }
    
    public void removeSelectedLocation(){
        if(selectedLocationId!=null){
            LocationWrapper selectedLocation = mSelectedLocations.get(selectedLocationId);
            mSelectedLocations.remove(selectedLocationId);
            selectedLocation.isSelected=false;
            mAvailableLocations.put(selectedLocationId,selectedLocation);
            selectedLocationId=null;
        }
    }
    
    public class LocationWrapper{
        public boolean isSelected {get;set;}
        public Location__c location {get;set;}
        public LocationWrapper(Location__c loc){
            this.isSelected=false;
            this.location=loc;
        }  
        
    }
}