/**
 * Authorization Engine Interface
 */
public interface EFLIAuthorizationEngine { 

	void createAuthorization(authorizations__c authRecord);
	boolean validate(authorizations__c authRecordOld, authorizations__c authRecord);
	void updateAuthorization(authorizations__c authRecordOld, authorizations__c authRecord);
	void handleCommunication(authorizations__c authRecord);
	void loadActivities(authorizations__c authRecord,list<sObject> activityList);
	//boolean validateAmendmentAction(authorizations__c authRecord); //Ravee Racharla 
	// boolean validateRenewalAction(authorizations__c authRecord); //Ravee Racharla 
	Application__c processAmendment(authorizations__c authRecord, String actionType); //Ravee Racharla
	/***To-Be Implemented ***/
	//void handleSharing(authorizations__c authRecord);   
	//void processAmendmentActions(authorizations__c authRecord);
	//void processRenewalActions(authorizations__c authRecord);
	//void updateRelatedRecords(authorizations__c authList);
}