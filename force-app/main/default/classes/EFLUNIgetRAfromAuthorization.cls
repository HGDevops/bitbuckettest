/**
* Author : Kishore Kumar
* Created Date : 09/18/2017
* Purpose : This is utility class to obtain Regulated articles of any Authorization for all pathways
* Releated Pages: None
*/
public without sharing  class EFLUNIgetRAfromAuthorization {

/*************************************************************************************** 
 * Purpose: Obtain Regulated Articles for an Authorization passed in authorizationIdList
 * Return Parameter: mapAuthRegArticles
 ***************************************************************************************/

 public static map<id,list<Regulated_Article__c>> getRegulatedArticles(list<id> authorizationIdList){
     
        map<id,list<Regulated_Article__c>> mapAuthRegArticles = new map<id,list<Regulated_Article__c>>();
        
        list<AC__c> lineItemList =new List<AC__c>();
        list<Regulated_Article__c> lineitemregarticles;
        
        if(authorizationIdList != null && !authorizationIdList.isEmpty()){
             lineItemList = [SELECT ID,
                                Application_Number__c,
                                RecordTypeID, Sex__c,Domain__r.name,Domain__c,Authorization__c,
                                Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.name, 
                                program_Line_Item_Pathway__r.Program__c,Program_Line_Item_Pathway__r.Program__r.name,
                                Scientific_name__c,Scientific_name__r.name,RA_Scientific_name__r.Name,
                                (SELECT Regulated_Article__c,Regulated_Article__r.name,Scientific_Name__c FROM Link_Regulated_Articles__r) 
                          FROM AC__c 
                          WHERE Authorization__c in: authorizationIdList ];
       
                if(lineItemList != null && !lineItemList.isEmpty()) {
                    
                   for(AC__c li : lineitemlist){
                       Regulated_Article__c regart = new Regulated_Article__c();
                       lineitemregarticles= new list<Regulated_Article__c>();
                       regart.id = li.Scientific_name__c;
                       regart.name = li.RA_Scientific_name__r.Name;
                       // if its BRS pathway get regulated articles from Link_Regulated_Articles__c junction object
                        if(li.Program_Line_Item_Pathway__r.Program__r.name == 'BRS'){
                              for(Link_Regulated_Articles__c lar : li.Link_Regulated_Articles__r){
                                       regart.id = lar.Regulated_Article__c;
                                       regart.name = lar.Scientific_Name__c;
                                       if(regart != null){
                                          lineitemregarticles.add(regart);
                                       } 
                             }
                        //other pathways get regulated articles from line item - Scientific_Name__c field         
                         }else {
                               if(regart != null){
                                  lineitemregarticles.add(regart); 
                               } 
                         } 
                         if(!mapAuthRegArticles.containsKey(li.Authorization__c)){
                              mapAuthRegArticles.put(li.Authorization__c,lineitemregarticles);
                         }else{
                              mapAuthRegArticles.get(li.Authorization__c).addAll(lineitemregarticles);
                         }
                        } 
                 }
        } 
        
     return mapAuthRegArticles;
  }
    
    
}