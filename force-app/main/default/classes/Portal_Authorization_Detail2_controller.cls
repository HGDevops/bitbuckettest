/**
* Author :  
* Created Date : 4/13/2016
* Purpose : Authorization page to display post submission details like Permit Package, 
*           Self Reporting, Amendment/Renewal details, Associated Regulations
* Related Pages: Portal_Authorization_Detail2
*/
public with sharing class Portal_Authorization_Detail2_controller{
    /**
*  Properties
**/      
    public Id authorizationID{get;set;}
    public Id applicationID{get;set;}
    public boolean displayCompAgreementButton{get;set;}
    public boolean BRSlinkregulations{get;set;}
    public List<Link_Authorization_Regulation__c> listlinkRegulations;
    public List<Workflow_Task__c> wfLst{get;set;}
    public string Sectionheadertitle{get;set;}
    public Authorizations__c objauthItem{get;set;}
    public boolean renewButton{get;set;}
    public string Renewal{get;set;}
    public Id lineitem{get;set;}
    public string delrecid{get;set;}
    public Map<string,string> sobjectkeys{get;set;}
    public string redirect{get;set;}
    public string recTypeName{get;set;}
    public Id recordTypeId{get;set;}
    public Id permitpackRecTypeId{get;set;} 
    public string decisiontype;
    public list<Amendment_Renewal__c> ARlineitems{get;set;} 
    public list<id> lineitemids=new  list<id>();
    public list<AC__c> lineitemstoshow{get;set;}
    public Id selectedAuthid {get; set;}
    public String requiredReports {get;set;} //W-027762 Added for displaying required reports
    /**
* Standard Controller Constructor  
**/ 
    public Portal_Authorization_Detail2_controller(ApexPages.StandardController controller){
        Map<string,Schema.SobjectType> describe=Schema.getGlobalDescribe();
        sobjectkeys=new  Map<string,string>();
        authorizationID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('id'));
        //system.debug('Auth id in 41>>>'+authorizationID);
        if(authorizationID!=null){
            wfLst=[SELECT Id,Name,Status__c FROM Workflow_Task__c WHERE Authorization__c=:authorizationID AND (Workflow_Order__c=3 OR Workflow_Order__c=6) AND Status__c='In Progress' and Program__c != 'BRS' LIMIT 1];
            if(wfLst.size()>0){
                string wfStatus=wfLst[0].status__c;
                //system.debug('workflow status : '+wfStatus);
                if(wfStatus=='In Progress')displayCompAgreementButton=true;
            }
            recordTypeId=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Self Reporting').getRecordTypeId();
            permitpackRecTypeId=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Permit Package').getRecordTypeId();
            //system.debug( '51 line is >>>>>>>>>>>>>>>>>'+[SELECT ID,Name FROM Authorizations__C WHERE Id=:authorizationID ]);
            objauthitem= [SELECT AC_Applicant_Address__c,Associated_Authorization__c,AC_Applicant_Email__c,AC_Applicant_Fax__c,AC_Applicant_Name__c,AC_Applicant_Phone__c,AC_Organization__c,AC_USDA_License_Expiration_Date__c,
                          AC_USDA_License_Number__c,AC_USDA_Registration_Expiration_Date__c,AC_USDA_Registration_Number__c,Applicant_Alternate_Email__c,Applicant_Instructions__c,Applicant_Organization__c,Applicant__c,Application_CBI__c,
                          Application__c,Authorization_Type__c,Authorized_User__c,Biological_Material_present_in_Article__c,Bio_Tech_Reviewer__c,BRAP_Manager1__c,BRAP_Manager2__c,BRAP_Manager_3__c,BRS_Create_Labels__c,BRS_Create_State_Review_Records__c,
                          BRS_Hand_Carry_For_Importation_Only__c,BRS_Introduction_Type__c,BRS_Number_of_Labels__c,BRS_Proposed_End_Date__c,BRS_Proposed_Start_Date__c,BRS_Purpose_of_Permit__c,BRS_Send_Labels__c,BRS_State_Review_Notification__c,CBI__c,
                          Country_and_Locality_Information__c,CreatedById,CreatedDate,Date_Issued__c,Documents_Sent_to_Applicant__c,Effective_Date__c,Email_Contents__c,EPA_Review_Required__c,Expiration_Date__c,Fee_Amount__c,Id,If_Yes_Please_Describe__c,
                          IsDeleted,Issuer_Name__c,Issuer__c,Is_violator__c,Jurisdiction__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Locked__c,Mailing_Address__c,Means_of_Movement__c,Name,Number_of_New_Design_Protocols__c,
                          Number_of_Release_Sites__c,Organization_Unique_ID__c,OwnerId,Permit_Number__c,Permit_Specialist__c,Plant_Inspection_Station__c,Ports_of_Arrival__c,Port_of_Entry_For_Importation_Only_del__c,Port_of_Entry__c,Position__c,Prefix__c,RecordTypeId,
                          Remaining_Agreement_Needed__c,Status__c,Sub_Status__c,SystemModstamp,Template__c,Thumbprint__c,Thumbprint__r.Program_Prefix__r.Renewal_Start_Period__c,Thumbprint__r.Program_Prefix__r.Renewal_End_Period__c,Total_Conditions__c,
                          Total_Supplemental_Conditions__c,UNI_Alternate_Phone__c,UNI_Country__c,UNI_County_Province__c,UNI_Zip__c,Expiry_Date__c,Share_Permit_Package_with_Applicant__c FROM authorizations__c 
                          WHERE Id=:authorizationID];//Applicant_Reference_Number__c, Removed by NUPUR         
            decisiontype=objauthitem.Authorization_Type__c;
            applicationID=objauthitem.Application__c;
            requiredReports='';
            requiredReports = getReqreports(objauthitem.Authorization_Type__c,objauthitem.Status__c,objauthitem.BRS_Introduction_Type__c,objauthitem.BRS_Purpose_of_Permit__c);

            if(applicationID!=null)AmendRenewalappllist();
        }
        else{
            displayCompAgreementButton=false;
        }
    }
    //W-027762 Added for displaying required reports
        string getReqreports(String Type,String Status,String MovementType,String PurposeofPermit){
       Required_Report_SelfReporting__mdt rReport;
        try{
       rReport=  [select Type__c,Status__c,MovementType__c,Required_Reports__c from Required_Report_SelfReporting__mdt where Type__c =:Type and Status__c =:Status and MovementType__c =:MovementType and Purpose_of_Permit__c =:PurposeofPermit];
        }catch(Exception ex){
            rReport = null;
        }
        if(rReport != null){
        return rReport.Required_Reports__c;
            }else
            {
                return '';
            }
    }
    /** 
* Notes and Attachments of Authorization
**/ 
    public List<Attachment> getattach(){
        return [SELECT Name,Description,ContentType,CreatedById,CreatedBy.Name,Id,ParentId FROM Attachment WHERE ParentId=:authorizationID ORDER BY createddate];
    }
    /** 
* Line Items of Authorization
**/ 
    public List<AC__c> getList(){
        /* return [SELECT ID,Name,Scientific_Name__r.Name,Regulated_article__r.Name,Status__c,Color__c,Sex__c,
Number_of_Labels__c,Signature__c,Program_Line_Item_Pathway__r.Program__r.Name 
FROM AC__c WHERE Authorization__c=:authorizationID];
*/
        return [SELECT ID,Name,Purpose_of_the_Importation__c,Application_Number__c,Status__c,Color__c,RecordTypeID,Regulated_article__r.Name, 
                Sex__c,Domain__r.name,Domain__c,Authorization__c,Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.name, 
                program_Line_Item_Pathway__r.Program__c,Program_Line_Item_Pathway__r.Program__r.name,Thumbprint__c,Scientific_name__c,
                Scientific_name__r.name,RA_Scientific_name__r.Name,
                (SELECT Regulated_Article__c, 
                 Regulated_Article__r.name,
                 Scientific_Name__c 
                 FROM Link_Regulated_Articles__r) 
                FROM AC__c 
                WHERE Authorization__c=:authorizationID 
                AND (Line_Item_Type_Hidden_Flag__c='product' 
                     OR Line_Item_Type_Hidden_Flag__c='')]; 
        
        
    }
    /** 
* Permit Package of Authorization
**/ 
    public List<Applicant_Attachments__c> getappattach(){
        return [SELECT ID,Name,File_Name__c,RecordTypeID,Attachment_Type__c,CreatedById,Document_Types__c,File_Description__c,Tags__c,Total_Files__c,(SELECT Name,Description,ContentType,CreatedById,Id,ParentId FROM Attachments) FROM Applicant_Attachments__c WHERE Authorization__c=:authorizationID and RecordTypeID=:permitpackRecTypeId];
    }
    
    
    public void deleteSummary() {
        String idVal = EFLGenericUtility.sanitizeString(System.currentPageReference().getParameters().get('idVal'));
        if( idVal != null && idVal != '' ) {
            delete [Select id from Report_Summary__c Where Id =: idVal];
        }
    }
    
    
    // Adding Action Items of BRS line items 
    public PageReference ActionItems(){
        PageReference actionitempg = new PageReference('/apex/CARPOL_BRS_WaitingPage');
        list<AC__c> lineitemlist= [select id,name,Application_Number__c,Program_Line_Item_Pathway__c from AC__C where Application_Number__c=:applicationID];
        //system.debug('!!!!!!!!lineitemlist'+lineitemlist);
        for(AC__c a:lineitemlist ){
            actionitempg.getParameters().put('Id',a.Id);
            actionitempg.getParameters().put('appId',a.Application_Number__c);
            actionitempg.getParameters().put('strPathwayId',a.Program_Line_Item_Pathway__c);
        }
        
        //system.debug('!!!!!!actionitempg'+actionitempg);
        actionitempg.setRedirect(true);
        
        return actionitempg;
    }
    /** 
* Amendment Renewal of Application
**/     
    public pagereference AmendRenewalappllist(){
        if(applicationID!=null&&decisiontype=='Permit'&&decisiontype!=''){
            //RR  4/16 - begin
            //id lineitemid=[SELECT id,Application_Number__c FROM AC__c WHERE Application_Number__c=:applicationID limit 1].Id;
            id lineitemid;
            List<AC__c> lineItem = [SELECT id,Application_Number__c FROM AC__c WHERE Application_Number__c=:applicationID limit 1]; 
            if(lineItem != null && lineItem.size() > 0){
                lineitemid= lineItem[0].Id;
            }
            //RR  4/16 - end
           
            if(lineitemid!=null){
                ARlineitems=[SELECT ID,Parent_Line_Item__c,Parent_line_item__r.Application_Number__c,child_line_item__c,Parent_Line_Item__r.id,child_line_item__r.Application_Number__c,RecordTypeID,RecordType.name FROM Amendment_Renewal__c WHERE (Parent_Line_Item__c=:lineitemid OR child_line_item__c=:lineitemid)];
                if(ARlineitems.size()>0){
                    for(Amendment_Renewal__c ARLine:ARlineitems){
                        if(ARLine.Parent_line_item__r.Application_Number__c!=applicationID)lineitemids.add(ARLine.Parent_Line_Item__r.id);
                        if(ARLine.child_line_item__r.Application_Number__c!=applicationID)lineitemids.add(ARLine.child_line_item__r.id);
                    }
                }
                if(lineitemids.size()>0)lineitemstoshow=[SELECT id,Name,Application_Number__c,Application_Number__r.Application_Status__c,Application_Number__r.Application_Type__c,Authorization__c,Authorization__r.Status__c FROM AC__c WHERE id=:lineitemids];
            }
        }
        return null;
    }
    /** 
* Agree Compliance logic
**/     
    public pagereference agreeCompliance(){
        Workflow_Task__c wf=new  Workflow_Task__c();
        wf.Id=wfLst[0].Id;
        wf.Status__c='Complete';
        Update wf;
        pageReference pg=Page.Portal_Authorization_Detail2;
        pg.getParameters().put('Id',authorizationID);
        pg.setRedirect(true);
        return pg;
    }
    /** 
* Reject Compliance logic
**/ 
    public pagereference rejectCompliance(){
        Workflow_Task__c wf=new  Workflow_Task__c();
        wf.Id=wfLst[0].Id;
        wf.Status__c='Pending';
        wf.Status_Categories__c='Other';
        Update wf;
        pageReference pg=Page.Portal_Authorization_Detail2;
        pg.getParameters().put('Id',authorizationID);
        pg.setRedirect(true);
        return pg;
    }
    /** 
* Amendment/Renewal 
**/
    public pageReference AmendmentAndRenew(){
        PageReference pg=Page.CARPOL_UNI_AmendmentsorRenewals;
        pg.getParameters().put('id',authorizationID);
        return pg;
    }
    /** 
* Self Reporting 
**/
    public List<Applicant_Attachments__c> getSelfReporting(){
        return [SELECT ID,Name,Animal_Care_AC__c,Attachment_Type__c,File_Description__c,RecordType.Name,Document_Types__c,File_Name__c,Status__c,Standard_Operating_Procedure__r.Name,Status_Graphical__c,Total_Files__c,(SELECT ID,Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Authorization__c=:authorizationID and recordTypeId=:recordTypeId];
    }
    /** 
* Delete Documents 
**/ 
    public PageReference deleteRecord(){
        string sobjkey=delrecid.substring(0,3);
        string sobjname=sobjectkeys.get(sobjkey);
        string strqurey='select id from '+sobjname+' where id=:delrecid';
        list<sobject> lst=database.query(strqurey);
        Delete lst;
        PageReference dirpage=new  PageReference('/apex/Portal_Authorization_Detail2?LineId='+authorizationID);
        dirpage.setRedirect(true);
        return dirpage;
    }
    /** 
* Add Self Reporting Documents 
**/
    /* KK Commented 06/15/2019
    public pageReference AddingSRP(){
        redirect='yes';
        recTypeName='Self Reporting';
        recordTypeId=[SELECT ID FROM Recordtype WHERE Name=:recTypeName].ID;
        pageReference pg=Page.portal_selfreporting;
        pg.getParameters().put('RecordType',recordTypeId);
        pg.getParameters().put('authid',authorizationID);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    } */
    /** 
* Return URL
**/ 
    public string getreturnURL(){
        pageReference pg=ApexPages.currentPage();
        pg.getParameters().put('showReqDocsSection','true');
        string newURL=pg.getUrl();
        return newURL;
    }
    
    /**
* Report Summary
**/
    public List<Report_Summary__c> reportsummary = new List<Report_Summary__c>();
    public List<Report_Summary__c> getReportSummary(){
        
        reportsummary = [SELECT ID,Name,Report_Type__c,LastModifiedDate, CreatedDate, Due_date__c,(SELECT ID,Name FROM Attachments LIMIT 1) FROM Report_Summary__c WHERE Authorization__c=:authorizationID ORDER BY LastModifiedDate DESC]; 
        return reportsummary;
        
    }    
    
    public Pagereference showPopup()
    {       
        if(selectedAuthid != null){
            Authorizations__c authwithdraw= [SELECT ID,Name,Authorization_Type__c,Status__c,Fee_Amount__c,(select Id,Name,Authorization__c,Status__c FROM Animal_Care_AC_Authorization__r) FROM Authorizations__c WHERE Id=:selectedAuthid limit 1];
            set<Id> lnid=new set<Id>();
            if(authwithdraw.Animal_Care_AC_Authorization__r.size() > 0){
                for(AC__c ln:authwithdraw.Animal_Care_AC_Authorization__r){
                    if(ln.Id != null){
                        lnid.add(ln.Id);
                    }
                }       
            }
            if(lnid != null && lnid.size() > 0 && lnid.size() == 1){
                authwithdraw.Status__c='Withdrawn';
                update authwithdraw;
                List<AC__c> WthdrAuthln = [select Id, Application_Number__c, name, status__c from AC__c where Id = :lnid];
                List<Application__c> apps = new List<Application__c>();
                for(AC__C l:wthdrAuthln){
                    l.Status__c ='Withdrawn';
                    apps.add(new Application__c(id=l.Application_Number__c,Application_Status__c='Withdrawn'));
                }
                List<Workflow_Task__c> WthdrAuthWft = [select Id, name, status__c from Workflow_Task__c where Authorization__c = :selectedAuthid];
                for(Workflow_Task__c wt:WthdrAuthWft){
                    wt.Status__c = 'Deferred';
                    
                }
                if(WthdrAuthln != null){
                    update WthdrAuthln;
                }
                if(!apps.isEmpty())
                {
                    update apps;
                }
                if(WthdrAuthWft != null){
                    update WthdrAuthWft;
                }
                PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
                pageRef.setRedirect(true);
                return pageRef;
            }
            
            if(lnid.size()>1){
                /*wrlnlst=new List<wraplnlst>();
List<AC__c> WthdrAuthln2 = [select Id, name, status__c,Intended_Use__r.name,Regulated_Article__r.name,Country_Of_Origin__r.name from AC__c where Id = :lnid];
for(AC__c wtdln:WthdrAuthln2){
wrlnlst.add(new wraplnlst(false,wtdln));
} */
                Pagereference pf=new Pagereference('/apex/EFLSelectLinetoWithdraw?said='+selectedAuthid);  
                pf.setredirect(true);
                return pf;   
                
            }
            
        }
        return null;
    }
    
    // Adding Action Items of BRS line items 
    public PageReference BRSItemDetails(){
        //PageReference actionitempg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');
        PageReference actionitempg = new PageReference('/apex/EFLLineItem');
        list<AC__c> lineitemlist= [select id,name,Application_Number__c,Program_Line_Item_Pathway__c from AC__C where Application_Number__c=:applicationID];
        for(AC__c a:lineitemlist ){
            actionitempg.getParameters().put('Id',a.Id);
            /*actionitempg.getParameters().put('LineId',a.Id);
actionitempg.getParameters().put('appId',a.Application_Number__c);
actionitempg.getParameters().put('strPathwayId',a.Program_Line_Item_Pathway__c);*/
        }
        
        actionitempg.setRedirect(true);
        
        return actionitempg;
    }    
    
    public class wraplnlst{
        public Boolean checkselect{get; set;}
        public AC__C lnrec{get; set;}
        public wraplnlst(boolean bc,AC__C l){
            checkselect=bc;
            lnrec=l;
        }
    } 
    
    public List<wraplnlst> wrlnlst{get; set;} 
    
}