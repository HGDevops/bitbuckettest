/* ***************************************************************************************
-- Begin Default  ---
** Class Name     : EQS_RFRTriggerHandler 
** Description   : Helper Class  
** Technical Info : Helper class for the apextrigger EQS_RFRTrigger
** Author(s)     : Harish Gudipudi (HG)
**        Revision History:-
** Version  Date        Author  Description of Action
** 1.0      08/24/2018 HG      Initiated Script  
 
**************************************************************************************** **/

public class EQS_RFRTriggerHandler {
    public static void updateRollUponPosCert(set<Id> rfrIds){
        Integer noOfRfrs, noOfApproved, noOfPending, noOfDispatcher, noOfRejected, noOfTotalEvaluated, noOfRequested, noOfVolunteer ; 
        list<EQS_Incident_Position_Certification__c> poscertToBeUpdated = new list<EQS_Incident_Position_Certification__c>();
        for(EQS_Incident_Position_Certification__c c : [SELECT Id, EQS_Approved__c, EQS_Rejected__c, EQS_Total_Evaluated__c, EQS_Requested__c, EQS_Volunteer__c, 
                    EQS_In_Progress__c, EQS_Released_By_Dispatcher__c,  
                    (Select Id, EQS_Status__c, EQS_Type__c FROM Resource_Orders__r)
                    FROM EQS_Incident_Position_Certification__c WHERE Id IN : rfrIds
        ]){
            noOfRfrs = c.Resource_Orders__r.size();
            noOfApproved = 0;
            noOfPending = 0;
            noOfDispatcher = 0;
            noOfRejected = 0;
            noOfRequested = 0;
            noOfVolunteer = 0;
            noOfTotalEvaluated = 0;
            for(EQS_Resource_Request__c s: c.Resource_Orders__r){
                if(s.EQS_Status__c == 'Final Approval'){
                    noOfApproved++;
                }
                else if(s.EQS_Status__c == 'Released By Dispatcher'){
                    noOfDispatcher++;
                }
                else if(s.EQS_Status__c == 'Rejected'){
                    noOfRejected++;
                }
                else if(s.EQS_Status__c == 'Pending - Under Dispatcher Consideration'||s.EQS_Status__c == 'Pending - Volunteer Application Submission'||s.EQS_Status__c == 'Pending - Volunteer Application Approved by Supervisor'||s.EQS_Status__c == 'Reserved by Dispatcher (Qualified)'){
                    noOfPending++;
                }
                if(s.EQS_Type__c == 'Requested'){
                    noOfRequested++;
                }
                else if(s.EQS_Type__c == 'Volunteer'){
                    noOfVolunteer++;
                }
                if(s.EQS_Type__c == 'Requested'||s.EQS_Type__c == 'Volunteer'||s.EQS_Type__c == 'VERRC'){
                    noOfTotalEvaluated++;
                }
            }
            c.EQS_Total_Evaluated__c =  noOfTotalEvaluated;
            c.EQS_Approved__c =  noOfApproved;
            c.EQS_Rejected__c =  noOfRejected;
            c.EQS_In_Progress__c =  noOfPending;
            c.EQS_Released_By_Dispatcher__c = noOfDispatcher;
            c.EQS_Requested__c = noOfRequested;
            c.EQS_Volunteer__c = noOfVolunteer;
            poscertToBeUpdated.add(c);
        }
        if(!poscertToBeUpdated.isEmpty()){
            update poscertToBeUpdated;
        }   
    }
    
    public static void updateLastTDYonContact(Set<Id> conIds){       
    List<Contact> con = new List<Contact>();
    List<AggregateResult> requests = new List<AggregateResult>();
     
    con = [SELECT Id, EQS_Last_TDY__c FROM Contact WHERE Id IN: conIds];    
    requests = [SELECT EQS_Responder__c, MAX(EQS_Last_TDY__c) LastTDY FROM EQS_Resource_Request__c WHERE EQS_Responder__c IN: conIds GROUP BY EQS_Responder__c ];
           
    for(Contact c: con){
        Date Lastdate;        
        for (AggregateResult b : requests){         
          if (b.get('EQS_Responder__c')==c.Id){ 
                if(b.get('LastTDY') != NULL){
                    Lastdate = (Date)b.get('LastTDY');
                    system.debug('@@@@'+Lastdate );
                }                 
            }
        } 
        c.EQS_Last_TDY__c = Lastdate;
    }
    update (con);  
  }

}