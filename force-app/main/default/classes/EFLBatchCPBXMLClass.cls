// This class generates the XML summary of the data to be supplied to CBP.  Transmission mechanism TBD
// Currently running by VF Page of the same name, which should be removed at a later date
// Author: Dawn Sangree
// Last Modified 7-25-2017

public with sharing class EFLBatchCPBXMLClass {

    public String xmlString {get;set;}

    
    public EFLBatchCPBXMLClass() {
    }
    
    public void getXML(){

        //Run date, set here and used throughout xml generation 
        DateTime rundate = DateTime.Now();
        String rundateString = rundate.format('yyyy-MM-dd\'T\'HH:mm:ssXXX');
        String completeFileNameDate = rundate.format('yyyyMMdd');
        String fileformatDate = rundate.format('yyyyMMdd_HHmmss');
        string errors;
        string fileName;
        Integer countofApps = 0;
        Integer numOfApps;
        string appType;
        Map<string, string> mapErrors = new Map<string, string>();
        Map<string, string> mapAppType = new Map<string, string>();
        Map<string, Integer> mapAppCount = new Map<string, Integer>();
        Map<Id, string> mapPDFName = new Map<Id, string>();
        Map<Id, Id> mapAttachmentIds = new Map<Id, Id>();
        List<attachment> permitAttachs = new List<attachment>();
        List<attachment> attachToInsert = new List<attachment>();
        List<Authorizations__c> auths = new List<Authorizations__c>();
        Map<id, id> mapCBI = new Map<id, id>();
        Decimal attachmentSizeSent = 0;
        Boolean permitAttached;
       Id ApplicationId;
        
        
        //Generate an EFLIntegrationLog record so we can reference it in our XML        
        EFLIntegrationLog__c efllog = new EFLIntegrationLog__c();
        efllog.EFLIntegrationJobType__c = 'CBP Transfer';
        efllog.EFLJobRunDate__c = rundate;
        insert efllog;
    
        
        //calls the function to create the xml header with all the necessary info as noted in the spec document
        XmlStreamWriter w = createHeader(efllog.id, rundateString);
            //start MessageBody of main xml        
            w.writeStartElement(null, 'MessageBody', null);
                //start list of applications/permits
                w.writeStartElement(null, 'EPermitDataList', null);
                
               //pulling all appropriate authorizations that meet criteria and have been modified in the last 24 hours
                auths = [SELECT Application__c, Application__r.Name,Application__r.Id, Application__r.RelatedApplication__c, Application_CBI__c, Applicant__r.Firstname,Applicant__r.Lastname,Applicant__r.Account.Name,Application_Type__c,Authorization_Type__c,Date_Issued__c,Effective_Date__c,Expiration_Date__c,(Select Id, Name, CreatedDate From Attachments), Permit_Number__c,Status__c, Prefix_CBP__c, Name, prefix__c,(Select Id FROM Animal_Care_AC_Authorization__r) FROM Authorizations__c WHERE Status__c IN ('Issued', 'Cancelled', 'Amendment Issued','Renewal Issued','Withdrawn Customer','Revoked') AND Authorization_Type__c IN ('Permit') AND LastModifiedDate > yesterday AND Select_Agent__c = false and Expiration_Date__c >= TODAY AND Thumbprint__r.Program_Prefix__r.Include_In_CBP__c = TRUE];
                numOfApps = auths.size();

                
                    
                //for loop here to gather all the auth data
                
                for(integer index=0;index<auths.size();index++) 
                {
                     ApplicationId = auths.get(index).Application__r.id;
                    //pull out the CBI auths so the deleted PDFs can be pulled
                    if(auths.get(index).Application_CBI__c == 'Yes'){
                        //checking for a line item, if none, that is something to be investigated so sending to error log
                        if (auths.get(index).Animal_Care_AC_Authorization__r.size() == 0){
                            //keep up with the permit not found error in the error map so it can be show in the summary xml later
                             mapErrors.put(auths.get(index).Name, 'Error in ' + auths.get(index).name + ': Permit PDF not found.');
                             system.debug('auths.size() ' + auths.size());
                            system.debug('index cbp ' + index);
                            system.debug('auths.get(index).Name cbp ' + auths.get(index).Name);
                             auths.remove(index);
                             numOfApps = auths.size();
                             continue;

                              }

                        
                        for(AC__c line : auths.get(index).Animal_Care_AC_Authorization__r){
                            //for each line item, store the auth id and line item id so the cbi deleted attachment can be pulled later
                            mapCBI.put(auths.get(index).id, line.id);
                                  
                        }        
                    }
                    else {//else it is not a CBI auth, just pull the regular Permit PDF attachment
                        //a boolean field to keep up with whether a permit PDF was found for the auth, so an error can be written to the log if one was not found
                        permitAttached = false;
                        //looping through all the attachments on an auth to get the Permit PDF
                        for(attachment attach : auths.get(index).attachments){
                            String attchname = attach.Name.toLowercase();
                            //legacy, keep until we are certain esignlive work is complete
                            if(attchname.Contains('esigned')){
                                mapAttachmentIds.put(auths.get(index).Id, attach.id);
                                permitAttached = true;
                            }
                            //new nomenclature for esigned permits they start with the permit number
                            try{
                                    string permitNum = auths.get(index).Permit_Number__c;
                                    permitNum = permitNum.toLowerCase();
                                    if(attchname.Contains(permitNum)){
                                        mapAttachmentIds.put(auths.get(index).Id, attach.id);
                                        permitAttached = true;
                                    }
                                 }catch (Exception e) {
                               
                                
                                }                   
                                
                            //legacy, pick up permits
                            if(attchname.Contains('permit')){
                                if(!(attchname.Contains('draft'))){
                                    mapAttachmentIds.put(auths.get(index).Id, attach.id);
                                    permitAttached = true;
                                } 
                            }
                          }
                        //if the boolean field permitAttached is false at this point, we know that a permit was not found, so that is an error that needs to be invistigated.
                           if ( permitAttached == false){
                            mapErrors.put(auths.get(index).Name, 'Error in ' + auths.get(index).name + ': Permit PDF not found.');
                            
                            system.debug('auths.size() ' + auths.size());
                            system.debug('index ' + index);
                            auths.remove(index);
                            system.debug('auths.size() after ' + auths.size());
                            numOfApps = auths.size();
                                continue;
                                
                        }
                        
                        //create map with program prefix and app types
               
                }
                
                 for(Program_Prefix__c prefix : [Select EFLePermitApplicationType__c, Prefix__c, Name from Program_Prefix__c]){
                    mapAppType.put(prefix.Name, prefix.EFLePermitApplicationType__c);
                }
                    //removing charaters from the permit number so that the formating is exactly how it is supposed to be as noted in the spec document     
                    String removespace = auths.get(index).Name;
                    removespace= removespace.replaceAll( '\\s+', '');
                    //writing the permit number to the xml file
                        w.writeStartElement(null, 'EPermitData', null);
                        w.writeStartElement(null, 'PermitNumber', null);
                        //e-File standard for unique name, does not match ePermits naming convention 
                        system.debug('permit num ' + auths.get(index).Permit_Number__c);
                        if(auths.get(index).Permit_Number__c !='' || !(String.isBlank(auths.get(index).Permit_Number__c))){   
                            try{
                                w.writeCharacters(auths.get(index).Permit_Number__c);  
                            }catch (Exception e) {
                                //if the permit number is not populated, that is written to the error log in the summary xml
                              mapErrors.put(auths.get(index).Name, 'Error in ' + auths.get(index).name + ': Permit Type not found. ');
                                
                            }
                          
                        }
                        w.writeEndElement();                                                
                        
                        //writing the permit type to the xml
                        w.writeStartElement(null, 'PermitType', null);
                        try{
                            //getting the permit type from the map that was created based on the auth prefix number
                            appType = mapAppType.get(auths.get(index).Prefix_CBP__c);
                            w.writeCharacters(mapAppType.get(auths.get(index).Prefix_CBP__c));
                            
                            //checking to see if this permit type is in the map that keeps count of the number of permits of each type
                            if(!mapAppCount.containsKey(appType)){    
                                //if the permit type is not in the map yet, add it in
                                mapAppCount.put(appType, 0);
                            }
                        //this is where the counter gets incremented by 1 each time a permit of that particular type is processed
                        countofApps = integer.ValueOf(mapAppCount.get(appType)) + 1;
                        //updating the count of the permit type in the map
                        mapAppCount.put(appType,countofApps);
                    
                        } catch (Exception e) {
                            //if the permit type is not found, add an error to the error map so that it can be put in the summary xml
                            mapErrors.put(auths.get(index).Name, 'Error in ' + auths.get(index).name + ': Permit Type not found. ');
                        }
                     
                        w.writeEndElement();                 
                        
                        //writing the application number to the xml
                        w.writeStartElement(null, 'ApplicationNumber', null);
                        if(auths.get(index).Application__c != null){
                             w.writeCharacters(auths.get(index).Application__r.Name+'_'+removespace); 
                        }
                        w.writeEndElement();
                        
                        //writing the prior application number to the xml
                        w.writeStartElement(null, 'PriorApplicationNumber', null);
                        if(auths.get(index).Application__r.RelatedApplication__c != null){
                            w.writeCharacters(auths.get(index).Application__r.RelatedApplication__c); 
                        }
                        w.writeEndElement(); 
                        
                        //writing the application type to the xml
                        w.writeStartElement(null, 'ApplicationType', null);
                        if(auths.get(index).Application_Type__c !=null){
                            w.writeCharacters(auths.get(index).Application_Type__c.substring(0,1)); 
                        }                      
                                     
                        w.writeEndElement();                      
                        
                        //information about applicant from their contact information record
                        w.writeStartElement(null, 'ApplicantInfo', null);
                            //writing applicant first name to xml
                            w.writeStartElement('nc', 'PersonGivenName', 'http://niem.gov/niem/niem-core/2.0');
                            if(auths.get(index).Applicant__r.Firstname != null){
                                w.writeCharacters(auths.get(index).Applicant__r.Firstname);   
                            }
                            w.writeEndElement();   
                            
                            //writing applicant last name to xml
                            w.writeStartElement('nc', 'PersonSurName', 'http://niem.gov/niem/niem-core/2.0');
                            if(auths.get(index).Applicant__r.Lastname !=null){
                                w.writeCharacters(auths.get(index).Applicant__r.Lastname); 
                            }
                            w.writeEndElement();                                                                              
                            
                            //writing applicant account name to xml
                            w.writeStartElement('nc', 'OrganizationName', 'http://niem.gov/niem/niem-core/2.0');
                            if(auths.get(index).Applicant__r.Account.Name != null){
                                w.writeCharacters(auths.get(index).Applicant__r.Account.Name); 
                            }     
                            w.writeEndElement();                                                                                                          

                        w.writeEndElement();                                                                                                                      

                        //Date permit issued
                        w.writeStartElement(null, 'IssuedDate', null);
                        if(auths.get(index).Date_Issued__c != null){
                            DateTime issueddate = auths.get(index).Date_Issued__c;
                            w.writeCharacters(issueddate.format('yyyy-MM-dd\'T\'HH:mm:ssXXX'));  
                        }
                        w.writeEndElement();                                                
                    
                        //Date permit expires
                        w.writeStartElement(null, 'ExpirationDate', null);
                        if(auths.get(index).Expiration_Date__c != null){
                            DateTime expdate= auths.get(index).Expiration_Date__c;
                            w.writeCharacters(expdate.format('yyyy-MM-dd\'T\'HH:mm:ssXXX'));   
                        }
                        w.writeEndElement();                                                
                    
                        //Date permit is effective
                        w.writeStartElement(null, 'EffectiveDate', null);
                        if(auths.get(index).Effective_Date__c != null){
                            DateTime effectdate = auths.get(index).Effective_Date__c;
                            w.writeCharacters(effectdate.format('yyyy-MM-dd\'T\'HH:mm:ssXXX')); 
                        }
                        w.writeEndElement();                                                
                        
                        //writing the permit status to the xml
                        w.writeStartElement(null, 'PermitStatus', null);
                        if(auths.get(index).Status__c != null){
                            if(auths.get(index).Status__c == 'Issued'){
                                w.writeCharacters(auths.get(index).Status__c.substring(0,1));
                            }
                            //if the status is 'Withdrawn, Withdrawn Customer, or Cancelled, writing 'C' to the xml for permit status
                            if(auths.get(index).Status__c == 'Withdrawn' || auths.get(index).Status__c == 'Withdrawn Customer' || auths.get(index).Status__c == 'Cancelled'){
                                String Cancelledpermitstring = 'Cancelled';
                                w.writeCharacters(Cancelledpermitstring.substring(0,1));
                           }
                           //if the status is 'Amendment Issued', writing 'S' to the xml for permit status
                           if(auths.get(index).Status__c == 'Amendment Issued'){
                                String Amendmentpermitstring = 'Superseded Permit';
                                w.writeCharacters(Amendmentpermitstring.substring(0,1));
                           }
                           //if the status is 'Revoked', writing 'X' to the xml for permit status
                           if(auths.get(index).Status__c == 'Revoked'){
                                String Suspendedpermitstring = 'Xsuspended Permit ';
                                w.writeCharacters(Suspendedpermitstring.substring(0,1));
                           }                          
                        }
                        w.writeEndElement();                                                
                        
                        //formatting and writing the completed file name to the xml and putting it in a PDF name map so it can be pulled later to save as the name for the permit PDF
                        w.writeStartElement(null, 'CompleteFileName', null);
                        fileName = auths.get(index).Permit_Number__c + '_'+auths.get(index).Application__r.Name +'_'+removespace+'_'+completeFileNameDate+'.pdf';
                            w.writeCharacters(fileName);   
                            mapPDFName.put(auths.get(index).id, fileName);
                        
                        w.writeEndElement();                                                
                    
                      w.writeEndElement(); 
                                                                 
                    } //end for loop
                    
                w.writeEndElement();            
            w.writeEndElement(); //end MessageBody
        w.writeEndElement();  //end ITDS-PGA-x:MessageEnvelope

        string xml = w.getXmlString();
        xmlString = xml;
        
        w.close();
        system.debug(xml);
        
       
        
        //write xml to log and attach it
        attachment att = createAttachment('EFile_'+fileformatDate+'.xml', efllog.Id, null, xml);
        //add the xml attachment to the list of attachments to be inserted all at once to cut down on the number of DMl statements
        attachToInsert.add(att);
        
        //getting the body and other important info of all of the permit PDF attachments from the auths
        permitAttachs =  [Select id,name, body, parentid, CreatedDate from attachment Where Id in: mapAttachmentIds.values() order by CreatedDate];
        
        //loop through the list of permit PDF attachments
        for(attachment permAttachment : permitAttachs) {
        //PDF as attachment to log
        try{ 
            //create a new attachment and insert the Permit PDF to the log
            Attachment pdfattachment = new Attachment();
            pdfattachment.ParentId = efllog.Id;
            string pdfName = mapPDFName.get(permAttachment.parentid);
            pdfattachment.Name = pdfName;  
            pdfattachment.body = permAttachment.body; //This copies the permit pdf to the log file
            insert pdfattachment;
            //attachmentSizeSent keeps up with the amount of total bytes of all the permit PDF attachments processed
            attachmentSizeSent += pdfAttachment.body.size();
            }catch(Exception e)
             {
                e.getMessage();
             }
            }
        //loop through the cbi deleted map and call the function to get the cbi deleted version of permit pdf   
        for (Id CBIKey : mapCBI.keySet()){
            fileName = mapPDFName.get(CBIKey); // get the file name of what the permit PDF needs to be to send to the cbi deleted function
          attachmentSizeSent = getCBIDeleted(CBIKey, mapCBI.get(CBIKey), fileName,efllog.id, attachmentSizeSent);
       }
       
        
        XmlStreamWriter ws = createHeader(efllog.id, rundateString); //create the summary header
            //start MessageBody of summary/reconciliation file         
            ws.writeStartElement(null, 'MessageBody', null);                     
                ws.writeStartElement(null, 'EPermitReconciliationDataList', null);
                //writing the total bytes transferred to the xml
                    ws.writeStartElement(null, 'EPermitTotalDataTransferred', null);
                    //if there are permit PDFs sent, calculate the amount sent in KB formatted out to two decimal places
                    if(attachmentSizeSent != 0)
                        attachmentSizeSent = attachmentSizeSent/1000;
                    ws.writeCharacters(string.valueof(attachmentSizeSent.SetSCale(2)) + ' KB');
                    ws.writeEndElement();                                                                

                    //writing the permit count list to the xml
                    ws.writeStartElement(null, 'EPermitPermitCountList', null);
                    
                    system.debug('Begin summary xml');
                    ws.writeStartElement(null, 'EPermitPermitCount', null);
                    //looping through the permit types in the map and getting the count of each
                    for(string permitMap : mapAppCount.keySet()){
                        
                            //writing the permit type to the xml
                            ws.writeStartElement(null, 'PermitType', null);
                            ws.writeCharacters(permitMap);
                            ws.writeEndElement();                                                                
                            //The number of permits for a given type
                            ws.writeStartElement(null, 'PermitCount', null);
                            ws.writeCharacters(String.valueOf(mapAppCount.get(permitMap)));
                            ws.writeEndElement();          
                                
                         }                                                                                  
                        ws.writeEndElement(); 
                        
                        //writing the total number of permits procesed in this transaction
                        ws.writeStartElement(null, 'TotalPermitCount', null);
                            ws.writeCharacters('Total Number of Permits: ' + numOfApps);
                            ws.writeEndElement();
                    ws.writeEndElement();
                    
                    //writing the error log list to the xml
                    ws.writeStartElement(null, 'ErrorLog', null);
                            ws.writeCharacters('List of errors:');
                            
                            //looping through all the errors and displaying them
                            for (string errorsMap : mapErrors.keySet()){
                                ws.writeStartElement(null, 'Errors', null);
                      
                                ws.writeCharacters(mapErrors.get(errorsMap));
                                ws.writeEndElement();       
                            }
                    ws.writeEndElement();         
                ws.writeEndElement();                                                
            ws.writeEndElement(); //end MessageBody
        ws.writeEndElement();  //end ITDS-PGA-x:MessageEnvelope

        String xmlsummary = ws.getXmlString();
        ws.close();
        
        //write xml to log for now
        //TODO:  This data is supposed to be emailed according to ePermits spec
        Attachment attws = createAttachment('Reconciliation_EFile_'+fileformatDate+'.xml', efllog.id, null, xmlsummary);
        attachToInsert.add(attws);

        
        insert attachToInsert;
        
        
     }
     
     //Function to create a new attachment
     public attachment createAttachment(string Name, Id Parentid, Blob attachBody, string xml){
        Attachment att = new Attachment();
        att.Name = Name;
        att.ParentId = Parentid;
        if (xml != '' || xml != null){
            Blob body = Blob.valueOf(xml);
            att.body = body;
        }
        else {
            att.body = attachBody;
        }
        
        return att;
     }
     
     
         
     //function that creates the xml header for the main xml and the summary xml
     public XmlStreamWriter createHeader(Id eflogId, string rdate){
         
         XmlStreamWriter ws =new XmlStreamWriter();
         //xml declaration
        ws.writeStartDocument('UTF-8', '1.0');
        //top level element and namespaces
        ws.writeStartElement(null,'ITDS-PGA-x:MessageEnvelope', null); 
        ws.writeDefaultNamespace('http://cbp.dhs.gov/ITDS-PGA-e');
        ws.writeNamespace('ITDS-PGA-x', 'http://cbp.dhs.gov/ITDS-PGA-x');
        ws.writeNamespace('nc', 'http://niem.gov/niem/niem-core/2.0');
        ws.writeNamespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        ws.writeAttribute('xsi', 'http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation', 'http://cbp.dhs.gov/ITDS-PGA-x ../exchange/ITDS-PGA-IWS-x.xsd');
            //message header        
            ws.writeStartElement(null, 'MessageHeader', null);
                //According to CBP, this is the schema version for this XML block
                ws.writeStartElement(null, 'MessageVersionText', null);
                ws.writeCharacters('2.79.06'); 
                ws.writeEndElement();            
                
                //The id of our integration log record for tracking
                ws.writeStartElement(null, 'MessageID', null);
                ws.writeCharacters(eflogId); 
                ws.writeEndElement();        
                
                ws.writeStartElement(null, 'MessageType', null);
                ws.writeCharacters('EFileData');
                ws.writeEndElement();                                                

                //Transmission date of this job  
                ws.writeStartElement(null, 'MessageSentDate', null);
                ws.writeCharacters(rdate);
                ws.writeEndElement();     
                
                ws.writeStartElement(null, 'MessageSenderID', null);
                ws.writeCharacters('APHIS-F');
                ws.writeEndElement();                                                
                                                                           
                ws.writeStartElement(null, 'MessageSenderSystemID', null);
                ws.writeCharacters('APHIS-F1');
                ws.writeEndElement();                                                
            //end MessageHeader                
            ws.writeEndElement(); 
            
            return ws;

     }
     
     //function that retrieves the CBI Deleted version of the permit PDF for an auth
     public Decimal getCBIDeleted(ID authId,Id lineId,string fileName,Id eflLogid, Decimal attachmentSizeSent){
         //sets up the page the we are calling to get the cbi deleted version
          PageReference pdf = new PageReference('/apex/EFLBRSApplicationPDF?id='+lineId+'&Version=CBIDeleted');
        //creating a new attachment for the CBI deleted version
        Attachment CBIatt=new  Attachment();
        Blob permitBody;
        try{
         //returning the CBI deleted version of the permit as a PDF
         //also checking if this is a unit test because you cannot call getcontent in a unit test
         if(!Test.isrunningTest()){
          permitBody=pdf.getContentAsPDF();
         }
         else{
             //if this is a unit test you have to give the body a value, but you cannot call getContent
             permitBody = Blob.valueOf('Unit Test');
         }
        }
        catch(VisualforceException e){
          system.debug('error cbp');
        }
        //setting up the rest of the attachment info and inserting the attachment to the log
        CBIatt.Name=fileName;
        CBIatt.ParentId=eflLogid;
        CBIatt.body=permitBody;
        insert CBIatt;
        //keeping up with the Total permit data transferred including any CBI deleted PDFs
        attachmentSizeSent += CBIatt.body.size();
        return attachmentSizeSent;
       }    
      
}