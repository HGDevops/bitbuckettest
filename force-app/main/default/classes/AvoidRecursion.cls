public Class AvoidRecursion{
    @TestVisible private static boolean firstRunBeforeInsert = true;
    @TestVisible private static boolean firstRunBeforeUpdate = true;
    
    @TestVisible private static boolean firstRunBulkBefore = true;
    @TestVisible private static boolean firstRunBulkAfter = true;
    
    public static boolean isFirstRunBeforeInsert(){
    if(firstRunBeforeInsert){
      firstRunBeforeInsert = false;
      return true;
    }else{
        return firstRunBeforeInsert;
    }
    }
    public static boolean isFirstRunBeforeUpdate(){
    if(firstRunBeforeUpdate){
      firstRunBeforeUpdate = false;
      return true;
    }else{
        return firstRunBeforeUpdate;
    }
    }
    
    public static boolean isfirstRunBulkBefore(){
    if(firstRunBulkBefore){
      firstRunBulkBefore = false;
      return true;
    }else{
        return firstRunBulkBefore;
    }
    }
    public static boolean isfirstRunBulkAfter(){
    if(firstRunBulkAfter){
      firstRunBulkAfter = false;
      return true;
    }else{
        return firstRunBulkAfter;
    }
    }
}