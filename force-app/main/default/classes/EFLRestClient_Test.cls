@isTest 
private class EFLRestClient_Test {
    static testMethod void validateGet() {
        RestClient restobj = new RestClient();
        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new EFLRestClientMockImpl());
        restobj = new RestClient('https://www.accenture.com', 'null', 'null');
        restobj = new RestClient('https://www.accenture.com', 'null');
        restobj = new RestClient('https://www.accenture.com', 'null', new map<String, String>());
        // Why so many Get and Request. No one is invoking it.
        restobj.get('https://www.accenture.com');
        restobj.get('https://www.accenture.com', new map<String, String>(), 'null');
        restobj.get('https://www.accenture.com', new map<String, String>());
        restobj.request('https://www.accenture.com', 'null', new map<String, String>{'Content-Type' => 'application/json'});
        restobj.request('https://www.accenture.com', 'null', 'null');
        restobj.request('https://www.accenture.com', 'null');
        restobj.handleResponse(new HttpResponse());
        HttpResponse res = restobj.post('https://www.accenture.com', new Map<String, String>(), 'null');
        Integer respCode = res.getStatusCode();
        
        String respBody = res.getBody();
        Test.stopTest();

        system.assert(respCode==200);
        system.assert(respBody.containsIgnoreCase('Number of records returned are 0'));
    }
}