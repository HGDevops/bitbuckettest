public inherited sharing virtual class EFLRelatedListServices {
    public virtual void doDelete(list<Id> recordIds, string ObjectName, boolean doDelete){
        list<sObject> objs = new list<sObject>();
        for(Id i : recordIds){
            sObject sObj = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;
            sObj.put('Id',i);
            objs.add(sObj);
        }
        delete objs;
    }
    
    public virtual list<sObject> getRecords(EFL_Related_List_Instance__mdt instance, set<string> fields, Id parentId){
        fields.add('Id');
        fields.add('Name');
        list<string> fieldList = new list<string>();
        fieldList.addAll(fields);
        // make a dynamic query & get all the records.
        string qry = 'SELECT ' + string.join(fieldList,',');
        qry += ' FROM ' + instance.SObject_Type__c;
        qry += ' WHERE ' + instance.Parent_Lookup_Field__c + ' = \'' + parentId + '\'';
        if(instance.Query_Filter__c != null){
            qry += ' AND ' + instance.Query_Filter__c;
        }
        if(instance.Order_By__c != null){
            qry += ' ORDER BY ' + instance.Order_By__c;
        }
        system.debug('query: ' + qry);
        return Database.query(qry);
    }
}