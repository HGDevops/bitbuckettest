@isTest
private class EFLContactTriggerHandlerTest{
    static testMethod void testCreateAccountFromContact() {
        string AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
        Account objacct = new  Account();
        objacct.Name = 'Global Account';
        objacct.RecordTypeId = AccountRecordTypeId;
        Insert objacct;
        
        // String ContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Owner').getRecordTypeId();
        String ContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Contact').getRecordTypeId();
        Contact objcont = new Contact();
        objcont.FirstName = 'FirstName';
        objcont.LastName = 'LastName';
        objcont.Email = 'test@email.com';
        objcont.MailingStreet = 'Mailing Street';
        objcont.MailingCity = 'Mailing City';
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '43002';
        objcont.RecordTypeId = ContRecordTypeId;
        objcont.AccountId = objacct.id;
        insert objcont;
        system.assert(objcont != null);                                   
    }    
    
    static testMethod void testCreateNewAccountFromContact() {
        String ContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual_Owner').getRecordTypeId();
        Contact objcont = new Contact();
        objcont.FirstName = 'FirstName';
        objcont.LastName = 'LastName';
        objcont.Email = 'test@email.com';
        objcont.MailingStreet = 'Mailing Street';
        objcont.MailingCity = 'Mailing City';
        objcont.MailingState = 'Ohio';
        objcont.MailingCountry = 'United States';
        objcont.MailingPostalCode = '43002';
        objcont.RecordTypeId = ContRecordTypeId;
        objcont.Individual_Owner_Account__c = false;
        insert objcont;
        system.assert(objcont != null);                                   
    }    
        
    static testMethod void testNonFAAccount() {
        string AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
        Account account = new Account();
        account.Name = 'TestAccount';
        account.RecordTypeId = AccountRecordTypeId;
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'FirstName1';
        contact.LastName = 'LastName';
        contact.EFL_Facility_Admin__c = true;
        contact.AccountId = account.Id;
        insert contact;
        
        Contact contact2 = new Contact();
        contact2.FirstName = 'FirstName2';
        contact2.LastName = 'LastName';
        contact2.EFL_Facility_Admin__c = false;
        contact2.AccountId = account.Id;
        insert contact2;
        
        Contact contact3 = new Contact();
        contact3.FirstName = 'FirstName3';
        contact3.LastName = 'LastName';
        contact3.EFL_Facility_Admin__c = false;
        contact3.AccountId = account.Id;
        insert contact3;
        
        contact2.EFL_Facility_Admin__c = true;
        contact3.EFL_Facility_Admin__c = true;
        List<Contact> contactsToUpdate = new List<Contact>();
        contactsToUpdate.add(contact2);
        contactsToUpdate.add(contact3);
        
        update contactsToUpdate;
    } 
    
    static testMethod void testMoreThanTwoFacilityAdminsUpdate() {
        Account account = new Account();
        account.Name = 'TestAccount';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(EFLContactTriggerhandler.AC_RL_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        insert account;
        
        //set to the correct contact record type - AC RL Contact
        String contactRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
        Contact contact = new Contact();
        contact.RecordTypeId = contactRecordtypeId;
        contact.FirstName = 'FirstName1';
        contact.LastName = 'LastName';
        contact.EFL_Facility_Admin__c = true;
        contact.AccountId = account.Id;
        insert contact;
        
        Contact contact2 = new Contact();
        contact2.RecordTypeId = contactRecordtypeId;
        contact2.FirstName = 'FirstName2';
        contact2.LastName = 'LastName';
        contact2.EFL_Facility_Admin__c = false;
        contact2.AccountId = account.Id;
        insert contact2;
        
        Contact contact3 = new Contact();
        contact3.RecordTypeId = contactRecordtypeId;
        contact3.FirstName = 'FirstName3';
        contact3.LastName = 'LastName';
        contact3.EFL_Facility_Admin__c = false;
        contact3.AccountId = account.Id;
        insert contact3;
        
        contact2.EFL_Facility_Admin__c = true;
        contact3.EFL_Facility_Admin__c = true;
        List<Contact> contactsToUpdate = new List<Contact>();
        contactsToUpdate.add(contact2);
        contactsToUpdate.add(contact3);
        boolean exceptionCaught = false;
        try {
        	update contactsToUpdate;
        } catch(Exception e){
            exceptionCaught = true;
            System.debug(e.getMessage());
            System.assert(true, e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, There are already two Admins for this account.'));
        }
              
        System.assertEquals(true, exceptionCaught, 'Expecting an exception in this test.');
        System.assertEquals(true, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact.Id].EFL_Facility_Admin__c);
        System.assertEquals(false, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact2.Id].EFL_Facility_Admin__c);
        System.assertEquals(false, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact3.Id].EFL_Facility_Admin__c);
    }
    
    static testMethod void testMoreThanTwoFacilityAdminsInsert() {
        Account account = new Account();
        account.Name = 'TestAccount';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(EFLContactTriggerhandler.AC_RL_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        insert account;
        
        //set to the correct contact record type - AC RL Contact
        String contactRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
        Contact contact = new Contact();
        contact.RecordTypeId = contactRecordtypeId;
        contact.FirstName = 'FirstName1';
        contact.LastName = 'LastName';
        contact.EFL_Facility_Admin__c = true;
        contact.AccountId = account.Id;
        
        Contact contact2 = new Contact();
        contact2.RecordTypeId = contactRecordtypeId;
        contact2.FirstName = 'FirstName2';
        contact2.LastName = 'LastName';
        contact2.EFL_Facility_Admin__c = true;
        contact2.AccountId = account.Id;
        
        Contact contact3 = new Contact();
        contact3.RecordTypeId = contactRecordtypeId;
        contact3.FirstName = 'FirstName3';
        contact3.LastName = 'LastName';
        contact3.EFL_Facility_Admin__c = true;
        contact3.AccountId = account.Id;
        List<Contact> contactsToInsert = new List<Contact>{contact, contact2, contact3};
        
        boolean exceptionCaught = false;
        try {
        	 insert contactsToInsert;
        } catch(Exception e){
            exceptionCaught = true;
            System.debug(e.getMessage());
            System.assert(true, e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, There are already two Admins for this account.'));
        }
              
        System.assertEquals(true, exceptionCaught, 'Expecting an exception in this test.');
    }
    
    static testMethod void testMoreThanTwoFacilityAdminsMultipleAccounts() {
        Account account = new Account();
        account.Name = 'TestAccount';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(EFLContactTriggerhandler.AC_RL_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        insert account;
        Account account2 = new Account();
        account2.Name = 'TestAccount2';
        account2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(EFLContactTriggerhandler.AC_RL_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        insert account2;
        
        String contactRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
        Contact contact = new Contact();
        contact.RecordTypeId = contactRecordtypeId;
        contact.FirstName = 'FirstName1';
        contact.LastName = 'LastName';
        contact.EFL_Facility_Admin__c = true;
        contact.AccountId = account.Id;
        insert contact;
        
        Contact contact2 = new Contact();
        contact2.RecordTypeId = contactRecordtypeId;
        contact2.FirstName = 'FirstName2';
        contact2.LastName = 'LastName';
        contact2.EFL_Facility_Admin__c = false;
        contact2.AccountId = account.Id;
        insert contact2;
        
        Contact contact3 = new Contact();
        contact3.RecordTypeId = contactRecordtypeId;
        contact3.FirstName = 'FirstName3';
        contact3.LastName = 'LastName';
        contact3.EFL_Facility_Admin__c = false;
        contact3.AccountId = account.Id;
        insert contact3;
                
        Contact contact4 = new Contact();
        contact4.RecordTypeId = contactRecordtypeId;
        contact4.FirstName = 'FirstName3';
        contact4.LastName = 'LastName';
        contact4.EFL_Facility_Admin__c = false;
        contact4.AccountId = account.Id;
        insert contact4;        
        
        Contact contact5 = new Contact();
        contact5.RecordTypeId = contactRecordtypeId;
        contact5.FirstName = 'FirstName3';
        contact5.LastName = 'LastName';
        contact5.EFL_Facility_Admin__c = false;
        contact5.AccountId = account.Id;
        insert contact5;
        
        contact2.EFL_Facility_Admin__c = true;
        contact3.EFL_Facility_Admin__c = true;
        contact5.EFL_Facility_Admin__c = true;
        List<Contact> contactsToUpdate = new List<Contact>();
        contactsToUpdate.add(contact2);
        contactsToUpdate.add(contact3);
        contactsToUpdate.add(contact5);
        boolean exceptionCaught = false;
        try {
        	update contactsToUpdate;
        } catch(Exception e){
            exceptionCaught = true;
            System.debug(e.getMessage());
            System.assert(true, e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, There are already two Admins for this account.'));
        }
              
        System.assertEquals(true, exceptionCaught, 'Expecting an exception in this test.');     
        System.assertEquals(true, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact.Id].EFL_Facility_Admin__c);
        System.assertEquals(false, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact2.Id].EFL_Facility_Admin__c);
        System.assertEquals(false, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact3.Id].EFL_Facility_Admin__c);
        System.assertEquals(false, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact5.Id].EFL_Facility_Admin__c);
    }
    
    static testMethod void testZeroFacilityAdmins() {
        Account account = new Account();
        account.Name = 'TestAccount';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(EFLContactTriggerhandler.AC_RL_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        insert account;
        
        String contactRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
        Contact contact = new Contact();
        contact.RecordTypeId = contactRecordtypeId;
        contact.FirstName = 'FirstName1';
        contact.LastName = 'LastName';
        contact.EFL_Facility_Admin__c = true;
        contact.AccountId = account.Id;
        insert contact;
        
        contact.EFL_Facility_Admin__c = false;
        boolean exceptionCaught = false;
        try {
        	update contact;
        } catch(Exception e){
            exceptionCaught = true;
            System.debug(e.getMessage());
            System.assert(true, e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, Cannot remove the sole Admin on this account.'));
        }
              
        System.assertEquals(true, exceptionCaught, 'Expecting an exception in this test.');                
        System.assertEquals(true, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact.Id].EFL_Facility_Admin__c);
    }  
    
    static testMethod void testZeroFacilityAdminsAcisContact() {
        Account account = new Account();
        account.Name = 'TestAccount';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(EFLContactTriggerhandler.AC_RL_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        insert account;
        
        String contactRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
        Contact contact = new Contact();
        contact.RecordTypeId = contactRecordtypeId;
        contact.FirstName = 'FirstName1';
        contact.LastName = 'LastName';
        contact.AccountId = account.Id;
        contact.EFL_Facility_Admin__c = false;
        insert contact;
        
        Contact contact2 = new Contact();
        contact2.RecordTypeId = contactRecordtypeId;
        contact2.FirstName = 'FirstName1';
        contact2.LastName = 'LastName';
        contact2.EFL_Facility_Admin__c = true;
        contact2.AccountId = account.Id;
        contact2.EFL_ACIS_Contact__c = contact.Id;
        insert contact2;
        
        contact2.EFL_Facility_Admin__c = false;
        update contact2;
                          
        System.assertEquals(false, [SELECT Id, EFL_Facility_Admin__c FROM Contact WHERE Id = :contact2.Id].EFL_Facility_Admin__c);
    }   
}