public with sharing class EFLBarcodeDigit {
     public decimal width {get;set;}
     public decimal height {get;set;}
     public decimal positionVertical {get;set;}
     public boolean drawBar {get;set;}
     public boolean drawSpacing {get;set;}

}