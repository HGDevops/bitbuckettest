public inherited sharing class EFLInspectionUtility {

    public static boolean needsTaskCreation(Inspection__c oldInspectionRecord, Inspection__c inspectionRecord){
        
        if(oldInspectionRecord.Stage__c != inspectionRecord.stage__c){ 
            return true;
        } 
        return false;
    }
    
}