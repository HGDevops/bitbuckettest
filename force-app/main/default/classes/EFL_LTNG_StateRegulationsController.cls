/**
* Author : Kishore Kumar
* Created Date : 09/13/2017
* Purpose : This is used to State requirements
* Related Pages: EFL_StateRegulations  
*/
public with sharing class EFL_LTNG_StateRegulationsController {
    
    public Id authId {get;set;}
    public Id reviewerId {get;set;}
    public Id stateRecordTypeId;
    private final Reviewer__c orrRec;
    public string orrId;
    public Authorizations__c auth {get;set;}
    public Reviewer__c reviewer {get;set;}
    public final String RECORD_TYPE_NAME = 'State Regulation Junction';
    public List<Authorization_Junction__c> selectedConditions {get;set;}
    public List<Regulation__c> newRequirements {get;set;}
    public List<wrapperRegulation> regulationsList {get;set;}
    public List<wrapperRegulation> selectedRegulations {get;set;}
    String paramValue;
    public final String SCROLL_TO_CLASS = 'btn-wrapper';
    public string scrollTo {get; set;} 
    
    public EFL_LTNG_StateRegulationsController(ApexPages.StandardController controller) {    
        selectedConditions = new List<Authorization_Junction__c>();
        newRequirements = new List<Regulation__c>();
        regulationsList = new List<wrapperRegulation>();
        selectedRegulations = new List<wrapperRegulation>();
        //load junction recrods
        authId = (id)ApexPages.currentPage().getParameters().get('id');
        reviewerId = (id)ApexPages.currentPage().getParameters().get('orr');
        stateRecordTypeId = Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get(RECORD_TYPE_NAME).getRecordTypeId();
        
        try{
            if(authId != null){
                auth = [select id, Name from Authorizations__c where id =: authId limit 1];
                
                if(reviewerId != null){
                    reviewer = [select id, name, State__c, Authorization__c from Reviewer__c where id =: reviewerId limit 1];
                }
            }
        } catch(Exception e){
            EFLErrorLog.createErrorLog('EFL_LTNG_StateRegulationsController.EFL_LTNG_StateRegulationsController()',e); 
        }
        //if(paramValue != 'savebtn')    
        evaluateSelectedConditions();
    }
    
    public void evaluateSelectedConditions(){
        regulationsList = new List<wrapperRegulation>();
        selectedRegulations = new List<wrapperRegulation>();
        getPreviouslySelectedConditions();
        getNotSelectedConditions();
    }
    
    public void getPreviouslySelectedConditions(){
        
        try{
            selectedConditions = [select id, name,  
                                  Authorization__c,Regulation__c, 
                                  Authorization__r.Name,
                                  Regulation_Description__c,Title__c, 
                                  Regulation__r.Name 
                                  from Authorization_Junction__c
                                  where recordTypeId =: stateRecordTypeId 
                                  and Official_Review_Record__c =: reviewerId
                                  and Authorization__c =: authId];
        } catch(Exception e){
            EFLErrorLog.createErrorLog('EFL_LTNG_StateRegulationsController.getPreviouslySelectedConditions()',e); 
        }
    }
    
    //selected conditions needs to be populated before this method can be called.
    public void getNotSelectedConditions(){
        
        List<String> regulationIds = new List<String>();
        Map<String, Authorization_Junction__c> regAuthMap = new Map<String,Authorization_Junction__c>();
        for(Authorization_Junction__c aj: selectedConditions){
            regulationIds.add(aj.Regulation__c);
            regAuthMap.put(aj.Regulation__c, aj);
            selectedRegulations.add(new wrapperRegulation(aj));
        }
        
        try{
            for(Regulation__c reg: [select id, name, State__r.Name, Title__c, Regulation_Description__c from Regulation__c
                                    where id in: regulationIds]){
                                        //selectedRegulations.add(new wrapperRegulation(reg, regAuthMap.get(reg.Id)));
                                    }
            newRequirements = [select id, name, State__r.Name, Title__c, Regulation_Description__c from Regulation__c
                               where Type__c = 'State Conditions'
                               and id not in: regulationIds];
            
            for(Regulation__c reg: newRequirements)
                regulationsList.add(new wrapperRegulation(reg));
            
        } catch(Exception e){
            EFLErrorLog.createErrorLog('EFL_LTNG_StateRegulationsController.getNotSelectedConditions()',e); 
        }
    }
    
    public PageReference addSelected(){
        //We create a new list of Regulations that will be populated only with Regulations if they are selected
        List<Regulation__c> selectedRegs = new List<Regulation__c>();
        
        //We will cycle through our list of wrapperRegulation and will check to see if the selected property is set to true, 
        //if it is we add the Contact to the selectedContacts list
        for(wrapperRegulation wReg: regulationsList) {
            if(wReg.selected == true) {
                selectedRegs.add(wReg.reg);
            }
        }
        
        createRelatedConditions(selectedRegs);
        //re-evaluate selected conditions
        evaluateSelectedConditions();
        
        scrollTo = SCROLL_TO_CLASS;
        PageReference pageRef = ApexPages.currentPage(); 
        pageRef.setRedirect(false);
        return pageRef;
    }
    
    public PageReference removeSelected(){
        List<Authorization_Junction__c> authJuncList = new List<Authorization_Junction__c>();
        for(wrapperRegulation wReg: selectedRegulations) {
            if(wReg.selected == true) {
                iF(Wreg.authJunc.id!=null){
                    authJuncList.add(wReg.authJunc);}
            }
        }
        
        if(authJuncList!=null && !authJuncList.isEmpty()){
            delete authJuncList;
        }  
        //re-evaluate selected conditions
        evaluateSelectedConditions();
        scrollTo = SCROLL_TO_CLASS;
        PageReference pageRef = ApexPages.currentPage(); 
        pageRef.setRedirect(false);
        return pageRef;
    }
    
    public pagereference saveEditedRequirements(){
        
        scrollTo = SCROLL_TO_CLASS;
        PageReference pageRef = ApexPages.currentPage(); 
        pageRef.setRedirect(false);
        List<Authorization_Junction__c> authJuncUpdateList = new List<Authorization_Junction__c>();
        List<Authorization_Junction__c> authJuncInsertList = new List<Authorization_Junction__c>();
        for(wrapperRegulation wReg: selectedRegulations) {
            
            if(wReg.authJunc.Regulation_Description__c=='' ||
               wReg.authJunc.Title__c=='' ){
                   ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter both title and requirements.');
                   ApexPages.addMessage(message); 
                   return pageRef;
               }         
            if(wReg.authJunc.id!=null){ authJuncUpdateList.add(wReg.authJunc); }
            if(wReg.authJunc.id==null){ authJuncInsertList.add(wReg.authJunc); }
        }
        
        if(!authJuncInsertList.isempty()) { insert authJuncInsertList;}
        if(!authJuncUpdateList.isempty()) { update authJuncUpdateList;}
        
        
        return pageRef;
    }    
    
    public void createRelatedConditions(List<Regulation__c> selectedRegs){
        
        List<Authorization_Junction__c> authJuncList = new List<Authorization_Junction__c>();
        for(Regulation__c reg: selectedRegs){
            Authorization_Junction__c authJunc = new Authorization_Junction__c();
            authJunc.Authorization__c = authId;
            authJunc.Official_Review_Record__c = reviewerId;
            authJunc.Regulation__c = reg.Id;
            authJunc.Title__c = reg.Title__c;
            authJunc.Regulation_Description__c = reg.Regulation_Description__c; //copying regulation description to authJunction
            authJunc.recordTypeId = stateRecordTypeId;
            authJuncList.add(authJunc);
        }
        
        insert authJuncList;
        
    }
    
    public class wrapperRegulation {
        
        public Regulation__c reg {get;set;}
        public Boolean selected {get;set;}
        public Authorization_Junction__c authJunc {get;set;}
        
        public wrapperRegulation(Regulation__c r){
            reg = r;
            selected = false;
        }
        
        public wrapperRegulation(Regulation__c r, Authorization_Junction__c aj){
            reg = r;
            selected = false;
            authJunc = aj;
        }
        public wrapperRegulation(Authorization_Junction__c aj){
            selected = false;
            authJunc = aj;
        }        
    }
    
    public pagereference addMoreRequirements(){
        Authorization_Junction__c req = new Authorization_Junction__c();
        req.Authorization__c = authId;
        req.Official_Review_Record__c = reviewerId;
        req.recordtypeId = stateRecordTypeId;
        selectedRegulations.add(new wrapperRegulation(req)); 
        scrollTo = SCROLL_TO_CLASS;
        PageReference pageRef = ApexPages.currentPage(); 
        pageRef.setRedirect(false);
        return pageRef;
    }
    
    //Method for navigating back to ORR record.     
    public pagereference backToRR(){
        PageReference pr = new PageReference (URL.getSalesforceBaseUrl().toExternalForm()+'/Collaborator/s/detail/'+reviewerId ); 
        return pr;
    }
}