public class CARPOL_PPQ_PermitPDFController {

    public ID appId {get; set;}
    public ID authId {get;set;}
    public Authorizations__c app {get; set;}
    List<AC__c> aclst {get; set;}
    public AC__c lineItem{get;set;}
    public String htmlText {get;set;}
    public String tdSpan = 'font-size:9.0pt;color:#221F1F';
    public String tdStyle = 'font-size:13px;border-right: thin solid black;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    public String tdStyleEnd ='font-size:13px;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    Integer counter =0;
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    String strPathway;
    public Integer scjsize {get;set;}
    public list<Reviewer__c> lstReviewRec{get;set;}
    public String currentTime{get;set;}
    public String defaultPart {get;set;}
    Private List<TableCol> tblColumns;
    public String firstentryport {get;set;}
    public String portexit {get;set;}    
    public String convinUS {get;set;}    
    public String convwhileinUS  {get;set;}        
    public String convoutUS {get;set;} 
    public string destination {get;set;}
    public string portsSelected {get;set;}    
    public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public string HandCarry {get;set;}
    public Decimal  facnum {get;set;}
    public transit_locale__c TransLoc{get;set;}
    public String articleCategoryName {get; set;}
    public String articleGroupName {get; set;}
    public String pathwayName {get; set;}
    public String permitRegTitle {get; set;}
    public String cbiDelete {get; set;}     
    public string today{get; set;}
    public string ExpArrDt{get;set;}
    public string ActArrDt {get;set;}
    public string applAddress{get;set;}
    
    public CARPOL_PPQ_PermitPDFController(ApexPages.StandardController controller){ //------start of constructor 
        applAddress = '';
        today = system.today().format();
        destination = '';
        convinUS = '';
        convwhileinUS  = '';
        convoutUS = '';
        firstentryport  = '';
        if(Datetime.now().hour()<13)
            currenttime = String.valueOf(Datetime.now().hour())+':'+String.valueOf(Datetime.now().minute())+' AM';
        else
            {
            Integer hour = Integer.valueOf(Datetime.now().hour())-12;
            currenttime = String.valueOf(hour)+':'+String.valueOf(Datetime.now().minute())+' PM';
            }
        portexit  = '';
        permitRegTitle = '';
        app = (Authorizations__c)controller.getRecord();
        authId = ApexPages.currentPage().getParameters().get('id');
        cbiDelete = ApexPages.currentPage().getParameters().get('CBIDelete');
        system.debug('----cbiDelete---'+cbiDelete);
       
        app = [Select Id, Application__c,Program_Pathway__c, Plant_Inspection_Station__c, Plant_Inspection_Station__r.name, Thumbprint__r.Program_Prefix__r.Process_Type__c,
                      Permit_Regulation_Title__c, Application_CBI__c,Authorized_User__r.FirstName, Authorized_User__r.lastName, applicant__r.Mailing_Street_LR__c, applicant__r.Mailing_City_LR__c,
                      applicant__r.State_Code__c, applicant__r.Mailing_Zip_Postalcode_LR__c, Applicant_Organization__c, application__r.Applicant_name__r.MailingStreet,
                      application__r.Applicant_name__r.MailingCity, application__r.Applicant_name__r.MailingState, application__r.Applicant_name__r.MailingPostalCode, 
                      application__r.Applicant_name__r.MailingCountry
                 from Authorizations__c 
                where Id=: authId]; // 9-22-16 VV added Thumbprint__r.Program_Prefix__r.Process_Type__c
        appId = app.Application__c;
        
        htmlText = '<p style="color:red;">Sample HTML Input</p>';
        getACRecords();
        defaultPart = 'Seed';
        getPDFFields();
        lstReviewRec=new  list<Reviewer__c>();
        portsSelected = ''; //5/16/16 VV       
        if(app.application__r.Applicant_name__r.MailingStreet!=null)
          applAddress = app.application__r.Applicant_name__r.MailingStreet;
        if(app.application__r.Applicant_name__r.MailingCity!=null)
          applAddress += ', '+app.application__r.Applicant_name__r.MailingCity;
        if(app.application__r.Applicant_name__r.MailingState!=null)
          applAddress += ', '+app.application__r.Applicant_name__r.MailingState;
        if(app.application__r.Applicant_name__r.MailingPostalCode!=null)
           applAddress += ', '+app.application__r.Applicant_name__r.MailingPostalCode;
        if(app.application__r.Applicant_name__r.MailingCountry!=null)
           applAddress += ', '+app.application__r.Applicant_name__r.MailingCountry;
        list<Authorization_Junction__c> lstAuthjunct = new list<Authorization_Junction__c>();
        for(Authorization_Junction__c authjunct: [select id,Name,Port__c,Port__r.Name,Authorization__c from Authorization_Junction__c where Authorization__c=:authId  and Port__c!=null])
            {
                lstAuthjunct.add(authjunct);
            }
        if(lstAuthjunct.size()>0){
                for(integer i=0;i<lstAuthjunct.size();i++){
                    if(i<lstAuthjunct.size()-1){
                        portsSelected += lstAuthjunct[i].Port__r.Name+'; ';
                    }
                    else{
                        portsSelected += lstAuthjunct[i].Port__r.Name;
                    }
                }
            }
        else{
               // 9-22-16 VV added for Fast track port
            if (portsSelected == '' && app.Thumbprint__r.Program_Prefix__r.Process_Type__c == 'Fast Track')
               {
                    portsSelected  = lineitem.port_of_entry__r.name;
               }
        
        }
        system.debug('----portsSelected---'+portsSelected);   //5/16/16 END VV 
        
        if(!pathwayName.containsIgnoreCase('PESTS'))
            {
                articleCategoryName = pathwayName;
            }
        else{
            articleCategoryName = articleGroupName;
            }
    }                                                                              //------end of constructor
    
    
    public CARPOL_PPQ_PermitPDFController()  //VV added
    {
    }

    
    
    
    public List<AC__c> getACRecords(){                                       //------start of AC Records method
        if(app.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ',Importer_Mailing_State_ProvinceLU__r.Level_1_Region_Code__c,port_of_entry__r.City__c,port_of_entry__r.State_LV1__r.name, Application_Number__r.name, Program_Line_Item_Pathway__r.Contains_CBI__c,Country_Of_Origin__r.Name, Hand_Carrier_Last_Name__r.Name, Country_of_destination__r.name, Growing_Location_County__r.name, Growing_Location_State__r.name, Intended_Use__r.Name, Port_of_Entry__r.Name, Scientific_Name__r.Scientific_Name__c,Scientific_Name__r.Name, Group__r.Name, Component__r.Name ,Country_of_Export__r.Name, DeliveryRecipient_Last_Name__r.Name,Importer_last_name__r.name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name, Scientific_Name__r.Category_Group_Ref__r.Name, State_Territory_of_Destination__r.Name, Regulated_Article__r.Plant_Part__r.Name, RA_Scientific_Name__r.name from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            lineItem = aclst[0];
            if(lineitem.Proposed_date_of_arrival__c!= null)
                expArrDt = string.valueOf(lineitem.Proposed_date_of_arrival__c.format());
            if(lineitem.Arrival_Time__c != null)
                ActArrDt = string.valueof(lineitem.Arrival_Time__c.format());
            // TehL testing START 5/19/2017
         /*   if (lineItem.Program_Line_Item_Pathway__r.Contains_CBI__c != null && 
                lineItem.Program_Line_Item_Pathway__r.Contains_CBI__c == 'Yes' 
                && CBIDelete == 'Yes') {
                CBI c = new CBI('PPQ');            
                lineItem  = c.getCBIData(lineItem  ); 
            } */
            if(app.Application_CBI__c == 'Yes' && CBIDelete == 'Yes'){
                 CBI c = new CBI('PPQ'); 
                 lineItem  = c.getCBIDeletedData(lineItem);
            }else if(app.Application_CBI__c == 'Yes' && CBIDelete == 'No'){
                 CBI c = new CBI('PPQ'); 
                 lineItem  = c.getCBIData(lineItem);            
            } 
            
            // TehL testing END
 
        }
        
        articleGroupName = aclst[0].Scientific_Name__r.Category_Group_Ref__r.Name; //Dinesh

        if(aclst[0].Movement_Type__c=='Transit')                                            //--------------start of transit IF loop
            {                                           
        
                set<string> lineitemsidset = new set<string>();
                for ( AC__C ac: aclst){
                        lineitemsidset.add(ac.id);
                    }
                    
                list<transit_locale__c> lstTransLoc = new list<transit_locale__c>();
                for (transit_locale__c TransLoc : [select Name, Arrival_Date_Time__c, Level_2_Region__c, Transit_Facility__c, From_Country__c,Line_Item__c,Method_of_Trans__c,Mode_of_Transportion__c, Packaging_Material_Category__c,Port_of_Entry__c,Port_of_Exit__c, Regulated_Article__c, Level_1_Region__c, To_Country__c,Type_of_Conveyance__c, Port_of_Exit__r.name, Port_of_Entry__r.name  from transit_locale__c where Line_Item__c in:lineitemsidset] )
                      {
                          if(TransLoc.Type_of_Conveyance__c=='Into the US' && TransLoc.Mode_of_Transportion__c!=null){
                              convinUS += TransLoc.Mode_of_Transportion__c+', ';
                          }
                          if(TransLoc.Type_of_Conveyance__c=='While in the US' && TransLoc.Mode_of_Transportion__c!=null){
                              convwhileinUS  += TransLoc.Mode_of_Transportion__c+', ';
                          }
                          if(TransLoc.Type_of_Conveyance__c=='Leaving the US' && TransLoc.Mode_of_Transportion__c!=null){
                              convoutUS += TransLoc.Mode_of_Transportion__c+', ';
                          }
                          portexit += TransLoc.Port_of_Exit__r.name+', ';
                          if (firstentryport  == '')
                             { firstentryport = TransLoc.Port_of_Entry__r.name;}
                       }    
                 convinUS = convinUS.removeEnd(', ');
                 convwhileinUS = convwhileinUS.removeEnd(', ');
                 convoutUS = convoutUS.removeEnd(', ');
                 portexit = portexit.removeEnd(', '); 
            }                                                                                   //--------------end of transit IF loop

        if (aclst[0].Hand_Carry__c !=null)                                                  //--------------start of Hand Carry IF loop
            {
                HandCarry = aclst[0].Hand_Carry__c;
            } 
            else 
            {
                HandCarry = 'No';
            }                                                                               //--------------end of Hand Carry IF loop
        
        
        if (aclst[0].Approved_Facility__c!=null)                                            //--------------start of Approved facility IF loop
            {                                            
                Facility__c facility = [select id, Facility_ID__c, Address_1__c,Address_2__c,Building__c,City__c,State_LV1__r.name, State_LV1__r.Level_1_Region_Code__c,Zip__c,Country__r.name,State_Code__c from facility__c where id =: aclst[0].Approved_Facility__c limit 1];
                FacNum = facility.Facility_ID__c ;
                if(facility.Address_1__c!=null)
                    {
                       destination = facility.Address_1__c;
                    }
                    
                if(facility.Address_2__c!=null)
                    {
                        if(destination=='')
                         {
                            destination += facility.Address_2__c;
                         }
                        else{
                           destination += ', '+facility.Address_2__c;
                         }
                    }
                    
                if(facility.Building__c!=null)
                    {
                        if(destination=='')
                         {
                            destination += facility.Building__c;
                         }
                       else{
                            destination += ', '+facility.Building__c;
                       }
                    }
                    
                if(facility.City__c!=null)
                    {
                        if(destination=='')
                        {
                            destination += facility.City__c;
                        }
                        else{
                            destination += ', '+facility.City__c;
                        }
                    }
                    
                if(facility.Zip__c!=null)
                    {
                        if(destination=='')
                         {
                            destination += facility.Zip__c;
                         }
                        else{
                            destination += ', '+facility.Zip__c;
                       }
                    }
                    
                if(facility.State_LV1__r.Level_1_Region_Code__c!=null)
                    {
                        if(destination=='')
                         {
                            destination += facility.State_LV1__r.Level_1_Region_Code__c;
                         }
                        else{
                            destination += ', '+facility.State_LV1__r.Level_1_Region_Code__c;
                       }
                    }
                    
                if(facility.Country__r.name!=null)
                    {
                       if(destination=='')
                        {
                            destination += facility.Country__r.name;
                        }
                        else{
                            destination += ', '+facility.Country__r.name;
                       }
                    }
           
            }                                                                           //--------------end of Approved facility IF loop


     
        return aclst;
    }                                                                                   //------end of AC Records method
    
    
    
        
     public void getPDFFields()                                                              //------start of getPDFFields method
        {

             //Regulations_Association_Matrix__c raMatrix = [Select ID, Name, Program_Line_Item_Pathway__c from Regulations_Association_Matrix__c where Id =:app.Final_Decision_Matrix_Id__c];
             pdfLnItmPway = [select id,Name, Column_1_API_Name__c, Column_2_API_Name__c, Column_3_API_Name__c, Column_4_API_Name__c, Column_5_API_Name__c, Column_6_API_Name__c, 
                                Column_1_Display_Name__c, Column_2_Display_Name__c, Column_3_Display_Name__c, Column_4_Display_Name__c, Column_5_Display_Name__c, Column_6_Display_Name__c ,   
                                PDF_Heading_1__c, PDF_Heading_2__c, PDF_Permit_Conditions_Static__c, Table_Section_Heading__c, Permit_PDF_Sub_Heading_1__c, Permit_PDF_Heading_1__c, Permit_PDF_Heading_2__c, 
                                PDF_Forwarded_Completed_App_Address__c from Program_Line_Item_Pathway__c where Id =: app.Program_Pathway__c];
             pathwayName = pdfLnItmPway.Name;
             if (pdfLnItmPway.Permit_PDF_Sub_Heading_1__c != null){
                 permitRegTitle = pdfLnItmPway.Permit_PDF_Sub_Heading_1__c;
                 }
             if (app.Permit_Regulation_Title__c !=null){
                 permitRegTitle = app.Permit_Regulation_Title__c;
                 }
                
                 
             tblColumns = new List<TableCol>();           
    
            if(pdfLnItmPway.Column_1_API_Name__c!=NULL)        
                tblColumns.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c)); 
            if(pdfLnItmPway.Column_2_API_Name__c!=NULL) 
                tblColumns.add(new TableCol(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c)); 
            if(pdfLnItmPway.Column_3_API_Name__c!=NULL)
                tblColumns.add(new TableCol(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c)); 
            if(pdfLnItmPway.Column_4_API_Name__c!=NULL) 
                tblColumns.add(new TableCol(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c)); 
            if(pdfLnItmPway.Column_5_API_Name__c!=NULL)
                tblColumns.add(new TableCol(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c)); 
            if(pdfLnItmPway.Column_6_API_Name__c!=NULL)
                 tblColumns.add(new TableCol(pdfLnItmPway.Column_6_Display_Name__c, pdfLnItmPway.Column_6_API_Name__c)); 
                    
         }                                                                                    //------end of getPDFFields method
    
    public String getTable()                                                        //------start of gettable method
        {
            htmlText ='';
            //List<CARPOL_UNI_ApplicationPDF_Table__c> tableSettings = CARPOL_UNI_ApplicationPDF_Table__c.getall().values();
            //tableSettings.sort();
            
            htmlText += '<tr>';
                //loop over column heads from tablecol object
            for(TableCol tbl: tblColumns)
                {
                        htmlText += '<th align="center"><u><span style="' + tdSpan + '">';
                        htmlText +=  tbl.Heading;
                        htmlText += '</span></u></th>';
                        counter++;
                }
                
            htmlText += '</tr>';
            
          //  for(AC__c a: lineItem)
               if(lineItem !=null)  
                {
                    counter =0;
                    htmlText += '<tr>';
                    for(TableCol tbl: tblColumns)
                        {
                            htmlText += '<td align="center"><span style="' + tdSpan + '">';
                            htmlText +=  (getFieldValue(lineItem,tbl.APIName)!=null) ? getFieldValue(lineItem,tbl.APIName): '';
                            
                            htmlText += '</span></td>';
                            counter++;
                        }
                    htmlText += '</tr>';    
                }
            
            return htmlText;
        }                                                                              //------end of gettable method
        
    public static Object getFieldValue(SObject o,String field)                                 //------start of getFieldValue method
        {
            if(o == null){
               return null; 
                
            }
               
             if(field.contains('.'))
                {
                     String nextField = field.substringAfter('.');
                     String relation = field.substringBefore('.');
                     return CARPOL_UNI_ApplicationPDF.getFieldValue((SObject)o.getSObject(relation),nextField);
                 }else
                 {
                    return o.get(field);
                 }
        }                                                                        //------end of getFieldValue method
    
    //Added by Tharun 04/27/2016
    
    public List<Reviewer__c>getReviewRecs(){
        Set<Reviewer__c> Set1 = new Set<Reviewer__c>();
         Map<Reviewer__c, List<Authorization_Junction__c>> RevConditionsMap = new Map<Reviewer__c, List<Authorization_Junction__c>>();
                    //  Map<Reviewer__c, List<AC__c>> AuthListMap = new Map<Authorizations__c, List<AC__c>>();
                    lstReviewRec = [SELECT Id, Name,Notes_to_State__c,
                                                 BRS_State_Reviewer_Email__c,
                                                 BRS_State_Reviewer_Notification__c,
                                                 Requirement_Desc__c,State_Comments__c,
                                                 State_not_Responded__c,State_has_comments__c,
                                                 No_Requirements__c,State_Regulatory_Official__r.FirstName,
                                                 Review_Complete__c,Reviewer__c,RecordTypeID, RecordType.Name,
                                                 Status__c,Reviewer_Unique_Id__c,State__c,
                                                 (select id,Regulation_Description__c,Official_Record_Type__c from Authorization_Junctions__r)
                                            FROM Reviewer__c 
                                           WHERE Authorization__c =:authId];
                   
                   if(lstReviewRec.size()>0){ 
                        for(Reviewer__c RevReg :lstReviewRec )
                        {
                           if(RevReg.Authorization_Junctions__r.size()>0){
                                Set1.add(RevReg);
                            }
                         }
                        
                        lstReviewRec.clear();
                        lstReviewRec.addAll(Set1);
                    
                   }  
                   return lstReviewRec;
    }
    
    public List<Authorization_Junction__c> getConditions()                          //------start of getConditions method
        {    
                 Set<Authorization_Junction__c> Set2 = new Set<Authorization_Junction__c>();
                 Set<Authorization_Junction__c> Set1 = new Set<Authorization_Junction__c>();
                
           try
             {
                
                    scj= new List<Authorization_Junction__c>();
                    scj = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, 
                            Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  
                            from Authorization_Junction__c where (Authorization__c =:authId AND Type__c!='State Conditions')   ORDER BY Order_Number__c ASC];//
                    scjA = new List<Authorization_Junction__c>();
                    scjB = new List<Authorization_Junction__c>();
                   
                    if(scj.size()>0){
                        for (Authorization_Junction__c SCJreg: scj)
                        {
                            Set2.add(SCJreg);
                            if(SCJreg.Regulation__r.Type__c=='Import Requirements' && SCJreg.Is_Active__c=='Yes' )
                                {
                                    scjA.add(SCJreg);
                                 }
                            
                            if(SCJreg.Regulation__r.Type__c=='Additional Information')
                                {
                                    scjB.add(SCJreg);
                                 }
                             
                             if(SCJreg.Regulation_Description__c==null)
                                 {
                                     Set2.remove(SCJreg);
                                 }
                            
                        }
                         scj.clear();
                         scj.addAll(Set2);
                         scjsize = scj.size();
                        
                    }
                }
                catch (Exception e){
                    System.debug('ERROR from CARPOL_PPQ_PermitPDFController is : -> ' + e);
                }
                
                
           
            return scj;
            
        }                                                                                    //------end of getConditions method
 
    public Class TableCol
    {
        public String Heading;
        public String APIName;
        public String sortOrder;

        public TableCol(String n, String a)
        {
            Heading = n; APIName = a;
        }
    }
    
}