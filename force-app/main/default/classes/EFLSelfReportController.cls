public with sharing class EFLSelfReportController {
    public Self_Reporting__c selfReporting {get;set;}
    public Date disabledStartDate {get;set;}
    public String disabledReleaseLocation {get;set;}

    //related Observation variables
    public Boolean isShowObsListPanel {get;set;}
	public Boolean isShowObsEditPanel {get;set;}
    public Observation__c erRelatedObjObservations {get;set;}
    public List<Observation__c> eflRelatedRecListObservation {get;set;}
    
    //related GPS Coordinates variables
    public Boolean isShowGpsListPanel {get;set;}
    public Boolean isShowGpsEditPanel {get;set;}
    public Boolean isLocRelatedEditMode {get;set;}
    public Boolean isGPSCBI {get;set;}
    public GPS_Coordinate__c erRelatedObjGPS {get;set;}
    public List<GPS_Coordinate__c> eflRelatedRecListGPS {get;set;}
    
    //related Construct variables
    public Boolean isShowConstructListPanel {get;set;}
	public Boolean isShowConstructEditPanel {get;set;}
    public EFL_Related_Record__c erRelatedObj  {get;set;}
   
    public List<SelectOption> paginationSizeOptions{get;set;}
    public Integer size{get;set;} 
    public Integer selfReportSize{get;set;} 
 	public Integer pageCount {get;set;}
    public Integer noOfRecords{get; set;} 
    public Integer i{get; set;} 
    @TestVisible private Map<String, String> reportTypeMap = new Map<String, String>{'planting_report' => CARPOL_Constants.PLANTING_REPORT,
                                                                        'volunteer_monitoring_report' => CARPOL_Constants.VOLUNTEER_MONITORING_REPORT};
                                                                         
    //Section boolean variables
    public Boolean readyToSubmit{get;set;}
 	public Boolean isSubmitted{get;set;}
    public Boolean plantingEditview{get;set;}
    public Boolean plantingListview{get;set;}
    public Boolean NoPlantingview{get;set;}
    public Boolean volunteerEditview{get;set;}
    public Boolean volunteerListview{get;set;}
	public Boolean NoMonitoringview{get;set;}

    //Render Variables
    public boolean renderDetail{get;set;} 
    public boolean renderList {get;set;}
   	public String action {get;set;}
    public String sobj {get;set;}
    public String delrecid{get;set;}
    public String locationIdParam{get;set;}
    public id selfReportId {get; set;}
    public Boolean submitBool {get;set;}
    public Boolean cbiValidation {get; set;}
    public String constructKeyPrefix{get;set;}
    public String locationKeyPrefix{get;set;}
    
    public FINAL String VAR_RELATED_CONSTRUCT = 'relatedConstruct';
    public FINAL String VAR_RELATED_GPS = 'relatedGps';
    public FINAL String VAR_RELATED_OBS = 'relatedObs';
    public FINAL String VAR_RELATED_MODE_EDIT = 'edit';
    public FINAL String VAR_RELATED_MODE_LIST = 'list';
    public FINAL String VAR_RELATED_MODE_CREATE = 'create';
    public FINAL String VAR_RELATED_MODE_VIEW = 'view';
    Public static final string PLANTING_REPORT = 'Planting Report';
    public static final string REPORT_SUBMITTED = 'Submitted';
    public static final string PLANTING_REPORT_URL_PARAM = 'planting_report';
    
    public ID authorizationID
    {
        get {
        if(authorizationID==NULL){
            authorizationID = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('authId'));
        } 
        return authorizationID;
    	}  
        set;
    }
    public Authorizations__c authObj {
        get{
            return [Select id, Name,Recordtype.name, Multi_Year_Permit__c, BRS_Introduction_Type__c, BRS_Purpose_of_Permit__c,Expiration_Date__c,
                    Date_Issued__c, Effective_Date__c ,Permit_Number__c, Status__c,Authorization_Type__c,Application_Type__c, Application_CBI__c 
                    From authorizations__c Where Id=:authorizationID limit 1];
        }
        set;   
    }
    public string doesAppContainCBI 
    { 
        get{
            doesAppContainCBI = '';
            if(authObj!=null)
            {
                doesAppContainCBI = authObj.Application_CBI__c;   
            }
            return doesAppContainCBI;
        }
        set;
    }
    public id reportSumId
    {
        get{
            reportSumId = null;
            if(ApexPages.currentPage().getParameters().get('rsId')!=null)
            {
                reportSumId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('rsId'));
            }   
            return reportSumId; 
        }
        set;
    }
    public string repSummName 
    { 
        get{
            repSummName = '';
            if(rs!=null)
            {
                repSummName = rs.Name;   
            }
            return repSummName;
        }
        set;
    }
    
    public Report_Summary__c rs
    {
        get{
            if(reportSumId!=null)
            {
                rs =  new Report_Summary__c();
                rs = EFLSelfReportingRepository.getReportSummaryById(reportSumId);           
            }   
            
            return rs; 
        }
        set;
    }
 
    public string reportType
    {
        get{
            reportType = '';
            if(ApexPages.currentPage().getParameters().get('reportType')!=null)
            {
                reportType = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('reportType'));
            }   
            return reportType; 
        }
        set;
    }
    public string reportTypeName
    {
        get{
            reportTypeName = '';
            if(reportType==PLANTING_REPORT_URL_PARAM)
            {
                reportTypeName = reportTypeMap.get(reportType);
            }else
                reportTypeName = reportTypeMap.get(reportType);
            return reportTypeName;
        }
        set;
    }
    public id recordTypeId
    {
        get{
            if(reportType==PLANTING_REPORT_URL_PARAM)
            {
            recordTypeId = EFLGenericUtility.getRecordTypeId(PLANTING_REPORT);
            }
            else 
            {
            recordTypeId = EFLGenericUtility.getRecordTypeId(CARPOL_Constants.VOLUNTEER_MONITORING_REPORT);
            }   
              return recordTypeId;    
        }
        set;
    }
  	public boolean rsIsSubmitted {
        get{
            rsIsSubmitted = false;
            if(rs!=null)
            {
                if(rs.Status__c==REPORT_SUBMITTED) {
                    rsIsSubmitted = true;
                }   
            }
            return rsIsSubmitted;
        } 
        set;
    }
 	public string applicantName {
        get{
            return UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
           }
        set;
    }
/*Variable and Properties - End*/ 
    
    /*Constructor - Start*/
   public EFLSelfReportController(ApexPages.StandardController controller) {

       locationKeyPrefix = Location__c.sObjectType.getDescribe().getKeyPrefix();
       constructKeyprefix = Construct__c.sObjectType.getDescribe().getKeyPrefix();
       selfReportSize = getselfRepList().isEmpty()?0:getselfRepList().size();
       
       isSubmitted = false;
       submitBool = False;
       cbiValidation = true;
       initPagination();
       displaySRSummaryPage();                   
       
    }
     /*Constructor - End*/
    
    //Initialize the pagination components bound to the page
    public void initPagination()
    {
        size=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('25','25'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        
    }
  
    //Set Controller - Load Planted Location and volunteer monitor report by AuthId    
    public ApexPages.StandardSetController setCon {
        get
        {	
            if(setCon == null) {
                List<Location__c> plantedLocations = new List<Location__c>();
                if(reportTypeName == CARPOL_Constants.VOLUNTEER_MONITORING_REPORT){
                     plantedLocations = EFLSelfReportingRepository.plantedReleaseLocationsByAuth(authorizationId);
                    setCon = new ApexPages.StandardSetController(EFLSelfReportingRepository.getFieldTestLocationsAndReportsByPlantedLocations(plantedLocations, recordTypeId, reportSumId));
                
                }else{
                    plantedLocations = EFLSelfReportingRepository.releaseLocationsByAuth(authorizationId);
                    setCon = new ApexPages.StandardSetController(EFLSelfReportingRepository.getFieldTestLocationsAndReportsByPlantedLocations(plantedLocations, recordTypeId, reportSumId));
                
                }
                //setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
                if(noOfRecords < size){
                    i = size;
                    
                    List<Self_Reporting__c> resultSearchedsrList = getselfRepList();
                    if(!resultSearchedsrList.isEmpty()){
                        i = i + resultSearchedsrList.size();
                        setCon.setPageSize(i);
                    }
                }else {
                    setCon.setPageSize(size);
                }
            }
            return setCon;
        }
        set; 
    }
    //Planting Location/Volunteer Monitoring Location List 
    public List<Location__c> getPlantingLocationsAndReports() {   
        return (List<Location__c>) setCon.getRecords();
    } 
    
    //Pagination size setter
    public PageReference refreshPageSize() {
      //  Known Issue exists the all rows from subquery are not displayed when the size is greater than no of records
      //  Code added to display all of records.
       if(noOfRecords < size){
            i = size;
            
            List<Self_Reporting__c> resultSearchedsrList = getselfRepList();
            if(!resultSearchedsrList.isEmpty()){
           	 	i = i + resultSearchedsrList.size();
            	setCon.setPageSize(i);
            }
        }else {
            setCon.setPageSize(size);
        }
        
        return null;
    }
    //Get Count of Self Reports for Report Summary to prevent submission when no self reports exist
    public List<Self_Reporting__c> getselfRepList() {
      
        List <Report_Summary__c> reportSumList = new List <Report_Summary__c> ();
        reportSumList.add(rs);
        List<Self_Reporting__c> resultSearchedsrList = new List<Self_Reporting__c>();
 
            for(Self_Reporting__c sr : EFLSelfReportingRepository.selfRepListByRSIdandRSType(reportSumList, reportTypeName)){
            	resultSearchedsrList.add(sr);
            }
        return resultSearchedsrList;
    }

 	//Toggle certify box for submisssion
    public void callToggleCheckBox() {
        //readyToSubmit=true;
        integer cbiCount = 0;
        integer cbiDelCount = 0;
        Report_Summary__c temp = [SELECT Id, CBI_Count__c, CBI_Deleted_Count__c FROM Report_Summary__c WHERE Id=:rs.Id LIMIT 1];
            cbiCount = !String.isBlank(temp.CBI_Count__c) ? integer.valueOf(temp.CBI_Count__c) :0;
            cbiDelCount = !String.isBlank(temp.CBI_Deleted_Count__c) ? integer.valueOf(temp.CBI_Deleted_Count__c) :0;
 
        if (cbiCount != cbiDelCount){
            cbiValidation = false;
        } else {
            cbiValidation = True;
        }
        if(submitBool){
            submitBool = false;
        }else{
            submitBool = True;
            if(checkReadyToSubmit(rs, reportTypeName) == false || !cbiValidation){ 
                readyToSubmit = false;
            	getInstructions();
            }
        }
        
 
    }
	//Delete Self Report
   public pagereference deleteSelfRepRec() {
       
        EFLGenericUtility.deleteRecord(selfReportId);
       	getPlantingLocationsAndReports();  
        PageReference EFLSelfReport = new PageReference('/apex/EFLSelfReport');
        EFLSelfReport.setRedirect(true);
        EFLSelfReport.getParameters().put('authId', authorizationId);
        EFLSelfReport.getParameters().put('reportType', reportType);
		EFLSelfReport.getParameters().put('rsId', reportSumId);
        return EFLSelfReport;
     
   }
   //Edit Self Report
   public void editForm() {
       selfReporting = new Self_Reporting__c();
       selfReporting = EFLSelfReportingRepository.selfRepById(selfReportId);
       if(selfReporting.id != null) {
           disabledStartDate = selfReporting.Start_Date__c;
           disabledReleaseLocation = selfReporting.Release_Record_ID__r.Name;
           if(selfReporting.Is_Submitted__c){
         	  	isSubmitted = true;
              }
           if( selfReporting.Is_No_Planting__c ) {
               NoPlantingview = true;
          }else if( selfReporting.No_Monitoring_Report__c) {
              NoMonitoringview = true;
           }
     }
       displayDetailsSection();
        
   }
   //Save Self Report
   public pageReference save() {
       try {
            if(selfReporting.Report_Summary__c == null){
                selfReporting.Report_Summary__c = rs.Id;
           }
            selfReporting.Authorization__c = authorizationId;
            selfReporting.RecordTypeId = recordTypeId;
     
            upsert selfReporting;
           if(NoPlantingview || NoMonitoringview){
               return cancelSRInfo();
           }else{
               cancelRelated();
           }
  			
       }catch(DmlException ex){
    		EFLErrorLog.createErrorLog('EFLSelfReportController.save()',ex); 
       }
       
       return null;

    } 
    //Returning user to location list
    public pagereference cancelSRInfo()
    {
        isSubmitted =false;
        hideAllRelatedSections();
        displaySRSummaryPage();
        getPlantingLocationsAndReports(); 
        PageReference EFLSelfReport = new PageReference('/apex/EFLSelfReport');
        EFLSelfReport.setRedirect(true);
        EFLSelfReport.getParameters().put('authId', authorizationId);
        EFLSelfReport.getParameters().put('reportType', reportType);
		EFLSelfReport.getParameters().put('rsId', reportSumId);
        return EFLSelfReport;
    }
   //Initialize and load add planting report
   public void addPlanting() {
 
        NoPlantingview = false;
        plantingEditview=true;
        addform();        
    }
    //Initialize and load add no planting report
    public void addNoPlanting() {
 
        NoPlantingview = true;
        addform();
    }
	//Initialize and load add Monitoring report
    public void addMonitoring() {
        NoMonitoringview = false;
        addform();
    }
    //Initialize and load add no Monitoring report
    public void addNoMonitoring() {
        NoMonitoringview = true;
        addform();
    }
    //Initialize and load add self report
    public void addform() {
        
        selfReporting = new Self_Reporting__c();
        selfReporting.Release_Record_ID__c = locationIdParam;
        disabledReleaseLocation = disabledReleaseLocation; 
        
        displayDetailsSection();
    }
    //If self report is not ready for submission, provide user instructions
    public string getInstructions(){
       
        if(!cbiValidation)    {
            return(EFLGenericutility.getInstructions(NULL,NULL,'CBI','Self Reporting'));
        }else if(reportTypeName == CARPOL_Constants.VOLUNTEER_MONITORING_REPORT)
        {
            return(EFLGenericutility.getInstructions(NULL,NULL,'Observation','Self Reporting'));
        }else if (reportTypeName == CARPOL_Constants.PLANTING_REPORT){
            return(EFLGenericutility.getInstructions(NULL,NULL,'Construct','Self Reporting')); 
        }
        
     return null;
    }
    //check if self report is ready for submission
    public Boolean checkReadyToSubmit(Report_Summary__c rs, String reportTypeName){
        if(rs != null && rs.id != null) {
            List<Self_Reporting__c> selfReportList = new List<Self_Reporting__c>();
            if(reportTypeName == CARPOL_Constants.VOLUNTEER_MONITORING_REPORT){
                selfReportList = [Select id,Is_Submitted__c,No_Monitoring_Report__c,Is_No_Planting__c,Observation_Count__c from Self_Reporting__c 
                                  where No_Monitoring_Report__c = false and Observation_Count__c = 0 and Report_Summary__c = :rs.id];
            }
            if(reportTypeName == CARPOL_Constants.PLANTING_REPORT){
                selfReportList = [Select id,Is_Submitted__c,No_Monitoring_Report__c,Is_No_Planting__c,Ready__c from Self_Reporting__c 
                                  where Is_No_Planting__c = false and Ready__c = false and Report_Summary__c = :rs.id];
            }
            if(!selfReportList.isEmpty())
            {
                return False;
            }else{
                return true;
            }
        }
        return null;
    }
	//Submit Report Summary History,Report summary and related self reports
    public Pagereference doSubmit() {
        if(rs != null && rs.id != null) {
            
            EFLSelfReportingHelper EFLSelfReportHelper = new EFLSelfReportingHelper();
            EFLSelfReportHelper.submitReport(rs);
        } 
        return new Pagereference('/apex/EFLReportSummary?authId='+authorizationID);
    }
	//Get related list records
    public List<EFL_Related_Record__c> getRelatedConstructs() {
        return [Select Construct__c,Construct__r.Construct_s__c,Construct__r.Name,Construct_CBI__c , id, Number_of_Volunteers_CBI__c,Lines_Events__c, Name 
                from EFL_Related_Record__c Where Self_Reporting__c=:selfReporting.id AND RecordType.DeveloperName='Construct'];
    }
    public List<Observation__c> getRelatedObservations() {
        
        return [Select id, Number_of_Volunteers__c,Number_of_Volunteers_CBI__c, Units__c, Action_Taken__c, Observation_Date__c, Number_of_Volunteers_Text__c,
                Self_Reporting__c, Comments__c from Observation__c Where Self_Reporting__c=:selfReporting.id];
    }
    public List<GPS_Coordinate__c> getRelatedGpsCoordinates() {
        return [Select Id,Name,GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,GPS_Coordinates_CBI__c,Location__r.GPS_Co_ordinates_CBI__c,
                GPS_Coordinates_Text__c,Self_Reporting__r.GPS_Co_ordinates_CBI__c 
                from GPS_Coordinate__c Where Self_Reporting__c=:selfReporting.id];
    }
    //Delete related records
     public void deleteRec(){ 
         EFLGenericUtility.deleteRecord(delrecid);
     
    }
    //Edit related list records
    public void editGps(){
        erRelatedObjGPS = [Select Id,GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,GPS_Coordinates_CBI__c,Location__r.GPS_Co_ordinates_CBI__c,Self_Reporting__r.GPS_Co_ordinates_CBI__c 
                           from GPS_Coordinate__c where Id =: delrecid];
        displayRelatedPage(VAR_RELATED_GPS,VAR_RELATED_MODE_EDIT);
    } 
    public void editObs(){
        erRelatedObjObservations = [Select Observation_Date__c, id, Number_of_Volunteers__c,Number_of_Volunteers_CBI__c , Units__c, Action_Taken__c, Self_Reporting__c, Comments__c from Observation__c where Id =: delrecid];
        displayRelatedPage(VAR_RELATED_OBS,VAR_RELATED_MODE_EDIT);
    } 
    public void editConstruct(){
        erRelatedObj = [Select Construct__c,Construct__r.Construct_s__c,Construct__r.Name,Construct_CBI__c , id, Lines_Events__c, Name from EFL_Related_Record__c Where Id =: delrecid];
        displayRelatedPage(VAR_RELATED_CONSTRUCT,VAR_RELATED_MODE_EDIT);   
    }
    public void cancelRelated(){
        hideAllRelatedSections();
        displayDetailsSection();
    }

    public void action(){
		Id recordTypeIdObs = Schema.SObjectType.EFL_Related_Record__c.getRecordTypeInfosByName().get('Observations').getRecordTypeId();
       Id recordTypeIdCon = Schema.SObjectType.EFL_Related_Record__c.getRecordTypeInfosByName().get('Construct').getRecordTypeId();
        try {
            if (action == 'save'){
                if (sobj == 'relatedConstruct'){
                    upsert erRelatedObj;
                } else if (sobj == 'relatedObs'){
                    upsert erRelatedObjObservations;
                } else if (sobj == 'relatedGps'){
                    isGPSCBI = selfreporting.GPS_Co_ordinates_CBI__c;
                    upsert erRelatedObjGPS;
                   // if(!String.isBlank(erRelatedObjGPS.Self_Reporting__c)){
                   //     selfreporting.GPS_Co_ordinates_CBI__c=isGPSCBI;

                        upsert selfreporting;
                        updateRelatedRecords(isGPSCBI);
                 //   }
                }
                cancelRelated();
            } else if (action == 'create'){
                 if (sobj == 'relatedGps'){
                    erRelatedObjGPS = new GPS_Coordinate__c();
                    erRelatedObjGPS.Self_Reporting__c = selfReporting.Id;
                    erRelatedObjGPS.Location__c = selfReporting.Release_Record_ID__c;
                    displayRelatedPage(VAR_RELATED_GPS,VAR_RELATED_MODE_EDIT);
                }else if (sobj == 'relatedConstruct'){
                   erRelatedObj = new EFL_Related_Record__c();
        		   erRelatedObj.Self_Reporting__c = selfReporting.Id;
                   erRelatedObj.RecordTypeId = recordTypeIdCon;
                   erRelatedObj.Authorization__c = authorizationId;
                    displayRelatedPage(VAR_RELATED_CONSTRUCT,VAR_RELATED_MODE_EDIT);
           		}else if (sobj == 'relatedObs'){
                    
                    erRelatedObjObservations = new Observation__c();
                    erRelatedObjObservations.Self_Reporting__c= selfReporting.Id;
                    displayRelatedPage(VAR_RELATED_OBS,VAR_RELATED_MODE_EDIT);
                }
            }
            
        }  catch (Exception ex){
            ApexPages.addMessages(ex);
        }
    }
 
    public void displayRelatedPage(String relatedObjType, String mode){
        hideAllRelatedSections();
        if (mode == VAR_RELATED_MODE_EDIT){
            isLocRelatedEditMode = true;
        if (relatedObjType == VAR_RELATED_CONSTRUCT){
                isShowConstructEditPanel = true; 
            } else if (relatedObjType == VAR_RELATED_GPS){
                isShowGpsEditPanel = true; 
            }else if (relatedObjType == VAR_RELATED_OBS){
                isShowObsEditPanel = true; 
            }
        } 
    }
    public void hideAllRelatedSections(){
      
        isShowGpsListPanel = false;
        isShowGpsEditPanel = false; 
        isShowObsListPanel = false;
        isShowObsEditPanel = false; 
        isShowConstructListPanel =false;
        isShowConstructEditPanel =false;
      
    }
    
    public void displayDetailsSection(){
        renderList = false;
        renderDetail = true;
  
		checkReportTypes(VAR_RELATED_MODE_EDIT); 
        isLocRelatedEditMode = false;
  
        if(plantingEditview || volunteerEditview){
        	displayRelatedDetailsSection();    
        }
    }   
    public void displayRelatedDetailsSection(){
        if (selfReporting.Id!=null){
            if(plantingEditview){
                isShowGPSListPanel = true;
            	isShowConstructListPanel =true;
                isShowObsListPanel = false;
            }else if(volunteerEditview){
        		isShowObsListPanel = true;
                isShowGPSListPanel = false;
            	isShowConstructListPanel =false;
            }
        } 
    }  
	public void displaySRSummaryPage(){
        plantingListview = false;
        plantingEditview = false;
        NoPlantingview = false;
        NoMonitoringview = false;
        volunteerEditview = false;
        volunteerListview = false;
     	checkReportTypes('List');
        renderDetail = false;
        readyToSubmit=true;
    }  
    private void checkReportTypes(String mode) {
  
        if(mode == 'List'){
            renderList = true;
        }
 
     	if(NoMonitoringview){
             if(mode == VAR_RELATED_MODE_EDIT){
          		selfReporting.No_Monitoring_Report__c=true;
             	volunteerEditview = False;
                volunteerListview = False;
             }else{
                NoMonitoringview=false;
                volunteerListview = True; 
	         }
         }
    	 else if(reportTypeName == CARPOL_Constants.VOLUNTEER_MONITORING_REPORT){
             if(mode == VAR_RELATED_MODE_EDIT){
          		volunteerEditview = True;
                volunteerListview = False;
             }else{
                volunteerListview = True;
                volunteerEditview = False;

             }
         }else if(NoPlantingview){
             if(mode == VAR_RELATED_MODE_EDIT){
          		selfReporting.Is_No_Planting__c=true;
                plantingEditview = False;
             	plantingListview = False;	
             }else{
                NoPlantingview=false;
                plantingListview = True; 
	         }
             
         }else if(reportTypeName == CARPOL_Constants.PLANTING_REPORT){
          	if(mode == VAR_RELATED_MODE_EDIT){
          		plantingEditview = True;
                plantingListview = False;
             }else{
                plantingListview = True;
                plantingEditview = False; 
             }
 	     }
     
    }
   //SpringCM Utility
    public String fireDocLauncher(){
		String URL = EFLSpringCMUtility.buildDocLauncherURL(reportSumID, rs.Name, '', 'Self Report Attachment');
        return URL;
    }
    private void updateRelatedRecords(Boolean isGPSCBI){
        List<GPS_Coordinate__c> gpsListtoUpdate = new List<GPS_Coordinate__c>();
        for(GPS_Coordinate__c gps: getRelatedGpsCoordinates()){
            gps.GPS_Coordinates_CBI__c=isGPSCBI;
            gpsListtoUpdate.add(gps);
        }
        if(!gpsListtoUpdate.isEmpty()){
            update gpsListtoUpdate;
        }
    }
    	
}