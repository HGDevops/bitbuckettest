public class Randomizer {
    //returns a random Integer
     public static Integer getRandomNumber(Integer size){
          Double d = math.random() * size;
          return d.intValue();
     }
    
    //Get's a random value from a list of strings
            public static String getRandomString(List<String> strings){
                  List<Double> ranks = new List<Double>();
                  Map<Double,String> rankMap = new Map<Double,String>();
        
                  for(String s : strings){
                       Boolean isDup = true;
                       Double rank;
        
                       While(isDup){
                            Double x = getRandomNumber(100000);
                            if(!rankMap.containsKey(x)){
                                 rank = x;
                                 isDup = false;
                            }
                       }
        
                       ranks.add(rank);
                       rankMap.put(rank,s);
                  }
        
                  ranks.sort();
                  return rankMap.get(ranks.get(0));
             }
                
            }