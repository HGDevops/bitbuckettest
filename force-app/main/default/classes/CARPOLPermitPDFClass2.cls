public with sharing class CARPOLPermitPDFClass2 {

  
    public Authorizations__c permit;
    private ID permitID;
    public string strSignId {get;set;}
    public list<Regulation__c> lstCond {get;set;} // 
    public list<Signature_Regulation_Junction__c> lstSignJucnt {get;set;}
    public list<string> lstSignJunctids {get;set;} 
    public list<string> lstconditions {get;set;} 
    public list<string> lstconditions2 {get;set;}
    
    // new added 
    public Contact objcont {get;set;}
    public Account objacct {get;set;}
    public string strgroupid {get;set;}
    public string strplant {get;set;}
    public string regulcount {get;set;}
    
    // Standard Controller Constructor 
    public CARPOLPermitPDFClass2(ApexPages.StandardController controller) {
        
        try
        {
            permitID = ApexPages.currentPage().getParameters().get('ID');
            lstCond = new list<Regulation__c>();
            strplant = '';
            regulcount = '';
            lstSignJucnt = new list<Signature_Regulation_Junction__c>();
            lstSignJunctids = new list<string>();
            lstconditions = new list<string>();
            System.Debug('<<<<<<< Permit ID : ' + permitID + ' >>>>>>>');
             objcont = new Contact();
             objacct = new Account();
             strgroupid = '';
            if(permitID != null)
            {
                permit = [SELECT ID, NAME, Application__c, Application__r.Applicant_Name__r.Name, Date_Issued__c, Mailing_Address__c, Receiving_Address__c, AC_Applicant_Phone__c, AC_Applicant_Fax__c, Expiry_Date__c,Granted_By__c, Application__r.Signature__c, Granted_By__r.Name, Application__r.Regulated_Article__r.Name, Application__r.Signature__r.From_Country__r.Name, Ports_of_Arrival__c FROM Authorizations__c WHERE ID = :permitID LIMIT 1];
                System.Debug('<<<<<<< permit.Application_NO__r.Signature__c  ' +permit.Application__r.Signature__c);
                if(permit.Application__r.Signature__c!=null){
                strSignId = permit.Application__r.Signature__c;
                System.Debug('<<<<<<< strSignId ID : ' + strSignId + ' >>>>>>>');
                lstSignJucnt = [select id,Regulation__c,Signature__c from Signature_Regulation_Junction__c where Signature__c =:strSignId and id!=null];
                for(Signature_Regulation_Junction__c sj :lstSignJucnt)
                {
                    lstSignJunctids.add(sj.Regulation__c);
                }
                System.Debug('<<<<<<< lstSignJunctidsID : ' + lstSignJunctids+ '-------------');
                if(lstSignJunctids.size()>0){
                        lstCond = [select id,Name,Short_Name__c,Regulation_Description__c from Regulation__c where id in:lstSignJunctids];
                        integer ruleno = 0;
                        for(Regulation__c c:lstCond )  
                        {
                            ruleno++;
                            if(ruleno!=1){
                            regulcount += ', '+string.valueof(ruleno);
                            }
                            else{
                                regulcount += string.valueof(ruleno);
                            }
                            lstconditions.add(ruleno+'. '+c.Regulation_Description__c);
                        }                 
                    }
                  
                }
                
                String appid = permit.Application__c;
                list<Application__c> lst = [select id,Name,Applicant_Name__r.Name,Applicant_Name__c,Applicant_Name__r.MailingStreet,Applicant_Name__r.MailingCity,Applicant_Name__r.MailingState,Applicant_Name__r.MailingPostalCode,
                  Applicant_Name__r.MailingCountry,Regulated_Article__r.Name,Import_Country__r.Name,Applicant_Name__r.AccountId,Signature__c,Signature__r.Group__c from Application__c where id=:appid limit 1];
                  strgroupid = lst[0].Signature__r.Group__c ;
                  strplant = lst[0].Regulated_Article__r.Name;
                  list<Contact> lstcont = [select id,Name,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Phone,Email from Contact where id=:lst[0].Applicant_Name__c limit 1];
                  objcont = lstcont[0];
                  list<Account> lstacct = [select id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry from Account where id=:lst[0].Applicant_Name__r.AccountId limit 1];
                  objacct = lstacct[0];
            }
           
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>');
        }
    }
     public List<wrappertable> lstw = new List<wrappertable>();
     public list<String> signcondmapids = new list<string>();
        public map<string,string> mapartregcustname = new map<string,string>();
        public map<string,string> mapartplant = new map<string,string>();
        public list<string> lstart = new list<string>();
     public List<wrappertable> getlstwrap(){
             integer ruleno = 0;
             lstconditions2 = new list<string>();
                 list<Signature__C> lstsigncondmap  = [select id,Name,Regulated_Article__r.Name,Plant_Part__c,Regulated_Article__c,Group__c 
                from Signature__C where Group__c=:strgroupid];
                
                for(Signature__C s:lstsigncondmap)
                {
                    signcondmapids.add(s.id);
                }
                list<Signature_Regulation_Junction__c> lstsigncondjuct = [select id,Name,Regulation__c,Signature__r.Regulated_Article__r.Name,Regulation_Custom_Name__c,Regulation__r.Regulation_Description__c,Signature_Plant_Part__c,Signature__r.Plant_Part__c from Signature_Regulation_Junction__c
                                                        where Signature__c in :signcondmapids order by Signature__r.Regulated_Article__r.Name];
                 for(Signature_Regulation_Junction__c s:lstsigncondjuct)
                    {
                        ruleno++;
                       // if(s.Condition_Rule__r.Regulation_Description__c!=null){
                        lstconditions2.add(ruleno+'. '+s.Regulation__r.Regulation_Description__c);
                       // }
                        if(mapartregcustname.containskey(s.Signature__r.Regulated_Article__r.Name))
                        {
                            string str = mapartregcustname.get(s.Signature__r.Regulated_Article__r.Name);
                            mapartregcustname.remove(s.Signature__r.Regulated_Article__r.Name);
                            str += ', '+string.valueof(ruleno);
                            mapartregcustname.put(s.Signature__r.Regulated_Article__r.Name,str);
                        }
                        else{
                            mapartregcustname.put(s.Signature__r.Regulated_Article__r.Name,string.valueof(ruleno));
                            mapartplant.put(s.Signature__r.Regulated_Article__r.Name,s.Signature__r.Plant_Part__c);
                            lstart.add(s.Signature__r.Regulated_Article__r.Name);
                            
                        }
                        /*if(!mapartplant.containskey(s.Signature__r.Regulated_Article__r.Name))
                        {
                            mapartplant.put(s.Signature__r.Regulated_Article__r.Name,s.Signature_Plant_Part__c);
                        }*/
                 }
                    for(string s:lstart)
                    {
                        lstw.add(new wrappertable(s,mapartregcustname.get(s),mapartplant.get(s)));
                    }
           return lstw;
         }
   public class wrappertable{ // inner class                                                                                             
        public String artname {get;set;} 
        public String regcustname{get;set;}
        public String pltpartname{get;set;}                                   
        public wrappertable(string artname,string regcustname,string pltpartname){                     
             this.artname = artname;
             this.regcustname = regcustname;
             this.pltpartname = pltpartname;
        }
    }
    
}