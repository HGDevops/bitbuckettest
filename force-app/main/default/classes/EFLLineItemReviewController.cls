/**
*@Purpose : As an applicant, after adding a lineitem, they are directed to this controller
which displays all the BRS related records of the line item and also has the
buttons to create related records to the lineitem based on the program line item pathways.
* Related Pages: CARPOL_LineItemPageForApplicant
*/
public with sharing class EFLLineItemReviewController{
/**
*  Properties
**/
    public Authorizations__c auth{get;set;}
    public id LineId{get;set;}
    public id LineItemId;
    public id AuthId{get;set;}
    public string releasestartdate{get;set;}
    public string releaseenddate{get;set;}
    public String proposedstartdateStr {get; set;}
    public String proposedenddateStr {get; set;}    
    public Id strPathwayId{get;set;}
    public boolean aabutton{get;set;}
    public boolean constructbutton{get;set;}
    public boolean sopbutton{get;set;}
    public boolean locationbutton{get;set;}
    public boolean RAbutton{get;set;}
    public boolean BRSPermitInfo{get;set;}
    public boolean BRSPermitReleaseInfo{get;set;}
    public boolean BRSNotificationInfo{get;set;}
    public boolean BRSCourtesyInfo{get;set;}
    public boolean prevReviewConst{get;set;}
    public List<AC__c> Linelst{get;set;}
    public id AppId;
    public string redirect{get;set;}
    public string recTypeName{get;set;}
    public string releaselocationinfo{get;set;}
    public boolean SOPDesignProtocol{get;set;}
    public string wfStatus{get;set;}
    public boolean displayCompAgreementButton{get;set;}
    public Map<string,string> sobjectkeys{get;set;}
    public string delrecid{get;set;}
    public List<Workflow_Task__c> wfLst{get;set;}
    public string errmsg{get;set;}
    public List<Regulated_Article__c> listRA{get;set;}
    public string usertype{get;set;}
    public string sSelCat{get;set;}
    public id UploadAttrecid{get;set;}
    public id soprecid{get;set;}
    public List<Applicant_Attachments__c> lstappattachment{get;set;}
    public List<Applicant_Attachments__c> lstsop{get;set;}
    public List<Applicant_Attachments__c> lstgenericattachment{get;set;}
    public String selectedOption{get;set;}
    public List<Construct__c> listConstructs{get;set;}
    public List<Article_Supplier_Developer__c> listArticleSuppliers{get;set;}
    //W-031276 - applying the new geno type datamodel based query
    //public Map<Id, Map<String, List<Genotype__c>>> genoTypesByConstruct{get;set;}
    //public Map<Id, Map<String, List<Genotype__c>>> genoTypesByRevConstruct{get;set;}
    public Map<Id, List<Genotype__c>> genoTypesByRevConstruct{get;set;}
    public Map<id, List<phenotype__c>> phenoTypeByConMap{get;set;}
    public List<Link_Regulated_Articles__c> linkedRas {get;set;}
    public id locationreportid {get;set;}
    public id Constructreportid {get;set;}
    Public List<Construct_Application_Junction__c> conAppJunList{get;set;}
    public List<Construct_Application_Junction__c> revCons{get{if(revcons!= null){
                                                                return revCons;
                                                               }else{
                                                                   return getRevCons();
                                                               }
                                                        }set;}
    public Map<id, List<Attachment>> sopAttachmentsMap{get;set;}
    public List<location__c> locations{get;set;}
    public string GeneralInstructions{get;set;}
    public List<SelectOption> getLineItemOptions() {
        List<SelectOption> LineItemOptions = new List<SelectOption>();
        LineItemOptions.add(new SelectOption('All','All'));
        LineItemOptions.add(new SelectOption('Attachments','Attachments'));
        LineItemOptions.add(new SelectOption('Constructs','Constructs'));
        LineItemOptions.add(new SelectOption('Dashboards','Dashboards'));
        LineItemOptions.add(new SelectOption('Reg Articles','Reg Articles'));
        LineItemOptions.add(new SelectOption('Locations','Locations'));
        LineItemOptions.add(new SelectOption('SOPs','SOPs'));
        LineItemOptions.add(new SelectOption('Article Sup/Dev','Article Sup/Dev'));

        return LineItemOptions;
    }
    public String docLauncherFullURL {get;set;}
    public String docLauncherFullURLCBIDeleted {get;set;}
    
     
/**
* Standard Controller Constructor : Obtains all the Construct's, Location's, Regulated Article's, SOP's , Line Item Information
**/
    public EFLLineItemReviewController(ApexPages.StandardController controller){
        lstappattachment=new  List<Applicant_Attachments__c>();
        //W-031276 - applying the new geno type datamodel based query
        //genoTypesByConstruct =  new Map<Id, Map<String, List<Genotype__c>>>();
        genoTypesByRevConstruct = new Map<Id, List<Genotype__c>>();
        phenoTypeByConMap = new Map<Id, List<Phenotype__c>>();
        sopAttachmentsMap = new Map<id, List<Attachment>>();
        linkedRas = new List<Link_Regulated_Articles__c>();
        locations = new List<location__c>();
        listConstructs = new List<Construct__c>(); 
        conAppJunList = new List<Construct_Application_Junction__c> ();
        lstsop=new  List<Applicant_Attachments__c>();
        lstgenericattachment=new  List<Applicant_Attachments__c>();
        userType=UserInfo.getUserType();
        Map<string,Schema.SobjectType> describe=Schema.getGlobalDescribe();
        sobjectkeys=new  Map<string,string>();
        for(string s:describe.keyset())sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
        BRSPermitReleaseInfo=false;
        SOPDesignProtocol=false;
        BRSNotificationInfo=false;
        BRSPermitInfo=false;
        BRSCourtesyInfo=false;
        selectedOption = 'All';
    //  LineId=ApexPages.currentPage().getParameters().get('LineId');
       AuthId = controller.getRecord().id;
        auth = [SELECT Id, Name,Program__c, Authorization_Type__c, Application_CBI__c FROM Authorizations__c WHERE Id = :AuthId LIMIT 1];
        //LineItemId=LineId;
        //strPathwayId=ApexPages.currentPage().getParameters().get('strPathwayId');
        Linelst=[SELECT AC_Manager_Comments__c,Release_Start_Date__c,Release_End_Date__c,AC_Unique_Id__c,Additional_Information__c,Age_in_months__c,Age_yr_months__c,Air_Transporter_Flight_Number__c,Applicant_Instructions__c,Application_Duration__c,Application_Number__c,Arrival_Date_and_Time__c,Arrival_Time__c,Authorization_Content__c,Authorization__c,Biological_Material_present_in_Article__c,Breed_Description__c,Breed_Name__c,Breed__c,CBI_Justification__c,Clerk_Comments__c,Color__c,Communication_Letter__c,Contains_Applicant_Attachments__c,Country_and_Locality_Information__c,Country_Of_Origin__c,Courtesy_Justification__c,CreatedById,CreatedDate,Date_of_Birth__c,DeliveryRecipient_Email__c,DeliveryRecipient_Fax__c,DeliveryRecipient_First_Name__c,DeliveryRecipient_Last_Name__c,DeliveryRecipient_Mailing_City__c,DeliveryRecipient_Mailing_CountryLU__c,DeliveryRecipient_Mailing_Street__c,DeliveryRecipient_Mailing_Zip__c,DeliveryRecipient_Phone__c,DeliveryRecipient_USDA_License_Number__c,DeliveryRecipient_USDA_Registration_Num__c,DeliveryRec_USDA_License_Expiration_Date__c,DeliveryRec_USDA_Regist_Expiration_Date__c,Delivery_Recipient_State_ProvinceLU__c,Deny_Resubmit__c,Departure_Date_and_Time__c,Departure_Time__c,Does_This_Application_Contain_CBI__c,Domain__c,Enforcement_Officer_Comments__c,Exporter_Email__c,Exporter_Fax__c,Exporter_First_Name__c,Exporter_Last_Name__c,Exporter_Mailing_City__c,Exporter_Mailing_CountryLU__c,Exporter_Mailing_State_ProvinceLU__c,movement_type__c,Exporter_Mailing_Street__c,Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,F_Importer_First_Name__c,Hand_Carry__c,Health_Certificate_Attached__c,Id,If_yes_Please_Describe__c,Importer_Email__c,Importer_Fax__c,Importer_First_Name__c,Importer_Last_Name__c,Importer_Mailing_City__c,Importer_Mailing_CountryLU__c,Importer_Mailing_State_ProvinceLU__c,Importer_Mailing_Street__c,Importer_Mailing_ZIP_code__c,Importer_Phone__c,Importer_USDA_License_Expiration_Date__c,Importer_USDA_License_Number__c,Importer_USDA_Registration_Exp_Date__c,Importer_USDA_Registration_Number__c,Intended_Use__c,Introduction_Type__c,IsDeleted,Is_violator__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Locked__c,Means_of_Movement__c,Microchip_Number__c,Move_out_Hawaii__c,Name,Number_of_Labels__c,Number_of_New_Design_Protocols__c,Number_of_Release_Sites__c,Other_identifying_information__c,OwnerId,Picture__c,Port_of_Embarkation__c,Port_of_Entry_State__c,Port_of_Entry__c,Processed_anyway_that_change_its_nature__c,Program_Line_Item_Pathway__c,Proposed_date_of_arrival__c,Proposed_End_Date__c,Proposed_Start_Date__c,Purpose_of_Permit__c,Purpose_of_the_Importation__c,Quarantine_Facility__c,Rabies_Vaccination_Certificate_Attached__c,RecordTypeId,RecordType.Name,Rejected_Date__c,Renew_Authorization__c,Reviewer_Comments__c,Seed_be_planted_or_propagated__c,Sex__c,Status__c,Street_Address__c,SystemModstamp,Tattoo_Number__c,Test_Field__c,Thumbprint__c,Time_Zone_of_Arriving_Place__c,Time_Zone_of_Departing_Place__c,Transporter_Name__c,Transporter_Type__c,Treatment_available_in_Country_of_Origin__c,USDA_Expiration_Date__c,USDA_License_Expiration_Date__c,USDA_License_Number_Format__c,USDA_License_Number__c,USDA_Registration_Number_Format__c,USDA_Registration_Number__c,Vet_Agreement_Attached__c,Are_you_applying_for_Variance__c,Do_you_want_to_use_a_previously_approved__c,What_is_the_previously_reviewed_variance__c,How_will_variance_be_used__c,Will_the_Seed_be_processed_in_any_way_th__c FROM AC__c WHERE Authorization__c=:AuthId];
        LineItemId = Linelst[0].Id;
        LineId = LineItemId;
        UploadAttrecid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Uploaded Attachment').getRecordTypeId();
        soprecid=Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Standard Operating Procedures').getRecordTypeId();
        lstsop=appattach(soprecid);
        lstgenericattachment=appattach(UploadAttrecid);
        if(Linelst.size()>0){
            GeneralInstructions = Linelst[0].Applicant_Instructions__c;
            AppId=Linelst[0].Application_Number__c;
                                   if( Linelst[0].Proposed_Start_Date__c!=null & Linelst[0].Proposed_End_Date__c!=null){
                                                Date proposedstartdate = Linelst[0].Proposed_Start_Date__c;
                                                proposedstartdateStr = proposedstartdate.format();
                                                Date proposedenddate = Linelst[0].Proposed_End_Date__c;
                                                proposedenddateStr=proposedenddate.format();
                                         }                                        
                                        if(Linelst[0].Release_Start_Date__c!=null& Linelst[0].Release_End_Date__c!=null){
                                                Date releasestart = Linelst[0].Release_Start_Date__c;
                                                releasestartdate = releasestart.format();
                                                Date releaseend = Linelst[0].Release_End_Date__c;
                                                releaseenddate =releaseend.format();
                                          }         
        /*  if(Linelst[0].RecordType.Name=='Biotechnology Regulatory Services - Notification'||Linelst[0].RecordType.Name=='Biotechnology Regulatory Services - Standard Permit')SOPDesignProtocol=true;
            if(Linelst[0].RecordType.Name=='Biotechnology Regulatory Services - Notification')BRSNotificationInfo=true;
            if(Linelst[0].RecordType.Name=='Biotechnology Regulatory Services - Standard Permit')BRSPermitInfo=true;
            if(Linelst[0].RecordType.Name=='Biotechnology Regulatory Services - Courtesy Permit')BRSCourtesyInfo=true;
            if(Linelst[0].movement_type__c=='Interstate Movement and Release'||Linelst[0].movement_type__c=='Release'||Linelst[0].movement_type__c=='Interstate Movement'){
                BRSPermitReleaseInfo=true;
                BRSPermitInfo=false;
                BRSNotificationInfo=false;
                BRSCourtesyInfo=false;
                if(Linelst[0].RecordType.Name=='Biotechnology Regulatory Services - Courtesy Permit'){
                    BRSCourtesyInfo=true;
                    BRSPermitReleaseInfo=false;
                }
            }
            if(Linelst[0].movement_type__c=='Interstate Movement and Release')releaselocationinfo='At least 1 or more Origin Locations, 1 or more Destination Locations, and 1 or more Release Locations must be added.';
            if(Linelst[0].movement_type__c=='Interstate Movement')releaselocationinfo='At least 1 or more Origin Locations and 1 or more Destination Locations must be added.';
            if(Linelst[0].movement_type__c=='Import')releaselocationinfo='1 Origin Locations and 1 Destination Locations must be added.';
            if(Linelst[0].movement_type__c=='Release')releaselocationinfo='At least 1 or more Release Locations must be added.';*/
        }
        /*List<Program_Line_Item_Pathway__c> linepthwaylst=[SELECT Id,Display_Applicant_Attachment_Button__c,Display_Construct_Button__c,Display_Design_Protocol_SOP_Button__c,Display_Location_Button__c,Display_Regulated_Articles_Button__c,Previously_approved_Constructs_SOPs__c FROM Program_Line_Item_Pathway__c WHERE Id=:strPathwayId];
        if(linepthwaylst.size()>0){
            for(Program_Line_Item_Pathway__c p:linepthwaylst){
                if(p.Display_Applicant_Attachment_Button__c==true)aabutton=true;
                if(p.Display_Construct_Button__c==true)constructbutton=true;
                if(p.Display_Design_Protocol_SOP_Button__c==true)sopbutton=true;
                if(p.Display_Location_Button__c==true)locationbutton=true;
                if(p.Display_Regulated_Articles_Button__c==true)RAbutton=true;
                if(p.Previously_approved_Constructs_SOPs__c==true)prevReviewConst=true;
            }
        }*/
        getConstruct();
        getArticleSuppliers();
        //W-031276 - applying the new geno type datamodel based query
        //genoTypesByConstruct = getGenoTypesForConstruct(listConstructs);
        getRA();
        getLocation();
        getreports();
        getRevCons();
        getRevSOP();
        GenerateDocLauncherURLs();

    }
    public string phenoTypeByConMapkeyset {get;set;} 
   private Map<Id, List<Genotype__c>> getGenoTypesForConstruct(List<Construct__c> constructList){
        
        //W-031276 - applying the new geno type datamodel based query
        //Map<Id, Map<String, List<Genotype__c>>> genotypesByConMap = new Map<Id, Map<String, List<Genotype__c>>>();
        Map<Id, List<Genotype__c>> genotypesByConMap = new Map<Id, List<Genotype__c>>();
        //System.debug('Get GenoTypes loop' + constructList);
        for(Construct__c con : constructList){
            
            //W-031276 - applying the new geno type datamodel based query
            /*Map<String, List<Genotype__c>> genoTypesByRecordType = new Map<String, List<Genotype__c>>();
            for(Genotype__c gt : con.Genotypes__r){
                if(genoTypesByRecordType.containsKey(gt.RecordType.name)){
                    genoTypesByRecordType.get(gt.RecordType.name).add(gt);
                }else{
                    genoTypesByRecordType.put(gt.RecordType.name, new Genotype__c[]{gt});
                }
            }
            genotypesByConMap.put(con.id, genoTypesByRecordType);*/
            List<Genotype__c> genoList = new List<Genotype__c>();
            genoList = con.Genotypes__r;
            genotypesByConMap.put(con.id, genoList);
            
            
            if(con.phenoTypes__r==null){
                //system.debug('con.id###'+con.id);
            }
            if(con.phenoTypes__r.size()>0){
                //system.debug('con.phenoTypes__r2###'+con.phenoTypes__r);
                phenoTypeByConMap.put(con.id, con.phenoTypes__r);
            }else{
                 phenoTypeByConMap.put(con.id, null);
            }
            
            //system.debug('phenoTypeByConMap@@@'+phenoTypeByConMap);
            //system.debug('phenoTypeByConMap.key$$$$'+phenoTypeByConMap.keySet());
            //system.debug('phenoTypeByConMap.Value####'+phenoTypeByConMap.values());
            
        }
       // phenoTypeByConMapkeyset = string.valueof(phenoTypeByConMap.keySet()).replace('{','').replace('}','');
        phenoTypeByConMapkeyset = string.valueof(phenoTypeByConMap.keySet());
        return genotypesByConMap;
    }

  /**********************************************************************************
  ****Prev Reviewed Construct Wrapper********
  *************************************************************************************/
    
   public class PreConstructWrapper{ 
        public Construct_Application_Junction__c preconrec{get;set;}
        public construct__c ConRec {get;set;}
        public list<phenotype__c> PhenoList{get;set;}
        public list<Genotype__c> GenoList{get;set;}
        public PreConstructWrapper( Construct_Application_Junction__c preconrec,construct__c ConRec,list<phenotype__c> PhenoList ,Genotype__c GenoList){
            preconrec = preconrec;
            ConRec = ConRec;
            PhenoList = PhenoList;
            GenoList = GenoList;
         }
     }
    public list<PreConstructWrapper> PreConstructWrapperList{get;set;}
    public list<PreConstructWrapper> getPrevRevConstruct(List<Construct_Application_Junction__c> RevCons){ 
        PreConstructWrapperList = new list<PreConstructWrapper>();
        for(Construct_Application_Junction__c revcon :RevCons){
            //system.debug('revcon.construct__r@@@'+revcon.construct__r);
            //system.debug('revcon.construct__r.phenoTypes__r@@@'+revcon.construct__r.phenoTypes__r);
            //system.debug('revcon.construct__r.GenoTypes__r@@@'+revcon.construct__r.GenoTypes__r);
          /* PreConstructWrapper precon = new PreConstructWrapper(revcon,revcon.construct__r,revcon.construct__r.phenoTypes__r,revcon.construct__r.genoTypes__r);
              precon.preconrec = revcon;
              precon.ConRec = revcon.construct__r;
              precon.phenoList = revcon.construct__r.phenoTypes__r;
              precon.genoList = revcon.construct__r.genoTypes__r;
              PreConstructWrapperList.add(precon);*/
        }
        //system.debug('PreConstructWrapperList@@@'+PreConstructWrapperList);
       return PreConstructWrapperList; 
        
    }

    
/**** Load Construct to Line Items ****/
    public List<Construct__c> getConstruct(){
        //W-031276 - applying the new geno type datamodel based query
        //listConstructs = EFLBRSLineItemUtility.getConstruct('Line_item__c', LineItemId);
        listConstructs = new List<Construct__c>();
        listConstructs = [SELECT Id,Construct_s__c,Mode_of_Transformation_Text__c,
                      CreatedDate,Regulated_Article__c, Corrections_Required__c,
                      Name, Status_Graphical__c,status__c, Link_Regulated_Article__c,
                     (SELECT ID,Phenotype_Category_Text__c,Phenotypic_Category__c,Phenotypic_Description__c 
                      FROM Phenotypes__r
                      ORDER BY CreatedDate ASC),
                     (SELECT ID,Name,GenotypeType__c,GenotypeType__r.Genotype_Category__c,recordtype.name,recordtype.Id,
                      Genotype__c,Construct_Component__c,Construct_Component_Text__c,
                      Construct_Component_Name__c,Donor_list__c,
                      Related_Construct_Record_Number__c,Construct_Component_Sort_Order__c,
                      Related_Construct_Record_Number__r.Construct_s__c, Description__c
                      FROM Genotypes__r 
                      ORDER BY 
                      GenotypeType__r.Name ASC,
                      GenotypeType__c ASC,
                      Construct_Component_Sort_Order__c ASC,
                      CreatedDate ASC
                     )  
                     FROM Construct__c 
                     WHERE line_item__c =:lineItemId 
                     ORDER By CreatedDate ASC
                     ];
        return listConstructs;
    }
    /**** Load Construct to Line Items ****/
    public List<Article_Supplier_Developer__c> getArticleSuppliers(){
        listArticleSuppliers = EFLBRSLineItemUtility.getArticleSuppliers('Line_Item__c', LineItemId);
        return listArticleSuppliers;
    }
/**** Load Locations to Line Items ****/ 
      
    public List<Location__c> getLocation(){
       // locations =  EFLBRSLineItemUtility.getLocation(LineItemId);
         locations = EFLLocationUtility.getSortedLocations(LineItemId); 
        //system.debug('locations@@@'+locations);
        return locations;
    }
/**** Load Self Operating Procedure & Attachments to Line Items ****/   
    public List<Applicant_Attachments__c> appattach(id rectypeid){
        lstappattachment=EFLBRSLineItemUtility.appattach(LineItemId, rectypeid);
        return lstappattachment;
    }
/**** Load Regulated Articles related to Line Items ****/   
    public List<Link_Regulated_Articles__c> getRA(){
        linkedRas = EFLBRSLineItemUtility.getRa(LineItemId);
        return linkedRas;
    }
/**** Load Previously approved constructs to Line Items ****/   
    public List<Construct_Application_Junction__c> getRevCons(){
        revCons = new List<Construct_Application_Junction__c>(EFLBRSLineItemUtility.getRevCons(LineItemId));
        System.debug('313revCons' + revCons + 'lineitemid' + LineItemId);
        Set<Id> conIds = new Set<id>();
        for(Construct_Application_Junction__c caj : revCons){
            conIds.add(caj.construct__c);
        }
      /*  String soqlQuery = 'SELECT ID,Name,Line_Item__c,Name__c,Construct_s__c,CreatedDate,Corrections_Required__c,Mode_of_Transformation__c,Mode_of_Transformation_Text__c,Status__c,Status_Graphical__c,Total_No_of_PhenoTypes__c,Link_Regulated_Article__c,' +  
               '(SELECT id,Name,Description__c,Construct_Component__c,Construct_Component_Text__c,Donor__C,Construct_Component_Name__c,Genotype__c,Related_Construct_Record_Number__c,Record_Type_Text__c,Donor_2__c,Donor_3__c,Donor_4__c,Donor_5__c,Donor_List__c,RecordType.Name,recordtype.Id FROM Genotypes__r ),' + 
               '(SELECT id, Name, Phenotype_Category_Text__c,Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,Construct__r.Id FROM Phenotypes__r) ' +
                'FROM Construct__c WHERE ID IN :conIds';
        List<Construct__c> conList = new List<Construct__c>((List<Construct__c>)Database.query(soqlQuery));  */
        System.debug('322conIds' + conIds);
        List<Construct__c> conList = EFLConstructRepository.selectByIds(conIds); 
        System.debug('323--- + conlist' + conList);
        genoTypesByRevConstruct = getGenoTypesForConstruct(conList);    
                System.debug('324---');
        getPrevRevConstruct(revCons);
        return revCons;
    }
/**** Load Previously approved SOP's  related to Line Items ****/   
    public List<Construct_Application_Junction__c> getRevSOP(){
        
        conAppJunList = new List<Construct_Application_Junction__c>(EFLBRSLineItemUtility.getRevSOP(LineItemId));
        set<id> appAttachIds = new set<id>();
        for(Construct_Application_Junction__c conApp : conAppJunList){
            appAttachIds.add(conApp.Design_Protocol_Record_SOP__c);
        }
        for(Applicant_Attachments__c aa: [select id, (SELECT ID,Name FROM Attachments LIMIT 1) from Applicant_Attachments__c where id in:appAttachIds]){
                if(aa.Attachments != null){
                    sopAttachmentsMap.put(aa.id, aa.Attachments);
                }else{
                    sopAttachmentsMap.put(aa.id, new List<Attachment>{});
                }
        }
        return conAppJunList;
    }
/**** Delete any attachments ****/  
    public PageReference deleteRecord(){
        string sobjkey=delrecid.substring(0,3);
        string sobjname=sobjectkeys.get(sobjkey);
        string strqurey='select id from '+sobjname+' where id=:delrecid';
        list<sobject> lst=database.query(strqurey);
        Delete lst;
        PageReference dirpage=new  PageReference('/apex/CARPOL_LineItemPageForApplicant?LineId='+LineItemId);
        dirpage.setRedirect(true);
        return dirpage;
    }
/***** START----- Save Button logic for each section ****/
/***** Added by Venkata Bhavanam on 07-26-17 *****/
 
   public Pagereference saveconstructs(){
        try{
            update listConstructs;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
           getConstruct();
           return null;   
    }  
    
       public void saveLocations(){
        try{
             for(Location__c l : locations){
               if(l.Status__c == 'Review Complete'){
                l.Applicant_Instructions__c='';
             }
            }        
            update locations;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        getLocation();
   
       
    }
    
    public void save(){
        try{
        update revCons;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
    getRevCons();
    
    }
    
    public void saveprevrop(){
    
    try{
        update conAppJunList;
    }
    catch(Exception e){
        ApexPages.addMessages(e);
    }
    getRevSOP();
    
    }
    
    public void saveattach(){
    
    try{
        update lstgenericattachment;
    }
    catch(Exception e){
        ApexPages.addMessages(e);
    }
    lstgenericattachment=appattach(UploadAttrecid);
    } 
    
    public void saveRAs(){
        Set<Id> raIds = new Set<Id>();
        List<construct__c> conTobeUpdated = new List<Construct__c>();
        List<sObject> sObjTobeUpdated = new List<sObject>();
        
        for(Link_Regulated_Articles__c lRA : linkedRas){
            if(lRA.status__c == 'Denied'){
                raIds.add(lRA.Regulated_Article__c);
            }
        }
        for(Construct__c con : listConstructs){
            if(raIds.contains(con.Link_Regulated_Article__c)){
                con.status__c = 'Waiting on Customer';
                conTobeUpdated.add(con);
            }
        }
        if(!conTobeUpdated.isempty()){
            sObjTobeUpdated.addAll((List<sObject>)conTobeUpdated);
        }
        sObjTobeUpdated.addAll((List<sObject>)linkedRas);
        try{
            update sObjTobeUpdated;
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        getRA();
        getConstruct();
    }
    
    public void savelineitem(){
       if(Linelst.size()>0){
          Linelst[0].Applicant_Instructions__c = GeneralInstructions; 
       }
       try{
           // Check if the user has update access on the Line Item
           EFLEnforceAccessUtility.checkObjectUpdateAccess('ac__c');
           update Linelst;

       }
       catch(exception e){
            EFLErrorLog.createErrorLog('EFLLineItemReviewController.savelineitem()',e);
            ApexPages.addMessages(e);
       }
    }    
    public void savelstSop(){
        try{
            update lstSop;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        lstsop=appattach(soprecid);
    }
/***** END----- Save Button logic for each section ****/  
/***** START----- Approve All Button logic for each section ****/ 
/***** Added by Venkata Bhavanam on 07-26-17 *****/
   
    public void markStatusAsReviewComplete(){
        for(Construct__c ct : listConstructs){
            ct.Status__c = 'Review Complete';
        }
        update listConstructs;
        getConstruct();
    }
    
    public void approveALLLocations(){
        try{
            for(Location__c l : locations){
                l.Status__c = 'Review Complete';
            }
            EFLEnforceAccessUtility.checkObjectUpdateAccess('Location__c');
            update locations;
            getLocation();
        }catch(Exception e){
            EFLErrorLog.createErrorLog('EFLLineItemReviewController.approveALLLocations()',e);
            ApexPages.addMessages(e);
        }   
    } 
    
    public void approveALLattachments(){
        for(Applicant_Attachments__c agreeall : lstgenericattachment){
            agreeall.Status__c = 'Review Complete';
        }
        update lstgenericattachment;
        lstgenericattachment=appattach(UploadAttrecid);
    }
    
    
    public void approveall(){
        for(Construct_Application_Junction__c ct : revCons){
            ct.Status__c = 'Review Complete';
        }
        update revCons;
        getRevCons();
    }
    
    public void approveallprevsop(){
        for(Construct_Application_Junction__c ct : conAppJunList){
            ct.Status__c = 'Review Complete';
        }
        update conAppJunList;
        getRevSOP();
    }
    
/***** END----- Approve All Button logic for each section ****/   
     
    public Pagereference redirect(){
        PageReference dirpage=new  PageReference('/apex/EFLLineitemEdit?id='+Authid);
        dirpage.setRedirect(true);
        return dirpage;     
    } 
    
    public void getreports(){
        
        for(Report report: [select id,name,developername from report where developername =: 'Line_Item_with_Constructs' or developername =: 'BRS_Line_Item_with_Locations']){
           
           if(report.developername == 'BRS_Line_Item_with_Locations' )
               locationreportid = report.id;
            if(report.developername == 'Line_Item_with_Constructs' )
               Constructreportid = report.id; 
        }
        
    }
    public Pagereference saveArticleSuppliers(){
        try{
            update listArticleSuppliers;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
           getArticleSuppliers();
           return null;   
    }  
    





    public void GenerateDocLauncherURLs() { 
        String docLauncherURL = 'Doc Launcher URL';
        String scmPath = 'ScmPath Path';
        String aid = 'aid'; 
        
        final String notificationWorkFlowCBI = 'Notification CBI Application';
        final String notificationWorkFlowNoCBI = 'Notification No CBI Application';
        final String notificationWorkFlowCBIDeleted = 'Notification CBI Deleted Application';
        
        final String permitWorkFlowCBI = 'Permit CBI Application';
        final String permitWorkFlowNoCBI = 'Permit No CBI Application';
        final String permitWorkFlowCBIDeleted = 'Permit CBI Deleted Application';
        
        SpringCM_Components__mdt[] springCMComonents =  [SELECT MasterLabel, Component_Value__c FROM SpringCM_Components__mdt];
        for(SpringCM_Components__mdt scmComp:springCMComonents){
            if(scmComp.MasterLabel == docLauncherURL){
                docLauncherURL = scmComp.Component_Value__c;
            } 
            else if(scmComp.MasterLabel == scmPath){
                scmPath = scmComp.Component_Value__c;
            }
            else if(scmComp.MasterLabel == aid){
                aid = scmComp.Component_Value__c;
            }
        }
        if(auth.Authorization_Type__c == 'Notification'){
            if(auth.Application_CBI__c == 'Yes'){
                docLauncherFullURL = docLauncherURL + notificationWorkFlowCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
                //system.debug('docLauncherFullURLCBIDeleted2:'+docLauncherFullURLCBIDeleted);
                docLauncherFullURLCBIDeleted = docLauncherURL + notificationWorkFlowCBIDeleted + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath; 
            }
            else{
                docLauncherFullURL = docLauncherURL + notificationWorkFlowNoCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
        }
        else
        {
            if(auth.Application_CBI__c == 'Yes'){
                docLauncherFullURL = docLauncherURL + permitWorkFlowCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
                //system.debug('docLauncherFullURLCBIDeleted4:'+docLauncherFullURLCBIDeleted);
                docLauncherFullURLCBIDeleted = docLauncherURL + permitWorkFlowCBIDeleted + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
            else{
                docLauncherFullURL = docLauncherURL + permitWorkFlowNoCBI + 
                    '?aid=' + aid + '&eos[0].Id=' + auth.Id + 
                    '&eos[0].System=Salesforce&eos[0].Type=Authorizations__c&eos[0].Name=' + 
                    auth.Name + '&eos[0].ScmPath=' + scmPath;
            }
        }
    }   
}