@isTest(seealldata=true)
public class EFLUserUtility_test {

static testMethod void userDetails() {       
            Id ProfileId = [SELECT Id FROM Profile WHERE Name IN ('eFile Applicant')].Id;
            User u = [Select Id, IsPortalEnabled, ContactId From User where IsPortalEnabled = true AND isActive = true AND ProfileId =: ProfileId Limit 1];
            system.runAs(u) {
            EFLUserUtility.isAccountAdmin();
            contact con = EFLUserUtility.contactDetails;
            system.assert(con != null); 
        }
    }
}