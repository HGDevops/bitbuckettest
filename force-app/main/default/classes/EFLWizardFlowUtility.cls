public class EFLWizardFlowUtility {
    
    public static EFLWizardActivity getNextUnansweredQuestion(EFLWizardFlow wizardFlow) {
        EFLWizardActivity wizardActivity;
        if(wizardFlow.currentQuestion.EvaluatedOption != null )
        {
            // does the question have value based logic that derives nextscreen 
            ID nextQuestionID = wizardFlow.currentQuestion.EvaluatedOption.Wizard_Next_Question__c;
            Wizard_Questionnaire__c nextQuestion = EFLPSQUtility.getNextQuestion(nextQuestionID);
            System.debug('nextQuestion: '+ nextQuestion);
            if(nextQuestion != null) {
                
                List<Wizard_Selections__c> questionOptions = EFLPSQUtility.getQuestionOptions(nextQuestion.ID,'',null); //TODO: create EFLPSQUtility.getQuestionOptions(nextQuestion.ID)
                wizardActivity = new EFLWizardActivity(wizardFlow,nextQuestion,questionOptions);
            } 
            if(wizardActivity != null){
                wizardFlow.QuestionList.add(wizardActivity);
                wizardFlow.currentQuestionIndex += 1;
            }
        }
        return wizardActivity;
    }
    
}