public class EFLPreScreeningWrapper {
    
    public Application__c application {get;set;}
    //public boolean checkToggle{get;set;}
    public EFLPSQQuestion currentQuestion {get;set;}
    public integer currentQuestionIndex {get; set;}
    
    public List<EFLPSQQuestion> QuestionList {get;set;}
    
    public EFLIPreScreeningProcessor PreScreeningProcessor {get; set;}
    
    public EFLPreScreeningWrapper() {  }
    
    public EFLPreScreeningWrapper( EFLIPreScreeningProcessor Processor) {       
        
        PreScreeningProcessor = Processor;
        currentQuestionIndex = 0;
        QuestionList = new List<EFLPSQQuestion>();
        application = new application__c();
    }
    
    private void initializeProcessor( EFLIPreScreeningProcessor Processor)
    {
        PreScreeningProcessor = Processor;
        System.debug('PreScreeningProcessor' + PreScreeningProcessor);
    }  
    
    public EFLPSQQuestion getNextQuestion()
    {
        PreScreeningProcessor.getNextQuestion(this);
        return this.currentQuestion ;
    }
    
    public EFLPSQQuestion getPrevQuestion()
    {
        PreScreeningProcessor.getPrevQuestion(this);
        return this.currentQuestion ;
    }
    
    /* public boolean  togglecheck()
    {
        system.debug('checkToggle'+ checkToggle);
        if (checkToggle == true)
        {checkToggle = false;}
        else
        checkToggle = true;
        
        return checkToggle;
    
    }*/
    
}