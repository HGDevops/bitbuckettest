public interface EFLInspQuestionnaireQnsEngine {
    void reorderInspectionQuestions(list<EFL_Inspection_Questionnaire_Questions__c> questionnaireList);
}