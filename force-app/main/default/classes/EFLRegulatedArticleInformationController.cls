public with sharing class EFLRegulatedArticleInformationController {
    
   public Id lineItemId {get;set;}
   public string delrecid{get;set;}
   public Map<string,string> sobjectkeys{get;set;}
   public RegulatedArticle_Information__c RAINFRecord{get;set;}
   public id RAINFID{get;set;} 
   public List <AC__c> relatedLineItem{get; set;}
   public String URL{get; set;}
   public boolean detectedChanges {get; set;}
   public Boolean dcrptnrequired{get;set;}
   public String selectedbreed{get;set;}
   public static final string AC_RAI_RETURN_TO_LIST_CONFIRMATION = 'ACRAIReturnToListConfirmation';
   public string detailPageInstruction {get; set;}
   public String confirmationMessage {
       get{
           confirmationMessage = '';
           confirmationMessage = EFLGenericUtility.getMessage(AC_RAI_RETURN_TO_LIST_CONFIRMATION);
           return confirmationMessage;
       }
       set;
   } 
  
   public list<RegulatedArticle_Information__c> RAINFList{get
    { 
        if(lineItemID!=NULL){ 
            RAINFList= EFLregulatedarticleinformationRepository.selectRAINFByLineItemID(lineItemID);}
       // system.debug('RAINFList###'+RAINFList);
       return RAINFList; 
    }set;}
    
    public AC__c lineItemRecord{      
        get{
            if(lineItemRecord==null && LineItemId!=null){ 
                lineItemRecord = efllineitemrepository.selectbyID(lineItemID);
            }
            return lineItemRecord;
        }set;
    }
    public boolean lockLineItem{
        get{
         lockLineItem = EFLLockUnlockUtility.lockLineitem(lineItemRecord);
         return lockLineItem;
        }set; 
    }
  
    
   //Rerender Attributes
    public boolean renderList {get;set;}
    public boolean renderDetail {get;set;}
    public boolean renderSpringCM {get; set;}
    public Boolean disableUpload{get; set;}
    public Boolean showRegulatedArticleView {get;set;}
    public String instructions {get;set;}

        
    public EFLRegulatedArticleInformationController(ApexPages.StandardController controller){ 
       lineItemId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('LineItemId'));
       relatedLineItem = new List <AC__c>();
       relatedLineItem = [SELECT id, Name FROM AC__c where id =: lineItemId];
       renderDetail = false;
       renderList = true;
       renderSpringCM = false;
       disableUpload = true;
       detectedChanges=false;
       dcrptnrequired = false;
       getInstructions();
       Map<string,Schema.SobjectType> describe=Schema.getGlobalDescribe();
       sobjectkeys=new  Map<string,string>();
        for(string s:describe.keyset())sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
        
        
    } 
    
       public void getInstructions(){    
        if (instructions == NULL) {      
            instructions =  EFLGenericutility.getInstructions(NULL,NULL,'Animal Information');      
            }     
            }  
    
      public void showRegulatedArticleDetailView() {
        RAINFRecord = EFLregulatedarticleinformationRepository.selectByID(rainfID);        
        showRegulatedArticleView = true;
        renderDetail = false;
        renderList = false;
        renderSpringCM = false;
      }
    
    public void showDetail(){
        RAINFRecord = new RegulatedArticle_Information__c();
        if(RAINFID!=null){
           RAINFRecord = EFLregulatedarticleinformationRepository.selectByID(rainfID);
           renderSpringCM = true;
           disableUpload = false;
           detailPageInstruction = 'Add Health Certificate, Rabies Vaccination Certificate, and other applicable documents using the “Upload Document” button which will pop-up a new window for you to select and provide details about your document. Please ensure your pop-up blocker is disabled. After a successful upload, the window will close automatically. <br/> <br/> To view the newly uploaded document, please refresh the folder “RAINF-000000xxx” by clicking on the folder icon in the top left corner. <br/> <br/> When you have completed your upload task, click the “Return to List of Dogs” button to review all dogs on this application and/or continue to add other dogs.';
        }else{
           RAINFRecord = new RegulatedArticle_Information__c();
           renderSpringCM = false;
           disableUpload = true;
           detailPageInstruction = 'You must click Save to enable document uploads.';
        }        
        renderDetail = true;
        renderList = false;
    }
    
    public void cancel(){
        RAINFID=null;
        renderDetail = false;
        renderList = true;
        renderSpringCM = false;
    } 
    
   public void updateRAINF(){    
      
       try{       
         if(RAINFRecord.line_Item__c ==null){
              RAINFRecord.line_Item__c = lineItemID;
           }
           if(RAINFRecord.id != NULL){
               // Purpose: As Document status is updated from springCM workflow and when user doesn't refresh widget, 
               //          we are quering for current document status 
               string currentDocumentStatus = [select id, document_Status__c
                                               from RegulatedArticle_Information__c
                                                where id =: RAINFRecord.id
                                               limit 1].document_Status__c;
               RAINFRecord.document_Status__c = currentDocumentStatus; 
           } 
            upsert RAINFRecord;
           if(RAINFID==null)
            {
                RAINFID = RAINFRecord.id;
                RAINFRecord = new RegulatedArticle_Information__c();
                RAINFRecord = EFLregulatedarticleinformationRepository.selectByID(rainfID);
            }  
            renderDetail = true;
            renderList = false;
            renderSpringCM = true;
            disableUpload = false;

           RAINFList = EFLregulatedarticleinformationRepository.selectRAINFByLineItemID(lineItemID); 
           detailPageInstruction = 'Add Health Certificate, Rabies Vaccination Certificate, and other applicable documents using the “Upload Document” button which will pop-up a new window for you to select and provide details about your document. Please ensure your pop-up blocker is disabled. After a successful upload, the window will close automatically. <br/> <br/> To view the newly uploaded document, please refresh the folder “RAINF-000000xxx” by clicking on the folder icon in the top left corner. <br/> <br/> When you have completed your upload task, click the “Return to List of Dogs” button to review all dogs on this application and/or continue to add other dogs.';
       }  catch( Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
    }      
    
   public void deleteRecord(){ 
       system.debug('deleteRecord');
       string sobjkey=delrecid.substring(0,3);
       string sobjname=sobjectkeys.get(sobjkey);
       string strqurey='select id from '+sobjname+' where id=:delrecid';
       list<sobject> lst=database.query(strqurey); 
       Delete lst;      
       
    } 
    
    //Added by Conor to launch Doc Launcher from custom button
    public String fireDocLauncher(){
        //lineItemId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('LineItemId'));
        //String URL = EFLSpringCMUtility.buildDocLauncherURL(lineItemId, lineItemRecord.Name, '', 'Live Dogs');
        String URL = EFLSpringCMUtility.buildDocLauncherURL(rainfID, RAINFRecord.Name, '', 'Live Dogs');
        return URL;
    }
    
    public void showListView(){
        renderDetail = false;
        renderList = true;
        renderSpringCM = false;
        RAINFID=null;
        RAINFRecord = null;
        detectedChanges = false;
    }
    
    //detect if there were unsaved changes and prompt the user before returning to list is clicked
    public void detectChanges()
    {
        id currentRecordId = rainfID;
        if(currentRecordId==null)
        {
            if(RAINFRecord.id!=null)
            {
                currentRecordId = RAINFRecord.id;
            }
        }
        //if Existing record then get record for comparison
        if(currentRecordId!=null)
        {
            RegulatedArticle_Information__c serverRAINFRecord = new RegulatedArticle_Information__c();
            serverRAINFRecord = EFLregulatedarticleinformationRepository.selectByID(currentRecordId);
            //compare UI record to Server record to detect changes
            if(RAINFRecord.Dog_Name__c!=serverRAINFRecord.Dog_Name__c
            || RAINFRecord.Color__c != serverRAINFRecord.Color__c
            || RAINFRecord.Sex__c != serverRAINFRecord.Sex__c
            || RAINFRecord.Breed_Description__c != serverRAINFRecord.Breed_Description__c
            || RAINFRecord.Date_of_Birth__c!= serverRAINFRecord.Date_of_Birth__c
            || RAINFRecord.Breed__c != serverRAINFRecord.Breed__c
            || RAINFRecord.Tattoo_Number__c != serverRAINFRecord.Tattoo_Number__c
            || RAINFRecord.Microchip_Number__c != serverRAINFRecord.Microchip_Number__c
            )
            {
                detectedChanges=true;
            }
            else
            {
                detectedChanges=false;
            }
       }
       //if New record then check if UI fields have values
       else
       {
           if(RAINFRecord.Dog_Name__c!=null
            || RAINFRecord.Color__c != null
            || RAINFRecord.Sex__c != null
            || RAINFRecord.Breed_Description__c != null
            || RAINFRecord.Date_of_Birth__c!= null
            || RAINFRecord.Breed__c != null
            || RAINFRecord.Tattoo_Number__c != null
            || RAINFRecord.Microchip_Number__c != null
            )
            {
                detectedChanges=true;
            }
            else
            {
                detectedChanges=false;
            }
       }
    }
    
    
    public void Descriptionrender(){
        dcrptnrequired = false;
        Breed__c breed= new Breed__c();
       // system.debug('selected breed' +selectedbreed);
        if(selectedbreed != null)
        {
         List<Breed__c> breedList = new List<Breed__c>();
         breedList = [Select Id,name from Breed__c where Id = :selectedbreed ];
         if(breedList.size()>0)
             breed= breedList[0];
        }
        if(breed!= Null)
        {
          if(breed.Name != null){
              if(breed.Name.equals('Other'))
              {
                  dcrptnrequired = True;
              }
          }
          else
          {
              dcrptnrequired = false;
          }   
        }
       // system.debug('dcrptn required ' +dcrptnrequired );
    }    
     
}