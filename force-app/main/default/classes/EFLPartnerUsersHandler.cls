/**********************************************************************  
* @Purpose : This Handler class handles the logic to retrieve 
*            major functionality for APHIS Parnter Users 
*            (SPRO/SPHD/FSIS/FADDL/NVSL) in efile. 
* @User Story Number :  
************************************************************************/
public with sharing class EFLPartnerUsersHandler{

 /* 
   Reviewers Wrapper 
 */
    public class reviewersWrapper {
        public Reviewer__c reviewerRec { get;set;}
        public string regulatedArticles { get;set;}
        public reviewersWrapper(Reviewer__c reviewerRec, string regArticles) {
            reviewerRec = reviewerRec;
            regulatedArticles = regArticles;
        }
    }

 /* 
   get Reviewers Records 
 */
    public static list < reviewersWrapper > getReviewerRecords() {
        list < reviewersWrapper > revWrapperList  = new list < reviewersWrapper >();
        List < Reviewer__c > reviewersList = new List < Reviewer__c > ();
        //retrieve all SPRO records 
        reviewersList = [SELECT Id, Name,
            State_Regulatory_Official__c, Authorization_ID__c,
            Expedite__c, State_Regulatory_Official__r.name,
            BRS_State_Reviewer_Email__c, State__c, Authorization__r.name,
            Authorization__r.AC_Applicant_Name__c, Authorization__r.high_priority_flag__c,
            Authorization__c, Authorization_Name__c, Applicant__c, Status__c,
            Type_of_Application__c, State_Review_Duration__c,
            State_Comments__c, state_has_no_comments__c,
            Requirement_Desc__c, Review_Complete__c, Days_Left__c,
            State_has_comments__c, No_Requirements__c,
            State_not_Responded__c, Notes_to_State__c,
            CreatedByID, LastModifiedDate, CreatedDate, LastModifiedByID,
            (SELECT Name, Description, ContentType, CreatedById, Id, ParentId FROM Attachments)
            FROM Reviewer__c
            WHERE State__c =: EFLUserUtility.userDetails.Contact.State__r.name
              AND (RecordType.Id =: EFLGenericUtility.getRecordTypeId('Reviewer BRS SPRO') 
              OR  RecordType.Id =: EFLGenericUtility.getRecordTypeId('Reviewer PPQ SPRO')) 
            ORDER BY CreatedDate DESC
            LIMIT 999
        ];
        list < Regulated_Article__c > regarticlelist = new list < Regulated_Article__c > ();
        map < id, list < Regulated_Article__c >> mapAuthRa = new map < id, list < Regulated_Article__c >> ();
        list < id > authorizationIdList = new list < id > ();
        revWrapperList = new List < reviewersWrapper > ();
        //obtain all authorization id's of all official review records 
        for (Reviewer__c rev: reviewersList) {
            if (rev.Authorization_ID__c != null) {
                authorizationIdList.add(rev.Authorization_ID__c);
            }
        }
        //obtain all regulated articles for each official review record - authorization   
        mapAuthRa = EFLUNIgetRAfromAuthorization.getRegulatedArticles(authorizationIdList);

        //prepare the reviewers wrapper list with all regulated articles
        for (Reviewer__c rev: reviewersList) {
            if (rev.Authorization_ID__c != null) {
                if (mapAuthRa.get(rev.Authorization_ID__c) != null) {
                    string ralist = '';
                    for (Regulated_Article__c r: mapAuthRa.get(rev.Authorization_ID__c)) {
                        if (ralist == '' || ralist == NULL)
                            ralist = r.name;
                        else
                        if (r.name != null && r.name != '')
                            ralist = ralist + ';' + r.name;
                    }
                    reviewersWrapper rw = new reviewersWrapper(rev, ralist);
                    rw.reviewerRec = rev;
                    rw.regulatedArticles = ralist;
                    revWrapperList.add(rw);
                }
            }

        }
        // return the wrapper list  
        return revWrapperList;
    }

 /* 
   get Reviewers related Requirements(Regulations) 
 */    
    public static List < Regulation__c > stateRegulationsList {
        get {
            if (stateRegulationsList == null) {
                try{
                stateRegulationsList = [SELECT ID, Name, Regulation_Description__c, State__c, 
                                             Title__c, Type__c, Status__c, Custom_Name__c, Short_Name__c 
                                        FROM Regulation__c 
                                       WHERE State__c =: EFLUserUtility.contactDetails.State__c 
                                         and RecordTypeId =: EFLGenericUtility.getRecordTypeId('Regulations State') 
                                    ORDER BY CreatedDate DESC LIMIT 999];
                }catch(Exception e){ } 
            }
            return stateRegulationsList;
        }
        set;
    } 
    
}