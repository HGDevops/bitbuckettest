@isTest
public class EFLProgressBarControllerTest {
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    @isTest
    public static void testHappyPath()
    {
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        update newContact;
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
       
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        /*CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLChangeHistoryTrigger'; 
        chDt.Disable__c = true; 
        insert chDt; */
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Authorizations__c auth = new Authorizations__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        test.startTest();
            
        app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
        id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
        auth = new Authorizations__c();
        auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
        
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        System.RunAs(sysAdm)
		{
            UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'BRS Program Specialist');
            insert ur;
            Profile p = [SELECT Id FROM Profile WHERE Name='eFile APHIS Staff'];
            User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;
            team__c team1 = new team__c();
            team1.member_role__c='BRS Program Specialist';
            team1.Recordtypeid=Schema.SObjectType.Team__c.getRecordTypeInfosByName().get('BRS Team').getRecordTypeId();
            team1.Member__c = u1.id;
            team1.Authorization__c=auth.id;
            insert team1;
        }
        
        LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        ApexPages.CurrentPage().getparameters().put('id', auth.id);              
        Apexpages.StandardController sc = new Apexpages.StandardController(auth);
        EFLProgressBarController ext = new EFLProgressBarController(sc); 
        ext.moveToFirstStage();
        ext.currentStageIndex = 200;
        ext.moveToNextStage();
        try{ext.updateStage('Application Review');}catch(exception ex){}
        
        Inspection__c Ins = new Inspection__c();
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Ins.RecordTypeId = IncRecTypeID;
        Ins.Stage__c = 'Inspection Report';
        Ins.status__c='Cancelled';
        Ins.Reason_for_Cancellation__c = 'Test Cancellation'; 
        Ins.authorization__c = auth.id;
        insert Ins;
        try{ext.moveToNextStage();}catch(exception ex){}
        
        LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Draft');
        COnstruct__c con = testData.newconstructByLIAndRegulatedArticle(lineitem.id, ra.Id);
        try{ext.moveToNextStage();}catch(exception ex){}
        
        /*delete Ins;
        delete con;
        ext.currentStageIndex = 200;*/
        
        test.stopTest();
    }
    
    
}