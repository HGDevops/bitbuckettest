@IsTest(SeeAllData=true)
public class EFLConditionDescUpdateCtrlTest{

    static testMethod void testEFLConditionDescUpdateCtrl() {
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        
        Application__c appObj = testData.newapplication();
        
        Authorizations__c authObj = testData.newAuth(appObj.id);
        
        Regulation__c regulationbj = testData.newRegulation('','');
        
        Authorization_Junction__c aujObj = testData.newAuthorizationJunction(authObj.id, regulationbj.id);
        
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(aujObj);
            EFLConditionDescUpdateCtrl ctrlObj = new EFLConditionDescUpdateCtrl (sc);
            
            Test.setCurrentPage(Page.EFLConditionDescUpdate);
            System.currentPageReference().getParameters().put('id', aujObj.id);
            ctrlObj.saveDescription();
        Test.stopTest();    
    }
}