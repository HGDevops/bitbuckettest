@isTest(seealldata=true)
private class EFL_StateRegulationsControllerTest {
 
    @IsTest  static void preparetestData(){
        string dlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        string locrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        string oanddlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();

        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Contact objcont = testData.newcontact();
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        AC__c ac1 = testData.newLineItem('Personal Use',objapp); 
        ac1.Authorization__c = objauth.id;
        update ac1;
        Program_Line_Item_Pathway__c plip =testdata.newCaninePathway();
        Regulated_Article__c regArt = testdata.newRegulatedArticle(plip.id);
        Link_Regulated_Articles__c linkra=testdata.newlinkRegArticle(ac1.id,objapp.Id,objauth.id);
        construct__c objconst=testdata.newconstruct(ac1.id,regArt);
        
        GenotypeType__c genoTypeType = new GenotypeType__c();
        genoTypeType.Construct__c = objconst.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;
        Genotype__c objgeno=testdata.newgenotype(objconst.id,genoTypeType);
        Phenotype__C objpheno=testdata.newphenotype(objconst.id);
        Test.startTest();
        Country__c UScountry=testdata.newcountryus();
        Level_1_Region__c level1reg=testdata.newlevel1region(UScountry.id);
        Level_2_Region__c level2reg=testdata.newlevel2region(level1reg.id);
        location__c orgloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,olocrectypeid);
        location__c desloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,dlocrectypeid);
        location__c relloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,locrectypeid);
        location__c oanddloc = testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,oanddlocrectypeid); 
        Regulation__c reg = new Regulation__c();
        reg.Regulation_Description__c = 'Test';
        reg.Custom_Name__c = 'Test';
        reg.short_Name__c = 'Test'; 
        insert reg;
        list<Regulation__c> reglist = new list<Regulation__c>();
        reglist.add(reg);
        Regulation__c reg1 = testdata.newRegulation('Import Requirements','Proof of Origin');
        id recordtypeid=Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('State Regulation Junction').getRecordTypeId();
        Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
        objcaj.Line_Item__c = ac1.id; 
        objcaj.Application__c = objapp.id;
        insert objcaj;
        Attachment objattach = testData.newAttachment(objcaj.id);
        
          Reviewer__c objrev = new Reviewer__c();
          objrev.Status__c= 'Open';
          objrev.Authorization__c = objauth.id;
          objrev.State_Regulatory_Official__c = objcont.id;
          objrev.BRS_State_Reviewer_Email__c = objcont.Email;          
          insert objrev; 
           Authorization_Junction__c authJun = testdata.newAuthJunction(objauth.id,reg1.id,objrev,recordtypeid);
      //Test.startTest();
      PageReference pageRef = Page.EFL_StateRegulations;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('id',objauth.id);
      ApexPages.currentPage().getParameters().put('orr',objrev.id);
      EFL_StateRegulationsController con = new EFL_StateRegulationsController(sc);
      con.evaluateSelectedConditions();
      con.getPreviouslySelectedConditions();
      con.getNotSelectedConditions();
      con.addSelected();
      con.removeSelected();
      con.saveEditedRequirements();
      con.addMoreRequirements();
      con.createRelatedConditions(reglist);
       EFL_StateRegulationsController.wrapperRegulation eflWrap = new  EFL_StateRegulationsController.wrapperRegulation(reg1);
        EFL_StateRegulationsController.wrapperRegulation eflWrapp = new  EFL_StateRegulationsController.wrapperRegulation(reg1,authJun);
        eflWrap.selected = true;
      Test.stopTest();       
    }

}