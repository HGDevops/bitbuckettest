@isTest
public class EFLTeamTriggerHandler_Test {
    public static Authorizations__c auth;
    public static team__c team;
    static testmethod void runTest(){
     EFLTeamTriggerHandler teamhandler = new EFLTeamTriggerHandler();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Program_Prefix__c objPrefix=new  Program_Prefix__c();
        objPrefix.Program__c=testData.newProgram('BRS').Id;
        objPrefix.Name='555';
        Insert objPrefix;
        Signature__c objTP=new  Signature__c();
        objTP.Name='Test AC TP Jialin';
        objTP.Recordtypeid=Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objTP.Program_Prefix__c=objPrefix.Id;
        Insert objTP;
        Application__c app = testData.newapplication();
        auth = testData.newAuth(app.Id);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='BRS Analyst'];
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        System.RunAs(sysAdm)

         {     
            UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'BRS Program Specialist');
            insert ur;
            UserRole ur1 = new UserRole(DeveloperName = 'MyCustomRole1', Name = 'BRS Biotechnologist');
            insert ur1;
            User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;
            User u2 = new User(Alias = 'stand', Email='standarduser1@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur1.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow1@testorg.com');
            insert u2;
        
         }
        User u3 = [SELECT Id FROM User WHERE LastName='Testing'];
        User u4 = [SELECT Id FROM User WHERE LastName='Testing1'];
        team__c team = new team__c();
        team.RecordTypeId = Schema.SObjectType.Team__c.getRecordTypeInfosByDeveloperName().get('BRS_Team').getRecordTypeId();
        team.member_role__c='BRS Program Specialist';
        team.Member__c = u3.id;
        team.Authorization__c=auth.id;
        insert team;
        teamhandler.IsDisabled();
        teamhandler.bulkBefore();
        teamhandler.bulkAfter();
        teamhandler .beforeInsert(team);
        teamhandler .beforeUpdate(team, team);
        teamhandler .beforeDelete(team);
        teamhandler .afterInsert(team);
        teamhandler .afterDelete(team);
        
      team.member_role__c='BRS Biotechnologist';
      team.Member__c = u4.id;
      update team;
      teamhandler .afterupdate(team,team);
      teamhandler .andFinally();
      
        
   }
   }