public class cgcheck { 

public String findTheGroup(Map<String,String> fieldAnswerMap,String ProgPathWayID){
        system.debug('------------fieldAnswerMap'+fieldAnswerMap);
        system.debug('------------ProgPathWayID'+ProgPathWayID);
        String groupID;
        Program_Line_Item_Pathway__c proPathWay = [SELECT id,Name FROM Program_Line_Item_Pathway__c WHERE id=:ProgPathWayID];
        list<Group__c> groupOb = [SELECT  ID,Program_Line_Item_Pathway__c FROM Group__c WHERE Program_Line_Item_Pathway__c =:ProgPathWayID];
        Set<ID> groupObIDs = new Set<Id>();
        for(Group__c gp: groupOb){
            groupObIDs.add(gp.ID);
        }
        
        system.debug('<<<<<<<!!!! groupObIDs'+groupObIDs);
        list<Program_Line_Field_Groups__c> Program_Line_Field_GroupsList = [SELECT ID,Name,Program_Line_Item_Field__c,Program_Line_Item_Field__r.Field_API_Name__c,Question_Response__c,Criteria_Group__c 
                                                                            FROM Program_Line_Field_Groups__c WHERE 
                                                                            Criteria_Group__c   IN:groupObIDs] ;
                                                                            
                             system.debug('<<<<<<!!!!! Program_Line_Field_GroupsList'+Program_Line_Field_GroupsList);                                               
        
        Map<ID,List<Program_Line_Field_Groups__c>> Program_Line_Field_GroupsMap   =  new Map<ID,List<Program_Line_Field_Groups__c>>();                                                                 
        For(Program_Line_Field_Groups__c ois : Program_Line_Field_GroupsList)
        {
            if(Program_Line_Field_GroupsMap.containsKey(ois.Criteria_Group__c))
            {
                List<Program_Line_Field_Groups__c> lstoi = Program_Line_Field_GroupsMap.get(ois.Criteria_Group__c);
                lstoi.add(ois);
                
                system.debug('<<<<<<<<!!!!! lstoi '+lstoi);
            }
            else
            {
                List<Program_Line_Field_Groups__c> lstoi = new List<Program_Line_Field_Groups__c>();
                lstoi.add(ois);
                
                system.debug('<<<<<<<<!!!!! lstoi '+lstoi);
                
                Program_Line_Field_GroupsMap.put(ois.Criteria_Group__c,lstoi);
                
                system.debug('<<<<<<<<<<<!!!!!! Program_Line_Field_GroupsMap'+Program_Line_Field_GroupsMap);
            }
        }
        
        system.debug('<<<<<!!!!!!!!! Program_Line_Field_GroupsMap'+Program_Line_Field_GroupsMap);
        
        //list<Program_Line_Field_Groups__c> Program_Line_Field_GroupsList = [SELECT ID,Name,Program_Line_Item_Field__c,Program_Line_Item_Field__r.Field_API_Name__c,Question_Response__c FROM Program_Line_Field_Groups__c WHERE Criteria_Group__c =:groupOb.ID] ;
        Boolean matched = false;   
         for(ID gp: groupObIDs){
             system.debug('<<<<<<<<!!!!!!!! gp '+gp);
           if(Program_Line_Field_GroupsMap.get(gp)!=null){ 
               system.debug('<<<<<<<<!!!!!!!! Program_Line_Field_GroupsMap.get(gp) '+Program_Line_Field_GroupsMap.get(gp));
            for(Program_Line_Field_Groups__c qr: Program_Line_Field_GroupsMap.get(gp)){
                    system.debug('<<<<<<!!!!! qr'+qr);
                 matched = false;
                 if(fieldAnswerMap.containsKey(qr.Program_Line_Item_Field__r.Field_API_Name__c))
                     system.debug('<<<<<<<<!!!!!!!!! qr.Program_Line_Item_Field__r.Field_API_Name__c'+qr.Program_Line_Item_Field__r.Field_API_Name__c);
                    if( fieldAnswerMap.get(qr.Program_Line_Item_Field__r.Field_API_Name__c) == qr.Question_Response__c){
                            system.debug('<<<<<<<!!!!!! qr.Program_Line_Item_Field__r.Field_API_Name__c'+qr.Program_Line_Item_Field__r.Field_API_Name__c);
                            system.debug('<<<<<<<!!!!!! qr.Question_Response__c'+qr.Question_Response__c);
                        matched = true;
                    }
                    else
                    {
                        Break;
                    }
                  } 
                  if(matched) {
                      groupID = gp;
                      break;
                  }
                  
            }   
         }
            system.debug('<<<<<<<<<<!!!!!!!!! groupID'+groupID);
           //return groupID;  
           return 'ok';
        
    }
    
    
    }