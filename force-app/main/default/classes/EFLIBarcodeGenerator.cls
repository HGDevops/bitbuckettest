public interface EFLIBarcodeGenerator {
    
    boolean validateInput(EFLBarcodeData bcData);
    boolean encodeInput(EFLBarcodeData bcData);
    boolean generateBarcodeHTML(EFLBarcodeData bcData);


}