@isTest
public class EFLPaginationController_Test {
    
	static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    static testMethod void testHappyPath()
    {
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('Accounts'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
        Phenotype__c phenotype = new Phenotype__c();
        Genotype__c genotype = new Genotype__c(); 
        
        CARPOL_URLs__c Urls = new CARPOL_URLs__c(name='Landing_Page', URL__c='/apex/CARPOL_CommunityLandingPage');
        insert Urls;
        
        test.startTest();
        
        app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
        id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
        Authorizations__c auth = new Authorizations__c();
        //auth = testdata.newAuthByAppRecordTypeQueueIdStatus(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer');
        auth = testdata.newAuthByAppRecordTypeQueueIdStatusAccountId(app, authBRSPermitRecordTypeId, authQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', newAccount.id);
        LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
        LRA = new Link_Regulated_Articles__c();
        LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Draft');  
        
        List<Link_Regulated_Articles__c> LRAs = new List<Link_Regulated_Articles__c>();
        LRAs.add(LRA);
        
        EFLPaginationController pc = new EFLPaginationController();
        pc.idList = EFLLinkRegulatedArticleRepository.selectLRAByLineItemID(LineItem.id);
        List<String> fields = new List<String>();
        fields.add('Id');
        fields.add('Name');
        pc.SobjFieldList = fields;
        ApexPages.StandardSetController setRecords = pc.setRecords;
        pc.getSObjectRecs();
        List<String> FieldList = new List<String>();
        FieldList = pc.FieldList;
        pc.refreshPageSize();
        string searchString = pc.searchString;
        EFLBRSLineItemReviewController pageController = pc.pageController;
        test.stopTest();
    }
  
}