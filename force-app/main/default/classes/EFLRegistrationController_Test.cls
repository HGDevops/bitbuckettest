@isTest
public class EFLRegistrationController_Test{
         
    @isTest
    public static void getRegistrationsOnlyOneTest(){
        User communityUser = [SELECT Id, ContactId FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator' AND UserRoleId != null LIMIT 1];
        System.debug(communityUser);
        User user;
        System.runAs(communityUser){            
            Account account = new Account(name ='Test Community Account') ;
            insert account; 
           
            Contact contact = new Contact(LastName ='testCon',AccountId = account.Id);
            insert contact; 
            
            EFLRegistration__c testReg = new EFLRegistration__c();
            testReg.EFLAccount__c = contact.AccountId;
            testReg.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
            insert testReg;
            
            Profile profile = [SELECT Id FROM Profile WHERE Name = 'Report Preparer' LIMIT 1];
            UserRole role = [SELECT Id FROM UserRole LIMIT 1];
            
            user = new User(alias = 'test123', 
                                 email='test123@noemail.com',
                                 emailencodingkey='UTF-8', 
                                 lastname='Testing', 
                                 languagelocalekey='en_US',
                                 localesidkey='en_US', 
                                 profileid = profile.Id, 
                                 country='United States',
                                 IsActive =true,
                                 ContactId = contact.Id,
                                 timezonesidkey='America/Los_Angeles', 
                                 username='tester@noemail.com');       
            insert user;
        }
        
        System.runAs(user){
            test.StartTest();
            List<EFLRegistration__c> registrations = EFLRegistrationController.getRegistrations();
            test.StopTest();
            
            System.assertEquals(1, registrations.size(), 'There should only be 1 registration');
        }
    }
    
    @isTest
    public static void getRegistrationsMultipleTest(){
        User communityUser = [SELECT Id, ContactId FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator' AND UserRoleId != null LIMIT 1];
        System.debug(communityUser);
        User user;
        System.runAs(communityUser){            
            Account account = new Account(name ='Test Community Account') ;
            insert account; 
           
            Contact contact = new Contact(LastName ='testCon',AccountId = account.Id);
            insert contact; 
            
            EFLRegistration__c testReg = new EFLRegistration__c();
            testReg.EFLAccount__c = contact.AccountId;
            testReg.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
            insert testReg;
            
            EFLRegistration__c testReg2 = new EFLRegistration__c();
            testReg2.EFLAccount__c = contact.AccountId;
            testReg2.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
            insert testReg2;
            
            Profile profile = [SELECT Id FROM Profile WHERE Name = 'Report Preparer' LIMIT 1];
            UserRole role = [SELECT Id FROM UserRole LIMIT 1];
            
            user = new User(alias = 'test123', 
                                 email='test123@noemail.com',
                                 emailencodingkey='UTF-8', 
                                 lastname='Testing', 
                                 languagelocalekey='en_US',
                                 localesidkey='en_US', 
                                 profileid = profile.Id, 
                                 country='United States',
                                 IsActive =true,
                                 ContactId = contact.Id,
                                 timezonesidkey='America/Los_Angeles', 
                                 username='tester@noemail.com');       
            insert user;
        }
        
        System.runAs(user){
            test.StartTest();
            List<EFLRegistration__c> registrations = EFLRegistrationController.getRegistrations();
            test.StopTest();
            
            System.assertEquals(2, registrations.size(), 'There should be 2 registrations');
        }
    }
    
    @isTest
    public static void getPageDataMultipleTest(){
        User communityUser = [SELECT Id, ContactId FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator' AND UserRoleId != null LIMIT 1];
        System.debug(communityUser);
        User user;
        Integer currentFY = Date.today().month() >= 10 ? Date.today().year() + 1 : Date.today().year();
        Integer activeFY = currentFY - 1;
        System.runAs(communityUser){            
            Account account = new Account(name ='Test Community Account') ;
            insert account; 
           
            Contact contact = new Contact(LastName ='testCon',AccountId = account.Id);
            insert contact; 
            
            
            EFLRegistration__c testReg = new EFLRegistration__c();
            testReg.EFLAccount__c = contact.AccountId;
            testReg.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
            testReg.EFLActive__c = 'Y';
            insert testReg;
            
            EFLAnnual_Report__c ar1 = new EFLAnnual_Report__c();
            ar1.EFLRegistration__c = testReg.Id;
            ar1.EFLFiscal_Year__c = string.valueOf(activeFY);
            insert ar1;
            
            EFLAnnual_Report__c ar4 = new EFLAnnual_Report__c();
            ar4.EFLRegistration__c = testReg.Id;
            ar4.EFLFiscal_Year__c = string.valueOf(activeFY - 1);
            insert ar4;
            
            EFLRegistration__c testReg2 = new EFLRegistration__c();
            testReg2.EFLAccount__c = contact.AccountId;
            testReg2.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
            testReg2.EFLActive__c = 'Y';
            insert testReg2;
            
            EFLAnnual_Report__c ar2 = new EFLAnnual_Report__c();
            ar2.EFLRegistration__c = testReg2.Id;
            ar2.EFLFiscal_Year__c = string.valueOf(activeFY);
            insert ar2;
            
            EFLAnnual_Report__c ar3 = new EFLAnnual_Report__c();
            ar3.EFLRegistration__c = testReg2.Id;
            ar3.EFLFiscal_Year__c = string.valueOf(activeFY - 1);
            insert ar3;
            
            Profile profile = [SELECT Id FROM Profile WHERE Name = 'Report Preparer' LIMIT 1];
            UserRole role = [SELECT Id FROM UserRole LIMIT 1];
            
            user = new User(alias = 'test123', 
                                 email='test123@noemail.com',
                                 emailencodingkey='UTF-8', 
                                 lastname='Testing', 
                                 languagelocalekey='en_US',
                                 localesidkey='en_US', 
                                 profileid = profile.Id, 
                                 country='United States',
                                 IsActive =true,
                                 ContactId = contact.Id,
                                 timezonesidkey='America/Los_Angeles', 
                                 username='tester@noemail.com');       
            insert user;
        }
        
        System.runAs(user){
            test.StartTest();
            EFLRegistrationController.PageData pd = EFLRegistrationController.getPageData();
            test.StopTest();
            
            System.assertEquals(2, pd.registrations.size(), 'There should be 2 registrations');
            for(EFLRegistrationController.Registration r : pd.registrations){
                system.assertNotEquals(null, r.currentAR);
                system.assertEquals(string.valueOf(activeFY), r.currentAR.EFLFiscal_Year__c);
                system.assertEquals(1, r.allReports.size());
            }
        }
    }
    
    @isTest
    public static void getCurrentUserTest(){
        User testUser = [SELECT Id, FirstName, LastName, Name FROM User WHERE IsActive = TRUE LIMIT 1];
        System.runAs(testUser){
            String name = EFLRegistrationController.getCurrentUser();
            System.assertEquals(name, testUser.Name, 'Name should match the running user');
        }
    }
    
    @isTest
    public static void saveAnnualReportNewTest(){
        Account account = new Account(name ='Test Community Account') ;
        insert account; 
        
        Contact contact = new Contact(LastName ='testCon',AccountId = account.Id);
        insert contact; 
        
        EFLRegistration__c testReg = new EFLRegistration__c();
        testReg.EFLAccount__c = contact.AccountId;
        testReg.EFLAnnual_Report_Created__c = false;
        testReg.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
        insert testReg;
        
        String annualReportId = EFLRegistrationController.saveNewAnnualReport(testReg.Id);
            
        System.assertNotEquals('', annualReportId);
		EFLAnnual_Report__c annualReport = [SELECT Id, EFLAccount__c, Status__c, EFLRegistration__c FROM EFLAnnual_Report__c WHERE Id = :annualReportId];
		System.assertEquals('Draft', annualReport.Status__c, 'Draft status expected');
		System.assertEquals(account.Id, annualReport.EFLAccount__c, 'Account should match the reg -> contact -> account Id');
		System.assertEquals(testReg.Id, annualReport.EFLRegistration__c, 'Registration should match the passed in reg Id');   
    }
    
/*    @isTest
    public static void saveAnnualReportExistingTest(){
        Account account = new Account(name ='Test Community Account') ;
        insert account; 
        
        Contact contact = new Contact(LastName ='testCon',AccountId = account.Id);
        insert contact; 
        
        EFLRegistration__c testReg = new EFLRegistration__c();
        testReg.EFLAccount__c = contact.AccountId;
        testReg.EFLAnnual_Report_Created__c = false;
        testReg.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
        insert testReg;
        
        EFLAnnual_report__c annualReport = new EFLAnnual_report__c(EFLAccount__c = account.Id, Status__c = 'Draft', EFLRegistration__c = testReg.Id);
        insert annualReport;
        
        String annualReportId = EFLRegistrationController.saveAnnualReport(testReg.Id);
            
        System.assertEquals(annualReport.Id, annualReportId);  
    }
    */
    @isTest
    public static void getARStatusTest(){
        Account account = new Account(name ='Test Community Account') ;
        insert account; 
        
        Contact contact = new Contact(LastName ='testCon',AccountId = account.Id);
        insert contact; 
        
        EFLRegistration__c testReg = new EFLRegistration__c();
        testReg.EFLAccount__c = contact.AccountId;
        testReg.EFLAnnual_Report_Created__c = false;
        testReg.EFLFiscal_Year__c = Integer.valueOf(System.Today().year());
        insert testReg;
        
        EFLAnnual_report__c annualReport = new EFLAnnual_report__c(EFLAccount__c = account.Id, Status__c = 'Draft', EFLRegistration__c = testReg.Id);
        insert annualReport;
        
        EFLAnnual_report__c arStatus = EFLRegistrationController.getARStatus(annualReport.Id);
            
        System.assertEquals(annualReport.Status__c, arStatus.Status__c);  
    }
    
    @isTest
    public static void getConTypeTest(){
        User communityUser = [SELECT Id, ContactId FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator' AND UserRoleId != null LIMIT 1];
        System.debug(communityUser);
        User user;
        Contact contact;
        System.runAs(communityUser){            
            Account account = new Account(name ='Test Community Account') ;
            insert account; 
           
            contact = new Contact(LastName ='testCon',AccountId = account.Id, EFL_Role__c = 'CEO');
            insert contact; 
            Profile profile = [SELECT Id FROM Profile WHERE Name = 'Report Preparer' LIMIT 1];
            UserRole role = [SELECT Id FROM UserRole LIMIT 1];
            
            user = new User(alias = 'test123', 
                                 email='test123@noemail.com',
                                 emailencodingkey='UTF-8', 
                                 lastname='Testing', 
                                 languagelocalekey='en_US',
                                 localesidkey='en_US', 
                                 profileid = profile.Id, 
                                 country='United States',
                                 IsActive =true,
                                 ContactId = contact.Id,
                                 timezonesidkey='America/Los_Angeles', 
                                 username='tester@noemail.com');       
            insert user;
        }
        
        System.runAs(user){
            test.StartTest();
            String contactType = EFLRegistrationController.getConType();
            test.StopTest();
            
            System.assertEquals(contact.EFL_Role__c, contactType);
        }
    }
}