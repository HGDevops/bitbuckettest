/**
* Author : Kishore Kumar
* Created Date : 4/13/2015
* Purpose : This is used to display Workflow task related action buttons
* Related Pages: CARPOL_UNI_WorkflowButtons
*/
public with sharing class CARPOL_UNI_WorkflowButtonsController{
/**
*  Properties
**/
	public Authorizations__c au;
	public Workflow_Task__c wf;
	public transient Boolean renderBRSApplicationNonCBI{get;set;}
	public transient Boolean renderBRSApplicationNoCBI{get;set;}
	public transient Boolean renderBRSApplicationCBI{get;set;}
	public transient Boolean renderPermitconditions{get;set;}
	public transient Boolean renderPermitPackage{get;set;}
	public transient Boolean renderGeneratePermit{get;set;}
	public transient Boolean renderPermit{get;set;}
	public transient Boolean renderCollaboration{get;set;}
	public transient string renderNEPA{get;set;}
	public transient string renderNoticeOffindings{get;set;}
	public transient string renderLetterofWarning{get;set;}
	public transient Boolean renderCoverLetter{get;set;}
	public transient Boolean renderEmail{get;set;}
	public transient string renderStatereview{get;set;}
	public string authid{get;set;}
	public string incidentid{get;set;}
	public string wfID{get;set;}
	public string Rtname{get;set;}
	public boolean showEditBtn{get;set;}
	public boolean bShowButton{get;set;}
	public List<Workflow_Task__c> listWT;
	public List<Attachment> listAtt;
	public string lineitemid{get;set;}
/**
* Standard Controller Constructor 
**/	
	public CARPOL_UNI_WorkflowButtonsController(ApexPages.StandardController Controller){
		wfID=ApexPages.currentPage().getParameters().get('id');
 		renderPermit=false;
		renderNEPA='False';
		renderNoticeOffindings='False';
		renderLetterofWarning='False';
 		renderCoverLetter=false;
		renderEmail=false;
		renderStatereview='False';
		renderPermitPackage=false;
		showEditBtn=true;
 		wf=[SELECT Id,Authorization__r.Authorization_Type__c,Incident_Number__c,Recordtype.Name,Authorization__c,buttons__c,Program__c,status__c,Authorization__r.Application_CBI__c FROM Workflow_Task__c WHERE Id=:wfId];
		incidentid=wf.Incident_Number__c;
		authid=wf.Authorization__c;
		if(authid!=null)lineitemid=[SELECT id FROM ac__c WHERE Authorization__c=:authid limit 1].id;
		Rtname=wf.Recordtype.Name;
/** 
* Set the render variables based on the action buttons that are configured for particular workflow task.
**/		
		if(wf.status__c=='Complete'||wf.status__c=='Deferred'||wf.status__c=='Waiting')showEditBtn=false;
		if((wf.Authorization__r.Authorization_Type__c=='Permit')&&(wf.buttons__c!=null)&&(wf.buttons__c.contains('Edit Permit'))){
			renderPermit=true;
			authid=wf.Authorization__c;
		}
		else authid=wf.Authorization__c;
		if((wf.Authorization__r.Authorization_Type__c=='Permit')&&(wf.buttons__c!=null)&&(wf.buttons__c.contains('Edit Permit Conditions'))){
			renderPermit=false;
			renderPermitconditions=true;
			authid=wf.Authorization__c;
		}
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('View Application'))){
			if(wf.Authorization__r.Application_CBI__c=='Yes'){
				renderBRSApplicationCBI=true;
				renderBRSApplicationNonCBI=true;
			}
			else if(wf.Authorization__r.Application_CBI__c=='No')renderBRSApplicationNoCBI=true;
		}
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Conditions Collaboration')))renderCollaboration=true;
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('NEPA Form')))renderNEPA='True';
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Send Letter of Warning')))renderLetterofWarning='True';
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Send Notice of findings')))renderNoticeOffindings='True';
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Edit Cover Letter')))renderCoverLetter=true;
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Create Permit Package')))renderPermitPackage=true;
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Generate Permit')))renderGeneratePermit=true;
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Send Email')))renderEmail=true;
		if((wf.buttons__c!=null)&&(wf.buttons__c.contains('Create State Review Records')))renderStatereview='True';
		bShowButton=false;
		listWT=[SELECT Id,Incident_Number__c,Incident_Number__r.Requester_email__c,Incident_Number__r.Requester_Name__c FROM Workflow_Task__c WHERE id=:apexpages.currentpage().getparameters().get('id') AND Name LIKE'%Management review of Noncompliance letter%'];
		if(listWT!=null&&listWT.size()>0){
			listAtt=[SELECT Id,IsDeleted,ParentId,Name,IsPrivate,ContentType,BodyLength,Body,OwnerId,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,SystemModstamp,Description FROM Attachment WHERE ParentId=:listWT[0].Incident_Number__c ORDER BY CreatedDate limit 1];
			if(listAtt!=null&&listAtt.size()>0)bShowButton=true;
		}
 	}
}