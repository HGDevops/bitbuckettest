public class EFLBarcodeGenerator_Code128 implements EFLIBarcodeGenerator {
    
    private map<integer,List<integer>> codeMapping =new map<integer,List<integer>>{
        32   => new List<integer>{2,1,2,2,2,2}, /* 00 */
33   => new List<integer>{2,2,2,1,2,2}, /* 01 */
34   => new List<integer>{2,2,2,2,2,1}, /* 02 */
35   => new List<integer>{1,2,1,2,2,3}, /* 03 */
36   => new List<integer>{1,2,1,3,2,2}, /* 04 */
37   => new List<integer>{1,3,1,2,2,2}, /* 05 */
38   => new List<integer>{1,2,2,2,1,3}, /* 06 */
39   => new List<integer>{1,2,2,3,1,2}, /* 07 */
40   => new List<integer>{1,3,2,2,1,2}, /* 08 */
41   => new List<integer>{2,2,1,2,1,3}, /* 09 */
42   => new List<integer>{2,2,1,3,1,2}, /* 10 */
43   => new List<integer>{2,3,1,2,1,2}, /* 11 */
44   => new List<integer>{1,1,2,2,3,2}, /* 12 */
45   => new List<integer>{1,2,2,1,3,2}, /* 13 */
46   => new List<integer>{1,2,2,2,3,1}, /* 14 */
47   => new List<integer>{1,1,3,2,2,2}, /* 15 */
48   => new List<integer>{1,2,3,1,2,2}, /* 16 */
49   => new List<integer>{1,2,3,2,2,1}, /* 17 */
50   => new List<integer>{2,2,3,2,1,1}, /* 18 */
51   => new List<integer>{2,2,1,1,3,2}, /* 19 */
52   => new List<integer>{2,2,1,2,3,1}, /* 20 */
53   => new List<integer>{2,1,3,2,1,2}, /* 21 */
54   => new List<integer>{2,2,3,1,1,2}, /* 22 */
55   => new List<integer>{3,1,2,1,3,1}, /* 23 */
56   => new List<integer>{3,1,1,2,2,2}, /* 24 */
57   => new List<integer>{3,2,1,1,2,2}, /* 25 */
58   => new List<integer>{3,2,1,2,2,1}, /* 26 */
59   => new List<integer>{3,1,2,2,1,2}, /* 27 */
60   => new List<integer>{3,2,2,1,1,2}, /* 28 */
61   => new List<integer>{3,2,2,2,1,1}, /* 29 */
62   => new List<integer>{2,1,2,1,2,3}, /* 30 */
63   => new List<integer>{2,1,2,3,2,1}, /* 31 */
64   => new List<integer>{2,3,2,1,2,1}, /* 32 */
65   => new List<integer>{1,1,1,3,2,3}, /* 33 */
66   => new List<integer>{1,3,1,1,2,3}, /* 34 */
67   => new List<integer>{1,3,1,3,2,1}, /* 35 */
68   => new List<integer>{1,1,2,3,1,3}, /* 36 */
69   => new List<integer>{1,3,2,1,1,3}, /* 37 */
70   => new List<integer>{1,3,2,3,1,1}, /* 38 */
71   => new List<integer>{2,1,1,3,1,3}, /* 39 */
72   => new List<integer>{2,3,1,1,1,3}, /* 40 */
73   => new List<integer>{2,3,1,3,1,1}, /* 41 */
74   => new List<integer>{1,1,2,1,3,3}, /* 42 */
75   => new List<integer>{1,1,2,3,3,1}, /* 43 */
76   => new List<integer>{1,3,2,1,3,1}, /* 44 */
77   => new List<integer>{1,1,3,1,2,3}, /* 45 */
78   => new List<integer>{1,1,3,3,2,1}, /* 46 */
79   => new List<integer>{1,3,3,1,2,1}, /* 47 */
80   => new List<integer>{3,1,3,1,2,1}, /* 48 */
81   => new List<integer>{2,1,1,3,3,1}, /* 49 */
82   => new List<integer>{2,3,1,1,3,1}, /* 50 */
83   => new List<integer>{2,1,3,1,1,3}, /* 51 */
84   => new List<integer>{2,1,3,3,1,1}, /* 52 */
85   => new List<integer>{2,1,3,1,3,1}, /* 53 */
86   => new List<integer>{3,1,1,1,2,3}, /* 54 */
87   => new List<integer>{3,1,1,3,2,1}, /* 55 */
88   => new List<integer>{3,3,1,1,2,1}, /* 56 */
89   => new List<integer>{3,1,2,1,1,3}, /* 57 */
90   => new List<integer>{3,1,2,3,1,1}, /* 58 */
91   => new List<integer>{3,3,2,1,1,1}, /* 59 */
92   => new List<integer>{3,1,4,1,1,1}, /* 60 */
93   => new List<integer>{2,2,1,4,1,1}, /* 61 */
94   => new List<integer>{4,3,1,1,1,1}, /* 62 */
95   => new List<integer>{1,1,1,2,2,4}, /* 63 */
96   => new List<integer>{1,1,1,4,2,2}, /* 64 */
97   => new List<integer>{1,2,1,1,2,4}, /* 65 */
98   => new List<integer>{1,2,1,4,2,1}, /* 66 */
99   => new List<integer>{1,4,1,1,2,2}, /* 67 */
100  => new List<integer>{1,4,1,2,2,1}, /* 68 */
101  => new List<integer>{1,1,2,2,1,4}, /* 69 */
102  => new List<integer>{1,1,2,4,1,2}, /* 70 */
103  => new List<integer>{1,2,2,1,1,4}, /* 71 */
104  => new List<integer>{1,2,2,4,1,1}, /* 72 */
105  => new List<integer>{1,4,2,1,1,2}, /* 73 */
106  => new List<integer>{1,4,2,2,1,1}, /* 74 */
107  => new List<integer>{2,4,1,2,1,1}, /* 75 */
108  => new List<integer>{2,2,1,1,1,4}, /* 76 */
109  => new List<integer>{4,1,3,1,1,1}, /* 77 */
110  => new List<integer>{2,4,1,1,1,2}, /* 78 */
111  => new List<integer>{1,3,4,1,1,1}, /* 79 */
112  => new List<integer>{1,1,1,2,4,2}, /* 80 */
113  => new List<integer>{1,2,1,1,4,2}, /* 81 */
114  => new List<integer>{1,2,1,2,4,1}, /* 82 */
115  => new List<integer>{1,1,4,2,1,2}, /* 83 */
116  => new List<integer>{1,2,4,1,1,2}, /* 84 */
117  => new List<integer>{1,2,4,2,1,1}, /* 85 */
118  => new List<integer>{4,1,1,2,1,2}, /* 86 */
119  => new List<integer>{4,2,1,1,1,2}, /* 87 */
120  => new List<integer>{4,2,1,2,1,1}, /* 88 */
121  => new List<integer>{2,1,2,1,4,1}, /* 89 */
122  => new List<integer>{2,1,4,1,2,1}, /* 90 */
123  => new List<integer>{4,1,2,1,2,1}, /* 91 */
124  => new List<integer>{1,1,1,1,4,3}, /* 92 */
125  => new List<integer>{1,1,1,3,4,1}, /* 93 */
126  => new List<integer>{1,3,1,1,4,1}, /* 94 */
127  => new List<integer>{1,1,4,1,1,3}, /* 95 */
128  => new List<integer>{1,1,4,3,1,1}, /* 96 */
129  => new List<integer>{4,1,1,1,1,3}, /* 97 */
130  => new List<integer>{4,1,1,3,1,1}, /* 98 */
131  => new List<integer>{1,1,3,1,4,1}, /* 99 */
132  => new List<integer>{1,1,4,1,3,1}, /* 100 */
133  => new List<integer>{3,1,1,1,4,1}, /* 101 */
134  => new List<integer>{4,1,1,1,3,1}, /* 102 */
135  => new List<integer>{2,1,1,4,1,2}, /* 103 START A */
136  => new List<integer>{2,1,1,2,1,4}, /* 104 START B */
137  => new List<integer>{2,1,1,2,3,2}, /* 105 START C */
138  => new List<integer>{2,3,3,1,1,1}, /* STOP */
139  => new List<integer>{2,0,0,0,0,0} /* END */

    };
    
    
    public boolean validateInput(EFLBarcodeData bcData)
    {
        boolean retVal = false;
        string validCharacters = '[ -~]+';
        
        pattern allowedPattern = pattern.compile(validCharacters);
        matcher ipMatcher = allowedPattern.matcher(bcData.inputData);
        if(ipMatcher.matches())
        {
            retVal = true;
            System.debug(bcData.inputData + '---------------Valid');
        }
        else
        {
            System.debug(bcData.inputData + '---------------Invalid');
        }
        
        return retVal;
    }
    
    public boolean encodeInput(EFLBarcodeData bcData)
    {
        integer barcodeStartID = 136;
        boolean retVal = true;
        if(String.isNotBlank(bcData.inputData))
        {
            Integer[] chars = bcData.inputData.getChars();
            Integer checksum = 104;
            bcData.encodedData.addAll(codeMapping.get(barcodeStartID));
            integer pos = 0;
            for(integer i: chars ){
                system.debug(' i: ' + i);
                if(i>=32 && i<=127){
                    system.debug('checksum: ' + checksum + ' pos:' + pos + 'val:' + (i-32) );
                    checksum += ((i-32)*(pos+1));
                    pos+=1;
                    bcData.encodedData.addAll(codeMapping.get(i));
                }
                else { system.debug('errored i: ' + i);  return false;   }
                
            }
             system.debug('checksum: ' + checksum);
            
                checksum = math.mod(checksum,103);
                system.debug('checksum/103: ' + checksum);
                if(codeMapping.get(checksum+32) != null)
                bcData.encodedData.addAll(codeMapping.get(checksum+32));
                bcData.encodedData.addAll(codeMapping.get(138));
                bcData.encodedData.addAll(codeMapping.get(139));
        }
        
        return retVal;
    }
    
    public boolean generateBarcodeHTML(EFLBarcodeData bcData)
    {
        boolean retVal = true;
        
        if(bcData.encodedData.size()>0)
        {
            
            bcData.maxHeight = 1;
            bcData.maxWidth = 19; //156
            
            //bcData.bars.add(new BarcodeDigit(width=2,height=1,positionVertical=0,drawBar=true,drawSpacing=false));
            
            //List<integer> temp1 = new List<integer>{2,1,1,2,1,4,2,3,1,1,1,3,1,1,2,2,1,4,2,2,1,1,1,4,2,2,1,1,1,4,1,3,4,1,1,1,2,1,2,2,2,2,3,1,1,3,2,1,1,3,4,1,1,1,1,2,1,2,4,1,2,2,1,1,1,4,1,4,1,2,2,1,1,1,2,3,3,1,2,3,3,1,1,1,2,0,0,0,0,0};
                
                Boolean flag = true;
            
            for(integer tmp: bcData.encodedData)
            {
                EFLBarcodeDigit bcDigit = new EFLBarcodeDigit();
                bcDigit.width=tmp;
                if(flag == true)
                {
                    bcDigit.height=1;
                    bcDigit.positionVertical=0;
                    bcDigit.drawBar=true;
                    bcDigit.drawSpacing=false;
                    flag=false; 
                }
                else
                {
                    bcDigit.height=1;
                    bcDigit.positionVertical=0;
                    bcDigit.drawBar=false;
                    bcDigit.drawSpacing=true;
                    flag=true; 
                }
                bcData.bars.add(bcDigit);
            }
            
            decimal widthFactor = 1;
            decimal totalHeight = 30; 
            string color = 'black';
            
            bcData.barcodeHTML = '<div style=\"font-size:0;position:relative;width:' + ( bcData.maxWidth * widthFactor) + 'px;height:' + totalHeight + 'px;\">' + 'n';
            decimal positionHorizontal = 0;
            for ( EFLBarcodeDigit bar : bcData.bars) {
                decimal barWidth = (bar.width * widthFactor).setScale(3);
                decimal barHeight = (bar.height * totalHeight / bcData.maxHeight).setScale(3);
                if (bar.drawBar) {
                    decimal positionVertical = (bar.positionVertical * totalHeight / bcData.maxHeight).setScale(3);
                    // draw a vertical bar
                    bcData.barcodeHTML += '<div style="background-color:' + color + ';width:' + barWidth + 'px;height:' + barHeight + 'px;position:absolute;left:' + positionHorizontal + 'px;top:' + positionVertical + 'px;"> </div>' + 'n';
                }
                positionHorizontal += barWidth;
            }
            bcData.barcodeHTML += '</div>';
            
        }
        return retVal;
    }

}