@isTest
public class EFLSelectAnimalsController_Test{

    @testSetup
    public static void createData() {
		EFLRegistration__c registration = new EFLRegistration__c();
        Database.insert(registration);
        
		EFLAnnual_Report__c annualReport = new EFLAnnual_Report__c();
		annualReport.EFLRegistration__c = registration.Id;
		Database.insert(annualReport);
        
        EFLAnimal__c otherAnimal = new EFLAnimal__c(Name = 'Bush dog', Species_Name__c = 'Speothos venaticus', Other_Animal__c = true, Search_Preference__c = 2);
        EFLAnimal__c dogs = new EFLAnimal__c(Name = 'Dogs', Species_Name__c = 'Dogs', Other_Animal__c = false, Search_Preference__c = 1);
        insert new List<EFLAnimal__c> {dogs, otherAnimal};
    }
    
    @isTest
    public static void testWithoutRegisteredAnimals_AnimalsAdded(){
        EFLAnnual_Report__c annualReport = [Select Id From EFLAnnual_Report__c Limit 1];
        
        List<String> selectedAnimals = new List<String>();
        selectedAnimals.add('Dogs');
        selectedAnimals.add('Other Farm Animals');
        
        EFLSelectAnimalsController.insertStdAnimals(annualReport.Id,selectedAnimals);
        
        List<EFLRegistered_Animal__c> regAnimals = [Select Id, Name, EFLSelectedForReport__c, EFLAnimal__c
                                                    , EFLOtherAnimal__c, EFLAnimalName__c
                                                    , EFLColumnBHeldNotUsed__c, EFLColumnCUsedPainMinimized__c
                                                    , EFLColumnDUsedPainMinimized__c, ELFColumnEPainNotMinimized__c
                                                    From EFLRegistered_Animal__c];
        
        System.assertEquals(9, regAnimals.size());
        for(EFLRegistered_Animal__c regAnimal :regAnimals) {
            System.debug('regAnimal ==>>' + regAnimal);
            System.debug('regAnimal.Name ==>>' + regAnimal.Name);
            System.debug('regAnimal.EFLSelectedForReport__c ==>>' + regAnimal.EFLSelectedForReport__c);
            System.debug('regAnimal.EFLAnimal__c ==>>' + regAnimal.EFLAnimal__c);
            System.debug('regAnimal.EFLOtherAnimal__c ==>>' + regAnimal.EFLOtherAnimal__c);

            if (regAnimal.EFLAnimal__c == 'Dogs') {
                System.assertEquals(true, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(false, regAnimal.EFLOtherAnimal__c);
            }
            else if (regAnimal.EFLAnimal__c == 'Other Farm Animals') {
                System.assertEquals(false, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(true, regAnimal.EFLOtherAnimal__c);
            }
            else if (regAnimal.EFLAnimal__c == 'Other Animals') {
                System.assertEquals(false, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(true, regAnimal.EFLOtherAnimal__c);
            }
            else {
                System.assertEquals(false, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(false, regAnimal.EFLOtherAnimal__c);
            }

            System.assertEquals(null, regAnimal.EFLColumnBHeldNotUsed__c);
            System.assertEquals(null, regAnimal.EFLColumnCUsedPainMinimized__c);
            System.assertEquals(null, regAnimal.EFLColumnDUsedPainMinimized__c);
            System.assertEquals(null, regAnimal.ELFColumnEPainNotMinimized__c);
        }
    }

    @isTest
    public static void testWithRegisteredOtherAnimals_AnimalsAdded(){
        EFLAnnual_Report__c annualReport = [Select Id From EFLAnnual_Report__c Limit 1];
        EFLRegistration__c registration = [Select Id From EFLRegistration__c Limit 1];
            
        // register some animals to the report
        List<EFLRegistered_Animal__c> regAnimals = new List<EFLRegistered_Animal__c>();
        List<EFLAnimal__c> animals = [Select Id, Name, Other_Animal__c From EFLAnimal__c];
        for(EFLAnimal__c animal :animals) {
            EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
            regAnimal.EFLAnimal__c = animal.Name;
            regAnimal.EFLAnimalName__c = animal.Id;
            regAnimal.EFLRegistration__c = registration.Id;
            regAnimal.EFLAnnual_Report__c = annualReport.Id;
            regAnimal.EFLOtherAnimal__c = animal.Other_Animal__c;
            regAnimal.EFLSelectedForReport__c = true;
            regAnimals.add(regAnimal);
        }
		insert regAnimals;
        
        List<String> selectedAnimals = new List<String>();
        selectedAnimals.add('Dogs');
        selectedAnimals.add('Other Animals');
        
        EFLSelectAnimalsController.insertStdAnimals(annualReport.Id,selectedAnimals);
        
        regAnimals = [Select Id, Name, EFLSelectedForReport__c, EFLAnimal__c
        			  , EFLOtherAnimal__c, EFLAnimalName__c
                      , EFLColumnBHeldNotUsed__c, EFLColumnCUsedPainMinimized__c
                      , EFLColumnDUsedPainMinimized__c, ELFColumnEPainNotMinimized__c
                      From EFLRegistered_Animal__c];
        
        System.assertEquals(2, regAnimals.size());

        for(EFLRegistered_Animal__c regAnimal :regAnimals) {
            System.debug('regAnimal ==>>' + regAnimal);
            System.debug('regAnimal.Name ==>>' + regAnimal.Name);
            System.debug('regAnimal.EFLSelectedForReport__c ==>>' + regAnimal.EFLSelectedForReport__c);
            System.debug('regAnimal.EFLAnimal__c ==>>' + regAnimal.EFLAnimal__c);
            System.debug('regAnimal.EFLOtherAnimal__c ==>>' + regAnimal.EFLOtherAnimal__c);

            if (regAnimal.EFLAnimal__c == 'Dogs') {
                System.assertEquals(true, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(false, regAnimal.EFLOtherAnimal__c);
            }
            else {
                System.assertEquals(true, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(true, regAnimal.EFLOtherAnimal__c);
            }

            System.assertEquals(null, regAnimal.EFLColumnBHeldNotUsed__c);
            System.assertEquals(null, regAnimal.EFLColumnCUsedPainMinimized__c);
            System.assertEquals(null, regAnimal.EFLColumnDUsedPainMinimized__c);
            System.assertEquals(null, regAnimal.ELFColumnEPainNotMinimized__c);
        }
    }
    
    @isTest
    public static void testWithRegisteredAnimals_AnimalsAdded(){
        EFLAnnual_Report__c annualReport = [Select Id From EFLAnnual_Report__c Limit 1];
        EFLRegistration__c registration = [Select Id From EFLRegistration__c Limit 1];
            
        // register some animals to the report
        List<EFLRegistered_Animal__c> regAnimals = new List<EFLRegistered_Animal__c>();
        List<EFLAnimal__c> animals = [Select Id, Name, Other_Animal__c From EFLAnimal__c];
        for(EFLAnimal__c animal :animals) {
            EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
            regAnimal.EFLAnimal__c = animal.Name;
            regAnimal.EFLAnimalName__c = animal.Id;
            regAnimal.EFLRegistration__c = registration.Id;
            regAnimal.EFLAnnual_Report__c = annualReport.Id;
            regAnimal.EFLOtherAnimal__c = animal.Other_Animal__c;
            regAnimal.EFLSelectedForReport__c = true;
            regAnimals.add(regAnimal);
        }
		insert regAnimals;
        
        List<String> selectedAnimals = new List<String>();
        selectedAnimals.add('Dogs');
        selectedAnimals.add('Other Farm Animals');
        
        EFLSelectAnimalsController.insertStdAnimals(annualReport.Id,selectedAnimals);
        
        regAnimals = [Select Id, Name, EFLSelectedForReport__c, EFLAnimal__c
        			  , EFLOtherAnimal__c, EFLAnimalName__c
                      , EFLColumnBHeldNotUsed__c, EFLColumnCUsedPainMinimized__c
                      , EFLColumnDUsedPainMinimized__c, ELFColumnEPainNotMinimized__c
                      From EFLRegistered_Animal__c];
        
        System.assertEquals(1, regAnimals.size());

        for(EFLRegistered_Animal__c regAnimal :regAnimals) {
            System.debug('regAnimal ==>>' + regAnimal);
            System.debug('regAnimal.Name ==>>' + regAnimal.Name);
            System.debug('regAnimal.EFLSelectedForReport__c ==>>' + regAnimal.EFLSelectedForReport__c);
            System.debug('regAnimal.EFLAnimal__c ==>>' + regAnimal.EFLAnimal__c);
            System.debug('regAnimal.EFLOtherAnimal__c ==>>' + regAnimal.EFLOtherAnimal__c);

			System.assertEquals('Dogs', regAnimal.EFLAnimal__c);
            System.assertEquals(true, regAnimal.EFLSelectedForReport__c);
            System.assertEquals(false, regAnimal.EFLOtherAnimal__c);

            System.assertEquals(null, regAnimal.EFLColumnBHeldNotUsed__c);
            System.assertEquals(null, regAnimal.EFLColumnCUsedPainMinimized__c);
            System.assertEquals(null, regAnimal.EFLColumnDUsedPainMinimized__c);
            System.assertEquals(null, regAnimal.ELFColumnEPainNotMinimized__c);
        }
    }    

    @isTest
    public static void testWithDifferentAnimal_AnimalNotAdded(){
        EFLAnnual_Report__c annualReport = [Select Id From EFLAnnual_Report__c Limit 1];
        EFLRegistration__c registration = [Select Id From EFLRegistration__c Limit 1];
            
        // register some animals to the report
        List<EFLRegistered_Animal__c> regAnimals = new List<EFLRegistered_Animal__c>();
        List<EFLAnimal__c> animals = [Select Id, Name, Other_Animal__c From EFLAnimal__c];
        for(EFLAnimal__c animal :animals) {
            EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
            regAnimal.EFLAnimal__c = animal.Name;
            regAnimal.EFLAnimalName__c = animal.Id;
            regAnimal.EFLRegistration__c = registration.Id;
            regAnimal.EFLAnnual_Report__c = annualReport.Id;
            regAnimal.EFLOtherAnimal__c = animal.Other_Animal__c;
            regAnimal.EFLSelectedForReport__c = true;
            regAnimals.add(regAnimal);
        }
		insert regAnimals;
        
        List<String> selectedAnimals = new List<String>();
        selectedAnimals.add('Ardwolf');
        
        List<String> statusMessages = EFLSelectAnimalsController.insertStdAnimals(annualReport.Id,selectedAnimals);
        for(string message :statusMessages) {
            System.debug('message ==>>' + message);
        }
        
        regAnimals = [Select Id, Name, EFLSelectedForReport__c, EFLAnimal__c
        			  , EFLOtherAnimal__c, EFLAnimalName__c
                      , EFLColumnBHeldNotUsed__c, EFLColumnCUsedPainMinimized__c
                      , EFLColumnDUsedPainMinimized__c, ELFColumnEPainNotMinimized__c
                      From EFLRegistered_Animal__c];

        System.assertEquals(1, regAnimals.size());

        /*
        for(EFLRegistered_Animal__c regAnimal :regAnimals) {
            System.debug('regAnimal ==>>' + regAnimal);
            System.debug('regAnimal.Name ==>>' + regAnimal.Name);
            System.debug('regAnimal.EFLSelectedForReport__c ==>>' + regAnimal.EFLSelectedForReport__c);
            System.debug('regAnimal.EFLAnimal__c ==>>' + regAnimal.EFLAnimal__c);
            System.debug('regAnimal.EFLOtherAnimal__c ==>>' + regAnimal.EFLOtherAnimal__c);

            if (regAnimal.EFLAnimal__c == 'Dogs') {
                System.assertEquals(true, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(false, regAnimal.EFLOtherAnimal__c);
            }
            else {
                System.assertEquals(true, regAnimal.EFLSelectedForReport__c);
                System.assertEquals(true, regAnimal.EFLOtherAnimal__c);
            }

            System.assertEquals(0, regAnimal.EFLColumnBHeldNotUsed__c);
            System.assertEquals(0, regAnimal.EFLColumnCUsedPainMinimized__c);
            System.assertEquals(0, regAnimal.EFLColumnDUsedPainMinimized__c);
            System.assertEquals(0, regAnimal.ELFColumnEPainNotMinimized__c);
        }*/
    }

    @isTest
    public static void lookForExistingAnimals_AnimalsReturned(){
        EFLAnnual_Report__c annualReport = [Select Id From EFLAnnual_Report__c Limit 1];
        EFLRegistration__c registration = [Select Id From EFLRegistration__c Limit 1];
            
        // register some animals to the report
        List<EFLRegistered_Animal__c> regAnimals = new List<EFLRegistered_Animal__c>();
        List<EFLAnimal__c> animals = [Select Id, Name, Other_Animal__c From EFLAnimal__c];
        for(EFLAnimal__c animal :animals) {
            EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
            regAnimal.EFLAnimal__c = animal.Name == 'Dogs' ? animal.Name : null; 
            regAnimal.EFLAnimalName__c = animal.Name == 'Dogs' ? animal.Id : null;
            regAnimal.EFLRegistration__c = registration.Id;
            regAnimal.EFLAnnual_Report__c = annualReport.Id;
            regAnimal.EFLOtherAnimal__c = animal.Other_Animal__c;
            regAnimal.EFLSelectedForReport__c = true;
            regAnimals.add(regAnimal);
        }
        
		EFLRegistered_Animal__c regAnimal = new EFLRegistered_Animal__c();
        regAnimal.EFLAnimal__c = 'Some Animal';
        regAnimal.EFLAnimalName__c = null;
        regAnimal.EFLRegistration__c = registration.Id;
        regAnimal.EFLAnnual_Report__c = annualReport.Id;
        regAnimal.EFLOtherAnimal__c = true;
        regAnimal.EFLSelectedForReport__c = false;
        regAnimals.add(regAnimal);

        insert regAnimals;
        
        EFLSelectAnimalsController.existingAnimalWrapper existingAnimals = EFLSelectAnimalsController.lookForExistingAnimals(annualReport.Id);
        
        System.assertEquals(2, existingAnimals.selectedAnimals.size());
        System.assertEquals(3, existingAnimals.totalAnimals);
        System.assert(existingAnimals.selectedAnimals.contains('Dogs') == true);
        System.assert(existingAnimals.selectedAnimals.contains('Bush dog') == false);
        System.assert(existingAnimals.selectedAnimals.contains('') == true);
    }
}