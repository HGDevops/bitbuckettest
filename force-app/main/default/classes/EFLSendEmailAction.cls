/*-----------------------------------------------------------------------  
@Purpose: This class defines the invocable method to fire apex emails
----------------------------------------------------------------------*/
public inherited sharing class EFLSendEmailAction {
    
    //EFLSendEmailAction - Processbuilder Invocable Method for emailing
    @InvocableMethod(label='EFLSendEmailAction' description='Email action.')
    public static void sendEmail(list<emailActionAttributes> emailActionAttributesList) {
        try
        {
            List<String> toAddresses = new List<String>();
            //emailActionAttributes class is where the InvocableVariable are set
            for(emailActionAttributes eaa : emailActionAttributesList){
                
                //Set toAddresses 
                toAddresses = getRecipients(eaa.CMDLabel, eaa.objectID);
                if(!toAddresses.isempty())
                {
                    EFLEmailUtility.toAddresses = toAddresses;
                    
                    //pull in template ID based on template Unique name
                    EFLEmailUtility.TemplateId = EFLEmailUtility.getEmailTemplateId(eaa.emailTemplateDeveloperName);  
                    
                    //Workaround: setting a contact from the org
                    EFLEmailUtility.targetObjectId = EFLContactRepository.contactIdForsettargetObjectId();
                    
                    //Context Object record Id for Template merge fields
                    EFLEmailUtility.whatId = eaa.objectID; 
                    EFLEmailUtility.sendEmail(); 
                }
            }
        }
        catch(exception ex)
        {
            EFLErrorLog.createErrorLog('EFLSendEmailAction.sendEmail()',ex);
            throw ex;
        }
    }
    
    /*
*@purpose:  Define invocable method attributes
*/    
    public with sharing class emailActionAttributes {
        @InvocableVariable(Label = 'emailTemplateUniqueName')
        public String emailTemplateDeveloperName; 
        
        @InvocableVariable(Label = 'RecordId')
        public ID objectID; 
        
        @InvocableVariable(Label = 'CMDLabel')
        public String CMDLabel;         
    }   
    
    /*
*@purpose:  Returns recipient email addresses
*/    
    private static List<String> getRecipients(string CMDLabel, ID parentId){
        List<String> recipientAddresses = new List<String>();
        
        //BUILD logic to get recipient email addresses based on the CMDLabel
        EFLRecipientEmail__mdt recipientSetup = new EFLRecipientEmail__mdt();
        recipientSetup = EFLEmailUtility.getRecipientSetup(CMDLabel);
        
        //Getting recipient team and setting the recipient list
        List<string> objFieldNames = new List<string>();
        objFieldNames = recipientSetup.EmailField__c.split('\\.');
        string objAPIName = objFieldNames[0];
        string recipientFieldAPIName = objFieldNames[1];
        string parentFieldAPIName = recipientSetup.ParentFieldApiName__c;
        List<sobject> sobjectList = new List<sobject>();
        
        //Forming Query
        string query = 'Select Id, ' + recipientFieldAPIName + ' From ' + objAPIName;
        query = query + ' Where ' +parentFieldAPIName + ' = :parentId';
        List<string> roles = new List<string>();
        roles = recipientSetup.Roles__c.split(',');
        query = query + ' AND Member_Role__c In :roles';
        
        //Adding recipients
        for(sObject sObj : database.query(query)) 
        {
            recipientAddresses.add((String)sObj.get(recipientFieldAPIName));
        }
        return recipientAddresses;
    }
    
}