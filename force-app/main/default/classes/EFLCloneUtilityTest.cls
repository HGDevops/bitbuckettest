@isTest
public class EFLCloneUtilityTest {
     public static Authorizations__c auth;
    public static Application__c app;
    public static  AC__c lineItem;
    public static  Link_Regulated_Articles__c lra;
     public static  Construct_Application_Junction__c caj;
     public static  Location__c loc;
     public static  Construct_Application_Junction__c sop;
     public static  Additional_Contact_Detail__c addCon;
     public static  Applicant_Attachments__c appAtt;
    public static  list<Applicant_Attachments__c> appAttachList;
     public static  Applicant_Attachments__c appAttach;
     public static  list<sobject> sobjectList;
    static testmethod void testdata(){
       CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
         testData.insertcustomsettings();
         app = testData.newapplication();
        lineItem = testData.newLineItemBRS('test',app);
        auth = testData.newAuth(app.id);
        lra = testData.newlinkRegArticle(lineItem.id,app.id,auth.id);
        appAttach = testData.newAttach(lineItem.id);
        appAttachList = new list<Applicant_Attachments__c>();
        appAttachList.add(appAttach);
        sobjectList = (list<sobject>)appAttachList;
        appAtt = testData.newAttach(lineItem.id);
        Country__c UScountry=testdata.newcountryus();
        Level_1_Region__c level1reg=testdata.newlevel1region(UScountry.id);
        Level_2_Region__c level2reg=testdata.newlevel2region(level1reg.id);
         string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        location__c orgloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,lineItem.id,olocrectypeid);
        Applicant_Contact__c appcon = testdata.newappcontact();
    
    Additional_Contact_Detail__c addCon = new Additional_Contact_Detail__c();
    addCon.Last_Name__c = appcon.id;
    addCon.Line_Item__c = lineItem.id;
    insert addCon;
         Construct_Application_Junction__c objcaj1 = new Construct_Application_Junction__c();
        objcaj1.Line_Item__c = lineItem.id;
        objcaj1.Application__c = app.id;
        insert objcaj1;

    }
    static testmethod void cloneLineItemTestMethod(){
        system.test.starttest();
        testdata();
        // TO-DO: TESTFIX
        //EFLCloneUtility.cloneLineItem(lineItem,app,'Ext11111');
        //EFLCloneUtility.cloneLinkRegulatedArticle(lineItem,lineItem);
        //EFLCloneUtility.cloneConstruct(lineItem,lineItem);
        EFLCloneUtility.cloneLocation(lineItem,lineItem);
        EFLCloneUtility.cloneSOP(lineItem,lineItem);
        EFLCloneUtility.cloneAdditionalAssociatedContacts(lineItem,lineItem);
        EFLCloneUtility.cloneApplicantAttachments(lineItem,lineItem);
        EFLCloneUtility.cloneAttachments(sobjectList);
        system.test.stoptest();
    }
}