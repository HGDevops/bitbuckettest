@isTest
public class  EFLGetExplanationDetails_Test {
    
    @isTest
    public static void  EFLGetExplanationDetails_Testmethod(){
      Account testAcc = new Account();
      testAcc.Name = 'Test Account';
      insert testAcc;
      
      Contact testCon = new COntact();
      testCon.LastName = 'Test Contact';
      testCon.AccountID = testAcc.Id;
      insert testCon;

      EFLRegistration__c testReg = new EFLRegistration__c();
      testReg.EFLAccount__c = testAcc.id;
      //testReg.EFLContact__c = testCon.id;
      //testReg.EFLStatus__c = 'Active';
      insert(testReg);
      
      EFLAnnual_Report__c testAnnlRep = new EFLAnnual_Report__c ();
      testAnnlRep .EFLAccount__c = testAcc.id;
      testAnnlRep .EFLContact__c = testCon.id;
      testAnnlRep .EFLRegistration__c = testReg.id;
      insert(testAnnlRep );
      
      EFLRegistered_Animal__c testReganimal = new EFLRegistered_Animal__c ();
      testReganimal.EFLAccount__c = testAcc.id;
      //testanimalReg .EFLContact__c = testCon.id;
      testReganimal.EFLAnnual_Report__c= testAnnlRep.id;
      testReganimal.EFLAnimal__c = 'Test';
     /* testReganimal.EFLColumnBHeldNotUsed__c = '2';
      testReganimal.EFLColumnCUsedPainMinimized__c = '3';
      testReganimal.EFLColumnDUsedPainMinimized__c = '2';
      testReganimal.ELFColumnEPainNotMinimized__c = '1'; */
      insert(testReganimal );
      
      test.StartTest();
        EFLGetExplanationDetails.getAnimals(testReganimal.id);
      test.StopTest();
    }
}