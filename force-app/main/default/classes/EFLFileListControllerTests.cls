@isTest
public class EFLFileListControllerTests {

    private static final string AccountName = 'Test Account';
    private static final string AccountDescription = 'Test Account Description';
    private static final string ContactLastName = 'Test';
    private static final string filterLogic = 'Test Usage Key';
    
    @TestSetup
    static void makeData(){
        // Make a parent object for Uploaded_File__c to be created under in test execution.
        Account account = new Account();
        account.Name = AccountName;
        account.Description = AccountDescription;
        insert account;
        
        Contact contact = new Contact();
        contact.LastName = ContactLastName;
        contact.AccountId = account.Id;
        insert contact;

        Uploaded_File__c uf = new Uploaded_File__c();
        uf.Name = 'Test';
        uf.ParentId__c = contact.Id;
        uf.Animal_Type__c = '';
        uf.Description__c = '';
        uf.External_File_Id__c = 'TestExtFileId';
        uf.File_Category__c = 'Exception';
        uf.File_Preview_Id__c = '';
        uf.File_Type__c = '';
        uf.Full_File_Name__c = '';
        uf.Integration_Provider__c = 'SpringCM';
        insert uf;

        uf = new Uploaded_File__c();
        uf.Name = 'Test 2';
        uf.ParentId__c = contact.Id;
        uf.Animal_Type__c = '';
        uf.Description__c = '';
        uf.External_File_Id__c = 'TestExtFileId';
        uf.File_Category__c = 'Exception';
        uf.File_Preview_Id__c = '';
        uf.File_Type__c = '';
        uf.Full_File_Name__c = '';
        uf.Integration_Provider__c = 'SpringCM';
        insert uf;
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'TestDocument',
            PathOnClient = 'TestDocument.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [Select Id, Title, LatestPublishedVersionId From ContentDocument];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = account.Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;        
    }
    
    @isTest
    static void testGetPageDataWithNameFilter_OneFileReturned(){
        Uploaded_File__c uf = [Select Id, Name, ParentId__c From Uploaded_File__c Limit 1];
        
        EFLFileListController.PageData pd = EFLFileListController.getPageData(new List<Id>{uf.ParentId__c}, filterLogic);
        System.assertEquals(1, pd.files.size());
        System.assertEquals(uf.Id, pd.files[0].Id);
        System.assert(pd.columnHeaders.contains('First Name'));
    }

    @isTest
    static void testGetPageDataWithMultipleParentIds_BothFilesReturned(){
        Account account = [Select Id From Account Limit 1];
        Contact contact = [Select Id From Contact Limit 1];
        
        EFLFileListController.PageData pd = EFLFileListController.getPageData(new List<Id>{contact.Id}, filterLogic);
        System.assertEquals(1, pd.files.size());
    }

    @isTest
    static void testSetupNewFile_FileSetup(){
        Account account = [Select Id, Name From Account Limit 1];
        ContentDocument document = [Select Id, Title, FileType From ContentDocument Limit 1];
        
        EFLFileListController.LightningFileUpload lfu = new EFLFileListController.LightningFileUpload();
        lfu.name = 'testSetupNewFile';
        lfu.documentId = document.Id;
        List<EFLFileListController.LightningFileUpload> lfuList = new List<EFLFileListController.LightningFileUpload>{lfu};
        String cd = JSON.serialize(lfuList);
        
        EFLFileListController.setupNewFile(JSON.serialize(lfuList), account.Id, filterLogic);

        List<Uploaded_File__c> ufs = [Select Id, Name, ParentId__c, File_Preview_Id__c, File_Type__c, File_Category__c From Uploaded_File__c Where Name = :document.Title];

        System.assertEquals(1, ufs.size());
        System.assertEquals(document.Title, ufs[0].Name);
        System.assertEquals('Other', ufs[0].File_Category__c);
        System.assertEquals(account.Id, ufs[0].ParentId__c);
        System.assertEquals(document.Id, ufs[0].File_Preview_Id__c);
        System.assertEquals(document.FileType, ufs[0].File_Type__c);
    }

    @isTest
    static void testDeleteFile_FileDeleted(){
        Uploaded_File__c uf = [Select Id From Uploaded_File__c Limit 1];
        
        EFLFileListController.deleteFile(uf.Id, true);

        List<Uploaded_File__c> ufs = [Select Id From Uploaded_File__c Where IsDeleted = True All Rows];
        System.assertEquals(1, ufs.size());
        System.assertEquals(uf.Id, ufs[0].Id);
    }
    
    @isTest
    static void testDeleteFile_FileDeleted_NoPermanentDelete(){
        Uploaded_File__c uf = [Select Id From Uploaded_File__c Limit 1];
        
        EFLFileListController.deleteFile(uf.Id, false);
        
        Uploaded_File__c ufRes = [Select Id, Is_Deleted__c From Uploaded_File__c WHERE Id = :uf.Id];
        
        List<Uploaded_File__c> allFiles = [Select Id, Is_Deleted__c From Uploaded_File__c Where IsDeleted = True All Rows];
        
        System.assertEquals(0, allFiles.size());
        System.assertEquals(true, ufRes.Is_Deleted__c);
    }

    @isTest
    static void testCompareMethodOfFieldOutput_ItemsSorted(){
        List<EFLFileListController.FieldOutput> foList = new List<EFLFileListController.FieldOutput>();
        for (Integer i = 3; i > 0; i--) {
            EFLFileListController.FieldOutput fo = new EFLFileListController.FieldOutput('Test Coverage', i, i);
            fo.dataType = 'Integer';
            foList.add(fo);
        }
        foList.add(new EFLFileListController.FieldOutput('Test Coverage', 3, 3));
        foList.sort();
        
        System.assertEquals(3, foList[0].order);
        System.assertEquals(3, foList[1].order);
        System.assertEquals(2, foList[2].order);
        System.assertEquals(1, foList[3].order);
    }

    @isTest
    static void testCompareMethodOfUploadedFile_ItemsSorted(){
        List<EFLFileListController.UploadedFile> ufList = new List<EFLFileListController.UploadedFile>();
        
        ContentDocument cd1 = [Select Id, Owner.Name, Title, Description, FileExtension, CreatedDate From ContentDocument Limit 1];
        EFLFileListController.UploadedFile uf1 = new EFLFileListController.UploadedFile(cd1);
        uf1.iconName = 'Test Coverage';
        ufList.add(uf1);
        ufList.add(uf1.clone());

        ContentVersion cv3 = new ContentVersion(
            Title = 'TestDocument 3',
            PathOnClient = 'TestDocument.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert cv3;    
        ContentDocument cd3 = [Select Id, Owner.Name, Title, Description, FileExtension, CreatedDate From ContentDocument Where Title = :cv3.Title Limit 1];
        EFLFileListController.UploadedFile uf3 = new EFLFileListController.UploadedFile(cd3);
        ufList.add(uf3);

        ContentVersion cv4 = new ContentVersion(
            Title = 'TestDocument 4',
            PathOnClient = 'TestDocument.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert cv4;    
        ContentDocument cd4 = [Select Id, Owner.Name, Title, Description, FileExtension, CreatedDate From ContentDocument Where Title = :cv4.Title Limit 1];
        EFLFileListController.UploadedFile uf4 = new EFLFileListController.UploadedFile(cd4);
        ufList.add(uf4);

        ufList.sort();
        
        // Can't assert the first two elements becuase Created dates are too close and the sort order could switch
        //System.assertEquals(uf4.id, ufList[0].id);
        //System.assertEquals(uf3.id, ufList[1].id);
        //System.assertEquals(uf1.id, ufList[2].id);
        //System.assertEquals(uf1.id, ufList[3].id);
    }
    
    @isTest
    static void createWrapperClasses_TestCoverageGoesUp(){
        EFLFileListController.filterMapping fm = new EFLFileListController.filterMapping();
        fm.fieldName = 'Name';
        fm.condition = '=';
        fm.fieldValue = 'Test Coverage';
        
        EFLFileListController.fileDataMapping fdm = new EFLFileListController.fileDataMapping();
        fdm.uploadedFileField = 'Name';
        fdm.uploadedFileValue = 'Test Coverage';
    }
}