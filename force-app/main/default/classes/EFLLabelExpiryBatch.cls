public class EFLLabelExpiryBatch implements Database.Batchable<sObject>{
    private string voided;
    private string active;
    public EFLLabelExpiryBatch(){
        voided = EFLGlobalConstants.getConstantValue('LABEL_STATUS_VOIDED');
        active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, Authorization__c 
                                         FROM Label__c 
                                         WHERE Authorization__r.BRS_Introduction_Type__c = 'Import' AND Status__c = :active AND Authorization__r.Expiration_Date__c < TODAY]);
    }

    public void execute(Database.BatchableContext bc, list<Label__c> labels){
        //system.debug('labels found: ' + labels);
        for(Label__c lab : labels){
            lab.Status__c = voided;
        }
        
        update labels;
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}