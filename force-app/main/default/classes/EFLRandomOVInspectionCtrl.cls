public with sharing class EFLRandomOVInspectionCtrl {
    
    public String authnum {get;set;}
    public String RandomInspection {get;set;}
    Inspection__c RandomInsp;
    public EFLRandomOVInspectionCtrl(ApexPages.StandardController controller) {
       
    }
    
    
    public void GenerateRandomOVInspection() {
    
        /*Selecting authorization at Random based on Prefix -----------> Start*/
        List<Authorizations__c> OVAuthList = [SELECT Name FROM Authorizations__c WHERE Prefix__c='611'];
        Decimal d = math.random() *OVAuthList.size();
        Integer i=d.intValue(); //Picked the Integer value at random
        authnum = OVAuthList[i].name; // picked the random authorization
        RandomInsp = new Inspection__c(); //creating the inspection with the random authorization chosen
        RandomInsp.Authorization__c = OVAuthList[i].id;
        RandomInsp.RecordTypeID  = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        insert RandomInsp;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Successfully created a Random Inspection'));
        RandomInspection = RandomInsp.APHIS_Inspection_ID__c;
        
    }
    
    public pageReference goToInspection(){
        PageReference InspectionDetailsRedirect = new PageReference('/' + RandomInsp.Id);
        InspectionDetailsRedirect.setRedirect(true);
        return InspectionDetailsRedirect;
        
    }

}