/*------
Name : CARPOL_UNI_MasterTransLoc_test.cls
Description : Test class for CARPOL_UNI_MasterTransLoc.trigger and CARPOL_UNI_MasterTransLoc_helper.cls
Author : Vijay Vellaturi
crated date : 06-Dec-16
-----*/
@isTest
public class CARPOL_UNI_MasterTransLoc_test{
static testmethod void testinsertRelatedDM(){ 
        string rtId = [select id from recordtype where name = 'Transit Locales'].id;
        Country__c countryUSA = new Country__c(Name='United States of America', Country_Code__c = '01');
        insert countryUSA;
        Level_1_Region__c L1Region = new Level_1_Region__c(Country__c=countryUSA.Id, name='Texas (TX)');
        insert L1Region;
        Level_1_Region__c L1Region2 = new Level_1_Region__c(Country__c=countryUSA.Id, name='Tennessee (TN)');
        insert L1Region2;        
          Test.startTest();    
          Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
          User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];         
          system.runAs(u){
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Column_1_API_Name__c = 'Scientific_Name__r.Name';
          plip.Column_2_API_Name__c = 'Country_Of_Origin__r.Name';
          plip.Column_3_API_Name__c = 'Component__r.Name';
          plip.Column_4_API_Name__c = 'Where_will_the_RA_be_grown__c';
          plip.Column_5_API_Name__c = 'Intended_Use__r.Name';
          plip.Column_6_API_Name__c = 'Total_number_of_shipments__c';
          update plip;
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          level_1_region__c objlevel1MD = testData.newlevel1regionMD();
          level_1_region__c objlevel1AL = testData.newlevel1regionAL();
          Country__c objUSctry = testData.newcountryus();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          objdm.State_of_Port_of_Entry__c = L1Region.id;
          objdm.State_of_Port_of_Exit__c = L1Region2.id;
          insert objdm;
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Level_1_Region__c FacState = testdata.newlevel1regionAL();
          Facility__c objFac = testData.newfacility('Domestic Port');          
          objFac.country__c = countryUSA.Id;          
          objFac.State_LV1__c = L1Region2.ID;
          update objFac;
          Facility__c objFac1 = testData.newfacility('Domestic Port');        
          objFac1.country__c = countryUSA.Id;
          objFac1.State_LV1__c = L1Region.Id;                    
          update objFac1;         
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          Transit_Locale__c objTL = new Transit_Locale__c (Line_Item__c= li.id,Port_of_Entry__c = objFac1.id, Port_of_Exit__c = objFac.id, Date_of_Departure__c=date.valueof('2018-06-06'), EFLArrival_Date__c=date.valueof('2018-06-07') );          
          objTL.recordtypeid = rtId; 
          insert objTL;
          Transit_Locale__c objTL1 = new Transit_Locale__c (Line_Item__c= li.id,Port_of_Entry__c = objFac.id, Port_of_Exit__c = objFac1.id, Date_of_Departure__c=date.valueof('2018-06-06'), EFLArrival_Date__c=date.valueof('2018-06-07') );          
          objTL1.recordtypeid = rtId; 
          insert objTL1;
          set<id> tlids = new set<id>();
          tlids.add(objTL.id);
          CARPOL_UNI_MasterTransLoc_helper.insertRelatedDM(tlids);
          //delete objTL;
          CARPOL_UNI_MasterTransLoc_helper.deleteRLD(tlids);
          CARPOL_UNI_MasterTransLoc_helper.prevAddlstopsEdit(tlids);          
          Test.stopTest();   
        }
       }
}