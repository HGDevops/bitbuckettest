@isTest(seealldata=true)
private class CreateAccountFromContactTest{
    static testMethod void testCreateAccountFromContact() {
      string AccountRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APHIS_Efile_Standard_Account').getRecordTypeId();
      Account objacct=new  Account();
        objacct.Name='Global Account';
        objacct.RecordTypeId=AccountRecordTypeId;
        Insert objacct;
        
   // String ContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Owner').getRecordTypeId();
     String ContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('APHIS Efile Standard Contact').getRecordTypeId();
      Contact objcont = new Contact();
      objcont.FirstName = 'FirstName';
      objcont.LastName = 'LastName';
      objcont.Email = 'test@email.com';
      objcont.MailingStreet = 'Mailing Street';
      objcont.MailingCity = 'Mailing City';
      objcont.MailingState = 'Ohio';
      objcont.MailingCountry = 'United States';
      objcont.MailingPostalCode = '43002';
      objcont.RecordTypeId = ContRecordTypeId;
      objcont.AccountId = objacct.id;
      insert objcont;
      system.assert(objcont != null);                                   
    }
   }