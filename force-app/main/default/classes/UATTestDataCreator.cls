public class UATTestDataCreator {
    
    public static void InsertTestData(Integer startNumber, Integer endNumber, String emailPrefix, string emailSuffix, string environment){
        //AC_AR1@accenturefederal.com
        List<Account> accountsToInsert = new List<Account>();
        for (Integer i = startNumber; i <= endNumber; i++){
            Account newAccount = new Account();
            newAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AC_R_L_Account').getRecordTypeId();
            newAccount.Name = 'Test AC_AR Account ' + i;
            newAccount.EFLAddress_Line1__c = i + ' Test St';
            newAccount.EFLCity__c = 'Baltimore';
            newAccount.EFLCountry__c = [SELECT Id FROM Country__c WHERE Name = 'United States of America' LIMIT 1].Id;
            newAccount.EFLState__c = [SELECT Id FROM Level_1_Region__c WHERE Name = 'Maryland (MD)' Limit 1].Id;
            newAccount.EFLZip_Code__c = '22223';
            newAccount.Phone = '444-444-4444';
            newAccount.ACIS_Accounts_External_ID__c= 100000000+i;
            accountsToInsert.add(newAccount);
        }
        insert accountsToInsert;
        
        //contact fields: account, first name, last name, role (Institutional Official), email address
        List<Contact> contactsToInsert = new List<Contact>();
        integer contactCount = startNumber;
        for (Account account : accountsToInsert){
            Contact newContact = new Contact();
            newContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('AC_R_L_Contact').getRecordTypeId();
            newContact.FirstName = 'AC_AR' + contactCount;
            newContact.LastName = 'Test';
            newContact.AccountId = account.Id;
            //newContact.EFL_Role__c = 'Institutional Official';
            newContact.Email = emailPrefix + contactCount + emailSuffix;
            contactsToInsert.add(newContact);
            newContact.EFLJIT_Contact__c=true;           
            contactCount++;
        }
        insert contactsToInsert;        
        
        //registration fields APHIS Registration Number (99-T-9950 start), Active = Y, Account
        List<EFLRegistration__c> registrationsToInsert = new List<EFLRegistration__c>();
        integer registrationCount = startNumber;        
        for (Account account : accountsToInsert){
            EFLRegistration__c newRegistration = new EFLRegistration__c();
            newRegistration.EFLAccount__c = account.Id;
            newRegistration.EFLActive__c = 'Y';
            newRegistration.EFLRegistrationNumber__c = '99-T-99';
            if (registrationCount < 10){
                newRegistration.EFLRegistrationNumber__c = newRegistration.EFLRegistrationNumber__c  + 0 + registrationCount;
            }else {
                newRegistration.EFLRegistrationNumber__c = newRegistration.EFLRegistrationNumber__c + registrationCount;                
            }
            registrationsToInsert.add(newRegistration);
            registrationCount++;
        }
        insert registrationsToInsert;
        
        List<User> usersToInsert = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Report Preparer'];        
        for (Contact contact : contactsToInsert){
            User user = new  User();
            user.Alias = (contact.FirstName + contact.lastName).left(8).trim();
            user.Email = contact.Email;
            user.EmailEncodingKey='UTF-8';
            user.LastName = contact.LastName;
            user.FirstName = contact.FirstName;
            user.LanguageLocaleKey = 'en_US'; 
            user.LocaleSidKey='en_US'; 
            user.ProfileId = p.Id;
            user.TimeZoneSidKey='America/Los_Angeles';
            user.Username = contact.Email + environment;
            user.ContactId = contact.Id;  
            user.SpringCMEos__SpringCM_User__c = true; 
            user.SpringCMEos__Portal_Only__c = true; 
            user.SpringCMEos__SpringCM_Role__c = 'Guest';              
            usersToInsert.add(user);
        }
        insert usersToInsert;
        for(User u : usersToInsert){
            system.setPassword(u.Id, '1LoveLightning!');
        } 
    }
}