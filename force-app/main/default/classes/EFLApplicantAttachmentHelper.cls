/**
 Purpose: Helper class for Applicant Attachment object 
**/
public with sharing class EFLApplicantAttachmentHelper {
    
    static ID SOPRecordtypeID = EFLGenericUtility.getRecordTypeId('Applicant Attachment SOP');
    static private string DOT = '.';
    
   // Sync CBI and CBI-Deleted File Names with respective Attachment Title Names 
    public static void syncFileNames(List <Applicant_Attachments__c> appAttachNewList,Map <id,Applicant_Attachments__c> appAttachOldMap) {
       list<ID> applicantAttachmentIDs = new list<ID>();
 
        for(Applicant_Attachments__c app : appattachNewList){
            // Check for SOP Record Type
            if(app.RecordTypeId == SOPRecordtypeID){
                applicantAttachmentIDs.add(app.Id);
            }
        }
        if(applicantAttachmentIDs!=NULL){
                List<Applicant_Attachments__c> updatedApplicantAttachments = [SELECT Id,File_Name__c,
                                                                                      CBI_Deleted_File_Name__c,
                                                                                      (Select Id, Name 
                                                                                         from attachments)  
                                                                                 FROM Applicant_Attachments__c 
                                                                                 WHERE Id in :applicantAttachmentIDs];
                List<attachment> relatedAttachmentsToUpdate = new List<attachment>();
                for (Applicant_Attachments__c app : updatedApplicantAttachments){
                     string attachmentName;
                     string oldCBIFileName = appAttachOldMap.get(app.ID).File_Name__c;
                     string oldCBIDELETEDFileName = appAttachOldMap.get(app.ID).CBI_Deleted_File_Name__c;
                    // Loop through each Related attachment records
                    for(attachment att : app.attachments){ 
                        if(att.Name.contains(DOT)){
                           attachmentName = att.Name.substringBeforeLast(DOT);
                        }else{
                           attachmentName = att.Name.substringBeforeLast(DOT);  
                        }
                        if(attachmentName == oldCBIFileName){
                             att.Name = app.File_Name__c + DOT + att.Name.substringAfterLast(DOT);
                         }else if(attachmentName == oldCBIDELETEDFileName){
                             att.Name = app.CBI_Deleted_File_Name__c + DOT + att.Name.substringAfterLast(DOT); 
                         }
                        relatedAttachmentsToUpdate.add(att);
                    }
                }
                if(relatedAttachmentsToUpdate!=NULL){
                   update relatedAttachmentsToUpdate;
                }
        }  
        
     }

}