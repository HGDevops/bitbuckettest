@isTest(seealldata=false)
private class Portal_Account_Edit_Controller_Test {
      @IsTest static void Portal_Account_Edit_Controller_Test() {

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();


          User usershare = new User();
          usershare = EFLUserTestDataFactory.getUser('eFile Applicant');   
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.Portal_Account_Edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objacct);
              Portal_Account_Edit_Controller extclassempty = new Portal_Account_Edit_Controller(sc);
              ApexPages.currentPage().getParameters().put('id',objacct.id);                    
              Portal_Account_Edit_Controller extclass = new Portal_Account_Edit_Controller(sc);
              extclass.bAddress = '';
              extclass.getEditPage();              
              extclass.Save();  
              System.assert(extclass != null);        
          Test.stopTest();   
      }
}