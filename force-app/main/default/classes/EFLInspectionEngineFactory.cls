public class EFLInspectionEngineFactory {
   /*
    * Purpose: Get Inspection Engine
    */ 
    public static EFLIInspectionEngine getEngine(Inspection__c inspectionRecord){
       
        EFLIInspectionEngine Engine; 
        Engine = getInspectionEngine(inspectionRecord);
         
        if(Engine==null){
            throw new EFLInspectionEngineException('No Inspection Engine found for Inspection: ' + inspectionRecord.Name);
            
        }
        
        return Engine;
      }  
   
   /*
    * Purpose: Get Inspection Engine based on program
    * Note: Enable the commented code once other Program Engines are implemented
    */     
    private static EFLIInspectionEngine getInspectionEngine(Inspection__c inspectionRecord){
         if (inspectionRecord.Program__c == 'BRS')
		{
			  return new EFLBRSInspectionEngine(); 
		}/*else if (inspectionRecord.Program__c == 'AC')
		{ 
			//return new EFLACInspectionEngine();
 
		}else if (inspectionRecord.Program__c == 'VS') 
		{
			//return new EFLVSInspectionEngine();
 
		}else if (inspectionRecord.Program__c == 'PPQ')
		{
			//return new EFLPPQInspectionEngine();
		}*/
         else
          return new EFLBRSInspectionEngine();

    }  
    
    public class EFLInspectionEngineException extends Exception {}    

}