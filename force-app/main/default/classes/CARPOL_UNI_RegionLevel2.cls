public with sharing class CARPOL_UNI_RegionLevel2 {
    public static map<string,string> allAnswersMap = new map<string,string>();
    public static List<SelectOption> RegionLvl2(string sSelReg1){
        if(sSelReg1 != null && sSelReg1 != ''){
            List<SelectOption> options = new List<SelectOption>();
            for(Level_2_Region__c recLR: [Select Name from Level_2_Region__c where Level_1_Region__c=:sSelReg1 and Level_2_Region_Status__c =: 'Active' Order By Name ]){
                options.add(new SelectOption(recLR.Id,recLR.Name));
                allAnswersMap.put(recLR.Id,recLR.Name);              
            }
            system.debug('<<<<<<<<<< COUNTY OPTION >>>>>>>>>>>>> '+ options);
            options.add(0,new SelectOption('','--Select--'));
            return options;            
        }
        else
            return null;
    }

}