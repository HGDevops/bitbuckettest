public with sharing class CARPOL_UNI_ExtWiz_DecisionMatrix {
    
    public Set<ID> regID = new Set<ID>();
    
    public static final string THUMBPRTINT_PARAM = 'thumbprintId';
    
    public id thumbprintId {
        get{
            if(ApexPages.currentPage().getParameters().get(THUMBPRTINT_PARAM)!=null)
            {
                id thumbprintId = ApexPages.currentPage().getParameters().get(THUMBPRTINT_PARAM);
                return thumbprintId;
            }
            return null;
        }
        set;
    }    
    
    public string instruction {
        get
        {
         instruction = 'Note: This is not a complete list of conditions and the import may be subject to more restrictive conditions.';
         return instruction;
        }
        set;
    }    
    
    public CARPOL_UNI_ExtWiz_DecisionMatrix(ApexPages.StandardController controller){
        if(thumbprintId != null)
        {
            getConditions();
        }
    }
    
    public List<Regulation__c> getrlist(){
    return [SELECT ID, Name, testinglong__c, isActive__c, Short_Name__c, Custom_Name__c, Title__c, Type__c, Regulation_Description__c FROM Regulation__c WHERE ID IN: regID];
    }
    
    public void getConditions(){
        //Program Pathway filter to show condtions - currently only filtered to show condotion for Dogs. In future, other pathways can be added
        if(thumbprintId != null)
        {
            List<Signature_Regulation_Junction__c> signRegs = new List<Signature_Regulation_Junction__c>();
            signRegs = EFLPSQUtility.regulationsByThumbprintId(thumbprintId);
            if(!signRegs.isEmpty())
            {
                for(Signature_Regulation_Junction__c srj : signRegs)
                {
                    regID.add(srj.Regulation__c);
                }                               
            }
        }        
    }    
    
}