@isTest
public class EFLApplicationTriggerHandlerTests {

    @TestSetup
    static void makeData(){
        // make an application with auth, line items
        CARPOL_BRS_TestDataManager mgr = new CARPOL_BRS_TestDataManager();
        mgr.insertcustomsettings();
        
        Application__c app = mgr.newapplication();
        
        Authorizations__c auth = mgr.newAuth(app.Id);
        Authorizations__c auth2 = mgr.newAuth(app.Id);
        
        // make a 2nd app with sharing account having a value.
        Account newAcct = mgr.newAccount(mgr.AccountRecordTypeId);
        Application__c app2 = mgr.newapplication();
        
        Authorizations__c auth3 = mgr.newAuth(app2.Id);
        Authorizations__c auth4 = mgr.newAuth(app2.Id);
        
        app2.Sharing_Account__c = newAcct.Id;
        update app2;
        
        // make line items
        mgr.newLineItemBRS('Personal Use', app);
    }
    
    /* This test changes the Sharing_Account__c field on Application__c object.
     * The result should be all related objects under the EFLAccountSharingSetting__mdt should also be updated 
     * to the new account.
     */
    @isTest
    static void testChangeSharingAccountOnApplication_Auths_LineItems(){
        system.debug([SELECT Id FROM Account]);
        list<Application__c> apps = [SELECT Id, Sharing_Account__c FROM Application__c WHERE Sharing_Account__c = NULL];
        system.assertEquals(1, apps.size());
        system.assertEquals(null, apps[0].Sharing_Account__c);
        
        CARPOL_BRS_TestDataManager mgr = new CARPOL_BRS_TestDataManager();
        Account newAcct = mgr.newAccount(mgr.AccountRecordTypeId);
        
        test.startTest();
        //checkRecursive.run = true;
        apps[0].Sharing_Account__c = newAcct.Id;
        update apps;
        test.stopTest();
        
        list<Authorizations__c> auths = [SELECT Id, Sharing_Account__c FROM Authorizations__c WHERE Application__c = :apps[0].Id];
        system.assertEquals(2, auths.size());
        
        // verify the Auths were updated.
        for(Authorizations__c auth: auths){
            system.assertEquals(newAcct.Id, auth.Sharing_Account__c);
        }
        
        list<AC__c> lineItems = [SELECT Id, Sharing_Account__c FROM AC__c WHERE Application_Number__c = :apps[0].Id];
        system.assertEquals(1, lineItems.size());
        
        // verify the Line Items were updated.
        for(AC__c li: lineItems){
            system.assertEquals(newAcct.Id, li.Sharing_Account__c);
        }
    }
    
    @isTest
    static void testChangeSharingAccountOnApplication_SetToNull(){
        system.debug([SELECT Id FROM Account]);
        list<Application__c> apps = [SELECT Id, Sharing_Account__c FROM Application__c WHERE Sharing_Account__c != NULL];
        system.assertEquals(1, apps.size());
        system.assertNotEquals(null, apps[0].Sharing_Account__c);
        
        test.startTest();
        // set acct to null.
        apps[0].Sharing_Account__c = null;
        update apps;
        test.stopTest();
        
        list<Authorizations__c> auths = [SELECT Id, Sharing_Account__c FROM Authorizations__c WHERE Application__c = :apps[0].Id];
        system.assertEquals(2, auths.size());
        
        // verify the Auths were updated.
        for(Authorizations__c auth: auths){
            system.assertEquals(null, auth.Sharing_Account__c);
        }
    }
}