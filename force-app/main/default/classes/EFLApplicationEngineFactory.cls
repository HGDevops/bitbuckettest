public class EFLApplicationEngineFactory {

 
   /*
    * Purpose: Get Application Engine
    */ 
    public static EFLIApplicationEngine getEngine(EFLPreScreeningWrapper psw){
       
        EFLIApplicationEngine Engine;
        string program = psw.QuestionList[psw.QuestionList.size()-1].psquestion.Signature__r.REF_Program_Name__c;
        Engine = getApplicationEngine(program);

        if(Engine==null){
            throw new EFLIApplicationEngineException('No Application Engine found for Application: ' );
        }
        
        return Engine;
      }  
   
    public static EFLIApplicationEngine getApplicationEngineFromProgramName(string program){
        return getApplicationEngine(program);
    }
   /*
    * Purpose: Get Application Engine based on program
    */     
    private static EFLIApplicationEngine getApplicationEngine(string program){
        
        if (program == 'BRS')
		{
		     return new EFLBRSApplicationEngine();
		}else if (program == 'AC')
		{ 
			return new EFLACApplicationEngine(); 
		}else if (program == 'VS')
		{
			//return new EFLVSApplicationEngine();
		}else if (program == 'PPQ')
		{
			//return new EFLPPQApplicationEngine();
		}
        return null;

    }  
    
    public class EFLIApplicationEngineException extends Exception {}    
   
}