/*
 * Dinesh - Added exception handler logging try-catch block
 */
public with sharing class CARPOL_VS_AphisAccount_extension {
    
    
    public String aphisAcctNum {get;set;}
    public String accountStatus;
    
    
    private ApexPages.StandardController appController;
    
    public ID applicationID;
    public string totalAmount;
    
    public CARPOL_VS_AphisAccount_extension (ApexPages.StandardController controller) 
    {
        
        applicationID = ApexPages.CurrentPage().getparameters().get('id');
        totalAmount = ApexPages.CurrentPage().getparameters().get('amount');
        
    }
    
    public PageReference submitAphisAccount()
    {
        try{ // try-catch block to log the exception
            if(Test.isRunningTest() != true){
                HTTPResponse res = callValdiateAccount();
                accountStatus = parseXML(res);
            }
            
            if (accountStatus == 'Valid'){
                
                PageReference ref = new PageReference('/apex/CARPOL_VS_ConfirmAPHISAccountPayment?id=' + applicationID + '&total=' + totalAmount + '&acctNum=' + aphisAcctNum);
                ref.setRedirect(true);
                return ref;
            }
            else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There is a problem with your APHIS Credit Account. Please call 1-877-777-2128 or e-mail ABSHelpline@aphis.usda.gov for further assistance. You may also opt to select an alternate payment method by clicking the Cancel button to return to the payment page.' );
                ApexPages.addMessage(myMsg);
                return null;
                
            }
        }catch(Exception ae){
            MessageServices__c objMS = new MessageServices__c();
            objMS.Object__c = 'CARPOL_VS_AphisAccount Validation';
            objMS.Original_Message__c = ae.getMessage();
            objMS.Message_Content__c = ae.getStackTraceString();
            insert objMS;
            return null;
        }
    }
    
    public HTTPResponse callValdiateAccount(){
        CARPOL_URLs__c validateURL = CARPOL_URLs__c.getValues('ValidateUFSAccount'); 
        
        String s='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aph="aphis.vs.ufs"><env:Header /><env:Body><aph:ValidateAccount><aph:accountNumber>' + aphisAcctNum + '</aph:accountNumber></aph:ValidateAccount></env:Body></env:Envelope>';
        //system.debug('Here is request: '+ s);
        HttpRequest req = new HttpRequest();
        Blob headerValue = Blob.valueOf('carpol_user:Sun5fl0wer');
		String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
		req.setHeader('Authorization', authorizationHeader);
        //req.setEndpoint('https://vslabsubroutes.aphis.usda.gov/services/ValidateUFSAccount.ValidateUFSAccountHttpSoap11Endpoint'); -- Commented by Dinesh
        req.setEndpoint(validateURL.URL__c); //Added by Dinesh
        req.setMethod('POST');
        req.setBody(s);
        req.setHeader('Content-Type', 'text/xml');
        req.setHeader('SOAPAction', '""');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        //System.debug('Here is your response:'+res.getBody());
        
        return res;
        
    }
    
    public PageReference returnToPaymentPage()
    {
        PageReference ref = new PageReference('/apex/CARPOL_VS_ApplicationFee_Payment?id=' + applicationID);
        ref.setRedirect(true);
        return ref;
    }
    
    public string parseXML(HTTPResponse res) {
        
        XmlStreamReader reader = res.getXmlStreamReader();
        
        // Read through the XML 
        string status = '';
        //this parsing XML is stupid and there must be a better way to do it
        //but for now this reads through the entire response object
        //whatever the final approach is, move it to a separate method that can be used by any call
        while(reader.hasNext()) {
            //looks for a start tag <TAGNAME>
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
                //checks to see if that start tag has the local name of <customerProfileId>
                if (reader.getLocalName() == 'payload') {
                    //if it does then you have to iterate through the characters until you get to the next end tag
                    while(reader.hasNext()) {
                        if (reader.getEventType() == XmlTag.END_ELEMENT) {
                            break;
                        } else if (reader.getEventType() == XmlTag.CHARACTERS) {
                            status = reader.getText();
                        }
                        reader.next();
                    }
                    //System.debug('status= ' + status);
                }
                
            }
            reader.next();
        }
        return status;
        
    }
    
}