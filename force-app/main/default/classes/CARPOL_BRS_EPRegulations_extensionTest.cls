@isTest(SeeAllData=true)
private class CARPOL_BRS_EPRegulations_extensionTest {
    @isTest
    static void testCARPOL_BRS_EPRegulations_extension() {
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont = 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Release',objapp);      
        AC__c ac3 = testData.newLineItem('Import',objapp);
        Regulation__c objreg1 = testData.newRegulation('Standard','');
        Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
        Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Attachment attach = testData.newattachment(ac.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        Authorizations__c authRecord = [SELECT Thumbprint__r.Program_Prefix__c FROM Authorizations__c WHERE Id = :objauth.Id LIMIT 1];
        
        Program_Prefix__c pp = new Program_Prefix__c();
        pp.Id = authRecord.Thumbprint__r.Program_Prefix__c;
        pp.Name = '101';
        update pp;
        
        ac.Authorization__c = objauth.Id;
        ac.Does_This_Application_Contain_CBI__c = 'Yes';
        update ac;
        
        Label__c lbl0 = new Label__c();
        lbl0.Name='testLbl';
        lbl0.Authorization__c = objauth.Id;
        lbl0.Status__c = 'Active';
        insert lbl0;
        
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        Workflow_Task__c objWF = testData.newworkflowtask('Test', objauth, 'Complete');
        String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
        String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        objreg4.RecordTypeid = ReguRecordTypeId;
        update objreg4;
        Domain__c objprog = testData.newProgram('BRS');
        Program_Prefix__c objPrefix = new Program_Prefix__c();
        objPrefix.Program__c = objprog.Id;
        objPrefix.Name = '101';
        objPrefix.Permit_PDF_Template__c = 'CARPOL_BRS_StandardPermit';
        insert objPrefix;
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test BRS KK';
        objTP.Recordtypeid = BRSTPRecordTypeId ;
        objTP.Program_Prefix__c = objPrefix.Id;
        insert objTP; 
        /*
        Domain__c objprog1 = testData.newProgram('BRS');
        Program_Prefix__c objPrefix1 = new Program_Prefix__c();
        objPrefix1.Program__c = objprog.Id;
        objPrefix1.Name = '102';
        objPrefix1.Permit_PDF_Template__c = 'CARPOL_BRS_CourtesyPermit';
        insert objPrefix1;
        Signature__c objTP1 = new Signature__c();
        objTP1.Name = 'Test BRS KK';
        objTP1.Recordtypeid = BRSTPRecordTypeId ;
        objTP1.Program_Prefix__c = objPrefix1.Id;
        insert objTP1; 
        */
        Applicant_Attachments__c objAtt = testData.newAttach(ac.Id);
        Applicant_Attachments__c objAtt1 = testData.newAttach(ac.Id);
        Applicant_Attachments__c objAtt2 = testData.newAttach(ac.Id);
        
        Test.startTest();
        PageReference pageRef = Page.CARPOL_BRS_EPRegulations;
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        ApexPages.currentPage().getParameters().put('wfid',objWF.id);
        ApexPages.currentPage().getParameters().put('PermitPackage','True');
        Test.setCurrentPage(pageRef);
        
        CARPOL_BRS_EPRegulations_extension acepreg = new CARPOL_BRS_EPRegulations_extension(sc);
        acepreg.renderPermitPackagelabel = true;
        acepreg.CBI = 'No';
        acepreg.regulationGroup = new Group__c();
        acepreg.selectedDocuments = new List<String>();
        acepreg.authorization = new Authorizations__c();
        
        acepreg.regulation = objreg1;
        acepreg.renderPermitPackagelabel = true;
        acepreg.getResults();
        acepreg.setRegulationType();
        acepreg.getAddByOptions();
        acepreg.selectInput();
        acepreg.getAddByOptions();
        acepreg.updateSignature();
        
        acepreg.createNewRegulation();
        acepreg.BRSCollaboration();
        acepreg.attachPDF();
        acepreg.GetDocLauncherURLs();
        acepreg.viewDraftPDF();
        acepreg.save();
        List<string> docTypes = new list<String> { 'PDF' , 'DOC'};
            string sDocType = acepreg.flattenSelectedDocuments(docTypes);
        acepreg.deleteSupplimentalRecords();
        acepreg.deleteRecord();
        //acepreg.redirect();
        acepreg.cancel();
        acepreg.attachcbiPDF();
        acepreg.attachcbideletedPDF();
        acepreg.SaveIssuedDt();
        //acepreg.viewCBIdeletedPdf();
        //acepreg.viewDraftPDF();
        acepreg.renderCourtesyPermit = false;
        acepreg.PDFVersion= '';
        
        objPrefix.Permit_PDF_Template__c = '';
        update objPrefix;
        objauth.Thumbprint__c = objTP.id;
        objauth.Recordtypeid = authRecordTypeId ;
        objauth.Authorization_Type__c = 'Permit';
        update objauth;
        Facility__c  entry = testData.newfacility('Domestic Port');
        Authorization_Junction__c objauthjun = new Authorization_Junction__c();
        objauthjun.Authorization__c = objauth.id;
        objauthjun.Port__c = entry.id;
        insert objauthjun;
        Label__c lbl = new Label__c();
        lbl.Name='testLbl';
        lbl.Authorization__c = objauth.Id;
        lbl.Status__c = 'Active';
        insert lbl;
        
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        ApexPages.currentPage().getParameters().put('wfid',objWF.id);
        ApexPages.currentPage().getParameters().put('PermitPackage','True');
        CARPOL_BRS_EPRegulations_extension acepreg2 = new CARPOL_BRS_EPRegulations_extension(sc2);
        //acepreg2.viewDraftPDF();
        acepreg2.attachPDF();
        //objPrefix1.Permit_PDF_Template__c = 'CARPOL_BRS_CourtesyPermit';
        //update objPrefix1;
        acepreg2.viewDraftPDF();
        //acepreg2.attachcbiPDF();
        //acepreg2.attachcbideletedPDF();
        acepreg2.SaveIssuedDt();
        //acepreg2.viewCBIdeletedPdf();
        //acepreg2.renderCourtesyPermit = false;
        acepreg2.PDFVersion= '';
        // string sDocType = acepreg.flattenSelectedDocuments();
        //objauth.Thumbprint__c = objTP1.id;
        objauth.Recordtypeid = authRecordTypeId ;
        objauth.Authorization_Type__c = 'Permit';
        update objauth;
        System.assert(acepreg != null);
        Test.stopTest();
    }
    
    @isTest
    static void getCodeCoverage(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Application__c app = testData.newApplication();
        Authorizations__c auth = testData.newAuth(app.Id);
        Authorizations__c authRecord = [SELECT Thumbprint__r.Program_Prefix__c FROM Authorizations__c WHERE Id = :auth.Id LIMIT 1];
        
        Program_Prefix__c pp = new Program_Prefix__c();
        pp.Id = authRecord.Thumbprint__r.Program_Prefix__c;
        pp.Name = '101';
        update pp;
        
        AC__c ac = testData.newLineItem('Release',app);
        
        ac.Authorization__c = auth.Id;
        ac.Does_This_Application_Contain_CBI__c = 'Yes';
        update ac;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(auth);
        test.startTest();
        CARPOL_BRS_EPRegulations_extension ext = new CARPOL_BRS_EPRegulations_extension(sc);
        ext.setRegulationType();
        test.stopTest();
    }
    
    

    //-----------------------------------------------------------------------
    @istest    
    static void testCARPOL_BRS_EPRegulations_extension1() {
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont = 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Release',objapp);      
        AC__c ac3 = testData.newLineItem('Import',objapp);
        Regulation__c objreg1 = testData.newRegulation('Standard','');
        Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
        Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Attachment attach = testData.newattachment(ac.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        Authorizations__c authRecord = [SELECT Thumbprint__r.Program_Prefix__c FROM Authorizations__c WHERE Id = :objauth.Id LIMIT 1];
        
        Program_Prefix__c pp = new Program_Prefix__c();
        pp.Id = authRecord.Thumbprint__r.Program_Prefix__c;
        pp.Name = '101';
        update pp;
        
        ac.Authorization__c = objauth.Id;
        ac.Does_This_Application_Contain_CBI__c = 'Yes';
        update ac;
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        Workflow_Task__c objWF = testData.newworkflowtask('Test', objauth, 'Complete');
        String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
        String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        objreg4.RecordTypeid = ReguRecordTypeId;
        update objreg4;
                Domain__c objprog1 = testData.newProgram('BRS');
        Program_Prefix__c objPrefix1 = new Program_Prefix__c();
        objPrefix1.Name = '102';
        objPrefix1.Permit_PDF_Template__c = 'CARPOL_BRS_CourtesyPermit';
        objPrefix1.Program__c = objprog1.Id;
        insert objPrefix1;
        Signature__c objTP1 = new Signature__c();
        objTP1.Name = 'Test BRS KK';
        objTP1.Recordtypeid = BRSTPRecordTypeId ;
        objTP1.Program_Prefix__c = objPrefix1.Id;
        insert objTP1; 
        Applicant_Attachments__c objAtt = testData.newAttach(ac.Id);
        Applicant_Attachments__c objAtt1 = testData.newAttach(ac.Id);
        Applicant_Attachments__c objAtt2 = testData.newAttach(ac.Id);
        
        Test.startTest();
        PageReference pageRef = Page.CARPOL_BRS_EPRegulations;
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        ApexPages.currentPage().getParameters().put('wfid',objWF.id);
        ApexPages.currentPage().getParameters().put('PermitPackage','True');
        Test.setCurrentPage(pageRef);
        
        CARPOL_BRS_EPRegulations_extension acepreg = new CARPOL_BRS_EPRegulations_extension(sc);
        acepreg.renderPermitPackagelabel = true;
        acepreg.regulationGroup = new Group__c();
        acepreg.selectedDocuments = new List<String>();
        acepreg.authorization = new Authorizations__c();
        
        acepreg.regulation = objreg1;
        acepreg.renderPermitPackagelabel = true;
        acepreg.getResults();
        acepreg.setRegulationType();
        acepreg.getAddByOptions();
        acepreg.selectInput();
        acepreg.getAddByOptions();
        acepreg.updateSignature();
        
        acepreg.createNewRegulation();
        acepreg.BRSCollaboration();
        acepreg.attachPDF();
        
        acepreg.save();
        List<string> docTypes = new list<String> { 'PDF' , 'DOC'};
            string sDocType = acepreg.flattenSelectedDocuments(docTypes);
        acepreg.deleteSupplimentalRecords();
        acepreg.deleteRecord();
        //acepreg.redirect();
        acepreg.cancel();
        acepreg.attachcbiPDF();
        acepreg.attachcbideletedPDF();
        acepreg.SaveIssuedDt();
        //acepreg.viewCBIdeletedPdf();
        //acepreg.viewDraftPDF();
        acepreg.renderCourtesyPermit = false;
        acepreg.PDFVersion= '';
        objauth.Recordtypeid = authRecordTypeId ;
        objauth.Authorization_Type__c = 'Permit';
        update objauth;
        Facility__c  entry = testData.newfacility('Domestic Port');
        Authorization_Junction__c objauthjun = new Authorization_Junction__c();
        objauthjun.Authorization__c = objauth.id;
        objauthjun.Port__c = entry.id;
        insert objauthjun;
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        ApexPages.currentPage().getParameters().put('wfid',objWF.id);
        ApexPages.currentPage().getParameters().put('PermitPackage','True');
        CARPOL_BRS_EPRegulations_extension acepreg2 = new CARPOL_BRS_EPRegulations_extension(sc2);
        acepreg2.viewDraftPDF();
        //
            try{
                acepreg2.attachPDF();}
            catch (exception ex){
                
            }
        //objPrefix1.Permit_PDF_Template__c = 'CARPOL_BRS_CourtesyPermit';
       /* update objPrefix1;
        acepreg2.viewDraftPDF();
        acepreg2.attachcbiPDF();
        acepreg2.attachcbideletedPDF();
        acepreg2.SaveIssuedDt();
        //acepreg2.viewCBIdeletedPdf();
        acepreg2.PDFVersion= '';
        // string sDocType = acepreg.flattenSelectedDocuments();
        //objauth.Thumbprint__c = objTP1.id;
        objauth.Recordtypeid = authRecordTypeId ;
        objauth.Authorization_Type__c = 'Permit';
        update objauth;
        System.assert(acepreg != null);*/
        id wfid = acepreg2.wfid;
        string wfname =acepreg2.wfname;
        String CBI = 'No';
        Test.stopTest();
    }
    //-----------------------------------------------------------------------
}