public class CBI {
    public class NameException extends Exception{}
    public static final Map<String, CBIService> strategies;
    static {
      //  List<USDAGroup__c> grpList = USDAGroup__c.getall().values();
   
        strategies = new Map<String, CBIService>();
        //Type t;
     /*   if (grpList != null && grpList.size() > 0) {
            for (USDAGroup__c u: grpList) {
                //try {
                    
                    //t = Type.forName(u.name+'impl');                     
                    //CBIDataService gs = (CBIDataService)t.newInstance();
                    //strategies.put(u.name, gs);
                   strategies.put (u.name, (CBIService)Type.forName(u.name+'impl').newInstance() ); 
                //} catch (Exception e) {
                //  
                //    continue;
                //}
                
                
            }
        } */
    }
    
    private CBIService strategy;
    
    public CBI(String name) {
       if (!strategies.containsKey(name))
            throw new NameException(name + ' is not valid.');
        strategy = strategies.get(name);
    }
    
    public AC__c getCBIData(AC__c lineItem) {
        return strategy.getCBIData(lineItem);
    }

    public AC__c getCBIDeletedData(AC__c lineItem) {
        return strategy.getCBIDeletedData(lineItem);
    }    
}