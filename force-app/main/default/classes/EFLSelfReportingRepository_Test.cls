@isTest(seealldata=false)
public class EFLSelfReportingRepository_Test {
	
    static Application__c app;
    static Authorizations__c auth;
    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static AC__c li;
    static Applicant_Attachments__c objaa;
    static Attachment objattach;
    static Id RtId;
    static Location__c loc;
    static Construct__c construct; 
    static Report_Summary__c rs;
    static Report_Summary__c rs1;
    static List<Report_Summary__c> rsList;
    static Self_Reporting__c sr;
    static EFL_Related_Record__c eflRelatedRec;
    
    @IsTest
    public static void init(){
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
      
        app = new Application__c();
        app = testData.newapplication();
        auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
        country = testDataBRS.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testDataBRS.newlevel2region(lr.id);
        li = new AC__c();
        li = testData.newLineItem('Resale/Adoption', app, auth);

          
        RtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId(); 
      
        loc = testDataBRS.newlocation(country.id,lr.id,lvl2Reg.id,li.id,RtId);
          
        construct = testDataBRS.newconstruct(li.id);
        construct.Authorization__c = auth.id;
        update construct;
         
         
        rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Volunteer Monitoring Report';
        rs.Status__c = 'UnSubmitted';
        insert rs;        
        
	    sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()- 62;
        sr.Monitoring_Period_End__c = date.today() - 60;
        sr.Observation_Date__c = date.today() - 61;
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Authorization__c = auth.id;
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.Planting_ID__c=loc.id;
        sr.Any_Planting_Material_Harvested__c = 'No';
        sr.In_field_Termination_Date__c = date.today() + 60;
        sr.In_Field_Description__c = 'Test description';
        sr.How_was_material_disposed__c = 'Both';
        sr.Stored_Quantity__c = 5;
        sr.Final_Volunteer_Monitoring_Report__c = 'No';
        sr.No_Monitoring_Report__c = False;
        //sr.Units__c = 'Acres';
        sr.Stored_Material_Type__c = 'test';
        sr.Stored_Description__c = 'Test';
        sr.Off_Field_Destruction_Date__c = date.today() + 61;
        sr.Off_Field_Description__c = 'test';
        sr.Before_Harvest_Destruction_Date__c = date.today() + 61;
        sr.Before_Harvest_Description__c = 'test';
        sr.Still_Growing_Quantity__c = 4;
        sr.Still_Growing_Description__c = 'test';
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
        sr.Planted_Material_Destroyed_Before_Harves__c = 'Yes';
        sr.Planting_Material_Still_Growing__c  = 'Yes';
        sr.Explanation__c = 'test';
        sr.Unexpected_Effects__c = 'test';
        sr.Deleterious_Effects__c = 'test';
        sr.Deleterious_Effects_Data__c = 'test';
        sr.Crop_Observations__c = 'test';
        sr.Is_Submitted__c = false;
        sr.Start_Date__c= date.today()+100;
        sr.Anticipated_Harvest_Destruct_Date__c = date.today();
        insert sr; 
       
                 
        objaa = testData.newAttach(li.id);
        objaa.Authorization__c = auth.id;
        objaa.Report_Summary__c = rs.id;
        
        String AppAttRepSummRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Report Summary').getRecordTypeId();
        objaa.recordtypeId = AppAttRepSummRecordTypeId;
        update objaa;
        objattach = testData.newAttachment(objaa.id);          
         
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = loc.id;
        eflRelatedRec.Construct__c  = construct.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;    
          
      }
 
      public static testMethod void AllGetMethod_Test(){
      
          init();
          
          EFLSelfReportingRepository.selfRepListByRSId(rs.Id);
          EFLSelfReportingRepository.selfRepById(sr.Id);
          
          List<Report_Summary__c> rsList = new List<Report_Summary__c>();
          Report_Summary__c rs1 = new Report_Summary__c();
          rs1.Authorization__c = auth.id;
          rs1.Certify_and_Submit__c = true;
          rs1.Description__c = 'Test';
          rs1.Due_Date__c = date.today() + 60;
          rs1.Equipment__c = 'Facility';
          rs1.Report_Type__c = 'Volunteer Monitoring Report';
          rs1.Status__c = 'UnSubmitted';
          rsList.add(rs1);
        	insert rsList;
          EFLSelfReportingRepository.selfRepListByRSIdandRSType(rsList,'Volunteer Monitoring Report');
          EFLSelfReportingRepository.searchSRByKeywordWithInReportSumID(rs.Id,'Test');
         }
    @isTest
    static void testselfRepListByRSId1() {
        init();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
     
          Test.startTest();
          try{
              EFLSelfReportingRepository.selfRepListByRSId(rs.Id);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
     @isTest
    static void testselfRepById1() {
        init();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
     
          Test.startTest();
          try{
              EFLSelfReportingRepository.selfRepById(sr.Id);
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
  /*   @isTest
    static void testselfRepListByRSIdandRSType1() {
        init();
        List<Self_Reporting__c> srList = new List<Self_Reporting__c>();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindLocationByLineItemId3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
     
          Test.startTest();
          try{
              List<Report_Summary__c> rsList = new List<Report_Summary__c>();
              Report_Summary__c rs1 = new Report_Summary__c();
              rs1.Authorization__c = auth.id;
              rs1.Certify_and_Submit__c = true;
              rs1.Description__c = 'Test';
              rs1.Due_Date__c = date.today() + 60;
              rs1.Equipment__c = 'Facility';
              rs1.Report_Type__c = 'Volunteer Monitoring Report';
              rs1.Status__c = 'UnSubmitted';
              rsList.add(rs1);
              insert rsList;
              EFLSelfReportingRepository.selfRepListByRSIdandRSType(rsList,'Volunteer Monitoring Report');
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }*/
    @isTest
    static void testsearchSRByKeywordWithInReportSumID1() {
        init();
        Id ChatterProfileId = [SELECT ID FROM Profile WHERE Name='BRS State Reviewer' LIMIT 1].Id;
        User chatterUser = new User(Alias = 'Chatter',userName = 'testFindSRByKeyword3@test.com',isActive = true, lastName='TestUser',ProfileId = ChatterProfileId,Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        insert chatterUser;
        
        System.runAs(chatterUser)
        {
     
          Test.startTest();
          try{
              EFLSelfReportingRepository.searchSRByKeywordWithInReportSumID(rs.Id,'Test');
          }
          catch(Exception e){
              System.assert(e.getmessage().contains('Operation not valid for this user type'),true);
          }
          Test.stopTest();
        }
    }
}