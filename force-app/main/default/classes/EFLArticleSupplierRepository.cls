/*
* Author: Ravee Racharla Date:11/8/2018
* Purpose: Central place to access Article Supplier/Developer object from database with different set of filter parameters.
*/
public inherited sharing class EFLArticleSupplierRepository {
    
    // Obtain Article Supplier Developer records using line Item ID 
    // Article_Supplier_Developer__c    
    public static List<Article_Supplier_Developer__c> selectASDByLineItemID(id lineItemID){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Article_Supplier_Developer__c');
            return [SELECT id, RecordType.Name,First_Name__c, name, Organization_Name__c,  City__c, State__r.name, Country__r.name, Program__c,country_text__C, state_text__c,Postal_Code_Text__c,Postal_Code__c,Alternate_Phone_Text__c,Record_Type_Name__c,Level_2_Region__r.Name,EnableASPDeleteAction__c,EnableASPDisplayAction__c,EnableASPUpdateAction__c,
                    Email_Address_Text__c,Phone_Text__c,Line_item__r.OwnerId, CreatedById,
                    Status__c, Status_Graphical__c, Applicant_Instructions__c
                    FROM Article_Supplier_Developer__c 
                    WHERE Line_Item__c =:lineItemID];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLinkRegulatedArticleRepository.selectLRAByLineItemID()',e);                
        } 
        return null;
    }
    
    public static List<Article_Supplier_Developer__c> selectASDByLineItemIDForClone(id lineItemID){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Article_Supplier_Developer__c');
            return [SELECT RecordType.Name,First_Name__c, name, Organization_Name__c,  Email__c, Phone__c, 
                    Alternate_Phone__c, Street_Address__c, City__c,Postal_Code__c,Program__c, EnableASPDeleteAction__c,EnableASPDisplayAction__c,EnableASPUpdateAction__c,
                    Email_Address_CBI__c,Alternate_Phone_CBI__c,Phone_CBI__c,Postal_Code_CBI__c,State_CBI__c,Country_CBI__c,
                    State__r.name, Country__r.name,Country_Text__c,State_Text__c,Postal_Code_Text__c,Record_Type_Name__c,Alternate_Phone_Text__c,Level_2_Region__r.Name,
                    Email_Address_Text__c,Phone_Text__c
                    FROM Article_Supplier_Developer__c 
                    WHERE Line_Item__c =:lineItemID];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLArticleSupplierRepository.selectASDByLineItemIDForClone()',e);                
        } 
        return null;
    }
    
    // Obtain Article Supplier Developer record using id  
    // Article_Supplier_Developer__c    
    public static  Article_Supplier_Developer__c selectByID(id asdID){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Article_Supplier_Developer__c');
            return [SELECT id,RecordType.Name,Line_Item__c,  First_Name__c, name, Organization_Name__c,  Email__c, Phone__c, 
                    Alternate_Phone__c, Street_Address__c, City__c,Postal_Code__c,Program__c, 
                    Email_Address_CBI__c,Alternate_Phone_CBI__c,Phone_CBI__c,Postal_Code_CBI__c,State_CBI__c,Country_CBI__c,EnableASPDeleteAction__c,EnableASPDisplayAction__c,EnableASPUpdateAction__c,
                    State__r.name, Country__r.name,Country_Text__c,State_Text__c,Record_Type_Name__c,Postal_Code_Text__c,Alternate_Phone_Text__c,Level_2_Region__r.Name,
                    Email_Address_Text__c,Phone_Text__c,
                    Status__c, Status_Graphical__c, Applicant_Instructions__c
                    FROM Article_Supplier_Developer__c 
                    WHERE ID =:asdID];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLArticleSupplierRepository.selectByID()',e);                
        } 
        return null;
    }
    
    
    // public static  Applicant_Contact__c  populateApplicantInfo(id ApplicationID){
    public static  Contact  populateApplicantInfo(id ApplicationID){   
        Application__c objapp = new   Application__c(); 
        //Applicant_Contact__c objappContact = new Applicant_Contact__c(); 
        Contact objappContact = new Contact();
        objapp = [select id,Name,Applicant_Name__c,Application_Status__c, Applicant_Name__r.Name,Applicant_Name__r.AccountId,Applicant_Name__r.FirstName,                   
                  Applicant_Name__r.LastName,Organization__c,Applicant_Email__c,Applicant_Phone__c,Applicant_Address__c,
                  Applicant_Fax__c,USDA_License_Expiration_Date__c,USDA_Registration_Expiration_Date__c, Applicant_Name__r.email,
                  Applicant_Name__r.Mailing_Street_LR__c, Applicant_Name__r.Mailing_City_LR__c,
                  Applicant_Name__r.Mailing_State_Province_LR__c,Applicant_Name__r.Mailing_Country_LR__c,Applicant_Name__r.Mailing_Zip_Postalcode_LR__c
                  from Application__c 
                  where id=:applicationID limit 1];
        //Ravee Racharla 12/4/2018
        //Read data from Associated Contact object 
        
        if (objapp != null ){
            
            /*     objappContact = [select id, Name, First_Name__c, Contact_Organization__c,Email_Address__c, Phone__c, 
Mailing_Street__c, Mailing_City__c, Mailing_State_LR__c, Mailing_Country_LR__c, Mailing_Zip_Postal_Code__c
from Applicant_Contact__c
where  Name =:objapp.Applicant_Name__r.LastName
and First_Name__c =:objapp.Applicant_Name__r.FirstName Limit 1];   

} */
            //Ravee Racharla 12/4/2018
            
            //KA:W-034599 ::: Starts:::::: Read data from Contact object
            objappContact = [select id, Name, FirstName , LastName ,Email, Phone,Account.Name, 
                             Mailing_Street_LR__c, Mailing_City_LR__c, Mailing_State_Province_LR__c, Mailing_Country_LR__c, Mailing_Zip_Postalcode_LR__c
                             from Contact
                             where  LastName =:objapp.Applicant_Name__r.LastName
                             and FirstName =:objapp.Applicant_Name__r.FirstName Limit 1];    
            
        }
        //KA:W-034599 ::: Ends::::::
        
        return objAppContact; 
    }
}