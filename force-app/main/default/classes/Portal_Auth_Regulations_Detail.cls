public with sharing class Portal_Auth_Regulations_Detail{
        public Id authID{get;set;}
        public Id Id{get;set;}
        public string AuthJuncRecordTypeId{get;set;}
        public List<Authorization_Junction__c> AssociatedRegulations;
        public List<Authorization_Junction__c> Regulations;
        public List<Authorization_Junction__c> AddInformation;
        public List<Authorization_Junction__c> CBPInformation;
        public List<Authorization_Junction__c> OriginalRegulations=new  List<Authorization_Junction__c>();
        public List<Authorization_Junction__c> OriginalAddInformation=new  List<Authorization_Junction__c>();
        public List<Authorization_Junction__c> OriginalCBPInformation=new  List<Authorization_Junction__c>();
        public string regulationtype;
        public string program;
        public integer TotalAgree;
        public integer TotalRegs;
        public integer Totalunanswered;
        public Boolean erroroccured;
       
        public Portal_Auth_Regulations_Detail(ApexPages.StandardController controller){
                authID=ApexPages.currentPage().getParameters().get('id');
                Regulations=new  List<Authorization_Junction__c>();
                AddInformation=new  List<Authorization_Junction__c>();
                CBPInformation=new  List<Authorization_Junction__c>();
                AuthJuncRecordTypeId=Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('Regulation Junction').getRecordTypeId();
                program=[SELECT Program_Pathway__c,Program_Pathway__r.program__r.name FROM Authorizations__c WHERE id=:authID].Program_Pathway__r.program__r.name;
        }
//--------------------------------Standard/Import Requirements-------------------------------------------------------------------------//  
        public List<Authorization_Junction__c> getRegulations(){
                regulationtype='Import Requirements';
                if(program=='BRS')regulationtype='Standard';
                Regulations=[SELECT ID,Name,Order_Number__c,Regulation_Short_Name__c,Is_Active__c,Regulation_Description__c,Applicant_Agree__c,Applicant_Comments__c,Regulation__c FROM Authorization_Junction__c WHERE RecordTypeID=:AuthJuncRecordTypeId AND Type__c=:regulationtype AND Authorization__c=:authID ORDER BY Order_Number__c];
                OriginalRegulations=Regulations.deepClone();
                return Regulations;
        }
//--------------------------------Additional Information/Supplemental Conditions(BRS)-------------------------------------------------//       
        public List<Authorization_Junction__c> getAddInformation(){
                regulationtype='Additional Information';
                if(program=='BRS')regulationtype='Supplemental Conditions';
                AddInformation=[SELECT ID,Name,Order_Number__c,Regulation_Short_Name__c,Is_Active__c,Regulation_Description__c,Applicant_Agree__c,Applicant_Comments__c FROM Authorization_Junction__c WHERE RecordTypeID=:AuthJuncRecordTypeId AND Type__c=:regulationtype AND Authorization__c=:authID ORDER BY Order_Number__c];
                OriginalAddInformation=AddInformation.deepClone();
                return AddInformation;
        }
//--------------------------------CBI Regulations list-------------------------------------------------------------------------------------//        
        public List<Authorization_Junction__c> getCBPInformation(){
                CBPInformation=[SELECT ID,Name,Order_Number__c,Regulation_Short_Name__c,Is_Active__c,Regulation_Description__c,Applicant_Agree__c,Applicant_Comments__c FROM Authorization_Junction__c WHERE RecordTypeID=:AuthJuncRecordTypeId AND Type__c=:'Instruction for CBP Officers' AND Authorization__c=:authID ORDER BY Order_Number__c];
                OriginalCBPInformation=CBPInformation.deepClone();
                return CBPInformation;
        }
//--------------------------------SAVE Button-------------------------------------------------------------------------------------// 
        public PageReference tosave(){
                system.debug('-- inside tosave');
                saveagreementchanges();
                if(erroroccured==false)ApexPages.AddMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'<font color = green>'+'Saved Successfully!' +'</font>'));
                //if(erroroccured==true)ApexPages.AddMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Error!'));
                return null;
        }
//--------------------------------Submit Button-------------------------------------// 
        public PageReference submit(){
                system.debug('inside submit' + authID);
                list<Authorizations__c> authToUpdate=[SELECT Id,Status__c, Application__c FROM Authorizations__c WHERE id=:authID limit 1];
                list<Ac__c> lineitem=new  list<Ac__c>();
                list<Workflow_Task__c> WFT=new  list<Workflow_Task__c>();
                TotalAgree=0;
                TotalRegs=0;
                Totalunanswered=0;
//-- Check if any Import Requirements or Standard conditons are not answered --// 
                for(Authorization_Junction__c rj:Regulations){
                        if(rj.Applicant_Agree__c=='Agree')TotalAgree++;
                        if(rj.Regulation__c!=null)TotalRegs++;
                        if(rj.Applicant_Agree__c==null||rj.Applicant_Agree__c=='')Totalunanswered++;
                }
//-- Check if any IAdditional Information or Supplemental conditons are not answered --//
                if(Totalunanswered==0){
                        for(Authorization_Junction__c rj:AddInformation)if(rj.Applicant_Agree__c==null||rj.Applicant_Agree__c=='')Totalunanswered++;
                }
//-- Check if any CBI related Information regulations are not answered --//
                if(Totalunanswered==0){
                        for(Authorization_Junction__c rj:CBPInformation)if(rj.Applicant_Agree__c==null||rj.Applicant_Agree__c=='')Totalunanswered++;
                }
                if(Totalunanswered>0){
                        ApexPages.addMessage(new  ApexPages.message(ApexPages.severity.ERROR,'<font color = red>'+'You must respond to all conditions to submit responses.'+'</font>'));
                        return null;
                       
                }
                else{ saveagreementchanges(); }
                system.debug('#### authToUpdate[0] = '+authToUpdate[0]);
                for(Ac__c li:[SELECT id,status__c FROM AC__c WHERE Authorization__c=:authToUpdate[0].id]){
                        li.status__c='Submitted';
                        lineitem.add(li);
                }
                Update lineitem;
                try {
                for(Workflow_Task__c w:[SELECT id,Status__c FROM Workflow_Task__c WHERE Authorization__c=:authToUpdate[0].id AND Status__c='Pending' AND Status_Categories__c='Customer Feedback']){
                        Workflow_Task__c wr = new Workflow_Task__c ();
                        wr.Id = w.Id;
                        wr.Status__c='In Progress';
                        wr.Status_Categories__c='';
                        WFT.add(wr);
                }
                
                Update WFT;
                } catch(Exception e) {
                    System.debug('Submit Exception e: '+e.getStackTraceString());
                }
                
                if(authToUpdate.size()>0){
                    authToUpdate[0].Status__c='In Review';
                    Update authToUpdate;
                    
                    List<Application__c> appToUpdate = [SELECT Id, Application_Status__c FROM Application__c WHERE Id = :authToUpdate[0].Application__c];
                    if (appToUpdate.size() > 0){
                        appToUpdate[0].Application_Status__c = 'Submitted';
                    	update appToUpdate[0];
                    }
                }
            
                ApexPages.AddMessage(new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully!')); 
                PageReference RowEditing=new  PageReference('/apex/Portal_Auth_Regulations_Detail?id='+authID);
                RowEditing.setRedirect(true);
                return RowEditing;
        }
//---------------------------------- Save Agreement changes----------------------------------------------------------- //
        public list<Authorization_Junction__c> saveagreementchanges(){
                try{
                       system.debug('-- inside saveagreementchanges');
                        AssociatedRegulations=new  List<Authorization_Junction__c>();
                        erroroccured=false;
                        for(integer i=0;i<Regulations.size();i++){
                                if(OriginalRegulations[i].Applicant_Agree__c!=Regulations[i].Applicant_Agree__c||OriginalRegulations[i].Applicant_Comments__c!=Regulations[i].Applicant_Comments__c){
                                        Authorization_Junction__c aj=new  Authorization_Junction__c();
                                        aj.Id=Regulations[i].Id;
                                        aj.Applicant_Agree__c=Regulations[i].Applicant_Agree__c;
                                        aj.Applicant_Comments__c=Regulations[i].Applicant_Comments__c;
                                        AssociatedRegulations.add(aj);
                                }
                        }
                        for(integer i=0;i<AddInformation.size();i++){
                                if(OriginalAddInformation[i].Applicant_Agree__c!=AddInformation[i].Applicant_Agree__c||OriginalAddInformation[i].Applicant_Comments__c!=AddInformation[i].Applicant_Comments__c){
                                        Authorization_Junction__c aj=new  Authorization_Junction__c();
                                        aj.Id=AddInformation[i].Id;
                                        aj.Applicant_Agree__c=AddInformation[i].Applicant_Agree__c;
                                        aj.Applicant_Comments__c=AddInformation[i].Applicant_Comments__c;
                                        AssociatedRegulations.add(aj);
                                }
                        }
                        for(integer i=0;i<CBPInformation.size();i++){
                                if(OriginalCBPInformation[i].Applicant_Agree__c!=CBPInformation[i].Applicant_Agree__c||OriginalCBPInformation[i].Applicant_Comments__c!=CBPInformation[i].Applicant_Comments__c){
                                        Authorization_Junction__c aj=new  Authorization_Junction__c();
                                        aj.Id=CBPInformation[i].Id;
                                        aj.Applicant_Agree__c=CBPInformation[i].Applicant_Agree__c;
                                        aj.Applicant_Comments__c=CBPInformation[i].Applicant_Comments__c;
                                        AssociatedRegulations.add(aj);
                                }
                        }
                        if(AssociatedRegulations.size()>0)
                            Update AssociatedRegulations;
                }
                catch(exception e){
                       
                
                        ApexPages.addMessages(e);
                        erroroccured=true;
                        return null;
                }
                return AssociatedRegulations;
        }
}