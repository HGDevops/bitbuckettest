@isTest(seealldata=false)
private class CARPOL_DuplicatePreventer_Test {
    //CM 5/29
    static testmethod void CARPOL_DuplicatePreventer_Test1(){
        Test.startTest();
        //CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        objapp.Share_with_Roles__c='BRS';
        update objapp; 
        Authorizations__c testAuth = testdata.newAuth(objapp.Id);
        AC__c testLine= testData.newLineItem('Personal Use',objapp);
        testLine.Status__c = 'Ready to Submit';
        testLine.Authorization__c = testAuth.Id;
        testLine.RecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByDeveloperName().get('BRS_PERMIT_RT').getRecordTypeId();
        update testLine;
        Map<Id,AC__c> testMap = new Map<Id,AC__c>();
        testMap.Put(testLine.Id, testline); 
        CARPOL_DuplicatePreventer DP = new CARPOL_DuplicatePreventer(testMap);
        DP.duplicatePreventer();
        testLine.RecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByDeveloperName().get('Live_Dogs_RT').getRecordTypeId();
        update testLine;
        DP.duplicatePreventer();
        Test.stopTest();
    }
}