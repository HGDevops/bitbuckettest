/* 
* Description  : Pre Screening Questionnaire 
*/
public without sharing class EFLPreScreeningQuestionnaireController {
    
    public EFLIPreScreeningProcessor PreScreeningProcessor {get; set;}
    public boolean startPage {get;set;}
    public boolean lastPage {get;set;}
    public boolean toggleCheck {get;set;}
    String rerenderPSQ2 = Apexpages.currentPage().getParameters().get('rerenderPSQ2');
    
    public string movementType{
        get{return movementType;} 
        set{ 
            movementType = value;  
            PreScreeningProcessor.updateMovementAndPathway(movementType,programPathway);  
        }
    }
    
    public id programPathway{ 
        get{return programPathway;} 
        set{ 
            programPathway = value;
            PreScreeningProcessor.updateMovementAndPathway(movementType,programPathway);  
        }
    }
    
    //--PSQ Attributes-----//
    public EFLPSQQuestion psqq {get;set;} 
    public EFLPreScreeningWrapper psw {get;set;}
    public EFLApplicationWrapper apw {get;set;}
    public String communityURL;                    
    public string selectedText {get; set;}
    public boolean showConditionsVisibility {get; set;}
    public string showConditionURL {get; set;}
    public Id personalAcctId;
    
    /*---Constructor---*/
    
    public EFLPreScreeningQuestionnaireController(){
        
        toggleCheck = true;
        PreScreeningProcessor = EFLPreScreeningFactory.getPrescreeningProcessor(movementType,programPathway);
        psw = new EFLPreScreeningWrapper(PreScreeningProcessor);
        apw = new EFLApplicationWrapper();
        psw.getNextQuestion();
        setStartPage(psw);
        psqq = psw.currentQuestion;
    }
    
    public List<SelectOption> getQuestionSelectOptions() 
    {
        return PreScreeningProcessor.getQuestionOptions(psw);    
    }
    
    public void setToggleStatus(){
        toggleCheck = Boolean.valueOf(Apexpages.currentPage().getParameters().get('rerenderPSQ2'));
    }
    
    public void getNextQuestion(){
        initShowConditions();
        if(psqq.SelectedOption ==NULL){
            ApexPages.Message message = new ApexPages.message(ApexPages.severity.ERROR,'Please select an option');       
            ApexPages.addMessage(message);
        }
        else {
            
            if(psqq.psquestion.Question_classification__c == 'Movement Type')
            {
                movementType = getMovementType(psqq.SelectedOption);
            }
            else if(psqq.psquestion.Question_classification__c == 'Program Pathway')
            {
                
                programPathway = getProgramPathway(psqq.SelectedOption);
            }
            else
            {
                movementType = null;
                programPathway = null;
            }
            //Account Look Up – Pre Populated for External Users----
            if(userinfo.getUserType() != 'Standard'){
               psw.application.Applicant_Account__c = EFLUserUtility.userDetails.Contact.AccountId; 
            }
            psw.getNextQuestion();
            setStartPage(psw);
            setLastPage(psw);
            psqq = psw.currentQuestion;
            
        }
        
    }
    
    public void getPrevQuestion(){
        initShowConditions();
        psw.getPrevQuestion(); 
        setStartPage(psw);
        setLastPage(psw);
        psqq = psw.currentQuestion;
        if(psqq.psquestion.Question_classification__c == 'Movement Type')
        {
            movementType = getMovementType(psqq.SelectedOption);
        }
        else if(psqq.psquestion.Question_classification__c == 'Program Pathway')
        {
            
            programPathway = getProgramPathway(psqq.SelectedOption);
        }
        else
        {
            movementType = null;
            programPathway = null;
        }
    } 
    
    public PageReference Cancel(){
        CARPOL_URLs__c  landingPage = CARPOL_URLs__c.getInstance('Landing_Page');  
        string landingPageURL  = landingPage.URL__c;
        PageReference pr = new PageReference(landingPageURL);
        pr.setRedirect(true);
        return pr;
    } 
    public string getMovementType(string selectedOption)
    {
        if(psw.QuestionList.size()>0)
        {
            for(Wizard_Selections__c opt : psw.QuestionList[0].psqOptions)
            {
                if(opt.id == psw.QuestionList[0].selectedOption)
                {
                    return opt.Name;
                }
            }
        }
        else
        {
            for(Wizard_Selections__c opt : psw.currentQuestion.psqOptions)
            {
                if(opt.id == selectedOption)
                {
                    return opt.Name;
                }
            }
        }
        return null;
    }
    
    public id getProgramPathway(string selectedOption)
    {
        for(Wizard_Selections__c opt : psw.currentQuestion.psqOptions)
        {
            if(opt.id == selectedOption)
            {
                return opt.Program_Pathway__c;
            }
        }
        return null;
    }
    
    public void setStartPage(EFLPreScreeningWrapper psw){
        startPage = false;
        if(psw.currentQuestionIndex == 1){
            startPage = true;
        }
    } 
    
    public void setLastPage(EFLPreScreeningWrapper psw){
        LastPage = false;
        if(psw.currentQuestion.psquestion.Type__c == 'PERMITTED'){
            LastPage = true;
            setShowConditions();
        }        
    }
    
    public pagereference proceedWithApplication(){
        PageReference p;
        if(string.IsBlank(psw.application.Applicant_Account__c)){
            System.debug(LoggingLevel.INFO,'===>string is blank');
            LastPage = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'<font color = red>'+'You must enter a value' +'</font>'));
            return p; 
        }
         System.debug(LoggingLevel.INFO,'===>continue string is blank');
        account selectedAccount = [select id,Type from Account where id = :psw.application.Applicant_Account__c];
        if(selectedAccount.type == 'Business' && userinfo.getUserType() == 'Standard'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'<font color = red>'+'Internal user can not create the Business Applications' +'</font>'));
            return p;
        }
        
        CARPOL_URLs__c  baseurl = CARPOL_URLs__c.getValues('External');  
        string communityURL  = baseurl.URL__c; 
        
        EFLIApplicationEngine handler = EFLApplicationEngineFactory.getEngine(psw);
        
        list<sObject> applWrap = new list<sObject>();
        id lineItemID;
        apw = handler.createApplication(psw);
        applWrap.add(apw.application);        
        applWrap.add(apw.lineItem);
        applWrap.addAll(apw.applicationRequestList);
        
        
        if(applWrap!=null){
            Database.SaveResult[] results = Database.insert(applWrap,true);
            // Check results.
            for (Integer i = 0; i < results.size(); i++) {
                if (results[i].isSuccess()) {
                    string lineitem = results[i].getId();
                    if(lineitem.startsWithIgnoreCase('a00')){
                        lineItemID = results[i].getId();} 
                } else {
                    //throwerror('Error: could not create any record '+ results[i].getErrors()[0].getMessage() + '\n');
                }
            }
        } 
        PageReference login;
        login = new PageReference('/apex/EFLLineItem?id='+lineItemID);
        login.setRedirect(true);
        return login;        
        
    }  
    
    public List<EFLPSQQuestion> getQuestions()
    {
        return psw.QuestionList;
    }
    
    public EFLPreScreeningQuestionnaireController getPageController()
    {
        return this;
    }  
    
    public void psqqSelectOptionOnChange()
    {
        psqq.SelectedOptionText = selectedText;
    }
    
    public void initShowConditions()
    {
        showConditionsVisibility = false;
        showConditionURL = '';
    }
    
    public void setShowConditions()
    {
        //get thumbprint from current question to filter regulation matrix (which is the last question since thi method is called from last question context)    
        id thumbprintId = psw.currentQuestion.psquestion.Signature__c;
        //Program Pathway filter to show condtions - currently only filtered to show condotion for Dogs Pathway. In future, other pathways can be added
        list<string> lineitemProgramPathways = new list<string>();
        lineitemProgramPathways.add(EFLGlobalConstants.getConstantValue('PROGRAM_PATHWAY_ACLD'));
        if(thumbprintId != null)
        {
            List<Signature_Regulation_Junction__c> signRegs = new List<Signature_Regulation_Junction__c>();
            signRegs = EFLPSQUtility.regulationsByThumbprintIdAndPathway(thumbprintId, lineitemProgramPathways);
            boolean regulationExist = false;
            if(!signRegs.isEmpty())
            {
                for(Signature_Regulation_Junction__c srj : signRegs)
                {
                    if(srj.Regulation__c!=null)
                    {
                        regulationExist = true;
                        break;
                    }
                }
                if(regulationExist)
                {
                    showConditionsVisibility = true;
                    showConditionURL = '/apex/CARPOL_UNI_DISPLAY_CONDITIONS?thumbprintId='+thumbprintId;
                }                
            }
        }        
    }
    
}