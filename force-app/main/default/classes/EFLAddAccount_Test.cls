@isTest(seealldata=false)
public class EFLAddAccount_Test {
    static User usershare;
    static Contact testContactUser;
    
    void EFLAddAccount_Test(){
        
    }
    private static void InitData(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings(); 
        usershare = EFLUserTestDataFactory.getUser('Partner Account Admin'); 
        testContactUser = [select Id, Name, AccountId from Contact where Id = :usershare.ContactId];
        PageReference addAccountPage = Page.EFLAddAccount;
        Test.setCurrentPage(addAccountPage);
        ApexPages.currentPage().getParameters().put('accId', testContactUser.AccountId);
        
    }
    
    @IsTest static void testAddAccount(){
        InitData();
        EFLAddAccount eflA = new EFLAddAccount();
        eflA.strAccountName = 'testAccount.Name';
        Test.startTest();
        
        System.runAs(usershare){
            PageReference returnPageRef = eflA.addAccount();
            System.assert(returnPageRef!= null);
        }
        
        Test.stopTest();
    }
    
    @IsTest static void testAddAccountFailure(){
        InitData();
        EFLAddAccount eflA = new EFLAddAccount();
        //eflA.strAccountName = 'testAccount.Name';
        Test.startTest();
        
        System.runAs(usershare){
            PageReference returnPageRef = eflA.addAccount();
            System.assert(returnPageRef== null);
        }
        
        Test.stopTest();
    }
    
     @IsTest static void testAddAccountFailureDuplicateName(){
        InitData();
        EFLAddAccount eflA = new EFLAddAccount();
        eflA.strAccountName = 'testAccount.Name';
        Test.startTest();
        
        System.runAs(usershare){
            PageReference returnPageRef = eflA.addAccount();
            returnPageRef = eflA.addAccount(); // try to add again so it throws duplicate error
            System.assert(returnPageRef== null);
            System.Assert(eflA.strErrorDesc.Contains('Account Name must be unique to this account') == true);
        }
        
        Test.stopTest();
    }
    
    @IsTest static void testEditAccount(){
        InitData();
        EFLAddAccount eflA = new EFLAddAccount();
        eflA.strAccountName = 'testAccount.Name';
        Test.startTest();
        
        System.runAs(usershare){
            PageReference returnPageRef = eflA.addAccount();
            Account testAccount = [Select Id, Name from Account where Name = :eflA.strAccountName];
            System.assert(testAccount != null); // to be sure that account was added
            
            //To edit the account
            ApexPages.currentPage().getParameters().put('accId', testAccount.Id);
            eflA.strAccountName = 'Changed Account Name';
            eflA.editAccount();
            Account testAccount1 = [Select Id, Name from Account where Name = :eflA.strAccountName];
            System.assert(testAccount1 != null); //account name was modified
        }
        
        Test.stopTest();
    }
    
    @IsTest static void testEditAccountFailure(){
        InitData();
        EFLAddAccount eflA = new EFLAddAccount();
        eflA.strAccountName = 'testAccount.Name';
        Test.startTest();
        
        System.runAs(usershare){
            PageReference returnPageRef = eflA.addAccount();
            Account testAccount = [Select Id, Name from Account where Name = :eflA.strAccountName];
            System.assert(testAccount != null); // to be sure that account was added
            
            //To edit the account
            //ApexPages.currentPage().getParameters().put('accId', '123435'); // Pass a random account id to fail
            eflA.strAccountName = 'Changed Account Name';
            try{
                eflA.editAccount();
            }
            catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('List has no rows for assignment to SObject');
                System.assertEquals(expectedExceptionThrown, true); //no account found
            }
        }
        
        Test.stopTest();
    }
    
    @IsTest static void testCancel(){
        //InitData();
        Test.startTest();
        //Start Test
        //System.runAs(usershare){
        EFLAddAccount eflA = new EFLAddAccount();
        PageReference pageRef = eflA.cancel();
        System.assert(pageRef != null);
        //eflA.paramAccountId = testContactUser.AccountId;
        //List<EFLAddAccount.ConWrapper> lstAccounts = eflA.getlstConWrapperAccount();
        //System.assertEquals(1 , lstAccounts.size());
        //}
        Test.stopTest();
    }
    
    @IsTest static void testgetlstConWrapperAccount(){
        InitData();
        Test.startTest();
        //Start Test
        System.runAs(usershare){
            EFLAddAccount eflA = new EFLAddAccount();
            eflA.paramAccountId = testContactUser.AccountId;
            List<EFLAddAccount.ConWrapper> lstAccounts = eflA.getlstConWrapperAccount();
            System.assertEquals(1 , lstAccounts.size());
        }
        Test.stopTest();
    }
    
}