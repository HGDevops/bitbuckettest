public with sharing class EFLLocationController{
    //Attributes
    public string delrecid{get;set;}
    public id locationID{get;set;}
    public location__c locationRecord{get;set;}
    public Integer size{get;set;} 
    public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
    public string searchString{get;set;}
    public list<ac__c> lineitemlist {get;set;}
    public ac__c lineitem {get;set;}
    public String CBIcheck {get;set;}
    public string movementtype {get;set;}
    public String IsandReleasemvttype {get;set;}
    public String Importmvttype {get;set;}
    public Map<string,string> sobjectkeys{get;set;}
    public String DestLoc {get;set;}
    public String OrigLoc {get;set;}
    public String OrigDestLoc {get;set;}
    public String RelLoc {get;set;}
    public String recordTypename {get;set;}
    public id recordTypeId{get;set;}    
    public Boolean dayphonerequired {get;set;}
    public Boolean emailrequired {get;set;}
    public Boolean showLocationView{get;set;}
    public Boolean disableLocationId {get;set;}
    
    //check variables
    public integer olocrectype;
    public integer dlocrectype;
    
    //Render Variables
    public boolean renderDetail{get;set;} 
    public boolean renderRTDetail{get;set;}
    public boolean renderList {get;set;}
    public boolean renderConVIew {get;set;}
    public boolean hideGPSbtn{get;set;}  //KA:
    //Empty variable
    Location__c emptyLoc = new Location__c();
    
    private string showAllLocQuery = 'Select ID,Name ,RecordType.Name,Contact_Name1__c,Contact_Name2__c,Street_Add1__c,Street_Add2__c,'
        + 'Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,'
        + 'Day_Phone_2__c,Zip__c,Zip_Text__c,Other_Material_Types__c,'
        + 'Proposed_Planting__c,Number_of_Acres_Text__c,'
        + 'Contact_Address1__c,  '                   
        + 'Contact_Zip__c,Contact_City__c,'
        + 'Day_Phone__c,Email_1_Text__c, Contact_Address2__c,'
        + 'Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, '
        + 'Line_Item__c,Description__c,Applicant_Instructions__c,'
      //  + 'Line_Item__r.Application_Number__r.Application_Type__c,'
        + 'Country__r.Name,Level_2_Region__r.name,Critical_Habitat_Involved_Text__c,'
        + 'Status__c,State__r.Name,Status_Graphical__c,CreatedDate,'
        + 'Material_Type_Text__c,Inspected_by_APHIS__c,Action_Required__c,Line_Item__r.OwnerId, '
        + 'Location_Unique_Id__c,Release_History__c,Critical_Habitat_Involved__c,'
        + 'If_Yes_Please_Explain__c,Org_L1__c,Org_L2__c, Zip_CBI__c, Critical_Habitat_Involved_CBI__c, '                 
        + 'Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,'
        + 'Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c, '
        + 'EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c '
        + 'FROM Location__c '
        + 'WHERE Line_Item__c=:lineItemID '
        + 'ORDER BY State__r.Name ASC,'
        + 'Level_2_Region__r.name ASC,'
        + 'Location_Unique_Id__c ASC, name asc';
    
    
    // FROM Location__c WHERE line_item__c =:lineItemId';
    private string filterLocQuery = 'ID,Name,RecordType.Name,status__c,Line_Item__r.Application_Name__r.Application_Type__c, FROM Location__c WHERE line_item__c =:lineItemId';
    
    string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
    string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
    string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
    string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId(); 
    
    public FINAL String VAR_RELATED_CONTACT = 'relatedContact';
    public FINAL String VAR_RELATED_MATERIAL = 'relatedMaterial';
    public FINAL String VAR_RELATED_GPS = 'relatedGps';
    public FINAL String VAR_RELATED_MODE_EDIT = 'edit';
    public FINAL String VAR_RELATED_MODE_LIST = 'list';
    public FINAL String VAR_RELATED_MODE_CREATE = 'create';
    public FINAL String VAR_RELATED_MODE_VIEW = 'view';
    
    //related contact variables
    public Related_Contact__c relatedContact {get;set;}
    public Boolean isShowContactListPanel {get;set;}
    public Boolean isShowContactEditPanel {get;set;}
    public string selectedContactId{get;set;}
    
    //related material variables
    public Material__c relatedMaterial {get;set;}
    public Boolean isShowMaterialListPanel {get;set;}
    public Boolean isShowMaterialEditPanel {get;set;}
    public string selectedMaterialId{get;set;}  
    public String isDisplayOtherMaterial {get;set;}
    
    //related GPS Coordinates variables
    public GPS_Coordinate__c relatedGPSCoord {get;set;}
    public Boolean isShowGpsListPanel {get;set;}
    public Boolean isShowGpsEditPanel {get;set;}
    public Boolean isShowReleaseSection {get;set;}  //controls visibility of release section with site history, release and GPS info
    public Boolean isShowContactDetailPanel {get;set;} 
    public string selectedGpsId{get;set;}  
    
    public Boolean isLocRelatedEditMode {get;set;}
    public Boolean isLocGpsCbiCheck {get;set;}
    public String action {get;set;}
    public String sobj {get;set;}
    
    public List<LocationWrapper> allLocations {get;set;}
    public List<LocationWrapper> filteredLocations {get;set;}
    public String USCountryID; 
    
    public EFLLocationController getPageController()
    {
        return this;
    } 
    
    public Id lineItemID{
        get{
            lineItemID = EFLGenericUtility.sanitizeString( ApexPages.currentPage().getParameters().get('LineItemId') );
            return lineItemID;
        }set;
    }  
    public AC__c lineItemRecord{      
        get{
            if(lineItemRecord==null && LineItemId!=null){ 
                lineItemRecord = efllineitemrepository.selectbyID(lineItemID);
            }
            return lineItemRecord;
        }set;
    }
    public boolean lockLineItem{
        get{
            lockLineItem = EFLLockUnlockUtility.lockLineitem(lineItemRecord);
            return lockLineItem;
        }set; 
    }
    public boolean enableApplicantInstructions{
        get{
            enableApplicantInstructions = EFLLockUnlockUtility.enableApplicantInstructions(lineItemRecord);
            return enableApplicantInstructions;
        }set; 
    }
    public string instructions{ 
        get{
            if(instructions == NULL){
                instructions =  EFLGenericutility.getInstructions(lineitem.Movement_Type__c,NULL,'Location','LineItem');  
            } 
            return instructions; 
        }
        set;
    }   
    
    //KA:W-031766 ----Starts-----
    Public Boolean getenableActionRequired(){
      string currentId = apexpages.currentpage().getparameters().get('LineItemId');
      if(string.isNotBlank(currentId)){
         //KA:W-038112 - GPS Coordinates should require 4-6 sets - via UI
         list<Location__c>  lList = [select id,recordType.DeveloperName,Line_Item__c,GPS_Count__c from Location__c where Line_Item__c =: currentId AND ((recordType.DeveloperName = 'Release_Location' AND GPS_Count__c < 4) OR ((recordType.DeveloperName = 'Destination_Location' OR recordType.DeveloperName = 'Origin_and_Destination') AND Material_Count__c = 0))];
        // system.debug('lList++' +lList);
       if(!lList.isEmpty())
         
          return true;
          
       } 
      return false;
    }
    
     public string getERRMESSAGE(){
     if(getenableActionRequired()){
       return(EFLGenericUtility.getMessage('Missing Records for Construct & Location'));
     } 
     return null;
    }
    //KA:W-031766 ----ENDS-----
    public Boolean getIsCountryFieldRequired(){
        Boolean isRequired = false;
        if(recordtypeid == olocrectypeid || recordtypeid == dlocrectypeid || recordtypeid == oanddlocrectypeid || recordtypeid == locrectypeid){
            isRequired = true;
        } 
        return isRequired;
    }
    
    public Boolean getIsCountryFieldReadonly(){
        Boolean isReadonly = false;
        if((recordtypeid == olocrectypeid && lineitem.Movement_Type__c != 'Import') || recordtypeid == dlocrectypeid || recordtypeid ==  oanddlocrectypeid || recordtypeid == locrectypeid){
            isReadonly = true;
        } 
        return isReadonly;
    }
    
    public Boolean getIsStateFieldRequired(){
        Boolean isRequired = false;
        if((recordtypeid == olocrectypeid && lineitem.Movement_Type__c != 'Import') || recordtypeid == dlocrectypeid || recordtypeid == oanddlocrectypeid || recordtypeid == locrectypeid){
            isRequired = true;
        } 
        return isRequired;
    }   
    
    public Boolean getIsCountyFieldRequired(){        
        Boolean isRequired = false;
        if((recordtypeid == olocrectypeid && lineitem.Movement_Type__c != 'Import') || recordtypeid == dlocrectypeid || recordtypeid == oanddlocrectypeid || recordtypeid == locrectypeid){
            isRequired = true;
        } 
        return isRequired;
    }    
    
    public Boolean getIsLocationUniqueIdFieldRequired(){
        Boolean isRequired = false;
        if(recordtypeid == locrectypeid){
            isRequired = true;
        } 
        return isRequired;
    }   
    
    public Boolean getIsLocationInspectedFieldRequired(){
        Boolean isRequired = false;
        if(recordtypeid == dlocrectypeid || recordtypeid == oanddlocrectypeid){
            isRequired = true;
        } 
        return isRequired;
    }
    
    public String getFederalCBIText(){
        return EFLGenericUtility.getMessage('EFLLocationFederalCBI');
    }
    
    
    public EFLLocationController(){ 
        //system.debug(LoggingLevel.INFO, 'Inside the EFLLocationController');
        size=5;
        system.debug('constructor++');
        hideGPSbtn = false; //KA
        Map<string,Schema.SobjectType> describe=Schema.getGlobalDescribe();
        sobjectkeys=new  Map<string,string>();
        for(string s:describe.keyset())sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
        setRecordtype();
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('25','25'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
       
        displayLocationSummaryPage();
        USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;
    }
    
    private void setRecordtype()
    {
        if(lineItemID !=null){
            movementtype = [select Movement_Type__c,OwnerId from ac__c where id =:lineItemID ].Movement_Type__c;}
        lineitemlist = [select id,Country_of_Origin_Text__c,OwnerId ,application_number__c,Does_This_Application_Contain_CBI__c,Movement_Type__c from ac__c where id =: LineItemId]; 
        lineitem = lineitemlist[0];
        CBIcheck = lineitemlist[0].Does_This_Application_Contain_CBI__c;
        
        
        list<RecordType> rts = [SELECT ID,name, DeveloperName FROM RecordType WHERE ID = :recordTypeId];
        if (lineitemlist !=null && lineitemlist.size()>0 ){
            for(ac__c lineitem: lineitemlist){
                if(rts.size()>0){
                    for(RecordType r : rts){
                        if(lineitem.Movement_Type__c == 'Import'){
                            Importmvttype = 'Yes';
                            
                        }
                        if(r.DeveloperName == 'Destination_Location' || r.DeveloperName == 'Origin_and_Destination'){
                            DestLoc = 'Yes';
                            OrigDestLoc = 'Yes';
                            recordTypename = r.name;
                        } else if(r.DeveloperName == 'Origin_Location'){
                            OrigLoc = 'Yes';
                            recordTypename = r.name;
                            if(lineitem.Movement_Type__c == 'Interstate Movement and Release' || lineitem.Movement_Type__c == 'Interstate Movement' ){
                                IsandReleasemvttype= 'Yes';
                                recordTypename = r.name;
                            }
                            
                        } else if(r.DeveloperName == 'Release_Location'){
                            RelLoc = 'Yes';
                            recordTypename = r.name;
                            //system.debug(LoggingLevel.INFO,'RelLoc:'+ RelLoc);
                        }
                    }
                }
            }
        }
    }
    
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {    
                List<Location__c> locs = EFLLocationUtility.getSortedLocations(lineItemID);
                setCon = new ApexPages.StandardSetController(locs);
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }  
            if(oldSearchString != searchString) { 
                List<Location__c> locs = EFLLocationUtility.getSearchedSortedLocations(lineItemID, searchString);
                setCon = new ApexPages.StandardSetController(locs);
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
                oldSearchString = searchString;
            }          
            return setCon;
        }
        set; 
    }
    
    public List<LocationWrapper> getAllLocationWrappers() {
        allLocations = new List<LocationWrapper>();
        for (Location__c loc : (List<Location__c>) setCon.getRecords()){            
            allLocations.add(new LocationWrapper(loc));
        }
        return allLocations;
    }
    
    public string oldSearchString {get; set;}
    public List<LocationWrapper> getSearchedLocationWrappers() {
    
        allLocations = new List<LocationWrapper>();
        for (Location__c loc : (List<Location__c>) setCon.getRecords()){            
            allLocations.add(new LocationWrapper(loc));
        }
        return allLocations;
    }   
    
    public List<LocationWrapper> getFilteredLocationWrappers() {
        //system.debug(LoggingLevel.INFO,'Inside the getFilteredLocationWrappers.');
       if (searchString == null || searchString.length() == 0){
            //system.debug(LoggingLevel.INFO,'filtered locations is null.');
            return getAllLocationWrappers();
        } else {
            //system.debug(LoggingLevel.INFO,'filtered locations is not empty.');
            return getSearchedLocationWrappers();
        }
    } 
    
    public void searchLocations()
    {
        //setCon = null;    
        getFilteredLocationWrappers();
    }
    
    public Map<Id,Location__c> getLocationsMap() {
        return new Map<Id,Location__c>((List<Location__c>) setCon.getRecords());
    } 
    
    public List<Related_Contact__c> getRelatedContacts() {
        //system.debug(LoggingLevel.INFO,'Inside the get related contacts');
        return [Select Id,location__r.Line_Item__r.OwnerId, CreatedById,Name,Primary__c,First_Name__c,Organization_Name__c,Address__c,City__c,Phone__c,Email__c,County__c,County_CBI__c,County_Text__c,State__c,Country__c,Zip__c,Phone_CBI__c,
                Alternate_Email_CBI__c,Alternate_Email__c,Email_CBI__c,Fax__c,Alternate_Phone__c,Zip_CBI__c,Fax_CBI__c,Alternate_Email_Text__c,Alternate_Phone_Text__c,Email_Text__c,Fax_Text__c,Phone_Text__c,Zip_Text__c 
                from Related_Contact__c where Location__r.Id =: locationRecord.Id];
    }
    
    public List<Material__c> getRelatedMaterials() {
        //system.debug(LoggingLevel.INFO,'Inside the get related materials');         
        return [Select Id,Name,Quantity__c,Unit_of_Measure__c,Material__c,Other_Material__c,Material_CBI__c,Material_Type_Text__c from Material__c where Location__r.Id =: locationRecord.Id];
    }
    
    public List<GPS_Coordinate__c> getRelatedGpsCoordinates() {
        //system.debug(LoggingLevel.INFO,'Inside the get related gps coordinates');          
        return [Select Id,Name,GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,Location__r.GPS_Co_ordinates_CBI__c,GPS_Coordinates_Text__c,Self_Reporting__c from GPS_Coordinate__c where Self_Reporting__c = null and Location__r.Id =: locationRecord.Id];
    }
    
    public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> recTypes : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Location__c' ORDER BY name]) {
            if(movementtype != null  && movementtype != ''){
                if(movementtype == 'Import'){
                    options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                }else if(movementtype == 'Interstate Movement'){
                    options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                    options.add(new SelectOption(oanddlocrectypeid, 'Origin and Destination Location'));
                }else if(movementtype == 'Interstate Movement and Release'){
                    options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                    options.add(new SelectOption(oanddlocrectypeid, 'Origin and Destination Location'));
                    options.add(new SelectOption(locrectypeid, 'Release Sites Location'));
                }else if(movementtype == 'Release'){
                    options.add(new SelectOption(locrectypeid, 'Release Sites Location'));
                }
                
            }else{
                for (RecordType rt : recTypes) {
                    options.add(new SelectOption(rt.ID, rt.Name));
                }
            }
        }
        options.add(0,new SelectOption('','--Select--'));
        return options;
    }
    
    
    public void showAll(){
        try
        {
            //system.debug(LoggingLevel.INFO,'searchString@@@'+searchString);
            if(String.isNotBlank(searchString) && searchString.length()>2)
            {  
                //system.debug(LoggingLevel.INFO,'searchString@@@'+searchString);
                //build SOQL query string
                // String query='SELECT ID,Name ,RecordType.Name,status__c FROM Location__c WHERE Location_s__c LIKE \'%' + searchString + '%\' order by Name LIMIT 1000';
                String query = 'SELECT Name ,RecordType.Name,status__c,Location_Unique_Id__c,State__r.Name,Country_Text__c,County_Text__c,Zip__c,Zip_Text__c,Status_Graphical__c,Number_of_Acres_Text__c, Line_Item__c,Line_Item__r.Application_Number__r.Application_Type__c,Action_Required__c,Line_Item__r.OwnerId FROM Location__c WHERE ';
                query +=  '(Name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                query +=  ' RecordType.Name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                query +=  ' status__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                query +=  ' Location_Unique_Id__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                query +=  ' State__r.Name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                query +=  ' County_Text__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
                query +=  ' Country_Text__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' )AND';
                query +=  ' Action_Required__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' )AND';
                query += '  line_item__c =:lineItemId' ;
                query += ' order by CreatedDate DESC LIMIT 1000';                     
                
                //system.debug(LoggingLevel.INFO,'query@@@'+query);
                //return querylocator to an instance of StandardSetController
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));            
                list<Location__c> cons = setCon.getRecords(); 
                //system.debug(LoggingLevel.INFO,'cons@@@'+cons);
                //getLocationWrappers();
            }else{
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(showAllLocQuery)); 
                setCon.setPageSize(size);  
            }            
        }catch(Exception ex)
        {
            //system.debug(LoggingLevel.DEBUG,'Error caught while searching: '+ex.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'errorStr:' +ex.getMessage()));
        }   
    }  
    
    
    public void showLocationDetailView(){
        //system.debug(LoggingLevel.INFO,'showLocationDetailView(). Location Id: '+locationID);
        locationRecord = getLocationsMap().get(locationID);
        showLocationView = true;
        recordtypeid = locationRecord.RecordTypeId;
        displayDetailsSection();
        renderDetail = false;
        renderConVIew  = true;
        
    }
    
    
    public PageReference deleteRecord(){ 
        string sobjkey = delrecid.substring(0,3);
        string sobjname = sobjectkeys.get(sobjkey);
        string strqurey = 'select id from '+ String.escapeSingleQuotes(sobjname) + ' where id=:delrecid';
        list<sobject> lst = database.query(strqurey); 
        Delete lst;
        if (sobj == 'relatedContact'){
         displayRelatedPage(VAR_RELATED_CONTACT,VAR_RELATED_MODE_EDIT);
        } else if (sobj == 'relatedMaterial'){
         displayRelatedPage(VAR_RELATED_MATERIAL,VAR_RELATED_MODE_EDIT);
        } else if (sobj == 'relatedGps'){
          displayRelatedPage(VAR_RELATED_GPS,VAR_RELATED_MODE_EDIT);
        }
        /*PageReference dirpage=new  PageReference('/apex/EFLLocation?LineItemId='+LineItemId);
        dirpage.setRedirect(true);
        return dirpage;*/
        return null;
    }
    
    
    /** START: action buttons  **/
    public PageReference deleteLocation(){
        ////system.debug(LoggingLevel.INFO,'Inside the deletelocation');
        string sobjkey = delrecid.substring(0,3);
        string sobjname = sobjectkeys.get(sobjkey);
        //string strqurey = 'select id,lineitem__r.OwnerId from '+ String.escapeSingleQuotes(sobjname) + ' where id=:delrecid';
        string strqurey = 'select id from '+ String.escapeSingleQuotes(sobjname) + ' where id=:delrecid';
       // system.debug('1++'+strqurey);
        list<sobject> lst=database.query(strqurey); 
        Delete lst;
        PageReference dirpage=new  PageReference('/apex/EFLLocation?LineItemId='+LineItemId);
        dirpage.setRedirect(true);
        return dirpage;
    }
    
    public void editLocation(){
        ////system.debug(LoggingLevel.INFO,'editLocation()::: Location Id: '+locationId);
        locationRecord = getLocationsMap().get(locationId);
        isLocGpsCbiCheck = locationRecord.GPS_Co_ordinates_CBI__c;
        recordtypeid = locationRecord.RecordTypeId;
        displayDetailsSection();
        setDisabledLocationId(locationRecord);
    }  

    public void setDisabledLocationId(Location__c location) {
        if (location.Status__c == 'Review Complete' && location.Line_Item__r.Application_Number__r.Application_Type__c == 'Renewal') {
            disableLocationId = true;
        } else {
            disableLocationId = false;
        }
    }
    
    //Changes the size of pagination
    public PageReference refreshPageSize() {
        setCon.setPageSize(size);
        return null;
    }
    
    public void saveRTSelection(){
       // //system.debug(LoggingLevel.INFO,'Inside the saveRTSelection');
        Profile p = [select name 
                     from Profile 
                     where id =:UserInfo.getProfileId()];
        olocrectype = 0;
        dlocrectype = 0;
        If(recordTypeId ==null ){
         //   //system.debug(LoggingLevel.DEBUG,'Record Type Not Selected');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Select Location Type.'));
            return;
        }
        locationRecord = new Location__c(RecordTypeId=recordTypeId,Line_Item__c = lineItemID);
        if (recordtypeid ==  olocrectypeid && movementtype != 'Import' ||recordTypeId == dlocrectypeid || recordtypeid ==  oanddlocrectypeid || recordtypeid ==  locrectypeid){
            locationRecord.Country__c = USCountryID;
        }
        setRecordtype();
        list<Location__c> listloc = [select id, recordTypeId from Location__c where Line_Item__c =: lineItemId];
       // //system.debug(LoggingLevel.INFO,'listloc:'+ listloc);
        if(movementtype !=null || movementtype != '' ){
            
            ////system.debug(LoggingLevel.INFO,'movementtype:'+movementtype);
            ////system.debug(LoggingLevel.INFO,'listlocSize'+ listloc.size());
            if(movementtype == 'Import' && listloc.size()>0){
                
                for(Location__c lc:listloc){
                    if(lc.recordtypeid == olocrectypeid)
                        olocrectype =  olocrectype+1;
                    if(lc.recordtypeid == dlocrectypeId)
                        dlocrectype = dlocrectype+1;
                } 
                
                If(olocrectype >= 1 && olocrectypeid == recordTypeId ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 origin location'));
                } else if(dlocrectype >= 1 && dlocrectypeId == recordTypeId ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 destination location'));
                } else {
                    displayDetailsSection();
                }
            } else {
                displayDetailsSection();
            }            
        }   
    }   
    
    public void saveLocationInfo(){
       // //system.debug(LoggingLevel.INFO,'Inside the saveLocationInfo. Location to be saved: '+locationRecord);                
        
        try {
            ApexPages.getMessages().clear();
            upsert locationRecord;
            displayDetailsSection();
        } catch (Exception ex){
          //  //system.debug(LoggingLevel.DEBUG,'caught exception: '+ex);            
            ApexPages.addMessages(ex); 
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
        }      
    } 
    
    public PageReference cancelLocationInfo(){
        /*PageReference dirpage=new  PageReference('/apex/EFLLocation?LineItemId='+LineItemId);
        dirpage.setRedirect(true);
        return dirpage;*/
        searchString = '';
        filteredLocations = null;
        List<Location__c> locs = EFLLocationUtility.getSortedLocations(lineItemID);
        setCon = new ApexPages.StandardSetController(locs);
        setCon.setPageSize(size);  
        noOfRecords = setCon.getResultSize();
        getFilteredLocationWrappers();
        displayLocationSummaryPage();
        isShowReleaseSection = false;
        isShowMaterialListPanel = false;
        isShowGPSListPanel = false;
        isShowContactListPanel = false;
        isLocRelatedEditMode = false;
        return null;
    }
    
    public void cancel(){
        hideAllRelatedSections();
        displayDetailsSection();
    }
    
    public void action(){
       // //system.debug(LoggingLevel.INFO,'Inside the action(): '+action + ':::sobj: '+sobj);
        
        try {
            if (action == 'save'){
                if (sobj == 'relatedContact'){
                    upsert relatedContact;
                } else if (sobj == 'relatedMaterial'){
                    upsert relatedMaterial;
                } else if (sobj == 'relatedGps'){
                    //check if location cbi 
                    ////system.debug(LoggingLevel.INFO,'location cbi: '+locationRecord.GPS_Co_ordinates_CBI__c + ':::isLocGpsCbiCheck: '+isLocGpsCbiCheck);
                    if (isLocGpsCbiCheck != locationRecord.GPS_Co_ordinates_CBI__c){
                        system.debug('save1++');
                        upsert locationRecord;
                    }
                    system.debug('save2++') ;
                   upsert relatedGPSCoord;
                   //KA
                   List<sobject> count= [select id from gps_coordinate__c where Location__c =:locationRecord.Id];
                    system.debug('save3++' +count.size());
                    if(count.size() < 6)
                    {
                    system.debug('save4++');
                     hideGPSbtn = false;
                     }
                }
                cancel();
            } else if (action == 'create'){
                if (sobj == 'relatedContact'){
                    relatedContact = new Related_Contact__c();
                    relatedContact.Location__c = locationRecord.Id;
                    displayRelatedPage(VAR_RELATED_CONTACT,VAR_RELATED_MODE_EDIT);
                } else if (sobj == 'relatedMaterial'){
                    relatedMaterial = new Material__c();
                    relatedMaterial.Location__c = locationRecord.Id;
                    displayRelatedPage(VAR_RELATED_MATERIAL,VAR_RELATED_MODE_EDIT);
                } else if (sobj == 'relatedGps'){
                    relatedGPSCoord = new GPS_Coordinate__c();
                    relatedGPSCoord.Location__c = locationRecord.Id;
                    displayRelatedPage(VAR_RELATED_GPS,VAR_RELATED_MODE_EDIT);
                                        
                }
            }
            
        }  catch (Exception ex){
            ////system.debug(LoggingLevel.DEBUG,'Exception while trying to save contact: '+ex);
            ApexPages.addMessages(ex);
        }
    }
    
    public void deleteRec(){ 
        string sobjkey = delrecid.substring(0,3);
        string sobjname = sobjectkeys.get(sobjkey);
        string strqurey = 'select id from '+ String.escapeSingleQuotes(sobjname) + ' where id=:delrecid';
        list<sobject> lst=database.query(strqurey); 
        Delete lst;
        //KA:
        if (String.escapeSingleQuotes(sobjname) == 'gps_coordinate__c'){
            List<sobject> count= [select id from gps_coordinate__c where Location__c =:locationRecord.Id];
            system.debug('delrec1++' +count.size());
            if(count.size() < 6)
            {
              hideGPSbtn = false;
            }
        } 
    }
    public void editContact(){
       // //system.debug(LoggingLevel.INFO,'Inside the editContact. SelectedContactId: '+delrecid);
        relatedContact = [Select Id,Name,Primary__c,First_Name__c,Organization_Name__c,Address__c,City__c,Phone__c,Email__c,County__c,State__c,Country__c,Zip__c,Phone_CBI__c,
                          Alternate_Email_CBI__c,Alternate_Email__c,Email_CBI__c,Fax__c,Alternate_Phone__c,Zip_CBI__c,Fax_CBI__c,Alternate_Phone_CBI__c
                          from Related_Contact__c where Id =: delrecid];
        displayRelatedPage(VAR_RELATED_CONTACT,VAR_RELATED_MODE_EDIT);
    }  
    
     public void viewContact(){
        ////system.debug(LoggingLevel.INFO,'Inside the editContact. SelectedContactId: '+delrecid);
        relatedContact = [Select Id,Name,Primary__c,First_Name__c,Organization_Name__c,Address__c,City__c,Phone__c,Email__c,County__c,State__c,Country__c,Zip__c,Phone_CBI__c,
                          Alternate_Email_CBI__c,Alternate_Email__c,Email_CBI__c,Fax__c,Alternate_Phone__c,Zip_CBI__c,Fax_CBI__c,Alternate_Phone_CBI__c,Country__r.name,State__r.name,
                          County__r.name,County_CBI__c,County_Text__c,Zip_Text__c,Phone_Text__c,Alternate_Phone_Text__c,Fax_Text__c,Email_Text__c,Alternate_Email_Text__c
                          from Related_Contact__c where Id =: delrecid];
        displayRelatedViewPage(VAR_RELATED_CONTACT,VAR_RELATED_MODE_VIEW);
    }  
    
    public void editMaterial(){
        //system.debug(LoggingLevel.INFO,'Inside the editMaterial. selectedMaterialId: '+delrecid);
        relatedMaterial = [Select Id,Quantity__c,Unit_of_Measure__c,Material__c,Other_Material__c,Material_CBI__c from Material__c where Id =: delrecid];
        displayRelatedPage(VAR_RELATED_MATERIAL,VAR_RELATED_MODE_EDIT);
    }  
    public void editGps(){
        //system.debug(LoggingLevel.INFO,'Inside the editGps. Id:: '+ delrecid);
        relatedGPSCoord = [Select Id,GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,GPS_Coordinates_CBI__c,Location__r.GPS_Co_ordinates_CBI__c from GPS_Coordinate__c where Id =: delrecid];
        isLocGpsCbiCheck = locationRecord.GPS_Co_ordinates_CBI__c;
        displayRelatedPage(VAR_RELATED_GPS,VAR_RELATED_MODE_EDIT);
        
    } 
    
    /** END: action buttons  **/
    
    
    /** START: display sections  **/
    
    public void displayLocationSummaryPage(){
        //system.debug(LoggingLevel.INFO,'Inside displayLocationSummaryPage');    
        locationRecord = null;
        recordtypename = null;
        recordtypeid = null;
        renderList = true;
        renderRTDetail = false;
        renderDetail = false;
    }  
    
    
    public void displayRTSelectionPage(){
        //system.debug(LoggingLevel.INFO,'Inside displayRTSelectionPage');
        renderList = false;
        renderRTDetail = true;
        renderDetail = false;
    }   
    
    public void displayDetailsSection(){
        //system.debug(LoggingLevel.INFO,'Inside display details section');
        renderList = false;
        renderRTDetail = false;
        renderDetail = true;
        isLocRelatedEditMode = false;
        
        if (locationRecord.RecordTypeId == locrectypeid ) {
            isShowReleaseSection = true;
        }        
        displayRelatedDetailsSection();    
        
    }   
    
    public void displayRelatedDetailsSection(){
         System.debug('1++');
        //system.debug(LoggingLevel.INFO,'Inside displayRelatedDetailsSection. LocationRecord Id: '+locationRecord.Id + 'location recordtypeId: '+locationRecord.RecordTypeId );
        //isShowRelatedSection = true;
        if (locationRecord.Id != null){
            isShowContactListPanel = true;
            if (locationRecord.RecordTypeId == dlocrectypeid || locationRecord.RecordTypeId == oanddlocrectypeid){
                isShowMaterialListPanel = true;
            } else if (locationRecord.RecordTypeId == locrectypeid ) {
                isShowGPSListPanel = true;
                //KA:
                List<sobject> count= [select id from gps_coordinate__c where Location__c =:locationRecord.Id];
                system.debug('save3++' +count.size());
                if(count.size() == 6)
                {
                 system.debug('save4++');
                 hideGPSbtn = true;
                }
            } 
        }  
        
    }
    
    public void displayRelatedPage(String relatedObjType, String mode){
        //system.debug(LoggingLevel.INFO,'Inside the displayRelatedPage. relatedObjType: '+relatedObjType+' : mode: '+mode);
        hideAllRelatedSections();
        if (mode == VAR_RELATED_MODE_EDIT){
            isLocRelatedEditMode = true;
            if (relatedObjType == VAR_RELATED_CONTACT){
                isShowContactEditPanel = true; 
            } else if (relatedObjType == VAR_RELATED_MATERIAL) {
                isShowMaterialEditPanel = true;
            } else if (relatedObjType == VAR_RELATED_GPS){
                isShowGpsEditPanel = true;
            }
        } else if (mode == VAR_RELATED_MODE_LIST){
            if (relatedObjType == VAR_RELATED_CONTACT){
                isShowContactListPanel = true;
            } else if (relatedObjType == VAR_RELATED_MATERIAL) {
                isShowMaterialListPanel = true;
            } else if (relatedObjType == VAR_RELATED_GPS){
                isShowGpsListPanel = true;
                 
            }
        }
    }
    
    public void displayRelatedViewPage(String relatedObjType, String mode){
      hideAllRelatedSections();
      isShowContactDetailPanel = true;
      renderConVIew = false; 
    }
        
    public void hideAllRelatedSections(){
        isShowMaterialListPanel = false;
        isShowMaterialEditPanel = false;
        isShowGpsListPanel = false;
        isShowGpsEditPanel = false; 
        isShowContactListPanel = false;
        isShowContactEditPanel = false;
        isShowContactDetailPanel = false;
    }
    
    
    /** END: display sections  **/
    
   
}