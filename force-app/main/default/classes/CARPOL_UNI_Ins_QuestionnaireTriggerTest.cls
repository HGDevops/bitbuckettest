@isTest
public class CARPOL_UNI_Ins_QuestionnaireTriggerTest{
    public static Trade_Agreement__c ta;
    public static Country__c Country;
    public static Country__c USCountry;
    public static Level_1_Region__c level1;
    public static Level_2_Region__c level2;
    Public static location__c loc;
    public static ac__c li2;
    public static Id locrectypeid;
    public static Id olocrectypeid;
    public static Id desRecId;
    public static testmethod void test(){
        locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        desRecId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();       
        Date dateTomorrow = date.today().addDays(1);  
        DateTime dateTimeTomorrow = DateTime.now().addDays(1);  
         CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
       // test.startTest();
        Contact objCont = testData.newContact();
        Application__c objapp = testData.newapplication();
        ta = testData.newta();
        Country = testData.newcountrywithassoc();
        USCountry = testData.newcountryus();
        level1 = testData.newlevel1region(Country.id);
        level2 = testData.newlevel2region(level1.id);
        Domain__c program = new Domain__c(Name='BRS');
        insert program;
        Program_Prefix__c programPrefix = new Program_Prefix__c(Program__c=program.Id);
        insert programPrefix;
        Signature__c thumbprint = new Signature__c(Program_Prefix__c = programPrefix.id);
        insert thumbprint; 
         
        Authorizations__c objauth = testData.newAuth(objapp.Id);
       
        test.startTest();
        AC__c li = TestData.newlineitem('Personal Use', objapp);
       // AC__c li = testData.newlineitem('Personal Use', objapp, objauth);
        loc = testData.newlocation(Country.id,level1.id,level2.id,li.id,olocrectypeid);
         User u = new User(Alias = 'standt', Email='EFLInspectionTriggerHandlerTest@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = [Select Id FROM Profile WHERE Name='System Administrator'].Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='EFLInspectionTriggerHandlerTest@testorg.com');
        insert u;
        Facility__c objFac = testData.newfacility('Domestic Port');
        Inspection__c objInsp = new Inspection__c();
        objInsp.Actual_Inspection__c = dateTomorrow;
        //objInsp.Application__c = objApp.id;
        objInsp.Authorization__c = objAuth.id;
        objInsp.Corrective_Actions_Text__c = 'take some action';
        objInsp.Inspection_Comments__c = 'my comments';
        objInsp.Facility__c = objFac.id;
        objInsp.Inspection_Results_Summary__c = 'summarized results';
        objInsp.Inspector__c = objCont.id;
        objInsp.Program__c = 'AC';
        objInsp.Scheduled_Date_of_Inspection__c = dateTomorrow;
        objInsp.Inspection_Due_Date__c =Date.today();
        objInsp.Location__c = loc.Id;
       // objInsp.EFL_Inspector__c = u.Id;
        insert objInsp;
        
        EFL_Inspection_Questions_Template__c objQuestTemp = new EFL_Inspection_Questions_Template__c(Name='Test Template');
        insert objQuestTemp;
        
        EFL_Inspection_Questionnaire__c objQuest = new EFL_Inspection_Questionnaire__c();
        objQuest.Description__c = 'description text';
        objQuest.EFL_Inspection_Questions_Template__c = objQuestTemp.id;
        objQuest.Inspection__c = objInsp.id;
        objQuest.Status__c = 'Finalize Questionnaire';
        insert objQuest;
        
        EFL_Inspection_Questions__c objInspQuest = new EFL_Inspection_Questions__c();
        objInspQuest.Question__c = 'Why?';
        objInspQuest.Use_Yes_No_options__c = true;
        objInspQuest.Answer_TYpe__c = 'Yes/No';
        insert objInspQuest;
        
        EFL_INS_Template_Question_Junction__c objQuestJunc = new EFL_INS_Template_Question_Junction__c();
        objQuestJunc.EFL_Inspection_Questions_Template__c = objQuestTemp.id;
        objQuestJunc.Inspection_Template_Questions__c = objInspQuest.id;
        insert objQuestJunc;
        
        /*          
Reviewer__c objRev = new Reviewer__c();
objRev.Authorization__c = objauth.id;
objRev.Notes_to_State__c = 'test Notes';
insert objRev;
*/          
        Workflow_Task__c objWflow = new Workflow_Task__c();
        objWflow.Authorization__c = objAuth.id;
        objWflow.Is_violator__c = 'No';
        objWflow.Status__c = 'Not Started';
        objWflow.Program__c = 'AC';
        insert objWflow;
        //test.startTest();    

        Id InspRType = Schema.getGlobalDescribe().get('Inspection__c').getDescribe().getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        objInsp.RecordTypeId = InspRType;
        objInsp.Program__c = 'BRS';
        update objInsp;
        update objQuest;
        
        InspRType = Schema.getGlobalDescribe().get('Inspection__c').getDescribe().getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        objInsp.RecordTypeId = InspRType;
        objInsp.Program__c = 'PPQ';
        update objInsp;
        //objQuest.Program__c = 'PPQ';
        update objQuest;
        
        InspRType = Schema.getGlobalDescribe().get('Inspection__c').getDescribe().getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        objInsp.RecordTypeId = InspRType;
        objInsp.Program__c = 'VS';
        update objInsp;
        update objQuest;
     
        
        objQuest.Status__c = 'Approved';
        update objQuest;
        test.stopTest();
    }
}