/*
 * Purpose: Perform AC specific Application operations
 */ 
public with sharing class EFLACApplicationEngine 
                    implements EFLIApplicationEngine{
 
                        
     //Load Application wrapper using PSQ inputs                   
      public EFLApplicationWrapper createApplication(EFLPreScreeningWrapper psw){  
         
           return EFLApplicationEngineUtility.createApplication(psw); 

      }                   
                        
    }