@isTest(SeeAllData=true)
public class CARPOL_UNI_MasterApplicationTrigger_Test {    
    @isTest
    static void testMasterApplicationTrigger(){
        String ACAppRecordTypeId = Schema.SObjectType.Application__c
            .getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Contact objCont = testData.newContact();
        
        Application__c objApp = new Application__c(); 
        objApp.Applicant_Name__c = objCont.Id;
        objapp.Recordtypeid = ACAppRecordTypeId;
        objapp.Application_Status__c = 'Waiting on Customer';
        objApp.Share_with_Roles__c = 'BRS';
        insert objApp;
        
        AC__c lineItem = testData.newLineItemBRS('Testing Purpose', objApp);
        Authorizations__c objAuth = testData.newAuth(objApp.Id);
        Regulated_Article_Package__c regArcPackage = new Regulated_Article_Package__c();
        insert regArcPackage;
        
        lineItem.Authorization__c = objAuth.Id;
        lineItem.Line_Item_Type_Hidden_Flag__c = 'Product';
        
        lineItem.Facility_Building_Type__c = 'Institution';
        Program_Line_Item_Pathway__c plip = [Select Id, Name from Program_Line_Item_Pathway__c 
                                             where id = :lineItem.Program_Line_Item_Pathway__c];
        lineItem.Regulated_Article_PackageId__c = regArcPackage.Id;
        plip.Name = 'New Facility';
        update plip;
        update lineItem;
        WorkFlow_Task__c objWorkflowTask =  testData.newworkflowtask('WF Test', objAuth, 'Pending');
        objWorkflowTask.Status_Categories__c='Customer Feedback';
        update objWorkflowTask;

        Test.startTest();
        objapp.Application_Status__c = 'On-Hold';
        objapp.Application_Submitted__c = false;
        objapp.EFLSaveastemplate__c = false;
        update objApp;
        
        objapp.Application_Status__c = 'Withdrawn';
        update objApp;        
        Test.stopTest();
    }
    
}