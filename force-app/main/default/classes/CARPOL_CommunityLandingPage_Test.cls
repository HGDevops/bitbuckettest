/*
// Author - Vinar Amrutia
// Date Created : 30th July 2015
// Purpose - Community Landing Page Test Class
*/

@isTest(seealldata=true)
private class CARPOL_CommunityLandingPage_Test {
    
    static testMethod void communityLandingPage() {
      
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port'); 
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newlineitem('Personal Use',objapp);
        
        PageReference pageRef = Page.CARPOL_CommunityLandingPage;
        Test.setCurrentPage(pageRef);
        CARPOL_CommunitiyLandingPage commLandingPage = new CARPOL_CommunitiyLandingPage();         
        commLandingPage.editContact();
        system.assert(commLandingPage != null);
    }
}