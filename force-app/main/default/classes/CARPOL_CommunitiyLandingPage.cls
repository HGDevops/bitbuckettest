public with sharing class CARPOL_CommunitiyLandingPage {
    
    public User userDetails { get; set; }
    public Contact contactDetails { get; set; }
    public List<Application__c> myApplications { get; set; }
    public List<Authorizations__c> myAuthorizations { get; set; }
    
    //Constructor
    public CARPOL_CommunitiyLandingPage()
    {
        ID userID = UserInfo.getUserID();
        userDetails = [SELECT ContactID FROM User WHERE ID = : userID LIMIT 1];
        System.Debug('<<<<<<< userDetails ' + userDetails + ' >>>>>>>');
        if(userDetails.ContactID != null)
        {
            contactDetails = [SELECT ID, FirstName, LastName, Account.Name, Phone, Email, Additional_Email__c, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode FROM Contact WHERE ID = : userDetails.ContactID LIMIT 1];
        }
        else
        {
            // For testing purpose using a dummy contact
            //userID = '005r0000000Eevk';
            userDetails = [SELECT Id, ContactID, Name, Profile.UserLicense.Name FROM User WHERE Profile.Name IN ('APHIS Applicant','eFile Applicant') AND ContactId != null LIMIT 1];            
//            userDetails = [SELECT Id, ContactID, Name, Profile.UserLicense.Name FROM User WHERE Profile.UserLicense.Name = 'Customer Community Login' AND ContactId != null LIMIT 1];
            contactDetails = [SELECT ID, FirstName, LastName, Account.Name, Phone, Email, Additional_Email__c, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode FROM Contact WHERE ID = : userDetails.ContactID LIMIT 1];
        }
        System.Debug('<<<<<<< contactDetails ' + contactDetails + ' >>>>>>>');
        
        myApplications = [SELECT ID, Name, CreatedDate, Application_Status__c, RecordType.Name FROM Application__c WHERE Applicant_Name__c = :contactDetails.ID ORDER BY CreatedDate DESC LIMIT 999];
        
        myAuthorizations = [SELECT ID, Name, CreatedDate, Status__c, RecordType.Name FROM Authorizations__c WHERE Authorized_User__c = :contactDetails.ID ORDER BY CreatedDate DESC LIMIT 999];
    }
    
    public PageReference editContact() {
        
        PageReference editContact = new PageReference('/' + contactDetails.ID + '/e');
        editContact.setRedirect(true);
        return editContact;
    }
}