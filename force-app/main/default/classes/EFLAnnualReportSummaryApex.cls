public without sharing class EFLAnnualReportSummaryApex {
    @AuraEnabled public static EFLAnnual_Report__c getReportForView(Id arId){
        system.debug([SELECT Id, EFL_Locked__c FROM EFLAnnual_Report__c WHERE Id = :arId]);
        return [SELECT Id, EFL_Locked__c FROM EFLAnnual_Report__c WHERE Id = :arId];
    }
    
    @AuraEnabled
    public static EFLAnnual_Report__c fetchFacilities(String annualReportId){
        EFLAnnual_Report__c annualReport = [Select ID,Status__c,EFL_Locked__c, Name, EFLSites__c, EFLRegistration__c
                                            , EFLSigner_Certified__c, EFLSigner_Agreed__c,EFLSigner_Selected_Email__c 
                                            , EFL_Using_Federal_Funds__c, EFL_Change_In_Ownership__c
                                            FROM EFLAnnual_Report__c 
                                            WHERE Id=:annualReportId];
        return annualReport;
    }
    
    @AuraEnabled
    public static EFLSpringURL__mdt getSpringCMUrl(String orgName){
        EFLSpringURL__mdt springUrl = [Select Id,SpringCM_URL_Generate__c,SpringCM_URL_Sign__c,suffix__c FROM EFLSpringURL__mdt WHERE Org_Name__c=:orgName];
        return springUrl;
    }
    
    
    @AuraEnabled
    public static List<EFLRegistered_Animal__c> fetchAnimalUsage(String annualReportId){
        List<EFLRegistered_Animal__c> allSelectedAnimals = new List<EFLRegistered_Animal__c>();
        allSelectedAnimals = [Select ID,EFLAnimal__c,EFLColumnBHeldNotUsed__c,EFLColumnCUsedPainMinimized__c,
                                                    EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,
                                                    EFLTotalAnimals__c,EFLSequenceForSpringCM__c, EFLAnimalName__r.Name
                                                    FROM EFLRegistered_Animal__c WHERE EFLAnnual_Report__c=:annualReportId
                                                    AND ((EFLSelectedForReport__c = true AND EFLAnimal__c = 'Other Animals')
                                                    OR (EFLAnimal__c != 'Other Animals'))
                                                    ORDER BY EFLSequence__c     ASC];

     return allSelectedAnimals;
    }
    
    /*@AuraEnabled
    public static List<Contact> fetchContacts(String searchParam){
        String cuurentUserConId = [Select ContactId FROM User WHERE Id=:UserInfo.getUserId()].ContactId;
        String accountId = [Select AccountId FROM Contact WHERE ID=:cuurentUserConId].AccountId;
        String query = 'SELECT Id,Name FROM Contact WHERE AccountId=:accountId AND EFL_Role__c=\'CEO\' AND Name LIKE \'%'+searchParam+'%\'';
        List<Contact> conList = Database.query(query);
        return conList;
    }*/
  
    @AuraEnabled
    public static void updateARStatus(String annualReportId){
        EFLAnnual_Report__c annualReport = [Select ID, Status__c FROM EFLAnnual_Report__c WHERE Id=:annualReportId];
        annualReport.Status__c = 'Ready for CEO/IO Signature';
        update annualReport;
    }

    @AuraEnabled
    public static String getConType(){
    Id conId = [select id,contactId from user where Id=:Userinfo.getUserId()].contactId; 
     contact con = [select Id,firstName,lastName,account.name, EFL_Role__c from contact where Id=:conId];  
     return con.EFL_Role__c;
    } 
    
    @AuraEnabled    
    public static void updateARContact (string annualReportId) {
     EFLAnnual_Report__c annualReport = [Select ID, EFLSigner_Agreed__c , Status__c,EFLContact__c, EFLAccount__c FROM EFLAnnual_Report__c WHERE Id=:annualReportId];
    Id jitconId = [select contactId from user where id =:userInfo.getUserId()].contactId;       
    //Id conId = [select id from contact where EFL_ACIS_Contact__c = '' and firstname=:userInfo.getFirstName() 
      //          and lastname=:userInfo.getLastName() and accountid=:annualReport.EFLAccount__c].Id;
    Id acisConId = [select EFL_ACIS_Contact__c from contact where id=:jitconId].EFL_ACIS_Contact__c;
    annualReport.EFLcontact__c =acisConId;  
    annualReport.Status__c= 'Submitted';
    annualReport.EFLReport_Signed_Date__c = system.today();
    update annualReport;
    
    //Added by Conor to fire off SpringCM workflow
    List<EFLAnnual_Report__c> temp = [SELECT Id, Name FROM EFLAnnual_Report__c WHERE Id=: annualReportId];
        for(EFLAnnual_Report__c a: temp){
            system.debug('Annual Report Name' + a.Name);
        }
        if(temp.Size() > 0){
            If(!test.isRunningTest()){
             	SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), temp.get(0).getSObjectType().getDescribe().getName(), temp, 'SignerWF');   
            } 
        }  
    }
    
    @auraEnabled
    public static void updateSingerAgreed (string annualReportId,boolean signerAgreed){
        eflAnnual_report__c ar = [select Id, EFLSigner_Agreed__c from EFLAnnual_Report__c where Id =:annualReportId];
        ar.EFLSigner_Agreed__c = signerAgreed;
        update ar;
    }

/*    @auraEnabled
    public static void updateSelEmail (string annualReportId,boolean signerSelEmail){
        eflAnnual_report__c ar = [select Id, EFLSigner_Selected_Email__c from EFLAnnual_Report__c where Id =:annualReportId];
        ar.EFLSigner_Selected_Email__c = signerSelEmail;
        update ar;
    }    
    @auraEnabled
    public static void updatesignerCertified (string annualReportId,boolean signerCertified){
        System.debug('#### signerCertified = '+signerCertified);
        eflAnnual_report__c ar = [select Id, EFLSigner_Certified__c from EFLAnnual_Report__c where Id =:annualReportId];
        ar.EFLSigner_Certified__c = signerCertified;
        System.debug('#### ar.EFLSigner_Certified__c  = '+ar.EFLSigner_Certified__c );
        update ar;
    }
 */   
    @AuraEnabled public static list<Id> getParentIds(Id arId){
        list<Id> ids = new list<Id>();
        ids.add(arId);
        for(EFLRegistered_Animal__c s : [SELECT Id FROM EFLRegistered_Animal__c 
                                         WHERE EFLAnnual_Report__c = :arId 
                                         AND EFLSelectedForReport__c = TRUE
                                         AND ELFColumnEPainNotMinimized__c > 0]){
            ids.add(s.Id);
        }
        return ids;
    }
}