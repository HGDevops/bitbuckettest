public with sharing class EFLPhenotypeTriggerHandler {
    
    
    public static void ValidateRequiredFields(List<Phenotype__c> phenotypes){
        
       for(Phenotype__c phRec:phenotypes)
        {
 
            if(phRec.Phenotypic_Category__c  =='' || phRec.Phenotypic_Category__c  ==NULL) {
                phRec.Phenotypic_Category__c.adderror('You must enter a value.');
            }
            if(phRec.Phenotypic_Description__c == '' || phRec.Phenotypic_Description__c  ==NULL)
            {phRec.Phenotypic_Description__c.adderror('You must enter a value.');}  

         }        
        
    }

    //Set "Ready to Submit" flag for Constructs when a new Genotype and Construct Component is created
    public static void processConstructReadiness(List<Phenotype__c> phenotypeList){
         list<Construct__c> constructList = new list<Construct__c>();
         set<ID> constructSetIDs =  prepareConstructSetIds(phenotypeList);
       /*  list<Construct__c> inCompleteconstructList = EFLLineItemUtility.getIncompleteConstructs(constructSetIDs);
        for(Construct__c cons:inCompleteconstructList){
            if(cons.GenotypeType__r.size() == 0 && cons.PhenoTypes__r.size()>0){
                cons.Ready_to_Submit__c= true;
                constructList.add(cons); 
            } else {
                cons.Ready_to_Submit__c= false;
                constructList.add(cons); 
            }
        }*/
        constructList = EFLLineItemUtility.processIncompleteConstructs(constructSetIDs);
         if(constructList!=NULL){
             update constructList;  }  
        
        //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
        	 set<id> idConstruct = new set<id>(); 
           	 for(Phenotype__c cc : phenotypeList){
                 if (string.isNotBlank(cc.Phenotypic_Description__c)){
                     string strDesc = cc.Phenotypic_Description__c;
                     if( strDesc.indexOf('[') >  -1 && strDesc.indexOf(']') > 0 ){
                     	idConstruct.add(cc.Construct__c);
                     }
                 }
         	} 
        	list<Construct__c> lstConstructCBI = new list<Construct__c>();
            if (idConstruct.size()> 0 ){
                 lstConstructCBI = [select id, Construct_is_CBI__c from construct__C where 
                                                     id in :idConstruct];   
            }
            if (lstConstructCBI.size()> 0 )	{
                for (construct__C c :lstConstructCBI){
                    c.Construct_is_CBI__c = True; 
                    system.debug ('Phenotype Construct is CBI #### ' +   c.Construct_is_CBI__c);
                }
                
                update lstConstructCBI;
            }
        //Ravee Racharla 10/29/2018 W-026991 Update Construct_is_CBI__c to True
      }
    
    //Reset "Ready to Submit" flag for Constructs when a Genotype and Construct Components are deleted
     public static void resetConstructReadiness(List<Phenotype__c> phenotypeList){ 
         list<Construct__c> constructList = new list<Construct__c>();
         set<ID> constructSetIDs =  prepareConstructSetIds(phenotypeList);
         list<Construct__c> relatedconstructList = getIncompleteConstructs(constructSetIDs);
        for(Construct__c cons:relatedconstructList){
            if(cons.GenotypeType__r.size() == 0 && cons.PhenoTypes__r.size()==0){
                cons.Ready_to_Submit__c= false;
                constructList.add(cons); 
            }else{
                cons.Ready_to_Submit__c= true;
                constructList.add(cons); 
            }
        }
         if(constructList!=NULL){
            Update constructList;  }                
      }
   
    //Prepare incomplete constructs to be processed for flag update
    private static list<Construct__c> getIncompleteConstructs(set<ID> constructSetIDs){
        return[select Id, 
                      Ready_to_Submit__c,
               (select id 
                  from GenotypeType__r
                where Ready_to_Submit__c =: false),
               (select id 
                  from PhenoTypes__r)
               from Construct__c
                where Id in :constructSetIDs]; 
    }   
    
    //Prepare Construct set Ids from phenotype List
    private static set<ID> prepareConstructSetIds (List<Phenotype__c> phenotypeList){
         set<ID> constructSetIDs = new set<ID>();
         for(Phenotype__c cc : phenotypeList){
            constructSetIDs.add(cc.Construct__c); 
         }        
        return constructSetIDs;
    }    
    
}