public inherited sharing class EFLApplicationTriggerHandler implements EFLITrigger{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    static final string APPLICANT_TRANSFER = 'Applicant Transfer';
    Set<ID> applicationIds = (new Map<Id,Application__c>((List<Application__c>)Trigger.New)).keySet();
    
    /*
* Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        if (EFLGenericUtility.isTriggerDisabled('CARPOL_UNI_MasterApplicationTrigger'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    /*
* Constructor
*/ 
    public EFLApplicationTriggerHandler()
    {
        
    }
    
    /**
* bulkBefore
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
        
    }
    
    /**
* bulkAfter
* This method is called prior to execution of an AFTER trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkAfter()
    {
        
        list<Application__c> apps = trigger.new;
        map<Id, Application__c> oldMap = (map<Id, Application__c>)trigger.oldMap;
        list<Application__c> changedApplicantNames = new list<Application__c>();
        list<Application__c> changedSharingAccounts = new list<Application__c>();
        map<Id,Id> contactToAccountMap = new map<Id,Id>();
        for(Application__c app : apps){
            if(!string.isBlank(app.Applicant_Name__c) && app.Applicant_Name__c != oldMap.get(app.Id).Applicant_Name__c){
                contactToAccountMap.put(app.Applicant_Name__c,null);
                contactToAccountMap.put(oldMap.get(app.Id).Applicant_Name__c,null);
                changedApplicantNames.add(app);
            }
            if(app.Sharing_Account__c != oldMap.get(app.Id).Sharing_Account__c){
                changedSharingAccounts.add(app);
            }
        }
        
        for(Contact c : [SELECT Id, AccountId FROM Contact WHERE Id IN :contactToAccountMap.keyset()]){
            contactToAccountMap.put(c.Id, c.AccountId);
        }
        set<Id> contactIds = new set<Id>();
        for(Application__c app: changedApplicantNames){
            contactIds.add(app.Applicant_Name__c);
            contactIds.add(oldMap.get(app.Id).Applicant_Name__c);
        }
        map<Id, User> userMap = new map<Id, User>();
        for(User u : [SELECT Id, ContactId FROM User WHERE ContactId IN :contactIds]){
            userMap.put(u.ContactId, u);
        }
        for(Application__c app: changedApplicantNames){
            set<Id> appIds = new set<Id>();
            appIds.add(app.Id);
            try{
                user oldApplicant = userMap.get(oldMap.get(app.Id).Applicant_Name__c);
                user newApplicant = userMap.get(app.Applicant_Name__c);
                EFLContactUtility.transferOwnership(
                    oldApplicant, 
                    newApplicant, 
                    APPLICANT_TRANSFER, 
                    appIds);
            }catch(exception e){
                system.debug(e.getMessage());
            }
        }
        
        system.debug('AFTER TRIGGER FOR APPLICATION OBJECT');
        
        if(!changedSharingAccounts.isEmpty()){
            EFLBRSApplicationEngine engine = (EFLBRSApplicationEngine)EFLApplicationEngineFactory.getApplicationEngineFromProgramName('BRS');
            engine.shareApplicationToAccount(changedSharingAccounts);
        }
    }
    
    
    /**
* beforeInsert
* This method is called iteratively for each record to be inserted during a BEFORE
* trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
*/
    public void beforeInsert(SObject so)
    {
    }
    
    /**
* beforeUpdate
* This method is called iteratively for each record to be updated during a BEFORE
* trigger.
*/
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
* beforeDelete
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete(SObject so)
    {
        
    }
    
    /**
* afterInsert
* This method is called iteratively for each record inserted during an AFTER
* trigger. Always put field validation in the 'After' methods in case another trigger
* has modified any values. The record is 'read only' by this point.
*/ 
    public void afterInsert(SObject so)
    {
        
    }
    
    /**
* afterUpdate
* This method is called iteratively for each record updated during an AFTER
* trigger.
*/
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
* afterDelete
* This method is called iteratively for each record deleted during an AFTER
* trigger.
*/ 
    public void afterDelete(SObject so){
    }
    
    /**
* andFinally
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    public void andFinally()
    {
        
    }
}