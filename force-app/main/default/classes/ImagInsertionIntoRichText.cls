public class ImagInsertionIntoRichText
{

public String naam{get;set;}
public ID folderid{get;set;}
public Blob file{get;set;}

public void insrt()
{

Document d = new Document();

d.name = naam;
d.body = file; // body field in document object which holds the file.
d.folderid= UserInfo.getUserID(); //folderid where the document will be stored insert d;
insert d;

AC__c cs = new AC__c();
cs = [SELECT ID, Name, Picture__c FROM AC__c WHERE ID = 'a00r0000000w8TC' LIMIT 1];
cs.Picture__c = '<img src="https://rustagiankit-developer-edition--c.ap1.content.force.com/servlet/servlet.FileDownload?file='+d.id+'" width="500" height="281"></img>';
update cs;

}
}