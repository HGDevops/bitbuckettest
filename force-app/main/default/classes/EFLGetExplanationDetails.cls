public class EFLGetExplanationDetails {
@auraEnabled
       public static String getAnimals(String RegisteredAnimalId){
        EFLRegistered_Animal__c animal = [Select ID,EFLAnimal__c,EFLAnnual_Report__c,
                                                     EFLColumnBHeldNotUsed__c,EFLColumnCUsedPainMinimized__c,
                                                     EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,
                                                     EFLOtherAnimal__c FROM EFLRegistered_Animal__c WHERE
                                                     Id =: RegisteredAnimalId];
         return animal.EFLAnimal__c;
    }
}