@isTest
public class EFLAR_RelatedListSecurityUtil_Test {
    
    @TestSetup
    static void makeData(){
        Account account = new Account();
        account.Name = 'Test Account';
        insert account;
        
        Contact acisContact = new Contact();
        acisContact.LastName = 'ACIS';
        acisContact.AccountId = account.Id;
        acisContact.EFL_Facility_Admin__c = true;
        insert acisContact;
        
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.AccountId = account.Id;
        contact.EFL_ACIS_Contact__c = acisContact.Id;
        contact.EFLJIT_Contact__c = true;
        insert contact;
    }

    @isTest
    public static void testAsCommunityUser_DataReturned(){
        Contact contact = [Select Id From Contact WHERE LastName = 'Test' Limit 1];
        User communityUser = getARGuestUser();
        
        user me = null;
        System.runAs(communityUser) {
	        me = EFLAR_RelatedListSecurityUtil.getMe();
        }
        System.assertEquals(communityUser.Id, me.Id);
        System.assertEquals(communityUser.ContactId, me.ContactId);
        
        Boolean canDelete = EFLAR_RelatedListSecurityUtil.canDelete(contact, me, false);
        System.assertEquals(false, canDelete);
        
        Boolean canEdit = EFLAR_RelatedListSecurityUtil.canEdit(contact, me, false);
        System.assertEquals(false, canEdit);
    }
    
    @isTest
    public static void testAsInternalUser_DataReturned(){
        Contact contact = [Select Id From Contact WHERE LastName = 'Test' Limit 1];
        
        user me = EFLAR_RelatedListSecurityUtil.getMe();

        System.assertEquals(UserInfo.getUserId(), me.Id);
        System.assertEquals(null, me.ContactId);
        
        Boolean canDelete = EFLAR_RelatedListSecurityUtil.canDelete(contact, me, true);
        System.assertEquals(true, canDelete);
        
        Boolean canEdit = EFLAR_RelatedListSecurityUtil.canEdit(contact, me, true);
        System.assertEquals(true, canEdit);
    }

    @isTest
    public static void testAsFacilityAdmin_DataReturned(){
        Contact contact = [Select Id From Contact WHERE LastName = 'Test' Limit 1];
        Contact cTest = new Contact();
        
        User communityUser = getARGuestUser();
        
        // Make the community user facility administrator
        User adminUser = new User(Id = UserInfo.getUserId());
        System.runAs(adminUser) {
            cTest.LastName = 'CTest';
            cTest.AccountId = [SELECT Id FROM Account LIMIT 1].Id;
            cTest.EFL_Facility_Admin__c = true;
            insert cTest;
            
            Contact fa = [Select Id, EFL_Facility_Admin__c From Contact Where Id = :communityUser.ContactId Limit 1];
            fa.EFL_ACIS_Contact__c = cTest.Id;
            update fa;
        }
        cTest = [SELECT Id FROM Contact WHERE LastName = 'CTest' LIMIT 1];
        
        user me = null;
        System.runAs(communityUser) {
	        me = EFLAR_RelatedListSecurityUtil.getMe();
        }
        System.assertEquals(communityUser.Id, me.Id);
        System.assertEquals(communityUser.ContactId, me.ContactId);
        System.assertEquals(true, me.Contact.EFL_ACIS_Contact__r.EFL_Facility_Admin__c);
        
        Boolean canDelete = EFLAR_RelatedListSecurityUtil.canDelete(contact, me, false);
        System.assertEquals(true, canDelete);
        
        Boolean canEdit = EFLAR_RelatedListSecurityUtil.canEdit(contact, me, false);
        System.assertEquals(true, canEdit);
    }

    @isTest
    public static void deleteOwnContact_DeletNotAllowed(){
        Contact contact = [Select Id From Contact WHERE LastName = 'Test' Limit 1];
        User communityUser = getARGuestUser();
        
        // Set ACIS Contact for the community user
        User adminUser = new User(Id = UserInfo.getUserId());
        System.runAs(adminUser) {
            Contact acisContact = [Select Id, EFL_ACIS_Contact__c From Contact Where Id = :communityUser.ContactId Limit 1];
            acisContact.EFL_ACIS_Contact__c = contact.Id;
            update acisContact;
        }
        
        user me = null;
        System.runAs(communityUser) {
	        me = EFLAR_RelatedListSecurityUtil.getMe();
        }
        System.assertEquals(communityUser.Id, me.Id);
        System.assertEquals(communityUser.ContactId, me.ContactId);
        System.assertEquals(contact.Id, me.Contact.EFL_ACIS_Contact__c);
        
        Boolean canDelete = EFLAR_RelatedListSecurityUtil.canDelete(contact, me, true);
        System.assertEquals(false, canDelete);
        
        Boolean canEdit = EFLAR_RelatedListSecurityUtil.canEdit(contact, me, false);
        System.assertEquals(false, canEdit);
    }
    
    private static User getARGuestUser(){
        return EFLACTestServices.getCommunityUser('ACIS_Community');
       // return EFLACTestServices.getCommunityUser('AnnualReports');
    }    
}