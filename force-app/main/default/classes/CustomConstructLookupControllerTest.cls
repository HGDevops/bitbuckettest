@isTest(seealldata=false)
private class CustomConstructLookupControllerTest {
    @IsTest static void CustomConstructLookupControllerTest() {
        PageReference pr = Page.CustomConstructLookup;
        pr.getParameters().put('lksrch','test');
        pr.getParameters().put('authId','');
        Test.setCurrentPage(pr);
        CustomConstructLookupController cont = new CustomConstructLookupController();
        cont.search();
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        testDataBRS.insertcustomsettings();
        
        Application__c app = new Application__c();
        app = testDataBRS.newapplication();
        Authorizations__c auth = new Authorizations__c();
        auth = testDataBRS.newAuth(app.id);
        
        AC__C lineitem = new AC__C();
        lineitem = testDataBRS.newLineItem('Resale/Adoption', app);
        lineitem.Authorization__c = auth.id;
        update lineitem;
        
        Regulated_Article__c regArt = testDataBRS.newRegulatedArticle(auth.Program_Pathway__c);
        Link_Regulated_Articles__c linkRA = testDataBRS.newlinkRegArticle(lineitem.Id, app.Id, auth.Id);
        //linkRA.Line_Item__c = lineitem.id;
        linkRA.Regulated_Article__c = regArt.id;
        upsert linkRA;
        
        Construct__c objcaj = new Construct__c();
        objcaj = testDataBRS.newconstruct(lineitem.id, regArt);
        
        GenotypeType__c genoTypeType = new GenotypeType__c();
        Genotype__c geno = new Genotype__c();
        genoTypeType.Construct__c = objcaj.id;
        genoTypeType.Genotype_Category__c = 'Gene Knock-Out';
        genoTypeType.Ready_to_Submit__c = false;
        genoTypeType.Count_of_Construct_Components__c = 1;
        insert genoTypeType;    
        geno = testDataBRS.newgenotype(objcaj.id, genoTypeType);
        
        Phenotype__c pheno = new Phenotype__c();
        pheno = testDataBRS.newphenotype(objcaj.id);
        
        List<Construct__c> constructList = new List<Construct__c>();
        constructList.add(objcaj);
        
        
        Construct__c newConstruct = new Construct__c(Construct_s__c='ConstructForControllerTestClass',Line_Item__c=lineitem.Id,Status__c='Waiting on Customer');
        newConstruct.Link_Regulated_Article__c = regArt.id;
        newConstruct.Mode_of_Transformation__c = 'Direct Injection';
        cont.construct = newConstruct;
        cont.results = new List<Construct__c>();
        cont.searchString = '';
        cont.authId = auth.id;
        cont.genoTypesByConstruct = new Map<ID, Map<string, List<Genotype__c>>>();
        cont.genoTypesByRevConstruct = new Map<Id, Map<String, List<Genotype__c>>>();
        cont.phenoTypeByConMap = new Map<Id, List<Phenotype__c>>();
        cont.lineitemid = lineitem.id;
        cont.Linelst = new List<AC__c>();
        //cont.size = 0;
        //cont.noOfRecords = 0;
        //cont.paginationSizeOptions = new List<SelectOption>();
        
        test.startTest();
        PageReference pgRef = cont.search();
        cont.searchString = 'test';  
        pgRef = cont.search();
        cont.construct.Construct_s__c='ConstructTestClass';
        cont.construct.Line_Item__c=lineitem.id;
        cont.construct.Status__c='Waiting on Customer';
        cont.construct.Revoked__c=False;
        pgRef = cont.saveConstruct();
        string tag = cont.getFormTag();
        string textBox = cont.getTextBox();
        cont.results = cont.getConstruct();
        cont.results.add(objcaj);
        cont.genoTypesByConstruct = cont.getGenoTypesForConstruct(constructList);
        // pgRef = cont.refreshPageSize();
        List<Construct__c> lstResults = new List<Construct__c>();
        // lstResults = cont.getresults();
        test.stopTest();
    }
}