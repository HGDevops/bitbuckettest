@isTest(seealldata=true)
public class CARPOL_PreviewAuthorization_Test {
    private static id olocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin');
    private static id dlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Destination');
    private static id oanddlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin and Destination');
    private static id releaserectypeid=EFLGenericUtility.getrecordtypeId('Location Release Sites');
    static List<location__c> finalLocationList = new List<Location__c>();    
    
      @IsTest static void CARPOL_PreviewAuthorization_Test() {
         
          String locRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
           String AccountRecordTypeId = testData.AccountRecordTypeId;
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Regulated_Article__c objRA = testData.newRegulatedArticle(plip.Id);
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Signature__c objTP = testData.newThumbprint();
          objTP.Regulated_Article__c = objRA.Id;
          update objTP;
          Authorizations__c objauth = testDataBRS.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          objauth.Authorization_Letter_Content__c = objcm.content__c;
          objauth.Application__c = objapp.Id;
          objauth.Thumbprint__c = objTP.Id;
          update objauth;
          
        id p=[select id from Profile where Name IN ('APHIS Applicant','eFile Applicant') Limit 1].Id;   
         User usershare = new User();
         usershare.Username ='aphistestemail@test.com';
         usershare.LastName = 'APHISTestLastName';
         usershare.Email = 'APHISTestEmail@test.com';
         usershare.alias = 'APHItest';
         usershare.TimeZoneSidKey = 'America/New_York';
         usershare.LocaleSidKey = 'en_US';
         usershare.EmailEncodingKey = 'ISO-8859-1';
         usershare.ProfileId = p;
         usershare.LanguageLocaleKey = 'en_US';
         usershare.ContactId = objcont.id;
         insert usershare;  
         
         Test.startTest();     
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          Workflow_Task__c objwft1 = testdata.newworkflowtask('Test', objauth, 'Pending', usershare.id);
          //Link_Regulated_Articles__c testlinkregarticles = testdata.linkregarticles(li.id);
          Reviewer__c testreviewer = testdata.reviewer(objauth.id, objcont.id);
          attachment attch = new attachment();
          attch.ParentID = objwft.id;
          attch.name = 'Draft Authorization Letter_'+System.TODAY().format();
          attch.body = blob.valueof('Test Body 1');
          insert attch;
          /*attachment attch1 = new attachment();
          attch1.ParentID = objwft.id;
          attch1.name = 'Draft Authorization Letter_'+System.TODAY().format();
          attch1.body = blob.valueof('Test Body 2');
          insert attch1;
           */
         setupLocations(li);
         
          //Test.startTest(); 
            PageReference pageRef = Page.CARPOL_PreviewAuthorization;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              ApexPages.currentPage().getParameters().put('tId',objwft.id);
              ApexPages.currentPage().getParameters().put('wfid',objwft.id);
              
              CARPOL_PreviewAuthorization extclass = new CARPOL_PreviewAuthorization(sc);
             // extclass.wtaskowner= usershare;
              extclass.populateTemplate(); 
              extclass.cancel(); 
              extclass.attachLetter();
              //extclass.attachDraftLetter();
          Test.stopTest();  
            extclass.saveDraft();
                
           extclass.previewTemplate();
              extclass.reviewerComments = '';
              extclass.selectedTemplateType='test'; 
              extclass.selectedTemplate='test'; 

          

      }
    
    //Prepare all record type locations
    private static void setupLocations(ac__c lineItem) { 
    
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;          
          
          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          l2.Level_2_Region_Status__c = 'Active';
          insert l2;
          
          finalLocationList.add(createLocation(c.id,lr.id,l2.id,lineItem.Id,olocrectypeid));
          finalLocationList.add(createLocation(c.id,lr.id,l2.id,lineItem.Id,dlocrectypeid));
          finalLocationList.add(createLocation(c.id,lr.id,l2.id,lineItem.Id,oanddlocrectypeid));
          finalLocationList.add(createLocation(c.id,lr.id,l2.id,lineItem.Id,releaserectypeid));
        
    }
   // Create location records 
    private static location__c createLocation(id countryID,id level1regionID,id level2regionID,id lineItemID,id recordTypeID){
          Location__c objLocation = new Location__c(Name='testloc',Inspected_by_APHIS__c='yes', Country__c=countryID, state__C= level1regionID,Zip__c='92505',
             Contact_Name1__c = 'Test',
             Primary_Contact_Last_Name__c = 'LName',
             Day_Phone__c = '(555) 555-1212',
             Level_2_Region__c = level2regionID, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             recordTypeId=recordTypeID,
             line_item__c = lineItemID);
            insert objLocation; 
        return objLocation;
        
    }
    
    
   
    

}