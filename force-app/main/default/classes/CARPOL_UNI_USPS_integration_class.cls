public inherited sharing class CARPOL_UNI_USPS_integration_class {
    
   @future (callout=true)
 
    public static void validate(String mailingStreet,String cityName,String zipcode,String st,ID recID) {
        try{
            String stateCode = [SELECT Level_1_Region_Code__c FROM Level_1_Region__c where id=:st].Level_1_Region_Code__c;
            
            String address1 = mailingStreet;
            String address2 = '';
            String city = cityName;
            String state = stateCode;
            String zip5 = zipcode;
            String zip4 = '';
            boolean addressValidated = false;
            String gentext = '';
            String addressupdated1 = '';
            String addressupdated2 = '';
            String cityupdated = '';
            String stateupdated = '';
            String zipupdated5 ='';
            String zipupdated4 = '';
        
            String  USPS_UID ='403SDE006350';   
            String BaseURL = 'http://production.shippingapis.com/ShippingAPI.dll?API=Verify&XML=';
            String MessageBody = '<AddressValidateRequest USERID="' + USPS_UID + '"><Address ID="0">';      
            MessageBody += '<Address1>'+address1+'</Address1>';
            MessageBody += '<Address2>'+address2+'</Address2>';
            MessageBody += '<City>'+city+'</City>';
            MessageBody += '<State>'+state+'</State>';
            MessageBody += '<Zip5>'+zip5+'</Zip5>';
            MessageBody += '<Zip4>'+zip4+'</Zip4>';     
            MessageBody += '</Address></AddressValidateRequest>';
            
            MessageBody = EncodingUtil.urlEncode(MessageBody, 'UTF-8');     
            MessageBody = BaseURL + MessageBody; 
            HttpRequest USPSRequest = new HttpRequest();
            Http USPSHttp = new Http();             
            USPSRequest.setMethod('GET');           
            USPSRequest.setEndpoint(MessageBody );
            HttpResponse USPSResponse = USPSHttp.send(USPSRequest);
            MessageBody = USPSResponse.getBody();  
            //system.debug('----------USPSResponse '+USPSResponse );
            //system.debug('----------MessageBody '+MessageBody );
    
            Dom.Document doc = USPSResponse.getBodyDocument();
            dom.XmlNode xroot = doc.getrootelement();
            dom.XmlNode [] xrec = xroot.getchildelements() ;
    
            for(Dom.XMLNode child : xrec){
                for (dom.XmlNode awr : child.getchildren() ) {  
                // ----------response from USPS-------------//
                //If Error in Address
                 if (awr.getname() == 'Error') {
                   for (dom.XmlNode err : awr.getchildren() ) { 
                        if (err.getname() == 'Description') { 
                            system.debug('Error: ' + err.gettext());
                        }
                        addressValidated = false;
                        gentext=awr.getname();
                   }
                 }
                 else{
                    
                    addressValidated = true;
                    if (awr.getname() == 'Address1') {addressupdated1=awr.gettext(); }                           
                    if (awr.getname() == 'Address2') { addressupdated2 = awr.gettext(); }   
                    if (awr.getname() == 'City') { cityupdated =awr.gettext(); }
                    if (awr.getname() == 'State') {stateupdated= awr.gettext();}
                    if (awr.getname() == 'Zip5') { zipupdated5 = awr.gettext();}
                    if (awr.getname() == 'Zip4') {zipupdated4 = awr.gettext();}                
                }
            
                }
            }
            Applicant_Contact__c associatedContact = new Applicant_Contact__c();
            associatedContact = [select ID,Address_Verified__c, Mailing_Street__c, Mailing_City__c, Mailing_Zip_Postal_Code__c, Mailing_State_LR__c FROM Applicant_Contact__c Where id =:recID];
            if(addressValidated == true){
            
                associatedContact.Mailing_Street__c = addressupdated2+'. '+addressupdated1;
                associatedContact.Mailing_City__c = cityupdated;
                associatedContact.Mailing_State_LR__c = [SELECT ID,Name,Level_1_Region_Code__c FROM Level_1_Region__c WHERE Level_1_Region_Code__c=:stateupdated LIMIT 1].id;
                associatedContact.Mailing_Zip_Postal_Code__c = zipupdated5+'-'+zipupdated4;
                associatedContact.Address_Verified__c = addressValidated;
            }
            if(gentext!=null){associatedContact.Gen_text__c = gentext;}
            update associatedContact;
        }catch(Exception ex){
        }
    }
}