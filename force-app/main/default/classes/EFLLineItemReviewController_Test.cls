@isTest
public class EFLLineItemReviewController_Test {
    static Construct__c objconst;
    static Construct_Application_Junction__c objcaj;
    static list<Phenotype__c> PhenoList;
    static list<Genotype__c> GenoList;
    
    @testsetup
    static void preparetestData(){
        string dlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        string locrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        string oanddlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();

        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        AC__c ac1 = testData.newLineItem('Personal Use',objapp); 
        ac1.Authorization__c = objauth.id;
        ac1.Release_Start_Date__c = system.today();
        ac1.Release_End_Date__c = system.today()+360;
        update ac1;
        Link_Regulated_Articles__c linkra=testdata.newlinkRegArticle(ac1.id,objapp.Id,objauth.id);
        linkra.status__c = 'Submitted';
        update linkra;
        Program_Line_Item_Pathway__c plip =testdata.newCaninePathway(); 
        Regulated_Article__c ra = testdata.newRegulatedArticle(plip.id);
        
       // construct__c objconst=testdata.newconstruct(ac1.id);
        string articleID = (string)(linkra.id);
        articleID  = articleID.substring(0,15);
        Construct__c objconst=new  Construct__c(Construct_s__c='Construct Test',Line_Item__c=ac1.id,Status__c='Waiting on Customer',Link_Regulated_Article__c=ra.id,Mode_of_Transformation__c='Direct Injection');
        insert objconst;
        
        GenotypeType__c objgtt= new GenotypeType__c();
        objgtt.Construct__c =  objconst.id;
        objgtt.Genotype_Category__c = 'Gene Knock-Out';
        insert objgtt;
        
        Genotype__c objgeno=testdata.newgenotype(objconst.id,objgtt);
        Phenotype__C objpheno=testdata.newphenotype(objconst.id);
        
        Country__c UScountry=testdata.newcountryus();
        Level_1_Region__c level1reg=testdata.newlevel1region(UScountry.id);
        Level_2_Region__c level2reg=testdata.newlevel2region(level1reg.id);
        location__c orgloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,olocrectypeid);
        location__c desloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,dlocrectypeid);
        location__c relloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,locrectypeid);
        location__c oanddloc = testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,oanddlocrectypeid); 
        
        Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
        objcaj.Line_Item__c = ac1.id;
        objcaj.Application__c = objapp.id;
        insert objcaj;
        Attachment objattach = testData.newAttachment(objcaj.id);
    }
    
    static testmethod void testCon(){
       preparetestData(); 
      Test.startTest();
      PageReference pageRef = Page.EFLLineItemEdit;
      Test.setCurrentPage(pageRef);
      Authorizations__c au = [select id,name from Authorizations__c limit 1][0];
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(au);
      ApexPages.currentPage().getParameters().put('Id',au.id);
      EFLLineItemReviewController con = new EFLLineItemReviewController(sc);
  //    EFLLineItemReviewController.PreConstructWrapper PCW = new EFLLineItemReviewController.PreConstructWrapper(objcaj,objconst,PhenoList,objgeno);  
      boolean prevRev= con.prevReviewConst; 
      con.getRevSop();
      con.getRevCons();
      con.saveconstructs();
      con.approveALLLocations();
      con.markStatusAsReviewComplete();
      con.saveLocations();
      con.saveRAs();
      con.savelstSop();
      con.getLineItemOptions();
      con.delrecId = au.id; 
      con.saveprevrop();
      con.save();
      con.saveattach();
      con.savelineitem();
      con.approveALLattachments();
      con.approveall();
      con.approveallprevsop();
      con.redirect();
      con.saveArticleSuppliers();
    //  con.deleteRecord();
      Test.stopTest();      
    }
    static testmethod void testCon1(){
       preparetestData(); 
      Test.startTest();
      PageReference pageRef = Page.EFLLineItemEdit;
      Test.setCurrentPage(pageRef);
      Authorizations__c au = [select id,name,Application_CBI__c from Authorizations__c limit 1][0];
      au.Application_CBI__c ='Yes';
      update au;
     
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(au);
      ApexPages.currentPage().getParameters().put('Id',au.id);
      EFLLineItemReviewController con = new EFLLineItemReviewController(sc);
      //EFLLineItemReviewController.PreConstructWrapper PCW = new EFLLineItemReviewController.PreConstructWrapper();//(objcaj,objconst,PhenoList,GenoList);  
      boolean prevRev= con.prevReviewConst; 
      con.getRevSop();
      con.getRevCons();
      con.saveconstructs();
      con.approveALLLocations();
      con.markStatusAsReviewComplete();
      con.saveLocations();
      con.saveRAs();
      con.savelstSop();
      con.getLineItemOptions();
      con.delrecId = au.id; 
      con.saveprevrop();
      con.save();
      con.saveattach();
      con.savelineitem();
      con.approveALLattachments();
      con.approveall();
      con.approveallprevsop();
      con.redirect();
      con.saveArticleSuppliers();
    //  con.deleteRecord();
      Test.stopTest();      
    }
    static testmethod void testCon2(){
       preparetestData(); 
      Test.startTest();
      PageReference pageRef = Page.EFLLineItemEdit;
      Test.setCurrentPage(pageRef);
      Authorizations__c au = [select id,name,Application_CBI__c from Authorizations__c limit 1][0];
      au.Authorization_Type__c = 'Notification';
      au.Application_CBI__c ='Yes';
      update au;
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(au);
      ApexPages.currentPage().getParameters().put('Id',au.id);
      EFLLineItemReviewController con = new EFLLineItemReviewController(sc);
  //    EFLLineItemReviewController.PreConstructWrapper PCW = new EFLLineItemReviewController.PreConstructWrapper(objcaj,objconst,PhenoList,objgeno); 
  	  Id strPathwayId = con.strPathwayId;   
      boolean aabutton = con.aabutton;
      boolean constructbutton = con.constructbutton;
      boolean sopbutton = con.sopbutton;
      boolean locationbutton = con.locationbutton;
      boolean RAbutton = con.RAbutton;
      boolean prevRev= con.prevReviewConst; 
      string redirect = con.redirect;
      string recTypeName = con.recTypeName;
      string releaselocationinfo = con.releaselocationinfo;
      string wfStatus = con.wfStatus;
	  boolean displayCompAgreementButton = con.displayCompAgreementButton;	
      string errmsg = con.errmsg;
      string sSelCat = con.sSelCat;
      con.getRevSop();
      con.getRevCons();
      con.saveconstructs();
      con.approveALLLocations();
      con.markStatusAsReviewComplete();
      con.saveLocations();
      con.saveRAs();
      con.savelstSop();
      con.getLineItemOptions();
      con.delrecId = au.id; 
      con.saveprevrop();
      con.save();
      con.saveattach();
      con.savelineitem();
      con.approveALLattachments();
      con.approveall();
      con.approveallprevsop();
      con.redirect();
      con.saveArticleSuppliers();
    //  con.deleteRecord();
      Test.stopTest();      
    } 
}