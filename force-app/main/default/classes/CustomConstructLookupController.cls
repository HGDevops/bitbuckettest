public with sharing class CustomConstructLookupController {

    public Construct__c construct        {get;set;} // new account to create
    public List<Construct__c> results    {get;set;} // search results
    public String searchString           {get;set;} // search keyword
    public String authId                 {get;set;}
    public Map<Id, Map<String, List<Genotype__c>>> genoTypesByConstruct{get;set;}
    public Map<Id, Map<String, List<Genotype__c>>> genoTypesByRevConstruct{get;set;}
    public Map<Id, List<Phenotype__c>> phenoTypeByConMap{get;set;}
    public id lineitemid {get;set;}
    public List<AC__c> Linelst{get;set;}
    public Set<Id> pscConIds {get;set;}
    
    public CustomConstructLookupController() {
        construct = new Construct__c();
        phenoTypeByConMap = new Map<Id, List<Phenotype__c>>();
        pscConIds = new Set<Id>();
        // get the current search string
        // FIXME: need to add a null check here or else this will cause an unhandled exception.
        searchString = String.escapeSingleQuotes(System.currentPageReference().getParameters().get('lksrch'));
        authId = String.escapeSingleQuotes(System.currentPageReference().getParameters().get('authId'));
        
        if(authId!=''){
           Linelst=[SELECT id,name FROM AC__c WHERE Authorization__c=:authId];
           if(Linelst.size()>0)
              lineitemid = Linelst[0].id;
         }

        genoTypesByConstruct = getGenoTypesForConstruct(getConstruct());
        runSearch();  
    }

    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }

    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 

    // run the search and return the records found. 
    private List<Construct__c> performSearch(string searchString) {
        Set<Id> lineItemsIdSet = new Set<Id>();
        if(authId != null && authId != '') {
            lineItemsIdSet = new Map<Id, AC__c>([Select id from AC__c Where Authorization__c =: authId]).keySet();
        }
        if(!String.isBlank(searchString)){
       // String soql = 'Select id, name,Link_Regulated_Article__r.Name,Link_Regulated_Article__c,Total_No_of_PhenoTypes__c from Construct__c Where';
          String query = 'SELECT ID,Name,Link_Regulated_Article__r.Name,Name__c,Construct_s__c,CreatedDate,Corrections_Required__c,Mode_of_Transformation__c,Status__c,Status_Graphical__c,Total_No_of_PhenoTypes__c,Link_Regulated_Article__c,' +  
               '(SELECT id,Name,Description__c,Construct_Component__c,Construct_Component_Text__c,Donor__C,Construct_Component_Name__c,Genotype__c,Related_Construct_Record_Number__c,Record_Type_Text__c,Donor_2__c,Donor_3__c,Donor_4__c,Donor_5__c,Donor_List__c,RecordType.Name,recordtype.Id FROM Genotypes__r order by recordtype.name, Construct_Component_Name__c),' + 
               '(SELECT id, Name, Phenotype_Category_Text__c,Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,Construct__r.Id FROM Phenotypes__r)' +
               'FROM Construct__c WHERE ';

        
            query +=  '(Mode_of_Transformation__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
            query +=  ' Regulated_Article__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
            query +=  ' Construct_s__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
            query +=  ' Name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' ) AND';
        
        	query += ' Line_Item__c =: lineitemid AND Revoked__c =FALSE';
            query += ' limit 25';
      	list<construct__c> filterConstructList = Database.Query(query);  
            query = 'SELECT ID,Name,Link_Regulated_Article__r.Name,Name__c,Construct_s__c,CreatedDate,Corrections_Required__c,Mode_of_Transformation__c,Status__c,Status_Graphical__c,Total_No_of_PhenoTypes__c,Link_Regulated_Article__c,' +  
                '(SELECT id,Name,Description__c,Construct_Component__c,Construct_Component_Text__c,Donor__C,Construct_Component_Name__c,Genotype__c,Related_Construct_Record_Number__c,Record_Type_Text__c,Donor_2__c,Donor_3__c,Donor_4__c,Donor_5__c,Donor_List__c,RecordType.Name,recordtype.Id FROM Genotypes__r order by recordtype.name, Construct_Component_Name__c),' + 
                '(SELECT id, Name, Phenotype_Category_Text__c,Phenotypic_Description__c,Phenotypic_Category__c,Construct__c,Construct__r.Id FROM Phenotypes__r)' +
                'FROM Construct__c WHERE ';

        
            query +=  '(Mode_of_Transformation__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
            query +=  ' Regulated_Article__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
            query +=  ' Construct_s__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR';
            query +=  ' Name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' ) AND';
        
        	query += ' Id In: pscConIds';
            query += ' limit 25';
            list<construct__c> filterPSCList = Database.Query(query);
            
            query  = 'select id,name,account__C, Mode_of_Transformation__c,Mode_of_Transformation_Text__c,Link_Regulated_Article__r.Name,Name__c,CreatedDate,Corrections_Required__c,Status__c,Status_Graphical__c,Total_No_of_PhenoTypes__c,Link_Regulated_Article__c,'+
                ' Construct_s__c,(SELECT ID,Phenotypic_Category__c,Phenotypic_Description__c FROM Phenotypes__r where'+
                ' Phenotypic_Category__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR' +
                ' Phenotypic_Description__c LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\')' +
                ' from Construct__c where Id In: pscConIds OR (Line_Item__c =: lineitemid AND Revoked__c =FALSE)';
            query += ' order by CreatedDate DESC LIMIT 1000';              
            system.debug('2++' +query );
            list<construct__c> filterwithPhenoConstructList = Database.Query(query);  
       
            Integer j = 0;
            while (j < filterwithPhenoConstructList.size())
            {
                if(filterwithPhenoConstructList.get(j).Phenotypes__r.size() == 0)
                {
                    filterwithPhenoConstructList.remove(j);
                }else
                {
                    j++;
                }
            }
            list<construct__c> allFilteredConstructList = new list<construct__c>();
            allFilteredConstructList.addAll(filterConstructList);
            allFilteredConstructList.addAll(filterPSCList);
            allFilteredConstructList.addAll(filterwithPhenoConstructList);
            return allFilteredConstructList;
      } else
          return getConstruct();
    }
    
    // save the new account record
    public PageReference saveConstruct() {
        insert construct;
        // reset the account
        construct = new Construct__c();
        return null;
    }

    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }

    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
     public List<Construct__c> getConstruct(){
         List<Construct__c> conListAll = new List<Construct__c>();
     //    Map<Id,Construct_Application_Junction__c> mapConAppJunc = new Map<Id,Construct_Application_Junction__c>();
         for(Construct_Application_Junction__c conAppJun: EFLBRSLineItemUtility.getRevCons(lineitemid)){
      //       mapConAppJunc.put(conAppJun.construct__c, conAppJun);
             pscConIds.add(conAppJun.construct__c);
         }
        
       //  conListAll.addAll(EFLConstructRepository.selectByIDs(mapConAppJunc.keyset()));
     //    conListAll.addAll(EFLBRSLineItemUtility.getConstruct('Line_item__c', lineitemid));*/
     	Set<id> constructIds = EFLConstructRepository.selectIDsByLineItemID(lineitemid);
        
         conListAll = EFLConstructRepository.selectByIDs(constructIds);
         return conListAll;
    }
    
    public Map<Id, Map<String, List<Genotype__c>>> getGenoTypesForConstruct(List<Construct__c> constructList){
        
        Map<Id, Map<String, List<Genotype__c>>> genotypesByConMap = new Map<Id, Map<String, List<Genotype__c>>>();
        for(Construct__c con : constructList){
            Map<String, List<Genotype__c>> genoTypesByRecordType = new Map<String, List<Genotype__c>>();
            for(Genotype__c gt : con.Genotypes__r){
                if(genoTypesByRecordType.containsKey(gt.RecordType.name)){
                    genoTypesByRecordType.get(gt.RecordType.name).add(gt);
                }else{
                    genoTypesByRecordType.put(gt.RecordType.name, new Genotype__c[]{gt});
                }
            }
            phenoTypeByConMap.put(con.id, con.phenoTypes__r);
            genotypesByConMap.put(con.id, genoTypesByRecordType);
        }
        return genotypesByConMap;
    }    
}