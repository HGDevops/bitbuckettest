public class CARPOL_UNI_ApplPDFCompController {
    public string lineitemID {get;set;}
    public string ApplicationID {get;set;}
    public string CBIcheckvalue{get;set;}
    public string logoutURL{get;set;}
    
    public CARPOL_UNI_ApplPDFCompController(){
        
        string UserProfileName = [select id, name from profile where Id =: userinfo.getProfileId()].Name;
        
         if(userinfo.getUserType() == 'PowerCustomerSuccess' && UserProfileName == 'Org Admin'){
             logoutURL = '/secur/logout.jsp';
         }else if(userinfo.getUserType() == 'PowerCustomerSuccess'){
             logoutURL = '/st/secur/logout.jsp';
         }else
          {
             logoutURL = '/secur/logout.jsp'; 
         }
         
         

    }

}