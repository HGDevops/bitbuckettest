/*
 * Purpose: Central place to access Application object from database with different set of filter parameters.
*/
public with sharing class EFLApplicationRepository {
 
 	// Select Application by Application ID
    public static Application__c selectApplicationById(Id applicationId) {
        try {
            EFLEnforceAccessUtility.checkObjectReadAccess('Application__c');
            return [SELECT Id, Name, Application_Type__c, Application_Status__c,
                           Applicant_Name__c, Applicant_Name__r.AccountId, Organization__c, Sharing_Account__c
                    FROM Application__c
                    WHERE Id = :applicationId];
        } catch(exception e) {
            EFLErrorLog.createErrorLog('EFLApplicationRepository.selectApplicationById()', e);                
        }
        return null;
    }
}