@isTest
private class EQSBatchforContactShareTest {
	static testMethod void ContactSharingTest() {
		Id recID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('APHIS EQS Responder').getRecordTypeId();
		//String entryID = recID.substring(0, recID.length()-3);
		Id accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('APHIS EQS Organization').getRecordTypeId();
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

		User u0 = new User(
		                   email = 'con1@testing123.com',
		                   profileid = p.Id,
		                   UserName = 'con22@testing.com',
		                   Alias = 'usr0',
		                   TimeZoneSidKey = 'America/New_York',
		                   EmailEncodingKey = 'ISO-8859-1',
		                   LocaleSidKey = 'en_US',
		                   LanguageLocaleKey = 'en_US',
		                   FirstName = 'Test EQS Data',
		                   LastName = 'Integrator',
		                   EQS_Employee_ID__c = '123456'
		);
		insert u0;

		User u1 = new User(
		                   email = 'con2@testing123.com',
		                   profileid = p.Id,
		                   UserName = 'con2@testing.com',
		                   Alias = 'usr1',
		                   TimeZoneSidKey = 'America/New_York',
		                   EmailEncodingKey = 'ISO-8859-1',
		                   LocaleSidKey = 'en_US',
		                   LanguageLocaleKey = 'en_US',
		                   FirstName = 'Tim',
		                   LastName = 'Bob',
		                   EQS_Employee_ID__c = '1236'
		);
		insert u1;

		User u2 = new User(
		                   email = 'con0@testing123.com',
		                   profileid = p.Id,
		                   UserName = 'con0@testing123.com',
		                   Alias = 'usr2',
		                   TimeZoneSidKey = 'America/New_York',
		                   EmailEncodingKey = 'ISO-8859-1',
		                   LocaleSidKey = 'en_US',
		                   LanguageLocaleKey = 'en_US',
		                   FirstName = 'Joe',
		                   LastName = 'Bro',
		                   EQS_Employee_ID__c = '156'
		);
		insert u2;
		Account acc = new Account(
		                          Name = 'Test Account',
		                          recordTypeId = accRecID
		);
		insert acc;

		//  acc.IsPartner = true;
		//  update acc;

		List<Contact> conList = new List<Contact> ();
		//this is the base contact, no rule should be created
		//
		Contact con0 = new Contact(
		                           FirstName = 'Con0',
		                           LastName = 'Con',
		                           AccountId = acc.Id,
		                           Phone = '5555555551',
		                           Email = 'con0@testing123.com',
		                           EQS_Employee_ID__c = '123456',
		                           RecordTypeId = recID,
		                           Ownerid = u1.id
		);
		insert con0;

		Contact con1 = new Contact(
		                           FirstName = 'Con1',
		                           LastName = 'Con',
		                           AccountId = acc.Id,
		                           Phone = '5555555551',
		                           Email = 'con1@testing123.com',
		                           EQS_Employee_ID__c = '1236',
		                           RecordTypeId = recID,
		                           Ownerid = u0.id,
		                           ReportsToId = con0.id
		);
		insert con1;

		Contact con2 = new Contact(
		                           FirstName = 'Con2',
		                           LastName = 'Con2',
		                           AccountId = acc.Id,
		                           Phone = '5555555551',
		                           Email = 'con2@testing123.com',
		                           EQS_Employee_ID__c = '156',
		                           RecordTypeId = recID,
		                           Ownerid = u0.id,
		                           ReportsToId = con0.id

		                           // User_id_used_for_sharing__c = u2.id
		);
		insert con2;

		System.debug('u0.id>>>' + u0.id);
		EQS_Responder_Training__c rtc = new EQS_Responder_Training__c(EQS_Responder__c = con2.id, OwnerId = u0.id);
		insert rtc;

		EQS_Resp_Prof_Cert__c erp = new EQS_Resp_Prof_Cert__c(EQS_Responder__c = con2.id, OwnerId = u0.id);
		insert erp;

		EQS_Resource_Request__c rfrc = new EQS_Resource_Request__c(EQS_Responder__c = con2.id, OwnerId = u0.id);
		insert rfrc;

		EQS_Responder_PPE__c rpe = new EQS_Responder_PPE__c(EQS_Responder__c = con2.id, OwnerId = u0.id);
		insert rpe;


		EQS_Responder_Medical_Clearance__c rmcc = new EQS_Responder_Medical_Clearance__c(EQS_Responder__c = con2.id, OwnerId = u0.id, EQS_Clearance_Type__c = 'Vaccination', EQS_Clearance_Requested_By__c = con1.id, EQS_APR_Denied__c = 'No', EQS_Med_Clearance_Exp_Date__c = Date.parse('12/12/2018'));
		System.debug('responder medical clearance ' + rmcc);
		insert rmcc;

		EQS_Responder_Position_Certification__c rpc = new EQS_Responder_Position_Certification__c(EQS_Responder__c = con2.id, OwnerId = u0.id);
		insert rpc;


		EQS_Responder_Respirators_Fit_Test__c rft = new EQS_Responder_Respirators_Fit_Test__c(EQS_Responder__c = con2.id, OwnerId = u0.id,EQS_Manufacturer__c = 'MOLDEX', EQS_Model__c = '2700', EQS_Mask_Style__c = 'N95');
		System.debug('eqs responder respirators fit test ' + rft);
		insert rft;

		//  EQSBatchforContactShare captureBatch = new EQSBatchforContactShare();


		Test.startTest();
		EQSBatchforContactShare captureBatch = new EQSBatchforContactShare();
		Database.executeBatch(captureBatch, 200);
		con2.ReportsToId = null;
		con2.User_id_used_for_sharing__c = u2.id;
		update con2;
		Database.executeBatch(captureBatch, 200);
		Test.stopTest();
	}
}