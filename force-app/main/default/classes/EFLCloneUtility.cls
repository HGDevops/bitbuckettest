/**
* Purpose: Handles the clone Utility functions
**/ 
public with sharing class EFLCloneUtility {
    
    //Clone Line Item and reset few fields
    public static AC__c cloneLineItem(AC__c originalLineItem, application__c applicationReference, string EXTERNALID, string ActionType,authorizations__c authRecord){
        AC__c clonedLineItem = new AC__c();
        clonedLineItem=originalLineItem.clone(false,false,false,false);
        system.debug('clonedLineItem@@@@' + clonedLineItem);  //Ravee Racharla 12/18/2018
        clonedLineItem.ExternalID__c= EXTERNALID;
        clonedLineItem.application_number__r = applicationReference;
        clonedLineItem.Parent_Line_Item__c = originalLineItem.ID;
        clonedLineItem.Authorization__c=null;
        clonedLineItem.Status__c='Saved';
        //Ravee Racharla 12/17/2018 W-026422  
        /*
if (originalLineItem.Program_Line_Item_Pathway__r.Show_Required_Documents_Button__c == true) {
clonedLineItem.Status__c='Saved';
} else {
clonedLineItem.Status__c='Draft';
}
*/ //Ravee Racharla 12/17/2018 W-026422 
        if (actiontype=='Clone Button'){
            clonedLineItem.Proposed_Start_Date__c= System.today();
            clonedLineItem.Proposed_end_date__c  = System.today().addYears(1) -1;
            clonedLineItem.Release_Start_Date__c = System.today();
            clonedLineItem.Release_end_Date__c   = System.today().addYears(1) -1; 
            //W-034721 - Rajesh Potla - 04/28/2019 - Reset the Status on LI 
            //and let Construct trigger recalculate the actual status after cloning. 
            clonedLineItem.Construct_Status__c = CARPOL_Constants.YET_TO_ADD;
        }
        if (actiontype=='Renewal'){
            clonedLineItem.Construct_Status__c = CARPOL_Constants.YET_TO_ADD;
        }
        if (actiontype=='Amendment'){
            clonedLineItem.Proposed_Start_Date__c= authrecord.Effective_Date__c;
            clonedLineItem.Proposed_end_date__c  = authrecord.Expiration_Date__c;
            clonedLineItem.Release_Start_Date__c = authrecord.Effective_Date__c;
            clonedLineItem.Release_end_Date__c   = authrecord.Expiration_Date__c; 
        }
        clonedLineItem.skip_validation__c    = true;
        clonedLineItem.Cloned_Line_Item__c   = true;
        clonedLineItem.Locked__c ='No';
        // clonedLineItem.Certification__c ='--None--';
        clonedLineItem.EFLPermittedMaterialDescription__c = null;
        return clonedLineItem;
    }
    
    //Clone Link Regulated Article  
    public static list<Link_Regulated_Articles__c> cloneLinkRegulatedArticle(ac__c originalLineItem, ac__c lineItemReference, string ActionType){
        
        list<Link_Regulated_Articles__c> originalLinkRegulatedArticleList = EFLLinkRegulatedArticleRepository.selectByLineItemID(originalLineItem.ID);
        system.debug('originalLinkRegulatedArticleList###'+originalLinkRegulatedArticleList);
        list<Link_Regulated_Articles__c> clonedLinkRegulatedArticleList = new list<Link_Regulated_Articles__c>(); 
        
        for(Link_Regulated_Articles__c linkRA : originalLinkRegulatedArticleList){
            
            Link_Regulated_Articles__c newlinkRA = new Link_Regulated_Articles__c();
            newlinkRA = linkRA.clone(false,false,false,false);  
            newlinkRA.Line_Item__r = lineItemReference;
            system.debug('newlinkRA@@@@'+newlinkRA);
            system.debug('newlinkRA.Line_Item__r@@@@'+newlinkRA.Line_Item__r);
            if (actiontype=='Clone Button'){
                newlinkRA.Status__c='Draft'; 
            }
            
            clonedLinkRegulatedArticleList.add(newlinkRA); 
        }
        
        return clonedLinkRegulatedArticleList;
    }  
    
    
    //Clone Construct 
    public static list<Construct_Application_Junction__c> cloneConstruct(ac__c originalLineItem, ac__c lineItemReference, string ActionType ){
        
        Set<ID> originalConstructIDs = EFLConstructRepository.selectIDsByLineItemID(originalLineItem.ID);
        List<Construct_Application_Junction__c> prevReviewedConstructList = new  List<Construct_Application_Junction__c>();
        
        for(ID conID : originalConstructIDs){
            Construct_Application_Junction__c newPrevReviewedConstruct = new  Construct_Application_Junction__c();
            newPrevReviewedConstruct.line_Item__r = lineItemReference;
            newPrevReviewedConstruct.Construct__c = conID;
            if (actiontype=='Amendment' || actiontype == 'Renewal'){
                newPrevReviewedConstruct.Status__c='Review Complete';
            }
            prevReviewedConstructList.add(newPrevReviewedConstruct);
        }
        return prevReviewedConstructList;
    }  
    
    //Clone Location This method is not used. New Method below: cloneLocationAndDetailObjects 
    public static list<Location__c> cloneLocation(ac__c originalLineItem, ac__c lineItemReference ){
        list<Location__c> originalLocationList = EFLLocationRepository.selectByLineItemIDforClone(originalLineItem.ID);
        list<Location__c> clonedLocationList = new list<Location__c>(); 
        
        for(Location__c loc : originalLocationList){
            Location__c newLocation = new Location__c();
            newLocation = loc.clone(false,true,false,false);  
            newLocation.Line_Item__r = lineItemReference;
            newLocation.Parent_Location__c = loc.ID;
            system.debug('Original Location' + loc.id  ); 
            system.debug('Parent Location' + newLocation.Parent_Location__c  ); 
            
            clonedLocationList.add(newLocation); 
        }
        return clonedLocationList;
    } 
    
    //DeepClone Location Ravee Racharla 12/19/2018
    // W-36122. Ravee Racharla. moving soql queries out of for loop to avoid soql 101
    public static list<sObject> cloneLocationAndDetailObjects(ac__c originalLineItem, ac__c lineItemReference, string ActionType ){
        list<sObject> sObjectLoc  = new list<sObject>();
        list<Location__c> originalLocationList = EFLLocationRepository.selectByLineItemIDforClone(originalLineItem.ID);
        list<Location__c> clonedLocationList = new list<Location__c>(); 
        //GPS_Coordinate__c  Material__c  Related_Contact__c
        list<GPS_Coordinate__c> clonedGPSList = new list<GPS_Coordinate__c>(); 
        list<Material__c> clonedMaterialList = new list<Material__c>(); 
        list<Related_Contact__c> clonedContactList = new list<Related_Contact__c>(); 
        
        //Set of loc ids used to query contacts, gps co-ords and materials
        Set<Id> locationsSet = new Set<Id>();
        for (Location__c loc : originalLocationList){
            locationsSet.add(loc.Id);
        }
        
        //A map for locations' related contacts
        Map<Id, List<Related_Contact__c>> locationsAndRelatedContactsMap = new Map<Id, List<Related_Contact__c>>();
        for(Related_Contact__c relContact : EFLLocationRepository.selectContactsByLocationsforClone(locationsSet)){
            if(locationsAndRelatedContactsMap.ContainsKey(relContact.Location_ID__c)){
                List<Related_Contact__c> relContactList = locationsAndRelatedContactsMap.get(relContact.Location_ID__c);
                relContactList.add(relContact);
                locationsAndRelatedContactsMap.put(relContact.Location_ID__c, relContactList);
            }
            else
                locationsAndRelatedContactsMap.put(relContact.Location_ID__c, new List<Related_Contact__c>{relContact});
             
        }
        
        //A map for locations'  gps coordinates
        Map<Id, List<GPS_Coordinate__c>> locationsAndGPSMap = new Map<Id, List<GPS_Coordinate__c>>();
        for(GPS_Coordinate__c gpsCoordiante : EFLLocationRepository.selectGPSByLocationsforClone(locationsSet)){
            if(locationsAndGPSMap.ContainsKey(gpsCoordiante.Location_ID__c)){
                List<GPS_Coordinate__c> gpsCoordinateList = locationsAndGPSMap.get(gpsCoordiante.Location_ID__c);
                gpsCoordinateList.add(gpsCoordiante);
                locationsAndGPSMap.put(gpsCoordiante.Location_ID__c, gpsCoordinateList);
            }
            else
                locationsAndGPSMap.put(gpsCoordiante.Location_ID__c, new List<GPS_Coordinate__c>{gpsCoordiante});
             
        }
        
        //A map for locations'  materials
        Map<Id, List<Material__c>> locationsAndMapterialsMap = new Map<Id, List<Material__c>>();
        for(Material__c locMaterial : EFLLocationRepository.selectMaterialsByLocationsClone(locationsSet)){
            if(locationsAndMapterialsMap.ContainsKey(locMaterial.Location_ID__c)){
                List<Material__c> locMaterialsList = locationsAndMapterialsMap.get(locMaterial.Location_ID__c);
                locMaterialsList.add(locMaterial);
                locationsAndMapterialsMap.put(locMaterial.Location_ID__c, locMaterialsList);
            }
            else
                locationsAndMapterialsMap.put(locMaterial.Location_ID__c, new List<Material__c>{locMaterial});
             
        }
        
        for(Location__c loc : originalLocationList){
            //Creates the External ID for each Line Item
            string EXTERNALID = loc.ID+EFLGenericUtility.randomString();
            Location__c LOCReference = new Location__c(Loc_Ext_ID__c= EXTERNALID);
            Location__c newLocation = new Location__c();
            
            newLocation = loc.clone(false,true,false,false);  
            newLocation.Line_Item__r = lineItemReference;
            newLocation.Loc_Ext_ID__c= EXTERNALID;  //Assign External id
            newLocation.Parent_Location__c = loc.ID;  //RR 5/13/2019 W-026035
            if (actiontype=='Clone Button'){
                newLocation.Status__c='Draft'; 
            }
            clonedLocationList.add(newLocation); 
            //clone Related Contact
            //list<Related_Contact__c> originalContactList = EFLLocationRepository.selectContactsByLocationIDforClone(loc.ID);
            list<Related_Contact__c> originalContactList = locationsAndRelatedContactsMap.get(loc.ID) ;
            if (originalContactList!=null &&  originalContactList.size()> 0){
                for (Related_Contact__c rc:originalContactList){
                    Related_Contact__c newRC = new Related_Contact__c(); 
                    newrc = rc.clone(false, false, false, false);
                    newrc.Location__r=LOCReference; 
                    clonedContactList.add(newrc);
                }
            }
            //clone GPS Coordinate  GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,     GPS_Coordinates_CBI__c
            //list<GPS_Coordinate__c> originalGPSList = EFLLocationRepository.selectGPSByLocationIDforClone(loc.ID);
            list<GPS_Coordinate__c> originalGPSList = locationsAndGPSMap.get(loc.ID);
            if (originalGPSList !=null && originalGPSList.size()> 0){
                for (GPS_Coordinate__c gps:originalGPSList){
                    GPS_Coordinate__c newGPS = new GPS_Coordinate__c(); 
                    newGPS = gps.clone(false, false, false, false); 
                    newGPS.GPS_Coordinates__latitude__s = gps.GPS_Coordinates__latitude__s; 
                    newGPS.GPS_Coordinates__longitude__s = gps.GPS_Coordinates__longitude__s; 
                    newGPS.Location__r=LOCReference; 
                    clonedGPSList.add(newGPS);
                }
            }
            
            //clone  Material
            //list<Material__c> originalMaterialList = EFLLocationRepository.selectMaterialsByLocationIDforClone(loc.ID);
            list<Material__c> originalMaterialList = locationsAndMapterialsMap.get(loc.Id);
            if (originalMaterialList != null && originalMaterialList.size()> 0){
                for (Material__c ms:originalMaterialList){
                    Material__c newMS = new Material__c(); 
                    newMS = ms.clone(false, false, false, false); 
                    newMS.Location__r=LOCReference; 
                    clonedMaterialList.add(newMS);
                }
            }
        }
        sObjectLoc.addall(clonedLocationList);
        sObjectLoc.addall(clonedContactList);
        sObjectLoc.addall(clonedGPSList);
        sObjectLoc.addall(clonedMaterialList);
        return sObjectLoc;
    } 
    
    //Clone SOP 
    public static list<Construct_Application_Junction__c> cloneSOP(ac__c originalLineItem, ac__c lineItemReference ){
        
        Set<ID> originalSOPIDs = EFLApplicantAttachmentRepository.selectIDsByLineItemID(originalLineItem.ID);
        List<Construct_Application_Junction__c> prevReviewedSOPList = new  List<Construct_Application_Junction__c>();
        
        for(ID sopID : originalSOPIDs){
            Construct_Application_Junction__c newPrevReviewedSOP = new  Construct_Application_Junction__c();
            newPrevReviewedSOP.line_Item__r = lineItemReference;
            newPrevReviewedSOP.Design_Protocol_Record_SOP__c = sopID;
            prevReviewedSOPList.add(newPrevReviewedSOP);
        }
        
        return prevReviewedSOPList;
    } 
    
    //Clone Additional Associated Contact 
    public static list<Additional_Contact_Detail__c> cloneAdditionalAssociatedContacts(ac__c originalLineItem, ac__c lineItemReference ){
        list<Additional_Contact_Detail__c> originalAdditionalContactDetailsList = EFLAdditionalContactDetailsRepository.selectByLineItemIDforClone(originalLineItem.ID);
        list<Additional_Contact_Detail__c> clonedAdditionalContactDetailsList = new list<Additional_Contact_Detail__c>(); 
        
        for(Additional_Contact_Detail__c acd : originalAdditionalContactDetailsList){
            Additional_Contact_Detail__c newAdditionalContactDetails = new Additional_Contact_Detail__c();
            newAdditionalContactDetails = acd.clone(false,false,false,false);  
            newAdditionalContactDetails.Line_Item__r = lineItemReference;
            clonedAdditionalContactDetailsList.add(newAdditionalContactDetails); 
        }
        return clonedAdditionalContactDetailsList;
    } 
    
    //Clone Applicant Attachment 
    public static list<Applicant_Attachments__c> cloneApplicantAttachments(ac__c originalLineItem, ac__c lineItemReference ){
        list<Applicant_Attachments__c> originalApplicantAttachmentsList = EFLApplicantAttachmentRepository.selectByLineItemIDforClone(originalLineItem.ID);
        list<Applicant_Attachments__c> clonedApplicantAttachmentsList = new list<Applicant_Attachments__c>(); 
        
        for(Applicant_Attachments__c aa : originalApplicantAttachmentsList){
            Applicant_Attachments__c newApplicantAttachment = new Applicant_Attachments__c();
            newApplicantAttachment = aa.clone(false,false,false,false);  
            newApplicantAttachment.Animal_Care_AC__r = lineItemReference;
            newApplicantAttachment.Parent_Applicant_Attachment__c=aa.id;  //Ravee Racharla 
            clonedApplicantAttachmentsList.add(newApplicantAttachment); 
        }
        return clonedApplicantAttachmentsList;
    } 
    
    //Clone Applicant Attachment related Attachments
    public static list<attachment> cloneAttachments(list<sObject> clonedsObjectList){
        list<Applicant_Attachments__c> clonedApplicantAttachmentsList = (list<Applicant_Attachments__c>)clonedsObjectList;
        list<attachment> originalAttachmentsList = EFLApplicantAttachmentRepository.selectAttachmentsByList(clonedApplicantAttachmentsList);
        list<attachment> clonedAttachmentsList = new list<attachment>(); 
        
        for(Applicant_Attachments__c aa: clonedApplicantAttachmentsList){
            for(attachment att : originalAttachmentsList){
                attachment newAttachment = new attachment();
                newAttachment = att.clone(false,false,false,false);  
                // newAttachment.parentID = lineItemReference.ID;
                newAttachment.parentID =att.parentID; //Ravee Racharla
                clonedAttachmentsList.add(newAttachment); 
            }             
            
        }
        
        
        return clonedAttachmentsList;
    }     
    
    //Clone Article Supplier/Developer  Ravee Racharla 12/14/2018
    public static list<Article_Supplier_Developer__c> cloneArticleSupplierDeveloper(ac__c originalLineItem, ac__c lineItemReference ){
        list<Article_Supplier_Developer__c> originalASDDetailsList = EFLArticleSupplierRepository.selectASDByLineItemIDForClone(originalLineItem.ID);
        list<Article_Supplier_Developer__c> clonedASDDetailsList = new list<Article_Supplier_Developer__c>(); 
        
        for(Article_Supplier_Developer__c asd : originalASDDetailsList){
            Article_Supplier_Developer__c newASDDetails = new Article_Supplier_Developer__c();
            newASDDetails = asd.clone(false,false,false,false);  
            newASDDetails.Line_Item__r = lineItemReference;
            clonedASDDetailsList.add(newASDDetails); 
        }
        return clonedASDDetailsList;
    } 
}