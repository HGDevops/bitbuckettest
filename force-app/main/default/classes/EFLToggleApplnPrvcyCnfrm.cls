public class EFLToggleApplnPrvcyCnfrm {
    
    public Id appId;
    public Application__c app {get;set;}
    public string StrPripub {get;set;}
    public string callingPage {
        get{ return apexPages.currentPage().getParameters().get('callingPage');}
        public set{}
    }
    public EFLToggleApplnPrvcyCnfrm(){
        appId = ApexPages.currentPage().getParameters().get('id');
        app = [select id, name, Application_Status__c, private_application__c from application__c where id = :appId];
        System.debug ('private flag is ' +  app.private_application__c);
        if (app.private_application__c == true){
          StrPripub = 'Public'; //already private, now turning public 
        } 
        else{
         StrPripub = 'Private'; //already public , now turning private
        }
    }
    
    public PageReference toggleApplicationPrivacy(){  
    PageReference redirect;
        if(callingPage != null){     
            redirect = new PageReference(callingPage);
         }
        Id userConid = [select contactid from user where id=:userinfo.getuserId()].contactid;
        Id accountid = [select accountid from contact where id=:userConid].accountid;
        string accountName = [select name from account where id=:accountid].name;
        Group grp = [SELECT Id, Name FROM Group WHERE Name =:accountName LIMIT 1];
        if(appId != null){
         if(!Test.isRunningTest()){
            CARPOL_ApexManagedSharing.toggleApplicationPrivacy(appId, grp.Id);
            }
        }
        return redirect;
      }
    public PageReference homeRedirect() {       
           PageReference newPgr = new PageReference('/portal_application');
           return newPgr;
    }

}