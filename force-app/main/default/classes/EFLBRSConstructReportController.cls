public with sharing class EFLBRSConstructReportController {
    
    public List<AC__c> Linelst{get;set;}
    public Id AuthId{get;set;}
    public id locationreportid {get;set;}
    public id Constructreportid {get;set;}
    
    public EFLBRSConstructReportController(Apexpages.StandardController controller){
        
        AuthId = ApexPages.currentPage().getParameters().get('id');
        
        Linelst=[SELECT Id,Name FROM AC__c WHERE Authorization__c=:AuthId];
        
        getreports(); 
    }
    
    public void getreports(){
        
        for(Report report: [select id,name,developername from report where developername =: 'Line_Item_with_Constructs' or developername =: 'Line_Item_with_Locations']){
            
            if(report.developername == 'Line_Item_with_Locations' )
                locationreportid = report.id;
            if(report.developername == 'Line_Item_with_Constructs' )
                Constructreportid = report.id; 
        }
        
    }
    
}