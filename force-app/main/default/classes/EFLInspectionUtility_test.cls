@isTest
public class EFLInspectionUtility_test{
    
    static testMethod void test(){
    CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        //EFLInspectionActivityHandler inspectionActivityHandler = new EFLInspectionActivityHandler();
        list<sObject> activityList = new list<sObject>();
        Domain__c objDom = new Domain__c();
         objDom.Name = 'BRS';
         insert objDom;
         
         Program_Prefix__c  objPre = new Program_Prefix__c();
         objPre.Name= 'BRS Inspection';
         objPre.Inspection_Related__c = true;
         objPre.Program__c = objDom.id;
         insert objPre;
         
          Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
          Application__c objapp = testData.newapplication();
       Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        Authorizations__c objauth = testData.newAuth(objapp.id);
         
          EFL_Inspection_Questions_Template__c inspTmp = new EFL_Inspection_Questions_Template__c();
         inspTmp.Name='test';
         insert inspTmp;
         
         EFL_Inspection_Questions_Template__c inspTmp1 = new EFL_Inspection_Questions_Template__c();
         inspTmp.Name='testing';
         insert inspTmp1;
         
        Inspection__c Ins = new Inspection__c();
        String IncRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Ins.RecordTypeId = IncRecTypeID;
        Ins.Stage__c = 'Inspection Assignment';
        Ins.status__c='Cancelled';
        Ins.Reason_for_Cancellation__c = 'testing';
        Ins.EFL_Inspection_Questionnaire_Template__c=inspTmp.id;
        insert Ins;
        
        Inspection__c Ins1 = new Inspection__c();
        Ins1.RecordTypeId = IncRecTypeID;
        Ins1.Stage__c = 'Inspection Assignment';
        Ins1.status__c='Cancelled';
        Ins1.Reason_for_Cancellation__c = 'testing';
        Ins1.EFL_Inspection_Questionnaire_Template__c=inspTmp.id;
        insert Ins1;
      /*  Profile p = [SELECT Id FROM Profile WHERE Name='eFile APHIS Staff'];
        user sysAdm = [select id,name from user where profile.name='System Administrator' and isactive=true limit 1];
        system.runas(sysAdm)
        {
            UserRole ur = new UserRole(DeveloperName = 'MyBRSPS', Name = 'BRS Program Specialist');
            insert ur;
            
            User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName='standarduserAuthWorkflow@testorg.com');
            insert u1;

        }
        User u2 = [SELECT Id FROM User WHERE LastName='Testing'];      
         team__c team = new team__c();
         team.recordtypeId = Schema.SObjectType.Team__c.getRecordTypeInfosByName().get('BRS Team').getRecordTypeId();
          team.member_role__c='BRS Program Specialist';
          team.Member__c = u2.id;
            team.Authorization__c=objauth.id;
            insert team;
            
        Ins.Stage__c = 'Inspection Report';
        Ins.Activity_Sequence__c = 0;
        Ins.EFL_Inspection_Questionnaire_Template__c=inspTmp1.id;
        update Ins;
        
        EFLInspectionUtility.needsTaskCreation(Ins,Ins);
  //     EFLInspectionUtility.needsQuestionQuestioneriesCreation(Ins,Ins);
        EFLInspectionUtility.needsTaskCreation(Ins,Ins1);
  //      EFLInspectionUtility.needsQuestionQuestioneriesCreation(Ins,Ins1); */
    }

}