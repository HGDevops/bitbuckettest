@isTest(seealldata=true)
    public class ConApp_Jun_TriggerHelper_Test {
    private static List<construct__c> prevReviewedConstructs;
    private static CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
    private static AC__c lineItem1;
    Construct_Application_Junction__c cajGlobal = new Construct_Application_Junction__c();
        
     //Use case: Insert previouly reviewed construct on LineItem with
    //           no Previously reviewed constructs and no new constructs
    //Expected Result: LineItem Construct Status should be "Ready to Submit"
    @isTest static void TestInsertFirstPreviouslyReviewedConstruct(){
        
        Test.startTest();
        //verify LineItem Construct status is "Yet to Add"
        System.debug('prevReviewedConstructs: '+ prevReviewedConstructs); 
        Application__c  app=testData.newapplication();
        String poi='Resale';
        
        
        Regulated_Article__c ra = testData.newRegulatedArticle(testData.newBRSPathway().id);
        
        AC__c acvar=testData.newLineItem(poi,app);
        acvar.SOP_Status__c='Yet to Add';
        acvar.construct_Status__c = 'Yet to Add';
        update acvar;
        
        Link_Regulated_Articles__c lra = testData.newlinkRegArticleByLIIdRAId(acvar.id, ra.Id, 'Draft');
                
        Construct__c ConstructVar= testdata.newconstructByLIAndRegulatedArticle(acvar.id, ra.id);
        prevReviewedConstructs = new List<construct__c>();
        prevReviewedConstructs.add(ConstructVar);
        
        Construct_Application_Junction__c caj = new Construct_Application_Junction__c();
        caj.Construct__c = prevReviewedConstructs[0].Id;
        caj.Line_Item__c = acvar.Id;
        insert caj;
        
       
        lineItem1 = [SELECT id,Construct_Status__c from AC__c where id = :acvar.id];
        System.assert(lineItem1.Construct_Status__c =='Yet to Add');
        
        Test.stopTest(); 
        
        
    }
      
      @isTest static void TestInsertFirstPreviouslyReviewedConstruct2(){
        
        Test.startTest();
        
        //verify LineItem Construct status is "Yet to Add"
        System.debug('prevReviewedConstructs: '+ prevReviewedConstructs); 
        Application__c  app=testData.newapplication();
        String poi='Resale';
        
        
        Regulated_Article__c ra = testData.newRegulatedArticle(testData.newBRSPathway().id);
        
        AC__c acvar=testData.newLineItem(poi,app);
        acvar.SOP_Status__c='Yet to Add';
        acvar.construct_Status__c = 'Yet to Add';
        update acvar;
        
        Link_Regulated_Articles__c lra = testData.newlinkRegArticleByLIIdRAId(acvar.id, ra.Id, 'Draft');
                
        Construct__c ConstructVar= testdata.newconstructByLIAndRegulatedArticle(acvar.id, ra.id);
        prevReviewedConstructs = new List<construct__c>();
        prevReviewedConstructs.add(ConstructVar);

        Applicant_Attachments__c appAttch = new Applicant_Attachments__c();
        insert appAttch;

         System.debug('%%%%%%%%%%%%%%%%%%%%% lineItem1 ' + lineItem1); 
        //System.assert(lineItem1.SOP_Status__c=='Yet to Add');
        Construct_Application_Junction__c caj = new Construct_Application_Junction__c();
        //caj.Construct__c = prevReviewedConstructs[0].Id;
        caj.Design_Protocol_Record_SOP__c = appAttch.id;
        caj.Line_Item__c = acvar.Id;
        insert caj;
        
       
         System.debug('&&&&&&&&&&&&&&&&&&&& lineItem1 ' + lineItem1); 
         System.debug('^^^^^^^^^^^^^^^^^^^^ caj ' + caj);
        lineItem1 = [SELECT id,Construct_Status__c from AC__c where id = :acvar.id];
        //System.assert(lineItem1.SOP_Status__c=='Ready to Submit');
        
        Test.stopTest();
        
        
    }

    /*
     //Use case:    Insert Additional previouly reviewed construct on LineItem with
    //              existing Previously reviewed construct(s) and no new constructs
    //Expected Result: LineItem Construct Status should be "Ready to Submit"
    @isTest static void TestInsertPreviouslyReviewedConstruct(){
        
    }
    
     //Use case:    Insert Additional previouly reviewed SOP on LineItem with
    //              existing Previously reviewed SOP(s) and no new SOPs
    //Expected Result: LineItem SOP Status should be "Ready to Submit"
     @isTest static void TestInsertPreviouslyReviewedSOP(){
        
    }
    
     //Use case:    Delete the last previouly reviewed construct on LineItem with
    //              more only one Previously reviewed constructs and no new constructs
    //Expected Result: LineItem Construct Status should be "Yet to ADD"
     @isTest static void TestDeleteLASTPreviouslyReviewedConstructs(){
        
    }
    
     //Use case:    Delete the last previouly reviewed SOP on LineItem with
    //              more only one Previously reviewed SOP and no new SOPs
    //Expected Result: LineItem SOP Status should be "Yet to ADD"
     @isTest static void TestDeletetLASTPreviouslyReviewedSOPs(){
        
    }
    
    //Delete one of the previouly reviewed constructs on LineItem with
    //more than one Previously reviewed constructs and no new constructs
    //Expected Result: LineItem Construct Status should be "Ready to Submit"
     @isTest static void TestDeletePreviouslyReviewedConstructs(){
        
    }
  
     //Delete one of the previouly reviewed SOPs on LineItem with
    //more than one Previously reviewed SOPs and no new SOPs
    ////Expected Result: LineItem SOP Status should be "Ready to Submit"
     @isTest static void TestDeletetPreviouslyReviewedSOPs(){
        
    }*/
        
            //Test Setup 
       
    private static void setupConstruct() {        
        prevReviewedConstructs = new List<construct__c> ();       
        prevReviewedConstructs.add(getPrevReviewedConstruct());
        lineItem1 = getLineItem();
    }
    
    private static construct__c getPrevReviewedConstruct()
    {
        construct__c objconst;       
        AC__c lineItem = getLineItem();        
        Authorizations__c objauth=testData.newAuth(lineItem.Application_Number__c);
        lineItem.Authorization__c=objauth.id;
        lineItem.construct_Status__c = 'Yet to Add';
        Update lineItem;
       
    //Prepare Issued Authorization
        objauth.Status__c = 'Issued'; 
        update objauth;        
 
//line item related records like link regulated article,Construct, Geno and Phenotype 
        objconst=testdata.newconstruct(lineItem.id);
        Genotype__c objgeno=testdata.newgenotype(objconst.id);
        Genotype__c objgeno1=testdata.newgenotype(objconst.id);
        Phenotype__C objpheno=testdata.newphenotype(objconst.id);
//Prepare reviewed Construct's
       objconst.status__c= 'Review Complete'; 
       update objconst; 
       
        return objconst;

    }
    
    private static AC__c getLineItem()
    {
        String AccountRecordTypeId = testData.AccountRecordTypeId; 
        testData.insertcustomsettings();
        Account objacct=testData.newAccount(AccountRecordTypeId);
        Contact objcont=testData.newcontact();
        Application__c objapp=testData.newapplication();
        Program_Line_Item_Pathway__c plip=testData.newCaninePathway();
        AC__c lineItem=testData.newLineItemBRS('Personal Use',objapp);
        Attachment attach=testData.newattachment(lineItem.Id);       
        lineItem.construct_Status__c = 'Yet to Add';       
        return lineItem;        
    }

    @isTest public static void TestDeleteReviewedConstruct2()
    {

        List<Construct__c> prevReviewedConstructs=new list<Construct__c>();
        Application__c  app=testData.newapplication();
        String poi='Resale';
        
        Regulated_Article__c ra = testData.newRegulatedArticle(testData.newBRSPathway().id);
        
        AC__c acvar=testData.newLineItem(poi,app);
        acvar = [SELECT id,Construct_Status__c from AC__c where id = :acvar.id For Update];
        acvar.Construct_Status__c='Ready to Submit';
        update acvar;
        
        Link_Regulated_Articles__c lra = testData.newlinkRegArticleByLIIdRAId(acvar.id, ra.Id, 'Draft');
                
        Construct__c ConstructVar= testdata.newconstructByLIAndRegulatedArticle(acvar.id, ra.id);
        prevReviewedConstructs.add(ConstructVar);
        
        Applicant_Attachments__c appAttch = new Applicant_Attachments__c();
        insert appAttch;
        
        Construct_Application_Junction__c caj = new Construct_Application_Junction__c();
        //caj.Construct__c = prevReviewedConstructs[0].Id;
        caj.Design_Protocol_Record_SOP__c = appAttch.id;
        caj.Line_Item__c = acvar.Id;
        insert caj;
        Delete caj;
    }
    @isTest public static void TestDeleteReviewedConstruct()
    {

        List<Construct__c> prevReviewedConstructs=new list<Construct__c>();
        Application__c  app=testData.newapplication();
        String poi='Resale';
        
        Regulated_Article__c ra = testData.newRegulatedArticle(testData.newBRSPathway().id);
        
        AC__c acvar=testData.newLineItem(poi,app);
        acvar.Construct_Status__c='Ready to Submit';
        update acvar;
        
        Link_Regulated_Articles__c lra = testData.newlinkRegArticleByLIIdRAId(acvar.id, ra.Id, 'Draft');
                
        Construct__c ConstructVar= testdata.newconstructByLIAndRegulatedArticle(acvar.id, ra.id);
        prevReviewedConstructs.add(ConstructVar);
        
        Applicant_Attachments__c appAttch = new Applicant_Attachments__c();
        insert appAttch;
        
        Construct_Application_Junction__c caj = new Construct_Application_Junction__c();
        caj.Construct__c = prevReviewedConstructs[0].Id;
        caj.Line_Item__c = acvar.Id;
        insert caj;
        Delete caj;
    }

    
}