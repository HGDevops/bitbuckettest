/**
 * Incident Engine Interface
 */
public interface EFLIIncidentEngine { 

   void loadActivities(Incident__c IncidentRecord,list<sObject> activityList);
    
}