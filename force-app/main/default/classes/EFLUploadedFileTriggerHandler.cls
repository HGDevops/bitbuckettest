public inherited sharing class EFLUploadedFileTriggerHandler {
    
    public static void doAfterUpdate(list<Uploaded_File__c> files){
        List<ContentDocument> documentsToDelete = new List<ContentDocument>();
        List<Uploaded_File__c> uploadedFilesToDelete = new List<Uploaded_File__c>();
        for (Uploaded_File__c file : files){
            if (file.Is_Deletable__c && 
                file.Is_Deleted__c &&
                String.isBlank(file.External_File_Id__c))
            {
                    ContentDocument docToDelete = new ContentDocument(Id = file.File_Preview_Id__c);
                    documentsToDelete.add(docToDelete);
                    uploadedFilesToDelete.add(new Uploaded_File__c(Id = file.Id));
            }
        }
        delete documentsToDelete;
        delete uploadedFilesToDelete;
    }
     
    public static void doBeforeInsert(list<Uploaded_File__c> files){
        // if parent Id is of type XXX, then set properties from parent into file record.
        map<Id, list<Uploaded_File__c>> inScope = new map<Id, list<Uploaded_File__c>>();
        list<EFLFileList_Dynamic_Detail__mdt> allMappings = [SELECT Parent_sObject_Name__c, Parent_Field__c, Uploaded_File_Field_to_Map_To__c 
                                                             FROM EFLFileList_Dynamic_Detail__mdt
                                                            WHERE Is_Test_data__c = :test.isRunningTest()];
        list<EFLFileList_Dynamic_Detail__mdt> mappingsNeeded = new list<EFLFileList_Dynamic_Detail__mdt>();
        
        set<string> parentTypes = new set<string>();
        
        for(EFLFileList_Dynamic_Detail__mdt m : allMappings){
            parentTypes.add(m.Parent_sObject_Name__c);
        }
        set<string> parentsNeeded = new set<string>();
        for(Uploaded_File__c f : files){
            Id parentId = f.ParentId__c;
            string sObjectType = string.valueOf(parentId.getSobjectType());
            system.debug('object type: ' + sObjectType);
            
            map<Id, string> parentMap = new map<Id, String>();
            
            if(parentTypes.contains(sObjectType)){
                if(!inScope.containsKey(f.ParentId__c)){
                    inScope.put(f.ParentId__c, new list<Uploaded_File__c>());
                }
                inScope.get(f.ParentId__c).add(f);
                for(EFLFileList_Dynamic_Detail__mdt m:allMappings){
                    if(m.Parent_sObject_name__c == sObjectType){
                        mappingsNeeded.add(m);
                        parentsNeeded.add(sObjectType);
                    }
                }
            }
        }
        if(!inScope.isEmpty()){
            setDataOnFiles(inScope, mappingsNeeded, parentsNeeded);
        }
    }
    
    public static void setDataOnFiles(map<Id, list<Uploaded_File__c>> files, list<EFLFileList_Dynamic_Detail__mdt> mappings,set<string> parentsNeeded){
        // build out the queries.
        set<Id> allIds = files.keyset();
        map<string, set<string>> objectQueryMap = new map<string, set<string>>();
        for(EFLFileList_Dynamic_Detail__mdt m : mappings){
            if(!objectQueryMap.containsKey(m.Parent_sObject_Name__c)){
                objectQueryMap.put(m.Parent_sObject_Name__c,new set<string>());
            }
            objectQueryMap.get(m.Parent_sObject_Name__c).add(m.Parent_Field__c);
        }
        
        // now query each object dynamically & set values where needed
        for(string s :objectQueryMap.keySet()){
            list<string> fields = new list<string>();
            fields.addAll(objectQueryMap.get(s));
            
            if(!objectQueryMap.get(s).contains('Id')){fields.add('Id');}
            
            string qry = 'SELECT ' + string.join(fields,',');
            qry += ' FROM ' + s;
            qry += ' WHERE Id IN :allIds';
            system.debug('query: ' + qry);
            
            // NOTE: this is a query inside a for-loop, however it is controlled by Custom Metadata & the # of queries is limited by
            // the unique number of sObjects listed in the CMD.  This should not be seen as a risk to governer limits.
            for(sObject sObj: Database.query(qry)){
                if(files.containsKey((Id)sObj.get('Id'))){
                    Id key = (Id)sObj.get('Id');
                    
                    for(EFLFileList_Dynamic_Detail__mdt m : mappings){
                        if(m.Parent_sObject_Name__c == s){
                            // we now have a mapping to make.
                            object objVal = EFLUtils.getSObjectField(sObj, m.Parent_Field__c);
                            if(objVal != null){
                                for(Uploaded_File__c file : files.get(key)){
                                	file.put(m.Uploaded_File_Field_to_Map_To__c,objVal);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}