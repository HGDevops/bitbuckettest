public class EFLInspQuestionnaireQnsEngineFactory {
	
	/*
    * Purpose: Get QuestionnaireQn Engine
    */ 
    public static EFLInspQuestionnaireQnsEngine getEngine(EFL_Inspection_Questionnaire_Questions__c questionnaireQnRec){
       
        EFLInspQuestionnaireQnsEngine Engine; 
        Engine = getQuestionnaireQnEngine(questionnaireQnRec);
         
        if(Engine==null){
            throw new EFLInspQuestionnaireQnsEngineException('No Questionnaire Engine found for InspectionQuestionnaireQn: ' + questionnaireQnRec.Name);
            
        }
        
        return Engine;
      }  
   
   /*
    * Purpose: Get Questionnaire Question Engine based on program
    * Note: Enable the commented code once other Program Engines are implemented
    */     
    private static EFLInspQuestionnaireQnsEngine getQuestionnaireQnEngine(EFL_Inspection_Questionnaire_Questions__c questionnaireQnRec){
         return new EFLBRSInspQuestionnaireQnsEngine(); 
        /* if (questionnaireQnRec.Program__c == 'BRS')
		{
			  return new EFLBRSInspectionEngine(); 
		}*//*else if (inspectionRecord.Program__c == 'AC')
		{ 
			//return new EFLACInspectionEngine();
 
		}else if (inspectionRecord.Program__c == 'VS') 
		{
			//return new EFLVSInspectionEngine();
 
		}else if (inspectionRecord.Program__c == 'PPQ')
		{
			//return new EFLPPQInspectionEngine();
		}*/
        /* else
          return new EFLBRSInspectionEngine();*/

    }  
    
    public class EFLInspQuestionnaireQnsEngineException extends Exception {}  
    
}