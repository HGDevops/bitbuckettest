public class EFLExternalURL {
    
    public  CARPOL_URLs__c baseurl {get;set;}
   
    public EFLExternalURL()
    {
        baseurl = CARPOL_URLs__c.getValues('External');        
    }

}