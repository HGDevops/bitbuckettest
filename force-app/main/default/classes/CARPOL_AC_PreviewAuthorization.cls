/* 
// Author : Vinar Amrutia
// Date Created : 04/09/2015
// Purpose : Ability to Create and Preview Authorization Letters
*/
public without sharing class CARPOL_AC_PreviewAuthorization {

    public String templateURL { get; set; }
    public String selectedTemplateType { get; set; }
    public String selectedTemplate { get; set; }
    public SelectOption[] allTemplateOptions { get; set; }
    public String reviewerComments { get; set; }
    public Boolean showPreview { get; set; }
    private AC__c acLineItem;
    
    public CARPOL_AC_PreviewAuthorization(ApexPages.StandardController controller) {
        
        this.acLineItem = (AC__c)controller.getRecord();
        if(acLineItem.ID == null){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a related AC Line Item.'));}
        selectedTemplateType = 'Letter of Denial';
        System.Debug('<<<<<<< Template Type : ' + selectedTemplateType + ' >>>>>>>');
        getTemplates();
        showPreview = false;
    }
    
    public PageReference getTemplates()
    {
        if(selectedTemplateType != null)
        {
            showPreview = false;
            System.Debug('<<<<<<< Getting all the templates and setting preview to : ' + showPreview + ' >>>>>>>');
            allTemplateOptions = new List<SelectOption>();
            List<Communication_Manager__c> tempTemplates = [SELECT ID, Name FROM Communication_Manager__c WHERE Type__c = :selectedTemplateType];
            for (Communication_Manager__c template : tempTemplates ) 
            {allTemplateOptions.add(new SelectOption(template.ID, template.Name));}
            
        }
        return null;
    }
    
    public PageReference getTemplateContent()
    {
        if(acLineItem.ID != null && selectedTemplate != '--None--')
        {
            acLineItem.Authorization_Content__c = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID =:selectedTemplate LIMIT 1].Content__c;
            update acLineItem;
            previewTemplate();
        }
        else{showPreview = false;ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a related AC Line Item.'));}
        return null;
    }
    
    public PageReference approveDraft()
    {
        try{
            
            String attachmentName;
            PageReference pdfACAuthorizationLetter;
            
            if(selectedTemplateType == 'Letter of Denial')
            {
                pdfACAuthorizationLetter = Page.CARPOL_AC_LiveDogs_LOD_Formal;
                attachmentName = 'LetterOfDenial';
            }
            if(selectedTemplateType == 'Letter of No Jurisdiction')
            {
                pdfACAuthorizationLetter = Page.CARPOL_AC_LiveDogs_NJLetter_Formal;
                attachmentName = 'LetterOfNoJurisdiction';
            }
            if(selectedTemplateType == 'Permit Letter')
            {
                //pdfACAuthorizationLetter = Page.CARPOL_AC_LiveDogsPermit;
                //attachmentName = 'Permit';
            }
            
            pdfACAuthorizationLetter.getParameters().put('id', acLineItem.ID);
            Blob pdfACAuthorizationLetterBlob;
            
            System.Debug('<<<<<<< Page about to be loaded : ' + pdfACAuthorizationLetter + ' >>>>>>');
            pdfACAuthorizationLetterBlob = pdfACAuthorizationLetter.getContent();
            
            String tempAttachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :acLineItem.ID AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size() > 0)
            {
                if(prevAttachments.size() == 1){attachmentName = attachmentName + '_v2';}
                else
                {
                    for(Integer i = 0; i < prevAttachments.size(); i++ )
                    {
                        if(i == prevAttachments.size() - 1)
                        {
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');
                            attachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;
                            System.Debug('<<<<<<< New Attachment Name : ' + attachmentName + ' >>>>>>');
                        }
                    }
                }
            }
            
            attachmentName = attachmentName + '.pdf';
            Attachment attachment = new Attachment(parentId = acLineItem.ID, name = attachmentName, body = pdfACAuthorizationLetterBlob);
            insert attachment;
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft Approved. Authorization attached successfully.'));
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
        return null;
    }
    
    public PageReference saveDraft()
    {
        try{
            update acLineItem;
            previewTemplate();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft saved successfully.'));
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
        return null;
    }
    
    public PageReference previewTemplate()
    {
        try{
            System.Debug('<<<<<<< Generating Preview !! >>>>>>>');
            if(selectedTemplate != '--None--')
            {
                showPreview = true;
                //update acLineItem;
                if(selectedTemplateType == 'Letter of Denial')
                {
                    templateURL = '/apex/CARPOL_AC_LiveDogs_LOD_Formal?id=' + acLineItem.ID;
                }
                if(selectedTemplateType == 'Letter of No Jurisdiction')
                {
                    templateURL = 'apex/CARPOL_AC_LiveDogs_NJLetter_Formal?id=' + acLineItem.ID;
                }
                if(selectedTemplateType == 'Permit Letter')
                {
                    //templateURL = '/apex/CARPOL_AC_LiveDogsPermit';  //page removed with CARPOL_AC_LiveDogsPermit_extension
                }
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
        return null;
    }
    
}