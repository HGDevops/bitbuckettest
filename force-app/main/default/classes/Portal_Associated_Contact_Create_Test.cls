@isTest
private class Portal_Associated_Contact_Create_Test
{
  static testMethod void Portal_Associated_Contact_Create_Meth() {

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          String AccountRecordTypeId = testData.AccountRecordTypeId; 
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);
          Applicant_Contact__c objacc = testData.newappcontact();
          
          User usershare = new User();
          usershare = EFLUserTestDataFactory.getUser('eFile Applicant');   
          System.runAs(usershare){
              ApexPages.currentPage().getParameters().put('recordtype',objacc.RecordTypeId);                                                                                                             
              ApexPages.StandardController stdController = new ApexPages.StandardController(new Applicant_Contact__c());
              Portal_Associated_Contact_Create objAss = new Portal_Associated_Contact_Create(stdController);
              PageReference pRef = Page.Portal_Associated_Contact_Create;
              objAss.appCon = objacc;
              pRef = objAss.matchValues();
              pRef = objAss.Cancel();
              pRef = objAss.editAssocContact();
              pRef = objAss.Save();
              System.assert(objass != null);                      
          }
  
  }
}