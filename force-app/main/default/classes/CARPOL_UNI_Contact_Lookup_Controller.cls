public with sharing class CARPOL_UNI_Contact_Lookup_Controller {

    public string searchString{get;set;} // search keyword, TODO: need to put some error checking on the get method for web safety
    public string newasscont {get;set;}
    public Map<Integer , Applicant_Contact__c> FacMap {get;set;}
    public Boolean showMessage {get;set;}
    public Applicant_Contact__c objImpContactNew {get;set;}    
    public List<Applicant_Contact__c> newContacts {get;set;}
    public Contact contactDetails { get; set; }
    public List<Program_Line_Item_Section__c> progsec = new List<Program_Line_Item_Section__c>();
    public id pathwayId;
    public Map<String,String> SecTypeSecNameMap = new Map<String,String>();
    public String selectedcountry {get;set;}
    public Boolean staterequired {get;set;}
    public Boolean showtable {get;set;} 
    public string ACtype;
    public string countryidUS;
    public Boolean isCVBpthwy{get;set;}
    public Boolean renderSection1{get;set;}
    public Boolean renderSection2{get;set;}
    public String defaultTab{get;set;}
    public String btnClass{get;set;}
    public String btnClass1{get;set;}
    public String btnClass2{get;set;}    
    public string LKPFilter; // VV added for 17676
    public Boolean showSearchError {get;set;}//remove after BRS release on 07/22/2019

    public void ChangeAndRerenderstate(){
        Country__c con = new Country__c();
        if(selectedcountry != Null)
        {
         List<Country__c> conList = new List<Country__c>();
         conList = [Select Id,name from Country__c where Id = :selectedcountry];
         if(conList.size()>0)
             con = conList[0];
        }
        if(con != Null)
        {
          if(con.Name != ''){
              if(con.Name.equals('United States of America'))
              {
                  staterequired = True;
              }
              else
              {
                  staterequired = False;
              } 
          }
          
        }
    }
    public CARPOL_UNI_Contact_Lookup_Controller() { 
        btnClass1 = 'btnbackgroundwhite';
        btnClass2 = 'btnbackgroundgreen';      
        renderSection1= true; 
         renderSection2 = false;
         defaultTab = 'tab1';        
        isCVBpthwy = false;
        searchString = '';
        newasscont = '';
        showMessage = false;
        showtable = false;
        staterequired = false;
        boolean importerSection = false;
        string url= ApexPages.currentPage().getUrl();
        if (url.indexof('importerLastName') >=0) // VV added for 17403 to exclude the Orgnization, Individual buttons just for Importer, show them only for shipper and producer
           importerSection = true;
        objImpContactNew = new Applicant_Contact__c();
        newContacts = new List<Applicant_Contact__c>();
        //contactDetails = [SELECT ID, FirstName, LastName, Account.Name, Phone, Fax, Email, Additional_Email__c, Mailing_Street_LR__c, Mailing_City_LR__c, Mailing_State_Province_LR__c, Mailing_Country_LR__c, Mailing_Zip_Postalcode_LR__c, Name FROM Contact WHERE ID = : appDetails.Applicant_Name__c LIMIT 1];        
        countryidUS = [select id from country__c where name = 'United States of America' limit 1].id;
        pathwayId = System.currentPageReference().getParameters().get('pathwayid');
        progsec = [SELECT ID,Name,Section_Type__c, Program_Line_Item_Pathway__r.name FROM Program_Line_Item_Section__c WHERE Program_Line_Item_Pathway__c=:pathwayId ];    
        if(progsec[0].Program_Line_Item_Pathway__r.name == 'Veterinary Biological Products' && importerSection == false) 
            // vv added 1-31-18 for 17403
            {
                isCVBpthwy = true;
            }
     
     //nitish testing ---------->
     String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        FacMap = new Map<Integer , Applicant_Contact__c>();
        List<Applicant_Contact__c> AssocConList = new List<Applicant_Contact__c>();
        
             AssocConList = [select id, name, First_Name__c ,Email_Address__c,USDA_Registration_Number__c,USDA_License_Number__c,USDA_License_Expiration_Date__c,
                             USDA_Registration_Expiration_Date__c,Phone__c,Fax__c,Contact_Organization__c,Mailing_City__c,Mailing_Country_LR__c,Mailing_Country_LR__r.Name,
                             Mailing_State_LR__c,Mailing_State_LR__r.Name,Mailing_Street__c,Mailing_Zip_Postal_Code__c from Applicant_Contact__c where Recordtypeid=:apcontRecordTypeId Order By Name]; 
            
        for(Integer index = 0 ; index < AssocConList.size() ; index++) {
           FacMap.put(index, AssocConList[index]);
         }
     
     ACtype = apexpages.currentpage().getparameters().get('ACtype');
     LKPFilter = apexpages.currentpage().getparameters().get('LKPFilter'); // VV added for 17676
     //nitish testing----------->
     
     
     
     
     
     
         
    }
    
  public PageReference search() {
   showtable = true;
    getperformSearch();
    return null;
  } 
  
  // This method is used to get associated contact records in page block table in pop-up 
     public List<Applicant_Contact__c> getperformSearch() { 
    
        List<Applicant_Contact__c> contactsearch = new List<Applicant_Contact__c>();
        String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        
        String pobox1 = 'P.O. Box';
        String pobox2 = 'PO Box';
        String pobox3 = 'P.O. BOX';
        String pobox4 = 'PO BOX'; 
        String searchParam = '%' + searchString +'%' ;  
        String street1 = '%' + pobox1 +'%';
        String street2 = '%' + pobox2 +'%';
        String street3 = '%' + pobox3 +'%';
        String street4 = '%' + pobox4 +'%';  
        String mailingCountry = countryidUS;                    
      
        if(searchString != '' && searchString != null){ // VV updated to show only contacts (not org contacts for all non-CVB-Producer/Shipper)
            if((ACtype !=null && ACtype != '') || (LKPFilter !=null && LKPFilter != '')){ // VV added LKPFilter for 17676
                if(ACtype == 'Permittees' || LKPFilter ==EFLGlobalConstants.getConstantValue('LAST NAME US ONLY') ){
                    contactsearch = [select id, name, First_Name__c ,Email_Address__c,USDA_Registration_Number__c,USDA_License_Number__c,USDA_License_Expiration_Date__c,USDA_Registration_Expiration_Date__c,Phone__c,Fax__c,Contact_Organization__c,Mailing_City__c,Mailing_Country_LR__c,Mailing_Country_LR__r.Name,Mailing_State_LR__c,Mailing_State_LR__r.Name,Mailing_Street__c,Mailing_Zip_Postal_Code__c from Applicant_Contact__c where Recordtypeid=:apcontRecordTypeId and (name LIKE : searchParam or First_Name__c LIKE : searchParam) and (NOT Mailing_Street__c like : street1) and (NOT Mailing_Street__c like : street2) and (NOT Mailing_Street__c like : street3) and (NOT Mailing_Street__c like : street4) and Mailing_Country_LR__c =: mailingCountry];// and First_Name__c!=null];
                }                
                if(ACtype == 'Shippers'  || LKPFilter ==EFLGlobalConstants.getConstantValue('LAST NAME FOREIGN ONLY')){ // VV added LKPFilter for 17676
                    contactsearch = [select id, name, First_Name__c ,Email_Address__c,USDA_Registration_Number__c,USDA_License_Number__c,USDA_License_Expiration_Date__c,USDA_Registration_Expiration_Date__c,Phone__c,Fax__c,Contact_Organization__c,Mailing_City__c,Mailing_Country_LR__c,Mailing_Country_LR__r.Name,Mailing_State_LR__c,Mailing_State_LR__r.Name,Mailing_Street__c,Mailing_Zip_Postal_Code__c from Applicant_Contact__c where Recordtypeid=:apcontRecordTypeId and (name LIKE : searchParam or First_Name__c LIKE : searchParam) and (NOT Mailing_Street__c like : street1) and (NOT Mailing_Street__c like : street2) and (NOT Mailing_Street__c like : street3) and (NOT Mailing_Street__c like : street4) and Mailing_Country_LR__c <>: mailingCountry];// and First_Name__c!=null];
                }
            }        
            else{
               if (isCVBpthwy){ // VV 02-15-18 Added per w-17403 to show contacts that are orgs along with regular contacts for CVB-Producer/shipper ONLY
                         contactsearch = [select id, name, First_Name__c ,Email_Address__c,
                                          USDA_Registration_Number__c,USDA_License_Number__c,
                                          USDA_License_Expiration_Date__c,USDA_Registration_Expiration_Date__c,
                                          Phone__c,Fax__c,Contact_Organization__c,Mailing_City__c,Mailing_Country_LR__c,
                                          Mailing_Country_LR__r.Name,Mailing_State_LR__c,Mailing_State_LR__r.Name,Mailing_Street__c,
                                          Mailing_Zip_Postal_Code__c from Applicant_Contact__c 
                                          where Recordtypeid=:apcontRecordTypeId and (name LIKE : searchParam or First_Name__c LIKE : searchParam)];
                     }else{
                         contactsearch = [select id, name, First_Name__c ,Email_Address__c,USDA_Registration_Number__c,
                                          USDA_License_Number__c,USDA_License_Expiration_Date__c,
                                          USDA_Registration_Expiration_Date__c,Phone__c,Fax__c,Contact_Organization__c,
                                          Mailing_City__c,Mailing_Country_LR__c,Mailing_Country_LR__r.Name,Mailing_State_LR__c,
                                          Mailing_State_LR__r.Name,Mailing_Street__c,Mailing_Zip_Postal_Code__c 
                                          from Applicant_Contact__c where Recordtypeid=:apcontRecordTypeId 
                                          and (name LIKE : searchParam or First_Name__c LIKE : searchParam) and First_Name__c!=null];
                     }
            } 
            /*else{
            contactsearch = [select id, name, First_Name__c ,Email_Address__c,
                                          USDA_Registration_Number__c,USDA_License_Number__c,
                                          USDA_License_Expiration_Date__c,USDA_Registration_Expiration_Date__c,
                                          Phone__c,Fax__c,Contact_Organization__c,Mailing_City__c,Mailing_Country_LR__c,
                                          Mailing_Country_LR__r.Name,Mailing_State_LR__c,Mailing_State_LR__r.Name,Mailing_Street__c,
                                          Mailing_Zip_Postal_Code__c from Applicant_Contact__c 
                                          where Recordtypeid=:apcontRecordTypeId and (name LIKE : searchParam or First_Name__c LIKE : searchParam)];

            }*/
            
        }
        else if(newContacts.size() > 0){
            contactsearch = newContacts;
        }
        // VV added this to concatenate into last name --> First name w/last, if first name is not null, or just use last name
        for (Applicant_Contact__c ac: contactsearch){
         if(ac.first_Name__c !=null) ac.name = ac.first_Name__c + ' '+ac.name;
        }
        return contactsearch;
  }  
  
  // This method is used to create new associated contact record
  public PageReference saveAppcont() {
    try {
    String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
    objImpContactNew.Recordtypeid = apcontRecordTypeId ;
    if(renderSection2)  // copy last name to business name field only when an org being enterd W-17403 VV 2-1-18
        objImpContactNew.EFL_Business_Name__c = objImpContactNew.Name; 
        // for VS additional addresses, check: address cannot be PO box. 
        //if ACtype = Permittees, allow only US address
        //if ACtype = shippers, allow only non-US address    
    if(((ACtype !=null && ACtype != '') || (LKPFilter!=null ||LKPFilter!='')) && !Test.isRunningTest()){ // VV added LKPFilter for 17676
        if ((ACtype == 'Permittees' || LKPFilter == EFLGlobalConstants.getConstantValue('LAST NAME US ONLY') )&& objImpContactNew.Mailing_Country_LR__c != countryidUS){
               ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, 'Contact must have a US Address'); //'Permittees should have a US address'
               ApexPages.addmessage(msg);
               return null;            
            }else if ((ACtype == 'Shippers' || LKPFilter == EFLGlobalConstants.getConstantValue('LAST NAME FOREIGN ONLY')) && objImpContactNew.Mailing_Country_LR__c == countryidUS){
               ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR, 'Contact must have a Foreign Address');//'Shippers should have a non US address'
               ApexPages.addmessage(msg);
               return null;           
            }else if ((ACtype == 'Shippers' || ACtype == 'Permittees') && (objImpContactNew.Mailing_Street__c.contains('PO BOX')|| objImpContactNew.Mailing_Street__c.contains('PO Box') || objImpContactNew.Mailing_Street__c.contains('P.O.'))){
               ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,'Address cannot be a P.O. Box' );
               ApexPages.addmessage(msg);
               return null;
            }else{
              insert objImpContactNew; 
           }                     
        }else{
        insert objImpContactNew;       
      }
    //return list of new contacts to search tab
    newContacts.add(objImpContactNew);
    
    objImpContactNew = new Applicant_Contact__c();
    showMessage = true;
    
    } catch(DmlException e) {
    System.debug('The following exception has occurred: ' + e.getMessage());
    ApexPages.addMessages(e);
    }
    return null;
  }  
  
// used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
   }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }  
  
  public string getjsonmap() {     
        
          for(Program_Line_Item_Section__c p:progsec){
            SecTypeSecNameMap.put(p.Section_Type__c,p.Name);
         }
         Map<String,String> newStrings = new Map<String,String>();
         for(String s:SecTypeSecNameMap.keyset()){
            
             if(s!=null){
                 newStrings.put(s,SecTypeSecNameMap.get(s));
             }
         }
         String str = JSON.serialize(newStrings);
         return str;
  }
  
  public void individual() // VV added 1-31-18 for 17403 Setting boolean based the click, to set which section to render (Individual or company)
  {
      btnClass1 = 'btnbackgroundwhite';
      btnClass2 = 'btnbackgroundgreen';
      renderSection1= true; 
      renderSection2 = false;
      defaultTab = 'tab2'; // tab to create Company/Indiv
  }
  public void company()
  {
      btnClass1 = 'btnbackgroundgreen';
      btnClass2 = 'btnbackgroundwhite';
      renderSection1 = false;
      renderSection2 = true;      
      defaultTab = 'tab2';
      
  }
  
}