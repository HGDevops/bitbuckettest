/**
* Author : Kishore Kumar
* Created Date : 9/13/2016
* Purpose : Unit Test to cover this CARPOL_UNI_PreviewApplVersionsExtension class
*/
@IsTest(SeeAllData=true)
private class CARPOL_UNI_PreviewApplVerExtensionTest{
	private static testmethod void CARPOL_UNI_PreviewApplVerExtensionTest(){
/**
* Prepare BRS line item with all related records
**/	    
		CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
		testData.insertcustomsettings();
		Application__c objapp=testData.newapplication();
		AC__c ac1=testData.newLineItemBRS('Personal Use',objapp);
		Attachment attach=testData.newattachment(ac1.Id);
		Authorizations__c objauth=testData.newAuth(objapp.Id);
		ac1.Authorization__c=objauth.id;
		Update ac1;
		
		Id authRTId=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
		objAuth.recordTypeId=authRTId;
		Update objAuth;
		ApexPages.currentPage().getParameters().put('id',objAuth.id);
		ApexPages.StandardController stdAuth=new  ApexPages.StandardController(objAuth);
		CARPOL_UNI_PreviewApplVersionsExtension customContr=new  CARPOL_UNI_PreviewApplVersionsExtension(stdAuth);
		Id authRTId2=Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
		objAuth.recordTypeId=authRTId2;
		Update objAuth;
		ApexPages.currentPage().getParameters().put('id',objAuth.id);
/**
*  Executing test scenarios
**/		
		CARPOL_UNI_PreviewApplVersionsExtension customContr2=new  CARPOL_UNI_PreviewApplVersionsExtension(stdAuth);
	}
}