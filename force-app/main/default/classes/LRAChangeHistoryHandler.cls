public class LRAChangeHistoryHandler implements IChangeHistoryHandler {
    
    static final string fieldName = 'Scientific_Name__c';
    static final string fieldLabel = 'Regulated Article';
    public void populateNewRecordDetails(Sobject newRecord,Change_History__c change)
    {
        Schema.SObjectType sObjectType = newRecord.getSObjectType();
        if (sObjectType != null)
        {
            //record identifier
            system.debug('newRecord>>>'+newRecord);
            if (newRecord.get(fieldName) != null) 
            {
                change.name = fieldLabel;
                change.Field_API_Name__c   = fieldName;            
                change.New_Value__c  = (String)newRecord.get(fieldName);
                Change.New_Entry__c = true;            
            }
            populateChangeHistoryLookupValues(newRecord, change);
        }
    }
    
    public void populateUpdateDetails(Sobject newRecord, Sobject oldRecord,Change_History__c change)
    {
        populateChangeHistoryLookupValues(newRecord, change);
        
    }
    
    public void populateDeletedRecordDetails(Sobject deletedRecord,Change_History__c change)
    {

        System.debug('Deleted Entry: '+ deletedRecord.get(fieldName));
        if (deletedRecord.get(fieldName) != null) {
            change.name = fieldLabel;
            change.Field_API_Name__c   = fieldName; 
            change.old_Value__c  = (String)deletedRecord.get(fieldName);
            change.Delete_Flag__c = true;
        	populateChangeHistoryLookupValues(deletedRecord, change);
            }
        
    }
    
    public void populateChangeHistoryLookupValues(Sobject record, Change_History__c change)
    {
        Link_Regulated_Articles__c lra = (Link_Regulated_Articles__c)record;
        //change.Application__c = lra.Application__c;
        /*if(lra.ApplicationId__c!='')
        {
         change.Application__c = Id.valueOf(lra.ApplicationId__c);
        }*/
        change.Line_Item__c = lra.Line_Item__c;
        if(!change.Delete_Flag__c)change.Link_Regulated_Articles__c = lra.id;
        
    }

}