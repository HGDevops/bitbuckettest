/* @created 1/28/2019 by j.benkert@accenturefederal.com
 * @description use this class to create bulk accounts, contacts, applications & authorizations as needed.
 * Typical use in unit test: create accounts ==> create contacts ==> create applications ==> create authorizations
 * See EFLLabelTriggerHandlerTest @TestSetup method for example.
 */
@isTest
public class EFLTestDataFactoryBulk {

    // 1/28/2019: adding bulk data methods
    // j.benkert@accenturefederal.com
    public static integer uniqueNum = 0;
    public static integer getUnique(){
        return getNext();
    }
    public static integer getNext(){
        uniqueNum += 1;
        return uniqueNum;
    }
    public static integer getRepeat(){
        return uniqueNum;
    }
    public static integer getPrevious(){
        uniqueNum -= 1;
        return uniqueNum;
    }
    
    public static list<Id> authIds; // this is a workaround for test classes with seeAllData = true... :(
    
    // adding a bulk data method for getting a list of Accounts
    // j.benkert - 1/28/2019
    public static list<Account> makeAccounts(integer numAccounts, string rtId, boolean doDML){
        list<Account> accs = new list<Account>();
        for(integer i = 0; i < numAccounts; i++){
            Account a = makeAccount('Acct'+i, rtId);
            accs.add(a);
        }
        if(doDML == true){
            insert accs;
        }
        return accs;
    }
    
    // return a single accoung, no DML
    public static Account makeAccount(string name, string rtId){
        Account a = new Account();
        a.name = name;
        a.RecordTypeId = rtId;
        return a;
    }
    
    // adding a bulk data method for getting a list of Contacts
    // j.benkert - 1/28/2019
    public static list<Contact> makeContacts(list<Account> accs, boolean doDML){
        list<Contact> cons = new list<Contact>();
        for(Account a : accs){
            Contact c = makeContact(a);
            cons.add(c);
        }
		
        if(doDML){
            insert cons;
        }
        return cons;
    }
    
    public static Contact makeContact(Account a){
        integer i = getUnique();
        Contact c = new Contact();
        c.FirstName='Contact' + i;
        c.LastName='LastName' + i;
        c.Email='Contact' + i + 'test@email.com' + i;
        c.AccountId=a.id;
        c.MailingStreet='Mailing Street' + i;
        c.MailingCity='Mailing City' + i;
        c.MailingState='Ohio';
        c.MailingCountry='United States';
        c.MailingPostalCode='32092';
        return c;
    }
    
    public static list<Application__c> makeApplications(list<Contact> cons, Id rtId, boolean doDML){
        list<Application__c> apps = new list<Application__c>();
        for(Contact c: cons){
        	apps.add(makeApplication(c, rtId));
        }
        if(doDML == true){
            insert apps;
        }
        return apps;
    }
    
    public static Application__c makeApplication(Contact c, Id rtId){
        Application__c app = new Application__c();
		app.Applicant_Name__c=c.Id;
		app.Recordtypeid=rtId;
		app.Application_Status__c='Open';
        return app;
    }
    
    public static list<Authorizations__c> makeAuthorizations(list<Application__c> apps, Id rtId, Id thumbprintId, boolean doDML){
        list<Authorizations__c> auths = new list<Authorizations__c>();
        for(Application__c a:apps){
            auths.add(makeAuth(a, rtId, thumbprintId));
        }
        if(doDML == true){
            insert auths;
        }
        return auths;
    }
    
    public static Authorizations__c makeAuth(Application__c a, Id rtId, Id thumbprintId){
        Authorizations__c auth = new Authorizations__c();
        auth.Applicant__c = a.Applicant_Name__c;
        auth.RecordTypeID=rtId;
		auth.Status__c='Submitted';
		auth.Date_Issued__c=date.today();
		auth.Applicant_Alternate_Email__c='test@test.com';
		auth.UNI_Zip__c='32092';
		auth.UNI_Country__c='United States';
		auth.UNI_State__c = 'TEST';
		auth.UNI_County_Province__c='Duval';
        auth.Effective_Date__c = Date.today().addDays(5);
		auth.BRS_Proposed_Start_Date__c= auth.Effective_Date__c +30;
		auth.BRS_Proposed_End_Date__c= auth.Effective_Date__c +40;
		auth.CBI__c='CBI Text';
		auth.Application_CBI__c='No';
		auth.Applicant_Alternate_Email__c='email2@test.com';
		auth.UNI_Alternate_Phone__c='(904) 123-2345';
		auth.AC_Applicant_Email__c='email2@test.com';
		auth.AC_Applicant_Fax__c='(904) 123-2345';
		auth.AC_Applicant_Phone__c='(904) 123-2345';
		auth.AC_Organization__c='ABC Corp';
		auth.Means_of_Movement__c='Hand Carry';
		auth.Biological_Material_present_in_Article__c='No';
		auth.If_Yes_Please_Describe__c='Text';
		auth.Applicant_Instructions__c='Make corrections';
		auth.BRS_Number_of_Labels__c=10;
		auth.BRS_Purpose_of_Permit__c='Importation';
		auth.Documents_Sent_to_Applicant__c=false;
		auth.Expiration_Timeframe_Override__c = 'Manual';
		auth.Thumbprint__c = thumbprintId;
		return auth;
    }
    
    public static list<Label__c> makeLabels(list<Authorizations__c> auths, boolean doDML){
        string active = EFLGlobalConstants.getConstantValue('LABEL_STATUS_ACTIVE');
        list<Label__c> labels = new list<Label__c>();
        for(Authorizations__c a:auths){
            labels.add(makeLabel(a, active));
        }
        if(doDML == true){
            insert labels;
        }
        return labels;
    }
    
    public static Label__c makeLabel(Authorizations__c auth, string status){
        Label__c label = new Label__c();
        label.Authorization__c = auth.Id;
        label.Status__c = status;
		return label;
    }
    
    // Creates Account, Contact, Application, Authorization w/1 Label for each NUMBER_OF_RECORDS specified.
    public static void setupTestDataSuite(integer NUMBER_OF_RECORDS){
        CARPOL_BRS_TestDataManager d = new CARPOL_BRS_TestDataManager();
        d.insertcustomsettings();
        Id acctRTID = d.AccountRecordTypeId;
        list<Account> accts = makeAccounts(NUMBER_OF_RECORDS, acctRTID, true);
        
        list<Contact> cons = makeContacts(accts, true);
        
        Id appRTID = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        list<Application__c> apps = makeApplications(cons, appRTID, true);
        
        Id authRTID = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Id tId = d.newThumbprint().Id;
        //d.insertcustomsettings();
        
        list<Authorizations__c> auths = makeAuthorizations(apps, authRTID, tId, true);
        authIds = new list<Id>();
        for(Authorizations__c auth:auths){
            authIds.add(auth.Id);
        }
        
        list<Label__c> labels = makeLabels(auths, true);
    }
    
    public static Profile getProfileByName(string name){
        return [SELECT Id FROM Profile WHERE Name = :name];
    }
    
    public static User getUser(Profile p){
        return new User(Alias = 'standt', Email='standarduser' + getUnique() + '@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='aphisuser' + getRepeat() + '@testorg'+getUnique()+'.com');
    }
}