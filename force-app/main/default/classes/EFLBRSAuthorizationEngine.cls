public with sharing class EFLBRSAuthorizationEngine implements EFLIAuthorizationEngine {
    
    public static Map<id, id> relatedStateReviewMap = new Map<id, id>();
    public static Map<id, id> relatedCondtionsMap = new Map<id, id>();
    /*
* Purpose: change authorization record before inserting 
*/      
    public boolean validateAmendmentAction(authorizations__c authRecord){
        boolean bActionStatus;
        // bActionStatus = EFLAuthorizationEngineUtility.validateAmendmentAction( authRecord); 
        return bActionStatus; 
    }
    public boolean validateRenewalAction(authorizations__c authRecord){
        boolean bActionStatus;
        //bActionStatus=EFLAuthorizationEngineUtility.validateRenewalAction(authRecord);
        return bActionStatus; 
    }
    public void createAuthorization(authorizations__c authRecord){
        authRecord.Expiration_Timeframe_Override__c = EFLGlobalConstants.getConstantValue('EXPIRATION TIME FRAME OVERRIDE MANUAL');
        authRecord.Multi_Year__c = determineMultiYear(authRecord.BRS_Proposed_Start_Date__c,authRecord.BRS_Proposed_End_Date__c);
        authRecord.Expiration_Date__c = authRecord.Effective_Date__c.addYears(determineExpirationDuration(authRecord.BRS_Proposed_Start_Date__c,authRecord.BRS_Proposed_End_Date__c));
        
        if(authRecord.BRS_Introduction_Type__c == 'Interstate Movement and Release'){
            authRecord.Interstate_Movement_Expiration_Date__c = authRecord.Effective_Date__c.addYears(1); 
        }
        authRecord.Workflow_Number__c = EFLAuthorizationEngineUtility.getWorkflowNumber(authRecord);
        authRecord.Activity_Sequence__c = 0;
        
    }
    
    /*
* Purpose: Determine Expiration durationthanks
*/ 
    private integer determineExpirationDuration(date startDate,date endDate){
        
        date newDate = startDate.addYears(1);
        if(newDate < endDate){
            return 3;
        } 
        return 1;
    } 
    /*
* Purpose: Determine Multi Year
*/
    private boolean determineMultiYear(date startDate,date endDate){
        
        date newDate = startDate.addYears(1);
        if(newDate < endDate){
            return true;
        } 
        return false;
    }     
    
    
    /*
* Purpose: validate Authorization record
*/      
    public boolean validate(authorizations__c authRecordOld, authorizations__c authRecord){
        
        return EFLAuthorizationEngineUtility.validate(authRecordOld, authRecord); 
        
    }
     /*
* Purpose: getting the data of authorizations to validate status.
*/      
    public static void getAuthorizationData(list<authorizations__c> authorizationList){
        
         EFLBRSAuthCheckStatusUtility.getDataForAuthStatusValidation(authorizationList); 
        
    }
     /*
* Purpose: getting the data of authorizations to validate status.
*/      
    public static void validateLineItemsWithAuthorizations(authorizations__c authRecord){
        
        EFLBRSAuthCheckStatusUtility.validateAuthLineItemRelatedStatus(authRecord); 
        
    }
    
    /*
* Purpose: update Authorization record
*/      
    public void updateAuthorization(authorizations__c authRecordOld, authorizations__c authRecord){
        
        Date AuthDate; 
        Date expAuthDate; 
        Date authexpdt;
        Integer timeFrameOverride = null;
        
        if (authRecord.Permit_Number__c == null && authRecord.stage__c!=NULL){
               if(EFLAuthorizationEngineUtility.checkValidStage(authRecord.workflow_Number__c,authRecord.stage__c) )
                    authRecord.Permit_Number__c = EFLAuthorizationEngineUtility.getPermitNumber(authRecord); 
            }
                
        if((authRecord.Authorization_Type__c == EFLGlobalConstants.getConstantValue('AUTHORIZATION DECISION TYPE PERMIT') ||
            authRecord.Authorization_Type__c == EFLGlobalConstants.getConstantValue('AUTHORIZATION DECISION TYPE NOTIFICATION'))&& 
           authRecordOld.status__c != EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS ISSUED') && 
           authRecord.status__c != EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS ISSUED') && 
           authRecord.Expiration_Timeframe_Override__c != EFLGlobalConstants.getConstantValue('EXPIRATION TIME FRAME OVERRIDE MANUAL'))
        {
            //Issued and Effective Logic
            authRecord.Date_Issued__c = System.today();
            
            if((authRecord.Effective_Date_Equals__c == null || 
                authRecord.Effective_Date_Equals__c ==EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS ISSUED')  || 
                authRecord.Effective_Date_Equals__c == 'Allow Manual') && 
               !authRecord.Manually_Set_Effective__c )
            {
                if(authRecord.Date_Issued__c!=null)
                    authRecord.Effective_Date__c = authRecord.Date_Issued__c;
            }
            
            //Expiration Logic
            Integer ExpirationTimeframe;
            if(authRecord.Effective_Date__c!=null)
                AuthDate =  Date.newInstance(authRecord.Effective_Date__c.Year(),authRecord.Effective_Date__c.month(), authRecord.Effective_Date__c.day());
            
            if((authRecordOld.Expiration_Date__c != authRecord.Expiration_Date__c) || 
               authRecord.Expiration_Timeframe_Override__c == EFLGlobalConstants.getConstantValue('EXPIRATION TIME FRAME OVERRIDE MANUAL'))
            {
                
            }                    
            else if(authRecord.Expiration_Timeframe_Override__c == 'Pathway' || authRecord.Expiration_Timeframe_Override__c == null)
            {
                ExpirationTimeframe = integer.valueOf(authRecord.Expiration_Timeframe__c); 
                authexpdt = AuthDate.addDays(ExpirationTimeframe); 
            }
            else
            {
                if(authRecord.Expiration_Timeframe_Override__c !=null){
                    timeFrameOverride = integer.valueOf(authRecord.Expiration_Timeframe_Override__c.substringBefore(' '));
                    if(authRecord.Expiration_Timeframe_Override__c.contains('Month'))
                    {
                        authexpdt = AuthDate.addMonths(timeFrameOverride);
                    }
                    else if(authRecord.Expiration_Timeframe_Override__c.contains('Year'))
                    {
                        authexpdt = AuthDate.addYears(timeFrameOverride);
                    }
                } 
            }
            
            if(authRecord.Expiration_Date__c != authexpdt )   
                authRecord.Expiration_Date__c = authexpdt;  
            
        }
        
        //To make Expiry_Date__c and Expiration_Date__c in sync 
        //Later after merging both fields we need to remove below logic 
        if((authRecordOld.Expiration_Date__c != authRecord.Expiration_Date__c)){
            authRecord.Expiry_Date__c =  authRecord.Expiration_Date__c;
        }
        if((authRecordOld.Expiry_Date__c != authRecord.Expiry_Date__c)){
            authRecord.Expiration_Date__c = authRecord.Expiry_Date__c;
        }
        // End of the Expiration Date Sync Logic
        
        if(authRecord.status__c == EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS ISSUED') ||
           authRecord.status__c == EFLGlobalConstants.getConstantValue('AUTHORIZATION STATUS APPROVED') )
        /*{
            authRecord.Date_Issued__c = system.today();   
        }*/
        
        //Determine Mulit year
        authRecord.Multi_Year__c = determineMultiYear(authRecord.Effective_Date__c,authRecord.Expiration_Date__c);
        authRecord.Workflow_Number__c = EFLAuthorizationEngineUtility.getWorkflowNumber(authRecord);
        
        if((authRecordOld.Stage__c != authRecord.Stage__c)){
            authRecord.Ready_for_next_stage__c =  false;
        }
        
    }
    
    /*
* Purpose: handle email communication
*/  
    public void handleCommunication(authorizations__c authRecord){
        EFLAuthorizationEngineUtility.sendEmail(authRecord);  
    }    
    
    /*
* Purpose: load Activities 
*/
    public void loadActivities(authorizations__c authRecord,list<sObject> activityList){
        
        // EFLActivityFactory.loadActivities(authRecord, activityList);
        
        //Validate Teams
 
        //validate Activity Tasks
        
        //Check if we are Last Task
        
        //Populate Next Tasks and increment Sequence of authorization
        
        list<team__c> teamMembersList = new list<team__c>();
        // authorizations__c authrecord = (authorizations__c)parentRecord;
        
        teamMembersList = EFLTeamRepository.selectByAuthorizationID(authrecord.ID); 
        ID ownerID;
        
        system.debug('team member list: ' + teamMembersList);
        
       
        
        if(!teamMembersList.isEmpty()){
            system.debug('authrecord '+ authrecord);
            integer taskSequence = authRecord.Activity_Sequence__c==null?1:authRecord.Activity_Sequence__c.intvalue();
            system.debug('taskSequence '+ taskSequence);
            Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(authrecord.stage__c, authrecord.Workflow_Number__c,taskSequence + 1);
            system.debug('currentActivityCMD '+ currentActivityCMD);
           
            
        
          /*
         
        if(TeamMemberMap.get('Program Specialist') == null){
            trigger.new[0].addError('Add Program Specialist before continuing to the Application Review Stage.');
        }
        if(TeamMemberMap.get('BRS Biotechnologist') == null){
            trigger.new[0].addError('Add BRS Biotechnologist before continuing to the Application Review Stage.');
        }
        if(TeamMemberMap.get('BRAP Manager') == null){
            trigger.new[0].addError('Add BRS Biotechnologist before continuing to the Application Review Stage.');
        }

*/
           /* if(currentActivityCMD == NULL && authrecord.stage__c != null ){ 
                throw new activityException('Please configure activities for this Stage.');
            }  */
                if(currentActivityCMD != NULL )
                {
                    /* if(EFLActivityUtility.isLastActivityForStage(currentActivityCMD))
                        {
                            authrecord.Ready_for_next_stage__c = true;
                        }
                        else
                        {
                            authrecord.Ready_for_next_stage__c = false;
                        }*/
                
                for(team__c t : teamMembersList){
                    if(currentActivityCMD.Assigned_To__c == t.member_role__c){
                        ownerID = t.Member__c;
                        break;
                    }
                } 
                if(ownerID == null){ 
                    //throw new activityException('Please assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
                    trigger.new[0].addError('Please assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
                }
                activityList.add(EFLActivityUtility.populateTask( authrecord, currentActivityCMD, ownerID ));
                system.debug('Before Activity_Sequence__c '+authRecord.Activity_Sequence__c);
                authRecord.Activity_Sequence__c = authRecord.Activity_Sequence__c + 1;
                system.debug('After Activity_Sequence__c '+authRecord.Activity_Sequence__c);
                
            }
            
           /* if( authRecordOld.stage__c == 'Application Review' && !EFLActivityUtility.needsTaskCreation(authRecordOld,authRecord) ){
            system.debug('stage update');
            if(!authRecord.Applicant_Compliance_Check__c){
                system.debug('compliance check');
                authRecord.addError('Please complete compliance check');  
                   return false;
            }
        }*/
       
        }
        
        else{

               trigger.new[0].addError('Please assign Team Members.');
            
        }
        
    }
    
    public String validateRoles(authorizations__c authRecord){
        String errorMessage = null;
         list<team__c> teamMembersList = new list<team__c>();
        // authorizations__c authrecord = (authorizations__c)parentRecord;
        system.debug('Authorization Record: ' + authRecord);
        teamMembersList = EFLTeamRepository.selectByAuthorizationID(authRecord.ID); 
        ID ownerID;
      
         // W025577 Terry 10-1-18
        map<String,boolean> TeamMemberMap = new map<String,boolean>();
        for(team__c currentMember : teamMembersList){
            if(!String.isBlank(currentMember.Member__c) && !String.isBlank(currentMember.Member_Role__c))
               { 
                 TeamMemberMap.put(currentMember.Member_Role__c.trim(),true);
               }
         } 
        system.debug('Current Team Map: ' + TeamMemberMap);
        system.debug('errorMessage Before# ' + errorMessage);
            //  W-025577 Terry 10-1-18
        List<String> currentRequiredRoles = EFLProgramPrefixUtility.getRequiredRoles(authrecord.Workflow_Number__c);
        string missedRoles = '';
        system.debug('size@@@'+currentRequiredRoles.size());
         system.debug('currentRequiredRoles # ' + currentRequiredRoles);
            for(String reqRole : currentRequiredRoles){
                 system.debug('testing required roles: ' + reqRole); 
                system.debug('testing required roles test result##' + TeamMemberMap.get(reqRole.trim()));

                if(TeamMemberMap.get(reqRole.trim()) == null){
                    //errorMessage = 'Error: Add ' + reqRole + ' before continuing to the Application Review Stage.';
                    if(missedRoles!=''){
                         missedRoles = missedRoles + ',' + reqRole;
                    }else{
                        missedRoles = reqRole;
                    }
                     
                    system.debug('missedRoles loop# ' + missedRoles);
                }
                
            }
         system.debug('missedRoles after# ' + missedRoles);
        if(missedRoles!=''){
            errorMessage = 'Error: Add ' + missedRoles + ' before continuing to the Application Review Stage.';
        }

        system.debug('errorMessage after# ' + errorMessage);
        return errorMessage;
    }

    public Application__C processAmendment(authorizations__c authRecord, String actionType){
        //Amendment
        system.debug('authRecord::' +authRecord);
        System.debug('actionType::' +actionType);
        List<Application__c> applst = [SELECT Applicant_Name__c,RecordtypeId, Applicant_Email__c,Applicant_Phone__c,Applicant_Fax__c,Organization__c,Application_Status__c,
                                              Applicant_Address__c, US_Address__c FROM Application__c WHERE ID = :authRecord.Application__c];
        
         
        //RR  4/16 - begin
        if(applst != null && applst.size() > 0){
        //RR  4/16 - end
        system.debug('1::::');
        id currentAppId = applst[0].id ;
        Application__c newapp;
        // EFLCloneApplicationUtility createapp = new EFLCloneApplicationUtility();
        EFLCloneApplicationHandler createapp = new EFLCloneApplicationHandler();

        Savepoint sp = Database.setSavepoint();
        try {
            newapp = New Application__c();
            newapp = createapp.CloneApplication(CurrentAppid, authRecord, actionType);
        } catch(Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
        }
        return newApp;
            }
        //RR  4/16 - begin
        else{
            System.debug('EFLBRSAuthorizationEngine.processAmendment() return 0 applications');
            return null;
                }
        //RR  4/16 - end
    }
    
    
    public void shareAuthorizationToAccount(list<Authorizations__c> auths){
        EFLAccountSharingUtility.shareChildrenFromParent(auths, 'BRS');
    }
    
    public class activityException extends Exception {}

}