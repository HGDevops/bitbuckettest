public with sharing class EFLApplicationChangeHistory{ 

// External variables
public SObject myObject {get; set;}
public Integer recordLimit {get; set;}
public static String objectLabel {get;}
public String lookupField {get;set;}


// Internal Variables
public list<change_history__c> objectHistoryList;
public list<change_history__c> newentryhistorylist;
public list<change_history__c> deletehistorylist;
 

/******
 * @Method: getobjectHistoryList()
 * @Purpose: Get list of all change history for that line item and its related records 
 * @Return: returns the record specific history
 ************/
  public List<change_history__c> getobjectHistoryList(){
     Id myObjectId = String.valueOf(myObject.get('Id'));
   //  objectHistoryList =  CARPOL_BRS_AuthorizationPDFExtension.historymap.get(myObjectId);
     return objectHistoryList;
  }

/******
 * @Method: getnewentryhistorylist() 
 * @Purpose: Get list of all new history records for that line item - related records
 * @Return: returns the section specific history
 ************/  
  public List<change_history__c> getnewentryhistorylist(){
     // newentryhistorylist =  CARPOL_BRS_AuthorizationPDFExtension.newentryhistorymap.get(lookupField); 
     return newentryhistorylist;
  }
/******
 * @Method: getdeletehistorylist()
 * @Purpose: Get list of all new history records for that line item - related records
 * @Return: returns the section specific history
 ************/ 
  public List<change_history__c> getdeletehistorylist(){
    // deletehistorylist =  CARPOL_BRS_AuthorizationPDFExtension.deletehistorymap.get(lookupField); 
     return deletehistorylist;   
  } 
  
  
}