public Interface EFLIPreScreeningProcessor {
     
    void updateMovementAndPathway (string movementType, string pathway);
    void getNextQuestion(EFLPreScreeningWrapper psw);
    void getPrevQuestion(EFLPreScreeningWrapper psw);
    List<SelectOption> getQuestionOptions (EFLPreScreeningWrapper psw);

}