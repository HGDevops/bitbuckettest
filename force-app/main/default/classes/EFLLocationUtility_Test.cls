@IsTest
Public class EFLLocationUtility_Test {
    private static List<location__c> allLocations;
    private static CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
    private static AC__c lineItem1;
    private static id olocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin');
    private static id dlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Destination');
    private static id oanddlocrectypeid=EFLGenericUtility.getrecordtypeId('Location Origin and Destination');
    private static id releaserectypeid=EFLGenericUtility.getrecordtypeId('Location Release Sites');

    static Country__c country;
    static Level_1_Region__c lr;
    static Level_2_Region__c lvl2Reg;
    static Id RtId;
    static Location__c loc;
    static List<location__c> finalLocationList = new List<Location__c>();
   
    //prepare line Item
     private static AC__c getLineItem()
    {
        String AccountRecordTypeId = testData.AccountRecordTypeId; 
        testData.insertcustomsettings();
		Account objacct=testData.newAccount(AccountRecordTypeId);
		Contact objcont=testData.newcontact();
		Application__c objapp=testData.newapplication();
		Program_Line_Item_Pathway__c plip=testData.newCaninePathway();
		AC__c lineItem=testData.newLineItemBRS('Personal Use',objapp);
		Attachment attach=testData.newattachment(lineItem.Id);        
        lineItem.construct_Status__c = 'Yet to Add';
        return lineItem;
        
    }

   //Prepare all record type locations
    private static void setupLocations() {
        
        allLocations = new List<Location__c> ();
        lineItem1 = getLineItem();
       
        country = testData.newcountryus();
        lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=country.Id;
        insert lr;
      
        lvl2Reg = testData.newlevel2region(lr.id);

        loc = testData.newlocation(country.id,lr.id,lvl2Reg.id,lineItem1.id,olocrectypeid);
        allLocations.add(loc);
        loc = testData.newlocation(country.id,lr.id,lvl2Reg.id,lineItem1.id,oanddlocrectypeid);
        allLocations.add(loc); 
        loc = testData.newlocation(country.id,lr.id,lvl2Reg.id,lineItem1.id,dlocrectypeid);
        allLocations.add(loc);
        loc = testData.newlocation(country.id,lr.id,lvl2Reg.id,lineItem1.id,releaserectypeid);
        allLocations.add(loc); 
        loc = testData.newlocation(country.id,lr.id,lvl2Reg.id,lineItem1.id,releaserectypeid);
        allLocations.add(loc); 
                
    }    
      
    //Check if all locations exist
      @IsTest 
       static void testCheckLocations() {
  
           setupLocations();          
          Test.startTest(); 
           finalLocationList = EFLLocationUtility.getSortedLocations(lineItem1.Id); 
           system.assert(finalLocationList!=null);
          Test.stopTest();   
      }    

}