public inherited sharing class EFLGPSConvertDecimal {
    public static decimal converttoDecimal(Decimal toConvert){
        if(toConvert != null){
        Decimal rounded = toConvert.setScale(6);
        return rounded;
            
        }
        return null;
    }

}