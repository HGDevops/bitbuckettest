public with sharing class InspectionPlantingInformationCtrl {
    public Inspection__c inspection{get;set;}
    public EFL_Inspection_Questionnaire__c Questionnaire {get;set;}
    public List<EFL_Inspection_Questionnaire__c> EFLQuestionnaireList = new List<EFL_Inspection_Questionnaire__c>();
    public String SiteofInspection{get;set;}
    //public Self_Reporting__c PlantingId{get;set;}
    public String PlantingInfo {get;set;}
    //public boolean RenderLocation{get;set;}
    public string releasestartdate{get;set;}
    public string releaseenddate{get;set;}
    public string harvestdate {get;set;}
    public InspectionPlantingInformationCtrl(ApexPages.StandardController controller) {
        Id inspectionId = ApexPages.currentPage().getParameters().get('id');
        inspection = [SELECT Id,Name,Status__c, APHIS_Inspection_ID__c ,EFL_Inspector__c ,Authorization__r.Name, Authorization__r.AC_Organization__c , Authorization__r.Mailing_Address__c,Authorization__r.Application__r.Name ,EFL_Inspector__r.Name, EFL_Inspector__r.Phone, Application__r.Applicant_Name__r.Name,
                      Inspection_Co_ordinates__Latitude__s, Inspection_Co_ordinates__Longitude__s, Inspection_Co_ordinates__c, Inspection_Comments__c,Facility_Features__c, Incident__c, 
                      Inspection_Due_Date__c, Scheduled_Date_of_Inspection__c, Inspector_Email__c,Actual_Inspection__c, Time_Spent_Preparing_for_the_Inspection__c, Time_Spent_Conducting_the_Inspection__c, Responsible_Party__c, Responsible_Party__r.Name, Responsible_Party__r.Phone, Responsible_Party__r.Email, Responsible_Party__r.MailingAddress, Responsible_Party__r.Account.Name,
                      Time_Spent_Creating_and_Revising_Report__c, Time_Spent_Travelling_to_and_from_Site__c, Total_Miles_Driven_to_and_from_Site__c, Authorization__c, Corrective_Actions_Text__c, 
                      Inspection_Results_Summary__c, Scheduled_Time_of_Inspection__c,Application__c,Application__r.Name,Application__r.Applicant_Name__c,Application__r.Organization__c,Application__r.Applicant_Address__c, Authorization__r.AC_Applicant_Fax__c,Authorization__r.AC_Applicant_Phone__c,
                      Application__r.Applicant_Fax__c,Authorization__r.Applicant__r.Name,Facility__r.Name,Facility__r.CBP_Port_Number__c,Facility__r.Address_1__c,Facility__r.Address_2__c,Facility__r.Country__r.Name,Facility__r.Office_Name__c,Facility__r.Office_Phone__c,
                      Facility__r.Office_Email__c,Inspector__r.Name,Inspector__r.Phone,EFL_Inspector__r.Email ,(SELECT Subject, 
                                                                                                                EndDateTime, StartDateTime FROM Events), (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId FROM OpenActivities) FROM Inspection__c WHERE id=:inspectionId];
        
        EFLQuestionnaireList = [SELECT ID,Name FROM EFL_Inspection_Questionnaire__c WHERE Inspection__c=:inspectionId];
        
        if(EFLQuestionnaireList.size()!=0){
            Questionnaire = [SELECT ID,Name, Location__c, Location__r.Name,Location__r.Street_Add1__c,Location__r.Street_Add2__c, Location__r.Street_Add3__c,Location__r.Street_Add4__c,Location__r.State__r.Name,Location__r.Country_Text__c, Location__r.Zip__c, Location__r.County_Text__c,Location__r.City__c, Location__r.Proposed_Planting__c, 
                             Location__r.Number_of_Acres_Text__c, Location__r.Location_Unique_Id__c, Location__r.Release_History__c, Location__r.Critical_Habitat_Involved__c, Location__r.If_Yes_Please_Explain__c, 
                             Location__r.GPS_Coordinates_1__Latitude__s,Location__r.GPS_Coordinates_2__Latitude__s,Location__r.GPS_Coordinates_3__Latitude__s,Location__r.GPS_Coordinates_4__Latitude__s,Location__r.GPS_Coordinates_5__Latitude__s,Location__r.GPS_Coordinates_6__Latitude__s, Location__r.GPS_Coordinates_1__Longitude__s, Location__r.GPS_Coordinates_2__Longitude__s,Location__r.GPS_Coordinates_3__Longitude__s,Location__r.GPS_Coordinates_4__Longitude__s,Location__r.GPS_Coordinates_5__Longitude__s,Location__r.GPS_Coordinates_6__Longitude__s,
                             Location__r.GPS_Coordinates_1_Text__c, Location__r.GPS_Coordinates_2_Text__c, Location__r.GPS_Coordinates_3_Text__c, Location__r.GPS_Coordinates_4_Text__c, Location__r.GPS_Coordinates_5_Text__c, Location__r.GPS_Coordinates_6_Text__c,
                             Planting_id__c,Planting_id__r.Name,Planting_id__r.Planting_ID__c, Planting_id__r.Anticipated_Harvest_Destruct_Date__c, Planting_id__r.Quantity_Acres__c, Planting_id__r.Construct_Name__c,
                             Planting_id__r.GPS_Coordinates_1_Text__c,Planting_id__r.GPS_Coordinates_2_Text__c,Planting_id__r.GPS_Coordinates_3_Text__c,Planting_id__r.GPS_Coordinates_4_Text__c,Planting_id__r.GPS_Coordinates_5_Text__c,Planting_id__r.GPS_Coordinates_6_Text__c,
                             Location__r.Line_Item__r.Release_Start_Date__c,Location__r.Line_Item__r.Release_end_Date__c
                             
                             FROM EFL_Inspection_Questionnaire__c WHERE Inspection__c=:inspectionId LIMIT 1];
            //RenderLocation = true;
            if(Questionnaire.Location__c!=null){
                SiteofInspection = Questionnaire.Location__r.Name;
                PlantingInfo = Questionnaire.Planting_ID__r.Name;
                if(Questionnaire.Location__r.Line_Item__r.Release_Start_Date__c!=null& Questionnaire.Location__r.Line_Item__r.Release_end_Date__c!=null){
                    Date releasestart = Questionnaire.Location__r.Line_Item__r.Release_Start_Date__c;
                    releasestartdate = releasestart.format();
                    Date releaseend = Questionnaire.Location__r.Line_Item__r.Release_Start_Date__c;
                    releaseenddate =releaseend.format();
                    //system.debug('st>>>>'+releasestartdate);
                    //system.debug('en'+releaseenddate);
                }
                if(Questionnaire.Planting_ID__r.Anticipated_Harvest_Destruct_Date__c!=null){
                    date harvdate;
                    harvdate = Questionnaire.Planting_ID__r.Anticipated_Harvest_Destruct_Date__c;
                    harvestdate =  harvdate.format();
                }
            }
        }else{
            //RenderLocation = false;
        }    
    }
    
}