public with sharing class EFLACAuthorizationEngine implements EFLIAuthorizationEngine {

    /*
    * Purpose: change authorization record before inserting
    */
    public void createAuthorization(authorizations__c authRecord){
    if(!Test.isRunningTest()){
    authRecord.Workflow_Number__c = getWorkflowNumber(authRecord);
    authRecord.Activity_Sequence__c = 0;
    }
    }
   /*
    * Purpose: Get workflow Number
    */    
    public static string getWorkflowNumber(authorizations__c authRecord) {
        string workflowNumber;
       // System.debug('In getwfnum authRecord>>'+authRecord);
        System.debug('In getwfnum authRecord Authorization_Type__c>>'+authRecord.Authorization_Type__c);
        System.debug('In getwfnum authRecord BRS_Introduction_Type__c>>'+authRecord.BRS_Introduction_Type__c);
        System.debug('In getwfnum authRecord Prefix__c >>'+authRecord.Prefix__c );
        workflowNumber = [Select label  
                                 from EFLPermitPrefixNumber__mdt 
                                 where Decision_Type__c=:authRecord.Authorization_Type__c 
                                 and Movement_Type__c=:authRecord.BRS_Introduction_Type__c 
                                 and Program_Prefix__c =:authRecord.Prefix__c LIMIT 1].label;
        //system.debug('Workflow Number: ' + workflowNumber);
            return workflowNumber;
    }
    /*
    * Purpose: validate Authorization record
    */
    public boolean validate(authorizations__c authRecordOld, authorizations__c authRecord) {
        return EFLAuthorizationEngineUtility.validate(authRecordOld, authRecord); 
    }

    /*
    * Purpose: update Authorization record
    */
    public void updateAuthorization(authorizations__c authRecordOld, authorizations__c authRecord) {
        EFLAuthorizationEngineUtility.updateAuthorization(authRecordOld, authRecord);
        if((authRecordOld.Stage__c != authRecord.Stage__c)){
            authRecord.Ready_for_next_stage__c =  false;
        }
    }

    /*
    * Purpose: handle email communication
    */
    public void handleCommunication(authorizations__c authRecord){
        EFLAuthorizationEngineUtility.sendEmail(authRecord);  
    }

    /*
    * Purpose: load Activities 
    */
    public void loadActivities(authorizations__c authRecord,list<sObject> activityList){
        // EFLActivityFactory.loadActivities(authRecord, activityList);
               
        //Validate Teams
 
        //validate Activity Tasks
        
        //Check if we are Last Task
        
        //Populate Next Tasks and increment Sequence of authorization
        
        list<team__c> teamMembersList = new list<team__c>();
        // authorizations__c authrecord = (authorizations__c)parentRecord;
        
        teamMembersList = EFLTeamRepository.selectByAuthorizationID(authrecord.ID); 
        ID ownerID;
        
       // system.debug('team member list: ' + teamMembersList);
        
       
        
        if(!teamMembersList.isEmpty()){
           // system.debug('authrecord '+ authrecord);
            integer taskSequence = authRecord.Activity_Sequence__c==null?1:authRecord.Activity_Sequence__c.intvalue();
           // system.debug('taskSequence '+ taskSequence);
            Program_Activity__mdt currentActivityCMD = EFLActivityUtility.getProgramActivity(authrecord.stage__c, authrecord.Workflow_Number__c,taskSequence + 1);
            //system.debug('currentActivityCMD '+ currentActivityCMD);
                     
        
                if(currentActivityCMD != NULL )
                {
                                    
                for(team__c t : teamMembersList){
                    if(currentActivityCMD.Assigned_To__c == t.member_role__c){
                        ownerID = t.Member__c;
                        break;
                    }
                } 
                if(ownerID == null){ 
                    //throw new activityException('Please assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
                    trigger.new[0].addError('Please assign Team Member for role '+ currentActivityCMD.Assigned_To__c);
                }
                activityList.add(EFLActivityUtility.populateTask( authrecord, currentActivityCMD, ownerID ));
               // system.debug('Before Activity_Sequence__c '+authRecord.Activity_Sequence__c);
                authRecord.Activity_Sequence__c = authRecord.Activity_Sequence__c + 1;
               // system.debug('After Activity_Sequence__c '+authRecord.Activity_Sequence__c);
                
            }
                   
        }
        
        else{

               trigger.new[0].addError('Please assign Team Members.');
            
        }
        
    }
    
    public String validateRoles(authorizations__c authRecord){
        String errorMessage = null;
         list<team__c> teamMembersList = new list<team__c>();
        // authorizations__c authrecord = (authorizations__c)parentRecord;
      //  system.debug('Authorization Record: ' + authRecord);
        teamMembersList = EFLTeamRepository.selectByAuthorizationID(authRecord.ID); 
        ID ownerID;
      
         // W025577 Terry 10-1-18
        map<String,boolean> TeamMemberMap = new map<String,boolean>();
        for(team__c currentMember : teamMembersList){
            if(!String.isBlank(currentMember.Member__c))
               { 
                 TeamMemberMap.put(currentMember.Member_Role__c.trim(),true);
               }
         } 
       // system.debug('Current Team Map: ' + TeamMemberMap);
       // system.debug('errorMessage Before# ' + errorMessage);
            //  W-025577 Terry 10-1-18
        List<String> currentRequiredRoles = EFLProgramPrefixUtility.getRequiredRoles(authrecord.Workflow_Number__c);
        string missedRoles = '';
      //  system.debug('size@@@'+currentRequiredRoles.size());
        // system.debug('currentRequiredRoles # ' + currentRequiredRoles);
            for(String reqRole : currentRequiredRoles){
                // system.debug('testing required roles: ' + reqRole); 
                //system.debug('testing required roles test result##' + TeamMemberMap.get(reqRole.trim()));

                if(TeamMemberMap.get(reqRole.trim()) == null){
                    //errorMessage = 'Error: Add ' + reqRole + ' before continuing to the Application Review Stage.';
                    if(missedRoles!=''){
                         missedRoles = missedRoles + ',' + reqRole;
                    }else{
                        missedRoles = reqRole;
                    }
                     
                    //system.debug('missedRoles loop# ' + missedRoles);
                }
                
            }
        // system.debug('missedRoles after# ' + missedRoles);
        if(missedRoles!=''){
            errorMessage = 'Error: Add ' + missedRoles + ' before continuing to the Application Review Stage.';
        }

       // system.debug('errorMessage after# ' + errorMessage);
        return errorMessage;
    }
    

    public Application__c processAmendment(authorizations__c authRecord, String actionType){
        Application__c newApp = new Application__c();
        return newApp;
    }
    
     
}