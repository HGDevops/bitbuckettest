@isTest
public class  OrderInspQuestionnaireQuestions_Test {
   

    static testmethod void setupData(){
        EFL_Inspection_Questions_Template__c inspTmpn = new EFL_Inspection_Questions_Template__c();
         inspTmpn.Name='test';
         insert inspTmpn;
        
        Inspection__c Insn = new Inspection__c();
        String IncRecTypeIDn = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
        Insn.RecordTypeId = IncRecTypeIDn;
        Insn.Stage__c = 'Inspection Assignment';
        Insn.status__c='Cancelled';
        Insn.Reason_for_Cancellation__c = 'testing';
        Insn.Program__c='BRS';
        Insn.Activity_Sequence__c = 0;
        Insn.EFL_Inspection_Questionnaire_Template__c=inspTmpn.id;
        insert Insn;
    }
    

    static testmethod void runTest(){
        setupData();
        Inspection__c oInspection = [Select Id From Inspection__c limit 1];
        EFL_Inspection_Questionnaire_Questions__c recEIQ=new EFL_Inspection_Questionnaire_Questions__c ();
        recEIQ.Question__c='test question1';
        recEIQ.Response__c = 'response1';
        recEIQ.Answer__c='answer';
        recEIQ.Comments__c ='ytest' ;
        recEIQ.Inspection__c=oInspection.id;
        recEIQ.Options__c='Finalize Questionnaire';
        
        List<EFL_Inspection_Questionnaire_Questions__c> toUpdateList = new List<EFL_Inspection_Questionnaire_Questions__c>();
        toUpdateList.add (new EFL_Inspection_Questionnaire_Questions__c(Inspection__c=oInspection.id,Question__c='test question1',Response__c = 'response1',Answer__c='answer',Options__c='Finalize Questionnaire',Comments__c ='c-test'));
        toUpdateList.add(recEIQ);
        
         ApexPages.CurrentPage().getParameters().put('id', oInspection.Id);
        Inspection__c asd = new Inspection__c ();
        ApexPages.StandardController sc = new ApexPages.StandardController(asd );
         OrderInspectionQuestionnaireQuestions oHandler = new  OrderInspectionQuestionnaireQuestions(sc);
         oHandler.back();
         oHandler.init();
       
       
    }
   

}