/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class CARPOL_UNI_RecordTypeTest{ 

    @IsTest(SeeAllData=false) public static void CARPOL_UNI_RecordTypeTest() {
        //String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();         
        
        // Instantiate a new controller with all parameters in the page
        CARPOL_UNI_RecordType controller = new CARPOL_UNI_RecordType();
        
        string n = [SELECT Name FROM RecordType WHERE SObjectType = 'Account' LIMIT 1].Name;
        CARPOL_UNI_RecordType.getObjectRecordTypeId(schemaMap.get('Account'),n);
        CARPOL_UNI_RecordType.getObjectRecordTypeName(schemaMap.get('Account'),AccountRecordTypeId);        
        System.assert(controller != null); 
        
    }
    
}