@istest
public class EFLWizardActivityFactory_Test {
    static testmethod void method1(){
        system.test.starttest();
        EFLWizardFlow eflFlw = new EFLWizardFlow();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        Program_Line_Item_Pathway__c testPathway = testData.newCaninePathwayImport();
         testPathway.Display_Regulated_Article_Search__c = true;
         testPathway.Display_Component_Selection__c = true;
         testPathway.Is_Scientific_Name_Required__c = true;
         testPathway.Is_Country_of_Export_Required__c = true;
         testPathway.Is_Country_of_Transit_Required__c = true;
         testPathway.Show_Intended_Use__c = true;
         testPathway.Country_of_Destination_required_for__c = 'Import'; // just adding to make test pass
         testPathway.EFLDiseaseValidationRequired__c = true;
         testPathway.Is_Scientific_Name_Required__c = true;//vv 3-7-17         
         update testPathway;
         Intended_Use__c testIU = testData.newIntendedUse(testPathway.Id);
         Wizard_Questionnaire__c testWQ = testData.newWizardQuestionnaire(testPathway.Id, testIU.Id);
         Wizard_Selections__c wzsel1 = new Wizard_Selections__c();
         wzsel1.Wizard_Questionnaire__c = testWQ.id;
         wzsel1.Value__c = 'Test 1'; 
         wzsel1.name = 'Test 1'; 
         wzsel1.Activity_Processor_Result__c = 'Test Resilt'; 
         insert wzsel1;
        List<Wizard_Selections__c> Options = new List<Wizard_Selections__c>();
        Options.add(wzsel1);
        EFLWizardActivity ewizact = new EFLWizardActivity(eflFlw,testWQ,Options);
        ewizact.EvaluatedOption = wzsel1;
        ewizact.SelectedOption =  wzsel1.id;
        ewizact.activityResult = 'Test Resilt';
        EFLWizardActivityFactory.getActivityProcessor(ewizact);
        EFLWizardActivityFactory.getImplementation(ewizact);
         
        system.test.stoptest();
    }
}