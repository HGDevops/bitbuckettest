/*
 *Purpose: Handles application Submission and thrown Success/Error Messages and any errors while submitting the application both on Community and Internal side.
 */ 
public with sharing class UpdateApplication {
    
    public Application__c application = new Application__c();
    public ID applicationID;
    
    public string getERRMESSAGE(){
         return(EFLGenericUtility.getMessage('Something Went Wrong'));
        } 
    
    public UpdateApplication(ApexPages.StandardController controller)
    {
        applicationID = EFLGenericUtility.sanitizeString( ApexPages.CurrentPage().getparameters().get('id') );
        application = (Application__c)controller.getRecord();
    }
    
    public PageReference goBackApplication()
    {
        PageReference applicationDetailsRedirect = new PageReference('/' + application.Id);
        applicationDetailsRedirect.setRedirect(true);
        return applicationDetailsRedirect;
    }
    
    public PageReference appUpdate()
    {
        try
        {
            application = [select id, Name,Application_Status__c 
                             from Application__c 
                            where id =: applicationID 
                            limit 1 FOR UPDATE];
            //Check if application is already submitted and throw information
            if(application.Application_Status__c !='Submitted'){
                application.Application_Status__c = 'Submitted'; 
                application.Agreed_and_certified__c = true; 
                update application; //This generates the authorization.
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,EFLGenericUtility.getMessage('Application Submitted')));
            }else{
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,EFLGenericUtility.getMessage('Application prev Submitted'))); 
            }
        }
        catch(exception e)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        
        return null;
    }
    
}