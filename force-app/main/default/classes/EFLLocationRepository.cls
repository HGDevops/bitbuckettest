/*
 * Purpose: Central place to access location object from database with different set of filter parameters.
*/ 
public with sharing class EFLLocationRepository {
    
    // Obtain Location records using line Item ID    
   /* public static List<Location__c> selectByLineItemID(Id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
            return [SELECT ID,Name ,RecordType.Name,Contact_Name1__c, 
                           Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                           Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                           Day_Phone_2__c,Zip__c,Other_Material_Types__c,
                           Proposed_Planting__c,Number_of_Acres_Text__c,
                           Primary_Contact_Last_Name__c,Contact_Address1__c,
                           Primary_State_Text__c,Primary_Country_Text__c,
                           Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                           Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                           Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                           Secondary_Country_Text__c,Secondary_Contact_County__c,
                           Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                           Line_Item__c,Description__c,Applicant_Instructions__c,
                           Country__r.Name,Level_2_Region__r.name,
                           Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                           Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                           GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                           GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                           GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                           GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                           Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                           GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                           If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                           Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                           Material_Type4__c, Material_Type5__c, Material_Type6__c,
                           Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                           Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                           Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                           Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                           Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                           Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                           Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c,
                     Critical_Habitat_Involved_Text__c, Inspected_by_APHIS__c,Zip_CBI__c, Critical_Habitat_Involved_CBI__c,                  
                     Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,
                     Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,
                     EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Action_Required__c
                      FROM Location__c 
                     WHERE Line_Item__c=:lineItemID 
                  ORDER BY State__r.Name ASC,
                           Level_2_Region__r.name ASC,
                            name asc ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLLocationRepository.selectByLineItemID()',e);                
           } 
         return null;
    }  
*/
    // Ravee Racharla. W-035205. Included lineitem owner id and location created by id
    public static List<Location__c> selectByLineItemId(Id lineItemID){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
            return [SELECT ID,Name ,RecordType.Name,Contact_Name1__c, 
                    Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                    Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                    Day_Phone_2__c,Zip__c,Zip_Text__c,Other_Material_Types__c,
                    Proposed_Planting__c,Number_of_Acres_Text__c,GPS_Count__c,
                    Primary_Contact_Last_Name__c,Contact_Address1__c,
                    Primary_State_Text__c,Primary_Country_Text__c,
                    Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                    Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                    Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                    Secondary_Country_Text__c,Secondary_Contact_County__c,
                    Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                    Line_Item__c,Description__c,Applicant_Instructions__c,
                    Country__r.Name,Level_2_Region__r.name,
                    Line_Item__r.Application_Number__r.Application_Type__c,Line_Item__r.OwnerId, CreatedById,
                    Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                    Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                    GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                    GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                    GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                    GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                    Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                    GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                    If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                    Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                    Material_Type4__c, Material_Type5__c, Material_Type6__c,
                    Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                    Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                    Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                    Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                    Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                    Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                    Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c,
                    Critical_Habitat_Involved_Text__c, Inspected_by_APHIS__c,Zip_CBI__c, Critical_Habitat_Involved_CBI__c,                  
                    Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,
                    Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,Action_Required__c,
                    EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Number_of_Proposed_Releases_CBI_Text__c,(select id,name,GPS_Coordinates_Text__c,Self_Reporting__c from GPS_Coordinates__r where Self_Reporting__c = null),
                    (select id,Material__c,Material_CBI__c,Material_Type_Text__c,Other_Material__c,Quantity__c,Unit_of_Measure__c from Materials__r)
                    FROM Location__c 
                    WHERE Line_Item__c=:lineItemID 
                    ORDER BY State__r.Name ASC,
                    Level_2_Region__r.name ASC,Location_Unique_Id__c ASC,
                    name asc ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLocationRepository.selectByLineItemID()',e);                
        } 
        return null;
    }
    public static List<Location__c> selectLocsWithRelSRsByLineItemId(Id lineItemID, String recordType, List<Self_Reporting__c> srList){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
            return [SELECT ID,Name ,RecordType.Name,Contact_Name1__c, 
                    Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                    Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                    Day_Phone_2__c,Zip__c,Other_Material_Types__c,GPS_Count__c,
                    Proposed_Planting__c,Number_of_Acres_Text__c,
                    Primary_Contact_Last_Name__c,Contact_Address1__c,
                    Primary_State_Text__c,Primary_Country_Text__c,
                    Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                    Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                    Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                    Secondary_Country_Text__c,Secondary_Contact_County__c,
                    Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                    Line_Item__c,Description__c,Applicant_Instructions__c,
                    Country__r.Name,Level_2_Region__r.name,
                    Line_Item__r.Application_Number__r.Application_Type__c,
                    Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                    Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                    GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                    GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                    GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                    GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                    Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                    GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                    If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                    Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                    Material_Type4__c, Material_Type5__c, Material_Type6__c,
                    Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                    Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                    Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                    Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                    Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c,Line_Item__r.OwnerId, 
                    Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                    Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c,
                    Critical_Habitat_Involved_Text__c, Inspected_by_APHIS__c,Zip_CBI__c, Critical_Habitat_Involved_CBI__c,                  
                    Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,
                    Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,Action_Required__c,
                    EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Number_of_Proposed_Releases_CBI_Text__c,(select id,name,GPS_Coordinates_Text__c from GPS_Coordinates__r),
                    (Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,Is_Submitted__c,Is_No_Planting__c,
                    No_Monitoring_Report__c,Release_Record_ID__r.Location_Unique_Id__c,Release_Record_ID__r.Name,
                    Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Anticipated_Harvest_Destruct_Date__c,Planting_ID__c,
                    Monitoring_Period_End__c,Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c,
                    Deleterious_Effects__c,Crop_Observations__c,Explanation__c,Unexpected_Effects__c,Deleterious_Effects_Data__c,
                    Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,Quantity_Acres_CBI__c,Quantity_Acres_Text__c,
                    Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c, 
                    In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, How_was_material_disposed__c,
                    Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, Off_Field_Description__c, 
                    Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c, 
                    Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c ,
                    Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Description__c
                    from Self_Reporting__r Where Id in :srList)
                    FROM Location__c 
                    WHERE Line_Item__c=:lineItemID and RecordTypeId =: recordType
                    ORDER BY State__r.Name ASC,
                    Level_2_Region__r.name ASC,Location_Unique_Id__c ASC,
                    name asc];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLocationRepository.selectByRecordTypeandLineItemId()',e);                
        } 
        return null;
    }
  
    public static List<Location__c> selectLocsWithRelVolSRsByLineItemId(Set<Id>  locList, List<Self_Reporting__c> srList){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
            return [SELECT ID,Name ,RecordType.Name,Contact_Name1__c, 
                    Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                    Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                    Day_Phone_2__c,Zip__c,Other_Material_Types__c,GPS_Count__c,
                    Proposed_Planting__c,Number_of_Acres_Text__c,
                    Primary_Contact_Last_Name__c,Contact_Address1__c,
                    Primary_State_Text__c,Primary_Country_Text__c,
                    Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                    Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                    Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                    Secondary_Country_Text__c,Secondary_Contact_County__c,
                    Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                    Line_Item__c,Description__c,Applicant_Instructions__c,
                    Country__r.Name,Level_2_Region__r.name,
                    Line_Item__r.Application_Number__r.Application_Type__c,
                    Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                    Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                    GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                    GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                    GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                    GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                    Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                    GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                    If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                    Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                    Material_Type4__c, Material_Type5__c, Material_Type6__c,
                    Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                    Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                    Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                    Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                    Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                    Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                    Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c,
                    Critical_Habitat_Involved_Text__c, Inspected_by_APHIS__c,Zip_CBI__c, Critical_Habitat_Involved_CBI__c,                  
                    Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,
                    Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,Action_Required__c,
                    EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Number_of_Proposed_Releases_CBI_Text__c,(select id,name,GPS_Coordinates_Text__c from GPS_Coordinates__r),
                    (Select id,Report_Summary__c,Release_Record_ID__c,Start_Date__c,Is_Submitted__c,Is_No_Planting__c,
                    No_Monitoring_Report__c,Release_Record_ID__r.Location_Unique_Id__c,
                    Release_Record_ID__r.country__r.name,Release_Record_ID__r.level_2_region__r.name,Anticipated_Harvest_Destruct_Date__c,Planting_ID__c,
                    Monitoring_Period_End__c,Monitoring_Period_Start__c,Longitude_3__c,Longitude_2__c,Longitude_1__c,Still_Growing_Description__c,
                    Deleterious_Effects__c,Crop_Observations__c,Explanation__c,Unexpected_Effects__c,Deleterious_Effects_Data__c,
                    Comments__c,Release_Record_ID__r.state__r.name,Quantity_Acres__c,Quantity_Acres_CBI__c,Quantity_Acres_Text__c,
                    Coordinates_CBI__c,GPS_Co_ordinates_CBI__c ,Final_Volunteer_Monitoring_Report__c, 
                    In_field_Termination_Date__c, In_Field_Description__c, How_was_it_terminated__c, How_was_material_disposed__c,
                    Before_Harvest_Destruction_Date__c, Before_Harvest_Description__c, Any_Planting_Material_Harvested__c, Off_Field_Description__c, 
                    Off_Field_Destruction_Date__c, Planted_Material_Destroyed_Before_Harves__c, Planting_Material_Still_Growing__c, 
                    Still_Growing_Quantity__c, Still_Growing_Quantity_CBI__c, Stored_Description__c, Stored_Quantity__c, Stored_Quantity_CBI__c,Units__c ,
                    Stored_Material_Type__c ,Field_Test_Report_Type__c ,Equipment__c,Description__c,Release_Record_ID__r.Name
                    from Self_Reporting__r Where Id in :srList)
                    FROM Location__c 
                    WHERE Id in: locList // Line_Item__c=:lineItemID and 
                    ORDER BY State__r.Name ASC,
                    Level_2_Region__r.name ASC,Location_Unique_Id__c ASC,
                    name asc ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLocationRepository.selectByRecordTypeandLineItemId()',e);                
        } 
        return null;
    }

     // Search Location records using Search keyword within the line Item ID  (W-027406 - Last Minute Fix)
    public static List<Location__c> searchLocByKeywordWithInLineItemID(Id lineItemID, string searckKey){
        try{
            searckKey = '%' + searckKey + '%';
            EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
            set<id> MaterialSearchedLocIds = new set<id>();
            for(AggregateResult ar : [Select Location__c from Material__c where Location__c in (Select id FROM Location__c WHERE Line_Item__c=:lineItemID) and Material_Type_Text__c like :searckKey Group By Location__c])
            {
             MaterialSearchedLocIds.add((id)ar.get('Location__c'));
            }
            return [SELECT ID,Name ,RecordType.Name,Contact_Name1__c, 
                    Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                    Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,GPS_Count__c,
                    Day_Phone_2__c,Zip__c,Zip_Text__c,Other_Material_Types__c,
                    Proposed_Planting__c,Number_of_Acres_Text__c,
                    Primary_Contact_Last_Name__c,Contact_Address1__c,
                    Primary_State_Text__c,Primary_Country_Text__c,
                    Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                    Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                    Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                    Line_Item__r.Application_Number__r.Application_Type__c,Line_Item__r.OwnerId, CreatedById,
                    Secondary_Country_Text__c,Secondary_Contact_County__c,
                    Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                    Line_Item__c,Description__c,Applicant_Instructions__c,
                    Country__r.Name,Level_2_Region__r.name,
                    Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                    Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                    GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                    GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                    GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                    GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                    Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                    GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                    If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                    Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                    Material_Type4__c, Material_Type5__c, Material_Type6__c,
                    Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                    Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                    Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                    Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                    Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                    Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                    Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c,
                    Critical_Habitat_Involved_Text__c, Inspected_by_APHIS__c,Zip_CBI__c, Critical_Habitat_Involved_CBI__c,                  
                    Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,
                    Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,
                    EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Action_Required__c,Number_of_Proposed_Releases_CBI_Text__c
                    FROM Location__c 
                    WHERE Line_Item__c=:lineItemID 
                    and
                    (
                     Name like :searckKey or
                     RecordType.Name like :searckKey or
                     status__c like :searckKey or
                     County_Text__c like :searckKey or
                     State__r.Name like :searckKey or
                     Country_Text__c like :searckKey or
                     Action_Required__c like :searckKey or   
                     Id in :MaterialSearchedLocIds or 
                     Location_Unique_Id__c like :searckKey or    
                     Number_of_Acres_Text__c like :searckKey   
                    )
                    ORDER BY State__r.Name ASC,
                    Level_2_Region__r.name ASC,Location_Unique_Id__c ASC,
                    name asc ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLocationRepository.selectByLineItemID()',e);                
        } 
        return null;
    }  
   
    
    // Obtain Location records using line Item ID used for ***CLONING Locations ONLY***   
    public static List<Location__c> selectByLineItemIDforClone(Id lineItemID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Location__c');
            return [SELECT Id, Name ,RecordType.Name,Contact_Name1__c, 
                           Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                           Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                           Day_Phone_2__c,Zip__c,Other_Material_Types__c,GPS_Count__c,
                           Proposed_Planting__c,Number_of_Acres_Text__c,
                           Primary_Contact_Last_Name__c,Contact_Address1__c,
                           Primary_State_Text__c,Primary_Country_Text__c,
                           Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                           Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                           Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                           Secondary_Country_Text__c,Secondary_Contact_County__c,
                           Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                           Description__c,Applicant_Instructions__c,
                           Country__r.Name,Level_2_Region__r.name,
                           State__r.Name,Status_Graphical__c,
                           Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                           GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                           GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                           GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                           GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                           Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                           GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                           If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                           Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                           Material_Type4__c, Material_Type5__c, Material_Type6__c,
                           Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                           Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                           Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                           Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                           Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                           Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                           Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c, Inspected_by_APHIS__c, status__C,Zip_CBI__c, Critical_Habitat_Involved_CBI__c,                  
                           Number_of_Acres__c,Number_of_Acres_CBI__c,Record_Type_Name__c,GPS_Co_ordinates_CBI__c,
                           Number_of_Proposed_Releases_CBI__c,Release_Site_History_CBI__c,If_Yes_Please_Explain_CBI__c,
                           EnableDeleteAction__c,EnableDisplayAction__c,EnableUpdateAction__c,Action_Required__c
                      FROM Location__c 
                     WHERE Line_Item__c=:lineItemID 
                  ORDER BY State__r.Name ASC,
                           Level_2_Region__r.name ASC,
                           Location_Unique_Id__c ASC, name asc ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLLocationRepository.selectByLineItemID()',e);                
           } 
         return null;
    }  
    
    // W-36122 - bulkify.
    // Obtain Related Contact records using Location ID used for ***CLONING Locations ONLY***   
    public static List<Related_Contact__c> selectContactsByLocationsforClone(Set<Id> locationsSet){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Related_Contact__c');
            return [SELECT Name, Address__c, Alternate_Email__c, Alternate_Email_CBI__c, 
                    Alternate_Phone__c, Alternate_Phone_CBI__c, City__c, Country__c, Country_CBI__c, 
                    County__c, Email__c, Email_CBI__c, Fax__c,  Fax_CBI__c, First_Name__c, Organization_Name__c, 
                    Phone__c, Phone_CBI__c, Primary__c, State__c, Zip__c, Zip_CBI__c, Location_ID__C 
                      FROM Related_Contact__c
                     WHERE Location__c  in :locationsSet
                     ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLLocationRepository.selectContactsByLocationsforClone()',e);                
           } 
         return null;
    } 
    // Obtain Location records using Location ID used for ***CLONING Locations ONLY***   
    public static List<Related_Contact__c> selectContactsByLocationIDforClone(Id LocationID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Related_Contact__c');
            return [SELECT Name, Address__c, Alternate_Email__c, Alternate_Email_CBI__c, 
                    Alternate_Phone__c, Alternate_Phone_CBI__c, City__c, Country__c, Country_CBI__c, 
                    County__c, Email__c, Email_CBI__c, Fax__c,  Fax_CBI__c, First_Name__c, Organization_Name__c, 
                    Phone__c, Phone_CBI__c, Primary__c, State__c, Zip__c, Zip_CBI__c
                      FROM Related_Contact__c
                     WHERE Location__c=:LocationID 
                     ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLLocationRepository.selectContactsByLocationIDforClone()',e);                
           } 
         return null;
    }  
    
    // W-36122 - bulkify.
    // Obtain Location records using Location ID used for ***CLONING Locations ONLY***   
    public static List<GPS_Coordinate__c> selectGPSByLocationsforClone(Set<Id> locationsSet){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('GPS_Coordinate__c');
            return [SELECT GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,
                    GPS_Coordinates_CBI__c, Location_ID__C, Self_Reporting__c 
                    FROM GPS_Coordinate__c
                    WHERE Self_Reporting__c = null and Location__c IN :locationsSet 
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLocationRepository.selectGPSByLocationsforClone()',e);                
        } 
        return null;
    }
    
    // Obtain Location records using Location ID used for ***CLONING Locations ONLY***   
    public static List<GPS_Coordinate__c> selectGPSByLocationIDforClone(Id LocationID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('GPS_Coordinate__c');
            return [SELECT GPS_Coordinates__latitude__s,GPS_Coordinates__longitude__s,  GPS_Coordinates_CBI__c, Self_Reporting__c
                      FROM GPS_Coordinate__c
                     WHERE Self_Reporting__c = null and Location__c=:LocationID 
                     ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLLocationRepository.selectGPSByLocationIDforClone()',e);                
           } 
         return null;
    }  
    
    // W-36122 - bulkify.
    // Obtain Location records using Location ID used for ***CLONING Locations ONLY***   
    public static List<Material__c> selectMaterialsByLocationsClone(Set<Id> locationsSet){
        try{
            EFLEnforceAccessUtility.checkObjectReadAccess('Material__c');
            return [SELECT  Material__c, Material_CBI__c, Other_Material__c, Quantity__c,
                    Unit_of_Measure__c, Location_ID__C 
                    FROM Material__c
                    WHERE Location__c IN :locationsSet 
                   ];
        }catch(exception e){
            EFLErrorLog.createErrorLog('EFLLocationRepository.selectMaterialsByLocationIDforClone()',e);                
        } 
        return null;
    }
    
    // Obtain Location records using Location ID used for ***CLONING Locations ONLY***   
    public static List<Material__c> selectMaterialsByLocationIDforClone(Id LocationID){
       try{
           EFLEnforceAccessUtility.checkObjectReadAccess('Material__c');
            return [SELECT  Material__c, Material_CBI__c, Other_Material__c, Quantity__c, Unit_of_Measure__c
                      FROM Material__c
                     WHERE Location__c=:LocationID 
                     ];
        }catch(exception e){
                      EFLErrorLog.createErrorLog('EFLLocationRepository.selectMaterialsByLocationIDforClone()',e);                
           } 
         return null;
    }  
    
    
    // Obtain Construct record using construct ID   
    public static location__c selectByID(id locationID){
        return [SELECT ID,Name ,RecordType.Name,Contact_Name1__c, 
                           Contact_Name2__c,Street_Add1__c,Street_Add2__c,
                           Street_Add3__c,Street_Add4__c,City__c,Country_Text__c,
                           Day_Phone_2__c,Zip__c,Other_Material_Types__c,
                           Proposed_Planting__c,Number_of_Acres_Text__c,
                           Primary_Contact_Last_Name__c,Contact_Address1__c,
                           Primary_State_Text__c,Primary_Country_Text__c,
                           Contact_Zip__c,Primary_County_Text__c,Contact_City__c,
                           Day_Phone__c,Email_1_Text__c,Secondary_Contact_Last_Name__c,
                           Contact_Address2__c,Secondary_Contact_City__c,Secondary_Contact_State__c,
                           Secondary_Country_Text__c,Secondary_Contact_County__c,
                           Email_2_Text__c,County_Text__c,Quantity_Text__c,Unit_of_Measure_Text__c, 
                           Line_Item__c,Description__c,Applicant_Instructions__c,
                           Country__r.Name,Level_2_Region__r.name,
                           Status__c,State__r.Name,Status_Graphical__c,CreatedDate,
                           Material_Type_Text__c,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c,
                           GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,
                           GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                           GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,
                           GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                           Location_Unique_Id__c,GPS_Coordinates_1_Text__c,GPS_Coordinates_2_Text__c,GPS_Coordinates_3_Text__c,
                           GPS_Coordinates_4_Text__c,GPS_Coordinates_5_Text__c,Release_History__c,Critical_Habitat_Involved__c,
                           If_Yes_Please_Explain__c,GPS_Coordinates_6_Text__c,Org_L1__c,Org_L2__c,
                           Material_Type1__c, Material_Type2__c, Material_Type3__c,Secondary_Contact_Zip__c, 
                           Material_Type4__c, Material_Type5__c, Material_Type6__c,
                           Material_Type1_Text__c,Material_Type2_Text__c,Material_Type3_Text__c,
                           Material_Type4_Text__c,Material_Type5_Text__c,Material_Type6_Text__c, 
                           Quantity_1__c, Quantity_2__c, Quantity_3__c, Quantity_4__c, Quantity_5__c, Quantity_6__c, 
                           Unit_of_Measure_1__c, Unit_of_Measure_2__c, Unit_of_Measure_3__c, 
                           Unit_of_Measure_4__c, Unit_of_Measure_5__c, Unit_of_Measure_6__c, 
                           Other_Material_Types_1__c, Other_Material_Types_2__c, Other_Material_Types_3__c, 
                           Other_Material_Types_4__c, Other_Material_Types_5__c, Other_Material_Types_6__c,Action_Required__c
                      FROM Location__c 
                WHERE id =:locationID
               ];
    }
    
    
}