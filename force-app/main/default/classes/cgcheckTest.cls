@isTest(seealldata=false)
private class cgcheckTest {
    static testMethod void testcgcheck() {
    
          
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Country__c cntryaf = testData.newcountryaf();
        Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
        Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
        String ACgroupRecordTypeId = Schema.SObjectType.Group__c.getRecordTypeInfosByName().get('Decision Group').getRecordTypeId();      
     /*   Group__c grp = new Group__c();
        grp.Program_Line_Item_Pathway__c = plip.id;
        grp.RecordTypeId = ACgroupRecordTypeId;   
        grp.Name = 'Test';
        grp.Group_Custom_Name__c = 'Test';
        grp.Status__c = 'Active';
        insert grp;    
        
        Program_Line_Field_Groups__c plfg = new Program_Line_Field_Groups__c();
        plfg.Criteria_Group__c = grp.id;
        plfg.Program_Pathway__c = plip.id;
        insert plfg;
        
        objdm.Country__c = cntryaf.id;
        objdm.Program_Line_Item_Pathway__c = plip.id;
        objdm.Regulated_Article__c = regart.id;
        objdm.Criteria_Group__c = grp.id;
        insert objdm;
*/
        
        Signature__c newsig = testData.newThumbprint();
        Regulation__c newreg1 = testData.newRegulation('Import Requirements','Test');
        Regulation__c newreg2 = testData.newRegulation('Additional Information','Test');        
        Signature_Regulation_Junction__c srj1 = testData.newSRJ(newsig.id,newreg1.id);
        Signature_Regulation_Junction__c srj2 = testData.newSRJ(newsig.id,newreg2.id);        
        
        cgcheck extclass = new cgcheck();
        extclass.findTheGroup(new Map<String,String>(),plip.id);
        system.assert(extclass != null);                             
    }
   }