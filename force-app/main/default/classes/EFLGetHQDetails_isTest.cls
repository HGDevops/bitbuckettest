@isTest
public class EFLGetHQDetails_isTest{
    
    @isTest
    public static void EFLGetHQDetails_isTestMethod1(){
      Account testAcc = new Account();
      testAcc.Name = 'Test Account';
      insert testAcc;
      
      Contact testCon = new COntact();
      testCon.LastName = 'Test Contact';
      testCon.Account = testAcc;
      insert testCon;

      EFLRegistration__c testReg = new EFLRegistration__c();
      testReg.EFLAccount__c = testAcc.id;
      testReg.EFLContact__c = testCon.id;
      //testReg.EFLStatus__c = 'Active';
      Database.insert(testReg);
      
      test.StartTest();
        EFLGetHQDetails.getAccountHQ(testReg.id);
      test.StopTest();
    }
}