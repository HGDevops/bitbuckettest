@isTest//(seealldata=true)
public class CARPOL_AC_UNIVERSAL_TRIGGER_TESTS {
/**
* CARPOL_AC_UNIVERSAL_TRIGGER_TESTS.class
*
* @author  Dawn Sangree
* @date   4/4/2016
* @desc   Tests that triggers for Application, Line Item, Authorization, Associated Contacts, Facility objects and their child classes
* CARPOL_UNI_MasterApplicationTrigger, CARPOL_UNI_MasterLineItemTrigger
*

*/  
    //inserting and editing an application (with contact and account)
    static testMethod void testCARPOL_UNI_MasterApplicationTriggerInsert(){
        //This code fully covers CARPOL_UNI_Master_Application Trigger but not the classes it calls
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();
        //tests the trigger on the intial insertion of an application for AC
        Application__c objapp = testData.newapplication();
        System.assert(objapp !=null);
        
        objapp.Application_Status__c = 'Submitted';
        //update some data and call update
        update objapp;

    }
    
    //inserting a line item
    static testMethod void testCARPOL_UNI_MasterLineItemTriggerInsert(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();        
        //tests the trigger on the intial insertion of an application for AC
        Test.startTest();
        AC__c objLineItem = testData.newLineItem('Resale/Adoption',null);
        System.assert(objLineItem !=null);
        system.debug('dawn ' + objLineItem);
        //update some data and call update
        //checkRecursive already set when application was inserted, can't fire off the rest of the update methods
        objLineItem.Importer_First_Name__c = 'Test';
        update objLineItem;
        
        //delete line item
        delete objLineItem;
        Test.stopTest();
    } 
       
    
    //inserting a line item
    static testMethod void testCARPOL_UNI_MasterApplicationTriggerUpdate(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();        
        Test.startTest();
        //create an application
        Application__c objapp = testData.newapplication();
        
        //Application update trigger uses line items to build authorizations
        AC__c objLineItem = testData.newLineItem('Resale/Adoption', objapp);


        objapp.Application_Status__c = 'Submitted';
        objapp.Share_with_roles__c = 'AC';
        //update some data and call update
        update objapp;
        
        //Application update trigger uses line items to build authorizations
        AC__c objLineItem2 = testData.newLineItem('Personal Use', objapp);
        System.assert(objLineItem2 != null);        
        //Add another line item that is the same to see 
        Test.stopTest();
    }
    
    //inserting a line item
    static testMethod void testCARPOL_UNI_MasterAuthorizationTriggerInsertUpdate(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();        
        Test.startTest();
        //create an application
        Application__c objapp = testData.newapplication();
        
        //Application update trigger uses line items to build authorizations
        AC__c objLineItem = testData.newLineItem('Resale/Adoption', objapp);


        Authorizations__c objauth = testDAta.newAuth(objapp.id);
        objLineItem.Authorization__c = objauth.id;
        update objLineItem;
        objauth.Authorization_Type__c = 'Permit';
        objauth.Status__c = 'Approved';
        objauth.Date_Issued__c = Date.today();
        update objauth;
        System.assert(objauth != null);
        //Add another line item that is the same to see 
        Test.stopTest();
    }        

    static testMethod void testInsertAssociatedContact(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();
        
        
        //making sure the future method in the trigger gets fired
        Test.startTest();      
        Applicant_Contact__c apcont = new Applicant_Contact__c();
        apcont.First_Name__c = 'apcontTest';
        apcont.Email_Address__c = 'apcont@test.com';
        apcont.Name = 'Associated Contacts Test';
        apcont.Mailing_Country__c = 'United States';
        Level_1_Region__c appL1R = testData.newlevel1regionMD();
        Level_2_Region__c l2 = testData.newlevel2region(appL1R.Id);
        apcont.Mailing_Country_LR__c = appL1R.Country__c;
        apcont.Mailing_County__c = l2.Id;
        apcont.Mailing_State_LR__c = appL1R.Id;
        apcont.Mailing_State__c = 'Maryland';
        apcont.Mailing_Street__c = '4700 River Road';
        apcont.Mailing_Zip_Postal_Code__c = '20737';
        apcont.Mailing_City__c = 'Riverdale';
        apcont.RecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        apcont.EFL_Business_Name__c = 'Test';
        insert apcont;
        System.assert(apcont !=null);
        Test.stopTest();
    }  
    
    static testMethod void testUpdateFacility(){

        Facility__c testFac = new Facility__c();
        testFac.Name = 'Test Facility';
        testFac.Address_1__c = '2120 West End Avenue';
      
        // Call Test.startTest before performing callout, but after setting test data.
        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMockImpl());
        insert testFac;
        System.assert(testFac != null);
        List<Id> lstFacIds = new List<Id>();
        lstFacIds.add(testFac.Id);
        
        // This causes a fake response to be sent, from the class that implements HttpCalloutMock. 
        HttpResponse res = new HttpResponse();
        CARPOL_AC_GooglePopulateTimeZone.CARPOL_AC_CallGoogleGeoAddress(lstFacIds);
        
        String actualValue = res.getBody();
        String expectedValue = 'Central Daylight Time';
       // System.assertEquals(actualValue, expectedValue);
        //System.assertEquals(200, res.getStatusCode());
        Test.stopTest();
    }
    
    static testMethod void testInsertDesignProtocolRecord(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();
        //making sure the future method in the trigger gets fired
        Test.startTest();  
        Application__c objapp = testData.newapplication();            
        AC__c objLineItem = testData.newLineItem('Resale/Adoption', objapp);
        
        Design_Protocol_Record__c dpr = new Design_Protocol_Record__c();
        dpr.Status__c = 'Waiting on Customer';
        dpr.Associate_Application__c = objapp.id;
        dpr.Line_Item__c = objLineItem.id;
        insert dpr;
        dpr.Status__c = 'Review Complete';
        update dpr;
        System.assert(dpr !=null);
        Test.stopTest();
    }  
    
   
     static testMethod void testInsertApplicationCondition(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();
        //making sure the future method in the trigger gets fired
        Test.startTest();  
        Application__c objapp = testData.newapplication();            
        AC__c objLineItem = testData.newLineItem('Resale/Adoption', objapp);
        
        Authorizations__c objauth = testData.newauth(objapp.id);
        objLineItem.Authorization__c = objauth.id;
        update objLineItem;

        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        
        Program_Condition__c prjcond = new Program_Condition__c();
        prjcond.Regulation__c = objreg1.id;
        prjcond.Active__c = true;
        prjcond.Condition_Number__c = 1;
        prjcond.Condition__c = 'test';
        insert prjcond;
        
        Link_Authorization_Regulation__c lar = new Link_Authorization_Regulation__c();
        lar.Authorization__c = objauth.id;
        lar.Regulation__c = objreg1.id;
        insert lar;
        
        Application_Condition__c ac = new Application_Condition__c();
        ac.Condition_Number__c = 1;
        ac.Agree__c = 'Agree';
        ac.Link_Authorization_Regulation__c = lar.id;
        insert ac;
        ac.Agree__c = 'Agree';
        update ac;
        System.assert(ac!=null);
        Test.stopTest();
    } 
    
     static testMethod void testPreparationHandshake(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        //making sure the future method in the trigger gets fired
        Test.startTest();  
        Preparer_Request__c objprereq = new Preparer_Request__c();
        objprereq.Contact_Email__c = 'test@email.com';
        objprereq.Last_Name__c = 'LastName';
        objprereq.Contact_Phone__c = '1234567890';
        insert objprereq;        
        objprereq.Email_Confirmation_Sent__c = True;
        objprereq.Applicant_Account1__c = objacct.id;
        update objprereq;

        System.assert(objprereq!=null);
        Test.stopTest();
    }              
    
    
    //create an application and manually create a new line item to get update stuff to wrok
    
    //inserting an authorization
    
    //editing an application
}