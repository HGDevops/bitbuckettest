/**
* Class containing tests for CARPOL_BRS_LocationsTrigger Trigger
*/
@IsTest
private class CARPOL_BRS_LocationsTrigger_Test {
    
    @testSetup
    static void initData() {
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        CARPOL_AC_TestDataManager testDataAC = new CARPOL_AC_TestDataManager();
        testDataBRS.insertcustomsettings();
        CARPOL_UNI_DisableTrigger__c dt = new  CARPOL_UNI_DisableTrigger__c(Name = 'CARPOL_BRS_LocationsTrigger', Disable__c = false);
        Insert dt;
        CARPOL_UNI_DisableTrigger__c dtmat = new  CARPOL_UNI_DisableTrigger__c(Name = 'EFLLocationRelatedMaterialTrigger', Disable__c = false);
        Insert dtmat;
        Contact testContact = testDataBRS.newContact();
        Country__c testCountry = testDataBRS.newcountryus();
        Level_1_Region__c testLevelRegion1 = testDataBRS.newlevel1region(testCountry.Id);
        Level_2_Region__c testLevelRegion2 = testDataBRS.newlevel2region(testLevelRegion1.Id); 
        
        //Application__c application = testDataAC.newApplication();
        Application__c application = testDataBRS.newapplication();
        AC__c lineItem = testDataBRS.newLineItem('Resale/Adoption', application);
    }
    
    static Location__c createLocation(String recordType) {
        Country__c testCountry = [Select Id from Country__c LIMIT 1];
        Level_1_Region__c testLevelRegion1 = [Select Id from Level_1_Region__c LIMIT 1];
        Level_2_Region__c testLevelRegion2 = [Select Id from Level_2_Region__c LIMIT 1];
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        Location__c loc1 = new Location__c(
            Name='testloc', Country__c=testCountry.Id, state__C= testLevelRegion1.Id,
            Level_2_Region__c = testLevelRegion2.Id, 
            RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId(),
            GPS_1__c = 'GPS_1__c',
            GPS_2__c = 'GPS_2__c',
            GPS_3__c = 'GPS_3__c',
            GPS_4__c = 'GPS_4__c',
            GPS_5__c = 'GPS_5__c',
            GPS_6__c = 'GPS_6__c',
            Contact_Name1__c = 'Test',
            Primary_Contact_Last_Name__c = 'LName', 
            Day_Phone__c = '555-555-1212',
            Status__c='Review Completed',
            Applicant_Instructions__c='Test Instructions',
            Unit_of_Measure_CBI__c=true,
            Material_Type_CBI__c=true,
            Primary_Country_CBI__c=true,
            Primary_State_CBI__c=true,
            Secondary_Country_CBI__c=true,
            Secondary_County_CBI__c = true,
            Secondary_State_CBI__c=true,
            Line_Item__c= lineItem.Id,
            Inspected_by_APHIS__c='Yes'
        );
        return loc1;
    }
    
    static Location__c createLocationBulk(String recordType, Country__c testCountry, Level_1_Region__c testLevelRegion1, 
                                         Level_2_Region__c testLevelRegion2, AC__c lineItem, integer i) {
        Location__c loc1 = new Location__c(
            Name='testloc', Country__c=testCountry.Id, state__C= testLevelRegion1.Id,
            Level_2_Region__c = testLevelRegion2.Id, 
            RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId(),
            GPS_1__c = 'GPS_1__c',
            GPS_2__c = 'GPS_2__c',
            GPS_3__c = 'GPS_3__c',
            GPS_4__c = 'GPS_4__c',
            GPS_5__c = 'GPS_5__c',
            GPS_6__c = 'GPS_6__c',
            Contact_Name1__c = 'Test',
            Primary_Contact_Last_Name__c = 'LName', 
            Day_Phone__c = '555-555-1212',
            Status__c='Review Completed',
            Applicant_Instructions__c='Test Instructions',
            Unit_of_Measure_CBI__c=true,
            Material_Type_CBI__c=true,
            Primary_Country_CBI__c=true,
            Primary_State_CBI__c=true,
            Secondary_Country_CBI__c=true,
            Secondary_County_CBI__c = true,
            Secondary_State_CBI__c=true,
            Line_Item__c= lineItem.Id,
            Inspected_by_APHIS__c='Yes',
            Location_Unique_Id__c = 'TestLocUniqueId' + string.valueOf(i),
            Proposed_Planting__c = 10,
            Number_of_Acres__c = i+1,
            Critical_Habitat_Involved__c = 'No',
            Release_History__c = 'Test'
        );
        return loc1;
    }

    @IsTest 
    static void testReleaseSiteLocationInsert() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Import';
        lineItem.Type_of_Permit__c = 'Notification';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Release Sites Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc1;
        Location__c loc2 = createLocation('Origin and Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc2;
        Location__c loc3 = createLocation('Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc3;
        Location__c loc4 = createLocation('Incident Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc4;
        Material__c mat = new Material__c();
        mat.Location__c=loc1.id;
        mat.Quantity__c = 2;
        mat.Unit_of_Measure__c = 'Grams';
        mat.Material__c = 'Eggs';
        insert mat;
        GPS_Coordinate__c gps = new GPS_Coordinate__c();
        gps.Location__c=loc1.id;
        insert gps;
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    @IsTest 
    static void testReleaseSiteLocationInsertOne() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Interstate Movement';
        lineItem.Type_of_Permit__c = 'Notification';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Release Sites Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc1;
        Location__c loc2 = createLocation('Origin and Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc2;
        Location__c loc3 = createLocation('Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc3;
        Location__c loc4 = createLocation('Incident Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc4;
        Material__c mat = new Material__c();
        mat.Location__c=loc1.id;
        mat.Quantity__c = 2;
        mat.Unit_of_Measure__c = 'Grams';
        mat.Material__c = 'Eggs';
        insert mat;
        GPS_Coordinate__c gps = new GPS_Coordinate__c();
        gps.Location__c=loc1.id;
        insert gps;
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    @IsTest 
    static void testReleaseSiteLocationInsertTwo() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Interstate Movement';
        lineItem.Type_of_Permit__c = 'Notification';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Release Sites Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc1;
        
        Location__c loc3 = createLocation('Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc3;
        Location__c loc4 = createLocation('Incident Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc4;
        Material__c mat = new Material__c();
        mat.Quantity__c = 2;
        mat.Location__c=loc1.id;
        mat.Unit_of_Measure__c = 'Grams';
        mat.Material__c = 'Eggs';
        insert mat;
        GPS_Coordinate__c gps = new GPS_Coordinate__c();
        gps.Location__c=loc1.id;
        insert gps;
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    @IsTest
    static void testReleaseSiteLocationInsertThree() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Interstate Movement';
        lineItem.Type_of_Permit__c = 'Notification';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        
        Location__c loc1 = createLocation('Release Sites Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc1;
        Location__c loc2 = createLocation('Origin and Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc2;
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    @IsTest 
    static void testReleaseSiteLocationInsertFour() {
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        Application__c application = testDataBRS.newApplication();
        AC__c lineItem = testDataBRS.newLineItemBRSByStatus('Resale',application, 'Submitted');
        Test.startTest();
        Location__c loc1 = createLocation('Release Sites Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc1;
        Location__c loc2 = createLocation('Origin and Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;        
        insert loc2;
        Material__c mat = new Material__c();
        mat.Location__c=loc1.id;
        mat.Quantity__c = 2;
        mat.Unit_of_Measure__c = 'Grams';
        mat.Material__c = 'Eggs';
        insert mat;
        mat.Material_CBI__c=true;
        update mat;
        delete mat;
        GPS_Coordinate__c gps = new GPS_Coordinate__c();
        gps.Location__c=loc1.id;
        insert gps;
        delete gps;
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    
    @IsTest 
    static void testOriginLocationInsert() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        Country__c objcountry=new  Country__c();
        objcountry.Name='Canada';
        objcountry.Country_Code__c='ES';
        objcountry.Country_Status__c='Active';
        Insert objcountry;
        Level_1_Region__c objL1R=new  Level_1_Region__c();
        objL1r.Country__c=objcountry.Id;
        objL1r.Name='Ontario';
        objL1r.Level_1_Name__c='Ontario';
        objL1r.Level_1_Region_Status__c='Active';
        Insert objL1r;
        
        lineItem.Movement_Type__c = 'Import';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Origin Location');
        loc1.Line_Item__c = lineItem.Id;
        loc1.country__c=objcountry.id;
        loc1.state__c=objL1r.id;
        loc1.Level_2_Region__c=null;
        //Reset runOnce
        //checkRecursive.run=true;
        insert loc1;     
        Location__c loc2 = createLocation('Origin Location');
        loc2.country__c=objcountry.id;
        loc2.state__c=objL1r.id;
        loc2.Line_Item__c = lineItem.Id;
        loc2.Level_2_Region__c=null;
        insert loc2;
        Location__c loc3 = createLocation('Origin Location');
        loc3.country__c=objcountry.id;
        loc3.state__c=objL1r.id;
        loc3.Line_Item__c = lineItem.Id;
        loc3.Level_2_Region__c=null;
        insert loc3;        
        Test.stopTest();
        System.assert(loc3.Id != null);
    }
    
    @IsTest 
    static void testOriginDestinationLocationInsertForInterstate() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Interstate Movement';
        lineItem.Type_of_Permit__c='Standard Permit';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Origin and Destination Location'); 
        //Reset runOnce
        //checkRecursive.run=true;
        insert loc1;     
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    
    @IsTest 
    static void testOriginDestinationLocationInsertForImport() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Import';
        lineItem.Type_of_Permit__c='Standard Permit';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Origin and Destination Location'); 
        //Reset runOnce
        //checkRecursive.run=true;
        insert loc1;     
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    
    @IsTest 
    static void testDestinationLocationInsert() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Interstate Movement';
        lineItem.Type_of_Permit__c='Standard Permit';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        Test.startTest();
        Location__c loc1 = createLocation('Destination Location');
        //Reset runOnce
        //checkRecursive.run=true;
        insert loc1;     
        Test.stopTest();
        System.assert(loc1.Id != null);
    }
    
    @isTest
    static void testInsertFailure() {
        /*AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Import';
        lineItem.Type_of_Permit__c='Standard Permit';
        lineItem.Status__c = 'Submitted';//status=submitted to test  failure
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordtypeId();
        update lineItem;*/
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        Application__c application = testDataBRS.newApplication();
        AC__c lineItem = testDataBRS.newLineItemBRSByStatus('Resale',application, 'Submitted');
        Test.startTest();
        Location__c loc1 = createLocation('Release Sites Location');
        try {
            insert loc1;
        } catch(Exception e) {
            boolean msg = false;
            if(e.getMessage().contains('Application is submitted you cannot insert new Location')) {
                msg=true;
            }
            System.AssertEquals(msg, true); 
        }        
        Test.stopTest();
    }
    
    @isTest 
    static void testDeleteLocation() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        lineItem.Movement_Type__c = 'Import';
        lineItem.Type_of_Permit__c='Standard Permit';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordtypeId();
        update lineItem;
        Location__c loc1 = createLocation('Release Sites Location');
        //Reset runOnce
        //checkRecursive.run = true;
        insert loc1;
        Test.startTest();
        List<Location__c> objToDelete = [SELECT Id from Location__c where Id = : loc1.id];
        //Reset runOnce
        //checkRecursive.run = true;
        delete objToDelete;
        Test.stopTest();
        List<Location__c> objDel = [SELECT Id from Location__c where Id = : loc1.id];
        System.assert(objDel.size() == 0);
    }
    
    @isTest
    static void testUpdateLocation() {
        Contact testContact = [Select Id from Contact LIMIT 1];
        //create APHIS Applicant User
        User usr = new User();
        usr = EFLUserTestDataFactory.getUser('eFile Applicant');        
        id contactIdforSharing = [select contactid from user where id = :usr.id limit 1].contactid;
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        Application__c application = testDataBRS.newApplication();
        AC__c lineItem = testDataBRS.newLineItem('Resale',application);
        lineItem.OwnerId = usr.id;
        update lineItem;
        application.Applicant_Name__c = contactIdforSharing;
        update application;
        Test.startTest();
        
        Location__c objloc = new Location__c();
            
        System.runAs(usr) {
            //Reset runOnce
            //checkRecursive.run = true;
            objloc = createLocation('Release Sites Location');
        	insert objloc;
            objLoc.Contact_Name1__c='Test Update';         
            //Reset runOnce
            //checkRecursive.run = true;
            update objloc;
            
            objLoc.Status__c = 'Review Complete';
            objLoc.Unit_of_Measure_CBI__c = false;
            objLoc.Material_Type_CBI__c = false;
            objLoc.Primary_Country_CBI__c = false;
            objLoc.Primary_State_CBI__c = false;
            objLoc.Secondary_Country_CBI__c = false;
            objLoc.Secondary_County_CBI__c = false;
            objLoc.Secondary_State_CBI__c = false;
            //Reset runOnce
            //checkRecursive.run = true;
            update objLoc;            
        }    
        Test.stopTest();
        List<Location__c> objUpd = [SELECT Id, Status__c from Location__c where Id = : objLoc.id];
        System.assert(objUpd[0].Status__c == 'Review Complete');
    }
    
   /* @isTest
    static void testUpdateLocationFailure() {
        
        Contact testContact = [Select Id from Contact LIMIT 1];
        //create APHIS Applicant User
        User usr = new User();
        usr = EFLUserTestDataFactory.getUser('eFile Applicant');        
        id contactIdforSharing = [select contactid from user where id = :usr.id limit 1].contactid;
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        CARPOL_BRS_TestDataManager testDataBRS = new CARPOL_BRS_TestDataManager();
        Application__c application = testDataBRS.newApplication();
        AC__c lineItem = testDataBRS.newLineItem('Resale',application);
        lineItem.OwnerId = usr.id;
        update lineItem;
        application.Applicant_Name__c = contactIdforSharing;
        update application;
        
        Test.startTest();
        
        Location__c objloc = new Location__c();
        
        System.runAs(usr) {
            
            
            objloc = createLocation('Release Sites Location');
        	insert objloc;
            //Reset runOnce
            //checkRecursive.run = true;
            
            lineItem.Status__c = 'Submitted'; 
            update lineItem;
            
            try{ 
                objLoc.Name='Test Update';         
                //Reset runOnce
                //checkRecursive.run = true;
                update objloc;
            } catch(Exception e) {
                boolean msg = false;
                if(e.getMessage().contains('Application is submitted you cannot modify the Location')) {
                    msg=true;
                }
                System.AssertEquals(msg, true); 
            }
        }    
        Test.stopTest();
        System.assert(objLoc != null);
    }   */ 
    
    @IsTest
    static void testLocationBulkInsert() {
        AC__c lineItem = [Select Id, Movement_Type__c, RecordTypeId from AC__c LIMIT 1];
        Country__c testCountry = [Select Id from Country__c LIMIT 1];
        Level_1_Region__c testLevelRegion1 = [Select Id from Level_1_Region__c LIMIT 1];
        Level_2_Region__c testLevelRegion2 = [Select Id from Level_2_Region__c LIMIT 1];
        lineItem.Movement_Type__c = 'Release';
        lineItem.RecordTypeId  = Schema.SObjectType.AC__c.getRecordtypeInfosByName().get('Biotechnology Regulatory Services - Notification').getRecordtypeId();
        update lineItem;
        List<Location__c> locations = new List<Location__c>();
        for(Integer i = 0; i < 200; i++ ) {  
            locations.add(createLocationBulk('Release Sites Location', testCountry, 
                                             testLevelRegion1, testLevelRegion2, lineItem, i));
        }
        
        Test.startTest();
        insert locations;
        Test.stopTest();
        System.assertNotEquals(locations[0].Id, null);
    }    
}