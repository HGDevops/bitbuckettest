public class SpringCMTriggerHandler {
    public static List<SObject> acc;

	public static void StartWorkflow(String session, String type, List <SObject> listfromtrigger, String workflow) {
		acc = new List<SObject>();
	    for(SObject a : listfromtrigger) {
	    	// HERE you would test your object(s) for inclusion to workflow
	        acc.add(a);
	    }
	    SpringCMRestHelper.StartWorkflow(acc, type, workflow, session);
	}

    //Developer Name: Peter Tran
    //W-032095
    //Method CreatedDate: 4/14/2019
    //Invoked by EFLReportSummaryTrigger.apxt after an update to Report_Summary__c's Status__c field
  /*  public static void setFolderViewOnly(List<Report_Summary__c> triggerNew) {
        //Set view only on Self_Report__c SpringCM folders
        String workflow = 'External Users View Only - Report Summary';
        
        /*List<Report_Summary__c> listOfRs = new List<Report_Summary__c>();
        for(Report_Summary__c rs : triggerNew) {
            if(rs.Status__c == 'Submitted') {
                listOfRs.add(rs);
            }
        }
        
        if(listOfRs.size() > 0){
            SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), Trigger.new.get(0).getSObjectType().getDescribe().getName(), Trigger.new, workflow);
        }*/   
   // }
}