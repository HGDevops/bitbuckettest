@isTest
public class ApplicationWrapper_Test{
    public static testMethod void ApplicationWrapper_TestMethod(){
        ApplicationWrapper inst = new ApplicationWrapper();
        //-------------------------------//
        ApplicationWrapper.Applicant applicant = new ApplicationWrapper.Applicant();
        applicant.AppTitle = 'App Title';
        applicant.AppFirstName = 'Kishore';
        applicant.AppMiddleName =  'Test';
        applicant.AppEmail = 'kkumar@phaseonecg.com';
        applicant.AppLastName = 'Applicant';
        //-------------------------------//
        
        //-------------------------------//
        ApplicationWrapper.LineItem lineItem = new ApplicationWrapper.LineItem();
        lineItem.RecordType = 'Biotechnology Regulatory Services - Standard Permit';
        lineItem.Introductiontype = 'Interstate Movement and Release';
        lineItem.ProposedStartDate = '04/10/2018';
        lineItem.ProposedEndDate = '04/06/2019';
        lineItem.ReleaseStartDate = '04/06/2018';
        lineItem.ReleaseEndDate = '04/05/2019';
        lineItem.BiologicalMaterial = 'Yes';
        lineItem.BiologicalMaterialDescription = 'culture medium, or host material';
        lineItem.HandCarry = 'Yes';
        lineItem.NumofLabels = '10';
        lineItem.CBIFlag = 'Yes';
        lineItem.CBIJustificationStatement = 'The justification should be provided in the fonnat below. The language used to prepare your justification should be in non-technical terms [when possible. Maintain the documents in the order provided and do not remove any documents.] The FOIA Office will make a final decision as to whether the information qualifies for protection under the FOIA. ';
        lineItem.PurposeofPermit = 'Industrial Product';
        lineItem.MeanofMovement = 'a group of people working together to advance their shared political, social, or artistic ideas.';
        lineItem.ApplicantReferenceNumber = '17Nov2016AP';
        //-------------------------------//
        
        //-------------------------------//        
        ApplicationWrapper.Article article = new ApplicationWrapper.Article();
        article.RegulatedArticle = 'Acidovorax venae';
        article.CultivarBreedingLine = 'These come from cultivators and strain breeders like American Growers Exchange, [Burning Bush Nurseries ]and King Klone, which originate from the most reputable clone nurseries in the [San Francisco Bay Area]';
        //-------------------------------//
        
        //-------------------------------// 
        ApplicationWrapper.Construct construct = new ApplicationWrapper.Construct();
        construct.ConstructName = '9Cons100';
        construct.IdentifyingLines = 'Test 9Cons100';
        construct.ModeofTransformation = '[No transformation]';
        construct.ConstructName = 'LineEvents';
        //-------------------------------//
        
        //-------------------------------// 
        ApplicationWrapper.Phenotype phenotype = new ApplicationWrapper.Phenotype();
        phenotype.PhenotypicCategory = '[BR-Bacterial Resistance]';
        phenotype.PhenotypicDescription = 'BR-Bacterial Resistance Descri';
        //-------------------------------//
         
        //-------------------------------// 
        ApplicationWrapper.Genotype genotype = new ApplicationWrapper.Genotype();
        genotype.GenotypeName = 'Gene(s) of Interest';
        genotype.ConstructComponent = 'Exon';
        genotype.ConstructComponentName = 'Gene(s) of Interest Name';
        genotype.ConstructCompDescription = 'Gene(s) of Interest Descrip';
        genotype.Donor = 'Donor1';
        //-------------------------------// 
        
        //-------------------------------// 
        ApplicationWrapper.OriginLocation originLocation = new ApplicationWrapper.OriginLocation();
        originLocation.LocationType = 'Origin Location';
        originLocation.LocationName = 'New Delhi';
        originLocation.LocStreetAddr1= '123 New delhi';
        originLocation.LocStreetAddr2= 'Gaandhi Marg';
        originLocation.LocStreetAddr3= '';
        originLocation.LocStreetAddr4= '';
        originLocation.LocCity= '';
        originLocation.LocZip= '93311';
        originLocation.LocDescription= '';
        originLocation.LocCountry= 'Mexico';
        originLocation.LocState= 'Baja California Sur';
        originLocation.LocCounty= 'Mexicali';
        originLocation.PriContactFirstName= 'Hillary';
        originLocation.PriContactLastName= 'Trump';
        originLocation.PriOrgName= 'HIT';
        originLocation.PriContactAddr= '123 Frankie Rogers Ave';
        originLocation.PriContactCity= 'Harrison';
        originLocation.PriContactCountry= 'United States of America';
        originLocation.PriContactState= 'New Jersey (NJ)';
        originLocation.PriContactZip= '07029';
        originLocation.PriContactCounty= 'Sussex';
        originLocation.PriContactPhone= 'Mexicali';
        originLocation.PriContactAltPhone= 'Hillary';
        originLocation.PriContactFax= 'Trump';
        originLocation.PriContactEmail= 'HIT';
        originLocation.PriContactAltEmail= '123 Frankie Rogers Ave';        
        originLocation.SecContactFirstName= 'Mexicali';
        originLocation.SecContactLastName= 'Hillary';
        originLocation.SecOrgName= 'Trump';
        originLocation.SecContactAddr= 'HIT';
        originLocation.SecContactCity= '123 Frankie Rogers Ave';
        originLocation.SecContactState= '';
        originLocation.SecContactCountry= '93311';
        originLocation.SecContactZip= '';
        originLocation.SecContactCounty= 'Mexico';
        originLocation.SecContactPhone= 'Baja California Sur';
        originLocation.SecContactFax= 'Mexicali';
        originLocation.SecContactEmail= 'Hillary';
        originLocation.addlocation= 'Trump';
        //-------------------------------// 
        
        //-------------------------------//
        ApplicationWrapper.DestinationLocation destinationLocation = new ApplicationWrapper.DestinationLocation();
        destinationLocation.LocationType = '';
        destinationLocation.LocationName = '';
        destinationLocation.LocStreetAddr1 = '';
        destinationLocation.LocStreetAddr2 = '';
        destinationLocation.LocStreetAddr3 = '';
        destinationLocation.LocStreetAddr4 = '';
        destinationLocation.LocCity = '';
        destinationLocation.LocZip = '';
        destinationLocation.LocDescription = '';
        destinationLocation.LocCountry = '';
        destinationLocation.LocState = '';
        destinationLocation.LocCounty = '';
        destinationLocation.PriContactFirstName = '';
        destinationLocation.PriContactLastName = '';
        destinationLocation.PriOrgName = '';
        destinationLocation.PriContactAddr = '';
        destinationLocation.PriContactCity = '';
        destinationLocation.PriContactCountry = '';
        destinationLocation.PriContactState = '';
        destinationLocation.PriContactZip = '';
        destinationLocation.PriContactCounty = '';
        destinationLocation.PriContactPhone = '';
        destinationLocation.PriContactAltPhone = '';
        destinationLocation.PriContactFax = '';
        destinationLocation.PriContactEmail = '';
        destinationLocation.PriContactAltEmail = '';
        destinationLocation.SecContactFirstName = '';
        destinationLocation.SecContactLastName = '';
        destinationLocation.SecOrgName = '';
        destinationLocation.SecContactAddr = '';
        destinationLocation.SecContactCity = '';
        destinationLocation.SecContactState = '';
        destinationLocation.SecContactCountry = '';
        destinationLocation.SecContactZip = '';
        destinationLocation.SecContactCounty = '';
        destinationLocation.SecContactPhone = '';
        destinationLocation.SecContactFax = '';
        destinationLocation.SecContactEmail = '';
        destinationLocation.addlocation = '';
        destinationLocation.LocCustRef = '';
        destinationLocation.Quantity = '';
        destinationLocation.UnitofMeasure = '';
        destinationLocation.MaterialTypes = '';
        destinationLocation.OtherMaterialTypes = '';
        //-------------------------------//
        
        //-------------------------------//
        ApplicationWrapper.ReleaseSitesLocation releaseSitesLocation = new ApplicationWrapper.ReleaseSitesLocation();
        releaseSitesLocation.LocationType = '';
        releaseSitesLocation.LocationName = '';
        releaseSitesLocation.LocStreetAddr1 = '';
        releaseSitesLocation.LocStreetAddr2 = '';
        releaseSitesLocation.LocStreetAddr3 = '';
        releaseSitesLocation.LocStreetAddr4 = '';
        releaseSitesLocation.LocCity = '';
        releaseSitesLocation.LocZip = '';
        releaseSitesLocation.LocDescription = '';
        releaseSitesLocation.LocCountry = '';
        releaseSitesLocation.LocState = '';
        releaseSitesLocation.LocCounty = '';
        releaseSitesLocation.PriContactFirstName = '';
        releaseSitesLocation.PriContactLastName = '';
        releaseSitesLocation.PriOrgName = '';
        releaseSitesLocation.PriContactAddr = '';
        releaseSitesLocation.PriContactCity = '';
        releaseSitesLocation.PriContactCountry = '';
        releaseSitesLocation.PriContactState = '';
        releaseSitesLocation.PriContactZip = '';
        releaseSitesLocation.PriContactCounty = '';
        releaseSitesLocation.PriContactPhone = '';
        releaseSitesLocation.PriContactAltPhone = '';
        releaseSitesLocation.PriContactFax = '';
        releaseSitesLocation.PriContactEmail = '';
        releaseSitesLocation.PriContactAltEmail = '';
        releaseSitesLocation.SecContactFirstName = '';
        releaseSitesLocation.SecContactLastName = '';
        releaseSitesLocation.SecOrgName = '';
        releaseSitesLocation.SecContactAddr = '';
        releaseSitesLocation.SecContactCity = '';
        releaseSitesLocation.SecContactState = '';
        releaseSitesLocation.SecContactCountry = '';
        releaseSitesLocation.SecContactZip = '';
        releaseSitesLocation.SecContactCounty = '';
        releaseSitesLocation.SecContactPhone = '';
        releaseSitesLocation.SecContactFax = '';
        releaseSitesLocation.SecContactEmail = '';
        releaseSitesLocation.addlocation = '';
        releaseSitesLocation.NumberofProposedReleases = '';
        releaseSitesLocation.NumberofAcres = '';
        releaseSitesLocation.CriticalHabitatInvolved = '';
        releaseSitesLocation.CriticalHabitatExplaination = '';
        releaseSitesLocation.ReleaseSiteHistory = '';
        releaseSitesLocation.GPSlatitude1 = '';
        releaseSitesLocation.GPSlongtitude1 = '';
        releaseSitesLocation.GPSlatitude2 = '';
        releaseSitesLocation.GPSlongtitude2 = '';
        releaseSitesLocation.GPSlatitude3 = '';
        releaseSitesLocation.GPSlongtitude3 = '';
        releaseSitesLocation.GPSlatitude4 = '';
        releaseSitesLocation.GPSlongtitude4 = '';
        releaseSitesLocation.GPSlatitude5 = '';
        releaseSitesLocation.GPSlongtitude5 = '';
        releaseSitesLocation.GPSlatitude6 = '';
        releaseSitesLocation.GPSlongtitude6 = '';
        //-------------------------------//
        String testXML = '<?xml version="1.0" encoding="utf-8" standalone="yes"?><Submission><Applicant><AppTitle /><AppFirstName>Kishore</AppFirstName><AppMiddleName/><AppLastName>Applicant</AppLastName><AppEmail>kkumar@phaseonecg.com</AppEmail></Applicant></Submission>';
        inst.parseXML(testXML);
    }
}