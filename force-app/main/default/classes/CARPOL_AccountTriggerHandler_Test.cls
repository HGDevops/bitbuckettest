@isTest
public class CARPOL_AccountTriggerHandler_Test {

	private static final String[] accountNames = new string[] {'Test Account 1', 'Test Account 2'};
    
    @testSetup
    public static void createData() {
        List<Account> accounts = new List<Account>();
        for(integer i=0; i<2; i++) {
	    	Account account = new Account(Name = accountNames[i]);
            accounts.add(account);
        }
        insert accounts;
    }
    
    @isTest
    public static void testForGroupStuff() {
		List<Group> groups = [Select Id, Name From Group Where Name In :accountNames Order By Name];
        System.assertEquals(0, groups.size());

		// Update the accounts and confirm that the groups are created
        List<Account> accounts = [Select Id, Name, Industry, Type From Account Order By Name];
        for(Account account :accounts) {
	    	account.Industry = 'Agriculture';
        }
        update accounts;
		        
		groups = [Select Id, Name From Group Where Name In :accountNames Order By Name];
        System.assertEquals(2, groups.size());
        System.assertEquals(accountNames[0], groups[0].Name);
        System.assertEquals(accountNames[1], groups[1].Name);

		// Update the accounts again and assert that new groups are not created
        for(Account account :accounts) {
	    	account.Type = 'Customer';
        }
        update accounts;

		groups = [Select Id, Name From Group Where Name In :accountNames Order By Name];
        System.assertEquals(2, groups.size());
        System.assertEquals(accountNames[0], groups[0].Name);
        System.assertEquals(accountNames[1], groups[1].Name);
    }
    
}