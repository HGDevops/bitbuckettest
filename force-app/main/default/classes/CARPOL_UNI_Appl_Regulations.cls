public with sharing class CARPOL_UNI_Appl_Regulations {

  

Public Authorization_Junction__c objAuthJunct {get;set;}
Public string strAuthJunctid {get;set;}
 public CARPOL_UNI_Appl_Regulations(ApexPages.StandardController controller) {
 
 objAuthJunct = new Authorization_Junction__c();
strAuthJunctid = ApexPages.currentPage().getParameters().get('id');
objAuthJunct = [select id,Name,Regulation_Description__c from Authorization_Junction__c where id=:strAuthJunctid];

    }
}