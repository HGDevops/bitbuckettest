@isTest(seealldata=true)

public class EFLPlantingIDSelection_controller_test {
    static Report_Summary__c rs;
    static Self_Reporting__c sr;
    static EFL_Related_Record__c eflRelatedRec;
    String auth;
    Boolean Selected;
    List<Self_Reporting__c> selectedLocations = new List<Self_Reporting__c>();
    private static testMethod void testEFLPlantSelection_controller() {
          CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        Facility__c fac = new Facility__c();
        String FacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Inspection Station').getRecordTypeId();
        fac.RecordTypeId = FacRecordTypeId; 
        fac.Name = 'Test Facility';
        insert fac;
        
        Application__c app = new Application__c();
        app = testData.newapplication();
        
        
        Authorizations__c auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
         
        AC__c ac1 = testData.newLineItem('Personal Use',app); 
        ac1.Authorization__c = auth.id;
        ac1.Release_Start_Date__c =  date.today() + 60;
        ac1.Release_End_Date__c = date.today() + 60;
        update ac1;
        
        Facility__c facc = [SELECT Id,Name FROM Facility__c WHERE ID=:fac.id];
        Inspection__c Ins = new Inspection__c();
        String InsRecordTypeId = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Ins.RecordTypeId = InsRecordTypeId;
        Ins.Authorization__c = auth.id;
        Ins.Facility__c = facc.id;
        Ins.Program__c = 'AC';         
        insert Ins;
        
       
        Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;          
          
          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
                  
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Contact_Name1__c = 'Test',
             Day_Phone__c = '(555) 555-1212',
             Primary_Contact_Last_Name__c = 'LName',
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Line_Item__c = ac1.id,                                  
             Authorization__c = auth.id);
            insert objloc;
        
          rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Volunteer Monitoring Report';
        rs.Status__c = 'UnSubmitted';
        insert rs;
      
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()-1;
        sr.Monitoring_Period_End__c = date.today();
        sr.Observation_Date__c = date.today();
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = objloc.id;
        sr.Authorization__c = auth.id;
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = objloc.id;
        sr.Any_Planting_Material_Harvested__c = 'No';
        sr.In_field_Termination_Date__c = date.today()-1;
        sr.In_Field_Description__c = 'Test description';
        sr.How_was_material_disposed__c = 'Both';
        sr.Stored_Quantity__c = 5;
        //sr.Units__c = 'Acres';
        sr.Stored_Material_Type__c = 'test';
        sr.Stored_Description__c = 'Test';
        sr.Off_Field_Destruction_Date__c = date.today();
        sr.Off_Field_Description__c = 'test';
        sr.Before_Harvest_Destruction_Date__c = date.today() + 1;
        sr.Before_Harvest_Description__c = 'test';
        sr.Still_Growing_Quantity__c = 4;
        sr.Still_Growing_Description__c = 'test';
        sr.Start_Date__c = date.today();
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 1;
        sr.Planted_Material_Destroyed_Before_Harves__c = 'Yes';
        sr.Planting_Material_Still_Growing__c  = 'Yes';
        sr.Explanation__c = 'test';
        sr.Unexpected_Effects__c = 'test';
        sr.Deleterious_Effects__c = 'test';
        sr.Deleterious_Effects_Data__c = 'test';
        sr.Crop_Observations__c = 'test';
        sr.Final_Volunteer_Monitoring_Report__c = 'Yes';
        insert sr;
          
        //List<List<EFL_Related_Record__c> RRList = [SELECT ID,Name,Inspection__c,Location__C FROM EFL_Related_Record__c WHERE Inspection__c=:InspectionId];> RRList = [SELECT ID,Name,Inspection__c,Location__C FROM EFL_Related_Record__c WHERE Inspection__c=:InspectionId];
        EFL_Related_Record__c RR = new EFL_Related_Record__c();
        RR.Authorization__c = auth.id;
        insert RR;
        
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = objloc.id;
        //eflRelatedRec.Construct__c  = objconst.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;    
        //EFLPlantingIdSelection_controller temp  = new EFLPlantingIdSelection_controller(new ApexPages.StandardController(Ins));
        //temp.SelfReportDisplayList[0].selected =true;
        
        /*test.startTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(Ins);
         EFLPlantingIdSelection_controller cont = new EFLPlantingIdSelection_controller(sc);
         cont.inspectionId = ins.id;
         
        PageReference pageRef = Page.EFLPlantingIdSelection ;
        pageRef.getParameters().put('id', Ins.id);
        Test.setCurrentPage(pageRef);
        cont.PlantingIdList.add(sr);
        //cont.SelfReportDisplayList[0].selected =true;
        //cont.AuthId = auth.id;
         cont.processSelected();
        test.stopTest();*/
        
         ApexPages.currentPage().getParameters().put('Id',Ins.id);
         ApexPages.StandardController stdCon = new ApexPages.StandardController(Ins);
         EFLPlantingIdSelection_controller  objCPQMYA= new EFLPlantingIdSelection_controller(stdCon);
         List<EFLPlantingIdSelection_controller.cLocation> innerList = new List<EFLPlantingIdSelection_controller.cLocation>();
         EFLPlantingIdSelection_controller.cLocation objCPQMYAInnerClass = new EFLPlantingIdSelection_controller.cLocation(sr);
         objCPQMYAInnerClass.selected = true;
         innerList.add(objCPQMYAInnerClass);
         objCPQMYA.SelfReportDisplayList = innerList;
            
         objCPQMYA.processSelected();
    }

    private static testMethod void testEFLPlantSelection_controller2() {
          CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
          
        Facility__c fac = new Facility__c();
        String FacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Inspection Station').getRecordTypeId();
        fac.RecordTypeId = FacRecordTypeId; 
        fac.Name = 'Test Facility';
        insert fac;
        
        Application__c app = new Application__c();
        app = testData.newapplication();
        
        
        Authorizations__c auth = new Authorizations__c();
        auth = testData.newAuth(app.id);
         
        AC__c ac1 = testData.newLineItem('Personal Use',app); 
        ac1.Authorization__c = auth.id;
        ac1.Release_Start_Date__c =  date.today() + 60;
        ac1.Release_End_Date__c = date.today() + 60;
        update ac1;
        
        Facility__c facc = [SELECT Id,Name FROM Facility__c WHERE ID=:fac.id];
         
        Inspection__c Ins2 = new Inspection__c();
        String InsRecordTypeId = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        
        String InsRecordTypeId2 = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Ins2.RecordTypeId = InsRecordTypeId;
        Ins2.Facility__c = facc.id;
        Ins2.Program__c = 'AC'; 
        Ins2.Authorization__c = auth.id;        
        insert Ins2;
        
        Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;          
          
          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          
          
                  
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Contact_Name1__c = 'Test',
             Day_Phone__c = '(555) 555-1212',
             Primary_Contact_Last_Name__c = 'LName',
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Line_Item__c = ac1.id,                                  
             Authorization__c = auth.id);
            insert objloc;
            
            
        
          rs = new Report_Summary__c();
        rs.Authorization__c = auth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Volunteer Monitoring Report';
        rs.Status__c = 'UnSubmitted';
        insert rs;
      
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()-1;
        sr.Monitoring_Period_End__c = date.today();
        sr.Observation_Date__c = date.today();
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = objloc.id;
        sr.Authorization__c = auth.id;
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = objloc.id;
        sr.Any_Planting_Material_Harvested__c = 'No';
        sr.In_field_Termination_Date__c = date.today() - 1;
        sr.In_Field_Description__c = 'Test description';
        sr.How_was_material_disposed__c = 'Both';
        sr.Stored_Quantity__c = 5;
        sr.Start_Date__c = date.today();
        //sr.Units__c = 'Acres';
        sr.Stored_Material_Type__c = 'test';
        sr.Stored_Description__c = 'Test';
        sr.Off_Field_Destruction_Date__c = date.today();
        sr.Off_Field_Description__c = 'test';
        sr.Before_Harvest_Destruction_Date__c = date.today();
        sr.Before_Harvest_Description__c = 'test';
        sr.Still_Growing_Quantity__c = 4;
        sr.Still_Growing_Description__c = 'test';
        sr.Anticipated_Harvest_Destruct_Date__c = date.today();
        sr.Planted_Material_Destroyed_Before_Harves__c = 'Yes';
        sr.Planting_Material_Still_Growing__c  = 'Yes';
        sr.Explanation__c = 'test';
        sr.Unexpected_Effects__c = 'test';
        sr.Deleterious_Effects__c = 'test';
        sr.Deleterious_Effects_Data__c = 'test';
        sr.Crop_Observations__c = 'test';
        sr.Final_Volunteer_Monitoring_Report__c = 'Yes';
        insert sr;
          
        //List<List<EFL_Related_Record__c> RRList = [SELECT ID,Name,Inspection__c,Location__C FROM EFL_Related_Record__c WHERE Inspection__c=:InspectionId];> RRList = [SELECT ID,Name,Inspection__c,Location__C FROM EFL_Related_Record__c WHERE Inspection__c=:InspectionId];
         EFL_Related_Record__c RR = new EFL_Related_Record__c();
        RR.Authorization__c = auth.id;
        RR.Inspection__c = Ins2.id;
        insert RR;
        
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = auth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = objloc.id;
        //eflRelatedRec.Construct__c  = objconst.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;    
        //EFLPlantingIdSelection_controller temp  = new EFLPlantingIdSelection_controller(new ApexPages.StandardController(Ins));
        //temp.SelfReportDisplayList[0].selected =true;
        
        /*test.startTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(Ins);
         EFLPlantingIdSelection_controller cont = new EFLPlantingIdSelection_controller(sc);
         cont.inspectionId = ins.id;
         
        PageReference pageRef = Page.EFLPlantingIdSelection ;
        pageRef.getParameters().put('id', Ins.id);
        Test.setCurrentPage(pageRef);
        cont.PlantingIdList.add(sr);
        //cont.SelfReportDisplayList[0].selected =true;
        //cont.AuthId = auth.id;
         cont.processSelected();
        test.stopTest();*/
        

        EFLPlantingIdSelection_controller.cLocation ClocationIns=new EFLPlantingIdSelection_controller.cLocation(sr);
        ClocationIns.selected=True;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Ins2);
        EFLPlantingIdSelection_controller cont = new EFLPlantingIdSelection_controller(sc);
        
        List<EFL_Inspection_Questionnaire__c> InspqVarlist=new List<EFL_Inspection_Questionnaire__c>();
        EFL_Inspection_Questionnaire__c InspqVar=new EFL_Inspection_Questionnaire__c();
        InspqVar.Location__c=sr.Release_Record_ID__c ;
        InspqVar.Status__c = 'Open';
        InspqVar.Inspection__c=cont.inspectionId;
        InspqVar.Planting_Id__c=sr.Id;
        Insert InspqVar;
        InspqVarlist.add(InspqVar);
        
        ApexPages.currentPage().getParameters().put('Id',Ins2.id);
         ApexPages.StandardController stdCon2 = new ApexPages.StandardController(Ins2);
         EFLPlantingIdSelection_controller  objCPQMYA2= new EFLPlantingIdSelection_controller(stdCon2);
            objCPQMYA2.processSelected();
    }    
    
    
}