@isTest(seealldata=false)
private class EQS_ManageRFRAutomationsTest 
{
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test() 
    {
      Test.startTest();
      EQSPrepareTestData();
      String jobId = System.schedule('ManageRFRAutomationsTest',CRON_EXP, new EQS_ManageRFRAutomations());

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
      System.assertEquals(CRON_EXP, ct.CronExpression);

      System.assertEquals(0, ct.TimesTriggered);

      System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
      
      Test.stopTest();

      // Now that the scheduled job has executed after Test.stopTest(),
      EQS_Resource_Request__c[] tmpRFR1 = [Select Id,EQS_Responder__c,EQS_Responder_Status__c,EQS_Position__c,EQS_Type__c,EQS_Rotation_Begin_Date__c,EQS_Rotation_End_Date__c From EQS_Resource_Request__c];
      System.assertEquals(tmpRFR1.size(), 1);
      System.assertEquals(tmpRFR1[0].EQS_Responder_Status__c+'','At Incident');

   }
   
   static testMethod void EQSPrepareTestData() 
   {
    
      String EQSAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('APHIS EQS Organization').getRecordTypeId();
      String EQSIncidentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('APHIS EQS Incident').getRecordTypeId();
      String EQSResponderRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('APHIS EQS Responder').getRecordTypeId();
      String EQSRFRRecordTypeId = Schema.SObjectType.EQS_Resource_Request__c.getRecordTypeInfosByName().get('Resource Fill Request - Dispatcher Initiated').getRecordTypeId();
      
      Case EQSIncident=new Case(RecordTypeId=EQSIncidentRecordTypeId,Subject='HPAI INDIANA 01 2016-Test');
      EQSIncident.EQS_Category__c='ANIMAL HEALTH';
      EQSIncident.Priority='Medium';
      EQSIncident.Status='Open';
      insert EQSIncident;
      Account Org1=new Account(RecordTypeId=EQSAccountRecordTypeId,Name='ADMIN, FAC & FLEET MGMT',EQS_Org_Abbreviation__c='AF&FM',EQS_Org_Code__c='05');
      insert Org1;
      Contact Responder1=new Contact(RecordTypeId=EQSResponderRecordTypeId);
      Responder1.FirstName = 'Responder1';
      Responder1.LastName = 'Test1';
      Responder1.Email = 'Responder1.Test1@gmail.com';
      Responder1.EQS_Responder_Status__c='Available';        
      Responder1.AccountId = Org1.Id;
      insert Responder1;
      
      EQS_Position_Cert__c PosiCert=new EQS_Position_Cert__c(Name='Liaison Officer (Aghealth)',EQS_Code__c='A060',EQS_Position_Title__c='Liaison Officer (Aghealth)');
      insert PosiCert;
      EQS_Incident_Position_Certification__c IPos=new EQS_Incident_Position_Certification__c(EQS_Incident__c=EQSIncident.Id,EQS_Qty__c=2,EQS_Position__c=PosiCert.Id);
      insert IPos;
      
      //date MobDate=date.newInstance(2016, 11, 14);
      //date DeMobDate=date.newInstance(2016, 11, 30);
      date MobDate=date.today();
      //MobDate=MobDate.addDays(-4);
      date DeMobDate=MobDate.addDays(15);
      
      EQS_Resource_Request__c RFR1=new EQS_Resource_Request__c(RecordTypeId=EQSRFRRecordTypeId,EQS_Responder__c=Responder1.Id,EQS_Position__c=IPos.Id,EQS_Type__c='Requested',EQS_Rotation_Begin_Date__c=MobDate,EQS_Rotation_End_Date__c=DeMobDate);
      insert RFR1;
      
    }  
    
}