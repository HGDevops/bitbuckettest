public with sharing class CARPOL_Account_Information_Edit{  
public User adminUser{get;set;}
public ID userID{get;set;}
public Boolean isReadOnly {get;set;}
public Account usrAccnt{get;set;}
public CARPOL_Account_Information_Edit(ApexPages.StandardController stdController){
    userID = UserInfo.getUserID();
    System.Debug('userID='+ userID);
      user adminUser = [SELECT Id, Contact.Id, contact.accountId,contact.Account_Admin__c from User WHERE Id=:userID LIMIT 1];
      if(adminUser!=null){           
            usrAccnt = [select name, billingstreet, billingcity, billingstatecode, billingCountryCode, billingPostalCode from account where Id=:adminUser.contact.accountId];
        if(adminUser.Contact.Account_Admin__c==true)
            isReadOnly=false;
        else
            isReadOnly=true;   
    }         
    
}   
     public PageReference cancel()
    {
         PageReference dirpage= new PageReference('/apex/CARPOL_Account_Information');
         dirpage.setRedirect(true);
         return dirpage;
    } 
public pageReference save(){  
    if(!Test.isRunningTest())
        Update usrAccnt;
    return null;   
    }
}