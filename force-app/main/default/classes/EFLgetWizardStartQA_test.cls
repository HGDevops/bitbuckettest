@IsTest(seeAllData=TRUE)
public class EFLgetWizardStartQA_test {
    static testMethod void myUnitTest() {
        
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
         //custom settings insert
         
         testData.insertcustomsettings();
         // Teh: 4/19/2018 no need to do this because seeAllData=true
         //testData.insertURLs();
         Country__c testAF = testData.newcountryaf();
         Country__c testUS = testData.newcountryus();
         CARPOL_External_Landing__c importLanding = testData.newExternalLanding('Import1', 1);
         Program_Line_Item_Pathway__c testPathway = testData.newCaninePathwayImport();
         testPathway.Display_Regulated_Article_Search__c = true;
         testPathway.Display_Component_Selection__c = true;
         testPathway.Is_Scientific_Name_Required__c = true;
         testPathway.Is_Country_of_Export_Required__c = true;
         testPathway.Is_Country_of_Transit_Required__c = true;
         String landopt = 'Import';
         testPathway.Show_Intended_Use__c = true;
         testPathway.Country_of_Destination_required_for__c = 'Import'; // just adding to make test pass
         testPathway.EFLDiseaseValidationRequired__c = true;
         testPathway.Is_Scientific_Name_Required__c = true;//vv 3-7-17         
         update testPathway;
         Intended_Use__c testIU = testData.newIntendedUse(testPathway.Id);
         Wizard_Questionnaire__c testWQ = testData.newWizardQuestionnaire(testPathway.Id, testIU.Id);
         
         //testWQ.EFLDiseaseSensitivity__c = 'ASF:Free';
         update testWQ;
         Wizard_Selections__c wzsel1 = new Wizard_Selections__c();
         wzsel1.Wizard_Questionnaire__c = testWQ.id;
         wzsel1.Value__c = 'Test 1';        
         insert wzsel1;
         EFLGeography__c EFLGeo = new EFLGeography__c (Name='Afghanistan',EFLGeographyType__c = 'Country',EFLCode__c ='AF', EFLStatus__c= 'Active');
         Insert EFLGeo;                                                   
        EFLRelatedGeography__c relGeo = new EFLRelatedGeography__c (EFLPrimaryGeography__c =EFLGeo.id ,EFLSecondaryGeography__c =EFLGeo.id);
        insert relGeo ;  
        Disease__c dis = new Disease__c(name = 'African Swine Fever',Abbreviation__c='ASF');
        insert Dis;   
        EFLDiseaseSensitivity__c EFLDes1 = New EFLDiseaseSensitivity__c( EFLGeography__c=EFLGeo.id, EFLDiseaseSensitivity__c ='ASF:Free', EFLDiseasePest__c=dis.id);
        Insert EFLDes1;
        EFLDiseaseSensitivity__c EFLDes2 = New EFLDiseaseSensitivity__c( EFLGeography__c=EFLGeo.id, EFLDiseaseSensitivity__c ='FMD:Affected', EFLDiseasePest__c=dis.id);
        Insert EFLDes2;
         
         Regulated_Article__c testRA = testData.newRegulatedArticle(testPathway.Id);
     
         //Level_1_Region__c l1r = testData.newlevel1regionAL();
         Level_1_Region__c objL1R = new Level_1_Region__c();
         objL1r.Country__c = testUS.Id;
         objL1r.Name = 'Alabama';
         objL1r.Level_1_Name__c = 'Alabama';
         objL1r.Level_1_Region_Code__c = 'AL';
         objL1r.Level_1_Region_Status__c = 'Active';
         insert objL1r;  
         level_2_Region__c l2 = new level_2_Region__c();
         l2.Name='Test';
         l2.level_1_Region__c=objL1R.Id;    
         l2.Level_2_Region_Status__c = 'Active'; 
         insert l2;
         Signature__c thumb = testData.newthumbprint();
         thumb.Response_Type__c = 'Permit';
         update thumb;
         testRA.Thumbprint__c = thumb.id;
         update testRA;
         Component__c comp = new Component__c();
         comp.Name = 'Embryo, Semen or Cloning Tissue';
         comp.Description__c = 'Test Description';
         comp.Default_to_Pathway__c = testPathway.id;
         comp.RegulatedArticleID__c = testRA.id;
         comp.BreedID__c = testData.newbreed().id;
         comp.Status__c = 'Active';
         comp.component_id__c = 7890123;
         insert comp;
         
         RA_Component_Junction__c rcj = new RA_Component_Junction__c();
         rcj.Component__c = comp.id;
         rcj.Regulated_Article__c = testRA.id;
         rcj.Thumbprint__c = thumb.id;
         rcj.Status__c = 'Active';
         insert rcj;
         
         RA_Scientific_Names__c objSN = testData.newScientificName();
         Set<ID> FinalRAGroupIds = new Set<ID>();
         FinalRAGroupIds.add(testRA.id);
         Set <ID> CountryGroupIDs = new Set<ID>();
         CountryGroupIDs.add(testAF.id);
        String sSelCountryExport = 'Afghanistan';
        Set<String> cntryorigdiseases = new Set<String>();
        cntryorigdiseases.add('African Swine Fever');
         Test.startTest();
        //EFLgetWizardStartQA WQA = new EFLgetWizardStartQA();
        Wizard_Questionnaire__c WQQA = EFLgetWizardStartQA.getWizardStartQA(testPathway.id, landopt, testRA.Name, FinalRAGroupIds, testAF.Name , CountryGroupIDs, sSelCountryExport, objL1R.Name, objL1R.Name, testWQ.ID, comp.Name, cntryorigdiseases, true);
        Test.stopTest();
        
    }
}