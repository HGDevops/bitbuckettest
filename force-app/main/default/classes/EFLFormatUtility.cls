/*-----------------------------------------------------------------------  
 @Purpose: This Utility class helps in handling fomratting functionality 
           across eFile
           i.Format Contact or Account Address field
           ii.Format Date
 ----------------------------------------------------------------------*/
public with Sharing class EFLFormatUtility {

/*
 @Purpose: Provide address in following format
           Format: Street
                   City, StateCode ZipCode

 */     
    Public static string formatAddress(address address){
      string BLANK = ' ';
       List<String> listString = new List<String>();
        
        //Street  
        if(address.getStreet() !=null){
            string street = address.getStreet()+'<br/>';
            listString.add(street); 
        } 
        //City
        if(address.getCity() !=null) {
            listString.add(address.getCity()+',');
        }
        //State
        if(address.getState() !=null){
            listString.add(address.getStateCode());
        }
        //Zip Code
        if(address.getPostalCode() !=null){
            listString.add(address.getPostalCode());
        }
        
        String formattedAddress = String.join(listString,BLANK );                 
       return formattedAddress;
    }    

/*
 @Purpose: Provide Date in required format
           Date Format's => 'MM/DD/YYYY' or 'yyyy-MM-dd HH:mm:ss'
 */     
   public static String formatDate(Date rawDate, String format) {
    if (rawDate == null) return null;

    DateTime dt = DateTime.newInstance(rawDate.year(), rawDate.month(), rawDate.day());
    return dt.format(format);
  }    
    
}