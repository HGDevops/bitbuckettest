@isTest(SeeAllData=true)
private class EFLPreparePermit_extensionTest { 
    
    
    
/*    static testMethod void testEFLPreparePermit_extension() {
        
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Group__c objgrp = testdata.newgroup();
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp);      
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        //Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        //Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
        //Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Attachment attach = testData.newattachment(ac.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.RecordTypeID = ACauthRecordTypeId;
        update objauth;
        ac.Authorization__c = objauth.Id;
        update ac;
        
        
        Test.startTest();
        
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        //Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        //Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        // Added by VV 
        String PPQTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        String authRecordTypeId2 = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        String authRecordTypeId3= Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        //objreg4.RecordTypeid = ReguRecordTypeId;
        //update objreg4;
        Domain__c objprog = testData.newProgram('PPQ');
        Program_Prefix__c objPrefix = new Program_Prefix__c();
        objPrefix.Program__c = objprog.Id;
        objPrefix.Name = '251';
        objPrefix.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
        insert objPrefix;
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP Jialin';
        objTP.Recordtypeid = PPQTPRecordTypeId ;
        objTP.Program_Prefix__c = objPrefix.Id;
        insert objTP; 
        Domain__c Program = new Domain__c (Name = 'PPQ');
        Insert Program;
        Program_Line_Item_Pathway__c ppthwy = new Program_Line_Item_Pathway__c (name = 'Plants or Plant Products', Program__c= program.id, Permit_PDF_Template__c='CARPOL_PPQ_PermitPDF');
        insert ppthwy;
        Program_Prefix__c objPrefix1 = new Program_Prefix__c();
        objPrefix1.Program__c = objprog.Id;
        objPrefix1.Name = '252';
        objPrefix1.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
        insert objPrefix1;
        Signature__c objTP1 = new Signature__c();
        objTP1.Name = 'Test AC TP Jialin';
        objTP1.Recordtypeid = PPQTPRecordTypeId ;
        objTP1.Program_Prefix__c = objPrefix1.Id;
        insert objTP1; 
        
        Program_Prefix__c objPrefix2 = new Program_Prefix__c();
        objPrefix2.Program__c = objprog.Id;
        objPrefix2.Name = '253';
        objPrefix2.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
        insert objPrefix2;
        Signature__c objTP2 = new Signature__c();
        objTP2.Name = 'Test AC TP Jialin';
        objTP2.Recordtypeid = PPQTPRecordTypeId ;
        objTP2.Program_Prefix__c = objPrefix2.Id;
        insert objTP2;
        objauth.Program_Pathway__c = ppthwy.id;
        objauth.Thumbprint__c = objTP.Id;
        objauth.Effective_Date__c = system.today();
        objauth.Proposed_Date_of_Arrival__c = system.today()-1 ;
        update objauth;
        
        List<SelectOption> selPorts = new List<SelectOption>();
        selPorts.add(new SelectOption(fac.id, fac.Name));
        selPorts.add(new SelectOption(fac2.id, fac2.Name));
        
        Facility_Vet_Services_Junction__c facvetjn = new Facility_Vet_Services_Junction__c();
        facvetjn.Facility__c= fac2.id;
        facvetjn.Vet_Services__c= objacct.id;
        facvetjn.Authorization__c=objauth.id;
        insert facvetjn;

        
        PageReference pageRef = Page.EFLPreparePermit;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        //EFLPreparePermit_extensionTest (ApexPages.StandardController stdController)
        //  {
        //    this.objauth= (Authorizations__c)stdController.getRecord();
        // }
        EFLPreparePermit_extension acepreg = new EFLPreparePermit_extension(sc);
        System.assert(acepreg != null);
        acepreg.regulation = objreg1;
        acepreg.getResults();
        acepreg.setRegulationType();
        acepreg.getAddByOptions();
        acepreg.getPorts();
        acepreg.savePorts();
        acepreg.getVets();
        acepreg.saveVets();
        acepreg.getAddByOptions();
        acepreg.updateSignature();
        acepreg.viewDraftPDF();
        acepreg.createNewRegulation();
        acepreg.managerrole = True ;
        acepreg.index = 1;
        acepreg.portpage ='';
      
        
        Test.stopTest();
        
        //System.assert(acepreg != null);
    }
*/   
 /*
    @isTest
    static void testEFLPreparePermit_extensiontwo() {
        
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port'); 
        fac.account__C =  objacct.id;
        update fac;
        Facility__c fac2 = testData.newfacility('Foreign Port');
        fac.account__C =  objacct.id;
        update fac2;     
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp);  
        ac.Port_of_Embarkation__c =  fac2.id;
        ac.Port_of_Entry__c = fac2.id;   
        update ac;
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
        Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Attachment attach = testData.newattachment(ac.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.RecordTypeID = ACauthRecordTypeId;
        update objauth;
        ac.Authorization__c = objauth.Id;
        update ac;
        
        Test.startTest();
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        // Added by VV 
        String PPQTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        String authRecordTypeId2 = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        String authRecordTypeId3= Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        objreg4.RecordTypeid = ReguRecordTypeId;
        update objreg4;
        Domain__c objprog = testData.newProgram('PPQ');
        Program_Prefix__c objPrefix = new Program_Prefix__c();
        objPrefix.Program__c = objprog.Id;
        objPrefix.Name = '251';
        objPrefix.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
        insert objPrefix;
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP Jialin';
        objTP.Recordtypeid = PPQTPRecordTypeId ;
        objTP.Program_Prefix__c = objPrefix.Id;
        insert objTP; 
        Domain__c Program = new Domain__c (Name = 'PPQ');
        Insert Program;
        Program_Line_Item_Pathway__c ppthwy = new Program_Line_Item_Pathway__c (name = 'Plants or Plant Products', Program__c= program.id, Permit_PDF_Template__c='CARPOL_PWS_PERMITPDF',Expiration_Timeframe__c=1095);
        insert ppthwy;
        Program_Prefix__c objPrefix1 = new Program_Prefix__c();
        objPrefix1.Program__c = objprog.Id;
        objPrefix1.Name = '252';
        objPrefix1.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
        insert objPrefix1;
        Signature__c objTP1 = new Signature__c();
        objTP1.Name = 'Test AC TP Jialin';
        objTP1.Recordtypeid = PPQTPRecordTypeId ;
        objTP1.Program_Prefix__c = objPrefix1.Id;
        insert objTP1; 
        
        Program_Prefix__c objPrefix2 = new Program_Prefix__c();
        objPrefix2.Program__c = objprog.Id;
        objPrefix2.Name = '253';
        objPrefix2.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
        insert objPrefix2;
        Signature__c objTP2 = new Signature__c();
        objTP2.Name = 'Test AC TP Jialin';
        objTP2.Recordtypeid = PPQTPRecordTypeId ;
        objTP2.Program_Prefix__c = objPrefix2.Id;
        insert objTP2;
        objauth.Program_Pathway__c = ppthwy.id;
        objauth.Thumbprint__c = objTP.Id;
        objauth.Effective_Date__c = system.today();
        update objauth;
        
        PageReference pageRef = Page.EFLPreparePermit;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        EFLPreparePermit_extension acepreg = new EFLPreparePermit_extension(sc);
        System.assert(acepreg != null);
        acepreg.regulation = objreg1;
        acepreg.getResults();
        acepreg.setRegulationType();
        acepreg.getAddByOptions();
        //acepreg.selectInput();
        acepreg.getAddByOptions();
        acepreg.updateSignature();
        acepreg.viewDraftPDF();
        acepreg.createNewRegulation();

        Test.stopTest();
        
        //System.assert(acepreg != null);
    }
*/

    @isTest
    static void testEFLPreparePermit_extensionthree() {
        
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettingsWithBRSTriggerDisabled();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp);      
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
        Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Attachment attach = testData.newattachment(ac.Id);           
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        objauth.RecordTypeID = ACauthRecordTypeId;
        update objauth;
        ac.Authorization__c = objauth.Id;
        update ac;
        
        Test.startTest();
        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        // Added by VV 
        String PPQTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        String authRecordTypeId2 = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        String authRecordTypeId3= Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        objreg4.RecordTypeid = ReguRecordTypeId;
        update objreg4;
        Domain__c objprog = testData.newProgram('VS');
        Program_Prefix__c objPrefix = new Program_Prefix__c();
        objPrefix.Program__c = objprog.Id;
        objPrefix.Name = '641';
        objPrefix.Permit_PDF_Template__c = 'Carpol_vs_authorizationpdf';
        insert objPrefix;
        
        Signature__c objTP = new Signature__c();
        objTP.Name = 'Test AC TP Jialin';
        //objTP.Recordtypeid = authRecordTypeId2 ;
        objTP.Program_Prefix__c = objPrefix.Id;
        insert objTP; 
        Domain__c Program = new Domain__c (Name = 'VS');
        Insert Program;
        Program_Line_Item_Pathway__c ppthwy = new Program_Line_Item_Pathway__c (name = 'Bovine', Program__c= program.id, Permit_PDF_Template__c='Carpol_vs_authorizationpdf',Expiration_Timeframe__c=1095);
        insert ppthwy;
        Program_Prefix__c objPrefix1 = new Program_Prefix__c();
        objPrefix1.Program__c = objprog.Id;
        objPrefix1.Name = '641';
        objPrefix1.Permit_PDF_Template__c = 'Carpol_vs_authorizationpdf';
        insert objPrefix1;
        Signature__c objTP1 = new Signature__c();
        objTP1.Name = 'Test AC TP Jialin';
        //objTP1.Recordtypeid = authRecordTypeId2 ;
        objTP1.Program_Prefix__c = objPrefix1.Id;
        insert objTP1; 
        
        Program_Prefix__c objPrefix2 = new Program_Prefix__c();
        objPrefix2.Program__c = objprog.Id;
        objPrefix2.Name = '253';
        objPrefix2.Permit_PDF_Template__c = 'Carpol_vs_authorizationpdf';
        insert objPrefix2;
        Signature__c objTP2 = new Signature__c();
        objTP2.Name = 'Test AC TP Jialin';
        //objTP2.Recordtypeid = authRecordTypeId2 ;
        objTP2.Program_Prefix__c = objPrefix2.Id;
        insert objTP2;
        objauth.Program_Pathway__c = ppthwy.id;
        objauth.Thumbprint__c = objTP.Id;
        objauth.Effective_Date__c = system.today();
        objauth.Expiration_Timeframe_Override__c = 'Pathway';
        objauth.Expiration_Timeframe__c = '365';
        
        update objauth;
        
        //String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        //Account objacct = testData.newAccount(AccountRecordTypeId);
        
        Facility__c fac1 = new Facility__c();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        fac1.RecordTypeId = PortsFacRecordTypeId;
        fac1.Name = 'Test_port';
        fac1.Type__c = 'Domestic Port'; 
        fac1.Account__c = objacct.id; 
        insert fac1;  
        
        fac.account__C =  objacct.id;
        update fac2; 
        
        ac.Port_of_Entry__c = fac1.id;
        ac.Port_of_Embarkation__c = fac2.id;
        update ac;
        
        Facility_Vet_Services_Junction__c facJn = new Facility_Vet_Services_Junction__c();
        facJn.Facility__c = fac1.id;
        facJn.Vet_Services__c = objacct.id;
        facJn.Authorization__c = objauth.id;
        insert facJn;
        
        EFLAdditional_Recipient__c addnRec = new EFLAdditional_Recipient__c();
        addnRec.Name = 'Test Recep';
        addnRec.EFLEmail__c = 'test@test.com';
        addnRec.EFLPhone_Number__c = '567-098-8765';     
        insert addnRec;
        
        EFLAuth_AddlRecpnt__c junc = new EFLAuth_AddlRecpnt__c();
        junc.EFLAdditional_Recipient__c = addnRec.id;
        junc.EFLAuthorization__c = objauth.id;
        junc.EFLEmail__c = addnRec.EFLEmail__c;
        junc.EFLPhone_Number__c = addnRec.EFLPhone_Number__c;        
        insert junc;
        
        PageReference pageRef = Page.EFLPreparePermit;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        EFLPreparePermit_extension acepreg = new EFLPreparePermit_extension(sc);
        System.assert(acepreg != null);
        acepreg.regulation = objreg1;
        acepreg.getVets();
        acepreg.saveVets();
        acepreg.getResults();
        acepreg.setRegulationType();
        acepreg.getAddByOptions();
        
        //acepreg.selectInput();
        acepreg.getAddByOptions();
        acepreg.updateSignature();
        acepreg.viewDraftPDF();
        acepreg.createNewRegulation();
        acepreg.getAddlRcpnts();
        acepreg.updateAddRcpnts();
/*        
        objauth.Thumbprint__c = objTP.id;
        objauth.Recordtypeid = authRecordTypeId ;
        objauth.Authorization_Type__c = 'Permit';
        update objauth;
        Facility__c  entry = testData.newfacility('Domestic Port');
        Authorization_Junction__c objauthjun = new Authorization_Junction__c();
        objauthjun.Authorization__c = objauth.id;
        objauthjun.Port__c = entry.id;
        insert objauthjun;
        
        Authorizations__c objauth2 = objauth.clone(false,false,false,false);
        
        insert objauth2;
        ac3.Authorization__c = objauth2.id;
        update ac3;
        ApexPages.Standardcontroller sc7 = new ApexPages.Standardcontroller(objauth2);
        ApexPages.currentPage().getParameters().put('Id',objauth2.id);
        EFLPreparePermit_extension acepreg7 = new EFLPreparePermit_extension(sc7);
        
        acepreg7.live_animal_nosave_port = false;
        acepreg7.showportspage = false;
        
        //acepreg7.attachPDFAndSign();
        
        Authorizations__c objauth3 = objauth2.clone(false,false,false,false);
        objAuth3.Recordtypeid=authRecordTypeId2;
        insert objAuth3;
        ac3.Authorization__c = objauth3.id;
        update ac3;
        ApexPages.Standardcontroller sc8 = new ApexPages.Standardcontroller(objauth3);
        ApexPages.currentPage().getParameters().put('Id',objauth3.id);
        EFLPreparePermit_extension acepreg8 = new EFLPreparePermit_extension(sc8);
        acepreg8.live_animal_nosave_port = false;
        acepreg8.showportspage = false;
        acepreg8.attachPDF();
        
        
        objauth3.recordTypeId=ACauthRecordTypeId;
        update objauth3;
        ApexPages.Standardcontroller sc9 = new ApexPages.Standardcontroller(objauth3);
        ApexPages.currentPage().getParameters().put('Id',objauth3.id);
        EFLPreparePermit_extension acepreg9 = new EFLPreparePermit_extension(sc9);
        
        acepreg9.live_animal_nosave_port = true;
        acepreg9.attachPDF();
        acepreg9.setAddlRcpnts();
        acepreg9.saveChanges();
        acepreg9.returntoreviewrecord();
        acepreg9.getAu();
        
        
        acepreg9.saveIssuedDt();
        acepreg9.revertEffDt();
        acepreg9.returntoauth();
*/
        Test.stopTest();
        
        //System.assert(acepreg != null);
    }
}