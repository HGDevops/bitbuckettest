public with sharing class EFLRegAnimalsTableController {
    @AuraEnabled
    public static List<dataTableWrapper> fetchTableDate(String annualReportId){
        List<EFLRegistered_Animal__c> animalsList = [Select ID,EFLAnimalName__r.name,EFLAnimal__c, EFLAnnual_Report__c,
                                                     EFLColumnBHeldNotUsed__c,EFLColumnCUsedPainMinimized__c,
                                                     EFLColumnDUsedPainMinimized__c,ELFColumnEPainNotMinimized__c,
                                                     EFLOtherAnimal__c FROM EFLRegistered_Animal__c WHERE
                                                     EFLAnnual_Report__c =: annualReportId
                                                     and EFLOtherAnimal__c = false
                                                     and EFLSelectedForReport__c = true];
        list<dataTableWrapper> finalAnimalList = new List<dataTableWrapper>();
        list<String> totalColumnValues = new List<String>();
        list<Boolean> showExceptionLinks = new List<Boolean>();
        for(EFLRegistered_Animal__c each : animalsList){
            //totalColumnValues.add('');
            //showExceptionLinks.add(false);
            finalAnimalList.add(new dataTableWrapper(each,'',false));
        }
        
        return finalAnimalList;
    }
    
    @AuraEnabled
    public static void deleteAnimalReg(String animalToDelete){
        //delete wrapData.animalToDelete;
        integer len = animalToDelete.length();
        integer startPt = len-20;
        integer endPt = len-2;
        string regAnId = animalToDelete.substring(startPt, endPt);        
        //system.debug('#### regAnId = '+regAnId);
        eflregistered_animal__c regAntoDelete = [select id from eflregistered_animal__c where id=:regAnId];
        delete regAntoDelete;
    }
    @AuraEnabled
    public static void saveRegAnmls(List<EFLRegistered_Animal__c> anmlList)
    {
        //System.debug('### UpdatedData: '+anmlList);
        database.update(anmlList);
        //Update conList;
    }    
    public class dataTableWrapper{
        @AuraEnabled
        public EFLRegistered_Animal__c finalAnimalList{get;set;}
        @AuraEnabled
        public String totalColumnValues{get;set;}
        @AuraEnabled
        public Boolean showExceptionLinks{get;set;}
        public dataTableWrapper(EFLRegistered_Animal__c finalAnimalListX, String totalColumnValueX, Boolean showExceptionLinkX){
            this.finalAnimalList = finalAnimalListX;
            this.totalColumnValues = totalColumnValueX;
            this.showExceptionLinks = showExceptionLinkX;
        }
    }
    
}