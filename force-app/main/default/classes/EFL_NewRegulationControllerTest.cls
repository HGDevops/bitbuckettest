@isTest (seeAllData=true)
private class EFL_NewRegulationControllerTest {
    
    private static testMethod void test() {
        
        Test.startTest();
        PageReference ref = Page.EFLNewRegulation;
        Test.setCurrentPage(ref);
        User u = new user();
        u = EFLUserTestDataFactory.getUser('Customer Community Plus');  
        System.runAs(u){
            EFL_NewRegulationController controller = new EFL_NewRegulationController();
            controller.regulation.Short_Name__c = 'test 123';
            controller.regulation.Regulation_Description__c = 'testing 1234';
            controller.regulation.title__c = 'coolest title';
            controller.regulation.Custom_Name__c = 'craziest';
            controller.save();
            System.assert(controller.regulation.id != null);
            ref = Page.EFL_RegulationsEditPage;
            ref.getParameters().put('Id', String.valueOf(controller.regulation.Id));
            Test.setCurrentPage(ref);
            ApexPages.StandardController sc = new ApexPages.StandardController(controller.regulation);
            EFL_NewRegulationController newController = new EFL_NewRegulationController(sc);
            newController.existingRegulation.title__c = 'Craziness';
            newController.updateRegulation();
            Regulation__c reg = [select id, title__c from Regulation__c where id =: newController.existingRegulation.id];
            System.assert(reg.title__c == newController.existingRegulation.title__c);
        }
        Test.stopTest();
    }

}