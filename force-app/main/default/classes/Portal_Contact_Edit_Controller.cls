public  with sharing class Portal_Contact_Edit_Controller {
    public Id contactID{get;set;}
    public Contact cont{get;set;}
    public Contact obj {get;set;} 
    public ApexPages.StandardController controller{get;set;}
    
    public Portal_Contact_Edit_Controller(ApexPages.StandardController controller)
    {
        contactID=ApexPages.currentPage().getParameters().get('id');
        if(contactID!=null){
        obj=(Contact)controller.getRecord();
        cont = [Select FirstName, LastName, AccountId, Contact_checkbox__c,Account_Admin__c, State__c,
               Recordtypeid, Phone, MobilePhone, Email, MailingCountryCode, MailingStreet, MailingCity,
                MailingStateCode, MailingPostalCode, OtherCountryCode, OtherStreet, OtherCity,
                OtherStateCode, OtherPostalCode, Name, MailingAddress, OtherAddress
                from Contact where ID=:contactID];
      
       // controller = controller;
        }
        else
        {
            cont = new Contact();
            cont.MailingCountryCode = 'US';
        }
    }
    
      public PageReference save()
    {
       system.debug('--------cont = '+cont);
       
    if(contactID!=null){
               // contactID = ApexPages.currentPage().getParameters().get('id');
               // obj.id = contactID;
               // obj.Phone = cont.Phone;
                update cont;
                PageReference pg= new PageReference ('/apex/Portal_Contact_Detail?id='+contactID);
                pg.setRedirect(true);
                return pg;
                }else{
                        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
                        cont.recordtypeId = conRecordTypeId ;
                        upsert cont;
                        id contid = cont.id;
                        PageReference pg= new PageReference ('/apex/Portal_Contact_Detail?id='+contid);
                        pg.setRedirect(true);
                        return pg;
        
                }
     } 
     
    public PageReference getEditPage()
    {
         PageReference dirpage= new PageReference('/apex/Portal_Contacts_Edit?id='+contactID);
         dirpage.setRedirect(true);
         return dirpage;
    }
    
     public PageReference cancel()
    {
         PageReference dirpage= new PageReference('/apex/contacts');
         dirpage.setRedirect(true);
         return dirpage;
    }  

}