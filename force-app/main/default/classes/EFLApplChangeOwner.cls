//Controller for page EFLApplChangeOwner. Account admin for a portal can access this page to change the contact on an application
public with sharing class EFLApplChangeOwner {
    public Application__c currApp{get;set;}
    public EFLApplChangeOwner(ApexPages.StandardController controller) {  

        String appId = EFLGenericUtility.sanitizeString(ApexPages.currentPage().getParameters().get('appId'));
        //system.debug('>>>> appId= '+ appId);
        if(appId != null){
            ApexPages.currentPage().getParameters().put('id', appId);
        }
        try{
            this.currApp = [select id, name, Authorized_User__c, Applicant_Name__c from application__c where id =:appId];
            //System.debug('The Record>>'+currApp);
        }catch(Exception e){
            System.Debug('Current APP exception'+e);
        }
        
    }
    
    public pagereference save(){
        //System.Debug('The Owner is>>>>>'+currApp.Applicant_Name__c);
        currApp.Authorized_User__c =currApp.Applicant_Name__c;
        update currApp;
        return new Pagereference('/apex/account_applications');
    }
    
    public pagereference cancel(){
        return new Pagereference('/apex/account_applications');
    }
    
}