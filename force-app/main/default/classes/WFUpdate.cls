//7-12-16 VV created for PEQ requirement
//For the Auth that has a review record that was created 15 days ago, and SPRO has not updated
//then set the WF task to complete and add comments
global class WFUpdate implements Database.Batchable<sObject>
{
    global  database.querylocator start(database.batchablecontext bc)
    {       
        // updated 5/9/2019 by JB: adding selective criteria to query locator.
        // string query = 'select id, Authorization__c, createddate, status__c from Reviewer__c';
        // return database.getquerylocator(query);
        // Starting new query logic.  JB
        Date expDate = Date.today().addDays(-15);
        String status = 'Completed';
        return database.getquerylocator([SELECT id, Authorization__c, createddate, status__c 
                                         FROM Reviewer__c 
                                         WHERE Status__c != :status 
                                         AND CreatedDate < :expDate]);
    }
    global void execute (database.batchablecontext bc, list<Reviewer__c> revRec)
    {
        // JB: adding logic to get SOQL & DML out of for-loop.
        list<Id> authIds = new list<Id>();
        for (Reviewer__c  r: revRec){
            authIds.add(r.Authorization__c);
        }
        
        if(!authIds.isEmpty()){
            list<Workflow_Task__c> stuffToDo = [SELECT id 
                                                FROM Workflow_Task__c 
                                                WHERE Authorization__c IN :authIds 
                                                AND status__c <> 'Complete'];
            for(Workflow_Task__c t : stuffToDo){
                t.status__c = 'Complete';
                t.comments__c = 'No response from the State Reviewer';
            }
            
            if(!stuffToDo.isEmpty()){
                update stuffToDo;
            }
        }
        /*
       for (Reviewer__c  r: revRec)
         {
         if (system.today().day() - r.createddate.day() > 15 && r.status__c != 'Completed')
            {
              Workflow_Task__c WFtoUpd = [select id from Workflow_Task__c where Authorization__c = : r.Authorization__c AND status__c <> 'Complete'];
              WFtoUpd.status__c = 'Complete';
              WFtoUpd.comments__c = 'No response from the State Reviewer';
              update WFtoUpd;
            }
         }*/
    }
    global void finish(database.batchablecontext bc){}
}