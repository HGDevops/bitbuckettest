public without sharing class EFLFileListServices {

    public static void doDelete(Id fileId, boolean doDelete){
        // add option for isDeleted__c = false rathr than delete.
        Uploaded_File__c f = new Uploaded_File__c(Id = fileId);
        if(doDelete == true){
        	delete f;
        }else{
            f.Is_Deleted__c = true;
            update f;
        }
    }
}