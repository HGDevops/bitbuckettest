@isTest
global class GoogleAPIMockImpl implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.
        //System.assertEquals('https://maps.googleapis.com/maps/api/geocode/xml?address=2120+West+End+Avenue&key=AIzaSyAAWygMsr763OpAOyrYqhKXlwB3_Yz1h4k', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        HttpResponse res = new HttpResponse();
         res.setBody('Central Daylight Time');
        res.setStatusCode(200);
        return res;
        
    }
   }