@IsTest
private class portal_application_edit_control_Test {
    
    static CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
    
    @isTest
    static void CommunityAccessContactTest() {
        
        testData.insertcustomsettings();
        Account newAccount = new Account();
        newAccount = testData.newAccount(EFLGenericUtility.getRecordTypeId('APHIS Efile Standard Account'));
        
        Contact newContact = new Contact();
        newContact = testData.newContact();
        newContact.accountid = newAccount.id;
        newContact.Account_Admin__c = true;
        update newContact;
        
        Applicant_Contact__c associatedContact = new Applicant_Contact__c();
        associatedContact = testData.newappcontact();
        
        associatedContact.Account__c = newAccount.id;
        update associatedContact;
        
        Account newAccount1 = new Account();
        newAccount1 = testData.newAccount(EFLGenericUtility.getRecordTypeId('APHIS Efile Standard Account'));
        Contact newContact1 = new Contact();
        newContact1 = testData.newContact();
        newContact1.accountid = newAccount1.id;
        update newContact1;
        
        Contact newContact2 = new Contact();
        newContact2 = testData.newContact();
        newContact2.accountid = newAccount1.id;
        update newContact2;
        
        Contact newContact3 = new Contact();
        newContact3 = testData.newContact();
        newContact3.accountid = newAccount1.id;
        update newContact3;
        
        user usershare = new User();
        usershare.Username ='orgadmin12192018@test.com';
        usershare.LastName = 'orgadmin12192018';
        usershare.Email = 'orgadmin12192018@test.com';
        usershare.alias = 'org19201';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name = 'eFile Applicant Plus' limit 1 ].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = newContact.id;
        insert usershare;
        
        user usershare1 = new User();
        usershare1.Username ='orgadmin12202018@test.com';
        usershare1.LastName = 'orgadmin12202018';
        usershare1.Email = 'orgadmin12202018@test.com';
        usershare1.alias = 'org20201';
        usershare1.TimeZoneSidKey = 'America/New_York';
        usershare1.LocaleSidKey = 'en_US';
        usershare1.EmailEncodingKey = 'ISO-8859-1';
        usershare1.ProfileId = [select id from Profile where Name = 'eFile Applicant' limit 1 ].Id;
        usershare1.LanguageLocaleKey = 'en_US';
        usershare1.ContactId = newContact1.id;
        insert usershare1;  
        
        SpringCMEos__EOS_Type__c eA = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(), true);
        SpringCMEos__EOS_Type__c eO = (SpringCMEos__EOS_Type__c)SpringCMTestDataFactory.createSObject(new SpringCMEos__EOS_Type__c(name = 'AC__c',
                                                                                                                                   SpringCMEos__Folder_Name__c = '{!Name}',
                                                                                                                                   SpringCMEos__Folder_Name_Format__c = '{0}',
                                                                                                                                   SpringCMEos__Path__c = '/path/{!AC__c.Id}',
                                                                                                                                   SpringCMEos__Path_Format__c = '/path/{1}',
                                                                                                                                   SpringCMEos__Variables__c = 'Name,AC__c.Id'), true);
        SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
        
        Account realORG = new Account();
        realORG = testData.newAccount(EFLGenericUtility.getRecordTypeId('APHIS Efile Standard Account'));
        newContact.accountid = realORG.id;
        update newContact;
        newContact1.accountid = realORG.id;
        update newContact1;
        
        List<Group>appLineItemQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Application__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'Standard_Application_Queue'];
        List<Group>authQueueRecord=[SELECT Id,name,(select QueueId, SobjectType from QueueSobjects where SobjectType = 'Authorizations__c' limit 1) FROM Group where type='Queue' and DeveloperName = 'BRS_Reviewer'];
        
        CARPOL_UNI_DisableTrigger__c chDt = new CARPOL_UNI_DisableTrigger__c(); 
        chDt.Name = 'EFLChangeHistoryTrigger'; 
        chDt.Disable__c = true; 
        insert chDt; 
        
        Application__c app = new Application__c();
        AC__c LineItem = new AC__c();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c RA =  testData.newRegulatedArticleWithScientificName(plip.id);
        Program_Line_Item_Pathway__c plip1 = testData.newBRSPathway();
        Regulated_Article__c RA1 =  testData.newRegulatedArticleWithScientificName(plip1.id);
        
        Link_Regulated_Articles__c LRA = new Link_Regulated_Articles__c();
        Construct__c con = new Construct__c();
        
        CARPOL_URLs__c Urls = new CARPOL_URLs__c(name='Landing_Page', URL__c='/apex/CARPOL_CommunityLandingPage');
        insert Urls;
        
        test.startTest();
        
        system.runAs(usershare1)
        {
            app = testdata.newapplicationByRecordTypeIdAndContactStatus(EFLGenericUtility.getRecordTypeId('Application Standard Application'), newContact1.Id, appLineItemQueueRecord[0].QueueSobjects[0].QueueId, 'Waiting on Customer', 1);
            
            id authBRSPermitRecordTypeId = EFLGenericUtility.getRecordTypeId('Authorization_BRS Standard Permit');
            Authorizations__c auth = new Authorizations__c();
            
            LineItem = testdata.newLineItemByAppAuthPoiStatus(app, auth.id, 'Release', 'Waiting on Customer');
            
            CARPOL_UNI_DisableTrigger__c LRAdt = new CARPOL_UNI_DisableTrigger__c(); 
            LRAdt.Name = 'CARPOL_BRS_Link_RegulatedTrigger'; 
            LRAdt.Disable__c = false; 
            insert LRAdt;
            LRA = new Link_Regulated_Articles__c();
            LRA = testData.newlinkRegArticleByLIIdRAId(LineItem.id,RA.id,'Draft');   
            
            CARPOL_UNI_DisableTrigger__c ConDt = new CARPOL_UNI_DisableTrigger__c(); 
            ConDt.Name = 'CARPOL_BRS_ConstructTrigger'; 
            ConDt.Disable__c = true; 
            insert ConDt;
            con = new Construct__c();
            con = testdata.newconstructByLIIdLRAId(LineItem.id, RA.Id, 'Waiting on Customer');
        }
        
        system.runAs(usershare)
        {
            ApexPages.CurrentPage().getparameters().put('id', app.id);      
            Apexpages.StandardController sc = new Apexpages.StandardController(app);
            portal_application_edit_control ext = new portal_application_edit_control(sc);
            boolean canEditApplicant = ext.canEditApplicant;
            try{
            app.Applicant_Name__c = newContact3.id;
            ext.UpdatedApplication();
            }
            catch(exception ex)
            {}
            ApexPages.StandardController controller = ext.controller;
            app.Applicant_Name__c = newContact.id;
            ext.UpdatedApplication();
        }
        
        test.stopTest();
    
    }
}