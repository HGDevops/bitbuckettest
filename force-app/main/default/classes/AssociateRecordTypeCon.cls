public with sharing class AssociateRecordTypeCon{

    

public id recordTypeId{get;set;}
public string BaseUrl;
public AssociateRecordTypeCon(ApexPages.StandardController controller) {
        BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();

    }
    
public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Applicant_Contact__c' ORDER BY name]) {
            for (RecordType rt : rts) {
                options.add(new SelectOption(rt.ID, rt.Name));
            } 
        }
        return options;
    }
public pageReference continueAssociate(){
    string URLString1  =  Label.Portal_Associate_Contact_Create_URL1;   
    string URLString2  =  Label.Portal_Associate_Contact_Create_URL2;   

    BaseUrl= BaseUrl +URLString1+recordTypeId+URLString2;
    PageReference pg = new PageReference(BaseUrl);
    pg.setRedirect(true);
    return pg;
}
/*public pageReference pgAction(){
    List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
    String MyProflieName = PROFILE[0].Name;
    System.debug('===='+MyProflieName);
    //BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    if(MyProflieName !='APHIS Account Admin' || MyProflieName !='APHIS Applicant' || MyProflieName !='APHIS Broker/Preparern'){
       BaseUrl+='/setup/ui/recordtypeselect.jsp?ent=01Ir00000000Alz&retURL=%2Fa12%3Ffcf%3D00Br0000000LeeT&save_new_url=%2Fa12%2Fe%3FretURL%3D%252Fa12%253Ffcf%253D00Br0000000LeeT'; 
    System.debug('==333333333=='+BaseUrl);
    }
    PageReference pg = new PageReference(BaseUrl);
    pg.setRedirect(true);
    return null;
}*/
}