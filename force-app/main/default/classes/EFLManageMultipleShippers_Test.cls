@isTest
public class EFLManageMultipleShippers_Test{

    public static testMethod void EFLManageMultipleShippers_Method1(){  
     
     Domain__c objProg = new Domain__c();
     objProg.Name = 'VS';
     objProg.Active__c = true;
     insert objProg;

    Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
    objParentPathway.Program__c = objProg.Id;
    objParentPathway.Name = 'Live Animals';
    objParentPathway.Status__c = 'Active';  
    System.assert(objParentPathway != null);     
    insert objParentPathway;
    
    CARPOL_AC_TestDataManager testInst = new CARPOL_AC_TestDataManager();
        testInst.insertcustomsettingsWithBRSTriggerDisabled();
    Application__c objapp = testInst.newapplication();
    AC__c ac = newLineItem('Resale/Adoption',objapp); 
      ac.Authorization_Group_String__c =   ac.id;
    
    Applicant_Contact__c appcon = testInst.newappcontact();
    
    Additional_Contact_Detail__c addCon = new Additional_Contact_Detail__c();
    addCon.Last_Name__c = appcon.id;
    addCon.Line_Item__c = ac.id;
    insert addCon;
    
    
    
    Pagereference pageRef = Page.EFLManageMultipleAssocCon;
    pageref.getParameters().put('strPathway',String.valueOf(objParentPathway.id));
    pageref.getParameters().put('lineitemId',String.valueOf(ac.id));
    pageref.getParameters().put('appid',String.valueOf(objapp.id));
    Test.setCurrentPage(pageref);
     
    EFLManageMultipleShippers shipInst = new EFLManageMultipleShippers();
    
    shipInst.newAc.Last_Name__c = appcon.id;
    shipInst.newAc.Line_Item__c = ac.id;
    shipInst.shipperToDelete = appcon.id;
  //  shipInst.lineitemid=String.valueOf(objapp.id);
    
    Level_1_Region__c appL1R = testInst.newlevel1regionAL();
    Level_2_Region__c appL2R = testInst.newlevel2region(appL1R.id);
    shipInst.brandNewAC.Name= 'Associated Contacts';
    shipInst.brandNewAC.First_Name__c= 'apcont';
    shipInst.brandNewAC.Email_Address__c= 'apcont@test.com';
    shipInst.brandNewAC.Phone__c='1234567890';
    shipInst.brandNewAC.Mailing_Country_LR__c=appL1R.Country__c;
    shipInst.brandNewAC.Mailing_State_LR__c=appL1R.Id;
    shipInst.brandNewAC.Mailing_Zip_Postal_Code__c='12345';
    shipInst.brandNewAC.Mailing_State__c = 'Alabama';
    shipInst.brandNewAC.Mailing_County__c = appL2R.Id;
    shipInst.brandNewAC.EFL_Business_Name__c = 'Test';
    shipInst.createAdd();        
    shipInst.populateLastNameLookup();
    shipInst.saveACList();
    shipInst.shipperToDelete = appcon.id;
    shipInst.deleteShipper();
    shipInst.returntoLine();
    shipInst.updateAuthGrpRec(appcon.id); 
    }
    
    public static AC__c newLineItem(string poi, Application__c application)
    {
    CARPOL_AC_TestDataManager testInst2 = new CARPOL_AC_TestDataManager();
    //default record type 'Live Dogs'
    
    Facility__c  entry = testInst2.newfacility('Domestic Port');
    Facility__c  embarkation = testInst2.newfacility('Foreign Port');
      
    AC__c ac = new AC__c();
    Applicant_Contact__c appcont = testInst2.newappcontact();
    if(application == null){
        ac.Application_Number__c = testInst2.newapplication().Id;
    } else {
        ac.Application_Number__c = application.Id;
    }
    ac.Departure_Time__c = date.today()+11;
    ac.Arrival_Time__c = date.today()+15;
    ac.Proposed_date_of_arrival__c = date.today()+15;
    ac.Port_of_Entry__c = entry.Id;
    ac.Port_of_Embarkation__c = embarkation.id;
    ac.Transporter_Type__c = 'Ground';
    ac.RecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('PPQ - Pests and Plant Pathogens').getRecordTypeId();
    ac.Date_of_Birth__c = date.today()-400;
    ac.Breed__c = testInst2.newbreed().id;
    ac.Color__c = 'Brown';
    ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
    ac.Sex__c = 'Male';
    ac.Country_Of_Origin__c = testInst2.newcountryus().Id;
    ac.Program_Line_Item_Pathway__c = testInst2.newCaninePathway().Id;    
    ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
    ac.Importer_Last_Name__c = appcont.id;
    ac.Importer_Mailing_CountryLU__c = [select id from country__c where name = 'United States of America' limit 1].id;
    ac.Exporter_Last_Name__c = appcont.id;
    ac.Status__c = 'Saved';
    ac.Purpose_of_the_Importation__c = poi; //'Resale/Adoption'
    ac.DeliveryRecipient_Last_Name__c = appcont.id;    
    insert ac; 
    ac.Authorization_Group_String__c= ac.id;
    update ac;
    Program_Line_Item_Pathway__c pthwy = [select id,Maximum_number_of_shippers_allowed__c from Program_Line_Item_Pathway__c where id =: ac.Program_Line_Item_Pathway__c];
    pthwy.Maximum_number_of_shippers_allowed__c = 3;
    return ac;         
    }
}