public class EFLESLPackageTriggerHelper {
    
    private static boolean run = true;
    
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    public void attachPermit(List<Attachment> AttachmentList) {
        Set<Id> eslPkgDocIds = new Set<Id>();
        List<Attachment> AttList = new List<Attachment>();
        List<Attachment> AppAttList = new List<Attachment>();
        List<Attachment> AttDelete = new List<Attachment>();
        
        
        for(Attachment att: AttachmentList){
            Id pId = att.parentId;
            
            if(!att.Name.contains('_esigned.pdf')) continue;           
            if (String.valueOf(pId.getSObjectType()) != 'esl__package_document__c') continue;   
            eslPkgDocIds.add(att.ParentId);
        }
        
        if (eslPkgDocIds.size() > 0) {
            Map<Id, ESL__Package_Document__c> eslPkgDocMap = new Map<Id, ESL__Package_Document__c>(
                [SELECT Id, ESL__Package__r.eFile_Signature__c, ESL__Package__r.Incident_Signature__c,
                 ESL__Package__r.Incident_Signature__r.Name,ESL__Package__r.eFile_Signature__r.Permit_Number__c,
                 ESL__Package__r.eFile_Signature__r.Application__r.Name, Name,
                 ESL__Package__r.eFile_Signature__r.Name, ESL__Package__r.eFile_Signature__r.RecordType.DeveloperName                                                              
                 FROM ESL__Package_Document__c 
                 WHERE Id IN: eslPkgDocIds]);
            
            // iterate through eslPkgDocMap and get eFile_Signature__c and ESL__Package_Document__c.Name
            Id authId = null;
            String fileName = null;
            DateTime d = datetime.now();
            String dateTimeStr = d.format('YYYMMDDHHHHSS');
            String AuthRecTypeName = null; 
            
            
            for(Attachment att: AttachmentList){
                AuthRecTypeName = eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__r.RecordType.DeveloperName; 
                
                Id pId = att.parentId;
                if(!att.Name.contains('_esigned.pdf')) continue;
                if (String.valueOf(pId.getSObjectType()) != 'esl__package_document__c') continue;
                authId = eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__c;
                
                if (authId != null) {
                    
                    if (eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__r.Permit_Number__c != null &&
                        eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__r.Permit_Number__c != '' ) {
                            fileName = eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__r.Permit_Number__c + '_';
                        } else {
                            fileName = 'Permit' + '_';
                        }
                    
                    fileName += eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__r.Application__r.Name + '_';
                    fileName += eslPkgDocMap.get(att.ParentId).ESL__Package__r.eFile_Signature__r.Name + '_';
                    fileName += dateTimeStr;
                    fileName += '.pdf';
                    
                    if (att.Name.containsIgnoreCase('Authorization Letter')) {
                        fileName = 'Authorization_Letter_Esgined_' + dateTimeStr + '.pdf';
                    } else if (att.Name.containsIgnoreCase('Denial Letter')) {
                        fileName = 'Denial_Letter_Esgined_' + dateTimeStr + '.pdf';
                    }
                    
                    Attachment a = new Attachment(Body = att.Body, ParentId = authId,Description = att.Description,
                                                  Name = fileName, ContentType = att.ContentType);
                    
                    if (AuthRecTypeName.contains('Biotechnology_Regulatory_Services')) {
                        AppAttList.add(a);
                    } else {
                        AttList.add(a);
                    }
                }
                /*************
authId = eslPkgDocMap.get(att.ParentId).ESL__Package__r.Inspection_Signature__c; 
if (authId != null) {

if (eslPkgDocMap.get(att.ParentId).ESL__Package__r.Inspection_Signature__c != null) {
fileName = eslPkgDocMap.get(att.ParentId).ESL__Package__r.Inspection_Signature__r.Name + '_';
}
fileName += dateTimeStr;
fileName += '.pdf';
Attachment a = new Attachment(Body = att.Body, ParentId = authId,Description = att.Description,
Name = fileName, ContentType = att.ContentType);
AttList.add(a);
}
****************/
                authId = eslPkgDocMap.get(att.ParentId).ESL__Package__r.Incident_Signature__c;

                if (authId != null) {
                    
                    if (eslPkgDocMap.get(att.ParentId).ESL__Package__r.Incident_Signature__c != null) {
                        fileName = eslPkgDocMap.get(att.ParentId).ESL__Package__r.Incident_Signature__r.Name + '_';
                    }
                    fileName += dateTimeStr;
                    fileName += '.pdf';
                    
                    Attachment a = new Attachment(Body = att.Body, ParentId = authId,Description = att.Description,
                                                  Name = fileName, ContentType = att.ContentType);
                    AttList.add(a);
                }
            }
            if (AttList.size() > 0) {
                deleteDraftAtt(eslPkgDocMap);
                insert AttList;
                //deleteDraftAtt(eslPkgDocMap);
            }
            if (AppAttList.size() > 0) {
                createApplAttachment(AppAttList);
            }            
        }       
    }
    
    private void deleteDraftAtt(Map<id,ESL__Package_Document__c> eslPkgDocMap) {

        Map<String, String> AuthAttFileNameMap = new Map<String, String>();
        List<Attachment> AttDraftList = new List<Attachment>();
        
        for (Id pkgdoc: eslPkgDocMap.keySet()) {
            if (eslPkgDocMap.get(pkgdoc).ESL__Package__r.eFile_Signature__c != null) {
                AuthAttFileNameMap.put(eslPkgDocMap.get(pkgdoc).ESL__Package__r.eFile_Signature__c, eslPkgDocMap.get(pkgdoc).Name);
            } else if (eslPkgDocMap.get(pkgdoc).ESL__Package__r.Incident_Signature__c!= null) {
                AuthAttFileNameMap.put(eslPkgDocMap.get(pkgdoc).ESL__Package__r.Incident_Signature__c, eslPkgDocMap.get(pkgdoc).Name);    
            }
            
        }
        
        Pattern fileNamePattern = pattern.compile('Authorization Letter_\\d+/\\d+/\\d+[_v\\d+]*.pdf');
        
        for (Attachment a: [SELECT id, Name FROM Attachment WHERE ParentId IN:AuthAttFileNameMap.keySet()]) {
            for (String id: AuthAttFileNameMap.KeySet()) {
                if(AuthAttFileNameMap.get(id) == a.Name) {
                    AttDraftList.add(a);
                } else if(fileNamePattern.matcher(a.Name).matches()) {
                    // This will include any Authroziation draft letter that was missed if Auth letter have v2, v3, v4 in filename
                    AttDraftList.add(a);
                }
            }
        }
        
        //AttDraftList = [SELECT id FROM Attachment 
        //                                 WHERE ParentId IN:AuthAttFileNameMap.keySet() AND Name IN:AuthAttFileNameMap.values()
        //                                 AND Createddate >= TODAY];        
        if (AttDraftList.size() > 0 )
            Delete AttDraftList;
        
    }
    
    private void createApplAttachment(List<Attachment> AttList) {
        Map<String, String> attParentIdMap = new Map<String, String>();
        List<Applicant_Attachments__c> appAttList = new List<Applicant_Attachments__c>();
        Map<Id, Id> AuthAppIsMap = new Map<Id, Id>();
        Set<Id> authIds = new Set<Id>();
        List<Applicant_Attachments__c> attToDelete = new List<Applicant_Attachments__c>(); 
        
        
        for (Attachment att: AttList) {
            authIds.add(att.ParentId);
        }
        
        
        List<Applicant_Attachments__c> brsPermitAtt = [SELECT Id, Document_Types__c 
                                                       FROM Applicant_Attachments__c 
                                                       WHERE Authorization__c IN: authIds
                                                       ORDER BY createddate desc]; 
        for (Applicant_Attachments__c att: brsPermitAtt) {
            attToDelete.add(att);
            break;
            
        }
        
        if (attToDelete.size() > 0 ) {
            delete(attToDelete);
        }  
        
        
        for (Authorizations__c au: [SELECT Id, Application__c FROM Authorizations__c WHERE ID =: authIds]) {
            AuthAppIsMap.put(au.Id, au.application__c);            
        }
        
        
        
        
        Id AttAppRecTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Permit Package').getRecordTypeId();
        for (Attachment a: AttList) {
            Applicant_Attachments__c appAtt = new Applicant_Attachments__c(
                Authorization__c = a.ParentId,
                Application__c = AuthAppIsMap.get(a.ParentId),
                Document_Types__c = 'eSigned Permit Document',
                RecordTypeId = AttAppRecTypeId,
                File_Description__c = 'Uplodaded ' + System.now(),
                File_Name__c = a.Name
            );
            appAttList.add(appAtt);                                  
        }
        if (appAttList.size() > 0) {
            insert appAttList;
        }
        
        for (Attachment a: AttList) {
            for (Applicant_Attachments__c aa: appAttList) {
                if (a.ParentId == aa.Authorization__c) {
                    a.ParentId = aa.Id;
                }
            }                          
        }
        
        if (AttList.size() > 0) {
            insert AttList;
        } 
        // Delete draft for permit for BRS
        //String CreatedDateStr = DateTime.now().addMinutes(-1).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\''); 
        //String AttSoql = 'SELECT Id FROM Attachment WHERE AND (Name like \'Permit%\' or Name like \'%Permit%\') ParentId IN: ' 
        //                    + appAttList + ' CreatedDate >= TODAY ';// + CreatedDateStr;
        //String AttSoql = 'SELECT Id FROM Attachment WHERE ParentId IN: appAttList  AND CreatedDate >= TODAY';
        
    }
    
    public void isAfterUpdate(List<ESL__Package__c> packageNewList, Map<Id,ESL__Package__c> packageOldMap){
        List<String> lstEmailID = new List<String>();
        Map<id,ESL__Package__c> mapAuthorizationIDWithPackage = new Map<id,ESL__Package__c>();
        
        for(ESL__Package__c esl: packageNewList){
            if(esl.ESL__Signer_Status__c == 'All signers have signed.' && packageOldMap.get(esl.Id).ESL__Signer_Status__c != 'All signers have signed.'){
                mapAuthorizationIDWithPackage.put(esl.id,esl);
                lstEmailID.add(esl.eFile_Signature__c);
            }
        }
        
        //SET<ID> eslPkgIds = new SET<ID>(mapAuthorizationIDWithPackage.keySet());
        Map<String, ESL__Package__c> eslPkgMap = new Map<String, ESL__Package__c>( [SELECT id, eFile_Signature__r.permit_number__c, eFile_Signature__r.Application__r.Name, 
                                                                                    eFile_Signature__r.Name   
                                                                                    FROM ESL__Package__c WHERE ID IN: mapAuthorizationIDWithPackage.keySet()]);
        
        DateTime d = datetime.now();
        String dateTimeStr = d.format('YYYMMDDHHHHSS');
        
        
        //GEt all the related package documents
        Map<id,ESL__Package_Document__c> mapPackageDocument = new Map<id,ESL__Package_Document__c>();
        Map<String,String> eslPkgDocMap = new Map<String, String>();
        String fileName = null;
        
        for(ESL__Package_Document__c esldoc: [SELECT Id, ESL__Document_Id__c, ESL__SourceId__c, ESL__Package__c FROM ESL__Package_Document__c where ESL__Package__c IN: mapAuthorizationIDWithPackage.keyset()]){
            
            mapPackageDocument.put(esldoc.id,esldoc);
            
            if (esldoc.esl__package__c != null) {
                if (eslPkgMap.containsKey(esldoc.esl__package__c)) {
                    ESL__Package__c eslP = eslPkgMap.get(esldoc.esl__package__c);
                    if (eslP.eFile_Signature__r.permit_number__c != '' && 
                        eslP.eFile_Signature__r.permit_number__c != null) {
                            fileName = eslP.eFile_Signature__r.permit_number__c + '_';
                            
                        } else {
                            fileName = 'Permit' + '_';
                        }
                    
                    
                    fileName += eslP.eFile_Signature__r.Application__r.Name + '_';
                    fileName += eslP.eFile_Signature__r.Name + '_';
                    fileName += dateTimeStr;
                    fileName += '.pdf';
                    eslPkgDocMap.put(esldoc.ESL__Package__c, fileName);
                }
            } 
            
        }
        
        List<Attachment> newAttachmentsList = new List<Attachment>();
        
        //attaching document to authorization
        String sFileName = 'test';
        String fileNameStr = null;
        for(Attachment objAtt : [SELECT Body,BodyLength,ContentType, Description,Id, Name,Parentid 
                                 FROM Attachment WHERE Parentid IN: mapPackageDocument.keySet()]){
                                     sFileName = objAtt.Name;
                                     fileNameStr = null;
                                     
                                     if(!sFileName.contains('esigned')) continue;
                                     
                                     //GEt Packagedoucmnet of Attachment
                                     if(mapPackageDocument.get(objAtt.Parentid)==null) continue;
                                     ESL__Package_Document__c objPD = mapPackageDocument.get(objAtt.Parentid);
                                     
                                     //Get package from Packagedocument
                                     if(mapAuthorizationIDWithPackage.get(objPD.ESL__Package__c)==null) continue;
                                     ESL__Package__c objPakage = mapAuthorizationIDWithPackage.get(objPD.ESL__Package__c);
                                     
                                     fileNameStr = objAtt.Name;
                                     if (eslPkgDocMap.get(objPD.ESL__Package__c) != null)
                                         fileNameStr = eslPkgDocMap.get(objPD.ESL__Package__c);
                                     Attachment att = new Attachment(Body = objAtt.Body, ContentType = objAtt.ContentType,
                                                                     Description = objAtt.Description, Name = fileNameStr, ParentId = objPakage.ESL__Parent_Id__c);
                                     newAttachmentsList.add(att);
                                     
                                 }
        
        if(!newAttachmentsList.isEmpty()){
            insert newAttachmentsList;
        }
        
    }
    
    
}