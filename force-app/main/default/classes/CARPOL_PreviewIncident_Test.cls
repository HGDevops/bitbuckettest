@isTest(seealldata=true)
public class CARPOL_PreviewIncident_Test {
    @IsTest static void CARPOL_PreviewIncedent_Test() {
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
         String AccountRecordTypeId = testData.AccountRecordTypeId;
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Application__c objapp = testData.newapplication();
        AC__c li = TestData.newlineitem('Personal Use', objapp);
        Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        Authorizations__c objauth = testData.newAuth(objapp.id);
        li.Authorization__c = objauth.id;
        update li;
        
        update objauth;
        Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
        
        
        //Create Facility
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Facility__c fac = new Facility__c();
        fac.RecordTypeId = PortsFacRecordTypeId;
        fac.Name = 'entryport';
        fac.Type__c = 'Laboratory';  
        insert fac;
        
        
        //Incident__c
        Incident__c incident = new Incident__c();
        incident.Requester_Name__c='testReqName';
        incident.EFL_Template__c=objcm.id;
        incident.Facility__c =fac.id;
        incident.Authorization__c= objauth.id;
        incident.EFL_Issued_Date__c = Date.today();
        incident.Related_Program__c = 'AC';
        incident.Requester_email__c='testReqName@test.com';
        incident.Incident_Date__c = Date.today();
        incident.Incident_Discovery_Date__c = Date.today();
        incident.Incident_Reported_Date__c = Date.today();
        insert incident;
        //Create Inspection__c
        
        Inspection__c inspection = new Inspection__c();
        inspection.Inspector__c=objcont.Id;
        inspection.Facility__c=fac.id;
        inspection.Incident__c=incident.id;
        inspection.Scheduled_Date_of_Inspection__c = date.today();
        // updated 5/20/2019 by JB: inspection causing errors and is not asserted later in test.
        //Insert inspection;
        
        String attachName = 'Incident Letter_' + System.TODAY().format();
        Blob attachBody = Blob.valueOf('UNIT.TEST');
        Attachment attachOne = new Attachment(parentId = incident.ID, name = attachName, 
                                               body = attachBody);
        insert attachOne;      

        Attachment attachTwo = new Attachment(parentId = incident.ID, 
                                          name = 'Draft Authorization Letter_' + System.TODAY().format() + '_v1.pdf', 
                                          body = attachBody);
        insert attachTwo; 
		
        Attachment attachThree = new Attachment(parentId = incident.ID, 
                                          name = 'Draft Authorization Letter_' + System.TODAY().format() + '_v2.pdf', 
                                          body = attachBody);
        insert attachThree;         
        
        Test.startTest(); 
        
        PageReference pageRef = Page.CARPOL_PreviewIncident;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(incident);
        ApexPages.currentPage().getParameters().put('id',incident.id);
        ApexPages.currentPage().getParameters().put('tId',objwft.id);
        
        CARPOL_PreviewIncident extclass = new CARPOL_PreviewIncident(sc);
        extclass.populateTemplate(); 
        
        extclass.cancel(); 
        extclass.attachLetter();
        List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :Incident.Id AND 
                                            Name LIKE 'Draft Authorization Letter_%' ORDER BY CreatedDate];
        system.debug('size=== ' + prevAttachments.size());
        extclass.attachDraftLetter();
        
        extclass.saveDraft();
        extclass.previewTemplate();
        extclass.attachandSignLetter();
        extclass.sendEmail();
        extclass.reviewerComments = '';
        system.assert(extclass != null);
        Test.stopTest();    
        
    }
    
}