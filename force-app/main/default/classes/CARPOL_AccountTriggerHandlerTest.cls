@isTest
public class CARPOL_AccountTriggerHandlerTest{
 static list<account> acctLst = new list<account>();
 static testmethod  void test1(){
    account newAcct = new account(name = 'Test Account',type='Partner');
    insert newAcct;    
    newAcct.isPartner = true;
    update newAcct;    
    acctLst.add(newAcct);   
    Group pg = new Group(Name=newAcct.name);
    insert pg;
    test.startTest();       
      CARPOL_AccountTriggerHandler.afterUpdateHandler(acctLst);
    test.stopTest();
 }
 static testmethod  void test2(){
    account newAcct = new account(name = 'ABC Inc.',type='Partner');
    insert newAcct;    
    newAcct.isPartner = true;
    update newAcct;    
    acctLst.add(newAcct);   
    test.startTest();       
      CARPOL_AccountTriggerHandler.afterUpdateHandler(acctLst);
    test.stopTest();
 }
}