public with sharing class CARPOL_UNI_GetCountryList {
    
    public List<SelectOption> getCountryList(Map<String, String> allAnswersMap){
        List<SelectOption> options = new List<SelectOption>();
        for(Country__c recCountry: [Select Name from Country__c where Country_Status__c=:'Active' Order By Name]){
                options.add(new SelectOption(recCountry.Id,recCountry.Name));
                allAnswersMap.put(recCountry.Id,recCountry.Name);
            }
            return options;
    }
}