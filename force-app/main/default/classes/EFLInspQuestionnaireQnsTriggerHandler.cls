public with sharing class EFLInspQuestionnaireQnsTriggerHandler implements EFLITrigger{
	
	
	 public static Boolean TriggerDisabled = false;
	 list<EFL_Inspection_Questionnaire_Questions__c> updateInspectionQuestionnaireQnList = new list<EFL_Inspection_Questionnaire_Questions__c>();
 
    /*
    * Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() 
    {
       if (EFLGenericUtility.isTriggerDisabled('EFLInspQuestionnaireQn'))
            return true;
        else
            return TriggerDisabled;
    }    
    
    /*
     * Constructor
     */ 
    public EFLInspQuestionnaireQnsTriggerHandler()
    {
    
     }
 
    /**
     * bulkBefore
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
    	set<id> inspectionIdSet = new set<id>();
        if(trigger.isInsert){        	
        	for(EFL_Inspection_Questionnaire_Questions__c qns : (List<EFL_Inspection_Questionnaire_Questions__c>)trigger.new){
           		inspectionIdSet.add(qns.Inspection__c);	
        	} 
        	if(inspectionIdSet!=null && inspectionIdSet.size()>0){
        		EFLBRSInspQuestionnaireQnsEngine.fetchQnCountForInspections(inspectionIdSet); 
        	}
        }
    }
    
    /**
     * bulkAfter
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter()
    {
    	set<id> inspectionIdSet = new set<id>();
        if(trigger.isDelete){
        	for(EFL_Inspection_Questionnaire_Questions__c qns : (List<EFL_Inspection_Questionnaire_Questions__c>)trigger.old){
           		inspectionIdSet.add(qns.Inspection__c);	
        	} 
        	if(inspectionIdSet!=null && inspectionIdSet.size()>0){
        		EFLBRSInspQuestionnaireQnsEngine.fetchQnsForReorder(inspectionIdSet);
        	}
        }
    	
    }    
  
    /**
     * beforeInsert
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so)
    {    
    	EFL_Inspection_Questionnaire_Questions__c newQnRecord = (EFL_Inspection_Questionnaire_Questions__c)so;
    	EFLBRSInspQuestionnaireQnsEngine.setOrder(newQnRecord); 
    } 

    /**
     * beforeUpdate
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so)
    {
        
    } 
    
    /**
     * beforeDelete
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {
        
    }
 
    /**
     * afterInsert
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */ 
    public void afterInsert(SObject so)
    {
        
       
    }

    /**
     * afterUpdate
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so)
    { 
        
    } 

    /**
     * afterDelete
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */ 
    public void afterDelete(SObject so)
    {
    	list<EFL_Inspection_Questionnaire_Questions__c> inspectionQuestionList = new list<EFL_Inspection_Questionnaire_Questions__c>();
    	EFL_Inspection_Questionnaire_Questions__c delQnRecord = (EFL_Inspection_Questionnaire_Questions__c)so;
    	if(EFLBRSInspQuestionnaireQnsEngine.inspectionQuestionMap!=null && EFLBRSInspQuestionnaireQnsEngine.inspectionQuestionMap.containsKey(delQnRecord.inspection__c)){
    		inspectionQuestionList = EFLBRSInspQuestionnaireQnsEngine.inspectionQuestionMap.get(delQnRecord.inspection__c);
    		
    		EFLInspQuestionnaireQnsEngine engine = EFLInspQuestionnaireQnsEngineFactory.getEngine(delQnRecord);
       		engine.reOrderInspectionQuestions(inspectionQuestionList);  
       		 
    		updateInspectionQuestionnaireQnList.addAll(inspectionQuestionList);
    	}
    	
    }
 
    /**
     * andFinally
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
     
    public void andFinally()
    {
       if(!updateInspectionQuestionnaireQnList.isEmpty()){
       		try{
       			update updateInspectionQuestionnaireQnList;
       		}catch(Exception e){
       			System.debug('Exception Occured Updating InspectionQuestionnaireQns');
       		}
       }
    }
    
}