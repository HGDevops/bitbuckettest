@isTest
public class EFLAR_RelatedListServices_Test {
    @TestSetup
    static void makeData(){
        Account account = new Account();
        account.Name = 'Test Account';
        insert account;
        
        Contact contact1 = new Contact();
        contact1.FirstName = 'Test';
        contact1.LastName = 'Test 1';
        contact1.AccountId = account.Id;

        Contact contact2 = new Contact();
        contact2.FirstName = null;
        contact2.LastName = 'Test 2';
        contact2.AccountId = account.Id;

        insert new List<Contact>{contact1, contact2};
    }

    @isTest
    public static void doDelete_RecordsDeleted(){
        Map<Id, Contact> contacts = new Map<Id, Contact>([Select Id From Contact]);
        System.assertEquals(2, contacts.keySet().size());
        
        EFLAR_RelatedListServices rls = new EFLAR_RelatedListServices();
        rls.doDelete(new List<Id>(contacts.keySet()), 'Contact', true);
        
        contacts = new Map<Id, Contact>([Select Id From Contact]);

        System.assertEquals(0, contacts.keySet().size());
    }


    @isTest
    public static void getRecords_DataReturned(){
        // query the mdt for the setup details.
        EFL_Related_List_Instance__mdt instance = [SELECT Title__c, SObject_Type__c, Parent_Lookup_Field__c, Icon_Name__c, Enable_Row_Selection__c, 
                                                   Edit_Screen_Component__c, Enforce_User_Access_Checks__c, Security_Helper_Class__c,
                                                   Service_Class__c, Permanent_Delete_on_Delete__c,Query_Filter__c, Order_By__c,
                                                   Add_New_Screen_Component__c, Object_Common_Name__c, Title_Sub_Text__c, Edit_Screen_CSS_File__c,
                                                   Save_Button_Text__c, Cancel_Button_Text__c,
                                                   (SELECT Field_API_Name__c, Field_Label__c, Sort_Order__c, Help_Text__c FROM EFL_Related_List_Fields__r
                                                    ORDER BY Sort_Order__c ASC)
                                                   FROM EFL_Related_List_Instance__mdt
                                                   WHERE Usage_Key__c = 'Test EFL AR Community Contact Table' And Is_Test_Data__c = True];
        
        Set<String> fields = new Set<String>{'FirstName', 'LastName', 'AccountId'};
		Account account = [Select Id From Account Limit 1];
        
        EFLAR_RelatedListServices rls = new EFLAR_RelatedListServices();
        List<Contact> contacts = rls.getRecords(instance, fields, account.Id);
        
        System.assertEquals(1, contacts.size());
        System.assertEquals('Test', contacts[0].FirstName);
        System.assertEquals('Test 1', contacts[0].LastName);
        System.assertEquals(account.Id, contacts[0].AccountId);
    }
}