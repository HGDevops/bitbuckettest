/*
* Purpose: Provides Core functionality around Location Object
*/ 
public with sharing class EFLLocationUtility{
    
    /**Sort Locations - pass in lineItemId*/
    public static list<Location__c> getSortedLocations(id lineItemID){        
        return sortLocations(EFLLocationRepository.selectByLineItemId(lineItemID));         
    }
    /**Searched Sort Locations - pass in lineItemId*/
    //W-027406 - Last Minute Fix
    public static list<Location__c> getSearchedSortedLocations(id lineItemID, string searchString){        
        return sortLocations(EFLLocationRepository.searchLocByKeywordWithInLineItemID(lineItemID, searchString));         
    }
    
    /**Sort Locations - pass in list of locations*/
    public static list<Location__c> getSortedLocations(List<Location__c> locationsToSort){
        return sortLocations(locationsToSort);        
    }
    
    /*Sort Locations by following Criteria:
1.Sorted first by recordtype in this order (Origins,Origin and Destinations, Destinations, Release Sites) 
2.Sorted by state in alphabetical order and then by counties in alphabetical . 
3.Release Sites it should be sorted by state in alphabetical order and 
then by county alphabetical order, 
then by alpha numeric order by name of unique location ID.*/
    private static list<Location__c> sortLocations(List<Location__c> locationsToSort){
        
        Map<string,list<Location__c>> locationMap = new Map<string,list<location__c>>();
        list<location__c> sortedLocationList = new list<location__c>();
        //list<Location__c> locationList = EFLLocationRepository.selectByLineItemId(lineItemID);
        
        for(Location__c loc: locationsToSort){
            if(loc.recordtype.Name != null){
                if(locationMap.containsKey(loc.recordtype.Name)) {
                    List<Location__c> locationtoadd = locationMap.get(loc.recordtype.Name);
                    locationtoadd.add(loc);
                    locationMap.put(loc.recordtype.Name, locationtoadd);
                } else {
                    locationMap.put(loc.recordtype.Name, new List<Location__c> { loc });
                }
            } 
        }
        sortedLocationList = sortByRecordTypes(locationMap);
        return sortedLocationList;  
    }
    
    /*
* Sort locations by record Types (Origins,Origin and Destinations, Destinations, Release Sites) 
*/   
    private static list<Location__c> sortByRecordTypes(Map<string,list<Location__c>> locationMap){
        list<location__c> OriginLocationList = new list<location__c>();
        list<location__c> OriginandDestinationLocationList = new list<location__c>();
        list<location__c> DestinationLocationList = new list<location__c>();
        list<location__c> ReleaseSiteLocationList = new list<location__c>();
        list<location__c> sortedLocationList = new list<location__c>();
        OriginLocationList = locationMap.get('Origin Location');
        
        if(OriginLocationList!=null)
            sortedLocationList.addAll(OriginLocationList);
        OriginandDestinationLocationList = locationMap.get('Origin and Destination Location');
        if(OriginandDestinationLocationList!=null)
        {
            //OriginandDestinationLocationList.sort();
            sortedLocationList.addAll(OriginandDestinationLocationList);
        }         
        DestinationLocationList = locationMap.get('Destination Location');
        if(DestinationLocationList!=null)
            sortedLocationList.addAll(DestinationLocationList);
        ReleaseSiteLocationList = locationMap.get('Release Sites Location'); 
        if(ReleaseSiteLocationList!=null)
            sortedLocationList.addAll(ReleaseSiteLocationList);
        return sortedLocationList;
    }
    
    
}