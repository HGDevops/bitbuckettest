public with sharing class EFLConstructs {

    /* Variables */
    public transient List<Construct__c> Constructs {get; set;}
    public boolean showConstructView {get; set;}
    public EFLConstructController pagecontroller{get; set;}
    public Construct__c constructRecord {get; set;}
    public id conId {
    get{
     conId = null;
     if(ApexPages.currentPage().getParameters().get('id')!=null)
     {
      conId = (id)ApexPages.currentPage().getParameters().get('id');
     }
     return conId;
    }
    set;
    }
    
    /* Constructor */
    Public EFLConstructs()
    {
        Constructs = new List<Construct__c>();
        if(conId!=null){
         showDetailView();
        }
        else
        {
         Constructs = EFLConstructRepository.selectAllConstructs();
        }
    }
    
    /* Show Construct View */
    
   public void showDetailView(){
        if(conId!=null){
            constructRecord = new construct__c();
            constructRecord = EFLConstructRepository.selectByID(conId);
            ApexPages.currentPage().getParameters().put('LineItemId',constructRecord.Line_Item__c);
            pagecontroller = new EFLConstructController();
            pagecontroller.constructID = constructRecord.Id;
            pagecontroller.showDetailView();
            showConstructView = true;
        }
     }

}