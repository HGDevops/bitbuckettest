public with sharing class CARPOL_SomethingWentWrong {
    
    public Application__c application = new Application__c();
    public ID applicationID;
    
    public CARPOL_SomethingWentWrong(ApexPages.StandardController controller)
    {
        applicationID = ApexPages.CurrentPage().getparameters().get('id');
        this.application = (Application__c)controller.getRecord();
    }
    
    public PageReference goBackApplication()
    {
        PageReference applicationDetailsRedirect = new PageReference('/' + application.Id);
        applicationDetailsRedirect.setRedirect(true);
        return applicationDetailsRedirect;
    }
    
    public PageReference deleteTransaction()
    {
        try
        {
            Transaction__c trans = [Select Id, Name From Transaction__c where Application__c =: applicationID LIMIT 1];
            if(trans != null){
                delete trans; //delete the transaction as the user cancelled.
            }
            PageReference dirpage=new  PageReference('/apex/portal_application_detail?id='+application.Id); //VV W-017588 02-20-18
            dirpage.setRedirect(true);
            return dirpage;
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong, please contact your System Administrator for details.'));
        }
        catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        
        return null;
        
    }
    
}