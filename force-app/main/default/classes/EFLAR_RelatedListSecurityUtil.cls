public without sharing class EFLAR_RelatedListSecurityUtil implements EFLRelatedListSecurityUtil_Base {
    public static User getMe(){
        return [SELECT Id, ContactId, Contact.Id, Contact.EFL_Facility_Admin__c, Contact.EFL_ACIS_Contact__c,
                Contact.EFL_ACIS_Contact__r.EFL_Facility_Admin__c FROM User WHERE Id = :UserInfo.getUserId()];
    }
    
    public static boolean canDelete(sObject c, User my, boolean defaultValue){
        if(my.ContactId == null){return defaultValue;}// user is internal, use OOTB sharing.
        
        if(my.Contact.EFL_ACIS_Contact__c == c.Id){ // you can't delete yourself.
            //system.debug('contact Ids match, cannot delete');
            return false;
        }else if(my.Contact.EFL_ACIS_Contact__r.EFL_Facility_Admin__c == true){ // admins can delete others.
            //system.debug('my.Contact.EFL_ACIS_Contact__r.EFL_Facility_Admin__c == true, CAN Delete');
            return true;
        }else{
        	return defaultValue; // none apply, go with standard record sharing.
        }
    }
    
    public static boolean canEdit(sObject c, User my, boolean defaultValue){
        if(my.ContactId == null){return defaultValue;}// user is internal, use OOTB sharing.
        
        if(my.Contact.EFL_ACIS_Contact__r.EFL_Facility_Admin__c == true){ // only admins can edit.
            //system.debug('my.Contact.EFL_ACIS_Contact__r.EFL_Facility_Admin__c == true, CAN Edit');
            return true;
        }
        return defaultValue; // follow OOTB sharing
    }
}