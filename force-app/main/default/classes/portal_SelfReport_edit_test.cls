@isTest
public class portal_SelfReport_edit_test {
    
    @IsTest static void test_portal_selfreporting() {
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Application__c objApp = TestData.newapplication();
       // AC__c li = TestData.newlineitem('Personal Use', null);
        Authorizations__c objAuth = TestData.newAuth(objApp.Id);
        AC__c li = testdata.newLineItemByAppAuthPoiStatus(objApp, objAuth.id, 'Release', 'Waiting on Customer');
            
        Location__c loc= new Location__c();
        loc = testdata.newlocationByLineItemAndStatus(li.Id, EFLGenericUtility.getRecordTypeId('Location Release Sites'), 'Saved');
        Construct__c objcaj = new Construct__c();
        objcaj = testData.newconstruct(li.id);
        
        Report_Summary__c plantingRS = testData.newReportSummayByTypeStatus(objAuth.id, CARPOL_Constants.PLANTING_REPORT, 'UnSubmitted');
        Self_Reporting__c sr = testData.newPlantingSelfReporting(objAuth.Id, plantingRS.id, loc.id);
        //Self_Reporting__c sr= new Self_Reporting__c();
       // sr = testdata.newSelfReporting(objcaj.id, objauth.id); 	
      
        User usershare = new User();
        usershare = EFLUserTestDataFactory.getUser('eFile Applicant');    
        
        //Hand_Carry_Information_Form
        id HCIFRecTypeId = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByDeveloperName().get('Hand_Carry_Information_Form').getRecordTypeId();
        id PRRecTypeId = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByDeveloperName().get('Planting_Report').getRecordTypeId();
        id VMRRecTypeId = Schema.SObjectType.Self_Reporting__c.getRecordTypeInfosByDeveloperName().get('Volunteer_Monitoring_Report').getRecordTypeId();
        
        Test.startTest();
        system.RunAs(usershare)
        {
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
            ApexPages.currentPage().getParameters().put('id',sr.id);
            ApexPages.currentPage().getParameters().put('AuthoId', objAuth.id);
            ApexPages.currentPage().getParameters().put('rectype', HCIFRecTypeId);
            portal_SelfReport_edit extclass = new portal_SelfReport_edit(sc);
            ApexPages.currentPage().getParameters().put('rectype', PRRecTypeId);
            extclass = new portal_SelfReport_edit(sc);
            ApexPages.currentPage().getParameters().put('rectype', VMRRecTypeId);
            extclass = new portal_SelfReport_edit(sc);
            ID rectype = extclass.rectype;
        }
        
        Test.stopTest();   
    }
    
}