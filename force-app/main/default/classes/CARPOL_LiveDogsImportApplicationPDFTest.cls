@isTest(seealldata=false)
private class CARPOL_LiveDogsImportApplicationPDFTest {
    static testMethod void testCARPOL_LiveDogsImportApplicationPDF() {

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
          String AccountRecordTypeId = testData.AccountRecordTypeId;
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();      
      AC__c ac = testData.newLineItem('Personal Use',objapp);        
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
      CARPOL_LiveDogsImportApplicationPDF cls = new CARPOL_LiveDogsImportApplicationPDF(sc);
        cls.liveDogCount = 1;
      system.assert(cls != null);
      }
     }