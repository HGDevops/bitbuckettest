public Interface IChangeHistoryHandler {
    void populateNewRecordDetails(Sobject newRecord, Change_History__c change);
    void populateUpdateDetails(Sobject newRecord, Sobject oldRecord, Change_History__c change);
    void populateDeletedRecordDetails(Sobject deletedRecord, Change_History__c change);
    void populateChangeHistoryLookupValues(Sobject record, Change_History__c change);

}