@isTest(seealldata=true)

private class CARPOL_Inspection_Report_test{
static Report_Summary__c rs;
    static Self_Reporting__c sr;
    static EFL_Related_Record__c eflRelatedRec;
private static testMethod void testCARPOL_inspection_report() {

         string dlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
        string olocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
        string locrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
        string oanddlocrectypeid=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();

        CARPOL_BRS_TestDataManager testData=new  CARPOL_BRS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        AC__c ac1 = testData.newLineItem('Personal Use',objapp); 
        ac1.Authorization__c = objauth.id;
        ac1.Release_Start_Date__c =  date.today() + 60;
        ac1.Release_End_Date__c = date.today() + 60;
        update ac1;
        Link_Regulated_Articles__c linkra=testdata.newlinkRegArticle(ac1.id,objapp.Id,objauth.id);
        construct__c objconst=testdata.newconstruct(ac1.id);
        Genotype__c objgeno=testdata.newgenotype(objconst.id);
        Phenotype__C objpheno=testdata.newphenotype(objconst.id);
        Country__c UScountry=testdata.newcountryus();
        Level_1_Region__c level1reg=testdata.newlevel1region(UScountry.id);
        Level_2_Region__c level2reg=testdata.newlevel2region(level1reg.id);
        test.startTest();
        location__c loc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,olocrectypeid);
        location__c desloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,dlocrectypeid);
        location__c relloc=testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,locrectypeid);
        location__c oanddloc = testData.newlocation(UScountry.id,level1reg.id,level2reg.id,ac1.id,oanddlocrectypeid); 
     Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
        objcaj.Line_Item__c = ac1.id;
        objcaj.Application__c = objapp.id;
        insert objcaj;
        
          rs = new Report_Summary__c();
        rs.Authorization__c = objauth.id;
        rs.Certify_and_Submit__c = true;
        rs.Description__c = 'Test';
        rs.Due_Date__c = date.today() + 60;
        rs.Equipment__c = 'Facility';
        rs.Report_Type__c = 'Volunteer Monitoring Report';
        rs.Status__c = 'UnSubmitted';
        insert rs;
      
        sr= new Self_Reporting__c();
        sr.Monitoring_Period_Start__c = date.today()-1;
        sr.Monitoring_Period_End__c = date.today();
        sr.Observation_Date__c = date.today();
        sr.Number_of_Volunteers__c = 5;
        sr.Release_Record_ID__c = loc.id;
        sr.Authorization__c = objauth.id;
        sr.Action_Taken__c = 'Test description';
        sr.report_summary__c = rs.id;
        sr.Release_Record_ID__c = loc.id;
        sr.Any_Planting_Material_Harvested__c = 'No';
        sr.In_field_Termination_Date__c = date.today() + 60;
        sr.In_Field_Description__c = 'Test description';
        sr.How_was_material_disposed__c = 'Both';
        sr.Stored_Quantity__c = 5;
        //sr.Units__c = 'Acres';
        sr.Stored_Material_Type__c = 'test';
        sr.Stored_Description__c = 'Test';
        sr.Off_Field_Destruction_Date__c = date.today() + 61;
        sr.Off_Field_Description__c = 'test';
        sr.Before_Harvest_Destruction_Date__c = date.today() + 61;
        sr.Before_Harvest_Description__c = 'test';
        sr.Still_Growing_Quantity__c = 4;
        sr.Still_Growing_Description__c = 'test';
        sr.Anticipated_Harvest_Destruct_Date__c = date.today() + 61;
        sr.Planted_Material_Destroyed_Before_Harves__c = 'Yes';
        sr.Planting_Material_Still_Growing__c  = 'Yes';
        sr.Explanation__c = 'test';
        sr.Unexpected_Effects__c = 'test';
        sr.Deleterious_Effects__c = 'test';
        sr.Deleterious_Effects_Data__c = 'test';
        sr.Crop_Observations__c = 'test';
        sr.Final_Volunteer_Monitoring_Report__c = 'Yes';
    	sr.Start_Date__c = date.today();
        insert sr;
          
          
         
        eflRelatedRec = new EFL_Related_Record__c();
        eflRelatedRec.Authorization__c = objauth.id;
        eflRelatedRec.Self_Reporting__c = sr.id;
        eflRelatedRec.Location__c = loc.id;
        eflRelatedRec.Construct__c  = objconst.id;
        eflRelatedRec.Description__c = 'Test';
        insert eflRelatedRec;    
        
      Inspection__c Ins = new Inspection__c();
      String InsRecTypeID = Schema.SObjectType.Inspection__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services(BRS)').getRecordTypeId();
      Ins.RecordTypeId = InsRecTypeID;
    insert Ins;
        
        EFL_Inspection_Questionnaire__c Questionnaire = new EFL_Inspection_Questionnaire__c ();
        String QuestionnnaireRecTypeID = Schema.SObjectType.EFL_Inspection_Questionnaire__c.getRecordTypeInfosByName().get('Compliance officer/staff').getRecordTypeId();
        Questionnaire.RecordTypeId = QuestionnnaireRecTypeID;
        Questionnaire.Inspection__c = Ins.Id;
        Questionnaire.Location__c = Loc.ID;
        Questionnaire.Planting_ID__c = sr.Id;
        insert Questionnaire;
    
        ApexPages.currentPage().getParameters().put('Id',Ins.id);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(Ins);
        CARPOL_Inspection_Report objCPQMYA= new CARPOL_Inspection_Report(stdCon);
        test.stopTest();
        
}

}