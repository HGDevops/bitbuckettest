public with sharing class EFLFileListController {
    
    @AuraEnabled public static PageData getPageData(list<Id> parentIds, string filterLogic){
        list<string>columns = new list<string>();
        map<string,string> columnMapping = new map<string,string>();
        // use the usage key to find the config mdt
        try{
            EFL_FileList_Instance__mdt config = [SELECT Id, Allow_Delete__c, Allow_Multiple_Files__c, Allow_Upload__c,
                                                 Message_when_no_Records__c, New_File_Modal_Component__c, Sub_header_Text__c,
                                                 Title__c, Show_Created_Date_Column__c, New_File_Modal_Button_Text__c, Fixed_Table_Width__c,
                                                 Show_Border_in_UI__c
                                                 FROM EFL_FileList_Instance__mdt 
                                                 WHERE Usage_Key__c = :filterLogic 
                                                 AND Is_Test_Data__c = :Test.isRunningTest() 
                                                 LIMIT 1];
            
            PageData pd = new PageData(config);
            
            if(string.isNotBlank(filterLogic)){
                for(EFLFileList_Column_Mapping__mdt m : [SELECT Column_Header_Text__c, Uploaded_File_Field_To_map__c, Order__c
                                                         FROM EFLFileList_Column_Mapping__mdt
                                                         WHERE Usage_Key__c = :filterLogic AND Is_Test_data__c = :test.isRunningTest()
                                                         ORDER BY Order__c ASC]){
                                                             columns.add(m.Column_Header_Text__c);
                                                             columnMapping.put(m.Column_Header_Text__c, m.Uploaded_File_Field_To_map__c);
                                                         }
            }
            pd.setHeaders(columns);
            system.debug('parentIds: ' + parentIds);
            pd.files = getFiles(parentIds, filterLogic, columnMapping, columns);
            return pd;
        }catch(Exception e){
            system.debug('exception: ' + e.getMessage());
            return null;
        }
    }
    
    public static list<UploadedFile> getFiles(list<Id> parentIds, string filterLogic, map<string,string> addColumns, list<string> columnsInOrder){
        set<string> constants = new set<string>{'Id', 'Name', 'CreatedDate', 'External_URL__c','File_Preview_Id__c','Is_Deletable__c'};
            Set<string> columns = new Set<string>();
        columns.addAll(constants); // always to be in place.
        if(addColumns != null && !addColumns.isEmpty()){
            columns.addAll(addColumns.values());
        }
        string qry = 'SELECT ' + string.join(new List<string>(columns),','); 
        qry += ' FROM Uploaded_File__c';
        qry += ' WHERE ParentId__c IN :parentIds AND Is_Deleted__c = false';
        system.debug('qry: ' + qry);
        if(string.isNotBlank(filterLogic)){
            for(EFLFileList_Filter_Logic__mdt f : [SELECT Field_Name__c, Field_Value__c 
                                                   FROM EFLFileList_Filter_Logic__mdt 
                                                   WHERE Usage_Key__c = :filterLogic
                                                   AND Is_Test_data__c = :test.isRunningTest()
                                                  ]){
                                                      qry += ' AND ' + f.Field_Name__c + ' = \'' + f.Field_Value__c + '\'';
                                                  }
        }
        
        if(UserInfo.getusertype() != 'Standard'){
            qry += ' AND EFLInternal_Only__c = false';
        }
        list<UploadedFile> fileList = new list<UploadedFile>();
        
        for(Uploaded_File__c uf : Database.query(qry)){
            UploadedFile upf = new UploadedFile(uf);
            integer i = 0;
            for(string s : columnsInOrder){
                upf.customValues.add(new FieldOutput(s,uf.get(addColumns.get(s)),i));
                i += 1;
            }
            fileList.add(upf);
        }
        fileList.sort();
        return fileList;
    }
    
    @AuraEnabled public static void setupNewFile(string cds, Id parentId, string fileDetails){
        system.debug('TESTESTESTESTESTEST: ');
        system.debug('file Details: ' + fileDetails);
        // after a contentdocument has been created, clone its details into an uploaded_file__c record
        list<LightningFileUpload> cdUploads = (list<LightningFileUpload>)JSON.deserialize(cds, list<LightningFileUpload>.class);
        map<Id, String> cdMap = new map<Id, String>();
        for(LightningFileUpload lfu : cdUploads){
            cdMap.put(lfu.documentId, lfu.name);
        }
        list<Uploaded_File__c> ufs = makeNewFiles(cdUploads, parentId, fileDetails, cdMap);
        if(!ufs.isEmpty()){
        	insert ufs;
        }
    }
    
    public static list<Uploaded_File__c> makeNewFiles (list<LightningFileUpload> cdUploads, Id parentId, string fileDetails, map<Id, String> cdMap){
        // clone the content document and reparent it to the new Uploaded_File__c record
        list<Uploaded_File__c> ufs = new list<Uploaded_File__c>();
        list<ContentDocumentLink> cdls = [SELECT Id, ContentDocument.Title, LinkedEntityId, ContentDocumentId, ContentDocument.FileType 
                                          FROM ContentDocumentLink 
                                          WHERE ContentDocumentId IN :cdMap.keySet()
                                          AND LinkedEntityId = :parentId];
        
        List<EFLFileList_Default_Detail__mdt> fileDetailsList = null;
        if(string.isNotBlank(fileDetails)){
            fileDetailsList = [SELECT Field_Value__c, Field_Name__c FROM EFLFileList_Default_Detail__mdt
                               WHERE Usage_Key__c = :fileDetails AND Is_Test_data__c = :test.isRunningTest()];
        }
        
        system.debug('file details to set: ' + fileDetailsList);
        
        for(ContentDocumentLink cdl:cdls){
            string n = cdl.ContentDocument.Title;
            if(n.length() > 80){ // Name field can only be 80 characters
                n = n.left(80);
            }
            Uploaded_File__c uf = new Uploaded_File__c(
                Name = n,
                ParentId__c = parentId,
                File_Preview_Id__c = cdl.ContentDocumentId,
                File_Type__c = cdl.ContentDocument.FileType,
                Full_File_Name__c = cdl.ContentDocument.Title
            );
            
            // move outside of for-loop and make this a map.
            if(fileDetailsList != null){
                for(EFLFileList_Default_Detail__mdt f: fileDetailsList){
                    uf.put(f.Field_Name__c, f.Field_Value__c);
                }
            }
            ufs.add(uf);
        }
        return ufs;
    }
    
    public class LightningFileUpload{
        @AuraEnabled public string name{get;set;}
        @AuraEnabled public string documentId{get;set;}
    }
    
    @AuraEnabled public static void deleteFile(Id fileId, boolean doDelete){
        // this is done in a without sharing class to allow DML
        EFLFileListServices.doDelete(fileId, doDelete);
    }
    
    // this is a wrapper class for Uploaded_File__c object.
    // Uploaded_File__c will have a single File record related to it.
    // The Uploaded_File__c record allows users to categorize a File.
    public class UploadedFile implements Comparable{
        @AuraEnabled public Id id{get;set;}
        @AuraEnabled public Id previewId{get;set;}
        @AuraEnabled public string fileName{get;set;}
        @AuraEnabled public string fileDescription{get;set;}
        @AuraEnabled public string iconName{get;set;}
        @AuraEnabled public string fileType{get;set;}
        @AuraEnabled public datetime fileCreated{get;set;}
        @AuraEnabled public string fileOwner{get;set;}
        @AuraEnabled public string downloadLink{get;set;}
        @AuraEnabled public boolean isDeletable{get;set;}
        @AuraEnabled public list<FieldOutput> customValues{get;set;}
        
        public UploadedFile(ContentDocument cd){
            this.fileName = cd.Title;
            this.fileDescription = cd.Description;
            this.id = cd.Id;
            this.fileType = cd.FileExtension;
            this.fileCreated = cd.CreatedDate;
            this.fileOwner = cd.Owner.Name;
            this.previewId = cd.Id;
        }
        
        public UploadedFile(Uploaded_File__c f){
            
            this.fileName = f.Name;
            this.id = f.Id;
            this.fileCreated = f.CreatedDate;
            this.customValues = new list<FieldOutput>();
            if(f.External_URL__c == null){
                string baseURL = '';
                if(userinfo.getusertype() == 'Standard'){ //for internal users
                    baseURL = URL.getSalesforceBaseUrl().toExternalForm();         
                } else{ //for community users
                    baseURL = connectapi.Communities.getCommunity(Network.getNetworkId()).siteUrl;    
                }
                //prepare download link for the current file
                this.downloadLink = baseURL+'/sfc/servlet.shepherd/document/download/'+f.File_Preview_Id__c + '?operationContext=S1';
            }else{
                this.downloadLink = f.External_URL__c;
            }
            this.previewId = f.File_Preview_Id__c;
            this.isDeletable = f.Is_Deletable__c;
        }
        
        public Integer compareTo(Object compareTo) {
            UploadedFile compareToFile = (UploadedFile)compareTo;
            if (fileCreated == compareToFile.fileCreated) return 0;
            if (fileCreated > compareToFile.fileCreated) return -1;
            return 1;        
        }
    }
    
    public class FieldOutput implements Comparable{
        @AuraEnabled public string columnHeader{get;set;}
        @AuraEnabled public object value{get;set;}
        @AuraEnabled public integer order{get;set;}
        @AuraEnabled public string dataType{get;set;}
        public FieldOutput(string h,object o, integer ord){
            this.columnHeader = h;
            this.value = o;
            this.order = ord;
        }
        
        public Integer compareTo(Object compareTo) {
            FieldOutput compareToFile = (FieldOutput)compareTo;
            if (order == compareToFile.order) return 0;
            if (order > compareToFile.order) return -1;
            return 1;        
        }
    }
    
    public class fileDataMapping{
        @AuraEnabled public string uploadedFileField{get;set;}
        @AuraEnabled public string uploadedFileValue{get;set;}
    }
    
    public class filterMapping{
        @AuraEnabled public string fieldName{get;set;}
        @AuraEnabled public string fieldValue{get;set;}
        @AuraEnabled public string condition{get;set;}
    }
    
    public class PageData{
        @AuraEnabled public list<string> columnHeaders{get;set;}
        @AuraEnabled public list<UploadedFile> files{get;set;}
        @AuraEnabled public map<string,string> columnMappings{get;set;}
        @AuraEnabled public EFL_FileList_Instance__mdt config{get;set;}
        @AuraEnabled public boolean isCommunity{get;set;}
        
        public PageData(EFL_FileList_Instance__mdt i){
            this.config = i;
            this.columnHeaders = new list<string>();
            this.files = new list<UploadedFile>();
            this.columnMappings = new map<string,string>();
            this.isCommunity = EFLUtils.getCommunityContext();
        }
        
        public void setHeaders(list<string> addColumns){
            this.columnHeaders =new list<string>();
            this.columnMappings = new map<string,string>();
            this.columnHeaders.add(Label.EFLAR_File_List_File_Name);
            this.columnMappings.put(Label.EFLAR_File_List_File_Name,'fileName');
            for(string s : addColumns){
                this.columnHeaders.add(s);
            }
            if(this.config.Show_Created_Date_Column__c == true){
            	this.columnHeaders.add(Label.EFLAR_File_List_Created_Date);
            	this.columnMappings.put(Label.EFLAR_File_List_Created_Date,'fileCreated');
            }
            
        }
    }
}