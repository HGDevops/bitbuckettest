global inherited sharing class EFLBatchCBPXMLClass {
    
    // public static List<Authorizations__c> auths { get; set; }
    
    global EFLBatchCBPXMLClass() {
    }
    @future(callout=true)
    global static void getXML(){
        
        //Run date, set here and used throughout xml generation 
        DateTime rundate = DateTime.Now();
        List<attachment> attachToInsertList = new List<attachment>();
        Map<String, EFLIntegrationDocReference__c> errorLogList = new Map<String, EFLIntegrationDocReference__c>();
        List<EFLIntegrationDocReference__c> eflIntDocList = new List<EFLIntegrationDocReference__c>();
        List<Authorizations__c> auths = new List<Authorizations__c>();
        Map<String, Blob> mapBlobAuth = new Map<String, Blob>();
        Decimal attachmentSizeSent = 0;
        Boolean isEFLAttReady = True;
        Boolean CBPSentFlag = False;
        Date queryDate = system.today().addDays(-1);
        String objectType = CARPOL_Constants.CBP_DOCREF_OBJECT_TYPE;
        String PERMIT_PDF_ERRORSTR = CARPOL_Constants.PERMIT_PDF_ERRORSTR;
        String PERMIT_PDF_ERROR = CARPOL_Constants.PERMIT_PDF_ERROR;
        String PERMIT_ATTACHMENT_ERROR = CARPOL_Constants.PERMIT_ATTACHMENT_ERROR;
        
        //Query all document Ids that are on the EFLIntegrationDocReference__c that have been modified in the last 24 hours
        eflIntDocList = EFLIntegrationDocReferenceRepository.selectByLastModified(objectType, queryDate, CBPSentFlag);
        if(!eflIntDocList.isEmpty()){
            SpringCMService sprCMService;
            for(EFLIntegrationDocReference__c efldocId : eflIntDocList){
                if (sprCMService == null){
            		sprCMService = new SpringCMService(null);
                }
                String springCMdocumentId = efldocId.Document_ID__c;
                //Make a callout to SpringCM to retrieve documents
                //
                Blob pdfAttachment = EFLBatchCBPXMLClassHelper.getDocument(springCMdocumentId, sprCMService);
                if(pdfAttachment==null){
                    //system.debug('No pdfAttachment >> ' + pdfAttachment);
                    efldocId.CBP_ErrorText__c = PERMIT_PDF_ERROR + efldocId.Object_Name__c + PERMIT_PDF_ERRORSTR;
                    errorLogList.put(efldocId.Id, efldocId);
                }
                else{
                    //Getting map of Authorization Names and EFL Doc Ids to create pdf Attachments
                    mapBlobAuth.put(efldocId.Object_Name__c,pdfAttachment);
                    //system.debug('mapBlobAuth >> '+mapBlobAuth);
                    //attachmentSizeSent keeps up with the amount of total bytes of all the permit PDF attachments processed
                    attachmentSizeSent += pdfAttachment.size();
                    efldocId.CBP_SentFlag__c=true;
                }   
            }                                               
            
            //Retrieving all appropriate authorizations that meet criteria and have been modified in the last 24 hours
            List<Authorizations__c> queriedAuths = EFLIntegrationDocReferenceRepository.selectByAuth(mapBlobAuth);
            
            //add non-expired authorizations
            for (Authorizations__c auth : queriedAuths){
                if (auth.Expiration_Date__c >= Date.today()){
                    auths.add(auth);
                }
            }
            
            /*
            //Need to also fetch superceded parent auths
            Map<String, Blob> parentAuthIds = new Map<String, Blob>();
            Set<String> authIds = new Set<String>();
            for (Authorizations__c auth : auths){
                authIds.add(auth.Id);
            }
            //system.debug('AuthIds: ' + authIds);
            for (Amendment_Renewal__c parent : [SELECT Id, Parent_Authorization__r.Name FROM Amendment_Renewal__c WHERE Child_Authorization__c IN :authIds]){
                if (String.isNotBlank(parent.Parent_Authorization__r.Name)){
                    parentAuthIds.put(parent.Parent_Authorization__r.Name, null);
                }
            }
            //system.debug('Parent Auth Ids: ' + parentAuthIds.keyset());
            List<Authorizations__c> parentAuths = EFLIntegrationDocReferenceRepository.selectByAuth(parentAuthIds);
            for (Authorizations__c parentAuth : parentAuths){
                if (parentAuth.Status__c == 'Superceded'){
                    auths.add(parentAuth);
                }
            }*/
            Map<String, Authorizations__c> dedupedAuths = new Map<String, Authorizations__c>();
            for (Authorizations__c auth : auths){
                //validate the status is usable
                if (EFLBatchCBPXMLClassHelper.validateStatus(auth)){
                    dedupedAuths.put(auth.Id, auth);
                }
            }
            auths = dedupedAuths.values();
            
            //only create an integration log if there are auths to run
            if (auths.size() > 0){                
                //Generate an EFLIntegrationLog record so we can reference it in our XML        
                EFLIntegrationLog__c efllog = new EFLIntegrationLog__c();
                efllog.EFLIntegrationJobType__c = 'CBP Transfer';
                efllog.EFLJobRunDate__c = rundate;
                insert efllog;
                
                //Get all attachments to add to the EFLIntegrationLog record
                attachToInsertList = EFLBatchCBPXMLClassHelper.createAttachmentList(auths,efllog.Id,mapBlobAuth,attachmentSizeSent);
                Database.SaveResult[] attList = Database.insert(attachToInsertList, false);
                for (Database.SaveResult att : attList) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : att.getErrors()) {
                        isEFLAttReady = False;
                        //system.debug(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
                
                // Attachments Insert Operation is a success, so set EFL Log Record Status to Ready to be picked up for transfer 
                if(isEFLAttReady){
                    efllog.Status__c = 'Ready';
                    update efllog;
                }else {
                    efllog.Status__c = 'Failed';
                    update efllog;                
                }
                
                //Update EFLIntegrationLog records with Error Text in case of errors
                if(errorLogList != null && errorLogList.size() > 0){
                    update errorLogList.values();
                }
                
                //update the EFL Docs to sent
                Boolean markerFileSuccess = true;
                List<EFLIntegrationDocReference__c> eflDocsToUpdate = new List<EFLIntegrationDocReference__c>();
                for(EFLIntegrationDocReference__c efldoc : eflIntDocList){
                    if (!errorLogList.containsKey(efldoc.Id)){
                        if (isEFLAttReady){
                            efldoc.CBP_SentFlag__c = true;
                            efldoc.CBP_ErrorText__c = '';
                            eflDocsToUpdate.add(efldoc);
                        }else {
                            markerFileSuccess = false;
                            efldoc.CBP_ErrorText__c = PERMIT_PDF_ERROR + efldoc.Object_Name__c + PERMIT_ATTACHMENT_ERROR;                        
                            eflDocsToUpdate.add(efldoc);
                        }
                    }
                }
                update eflDocsToUpdate;
            }
            
            /*Integer bodyLength = 0;
            List<String> attachmentIds = new List<String>();
            
            for (Attachment attachment : attachToInsertList){
                attachmentIds.add(attachment.Id);
            }
            
            List<Attachment> attachmentsInserted = [SELECT Id, Name, BodyLength FROM Attachment WHERE Id IN :attachmentIds];   
            for (Attachment attachment : attachmentsInserted){
                if (!attachment.Name.contains('.xml')){
                    bodylength += attachment.BodyLength;
                }
            }
            insert EFLBatchCBPXMLClassHelper.createMarkerFile(efllog.Id, markerFileSuccess, bodylength);*/
        }
    }
}