@istest
public class CARPOL_UNI_eAuthExternalProfileReqTest{
    static testmethod void testOne(){
        CARPOL_AC_TestDataManager testData=new  CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objAcct=testData.newAccount(AccountRecordTypeId);
        Contact objCont=testData.newContact();
        Preparer_Request__c objPrepReq=new  Preparer_Request__c();
        objPrepReq.Preparer_Broker_Name__c=objCont.id;
        objPrepReq.Applicant_Account1__c=objAcct.id;
        objPrepReq.Contact_Email__c='test@testNoDeliver.au';
        objPrepReq.Last_Name__c='Test Last';
        objPrepReq.Contact_Phone__c='703-123-1234';
        Insert objPrepReq;
        Profile p=[SELECT Id FROM Profile WHERE Name IN ('APHIS Applicant','eFile Applicant') Limit 1];
        User usr=new  User(Alias='TstAppl',Email='TestAphisAppl@testAphis.com',EmailEncodingKey='UTF-8',LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=p.Id,TimeZoneSidKey='America/Los_Angeles',UserName='TestAphisAppl@testAphis.com',Phone='703-123-1234',ContactID=objCont.id);
        test.startTest();
        system.runas(usr){
            pagereference pageref=Page.CARPOL_UNI_eAuthExternalProfileRequest;
            Test.setCurrentPage(pageRef);
            CARPOL_UNI_eAuthExternalProfileRequest objCARPOL=new  CARPOL_UNI_eAuthExternalProfileRequest(new  ApexPages.StandardController(objPrepReq));
            objCARPOL.getProfileOptions();
            objCARPOL.submitRequest();
        }
        test.stopTest();
    }
    static testmethod void testtwo(){
        CARPOL_AC_TestDataManager testData=new  CARPOL_AC_TestDataManager();
        String AccountRecordTypeId = testData.AccountRecordTypeId;
        Account objAcct=testData.newAccount(AccountRecordTypeId);
        //Contact objCont=new Contact(firstname='Tst', lastname='Appl2', accountid=objAcct.id);
        //insert objCont;
        Contact objCont=testData.newContact();
        Preparer_Request__c objPrepReq=new  Preparer_Request__c();
        objPrepReq.Preparer_Broker_Name__c=objCont.id;
        objPrepReq.Applicant_Account1__c=objAcct.id;
        objPrepReq.Contact_Email__c='test@testNoDeliver.au';
        objPrepReq.Last_Name__c='Test Last';
        objPrepReq.Contact_Phone__c='703-123-1234';
        Insert objPrepReq;
        Profile p=[SELECT Id FROM Profile WHERE userlicense.name = 'Customer Community Login' Limit 1];
        User usr=new  User(Alias='TstAppl2',Email='TestAphisAppl@testAphis.com',EmailEncodingKey='UTF-8',LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=p.Id,TimeZoneSidKey='America/Los_Angeles',UserName='TestAphisAppl2@testAphis.com',Phone='703-123-1234',ContactID=objCont.id);
        insert usr;
        system.runas(usr){
        test.startTest();
            pagereference pageref=Page.CARPOL_UNI_eAuthExternalProfileRequest;
            Test.setCurrentPage(pageRef);
            CARPOL_UNI_eAuthExternalProfileRequest objCARPOL=new  CARPOL_UNI_eAuthExternalProfileRequest(new  ApexPages.StandardController(objPrepReq));
            objCARPOL.getProfileOptions();
            objCARPOL.submitRequest();
        }
        test.stopTest();
    }    
}