@isTest//(seeAllData=true)
private class CARPOL_BRS_ROP_Cond_CollaborationTest {
    static testMethod void testCARPOL_BRS_ROP_Cond_Collaboration() {
        User usershare = new User();
        usershare.Username ='aphistestemail@test.com';
        usershare.LastName = 'APHISTestLastName';
        usershare.Email = 'APHISTestEmail@test.com';
        usershare.alias = 'APHtest';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name='System Administrator'].Id;
        userShare.UserRoleId = [SELECT Id FROM UserRole 
                                WHERE Name = 'ADMIN' LIMIT 1].id;
        usershare.LanguageLocaleKey = 'en_US';
        system.runas(usershare){
            
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            testData.insertcustomsettings();
            String AccountRecordTypeId = testData.AccountRecordTypeId;
            Account objacct = testData.newAccount(AccountRecordTypeId); 
            Contact objcont = testData.newcontact();
            breed__c objbrd = testData.newbreed(); 
            Applicant_Contact__c apcont = testData.newappcontact(); 
            Applicant_Contact__c apcont2 = testData.newappcontact();
            Facility__c fac = testData.newfacility('Domestic Port');  
            Facility__c fac2 = testData.newfacility('Foreign Port');
            Application__c objapp = testData.newapplication();
            AC__c ac = testData.newLineItem('Release',objapp);      
            AC__c ac3 = testData.newLineItem('Import',objapp);
            Regulation__c objreg1 = testData.newRegulation('Standard','');
            Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
            Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
            Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
            Attachment attach = testData.newattachment(ac.Id);           
            Authorizations__c objauth = testData.newAuth(objapp.Id); 
            ac.Authorization__c = objauth.Id;
            update ac;
            
            Test.startTest();
            
            Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
            Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
            Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
            Workflow_Task__c objWF = testData.newworkflowtask('Process Authorization', objauth, 'Complete');
            
            String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
            String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
            String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
            objreg4.RecordTypeid = ReguRecordTypeId;
            update objreg4;
            Domain__c objprog = testData.newProgram('BRS');
            Program_Prefix__c objPrefix = new Program_Prefix__c();
            objPrefix.Program__c = objprog.Id;
            objPrefix.Name = '101';
            objPrefix.Permit_PDF_Template__c = 'CARPOL_BRS_StandardPermit';
            insert objPrefix;
            Signature__c objTP = new Signature__c();
            objTP.Name = 'Test BRS KK';
            objTP.Recordtypeid = BRSTPRecordTypeId ;
            objTP.Program_Prefix__c = objPrefix.Id;
            insert objTP; 
            
            
            PageReference pageRef = Page.CARPOL_BRS_ROP_Cond_Collaboration;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Authorizations__c>());
            ApexPages.currentPage().getParameters().put('Id',objauth.id);
            ApexPages.currentPage().getParameters().put('wfid',objWF.id);
            CARPOL_BRS_ROP_Cond_Collaboration acepreg = new CARPOL_BRS_ROP_Cond_Collaboration(sc);
            acepreg.options = 'All';
            acepreg.biotechrole = true;
            acepreg.save();
            acepreg.getFilteredConditions();
            acepreg.savesuppconditions();
            acepreg.redirect();
            acepreg.redirectPermitPage();
            List<Authorization_Junction__c> lstAJ = acepreg.getAddInformation();
            List<Authorization_Junction__c> lstAJ1 = acepreg.getRegulations();
            acepreg.approveAll();
            acepreg.approveAllRop();   
            System.assert(acepreg != null);
            
            Test.stopTest();
        }
    }
    
    static testMethod void testCARPOL_BRS_ROP_Cond_Collaboration3() {
        
        User usershare = new User();
        usershare.Username ='aphistestemail@test.com';
        usershare.LastName = 'APHISTestLastName';
        usershare.Email = 'APHISTestEmail@test.com';
        usershare.alias = 'APHtest';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name='BRS ROP Reviewer'].Id;
        userShare.UserRoleId = [SELECT Id FROM UserRole 
                                WHERE Name = 'ROP Director' LIMIT 1].id;
        usershare.LanguageLocaleKey = 'en_US';
        system.runas(usershare){
            
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            String AccountRecordTypeId = testData.AccountRecordTypeId;
            testData.insertcustomsettings();
            Account objacct = testData.newAccount(AccountRecordTypeId); 
            Contact objcont = testData.newcontact();
            breed__c objbrd = testData.newbreed(); 
            Applicant_Contact__c apcont = testData.newappcontact(); 
            Applicant_Contact__c apcont2 = testData.newappcontact();
            Facility__c fac = testData.newfacility('Domestic Port');  
            Facility__c fac2 = testData.newfacility('Foreign Port');
            Application__c objapp = testData.newapplication();
            AC__c ac = testData.newLineItem('Release',objapp);      
            AC__c ac3 = testData.newLineItem('Import',objapp);
            Regulation__c objreg1 = testData.newRegulation('Standard','');
            Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
            Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
            Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
            Attachment attach = testData.newattachment(ac.Id);           
            Authorizations__c objauth = testData.newAuth(objapp.Id); 
            ac.Authorization__c = objauth.Id;
            update ac;
            
            Test.startTest();
            
            Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
            Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
            Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
            Workflow_Task__c objWF = testData.newworkflowtask('Process Authorization', objauth, 'Complete');
            
            String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
            String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
            String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
            objreg4.RecordTypeid = ReguRecordTypeId;
            update objreg4;
            Domain__c objprog = testData.newProgram('BRS');
            Program_Prefix__c objPrefix = new Program_Prefix__c();
            objPrefix.Program__c = objprog.Id;
            objPrefix.Name = '101';
            objPrefix.Permit_PDF_Template__c = 'CARPOL_BRS_StandardPermit';
            insert objPrefix;
            Signature__c objTP = new Signature__c();
            objTP.Name = 'Test BRS KK';
            objTP.Recordtypeid = BRSTPRecordTypeId ;
            objTP.Program_Prefix__c = objPrefix.Id;
            insert objTP; 
            
            PageReference pageRef = Page.CARPOL_BRS_ROP_Cond_Collaboration;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Authorization_Junction__c>());
            ApexPages.currentPage().getParameters().put('Id',objauth.id);
            ApexPages.currentPage().getParameters().put('wfid',objWF.id);
            CARPOL_BRS_ROP_Cond_Collaboration acepreg = new CARPOL_BRS_ROP_Cond_Collaboration(sc);
            acepreg.options = 'Collaborated';
            acepreg.managerandaboverole= true;
            acepreg.save();
            acepreg.getFilteredConditions();
            acepreg.savesuppconditions();
            acepreg.redirect();
            acepreg.redirectPermitPage();
            List<Authorization_Junction__c> lstAJ = acepreg.getAddInformation();
            List<Authorization_Junction__c> lstAJ1 = acepreg.getRegulations();
            acepreg.approveAll();
            acepreg.approveAllRop();   
            System.assert(acepreg != null);
            
            Test.stopTest();
        }
    }
    static testMethod void testCARPOL_BRS_ROP_Cond_Collaboration4() {
        User usershare = new User();
        usershare.Username ='aphistestemail@test.com';
        usershare.LastName = 'APHISTestLastName';
        usershare.Email = 'APHISTestEmail@test.com';
        usershare.alias = 'APHtest';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name='BRS ROP Reviewer'].Id;
        userShare.UserRoleId = [SELECT Id FROM UserRole 
                                WHERE Name = 'BRAP Manager' LIMIT 1].id;
        usershare.LanguageLocaleKey = 'en_US';
        
        System.runAs(usershare)
        {
            
            CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
            testData.insertcustomsettings();
            String AccountRecordTypeId = testData.AccountRecordTypeId;
            Account objacct = testData.newAccount(AccountRecordTypeId); 
            Contact objcont = testData.newcontact();
            breed__c objbrd = testData.newbreed(); 
            Applicant_Contact__c apcont = testData.newappcontact(); 
            Applicant_Contact__c apcont2 = testData.newappcontact();
            Facility__c fac = testData.newfacility('Domestic Port');  
            Facility__c fac2 = testData.newfacility('Foreign Port');
            Application__c objapp = testData.newapplication();
            AC__c ac = testData.newLineItem('Release',objapp);      
            AC__c ac3 = testData.newLineItem('Import',objapp);
            Regulation__c objreg1 = testData.newRegulation('Standard','');
            Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
            Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
            Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
            Attachment attach = testData.newattachment(ac.Id);           
            Authorizations__c objauth = testData.newAuth(objapp.Id); 
            ac.Authorization__c = objauth.Id;
            update ac;
            
            Test.startTest();
            
            Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
            Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
            Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
            Workflow_Task__c objWF = testData.newworkflowtask('Process Authorization', objauth, 'Complete');
            
            String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
            String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
            String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
            objreg4.RecordTypeid = ReguRecordTypeId;
            update objreg4;
            Domain__c objprog = testData.newProgram('BRS');
            Program_Prefix__c objPrefix = new Program_Prefix__c();
            objPrefix.Program__c = objprog.Id;
            objPrefix.Name = '101';
            objPrefix.Permit_PDF_Template__c = 'CARPOL_BRS_StandardPermit';
            insert objPrefix;
            Signature__c objTP = new Signature__c();
            objTP.Name = 'Test BRS KK';
            objTP.Recordtypeid = BRSTPRecordTypeId ;
            objTP.Program_Prefix__c = objPrefix.Id;
            insert objTP; 
            
            PageReference pageRef = Page.CARPOL_BRS_ROP_Cond_Collaboration;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Authorizations__c>());
            ApexPages.currentPage().getParameters().put('Id',objauth.id);
            ApexPages.currentPage().getParameters().put('wfid',objWF.id);
            CARPOL_BRS_ROP_Cond_Collaboration acepreg = new CARPOL_BRS_ROP_Cond_Collaboration(sc);
            acepreg.options = 'Collaborated';
            //acepreg.ropreviwersrole= true;
            acepreg.save();
            acepreg.getFilteredConditions();
            acepreg.savesuppconditions();
            acepreg.redirect();
            acepreg.redirectPermitPage();
            List<Authorization_Junction__c> lstAJ = acepreg.getAddInformation();
            List<Authorization_Junction__c> lstAJ1 = acepreg.getRegulations();
            acepreg.approveAll();
            acepreg.approveAllRop();   
            System.assert(acepreg != null);
            Test.stopTest();
        }
    }    
    
}